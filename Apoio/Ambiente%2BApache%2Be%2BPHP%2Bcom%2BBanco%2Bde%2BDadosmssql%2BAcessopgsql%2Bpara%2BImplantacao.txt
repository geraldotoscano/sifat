### Recursos necess�rios para configura��o do ambiente
Apache 2.2
PHP 5.2
Postgre 8.4

### Arquivos necess�rios para o PHP5 na pasta c:/windows/system32
php5ts.dll 
php5apache2_2.dll
php_pdo.dll
php_pdo_mssql.dll
msql.dll


### Configura��es do httpd.conf do Apache para PHP 5 

#Localize o m�dulo de reencaminhamento, descomentar (retirando o "#" do in�cio da linha)
LoadModule rewrite_module modules/mod_rewrite.so

#Localize as diretivas para sobrescrever as configura��es do DocumentRoot para:
AllowOverride All

#Localize a linha que define o "DirectoryIndex"
#Adicione o "index.php",  por exemplo:

DirectoryIndex index.html index.php


#Adicione as configura��es para o funcionamento do m�dulo php 5
PHPIniDir "C:\php"
LoadModule php5_module "c:\php\php5apache2_2.dll"

#Configura��es para funcionamento do Zend

AddType application/x-httpd-php .php
AddType application/x-httpd-php .phtml
AddType application/x-httpd-php-source .phps
AddType "text/html; charset=ISO-8859-1" html

AccessFileName .htaccess


### Configura��es do php.ini do PHP 5 

#Verifique se o php suporta tags abreviadas "<? ?>"
short_open_tag = On

#Localize a seguinte linha: extension_dir = "./"
#Configure para: "c:\php\ext", por exemplo:

extension_dir = "c:\php\ext"


#� necess�rio descomentar (retirando o ";" do in�cio da linha) das seguintes extens�es:

extension=php_msql.dll
extension=php_mssql.dll
extension=php_pdo.dll
extension=php_pdo_mssql.dll
extension=php_pdo_pgsql.dll
extension=php_pgsql.dll

#Para funcionamento no linux
#instalar o m�dulo sybase para conex�o com SQL Server atrav�s do pdo_dblib
#editar o arquivo rpa/aplication/config/config.php substituindo "php_mssql" para "pdo_dblib"