Implanta��o - Sistema SiFat-Intranet SLU
Migra��o e Saneamento de arquivos de dados.

1 - Migra��o Geral (  F  E  I  T  O  )
2 - Migra��o de faturas excluidas para as repectivas tabelas de faturas canceladas (  F  E  I  T  O  )
    * CADFAT_CANC (  F  E  I  T  O  )
    * MOVFAT_CANC (  F  E  I  T  O  )
3 - Exclus�o de registros duplicados. (  F  E  I  T  O  )
4 - Altera��o de todos os valores num�ricos de nulo para zero.(  F  E  I  T  O  )

5 - Cadastramento  das al�quotas de INSS,ISSQN,UFIR dentre outros em suas respectivas vig�ncias.(  F  E  I  T  O  )

6 - Manter o arquivo de usu�rios e senhas TABOPE como o do banco de homologa��o.(  F  E  I  T  O  )

7 - Alterar o atributo VALMUL da tabela CADFAT e CADFAT_CANC para o valor do juro proporcional referente ao juro de 
    um dia sobre o valor da fatura a taxa de 1%(um) por cento ao m�s (VALFAT/100/30).(  F  E  I  T  O  )

8 - Testar a altera��o de senha com a senha do miguel.(  F  E  I  T  O  )
    Obs : A altera��o de senhas n�o est� funcionando.

9 - Em funcionando o item 8, eliminar a senha do Miguel no arquivo TABOPE.(  F  E  I  T  O  )
10 - Em funcionando os itens 8 e 9 sugerir ao usu�rio que mude a sua senha de acesso.(  F  E  I  T  O  )
11 - Eliminar a senha do Rosangela no arquivo TABOPE em ambiente de homologa��o.(  F  E  I  T  O  )
