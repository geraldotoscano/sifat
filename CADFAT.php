<?php
   global $DBfaturar;
   $Page = CCGetParentPage($sender);
   $db  = dbase_open('c:/sistfat/cadfat.dbf', 2);
   $def = array(
     array("CODFAT",   "C",  06),
     array("CODCLI",   "C",  06),
     array("MESREF",   "C",  07),
     array("DATEMI",   "D"     ),
     array("DATVNC",   "D"     ),
     array("VALFAT",   "N",12,2),
     array("VALMOV",   "N",12,2),
     array("RET_INSS", "N",12,2),
     array("DATPGT",   "D"     ),
     array("VALPGT",   "N",12,2),
     array("VALCOB",   "N",12,2),
     array("VALJUR",   "N",12,2),
     array("VALMUL",   "N",12,2),
     array("EXPORT",   "L"     ),
     array("PGTOELET", "C",  01),
     array("CONSIST",  "C",  01),
     array("CONSIST_M","C",  01),
     array("ISSQN",    "N",07,2)
   );
   $db1 = dbase_create('c:/sistfat/cadfatex.dbf', $def);
   if (!$db) 
   {
      echo "N�o consegui";
   }
   else
   {
      $record_numbers = dbase_numrecords($db);
      for ($i = 1; $i <= $record_numbers; $i++) 
      {
         $row = dbase_get_record_with_names($db, $i);
         $CODFAT    = $row['CODFAT'];
         $CODCLI    = $row['CODCLI'];
         $MESREF    = $row['MESREF'];
         $DATEMI    = $row['DATEMI'];
         $DATVNC    = $row['DATVNC'];
         $VALFAT    = $row['VALFAT'];
         $VALMOV    = $row['VALMOV'];
         $RET_INSS  = $row['RET_INSS'];
         $DATPGT    = $row['DATPGT'];
         $VALPGT    = $row['VALPGT'];
         $VALCOB    = $row['VALCOB'];
         $VALJUR    = $row['VALJUR'];
         $VALMUL    = $row['VALMUL'];
         $EXPORT    = $row['EXPORT'];
         $PGTOELET  = $row['PGTOELET'];
         $CONSIST   = $row['CONSIST'];
         $CONSIST_M = $row['CONSIST_M'];
         if ($row['deleted'] != 1 and $CODCLI = CCGetParam("CODCLI","")) 
         {
            $mINSS_MIM = $CADFAT->HINSS_MIM->Value;
			$mdias     = 0;
            //             C A L C U L O   D O S   J U R O S
            if ($VALPGT > 0.00) 
            {
               $mvr_multa  = 0.00;
            }
            else 
            {           
               //$mdias = CCDLookUp("date() - datvnc", "cadfat", "codfat = '".$CODFAT."'", $Page->Connections["Faturar"]);
			   $mdias = (int)$mdias;
               if ($mdias <= 0) //date() - datvnc
               {
                  $mvr_multa = 0.00;
               }
               else 
               {
                  if (substr($MESREF,3,4).substr($MESREF,0,2)>'199901')
                  {
                     $nInss  = round(($VALFAT)*11/100,2);
                     if ($nInss <= $mINSS_MIM)
                     {
                        $nInss = 0;
                     }
                  }
                  else
                  {
                     $nInss = 0;
                  }
                  $mdias = (int)$CADFAT->Hidden1->Value;
                  //echo $mDataHoje.'-'.$mDataVnc.'-';
                  $mvr_multa = round ((1 * ($VALFAT - $nInss)) / 100 / 30 * $mdias, 2);
               }
            }
            $VALJUR = $mvr_multa;
            //             C A L C U L O   D O   I S S Q N 
            if ((substr($MESREF,3,4).substr($MESREF,0,2))<='199901')
            {
               $ISSQN = 0.00;
            }
            else
            {
               $ISSQN = round($row['VALFAT'] * 2 / 100,2);
            }
            //             C � L C U L O   D O   I N S S
            if (substr($MESREF,3,4).substr($MESREF,0,2)>'199901')
            {
               $nInss  = round(($VALFAT)*11/100,2);
               if ($nInss <= $mINSS_MIM)
               {
                  $nInss = 0;
               }
            }
            else
            {
               $nInss = 0;
            }
            $RET_INSS = $nInss;
            //                                C � L C U L O   D O   D � B I T O
           if (substr($MESREF,3,4).substr($MESREF,0,2)>'199901')
           {
              $mIssqn = round(($VALFAT)*2/100,2);
              $nInss  = round(($VALFAT)*11/100,2);
              if ($nInss <= $mINSS_MIM)
              {
                 $nInss = 0;
              }
           }
           else
           {
              $mIssqn = 0;
              $nInss = 0;
           }
           if ($VALPGT > 0.00)
           {
              $mdeb_atual = 0.00;
           }
           else 
           {
              if ($mdias  <= 0) //date() - datvnc
              {
                 $mvr_multa = 0.00;
              }
              else 
              {
                 $mvr_multa = round ((1 * ($VALFAT - $nInss)) / 100 / 30 * $mdias, 2);
              }
              $mdeb_atual = $VALFAT + $mvr_multa - ($nInss + $mIssqn);
           }
           $VALMUL = $mdeb_atual;
            dbase_add_record($db1, array(
                                           $CODFAT,
                                           $CODCLI,
                                           $MESREF,
                                           $DATEMI,
                                           $DATVNC,
                                           $VALFAT,
                                           $VALMOV,
                                           $RET_INSS,
                                           $DATPGT,
                                           $VALPGT,
                                           $VALCOB,
                                           $VALJUR,
                                           $VALMUL,
                                           $EXPORT,
                                           $PGTOELET,
                                           $CONSIST,
                                           $CONSIST_M,
                                           $ISSQN
                                        )
                            );
         }
      }
   }
   dbase_close($db);
   dbase_close($db1);
?> 
