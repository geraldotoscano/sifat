<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="3" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="23" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="CADCLI_GRPATI_SUBATI_TABD" wizardCaption="Search CADCLI GRPATI SUBATI TABD " wizardOrientation="Vertical" wizardFormMethod="post" returnPage="CadCadCli.ccp" debugMode="False" pasteAsReplace="pasteAsReplace">
			<Components>
				<TextBox id="25" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_CODCLI" wizardCaption="CODCLI" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" html="False" editable="True" hasErrorCollection="True">
					<Components/>
					<Events>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="131" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="S_DESCLI">
					<Components/>
					<Events>
						<Event name="OnKeyPress" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="137"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="26" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_CGCCPF" wizardCaption="CGCCPF" wizardSize="14" wizardMaxLength="14" wizardIsPassword="False" html="False" editable="True" hasErrorCollection="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="127" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_CPF" html="False" editable="True" hasErrorCollection="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="140" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_OPUS">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="141" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_GERAL">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<Link id="129" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link1" hrefSource="ManutCadCadCli1.ccp" wizardUseTemplateBlock="False" editable="False">
					<Components/>
					<Events/>
					<LinkParameters/>
					<Attributes/>
					<Features/>
				</Link>
				<Button id="24" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="142" secured="False" sourceType="SQL" returnValueType="Number" defaultPageSize="10" connection="Faturar" name="cadcli" pageSizeLimit="100" wizardCaption="List of Cadcli " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="Não encontrado." activeCollection="SQLParameters" parameterTypeListName="ParameterTypeList" dataSource="SELECT * 
FROM (SELECT CODCLI, DESCLI, CGCCPF,OPUS, upper(CODCLI||DESCLI||DESFAN||CODUNI||CODDIS||CODSET||GRPATI||SUBATI||CODTIP||CGCCPF||LOGRAD||BAIRRO||CIDADE||ESTADO||CODPOS||TELEFO||TELFAX||CONTAT||LOGCOB||BAICOB||CIDCOB||ESTCOB||POSCOB||TELCOB||DATALT||PGTOELET||ESFERA||OPUS||EMAIL) tudo FROM CADCLI ) CADCLI 
WHERE (CODCLI LIKE '%{s_CODCLI}%' and not trim('{s_CODCLI}') is null )
OR (CGCCPF LIKE '%{s_CGCCPF}%' and not trim('{s_CGCCPF}') is null )
OR (DESCLI LIKE '%{S_DESCLI}%' and not trim('{S_DESCLI}') is null )
OR (TUDO LIKE upper('%{s_GERAL}%')  and not trim('{s_GERAL}') is null )
OR (OPUS LIKE upper('%{s_OPUS}%')  and not trim('{s_OPUS}') is null )
OR (TUDO LIKE '%' and trim('{s_GERAL}') is null and trim('{S_DESCLI}') is null and trim('{s_CGCCPF}') is null and trim('{s_CODCLI}') is null and trim('{s_OPUS}') is null and trim('{s_CODCLI}') is null )"><Components><Sorter id="143" visible="True" name="Sorter_CODCLI" column="CODCLI" wizardCaption="CODCLI" wizardSortingType="SimpleDir" wizardControl="CODCLI" wizardAddNbsp="False"><Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="144" visible="True" name="Sorter_DESCLI" column="DESCLI" wizardCaption="DESCLI" wizardSortingType="SimpleDir" wizardControl="DESCLI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="145" visible="True" name="Sorter_CGCCPF" column="CGCCPF" wizardCaption="CGCCPF" wizardSortingType="SimpleDir" wizardControl="CGCCPF" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="146" visible="True" name="Sorter_OPUS" column="OPUS" wizardCaption="OPUS" wizardSortingType="SimpleDir" wizardControl="OPUS" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Link id="147" fieldSourceType="DBColumn" dataType="Text" html="False" name="CODCLI" fieldSource="CODCLI" wizardCaption="CODCLI" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="ManutCadCadCli1.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="157" sourceType="DataField" name="CODCLI" source="CODCLI"/>
					</LinkParameters>
				</Link>
				<Link id="148" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESCLI" fieldSource="DESCLI" wizardCaption="DESCLI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="CadSer.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="163" sourceType="DataField" name="s_CODCLI" source="CODCLI"/>
					</LinkParameters>
				</Link>
				<Link id="149" fieldSourceType="DBColumn" dataType="Text" html="False" name="CGCCPF" fieldSource="CGCCPF" wizardCaption="CGCCPF" wizardSize="14" wizardMaxLength="14" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="Extrato.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="164" sourceType="DataField" name="s_CODCLI" source="CODCLI"/>
					</LinkParameters>
				</Link>
				<Label id="150" fieldSourceType="DBColumn" dataType="Text" html="False" name="OPUS" fieldSource="OPUS" wizardCaption="OPUS" wizardSize="16" wizardMaxLength="16" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="151" size="10" type="Centered" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="of" wizardImagesScheme="Blueprint">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="153" conditionType="Parameter" useIsNull="False" field="CODCLI" dataType="Text" searchConditionType="Contains" parameterType="URL" logicOperator="Or" parameterSource="s_CODCLI" leftBrackets="0" rightBrackets="0"/>
				<TableParameter id="154" conditionType="Parameter" useIsNull="False" field="CGCCPF" dataType="Text" searchConditionType="Contains" parameterType="URL" logicOperator="Or" parameterSource="s_CGCCPF" leftBrackets="0" rightBrackets="0"/>
				<TableParameter id="155" conditionType="Parameter" useIsNull="False" field="DESCLI" dataType="Text" searchConditionType="Contains" parameterType="URL" logicOperator="Or" parameterSource="s_DESCLI" leftBrackets="0" rightBrackets="0"/>
				<TableParameter id="156" conditionType="Parameter" useIsNull="False" field="TUDO" dataType="Memo" searchConditionType="Contains" parameterType="URL" logicOperator="Or" expression="upper(CODCLI||DESCLI||DESFAN||CODUNI||CODDIS||CODSET||GRPATI||SUBATI||CODTIP||CGCCPF||LOGRAD||BAIRRO||CIDADE||ESTADO||CODPOS||TELEFO||TELFAX||CONTAT||LOGCOB||BAICOB||CIDCOB||ESTCOB||POSCOB||TELCOB||DATALT||PGTOELET||ESFERA||OPUS||EMAIL) like '%{s_GERAL}%'" leftBrackets="0" rightBrackets="0" parameterSource="s_GERAL"/>
			</TableParameters>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<SPParameters/>
			<SQLParameters>
				<SQLParameter id="159" parameterType="URL" variable="s_CODCLI" dataType="Text" parameterSource="s_CODCLI"/>
				<SQLParameter id="160" parameterType="URL" variable="s_CGCCPF" dataType="Text" parameterSource="s_CGCCPF"/>
				<SQLParameter id="161" parameterType="URL" variable="S_DESCLI" dataType="Text" parameterSource="S_DESCLI"/>
				<SQLParameter id="162" parameterType="URL" variable="s_GERAL" dataType="Memo" parameterSource="s_GERAL" designDefaultValue="33"/>
				<SQLParameter id="165" variable="s_OPUS" parameterType="URL" dataType="Text" parameterSource="s_OPUS"/>
			</SQLParameters>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="2" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="CadCadCli.php" forShow="True" url="CadCadCli.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="CadCadCli_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="133" groupID="1"/>
		<Group id="134" groupID="2"/>
		<Group id="135" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="136"/>
			</Actions>
		</Event>
	</Events>
</Page>
