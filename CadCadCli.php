<?php
//Include Common Files @1-9B72C052
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "CadCadCli.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @3-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordCADCLI_GRPATI_SUBATI_TABD { //CADCLI_GRPATI_SUBATI_TABD Class @23-4F1C41B6

//Variables @23-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @23-82A8C5DE
    function clsRecordCADCLI_GRPATI_SUBATI_TABD($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record CADCLI_GRPATI_SUBATI_TABD/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "CADCLI_GRPATI_SUBATI_TABD";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_CODCLI = new clsControl(ccsTextBox, "s_CODCLI", "s_CODCLI", ccsText, "", CCGetRequestParam("s_CODCLI", $Method, NULL), $this);
            $this->S_DESCLI = new clsControl(ccsTextBox, "S_DESCLI", "S_DESCLI", ccsText, "", CCGetRequestParam("S_DESCLI", $Method, NULL), $this);
            $this->s_CGCCPF = new clsControl(ccsTextBox, "s_CGCCPF", "s_CGCCPF", ccsText, "", CCGetRequestParam("s_CGCCPF", $Method, NULL), $this);
            $this->s_CPF = new clsControl(ccsTextBox, "s_CPF", "s_CPF", ccsText, "", CCGetRequestParam("s_CPF", $Method, NULL), $this);
            $this->s_OPUS = new clsControl(ccsTextBox, "s_OPUS", "s_OPUS", ccsText, "", CCGetRequestParam("s_OPUS", $Method, NULL), $this);
            $this->s_GERAL = new clsControl(ccsTextBox, "s_GERAL", "s_GERAL", ccsText, "", CCGetRequestParam("s_GERAL", $Method, NULL), $this);
            $this->Link1 = new clsControl(ccsLink, "Link1", "Link1", ccsText, "", CCGetRequestParam("Link1", $Method, NULL), $this);
            $this->Link1->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
            $this->Link1->Page = "ManutCadCadCli1.php";
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
        }
    }
//End Class_Initialize Event

//Validate Method @23-E8435FFF
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_CODCLI->Validate() && $Validation);
        $Validation = ($this->S_DESCLI->Validate() && $Validation);
        $Validation = ($this->s_CGCCPF->Validate() && $Validation);
        $Validation = ($this->s_CPF->Validate() && $Validation);
        $Validation = ($this->s_OPUS->Validate() && $Validation);
        $Validation = ($this->s_GERAL->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->s_CODCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->S_DESCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_CGCCPF->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_CPF->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_OPUS->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_GERAL->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @23-5FC60842
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_CODCLI->Errors->Count());
        $errors = ($errors || $this->S_DESCLI->Errors->Count());
        $errors = ($errors || $this->s_CGCCPF->Errors->Count());
        $errors = ($errors || $this->s_CPF->Errors->Count());
        $errors = ($errors || $this->s_OPUS->Errors->Count());
        $errors = ($errors || $this->s_GERAL->Errors->Count());
        $errors = ($errors || $this->Link1->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @23-F14BAA7E
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "CadCadCli.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                $Redirect = "CadCadCli.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @23-02608769
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->s_CODCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->S_DESCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_CGCCPF->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_CPF->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_OPUS->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_GERAL->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Link1->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_CODCLI->Show();
        $this->S_DESCLI->Show();
        $this->s_CGCCPF->Show();
        $this->s_CPF->Show();
        $this->s_OPUS->Show();
        $this->s_GERAL->Show();
        $this->Link1->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End CADCLI_GRPATI_SUBATI_TABD Class @23-FCB6E20C

class clsGridcadcli { //cadcli class @142-6C515C0E

//Variables @142-F8CD3184

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_CODCLI;
    public $Sorter_DESCLI;
    public $Sorter_CGCCPF;
    public $Sorter_OPUS;
//End Variables

//Class_Initialize Event @142-F903D86B
    function clsGridcadcli($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "cadcli";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid cadcli";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clscadcliDataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 10;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("cadcliOrder", "");
        $this->SorterDirection = CCGetParam("cadcliDir", "");

        $this->CODCLI = new clsControl(ccsLink, "CODCLI", "CODCLI", ccsText, "", CCGetRequestParam("CODCLI", ccsGet, NULL), $this);
        $this->CODCLI->Page = "ManutCadCadCli1.php";
        $this->DESCLI = new clsControl(ccsLink, "DESCLI", "DESCLI", ccsText, "", CCGetRequestParam("DESCLI", ccsGet, NULL), $this);
        $this->DESCLI->Page = "CadSer.php";
        $this->CGCCPF = new clsControl(ccsLink, "CGCCPF", "CGCCPF", ccsText, "", CCGetRequestParam("CGCCPF", ccsGet, NULL), $this);
        $this->CGCCPF->Page = "Extrato.php";
        $this->OPUS = new clsControl(ccsLabel, "OPUS", "OPUS", ccsText, "", CCGetRequestParam("OPUS", ccsGet, NULL), $this);
        $this->Sorter_CODCLI = new clsSorter($this->ComponentName, "Sorter_CODCLI", $FileName, $this);
        $this->Sorter_DESCLI = new clsSorter($this->ComponentName, "Sorter_DESCLI", $FileName, $this);
        $this->Sorter_CGCCPF = new clsSorter($this->ComponentName, "Sorter_CGCCPF", $FileName, $this);
        $this->Sorter_OPUS = new clsSorter($this->ComponentName, "Sorter_OPUS", $FileName, $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpCentered, $this);
    }
//End Class_Initialize Event

//Initialize Method @142-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @142-6D65C586
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urls_CODCLI"] = CCGetFromGet("s_CODCLI", NULL);
        $this->DataSource->Parameters["urls_CGCCPF"] = CCGetFromGet("s_CGCCPF", NULL);
        $this->DataSource->Parameters["urlS_DESCLI"] = CCGetFromGet("S_DESCLI", NULL);
        $this->DataSource->Parameters["urls_GERAL"] = CCGetFromGet("s_GERAL", NULL);
        $this->DataSource->Parameters["urls_OPUS"] = CCGetFromGet("s_OPUS", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["CODCLI"] = $this->CODCLI->Visible;
            $this->ControlsVisible["DESCLI"] = $this->DESCLI->Visible;
            $this->ControlsVisible["CGCCPF"] = $this->CGCCPF->Visible;
            $this->ControlsVisible["OPUS"] = $this->OPUS->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->CODCLI->SetValue($this->DataSource->CODCLI->GetValue());
                $this->CODCLI->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->CODCLI->Parameters = CCAddParam($this->CODCLI->Parameters, "CODCLI", $this->DataSource->f("CODCLI"));
                $this->DESCLI->SetValue($this->DataSource->DESCLI->GetValue());
                $this->DESCLI->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->DESCLI->Parameters = CCAddParam($this->DESCLI->Parameters, "s_CODCLI", $this->DataSource->f("CODCLI"));
                $this->CGCCPF->SetValue($this->DataSource->CGCCPF->GetValue());
                $this->CGCCPF->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->CGCCPF->Parameters = CCAddParam($this->CGCCPF->Parameters, "s_CODCLI", $this->DataSource->f("CODCLI"));
                $this->OPUS->SetValue($this->DataSource->OPUS->GetValue());
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->CODCLI->Show();
                $this->DESCLI->Show();
                $this->CGCCPF->Show();
                $this->OPUS->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        $this->Sorter_CODCLI->Show();
        $this->Sorter_DESCLI->Show();
        $this->Sorter_CGCCPF->Show();
        $this->Sorter_OPUS->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @142-9D506686
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->CODCLI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESCLI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CGCCPF->Errors->ToString());
        $errors = ComposeStrings($errors, $this->OPUS->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End cadcli Class @142-FCB6E20C

class clscadcliDataSource extends clsDBFaturar {  //cadcliDataSource Class @142-5D4F2E82

//DataSource Variables @142-9BE7CF89
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $CODCLI;
    public $DESCLI;
    public $CGCCPF;
    public $OPUS;
//End DataSource Variables

//DataSourceClass_Initialize Event @142-EB48AC3F
    function clscadcliDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid cadcli";
        $this->Initialize();
        $this->CODCLI = new clsField("CODCLI", ccsText, "");
        $this->DESCLI = new clsField("DESCLI", ccsText, "");
        $this->CGCCPF = new clsField("CGCCPF", ccsText, "");
        $this->OPUS = new clsField("OPUS", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @142-DD0A8F69
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_CODCLI" => array("CODCLI", ""), 
            "Sorter_DESCLI" => array("DESCLI", ""), 
            "Sorter_CGCCPF" => array("CGCCPF", ""), 
            "Sorter_OPUS" => array("OPUS", "")));
    }
//End SetOrder Method

//Prepare Method @142-6FBA018C
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_CODCLI", ccsText, "", "", $this->Parameters["urls_CODCLI"], "", false);
        $this->wp->AddParameter("2", "urls_CGCCPF", ccsText, "", "", $this->Parameters["urls_CGCCPF"], "", false);
        $this->wp->AddParameter("3", "urlS_DESCLI", ccsText, "", "", $this->Parameters["urlS_DESCLI"], "", false);
        $this->wp->AddParameter("4", "urls_GERAL", ccsMemo, "", "", $this->Parameters["urls_GERAL"], "", false);
        $this->wp->AddParameter("5", "urls_OPUS", ccsText, "", "", $this->Parameters["urls_OPUS"], "", false);
    }
//End Prepare Method

//Open Method @142-77552D2A
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*) FROM (SELECT * \n" .
        "FROM (SELECT CODCLI, DESCLI, CGCCPF,OPUS, upper(CODCLI||DESCLI||DESFAN||CODUNI||CODDIS||CODSET||GRPATI||SUBATI||CODTIP||CGCCPF||LOGRAD||BAIRRO||CIDADE||ESTADO||CODPOS||TELEFO||TELFAX||CONTAT||LOGCOB||BAICOB||CIDCOB||ESTCOB||POSCOB||TELCOB||DATALT||PGTOELET||ESFERA||OPUS||EMAIL) tudo FROM CADCLI ) CADCLI \n" .
        "WHERE (CODCLI LIKE '%" . $this->SQLValue($this->wp->GetDBValue("1"), ccsText) . "%' and not trim('" . $this->SQLValue($this->wp->GetDBValue("1"), ccsText) . "') is null )\n" .
        "OR (CGCCPF LIKE '%" . $this->SQLValue($this->wp->GetDBValue("2"), ccsText) . "%' and not trim('" . $this->SQLValue($this->wp->GetDBValue("2"), ccsText) . "') is null )\n" .
        "OR (DESCLI LIKE '%" . $this->SQLValue($this->wp->GetDBValue("3"), ccsText) . "%' and not trim('" . $this->SQLValue($this->wp->GetDBValue("3"), ccsText) . "') is null )\n" .
        "OR (TUDO LIKE upper('%" . $this->SQLValue($this->wp->GetDBValue("4"), ccsMemo) . "%')  and not trim('" . $this->SQLValue($this->wp->GetDBValue("4"), ccsMemo) . "') is null )\n" .
        "OR (OPUS LIKE upper('%" . $this->SQLValue($this->wp->GetDBValue("5"), ccsText) . "%')  and not trim('" . $this->SQLValue($this->wp->GetDBValue("5"), ccsText) . "') is null )\n" .
        "OR (TUDO LIKE '%' and trim('" . $this->SQLValue($this->wp->GetDBValue("4"), ccsMemo) . "') is null and trim('" . $this->SQLValue($this->wp->GetDBValue("3"), ccsText) . "') is null and trim('" . $this->SQLValue($this->wp->GetDBValue("2"), ccsText) . "') is null and trim('" . $this->SQLValue($this->wp->GetDBValue("1"), ccsText) . "') is null and trim('" . $this->SQLValue($this->wp->GetDBValue("5"), ccsText) . "') is null and trim('" . $this->SQLValue($this->wp->GetDBValue("1"), ccsText) . "') is null )) cnt";
        $this->SQL = "SELECT * \n" .
        "FROM (SELECT CODCLI, DESCLI, CGCCPF,OPUS, upper(CODCLI||DESCLI||DESFAN||CODUNI||CODDIS||CODSET||GRPATI||SUBATI||CODTIP||CGCCPF||LOGRAD||BAIRRO||CIDADE||ESTADO||CODPOS||TELEFO||TELFAX||CONTAT||LOGCOB||BAICOB||CIDCOB||ESTCOB||POSCOB||TELCOB||DATALT||PGTOELET||ESFERA||OPUS||EMAIL) tudo FROM CADCLI ) CADCLI \n" .
        "WHERE (CODCLI LIKE '%" . $this->SQLValue($this->wp->GetDBValue("1"), ccsText) . "%' and not trim('" . $this->SQLValue($this->wp->GetDBValue("1"), ccsText) . "') is null )\n" .
        "OR (CGCCPF LIKE '%" . $this->SQLValue($this->wp->GetDBValue("2"), ccsText) . "%' and not trim('" . $this->SQLValue($this->wp->GetDBValue("2"), ccsText) . "') is null )\n" .
        "OR (DESCLI LIKE '%" . $this->SQLValue($this->wp->GetDBValue("3"), ccsText) . "%' and not trim('" . $this->SQLValue($this->wp->GetDBValue("3"), ccsText) . "') is null )\n" .
        "OR (TUDO LIKE upper('%" . $this->SQLValue($this->wp->GetDBValue("4"), ccsMemo) . "%')  and not trim('" . $this->SQLValue($this->wp->GetDBValue("4"), ccsMemo) . "') is null )\n" .
        "OR (OPUS LIKE upper('%" . $this->SQLValue($this->wp->GetDBValue("5"), ccsText) . "%')  and not trim('" . $this->SQLValue($this->wp->GetDBValue("5"), ccsText) . "') is null )\n" .
        "OR (TUDO LIKE '%' and trim('" . $this->SQLValue($this->wp->GetDBValue("4"), ccsMemo) . "') is null and trim('" . $this->SQLValue($this->wp->GetDBValue("3"), ccsText) . "') is null and trim('" . $this->SQLValue($this->wp->GetDBValue("2"), ccsText) . "') is null and trim('" . $this->SQLValue($this->wp->GetDBValue("1"), ccsText) . "') is null and trim('" . $this->SQLValue($this->wp->GetDBValue("5"), ccsText) . "') is null and trim('" . $this->SQLValue($this->wp->GetDBValue("1"), ccsText) . "') is null )";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @142-76BD12DF
    function SetValues()
    {
        $this->CODCLI->SetDBValue($this->f("CODCLI"));
        $this->DESCLI->SetDBValue($this->f("DESCLI"));
        $this->CGCCPF->SetDBValue($this->f("CGCCPF"));
        $this->OPUS->SetDBValue($this->f("OPUS"));
    }
//End SetValues Method

} //End cadcliDataSource Class @142-FCB6E20C

//Include Page implementation @2-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-7836CB5E
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "CadCadCli.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-946ECC7A
CCSecurityRedirect("1;2;3", "");
//End Authenticate User

//Include events file @1-6E80AB48
include("./CadCadCli_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-C493B202
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$CADCLI_GRPATI_SUBATI_TABD = new clsRecordCADCLI_GRPATI_SUBATI_TABD("", $MainPage);
$cadcli = new clsGridcadcli("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->CADCLI_GRPATI_SUBATI_TABD = & $CADCLI_GRPATI_SUBATI_TABD;
$MainPage->cadcli = & $cadcli;
$MainPage->rodape = & $rodape;
$cadcli->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-40F68F62
$cabec->Operations();
$CADCLI_GRPATI_SUBATI_TABD->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-6522BF5F
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($CADCLI_GRPATI_SUBATI_TABD);
    unset($cadcli);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-4FF3E157
$cabec->Show();
$CADCLI_GRPATI_SUBATI_TABD->Show();
$cadcli->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-2E91002E
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($CADCLI_GRPATI_SUBATI_TABD);
unset($cadcli);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
