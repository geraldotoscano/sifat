<?php
//Include Common Files @1-FC381C00
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "CadCadEmp.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsGridCADEMP { //CADEMP class @4-D7D31E2F

//Variables @4-932356CA

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_FANTAS;
    public $Sorter_RAZSOC;
    public $Sorter_LOGRAD;
    public $Sorter_DESBAI;
    public $Sorter_DESCID;
    public $Sorter_ESTADO;
    public $Sorter_CODPOS;
    public $Sorter_TELEFO;
    public $Sorter_COMPLE;
    public $Sorter_MENSAG;
    public $Sorter_FAX;
    public $Sorter1_EMAIL;
//End Variables

//Class_Initialize Event @4-403D3330
    function clsGridCADEMP($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "CADEMP";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid CADEMP";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clsCADEMPDataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 1;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("CADEMPOrder", "");
        $this->SorterDirection = CCGetParam("CADEMPDir", "");

        $this->FANTAS = new clsControl(ccsLink, "FANTAS", "FANTAS", ccsText, "", CCGetRequestParam("FANTAS", ccsGet, NULL), $this);
        $this->FANTAS->Page = "ManutCadEmp.php";
        $this->RAZSOC = new clsControl(ccsLabel, "RAZSOC", "RAZSOC", ccsText, "", CCGetRequestParam("RAZSOC", ccsGet, NULL), $this);
        $this->LOGRAD = new clsControl(ccsLabel, "LOGRAD", "LOGRAD", ccsText, "", CCGetRequestParam("LOGRAD", ccsGet, NULL), $this);
        $this->DESBAI = new clsControl(ccsLabel, "DESBAI", "DESBAI", ccsText, "", CCGetRequestParam("DESBAI", ccsGet, NULL), $this);
        $this->DESCID = new clsControl(ccsLabel, "DESCID", "DESCID", ccsText, "", CCGetRequestParam("DESCID", ccsGet, NULL), $this);
        $this->ESTADO = new clsControl(ccsLabel, "ESTADO", "ESTADO", ccsText, "", CCGetRequestParam("ESTADO", ccsGet, NULL), $this);
        $this->CODPOS = new clsControl(ccsLabel, "CODPOS", "CODPOS", ccsText, "", CCGetRequestParam("CODPOS", ccsGet, NULL), $this);
        $this->TELEFO = new clsControl(ccsLabel, "TELEFO", "TELEFO", ccsText, "", CCGetRequestParam("TELEFO", ccsGet, NULL), $this);
        $this->COMPLE = new clsControl(ccsLabel, "COMPLE", "COMPLE", ccsText, "", CCGetRequestParam("COMPLE", ccsGet, NULL), $this);
        $this->MENSAG = new clsControl(ccsLabel, "MENSAG", "MENSAG", ccsText, "", CCGetRequestParam("MENSAG", ccsGet, NULL), $this);
        $this->FAX = new clsControl(ccsLabel, "FAX", "FAX", ccsText, "", CCGetRequestParam("FAX", ccsGet, NULL), $this);
        $this->EMAIL = new clsControl(ccsLabel, "EMAIL", "EMAIL", ccsText, "", CCGetRequestParam("EMAIL", ccsGet, NULL), $this);
        $this->Sorter_FANTAS = new clsSorter($this->ComponentName, "Sorter_FANTAS", $FileName, $this);
        $this->Sorter_RAZSOC = new clsSorter($this->ComponentName, "Sorter_RAZSOC", $FileName, $this);
        $this->Sorter_LOGRAD = new clsSorter($this->ComponentName, "Sorter_LOGRAD", $FileName, $this);
        $this->Sorter_DESBAI = new clsSorter($this->ComponentName, "Sorter_DESBAI", $FileName, $this);
        $this->Sorter_DESCID = new clsSorter($this->ComponentName, "Sorter_DESCID", $FileName, $this);
        $this->Sorter_ESTADO = new clsSorter($this->ComponentName, "Sorter_ESTADO", $FileName, $this);
        $this->Sorter_CODPOS = new clsSorter($this->ComponentName, "Sorter_CODPOS", $FileName, $this);
        $this->Sorter_TELEFO = new clsSorter($this->ComponentName, "Sorter_TELEFO", $FileName, $this);
        $this->Sorter_COMPLE = new clsSorter($this->ComponentName, "Sorter_COMPLE", $FileName, $this);
        $this->Sorter_MENSAG = new clsSorter($this->ComponentName, "Sorter_MENSAG", $FileName, $this);
        $this->Sorter_FAX = new clsSorter($this->ComponentName, "Sorter_FAX", $FileName, $this);
        $this->Sorter1_EMAIL = new clsSorter($this->ComponentName, "Sorter1_EMAIL", $FileName, $this);
    }
//End Class_Initialize Event

//Initialize Method @4-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @4-A5547299
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;


        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["FANTAS"] = $this->FANTAS->Visible;
            $this->ControlsVisible["RAZSOC"] = $this->RAZSOC->Visible;
            $this->ControlsVisible["LOGRAD"] = $this->LOGRAD->Visible;
            $this->ControlsVisible["DESBAI"] = $this->DESBAI->Visible;
            $this->ControlsVisible["DESCID"] = $this->DESCID->Visible;
            $this->ControlsVisible["ESTADO"] = $this->ESTADO->Visible;
            $this->ControlsVisible["CODPOS"] = $this->CODPOS->Visible;
            $this->ControlsVisible["TELEFO"] = $this->TELEFO->Visible;
            $this->ControlsVisible["COMPLE"] = $this->COMPLE->Visible;
            $this->ControlsVisible["MENSAG"] = $this->MENSAG->Visible;
            $this->ControlsVisible["FAX"] = $this->FAX->Visible;
            $this->ControlsVisible["EMAIL"] = $this->EMAIL->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->FANTAS->SetValue($this->DataSource->FANTAS->GetValue());
                $this->FANTAS->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->FANTAS->Parameters = CCAddParam($this->FANTAS->Parameters, "FANTAS", $this->DataSource->f("FANTAS"));
                $this->RAZSOC->SetValue($this->DataSource->RAZSOC->GetValue());
                $this->LOGRAD->SetValue($this->DataSource->LOGRAD->GetValue());
                $this->DESBAI->SetValue($this->DataSource->DESBAI->GetValue());
                $this->DESCID->SetValue($this->DataSource->DESCID->GetValue());
                $this->ESTADO->SetValue($this->DataSource->ESTADO->GetValue());
                $this->CODPOS->SetValue($this->DataSource->CODPOS->GetValue());
                $this->TELEFO->SetValue($this->DataSource->TELEFO->GetValue());
                $this->COMPLE->SetValue($this->DataSource->COMPLE->GetValue());
                $this->MENSAG->SetValue($this->DataSource->MENSAG->GetValue());
                $this->FAX->SetValue($this->DataSource->FAX->GetValue());
                $this->EMAIL->SetValue($this->DataSource->EMAIL->GetValue());
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->FANTAS->Show();
                $this->RAZSOC->Show();
                $this->LOGRAD->Show();
                $this->DESBAI->Show();
                $this->DESCID->Show();
                $this->ESTADO->Show();
                $this->CODPOS->Show();
                $this->TELEFO->Show();
                $this->COMPLE->Show();
                $this->MENSAG->Show();
                $this->FAX->Show();
                $this->EMAIL->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Sorter_FANTAS->Show();
        $this->Sorter_RAZSOC->Show();
        $this->Sorter_LOGRAD->Show();
        $this->Sorter_DESBAI->Show();
        $this->Sorter_DESCID->Show();
        $this->Sorter_ESTADO->Show();
        $this->Sorter_CODPOS->Show();
        $this->Sorter_TELEFO->Show();
        $this->Sorter_COMPLE->Show();
        $this->Sorter_MENSAG->Show();
        $this->Sorter_FAX->Show();
        $this->Sorter1_EMAIL->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @4-439D3058
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->FANTAS->Errors->ToString());
        $errors = ComposeStrings($errors, $this->RAZSOC->Errors->ToString());
        $errors = ComposeStrings($errors, $this->LOGRAD->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESBAI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESCID->Errors->ToString());
        $errors = ComposeStrings($errors, $this->ESTADO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CODPOS->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TELEFO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->COMPLE->Errors->ToString());
        $errors = ComposeStrings($errors, $this->MENSAG->Errors->ToString());
        $errors = ComposeStrings($errors, $this->FAX->Errors->ToString());
        $errors = ComposeStrings($errors, $this->EMAIL->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End CADEMP Class @4-FCB6E20C

class clsCADEMPDataSource extends clsDBFaturar {  //CADEMPDataSource Class @4-15AED3F4

//DataSource Variables @4-AB9AA7F5
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $FANTAS;
    public $RAZSOC;
    public $LOGRAD;
    public $DESBAI;
    public $DESCID;
    public $ESTADO;
    public $CODPOS;
    public $TELEFO;
    public $COMPLE;
    public $MENSAG;
    public $FAX;
    public $EMAIL;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-829D0D88
    function clsCADEMPDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid CADEMP";
        $this->Initialize();
        $this->FANTAS = new clsField("FANTAS", ccsText, "");
        $this->RAZSOC = new clsField("RAZSOC", ccsText, "");
        $this->LOGRAD = new clsField("LOGRAD", ccsText, "");
        $this->DESBAI = new clsField("DESBAI", ccsText, "");
        $this->DESCID = new clsField("DESCID", ccsText, "");
        $this->ESTADO = new clsField("ESTADO", ccsText, "");
        $this->CODPOS = new clsField("CODPOS", ccsText, "");
        $this->TELEFO = new clsField("TELEFO", ccsText, "");
        $this->COMPLE = new clsField("COMPLE", ccsText, "");
        $this->MENSAG = new clsField("MENSAG", ccsText, "");
        $this->FAX = new clsField("FAX", ccsText, "");
        $this->EMAIL = new clsField("EMAIL", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @4-B6F2C47C
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_FANTAS" => array("FANTAS", ""), 
            "Sorter_RAZSOC" => array("RAZSOC", ""), 
            "Sorter_LOGRAD" => array("LOGRAD", ""), 
            "Sorter_DESBAI" => array("DESBAI", ""), 
            "Sorter_DESCID" => array("DESCID", ""), 
            "Sorter_ESTADO" => array("ESTADO", ""), 
            "Sorter_CODPOS" => array("CODPOS", ""), 
            "Sorter_TELEFO" => array("TELEFO", ""), 
            "Sorter_COMPLE" => array("COMPLE", ""), 
            "Sorter_MENSAG" => array("MENSAG", ""), 
            "Sorter_FAX" => array("FAX", ""), 
            "Sorter1_EMAIL" => array("EMAIL", "")));
    }
//End SetOrder Method

//Prepare Method @4-14D6CD9D
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
    }
//End Prepare Method

//Open Method @4-32ECC6C7
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM CADEMP";
        $this->SQL = "SELECT * \n\n" .
        "FROM CADEMP {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @4-CD8FE9C6
    function SetValues()
    {
        $this->FANTAS->SetDBValue($this->f("FANTAS"));
        $this->RAZSOC->SetDBValue($this->f("RAZSOC"));
        $this->LOGRAD->SetDBValue($this->f("LOGRAD"));
        $this->DESBAI->SetDBValue($this->f("DESBAI"));
        $this->DESCID->SetDBValue($this->f("DESCID"));
        $this->ESTADO->SetDBValue($this->f("ESTADO"));
        $this->CODPOS->SetDBValue($this->f("CODPOS"));
        $this->TELEFO->SetDBValue($this->f("TELEFO"));
        $this->COMPLE->SetDBValue($this->f("COMPLE"));
        $this->MENSAG->SetDBValue($this->f("MENSAG"));
        $this->FAX->SetDBValue($this->f("FAX"));
        $this->EMAIL->SetDBValue($this->f("EMAIL"));
    }
//End SetValues Method

} //End CADEMPDataSource Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-A37AE05B
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "CadCadEmp.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-946ECC7A
CCSecurityRedirect("1;2;3", "");
//End Authenticate User

//Include events file @1-55CC05F1
include("./CadCadEmp_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-FF93149E
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$CADEMP = new clsGridCADEMP("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->CADEMP = & $CADEMP;
$MainPage->rodape = & $rodape;
$CADEMP->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-30AB84C1
$cabec->Operations();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-D6368128
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($CADEMP);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-BFB904B5
$cabec->Show();
$CADEMP->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-D01D521E
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($CADEMP);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
