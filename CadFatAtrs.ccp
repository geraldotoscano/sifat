<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="11" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="cadcli_FATATRS" wizardCaption=" Cadcli FATATRS " wizardOrientation="Vertical" wizardFormMethod="post" returnPage="CadFatAtrs.ccp">
			<Components>
				<Label id="25" fieldSourceType="CodeExpression" dataType="Text" html="True" name="lblinformes">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="26" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<TextBox id="13" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_DESCLI" wizardCaption="DESCLI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="14" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_CODCLI" wizardCaption="CODCLI" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="15" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_CODFATATRS" wizardCaption="CODFATATRS" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<RadioButton id="49" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" html="True" returnValueType="Number" name="s_SITFATATRS" _valueOfList="t" _nameOfList="Todas" dataSource="t;Todas;p;Pagas;n;Não Pagas" defaultValue="t">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</RadioButton>
				<Button id="12" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="16" secured="False" sourceType="SQL" returnValueType="Number" defaultPageSize="10" connection="Faturar" dataSource="SELECT DESCLI, FATATRS.* 
FROM CADCLI,
FATATRS
WHERE ( (CADCLI.CODCLI = FATATRS.CODCLI) ) AND ( CADCLI.DESCLI LIKE '%{s_DESCLI}%'
AND FATATRS.CODCLI LIKE '%{s_CODCLI}%'
AND FATATRS.CODFATATRS LIKE '%{s_CODFATATRS}%'
AND (( FATATRS.VALPGT &gt; {Expr0}
AND ( 'p'='{s_SITFATATRS}' ) )
OR ( FATATRS.VALPGT = {Expr1}
AND ( 'n'='{s_SITFATATRS}' ) )
OR ( 't'='{s_SITFATATRS}' ) ) )
ORDER BY DESCLI" name="cadcli_FATATRS2" orderBy="DESCLI" pageSizeLimit="100" wizardCaption="List of Cadcli, FATATRS " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="Não encontrado." pasteAsReplace="pasteAsReplace" activeCollection="SQLParameters" parameterTypeListName="ParameterTypeList">
			<Components>
				<Sorter id="30" visible="True" name="Sorter_DESCLI" column="DESCLI" wizardCaption="DESCLI" wizardSortingType="SimpleDir" wizardControl="DESCLI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="31" visible="True" name="Sorter_CODFATATRS" column="CODFATATRS" wizardCaption="CODFATATRS" wizardSortingType="SimpleDir" wizardControl="CODFATATRS" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="32" visible="True" name="Sorter_DATVNC" column="DATVNC" wizardCaption="DATVNC" wizardSortingType="SimpleDir" wizardControl="DATVNC" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="33" visible="True" name="Sorter_VALFAT" column="VALFAT" wizardCaption="VALFAT" wizardSortingType="SimpleDir" wizardControl="VALFAT" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="34" visible="True" name="Sorter_RET_INSS" column="RET_INSS" wizardCaption="RET INSS" wizardSortingType="SimpleDir" wizardControl="RET_INSS" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="35" visible="True" name="Sorter_VALJUR" column="VALJUR" wizardCaption="VALJUR" wizardSortingType="SimpleDir" wizardControl="VALJUR" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Label id="36" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESCLI" fieldSource="DESCLI" wizardCaption="DESCLI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Link id="37" fieldSourceType="DBColumn" dataType="Text" html="False" name="CODFATATRS" fieldSource="CODFATATRS" wizardCaption="CODFATATRS" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="Extrato_fatatrs.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="47" sourceType="DataField" name="CODFATATRS" source="CODFATATRS"/>
					</LinkParameters>
				</Link>
				<Label id="38" fieldSourceType="DBColumn" dataType="Date" html="False" name="DATVNC" fieldSource="DATVNC" wizardCaption="DATVNC" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="39" fieldSourceType="DBColumn" dataType="Float" html="False" name="VALFAT" fieldSource="VALFAT" wizardCaption="VALFAT" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" format="R$ #.##0,0;;;;,;.;">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="40" fieldSourceType="DBColumn" dataType="Float" html="False" name="RET_INSS" fieldSource="RET_INSS" wizardCaption="RET INSS" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" format="R$ #.##0,0;;;;,;.;">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="41" fieldSourceType="DBColumn" dataType="Float" html="False" name="VALJUR" fieldSource="VALJUR" wizardCaption="VALJUR" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" format="R$ #.##0,0;;;;,;.;">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="44" fieldSourceType="DBColumn" dataType="Float" html="False" name="LBLDebitoAtual" format="R$ #0.##0,0;;;;,;.;">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="45"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="48" fieldSourceType="DBColumn" dataType="Float" html="False" name="LBLValpago" fieldSource="VALPGT" format="R$ #0.##0,0;;;;,;.;">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="42" size="10" type="Centered" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="of" wizardImagesScheme="Blueprint">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="27" conditionType="Parameter" useIsNull="False" field="cadcli.DESCLI" parameterSource="s_DESCLI" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="1"/>
				<TableParameter id="28" conditionType="Parameter" useIsNull="False" field="FATATRS.CODCLI" parameterSource="s_CODCLI" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="2"/>
				<TableParameter id="29" conditionType="Parameter" useIsNull="False" field="FATATRS.CODFATATRS" parameterSource="s_CODFATATRS" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="3"/>
				<TableParameter id="50" conditionType="Parameter" useIsNull="False" field="FATATRS.VALPGT" dataType="Float" searchConditionType="GreaterThan" parameterType="Expression" logicOperator="And" parameterSource="0" leftBrackets="1" rightBrackets="0"/>
				<TableParameter id="53" conditionType="Expression" useIsNull="False" searchConditionType="Equal" parameterType="URL" logicOperator="Or" expression="'p'='{s_SITFATATRS}'" leftBrackets="0" rightBrackets="1" field="p" dataType="Text" parameterSource="s_SITFATATRS"/>
				<TableParameter id="51" conditionType="Parameter" useIsNull="False" field="FATATRS.VALPGT" dataType="Float" searchConditionType="Equal" parameterType="Expression" logicOperator="And" leftBrackets="1" rightBrackets="0" parameterSource="0"/>
				<TableParameter id="52" conditionType="Expression" useIsNull="False" field="n" dataType="Text" searchConditionType="Equal" parameterType="URL" logicOperator="Or" expression="'n'='{s_SITFATATRS}'" rightBrackets="1" parameterSource="s_SITFATATRS"/>
				<TableParameter id="54" conditionType="Expression" useIsNull="False" searchConditionType="Equal" parameterType="URL" logicOperator="And" expression="'t'='{s_SITFATATRS}'"/>
			</TableParameters>
			<JoinTables>
			</JoinTables>
			<JoinLinks>
			</JoinLinks>
			<Fields>
			</Fields>
			<SPParameters/>
			<SQLParameters>
				<SQLParameter id="55" parameterType="URL" variable="s_DESCLI" dataType="Text" parameterSource="s_DESCLI"/>
				<SQLParameter id="56" parameterType="URL" variable="s_CODCLI" dataType="Text" parameterSource="s_CODCLI"/>
				<SQLParameter id="57" parameterType="URL" variable="s_CODFATATRS" dataType="Text" parameterSource="s_CODFATATRS"/>
				<SQLParameter id="58" parameterType="Expression" variable="Expr0" dataType="Float" parameterSource="0"/>
				<SQLParameter id="59" parameterType="Expression" variable="Expr1" dataType="Float" parameterSource="0"/>
				<SQLParameter id="60" variable="s_SITFATATRS" parameterType="URL" dataType="Text" parameterSource="s_SITFATATRS" defaultValue="'t'"/>
			</SQLParameters>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="4" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="CadFatAtrs.php" forShow="True" url="CadFatAtrs.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="CadFatAtrs_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="46"/>
			</Actions>
		</Event>
	</Events>
</Page>
