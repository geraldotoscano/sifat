<?php
//Include Common Files @1-532DD3E1
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "CadFatAtrs.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordcadcli_FATATRS { //cadcli_FATATRS Class @11-5E0D8A7A

//Variables @11-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @11-237CB98A
    function clsRecordcadcli_FATATRS($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record cadcli_FATATRS/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "cadcli_FATATRS";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->lblinformes = new clsControl(ccsLabel, "lblinformes", "lblinformes", ccsText, "", CCGetRequestParam("lblinformes", $Method, NULL), $this);
            $this->lblinformes->HTML = true;
            $this->s_DESCLI = new clsControl(ccsTextBox, "s_DESCLI", "s_DESCLI", ccsText, "", CCGetRequestParam("s_DESCLI", $Method, NULL), $this);
            $this->s_CODCLI = new clsControl(ccsTextBox, "s_CODCLI", "s_CODCLI", ccsText, "", CCGetRequestParam("s_CODCLI", $Method, NULL), $this);
            $this->s_CODFATATRS = new clsControl(ccsTextBox, "s_CODFATATRS", "s_CODFATATRS", ccsText, "", CCGetRequestParam("s_CODFATATRS", $Method, NULL), $this);
            $this->s_SITFATATRS = new clsControl(ccsRadioButton, "s_SITFATATRS", "s_SITFATATRS", ccsText, "", CCGetRequestParam("s_SITFATATRS", $Method, NULL), $this);
            $this->s_SITFATATRS->DSType = dsListOfValues;
            $this->s_SITFATATRS->Values = array(array("t", "Todas"), array("p", "Pagas"), array("n", "N�o Pagas"));
            $this->s_SITFATATRS->HTML = true;
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
            if(!$this->FormSubmitted) {
                if(!is_array($this->s_SITFATATRS->Value) && !strlen($this->s_SITFATATRS->Value) && $this->s_SITFATATRS->Value !== false)
                    $this->s_SITFATATRS->SetText(t);
            }
        }
    }
//End Class_Initialize Event

//Validate Method @11-22DAD144
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_DESCLI->Validate() && $Validation);
        $Validation = ($this->s_CODCLI->Validate() && $Validation);
        $Validation = ($this->s_CODFATATRS->Validate() && $Validation);
        $Validation = ($this->s_SITFATATRS->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->s_DESCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_CODCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_CODFATATRS->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_SITFATATRS->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @11-749BD7E4
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->lblinformes->Errors->Count());
        $errors = ($errors || $this->s_DESCLI->Errors->Count());
        $errors = ($errors || $this->s_CODCLI->Errors->Count());
        $errors = ($errors || $this->s_CODFATATRS->Errors->Count());
        $errors = ($errors || $this->s_SITFATATRS->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @11-8E8FF75F
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "CadFatAtrs.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                $Redirect = "CadFatAtrs.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @11-D0FE3E12
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->s_SITFATATRS->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->lblinformes->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_DESCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_CODCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_CODFATATRS->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_SITFATATRS->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->lblinformes->Show();
        $this->s_DESCLI->Show();
        $this->s_CODCLI->Show();
        $this->s_CODFATATRS->Show();
        $this->s_SITFATATRS->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End cadcli_FATATRS Class @11-FCB6E20C

class clsGridcadcli_FATATRS2 { //cadcli_FATATRS2 class @16-238BC9A0

//Variables @16-1C7B871D

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_DESCLI;
    public $Sorter_CODFATATRS;
    public $Sorter_DATVNC;
    public $Sorter_VALFAT;
    public $Sorter_RET_INSS;
    public $Sorter_VALJUR;
//End Variables

//Class_Initialize Event @16-ECF46C96
    function clsGridcadcli_FATATRS2($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "cadcli_FATATRS2";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid cadcli_FATATRS2";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clscadcli_FATATRS2DataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 10;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("cadcli_FATATRS2Order", "");
        $this->SorterDirection = CCGetParam("cadcli_FATATRS2Dir", "");

        $this->DESCLI = new clsControl(ccsLabel, "DESCLI", "DESCLI", ccsText, "", CCGetRequestParam("DESCLI", ccsGet, NULL), $this);
        $this->CODFATATRS = new clsControl(ccsLink, "CODFATATRS", "CODFATATRS", ccsText, "", CCGetRequestParam("CODFATATRS", ccsGet, NULL), $this);
        $this->CODFATATRS->Page = "Extrato_fatatrs.php";
        $this->DATVNC = new clsControl(ccsLabel, "DATVNC", "DATVNC", ccsDate, $DefaultDateFormat, CCGetRequestParam("DATVNC", ccsGet, NULL), $this);
        $this->VALFAT = new clsControl(ccsLabel, "VALFAT", "VALFAT", ccsFloat, array(True, 4, ",", ".", False, array("R\$ ", "#"), array("#", "#", "0", "0"), 1, True, ""), CCGetRequestParam("VALFAT", ccsGet, NULL), $this);
        $this->RET_INSS = new clsControl(ccsLabel, "RET_INSS", "RET_INSS", ccsFloat, array(True, 4, ",", ".", False, array("R\$ ", "#"), array("#", "#", "0", "0"), 1, True, ""), CCGetRequestParam("RET_INSS", ccsGet, NULL), $this);
        $this->VALJUR = new clsControl(ccsLabel, "VALJUR", "VALJUR", ccsFloat, array(True, 4, ",", ".", False, array("R\$ ", "#"), array("#", "#", "0", "0"), 1, True, ""), CCGetRequestParam("VALJUR", ccsGet, NULL), $this);
        $this->LBLDebitoAtual = new clsControl(ccsLabel, "LBLDebitoAtual", "LBLDebitoAtual", ccsFloat, array(True, 4, ",", ".", False, array("R\$ ", "#", "0"), array("#", "#", "0", "0"), 1, True, ""), CCGetRequestParam("LBLDebitoAtual", ccsGet, NULL), $this);
        $this->LBLValpago = new clsControl(ccsLabel, "LBLValpago", "LBLValpago", ccsFloat, array(True, 4, ",", ".", False, array("R\$ ", "#", "0"), array("#", "#", "0", "0"), 1, True, ""), CCGetRequestParam("LBLValpago", ccsGet, NULL), $this);
        $this->Sorter_DESCLI = new clsSorter($this->ComponentName, "Sorter_DESCLI", $FileName, $this);
        $this->Sorter_CODFATATRS = new clsSorter($this->ComponentName, "Sorter_CODFATATRS", $FileName, $this);
        $this->Sorter_DATVNC = new clsSorter($this->ComponentName, "Sorter_DATVNC", $FileName, $this);
        $this->Sorter_VALFAT = new clsSorter($this->ComponentName, "Sorter_VALFAT", $FileName, $this);
        $this->Sorter_RET_INSS = new clsSorter($this->ComponentName, "Sorter_RET_INSS", $FileName, $this);
        $this->Sorter_VALJUR = new clsSorter($this->ComponentName, "Sorter_VALJUR", $FileName, $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpCentered, $this);
    }
//End Class_Initialize Event

//Initialize Method @16-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @16-7175BF10
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urls_DESCLI"] = CCGetFromGet("s_DESCLI", NULL);
        $this->DataSource->Parameters["urls_CODCLI"] = CCGetFromGet("s_CODCLI", NULL);
        $this->DataSource->Parameters["urls_CODFATATRS"] = CCGetFromGet("s_CODFATATRS", NULL);
        $this->DataSource->Parameters["expr58"] = 0;
        $this->DataSource->Parameters["expr59"] = 0;
        $this->DataSource->Parameters["urls_SITFATATRS"] = CCGetFromGet("s_SITFATATRS", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["DESCLI"] = $this->DESCLI->Visible;
            $this->ControlsVisible["CODFATATRS"] = $this->CODFATATRS->Visible;
            $this->ControlsVisible["DATVNC"] = $this->DATVNC->Visible;
            $this->ControlsVisible["VALFAT"] = $this->VALFAT->Visible;
            $this->ControlsVisible["RET_INSS"] = $this->RET_INSS->Visible;
            $this->ControlsVisible["VALJUR"] = $this->VALJUR->Visible;
            $this->ControlsVisible["LBLDebitoAtual"] = $this->LBLDebitoAtual->Visible;
            $this->ControlsVisible["LBLValpago"] = $this->LBLValpago->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->DESCLI->SetValue($this->DataSource->DESCLI->GetValue());
                $this->CODFATATRS->SetValue($this->DataSource->CODFATATRS->GetValue());
                $this->CODFATATRS->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->CODFATATRS->Parameters = CCAddParam($this->CODFATATRS->Parameters, "CODFATATRS", $this->DataSource->f("CODFATATRS"));
                $this->DATVNC->SetValue($this->DataSource->DATVNC->GetValue());
                $this->VALFAT->SetValue($this->DataSource->VALFAT->GetValue());
                $this->RET_INSS->SetValue($this->DataSource->RET_INSS->GetValue());
                $this->VALJUR->SetValue($this->DataSource->VALJUR->GetValue());
                $this->LBLValpago->SetValue($this->DataSource->LBLValpago->GetValue());
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->DESCLI->Show();
                $this->CODFATATRS->Show();
                $this->DATVNC->Show();
                $this->VALFAT->Show();
                $this->RET_INSS->Show();
                $this->VALJUR->Show();
                $this->LBLDebitoAtual->Show();
                $this->LBLValpago->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        $this->Sorter_DESCLI->Show();
        $this->Sorter_CODFATATRS->Show();
        $this->Sorter_DATVNC->Show();
        $this->Sorter_VALFAT->Show();
        $this->Sorter_RET_INSS->Show();
        $this->Sorter_VALJUR->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @16-47B29965
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->DESCLI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CODFATATRS->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DATVNC->Errors->ToString());
        $errors = ComposeStrings($errors, $this->VALFAT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->RET_INSS->Errors->ToString());
        $errors = ComposeStrings($errors, $this->VALJUR->Errors->ToString());
        $errors = ComposeStrings($errors, $this->LBLDebitoAtual->Errors->ToString());
        $errors = ComposeStrings($errors, $this->LBLValpago->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End cadcli_FATATRS2 Class @16-FCB6E20C

class clscadcli_FATATRS2DataSource extends clsDBFaturar {  //cadcli_FATATRS2DataSource Class @16-6422E0F7

//DataSource Variables @16-304119C0
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $DESCLI;
    public $CODFATATRS;
    public $DATVNC;
    public $VALFAT;
    public $RET_INSS;
    public $VALJUR;
    public $LBLValpago;
//End DataSource Variables

//DataSourceClass_Initialize Event @16-F02F5ABE
    function clscadcli_FATATRS2DataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid cadcli_FATATRS2";
        $this->Initialize();
        $this->DESCLI = new clsField("DESCLI", ccsText, "");
        $this->CODFATATRS = new clsField("CODFATATRS", ccsText, "");
        $this->DATVNC = new clsField("DATVNC", ccsDate, $this->DateFormat);
        $this->VALFAT = new clsField("VALFAT", ccsFloat, "");
        $this->RET_INSS = new clsField("RET_INSS", ccsFloat, "");
        $this->VALJUR = new clsField("VALJUR", ccsFloat, "");
        $this->LBLValpago = new clsField("LBLValpago", ccsFloat, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @16-F2A6C91C
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "DESCLI";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_DESCLI" => array("DESCLI", ""), 
            "Sorter_CODFATATRS" => array("CODFATATRS", ""), 
            "Sorter_DATVNC" => array("DATVNC", ""), 
            "Sorter_VALFAT" => array("VALFAT", ""), 
            "Sorter_RET_INSS" => array("RET_INSS", ""), 
            "Sorter_VALJUR" => array("VALJUR", "")));
    }
//End SetOrder Method

//Prepare Method @16-4099A21C
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_DESCLI", ccsText, "", "", $this->Parameters["urls_DESCLI"], "", false);
        $this->wp->AddParameter("2", "urls_CODCLI", ccsText, "", "", $this->Parameters["urls_CODCLI"], "", false);
        $this->wp->AddParameter("3", "urls_CODFATATRS", ccsText, "", "", $this->Parameters["urls_CODFATATRS"], "", false);
        $this->wp->AddParameter("4", "expr58", ccsFloat, "", "", $this->Parameters["expr58"], "", false);
        $this->wp->AddParameter("5", "expr59", ccsFloat, "", "", $this->Parameters["expr59"], "", false);
        $this->wp->AddParameter("6", "urls_SITFATATRS", ccsText, "", "", $this->Parameters["urls_SITFATATRS"], 't', false);
    }
//End Prepare Method

//Open Method @16-F9CCDB2C
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*) FROM (SELECT DESCLI, FATATRS.* \n" .
        "FROM CADCLI,\n" .
        "FATATRS\n" .
        "WHERE ( (CADCLI.CODCLI = FATATRS.CODCLI) ) AND ( CADCLI.DESCLI LIKE '%" . $this->SQLValue($this->wp->GetDBValue("1"), ccsText) . "%'\n" .
        "AND FATATRS.CODCLI LIKE '%" . $this->SQLValue($this->wp->GetDBValue("2"), ccsText) . "%'\n" .
        "AND FATATRS.CODFATATRS LIKE '%" . $this->SQLValue($this->wp->GetDBValue("3"), ccsText) . "%'\n" .
        "AND (( FATATRS.VALPGT > " . $this->SQLValue($this->wp->GetDBValue("4"), ccsFloat) . "\n" .
        "AND ( 'p'='" . $this->SQLValue($this->wp->GetDBValue("6"), ccsText) . "' ) )\n" .
        "OR ( FATATRS.VALPGT = " . $this->SQLValue($this->wp->GetDBValue("5"), ccsFloat) . "\n" .
        "AND ( 'n'='" . $this->SQLValue($this->wp->GetDBValue("6"), ccsText) . "' ) )\n" .
        "OR ( 't'='" . $this->SQLValue($this->wp->GetDBValue("6"), ccsText) . "' ) ) )) cnt";
        $this->SQL = "SELECT DESCLI, FATATRS.* \n" .
        "FROM CADCLI,\n" .
        "FATATRS\n" .
        "WHERE ( (CADCLI.CODCLI = FATATRS.CODCLI) ) AND ( CADCLI.DESCLI LIKE '%" . $this->SQLValue($this->wp->GetDBValue("1"), ccsText) . "%'\n" .
        "AND FATATRS.CODCLI LIKE '%" . $this->SQLValue($this->wp->GetDBValue("2"), ccsText) . "%'\n" .
        "AND FATATRS.CODFATATRS LIKE '%" . $this->SQLValue($this->wp->GetDBValue("3"), ccsText) . "%'\n" .
        "AND (( FATATRS.VALPGT > " . $this->SQLValue($this->wp->GetDBValue("4"), ccsFloat) . "\n" .
        "AND ( 'p'='" . $this->SQLValue($this->wp->GetDBValue("6"), ccsText) . "' ) )\n" .
        "OR ( FATATRS.VALPGT = " . $this->SQLValue($this->wp->GetDBValue("5"), ccsFloat) . "\n" .
        "AND ( 'n'='" . $this->SQLValue($this->wp->GetDBValue("6"), ccsText) . "' ) )\n" .
        "OR ( 't'='" . $this->SQLValue($this->wp->GetDBValue("6"), ccsText) . "' ) ) ){SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @16-45AAB993
    function SetValues()
    {
        $this->DESCLI->SetDBValue($this->f("DESCLI"));
        $this->CODFATATRS->SetDBValue($this->f("CODFATATRS"));
        $this->DATVNC->SetDBValue(trim($this->f("DATVNC")));
        $this->VALFAT->SetDBValue(trim($this->f("VALFAT")));
        $this->RET_INSS->SetDBValue(trim($this->f("RET_INSS")));
        $this->VALJUR->SetDBValue(trim($this->f("VALJUR")));
        $this->LBLValpago->SetDBValue(trim($this->f("VALPGT")));
    }
//End SetValues Method

} //End cadcli_FATATRS2DataSource Class @16-FCB6E20C

//Include Page implementation @4-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-03EE50F0
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "CadFatAtrs.html";
$BlockToParse = "main";
$TemplateEncoding = "ISO-8859-1";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-872FD3D7
CCSecurityRedirect("", "");
//End Authenticate User

//Include events file @1-6B472C7E
include("./CadFatAtrs_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-A2471434
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$cadcli_FATATRS = new clsRecordcadcli_FATATRS("", $MainPage);
$cadcli_FATATRS2 = new clsGridcadcli_FATATRS2("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->cadcli_FATATRS = & $cadcli_FATATRS;
$MainPage->cadcli_FATATRS2 = & $cadcli_FATATRS2;
$MainPage->rodape = & $rodape;
$cadcli_FATATRS2->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "iso-8859-1";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-A8C4B9DA
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "ISO-8859-1");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-472085C9
$cabec->Operations();
$cadcli_FATATRS->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-6A5615A0
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($cadcli_FATATRS);
    unset($cadcli_FATATRS2);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-1F74919D
$cabec->Show();
$cadcli_FATATRS->Show();
$cadcli_FATATRS2->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-99FF5F0B
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($cadcli_FATATRS);
unset($cadcli_FATATRS2);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
