<?php
//BindEvents Method @1-FC9E4287
function BindEvents()
{
    global $cadcli_FATATRS;
    global $cadcli_FATATRS2;
    global $CCSEvents;
    $cadcli_FATATRS->lblinformes->CCSEvents["BeforeShow"] = "cadcli_FATATRS_lblinformes_BeforeShow";
    $cadcli_FATATRS2->LBLDebitoAtual->CCSEvents["BeforeShow"] = "cadcli_FATATRS2_LBLDebitoAtual_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//cadcli_FATATRS_lblinformes_BeforeShow @25-A86B3AFD
function cadcli_FATATRS_lblinformes_BeforeShow(& $sender)
{
    $cadcli_FATATRS_lblinformes_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $cadcli_FATATRS; //Compatibility
//End cadcli_FATATRS_lblinformes_BeforeShow

//Custom Code @26-2A29BDB7
// -------------------------

    $db = new clsDBfaturar();

    $erro_validando=false;
	$db->query("select fac.codfat, fac.codfatatrs from fatatrs_cadfat fac,cadfat f, fatatrs fa where fa.codfatatrs= fac.codfatatrs and fac.codfat= f.codfat and f.valpgt>0 and ( fa.valpgt=0 or fa.valpgt is null) order by fac.codfatatrs");
	$faturas_pagas="";
	$ultcodfatatrs="";
	if($db->next_record())
	  {
       $erro_validando=true;
	   $faturas_pagas=$db->f("codfatatrs")."(".$db->f("codfat");
	   $ultcodfatatrs=$db->f("codfatatrs");
	  }

	while($db->next_record())
	  {
	   if($ultcodfatatrs==$db->f("codfatatrs"))
	     {
          $faturas_pagas=$faturas_pagas.", ".$db->f("codfat");
	     }
        else
		 {
 	      $faturas_pagas=$faturas_pagas.") ".$db->f("codfatatrs")."(".$db->f("codfat");
	      $ultcodfatatrs=$db->f("codfatatrs");
		 }
	  }

    if($faturas_pagas!=0)
	  {
	    $faturas_pagas=$faturas_pagas.")";
        $faturas_pagas="\n<br>As seguintes faturas foram pagas separadamente cobran�a(faturas,...): ".$faturas_pagas;
	  }

	$db->query("select fac.codfat from fatatrs_cadfat fac, cadfat_canc f, fatatrs fa where fa.codfatatrs= fac.codfatatrs and fac.codfat= f.codfat and  ( fa.valpgt=0 or fa.valpgt is null)");
	$faturas_canceladas="";
	if($db->next_record())
	  {
       $erro_validando=true;
	   $faturas_canceladas=$db->f("codfat");
	  }

	while($db->next_record())
	  {
       $faturas_canceladas=$faturas_canceladas.", ".$db->f("codfat");
	  }
    if($faturas_canceladas!="")
      $faturas_canceladas="\n<br>As seguintes faturas canceladas ".$faturas_canceladas;
    if($erro_validando)
	  {
       $NRRetFatPag->lblinformes->SetValue("N�o ser� poss�vel realizar o recebimento das faturas em atraso devido:\n<br>".$faturas_pagas.$faturas_canceladas);
	   $NRRetFatPag->Button1->Visible=false;

	  }



// -------------------------
//End Custom Code

//Close cadcli_FATATRS_lblinformes_BeforeShow @25-8306843A
    return $cadcli_FATATRS_lblinformes_BeforeShow;
}
//End Close cadcli_FATATRS_lblinformes_BeforeShow

//cadcli_FATATRS2_LBLDebitoAtual_BeforeShow @44-D202F3B5
function cadcli_FATATRS2_LBLDebitoAtual_BeforeShow(& $sender)
{
    $cadcli_FATATRS2_LBLDebitoAtual_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $cadcli_FATATRS2; //Compatibility
//End cadcli_FATATRS2_LBLDebitoAtual_BeforeShow

//Custom Code @45-2A29BDB7
// -------------------------


	global $mTotGer;
	
	$mFat = str_replace(",", ";", $cadcli_FATATRS2->VALFAT->GetValue()."");
	$mFat = str_replace(".", "", $mFat);
	$mFat = str_replace(";", ".", $mFat);

	$mFat = (float)$cadcli_FATATRS2->VALFAT->GetValue();


	$mJur = str_replace(",", ";", $cadcli_FATATRS2->VALJUR->GetValue()."");
	$mJur = str_replace(".", "", $mJur);
	$mJur = str_replace(";", ".", $mJur);

	$mJur = (float)$cadcli_FATATRS2->VALJUR->GetValue();

	$mSS = str_replace(",", ";", $cadcli_FATATRS2->RET_INSS->GetValue()."");
	$mSS = str_replace(".", "", $mSS);
	$mSS = str_replace(";", ".", $mSS);

	$mSS = (float)$cadcli_FATATRS2->RET_INSS->GetValue();

/*
	$mValFat = ((float)$mFat)/100;
	$mValJur = ((float)$mJur)/100;
	$mINSS =   ((float)$mSS)/100;
*/

	$mValFat = $mFat;
	$mValJur = $mJur;
	$mINSS =   $mSS;


    $sender->SetValue($mValFat+$mValJur-($mINSS));


// -------------------------
//End Custom Code

//Close cadcli_FATATRS2_LBLDebitoAtual_BeforeShow @44-9C8310C7
    return $cadcli_FATATRS2_LBLDebitoAtual_BeforeShow;
}
//End Close cadcli_FATATRS2_LBLDebitoAtual_BeforeShow

//Page_BeforeShow @1-A63BDBD4
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CadFatAtrs; //Compatibility
//End Page_BeforeShow

//Custom Code @46-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(55);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
