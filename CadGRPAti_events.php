<?php
//BindEvents Method @1-080DCB20
function BindEvents()
{
    global $GRPATISearch;
    global $CCSEvents;
    $GRPATISearch->s_GRPATI->ds->CCSEvents["BeforeBuildSelect"] = "GRPATISearch_s_GRPATI_ds_BeforeBuildSelect";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//GRPATISearch_s_GRPATI_ds_BeforeBuildSelect @7-2BD99F12
function GRPATISearch_s_GRPATI_ds_BeforeBuildSelect(& $sender)
{
    $GRPATISearch_s_GRPATI_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GRPATISearch; //Compatibility
//End GRPATISearch_s_GRPATI_ds_BeforeBuildSelect

//Custom Code @25-2A29BDB7
// -------------------------
  If ($GRPATISearch->s_GRPATI->DataSource->Order == "") { 
     $GRPATISearch->s_GRPATI->DataSource->Order = "DESATI";
  }
// -------------------------
//End Custom Code

//Close GRPATISearch_s_GRPATI_ds_BeforeBuildSelect @7-F7A35B9D
    return $GRPATISearch_s_GRPATI_ds_BeforeBuildSelect;
}
//End Close GRPATISearch_s_GRPATI_ds_BeforeBuildSelect

//Page_BeforeShow @1-7A3D86DD
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CadGRPAti; //Compatibility
//End Page_BeforeShow

//Custom Code @26-2A29BDB7
// -------------------------

        include("controle_acesso.php");
        $Tabela = new clsDBfaturar();
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(7,6);
		controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
