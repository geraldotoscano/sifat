<?php
//BindEvents Method @1-940E5503
function BindEvents()
{
    global $GRPSERSearch;
    global $CCSEvents;
    $GRPSERSearch->s_GRPSER->ds->CCSEvents["BeforeBuildSelect"] = "GRPSERSearch_s_GRPSER_ds_BeforeBuildSelect";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//GRPSERSearch_s_GRPSER_ds_BeforeBuildSelect @7-92CF8756
function GRPSERSearch_s_GRPSER_ds_BeforeBuildSelect(& $sender)
{
    $GRPSERSearch_s_GRPSER_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GRPSERSearch; //Compatibility
//End GRPSERSearch_s_GRPSER_ds_BeforeBuildSelect

//Custom Code @24-2A29BDB7
// -------------------------
    if ($GRPSERSearch->s_GRPSER->DataSource->Order == "")
	{
		$GRPSERSearch->s_GRPSER->DataSource->Order = "DESSER";
	}
// -------------------------
//End Custom Code

//Close GRPSERSearch_s_GRPSER_ds_BeforeBuildSelect @7-B3A57C85
    return $GRPSERSearch_s_GRPSER_ds_BeforeBuildSelect;
}
//End Close GRPSERSearch_s_GRPSER_ds_BeforeBuildSelect

//Page_BeforeShow @1-F3D5A96B
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CadGrpSer; //Compatibility
//End Page_BeforeShow

//Custom Code @25-2A29BDB7
// -------------------------
/*
  O usuario pode ter permiss�o para somente cadastrar servi�os por isto esta autenticacao � ligeiramente diferente

*/

        include("controle_acesso.php");
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(27,28);
		$permissao_requerida_extra=array(29);
        controleacesso_extra($perfil,$permissao_requerida,"acessonegado.php",$permissao_requerida_extra,"ManutGrpSer.php");




// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
