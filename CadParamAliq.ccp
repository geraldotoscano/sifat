<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="5" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="PARAM_ALIQUOTASearch" wizardCaption=" PARAM ALIQUOTA " wizardOrientation="Vertical" wizardFormMethod="post" returnPage="CadParamAliq.ccp">
			<Components>
				<TextBox id="7" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="s_VIGENCIA_INSS" wizardCaption="VIGENCIA INSS" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="8" name="DatePicker_s_VIGENCIA_INSS" control="s_VIGENCIA_INSS" wizardSatellite="True" wizardControl="s_VIGENCIA_INSS" wizardDatePickerType="Image" wizardPicture="Styles/Blueprint/Images/DatePicker.gif" style="Styles/Blueprint/Style.css">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
				<Link id="23" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link1" hrefSource="ManutParamAliq.ccp" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<LinkParameters/>
					<Attributes/>
					<Features/>
				</Link>
				<Button id="6" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="4" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="Faturar" dataSource="PARAM_ALIQUOTA_INSS" name="PARAM_ALIQUOTA" pageSizeLimit="100" wizardCaption="List of PARAM ALIQUOTA " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="Nenhuma alíquota foi encontrada para esta vigência." wizardAllowSorting="True" pasteAsReplace="pasteAsReplace">
			<Components>
				<Sorter id="13" visible="True" name="Sorter_TAXA_INSS" column="TAXA_INSS" wizardCaption="TAXA INSS" wizardSortingType="SimpleDir" wizardControl="TAXA_INSS" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="30" visible="True" name="Sorter_INSS_MIM" wizardSortingType="SimpleDir" wizardCaption="INSS Mínimo" column="INSS_MIM">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="15" visible="True" name="Sorter_VIGENCIA_INSS" column="VIGENCIA_INSS" wizardCaption="VIGENCIA INSS" wizardSortingType="SimpleDir" wizardControl="VIGENCIA_INSS" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Label id="17" fieldSourceType="DBColumn" dataType="Float" html="False" name="TAXA_INSS" fieldSource="TAXA_INSS" wizardCaption="TAXA INSS" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="ManutParamAliq.ccp">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="32"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="21" sourceType="DataField" name="TAXA_INSS" source="TAXA_INSS"/>
					</LinkParameters>
				</Label>
				<Label id="31" fieldSourceType="DBColumn" dataType="Float" html="False" name="INSS_MIM" fieldSource="INSS_MIM">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="33"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="19" fieldSourceType="DBColumn" dataType="Date" html="False" name="VIGENCIA_INSS" fieldSource="VIGENCIA_INSS" wizardCaption="VIGENCIA INSS" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" format="dd/mm/yyyy">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="11" conditionType="Parameter" useIsNull="False" field="VIGENCIA_INSS" parameterSource="s_VIGENCIA_INSS" dataType="Date" logicOperator="And" searchConditionType="Equal" parameterType="URL" orderNumber="1"/>
				<TableParameter id="12" conditionType="Parameter" useIsNull="False" field="VIGENCIA_ISSQN" parameterSource="s_VIGENCIA_ISSQN" dataType="Date" logicOperator="And" searchConditionType="Equal" parameterType="URL" orderNumber="2"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="27" tableName="PARAM_ALIQUOTA_INSS" posLeft="10" posTop="10" posWidth="141" posHeight="88"/>
			</JoinTables>
			<JoinLinks/>
			<Fields/>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="CadParamAliq.php" forShow="True" url="CadParamAliq.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="CadParamAliq_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="24" groupID="1"/>
		<Group id="25" groupID="2"/>
		<Group id="26" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="34"/>
			</Actions>
		</Event>
	</Events>
</Page>
