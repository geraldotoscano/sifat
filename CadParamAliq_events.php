<?php
//BindEvents Method @1-7749A9E2
function BindEvents()
{
    global $PARAM_ALIQUOTA;
    global $CCSEvents;
    $PARAM_ALIQUOTA->TAXA_INSS->CCSEvents["BeforeShow"] = "PARAM_ALIQUOTA_TAXA_INSS_BeforeShow";
    $PARAM_ALIQUOTA->INSS_MIM->CCSEvents["BeforeShow"] = "PARAM_ALIQUOTA_INSS_MIM_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//PARAM_ALIQUOTA_TAXA_INSS_BeforeShow @17-4343B818
function PARAM_ALIQUOTA_TAXA_INSS_BeforeShow(& $sender)
{
    $PARAM_ALIQUOTA_TAXA_INSS_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $PARAM_ALIQUOTA; //Compatibility
//End PARAM_ALIQUOTA_TAXA_INSS_BeforeShow

//Custom Code @32-2A29BDB7
// -------------------------
	$mISSQN = $PARAM_ALIQUOTA->TAXA_INSS->GetValue();
	$mISSQN = number_format($mISSQN, 2,',','.');
	$PARAM_ALIQUOTA->TAXA_INSS->SetValue($mISSQN);
// -------------------------
//End Custom Code

//Close PARAM_ALIQUOTA_TAXA_INSS_BeforeShow @17-452DCB1C
    return $PARAM_ALIQUOTA_TAXA_INSS_BeforeShow;
}
//End Close PARAM_ALIQUOTA_TAXA_INSS_BeforeShow

//PARAM_ALIQUOTA_INSS_MIM_BeforeShow @31-75049ACA
function PARAM_ALIQUOTA_INSS_MIM_BeforeShow(& $sender)
{
    $PARAM_ALIQUOTA_INSS_MIM_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $PARAM_ALIQUOTA; //Compatibility
//End PARAM_ALIQUOTA_INSS_MIM_BeforeShow

//Custom Code @33-2A29BDB7
// -------------------------
	$mISSQN = $PARAM_ALIQUOTA->INSS_MIM->GetValue();
	$mISSQN = number_format($mISSQN, 2,',','.');
	$PARAM_ALIQUOTA->INSS_MIM->SetValue($mISSQN);
// -------------------------
//End Custom Code

//Close PARAM_ALIQUOTA_INSS_MIM_BeforeShow @31-EC74748B
    return $PARAM_ALIQUOTA_INSS_MIM_BeforeShow;
}
//End Close PARAM_ALIQUOTA_INSS_MIM_BeforeShow

//Page_BeforeShow @1-1DD8E806
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CadParamAliq; //Compatibility
//End Page_BeforeShow

//Custom Code @34-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(47);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
