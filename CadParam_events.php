<?php
//BindEvents Method @1-9ABEB8FE
function BindEvents()
{
    global $PARAM;
    global $CCSEvents;
    $PARAM->TAXA_JUROS->CCSEvents["BeforeShow"] = "PARAM_TAXA_JUROS_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//PARAM_TAXA_JUROS_BeforeShow @6-59DEF5F9
function PARAM_TAXA_JUROS_BeforeShow(& $sender)
{
    $PARAM_TAXA_JUROS_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $PARAM; //Compatibility
//End PARAM_TAXA_JUROS_BeforeShow

//Custom Code @14-2A29BDB7
// -------------------------
	$mISSQN = $PARAM->TAXA_JUROS->GetValue();
	$mISSQN = number_format($mISSQN, 2,',','.');
	$PARAM->TAXA_JUROS->SetValue($mISSQN);
// -------------------------
//End Custom Code

//Close PARAM_TAXA_JUROS_BeforeShow @6-6F1A936F
    return $PARAM_TAXA_JUROS_BeforeShow;
}
//End Close PARAM_TAXA_JUROS_BeforeShow

//Page_BeforeShow @1-F028D446
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CadParam; //Compatibility
//End Page_BeforeShow

//Custom Code @15-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(46);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
