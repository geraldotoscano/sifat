<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="68" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="cadcli_MOVSER_SUBSER" wizardCaption=" Cadcli MOVSER SUBSER " wizardOrientation="Vertical" wizardFormMethod="post" returnPage="CadSer.ccp">
			<Components>
				<TextBox id="70" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_CODCLI" wizardCaption="CODCLI" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="71" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_CGCCPF" wizardCaption="CGCCPF" wizardSize="14" wizardMaxLength="14" wizardIsPassword="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<Link id="97" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link1" hrefSource="ManutCadSer.ccp" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<LinkParameters/>
					<Attributes/>
					<Features/>
				</Link>
				<Button id="69" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="55" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="Faturar" dataSource="MOVSER, CADCLI, SUBSER" name="cadcli_MOVSER_SUBSER1" pageSizeLimit="100" wizardCaption="List of Cadcli, MOVSER, SUBSER " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="Não encontrado." orderBy="DESCLI, cadcli.CODCLI, DATINI, DATFIM">
			<Components>
				<Sorter id="74" visible="True" name="Sorter_CODCLI" column="MOVSER.CODCLI" wizardCaption="CODCLI" wizardSortingType="SimpleDir" wizardControl="CODCLI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="75" visible="True" name="Sorter_CGCCPF" column="CGCCPF" wizardCaption="CGCCPF" wizardSortingType="SimpleDir" wizardControl="CGCCPF" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="76" visible="True" name="Sorter_DESCLI" column="DESCLI" wizardCaption="DESCLI" wizardSortingType="SimpleDir" wizardControl="DESCLI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="77" visible="True" name="Sorter_QTDMED" column="QTDMED" wizardCaption="QTDMED" wizardSortingType="SimpleDir" wizardControl="QTDMED" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="78" visible="True" name="Sorter_DESSUB" column="DESSUB" wizardCaption="DESSUB" wizardSortingType="SimpleDir" wizardControl="DESSUB" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="79" visible="True" name="Sorter_SUBRED" column="SUBRED" wizardCaption="SUBRED" wizardSortingType="SimpleDir" wizardControl="SUBRED" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="80" visible="True" name="Sorter_UNIDAD" column="UNIDAD" wizardCaption="UNIDAD" wizardSortingType="SimpleDir" wizardControl="UNIDAD" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="81" visible="True" name="Sorter_DATINI" column="DATINI" wizardCaption="DATINI" wizardSortingType="SimpleDir" wizardControl="DATINI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="82" visible="True" name="Sorter_DATFIM" column="DATFIM" wizardCaption="DATFIM" wizardSortingType="SimpleDir" wizardControl="DATFIM" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Link id="83" fieldSourceType="DBColumn" dataType="Text" html="False" name="CODCLI" fieldSource="CODCLI" wizardCaption="CODCLI" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="ManutCadSer.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="93" sourceType="DataField" name="CODCLI" source="CODCLI"/>
						<LinkParameter id="94" sourceType="DataField" name="IDSERCLI" source="IDSERCLI"/>
					</LinkParameters>
				</Link>
				<Link id="95" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link1" wizardUseTemplateBlock="False" hrefSource="ManutCadSer.ccp">
					<Components/>
					<Events/>
					<LinkParameters>
						<LinkParameter id="96" sourceType="DataField" name="CODCLI" source="CODCLI"/>
					</LinkParameters>
					<Attributes/>
					<Features/>
				</Link>
				<Label id="84" fieldSourceType="DBColumn" dataType="Text" html="False" name="CGCCPF" fieldSource="CGCCPF" wizardCaption="CGCCPF" wizardSize="14" wizardMaxLength="14" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="85" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESCLI" fieldSource="DESCLI" wizardCaption="DESCLI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="86" fieldSourceType="DBColumn" dataType="Float" html="False" name="QTDMED" fieldSource="QTDMED" wizardCaption="QTDMED" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="87" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESSUB" fieldSource="DESSUB" wizardCaption="DESSUB" wizardSize="50" wizardMaxLength="75" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="88" fieldSourceType="DBColumn" dataType="Text" html="False" name="SUBRED" fieldSource="SUBRED" wizardCaption="SUBRED" wizardSize="50" wizardMaxLength="60" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="89" fieldSourceType="DBColumn" dataType="Text" html="False" name="UNIDAD" fieldSource="UNIDAD" wizardCaption="UNIDAD" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="90" fieldSourceType="DBColumn" dataType="Date" html="False" name="DATINI" fieldSource="DATINI" wizardCaption="DATINI" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="91" fieldSourceType="DBColumn" dataType="Date" html="False" name="DATFIM" fieldSource="DATFIM" wizardCaption="DATFIM" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="92" size="10" type="Centered" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="of" wizardImagesScheme="Blueprint">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="72" conditionType="Parameter" useIsNull="False" field="MOVSER.CODCLI" parameterSource="s_CODCLI" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="1"/>
				<TableParameter id="73" conditionType="Parameter" useIsNull="False" field="cadcli.CGCCPF" parameterSource="s_CGCCPF" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="2"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="56" tableName="MOVSER" posLeft="10" posTop="10" posWidth="115" posHeight="180"/>
				<JoinTable id="57" tableName="CADCLI" schemaName="SL005V3" posLeft="328" posTop="230" posWidth="115" posHeight="180"/>
				<JoinTable id="58" tableName="SUBSER" schemaName="SL005V3" posLeft="489" posTop="45" posWidth="115" posHeight="152"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="59" tableLeft="CADCLI" tableRight="MOVSER" fieldLeft="CADCLI.CODCLI" fieldRight="MOVSER.CODCLI" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="60" tableLeft="SUBSER" tableRight="MOVSER" fieldLeft="SUBSER.SUBSER" fieldRight="MOVSER.SUBSER" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="61" tableName="MOVSER" fieldName="MOVSER.*"/>
				<Field id="62" tableName="CADCLI" fieldName="CGCCPF"/>
				<Field id="64" tableName="CADCLI" fieldName="DESCLI"/>
				<Field id="65" tableName="SUBSER" fieldName="SUBRED"/>
				<Field id="66" tableName="SUBSER" fieldName="UNIDAD"/>
				<Field id="67" tableName="SUBSER" fieldName="DESSUB"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="CadSer.php" forShow="True" url="CadSer.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="CadSer_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="50" groupID="1"/>
		<Group id="51" groupID="2"/>
		<Group id="52" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="54"/>
			</Actions>
		</Event>
	</Events>
</Page>
