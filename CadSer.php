<?php
//Include Common Files @1-036252A4
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "CadSer.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation



class clsRecordcadcli_MOVSER_SUBSER { //cadcli_MOVSER_SUBSER Class @68-26484A92

//Variables @68-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @68-4702DD80
    function clsRecordcadcli_MOVSER_SUBSER($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record cadcli_MOVSER_SUBSER/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "cadcli_MOVSER_SUBSER";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_CODCLI = new clsControl(ccsTextBox, "s_CODCLI", "s_CODCLI", ccsText, "", CCGetRequestParam("s_CODCLI", $Method, NULL), $this);
            $this->s_CGCCPF = new clsControl(ccsTextBox, "s_CGCCPF", "s_CGCCPF", ccsText, "", CCGetRequestParam("s_CGCCPF", $Method, NULL), $this);
            $this->Link1 = new clsControl(ccsLink, "Link1", "Link1", ccsText, "", CCGetRequestParam("Link1", $Method, NULL), $this);
            $this->Link1->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
            $this->Link1->Page = "ManutCadSer.php";
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
        }
    }
//End Class_Initialize Event

//Validate Method @68-A1694A5F
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_CODCLI->Validate() && $Validation);
        $Validation = ($this->s_CGCCPF->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->s_CODCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_CGCCPF->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @68-A06DE8DA
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_CODCLI->Errors->Count());
        $errors = ($errors || $this->s_CGCCPF->Errors->Count());
        $errors = ($errors || $this->Link1->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @68-FF614415
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "CadSer.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                $Redirect = "CadSer.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @68-E1C1A061
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->s_CODCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_CGCCPF->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Link1->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_CODCLI->Show();
        $this->s_CGCCPF->Show();
        $this->Link1->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End cadcli_MOVSER_SUBSER Class @68-FCB6E20C

class clsGridcadcli_MOVSER_SUBSER1 { //cadcli_MOVSER_SUBSER1 class @55-2C4C2F62

//Variables @55-E804A21A

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_CODCLI;
    public $Sorter_CGCCPF;
    public $Sorter_DESCLI;
    public $Sorter_QTDMED;
    public $Sorter_DESSUB;
    public $Sorter_SUBRED;
    public $Sorter_UNIDAD;
    public $Sorter_DATINI;
    public $Sorter_DATFIM;
//End Variables

//Class_Initialize Event @55-43556182
    function clsGridcadcli_MOVSER_SUBSER1($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "cadcli_MOVSER_SUBSER1";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid cadcli_MOVSER_SUBSER1";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clscadcli_MOVSER_SUBSER1DataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 10;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("cadcli_MOVSER_SUBSER1Order", "");
        $this->SorterDirection = CCGetParam("cadcli_MOVSER_SUBSER1Dir", "");

        $this->CODCLI = new clsControl(ccsLink, "CODCLI", "CODCLI", ccsText, "", CCGetRequestParam("CODCLI", ccsGet, NULL), $this);
        $this->CODCLI->Page = "ManutCadSer.php";
        $this->Link1 = new clsControl(ccsLink, "Link1", "Link1", ccsText, "", CCGetRequestParam("Link1", ccsGet, NULL), $this);
        $this->Link1->Page = "ManutCadSer.php";
        $this->CGCCPF = new clsControl(ccsLabel, "CGCCPF", "CGCCPF", ccsText, "", CCGetRequestParam("CGCCPF", ccsGet, NULL), $this);
        $this->DESCLI = new clsControl(ccsLabel, "DESCLI", "DESCLI", ccsText, "", CCGetRequestParam("DESCLI", ccsGet, NULL), $this);
        $this->QTDMED = new clsControl(ccsLabel, "QTDMED", "QTDMED", ccsFloat, "", CCGetRequestParam("QTDMED", ccsGet, NULL), $this);
        $this->DESSUB = new clsControl(ccsLabel, "DESSUB", "DESSUB", ccsText, "", CCGetRequestParam("DESSUB", ccsGet, NULL), $this);
        $this->SUBRED = new clsControl(ccsLabel, "SUBRED", "SUBRED", ccsText, "", CCGetRequestParam("SUBRED", ccsGet, NULL), $this);
        $this->UNIDAD = new clsControl(ccsLabel, "UNIDAD", "UNIDAD", ccsText, "", CCGetRequestParam("UNIDAD", ccsGet, NULL), $this);
        $this->DATINI = new clsControl(ccsLabel, "DATINI", "DATINI", ccsDate, $DefaultDateFormat, CCGetRequestParam("DATINI", ccsGet, NULL), $this);
        $this->DATFIM = new clsControl(ccsLabel, "DATFIM", "DATFIM", ccsDate, $DefaultDateFormat, CCGetRequestParam("DATFIM", ccsGet, NULL), $this);
        $this->Sorter_CODCLI = new clsSorter($this->ComponentName, "Sorter_CODCLI", $FileName, $this);
        $this->Sorter_CGCCPF = new clsSorter($this->ComponentName, "Sorter_CGCCPF", $FileName, $this);
        $this->Sorter_DESCLI = new clsSorter($this->ComponentName, "Sorter_DESCLI", $FileName, $this);
        $this->Sorter_QTDMED = new clsSorter($this->ComponentName, "Sorter_QTDMED", $FileName, $this);
        $this->Sorter_DESSUB = new clsSorter($this->ComponentName, "Sorter_DESSUB", $FileName, $this);
        $this->Sorter_SUBRED = new clsSorter($this->ComponentName, "Sorter_SUBRED", $FileName, $this);
        $this->Sorter_UNIDAD = new clsSorter($this->ComponentName, "Sorter_UNIDAD", $FileName, $this);
        $this->Sorter_DATINI = new clsSorter($this->ComponentName, "Sorter_DATINI", $FileName, $this);
        $this->Sorter_DATFIM = new clsSorter($this->ComponentName, "Sorter_DATFIM", $FileName, $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpCentered, $this);
    }
//End Class_Initialize Event

//Initialize Method @55-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @55-2F822DD7
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urls_CODCLI"] = CCGetFromGet("s_CODCLI", NULL);
        $this->DataSource->Parameters["urls_CGCCPF"] = CCGetFromGet("s_CGCCPF", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["CODCLI"] = $this->CODCLI->Visible;
            $this->ControlsVisible["Link1"] = $this->Link1->Visible;
            $this->ControlsVisible["CGCCPF"] = $this->CGCCPF->Visible;
            $this->ControlsVisible["DESCLI"] = $this->DESCLI->Visible;
            $this->ControlsVisible["QTDMED"] = $this->QTDMED->Visible;
            $this->ControlsVisible["DESSUB"] = $this->DESSUB->Visible;
            $this->ControlsVisible["SUBRED"] = $this->SUBRED->Visible;
            $this->ControlsVisible["UNIDAD"] = $this->UNIDAD->Visible;
            $this->ControlsVisible["DATINI"] = $this->DATINI->Visible;
            $this->ControlsVisible["DATFIM"] = $this->DATFIM->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->CODCLI->SetValue($this->DataSource->CODCLI->GetValue());
                $this->CODCLI->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->CODCLI->Parameters = CCAddParam($this->CODCLI->Parameters, "CODCLI", $this->DataSource->f("CODCLI"));
                $this->CODCLI->Parameters = CCAddParam($this->CODCLI->Parameters, "IDSERCLI", $this->DataSource->f("IDSERCLI"));
                $this->Link1->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->Link1->Parameters = CCAddParam($this->Link1->Parameters, "CODCLI", $this->DataSource->f("CODCLI"));
                $this->CGCCPF->SetValue($this->DataSource->CGCCPF->GetValue());
                $this->DESCLI->SetValue($this->DataSource->DESCLI->GetValue());
                $this->QTDMED->SetValue($this->DataSource->QTDMED->GetValue());
                $this->DESSUB->SetValue($this->DataSource->DESSUB->GetValue());
                $this->SUBRED->SetValue($this->DataSource->SUBRED->GetValue());
                $this->UNIDAD->SetValue($this->DataSource->UNIDAD->GetValue());
                $this->DATINI->SetValue($this->DataSource->DATINI->GetValue());
                $this->DATFIM->SetValue($this->DataSource->DATFIM->GetValue());
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->CODCLI->Show();
                $this->Link1->Show();
                $this->CGCCPF->Show();
                $this->DESCLI->Show();
                $this->QTDMED->Show();
                $this->DESSUB->Show();
                $this->SUBRED->Show();
                $this->UNIDAD->Show();
                $this->DATINI->Show();
                $this->DATFIM->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        $this->Sorter_CODCLI->Show();
        $this->Sorter_CGCCPF->Show();
        $this->Sorter_DESCLI->Show();
        $this->Sorter_QTDMED->Show();
        $this->Sorter_DESSUB->Show();
        $this->Sorter_SUBRED->Show();
        $this->Sorter_UNIDAD->Show();
        $this->Sorter_DATINI->Show();
        $this->Sorter_DATFIM->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @55-8474F222
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->CODCLI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Link1->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CGCCPF->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESCLI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->QTDMED->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESSUB->Errors->ToString());
        $errors = ComposeStrings($errors, $this->SUBRED->Errors->ToString());
        $errors = ComposeStrings($errors, $this->UNIDAD->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DATINI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DATFIM->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End cadcli_MOVSER_SUBSER1 Class @55-FCB6E20C

class clscadcli_MOVSER_SUBSER1DataSource extends clsDBFaturar {  //cadcli_MOVSER_SUBSER1DataSource Class @55-CABEBE07

//DataSource Variables @55-686037EC
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $CODCLI;
    public $CGCCPF;
    public $DESCLI;
    public $QTDMED;
    public $DESSUB;
    public $SUBRED;
    public $UNIDAD;
    public $DATINI;
    public $DATFIM;
//End DataSource Variables

//DataSourceClass_Initialize Event @55-6879CD4E
    function clscadcli_MOVSER_SUBSER1DataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid cadcli_MOVSER_SUBSER1";
        $this->Initialize();
        $this->CODCLI = new clsField("CODCLI", ccsText, "");
        $this->CGCCPF = new clsField("CGCCPF", ccsText, "");
        $this->DESCLI = new clsField("DESCLI", ccsText, "");
        $this->QTDMED = new clsField("QTDMED", ccsFloat, "");
        $this->DESSUB = new clsField("DESSUB", ccsText, "");
        $this->SUBRED = new clsField("SUBRED", ccsText, "");
        $this->UNIDAD = new clsField("UNIDAD", ccsText, "");
        $this->DATINI = new clsField("DATINI", ccsDate, $this->DateFormat);
        $this->DATFIM = new clsField("DATFIM", ccsDate, $this->DateFormat);

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @55-CE88F105
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "DESCLI, cadcli.CODCLI, DATINI, DATFIM";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_CODCLI" => array("MOVSER.CODCLI", ""), 
            "Sorter_CGCCPF" => array("CGCCPF", ""), 
            "Sorter_DESCLI" => array("DESCLI", ""), 
            "Sorter_QTDMED" => array("QTDMED", ""), 
            "Sorter_DESSUB" => array("DESSUB", ""), 
            "Sorter_SUBRED" => array("SUBRED", ""), 
            "Sorter_UNIDAD" => array("UNIDAD", ""), 
            "Sorter_DATINI" => array("DATINI", ""), 
            "Sorter_DATFIM" => array("DATFIM", "")));
    }
//End SetOrder Method

//Prepare Method @55-CFA74A09
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_CODCLI", ccsText, "", "", $this->Parameters["urls_CODCLI"], "", false);
        $this->wp->AddParameter("2", "urls_CGCCPF", ccsText, "", "", $this->Parameters["urls_CGCCPF"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "MOVSER.CODCLI", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opContains, "cadcli.CGCCPF", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsText),false);
        $this->Where = $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]);
        $this->Where = $this->wp->opAND(false, "( (CADCLI.CODCLI = MOVSER.CODCLI) AND (SUBSER.SUBSER = MOVSER.SUBSER) )", $this->Where);
    }
//End Prepare Method

//Open Method @55-98D8D657
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM MOVSER,\n\n" .
        "CADCLI,\n\n" .
        "SUBSER";
        $this->SQL = "SELECT MOVSER.*, CGCCPF, DESCLI, SUBRED, UNIDAD, DESSUB \n\n" .
        "FROM MOVSER,\n\n" .
        "CADCLI,\n\n" .
        "SUBSER {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @55-7BE24F0E
    function SetValues()
    {
        $this->CODCLI->SetDBValue($this->f("CODCLI"));
        $this->CGCCPF->SetDBValue($this->f("CGCCPF"));
        $this->DESCLI->SetDBValue($this->f("DESCLI"));
        $this->QTDMED->SetDBValue(trim($this->f("QTDMED")));
        $this->DESSUB->SetDBValue($this->f("DESSUB"));
        $this->SUBRED->SetDBValue($this->f("SUBRED"));
        $this->UNIDAD->SetDBValue($this->f("UNIDAD"));
        $this->DATINI->SetDBValue(trim($this->f("DATINI")));
        $this->DATFIM->SetDBValue(trim($this->f("DATFIM")));
    }
//End SetValues Method

} //End cadcli_MOVSER_SUBSER1DataSource Class @55-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-B19D34CC
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "CadSer.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-946ECC7A
CCSecurityRedirect("1;2;3", "");
//End Authenticate User

//Include events file @1-4BCF07F7
include("./CadSer_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-28BCD60B
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$cadcli_MOVSER_SUBSER = new clsRecordcadcli_MOVSER_SUBSER("", $MainPage);
$cadcli_MOVSER_SUBSER1 = new clsGridcadcli_MOVSER_SUBSER1("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->cadcli_MOVSER_SUBSER = & $cadcli_MOVSER_SUBSER;
$MainPage->cadcli_MOVSER_SUBSER1 = & $cadcli_MOVSER_SUBSER1;
$MainPage->rodape = & $rodape;
$cadcli_MOVSER_SUBSER1->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-DD74BA80
$cabec->Operations();
$cadcli_MOVSER_SUBSER->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-C52388B4
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($cadcli_MOVSER_SUBSER);
    unset($cadcli_MOVSER_SUBSER1);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-6BADF125
$cabec->Show();
$cadcli_MOVSER_SUBSER->Show();
$cadcli_MOVSER_SUBSER1->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-39CDBC4A
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($cadcli_MOVSER_SUBSER);
unset($cadcli_MOVSER_SUBSER1);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
