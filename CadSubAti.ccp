<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="10" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="GRPATI_SUBATI" wizardCaption="Search GRPATI SUBATI " wizardOrientation="Vertical" wizardFormMethod="post" returnPage="CadSubAti.ccp" debugMode="False">
			<Components>
				<ListBox id="12" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="s_GRPATI" wizardCaption="GRPATI" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="SUBATI" boundColumn="SUBATI" textColumn="DESSUB" editable="True" hasErrorCollection="True">
					<Components/>
					<Events>
						<Event name="BeforeBuildSelect" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="31"/>
							</Actions>
						</Event>
					</Events>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<Link id="26" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link1" hrefSource="ManutSubAti.ccp" wizardUseTemplateBlock="False" editable="False">
					<Components/>
					<Events/>
					<LinkParameters/>
					<Attributes/>
					<Features/>
				</Link>
				<Button id="11" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="4" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="Faturar" dataSource="SUBATI, GRPATI" name="GRPATI_SUBATI1" pageSizeLimit="100" wizardCaption="List of GRPATI, SUBATI " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="Nenhum Subgrupo de atividade foi encontrado" activeCollection="TableParameters">
			<Components>
				<Sorter id="15" visible="True" name="Sorter_SUBATI" column="SUBATI" wizardCaption="SUBATI" wizardSortingType="SimpleDir" wizardControl="SUBATI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="16" visible="True" name="Sorter_DESSUB" column="DESSUB" wizardCaption="DESSUB" wizardSortingType="SimpleDir" wizardControl="DESSUB" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="14" visible="True" name="Sorter_DESATI" column="DESATI" wizardCaption="DESATI" wizardSortingType="SimpleDir" wizardControl="DESATI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Link id="20" fieldSourceType="DBColumn" dataType="Text" html="False" name="SUBATI" fieldSource="SUBATI" wizardCaption="SUBATI" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="ManutSubAti.ccp" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="27" sourceType="DataField" name="SUBATI" source="SUBATI"/>
					</LinkParameters>
				</Link>
				<Label id="22" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESSUB" fieldSource="DESSUB" wizardCaption="DESSUB" wizardSize="35" wizardMaxLength="35" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="18" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESATI" fieldSource="DESATI" wizardCaption="DESATI" wizardSize="35" wizardMaxLength="35" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="25" size="10" name="Navigator" wizardPagingType="Custom" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardImages="Images" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="True" wizardOfText="of" wizardImagesScheme="Pine" type="Simple">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="13" conditionType="Parameter" useIsNull="False" field="SUBATI.SUBATI" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="1" parameterSource="s_GRPATI"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="5" tableName="SUBATI" posLeft="10" posTop="10" posWidth="133" posHeight="151"/>
				<JoinTable id="6" tableName="GRPATI" posLeft="164" posTop="10" posWidth="111" posHeight="148"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="7" tableLeft="GRPATI" tableRight="SUBATI" fieldLeft="GRPATI.GRPATI" fieldRight="SUBATI.GRPATI" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="8" tableName="SUBATI" fieldName="SUBATI.*"/>
				<Field id="9" tableName="GRPATI" fieldName="DESATI"/>
				<Field id="19" tableName="SUBATI" fieldName="SUBATI" alias="SUBATI"/>
				<Field id="21" tableName="SUBATI" fieldName="DESSUB" alias="DESSUB"/>
				<Field id="23" tableName="SUBATI" fieldName="GRPATI" alias="GRPATI"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="CadSubAti.php" forShow="True" url="CadSubAti.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="CadSubAti_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="28" groupID="1"/>
		<Group id="29" groupID="2"/>
		<Group id="30" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="32"/>
			</Actions>
		</Event>
	</Events>
</Page>
