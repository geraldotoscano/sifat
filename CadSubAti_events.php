<?php
//BindEvents Method @1-25E151FF
function BindEvents()
{
    global $GRPATI_SUBATI;
    global $CCSEvents;
    $GRPATI_SUBATI->s_GRPATI->ds->CCSEvents["BeforeBuildSelect"] = "GRPATI_SUBATI_s_GRPATI_ds_BeforeBuildSelect";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//GRPATI_SUBATI_s_GRPATI_ds_BeforeBuildSelect @12-6EAD2FEB
function GRPATI_SUBATI_s_GRPATI_ds_BeforeBuildSelect(& $sender)
{
    $GRPATI_SUBATI_s_GRPATI_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GRPATI_SUBATI; //Compatibility
//End GRPATI_SUBATI_s_GRPATI_ds_BeforeBuildSelect

//Custom Code @31-2A29BDB7
// -------------------------
    if ($GRPATI_SUBATI->s_GRPATI->DataSource->Order == "") 
	{
		$GRPATI_SUBATI->s_GRPATI->DataSource->Order = "DESSUB";
	}
// -------------------------
//End Custom Code

//Close GRPATI_SUBATI_s_GRPATI_ds_BeforeBuildSelect @12-3E4AEB1D
    return $GRPATI_SUBATI_s_GRPATI_ds_BeforeBuildSelect;
}
//End Close GRPATI_SUBATI_s_GRPATI_ds_BeforeBuildSelect

//Page_BeforeShow @1-41B071BE
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CadSubAti; //Compatibility
//End Page_BeforeShow

//Custom Code @32-2A29BDB7
// -------------------------

        include("controle_acesso.php");
        $Tabela = new clsDBfaturar();
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(9,8);
		controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
