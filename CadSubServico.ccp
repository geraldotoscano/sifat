<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="5" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="SUBSERSearch" wizardCaption="Search SUBSER " wizardOrientation="Vertical" wizardFormMethod="post" returnPage="CadSubServico.ccp" debugMode="False">
			<Components>
				<ListBox id="7" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="s_SUBSER" wizardCaption="SUBSER" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="SUBSER" boundColumn="SUBSER" textColumn="DESSUB" editable="True" hasErrorCollection="True">
					<Components/>
					<Events>
						<Event name="BeforeBuildSelect" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="47"/>
							</Actions>
						</Event>
					</Events>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<ListBox id="48" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="s_GRPSER" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="GRPSER" boundColumn="GRPSER" textColumn="DESSER">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<TextBox id="49" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_DESSUB">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<Link id="42" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link1" hrefSource="ManutSubServico.ccp" wizardUseTemplateBlock="False" editable="False">
					<Components/>
					<Events/>
					<LinkParameters/>
					<Attributes/>
					<Features/>
				</Link>
				<Button id="6" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="4" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="Faturar" dataSource="SUBSER, GRPSER" name="SUBSER" orderBy="SUBSER.SUBSER" pageSizeLimit="100" wizardCaption="List of SUBSER " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Controls" wizardRecordSeparator="True" wizardNoRecords="Nenhum Sub Serviço foi encontrado" activeCollection="TableParameters">
			<Components>
				<Label id="8" fieldSourceType="DBColumn" dataType="Text" html="False" name="SUBSER_TotalRecords" wizardUseTemplateBlock="False" editable="False">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Retrieve number of records" actionCategory="Database" id="9"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Panel id="11" visible="True" name="Header_SUBSER">
					<Components>
						<Sorter id="12" visible="True" name="Sorter_SUBSER" column="SUBSER" wizardCaption="SUBSER" wizardSortingType="SimpleDir" wizardControl="SUBSER" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="13" visible="True" name="Header_DESSUB">
					<Components>
						<Sorter id="14" visible="True" name="Sorter_DESSUB" column="DESSUB" wizardCaption="DESSUB" wizardSortingType="SimpleDir" wizardControl="DESSUB" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="15" visible="True" name="Header_SUBRED">
					<Components>
						<Sorter id="16" visible="True" name="Sorter_SUBRED" column="SUBRED" wizardCaption="SUBRED" wizardSortingType="SimpleDir" wizardControl="SUBRED" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="17" visible="True" name="Header_GRPSER">
					<Components>
						<Sorter id="18" visible="True" name="Sorter_GRPSER" column="GRPSER" wizardCaption="GRPSER" wizardSortingType="SimpleDir" wizardControl="GRPSER" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="19" visible="True" name="Header_UNIDAD">
					<Components>
						<Sorter id="20" visible="True" name="Sorter_UNIDAD" column="UNIDAD" wizardCaption="UNIDAD" wizardSortingType="SimpleDir" wizardControl="UNIDAD" wizardAddNbsp="False" connection="faturar_Homo">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="22" visible="True" name="Data_SUBSER">
					<Components>
						<Link id="23" fieldSourceType="DBColumn" dataType="Text" html="False" name="SUBSER" fieldSource="SUBSER" wizardCaption="SUBSER" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="ManutSubServico.ccp">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
							<LinkParameters>
								<LinkParameter id="43" sourceType="DataField" name="SUBSER" source="SUBSER"/>
							</LinkParameters>
						</Link>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="25" visible="True" name="Data_DESSUB">
					<Components>
						<Label id="26" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESSUB" fieldSource="DESSUB" wizardCaption="DESSUB" wizardSize="50" wizardMaxLength="75" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="28" visible="True" name="Data_SUBRED">
					<Components>
						<Label id="29" fieldSourceType="DBColumn" dataType="Text" html="False" name="SUBRED" fieldSource="SUBRED" wizardCaption="SUBRED" wizardSize="50" wizardMaxLength="60" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="31" visible="True" name="Data_GRPSER">
					<Components>
						<Label id="32" fieldSourceType="DBColumn" dataType="Text" html="False" name="GRPSER" fieldSource="DESSER" wizardCaption="GRPSER" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="34" visible="True" name="Data_UNIDAD">
					<Components>
						<Label id="35" fieldSourceType="DBColumn" dataType="Text" html="False" name="UNIDAD" fieldSource="UNIDAD" wizardCaption="UNIDAD" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Navigator id="36" size="10" name="Navigator" wizardPagingType="Custom" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardImages="Images" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="True" wizardOfText="of" wizardImagesScheme="Apricot" type="Simple">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="10" conditionType="Parameter" useIsNull="False" field="SUBSER.SUBSER" parameterSource="s_SUBSER" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="1"/>
				<TableParameter id="50" conditionType="Parameter" useIsNull="False" field="SUBSER.GRPSER" dataType="Text" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="s_GRPSER"/>
				<TableParameter id="51" conditionType="Parameter" useIsNull="False" field="SUBSER.DESSUB" dataType="Text" searchConditionType="Contains" parameterType="URL" logicOperator="And" parameterSource="s_DESSUB"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="37" tableName="SUBSER" posLeft="10" posTop="10" posWidth="115" posHeight="152"/>
				<JoinTable id="38" tableName="GRPSER" posLeft="146" posTop="10" posWidth="95" posHeight="88"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="39" tableLeft="GRPSER" tableRight="SUBSER" fieldLeft="GRPSER.GRPSER" fieldRight="SUBSER.GRPSER" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="40" tableName="SUBSER" fieldName="SUBSER.*"/>
				<Field id="41" tableName="GRPSER" fieldName="DESSER"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="PHPTemplates" name="CadSubServico_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Code" language="PHPTemplates" name="CadSubServico.php" forShow="True" url="CadSubServico.php" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="44" groupID="1"/>
		<Group id="45" groupID="2"/>
		<Group id="46" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="52"/>
			</Actions>
		</Event>
	</Events>
</Page>
