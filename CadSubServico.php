<?php
//Include Common Files @1-6B6463A1
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "CadSubServico.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordSUBSERSearch { //SUBSERSearch Class @5-28927767

//Variables @5-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @5-DDFC397A
    function clsRecordSUBSERSearch($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record SUBSERSearch/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "SUBSERSearch";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_SUBSER = new clsControl(ccsListBox, "s_SUBSER", "s_SUBSER", ccsText, "", CCGetRequestParam("s_SUBSER", $Method, NULL), $this);
            $this->s_SUBSER->DSType = dsTable;
            $this->s_SUBSER->DataSource = new clsDBFaturar();
            $this->s_SUBSER->ds = & $this->s_SUBSER->DataSource;
            $this->s_SUBSER->DataSource->SQL = "SELECT * \n" .
"FROM SUBSER {SQL_Where} {SQL_OrderBy}";
            list($this->s_SUBSER->BoundColumn, $this->s_SUBSER->TextColumn, $this->s_SUBSER->DBFormat) = array("SUBSER", "DESSUB", "");
            $this->s_GRPSER = new clsControl(ccsListBox, "s_GRPSER", "s_GRPSER", ccsText, "", CCGetRequestParam("s_GRPSER", $Method, NULL), $this);
            $this->s_GRPSER->DSType = dsTable;
            $this->s_GRPSER->DataSource = new clsDBFaturar();
            $this->s_GRPSER->ds = & $this->s_GRPSER->DataSource;
            $this->s_GRPSER->DataSource->SQL = "SELECT * \n" .
"FROM GRPSER {SQL_Where} {SQL_OrderBy}";
            list($this->s_GRPSER->BoundColumn, $this->s_GRPSER->TextColumn, $this->s_GRPSER->DBFormat) = array("GRPSER", "DESSER", "");
            $this->s_DESSUB = new clsControl(ccsTextBox, "s_DESSUB", "s_DESSUB", ccsText, "", CCGetRequestParam("s_DESSUB", $Method, NULL), $this);
            $this->Link1 = new clsControl(ccsLink, "Link1", "Link1", ccsText, "", CCGetRequestParam("Link1", $Method, NULL), $this);
            $this->Link1->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
            $this->Link1->Page = "ManutSubServico.php";
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
        }
    }
//End Class_Initialize Event

//Validate Method @5-AF893530
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_SUBSER->Validate() && $Validation);
        $Validation = ($this->s_GRPSER->Validate() && $Validation);
        $Validation = ($this->s_DESSUB->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->s_SUBSER->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_GRPSER->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_DESSUB->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @5-3494A2EE
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_SUBSER->Errors->Count());
        $errors = ($errors || $this->s_GRPSER->Errors->Count());
        $errors = ($errors || $this->s_DESSUB->Errors->Count());
        $errors = ($errors || $this->Link1->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @5-82946431
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "CadSubServico.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                $Redirect = "CadSubServico.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @5-981809B9
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->s_SUBSER->Prepare();
        $this->s_GRPSER->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->s_SUBSER->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_GRPSER->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_DESSUB->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Link1->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_SUBSER->Show();
        $this->s_GRPSER->Show();
        $this->s_DESSUB->Show();
        $this->Link1->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End SUBSERSearch Class @5-FCB6E20C

class clsGridSUBSER { //SUBSER class @4-EE5D0CB5

//Variables @4-3FD983F0

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_SUBSER;
    public $Sorter_DESSUB;
    public $Sorter_SUBRED;
    public $Sorter_GRPSER;
    public $Sorter_UNIDAD;
//End Variables

//Class_Initialize Event @4-A9626F14
    function clsGridSUBSER($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "SUBSER";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid SUBSER";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clsSUBSERDataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 10;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("SUBSEROrder", "");
        $this->SorterDirection = CCGetParam("SUBSERDir", "");

        $this->Data_SUBSER = new clsPanel("Data_SUBSER", $this);
        $this->SUBSER = new clsControl(ccsLink, "SUBSER", "SUBSER", ccsText, "", CCGetRequestParam("SUBSER", ccsGet, NULL), $this);
        $this->SUBSER->Page = "ManutSubServico.php";
        $this->Data_DESSUB = new clsPanel("Data_DESSUB", $this);
        $this->DESSUB = new clsControl(ccsLabel, "DESSUB", "DESSUB", ccsText, "", CCGetRequestParam("DESSUB", ccsGet, NULL), $this);
        $this->Data_SUBRED = new clsPanel("Data_SUBRED", $this);
        $this->SUBRED = new clsControl(ccsLabel, "SUBRED", "SUBRED", ccsText, "", CCGetRequestParam("SUBRED", ccsGet, NULL), $this);
        $this->Data_GRPSER = new clsPanel("Data_GRPSER", $this);
        $this->GRPSER = new clsControl(ccsLabel, "GRPSER", "GRPSER", ccsText, "", CCGetRequestParam("GRPSER", ccsGet, NULL), $this);
        $this->Data_UNIDAD = new clsPanel("Data_UNIDAD", $this);
        $this->UNIDAD = new clsControl(ccsLabel, "UNIDAD", "UNIDAD", ccsText, "", CCGetRequestParam("UNIDAD", ccsGet, NULL), $this);
        $this->SUBSER_TotalRecords = new clsControl(ccsLabel, "SUBSER_TotalRecords", "SUBSER_TotalRecords", ccsText, "", CCGetRequestParam("SUBSER_TotalRecords", ccsGet, NULL), $this);
        $this->Header_SUBSER = new clsPanel("Header_SUBSER", $this);
        $this->Sorter_SUBSER = new clsSorter($this->ComponentName, "Sorter_SUBSER", $FileName, $this);
        $this->Header_DESSUB = new clsPanel("Header_DESSUB", $this);
        $this->Sorter_DESSUB = new clsSorter($this->ComponentName, "Sorter_DESSUB", $FileName, $this);
        $this->Header_SUBRED = new clsPanel("Header_SUBRED", $this);
        $this->Sorter_SUBRED = new clsSorter($this->ComponentName, "Sorter_SUBRED", $FileName, $this);
        $this->Header_GRPSER = new clsPanel("Header_GRPSER", $this);
        $this->Sorter_GRPSER = new clsSorter($this->ComponentName, "Sorter_GRPSER", $FileName, $this);
        $this->Header_UNIDAD = new clsPanel("Header_UNIDAD", $this);
        $this->Sorter_UNIDAD = new clsSorter($this->ComponentName, "Sorter_UNIDAD", $FileName, $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple, $this);
        $this->Header_SUBSER->AddComponent("Sorter_SUBSER", $this->Sorter_SUBSER);
        $this->Header_DESSUB->AddComponent("Sorter_DESSUB", $this->Sorter_DESSUB);
        $this->Header_SUBRED->AddComponent("Sorter_SUBRED", $this->Sorter_SUBRED);
        $this->Header_GRPSER->AddComponent("Sorter_GRPSER", $this->Sorter_GRPSER);
        $this->Header_UNIDAD->AddComponent("Sorter_UNIDAD", $this->Sorter_UNIDAD);
        $this->Data_SUBSER->AddComponent("SUBSER", $this->SUBSER);
        $this->Data_DESSUB->AddComponent("DESSUB", $this->DESSUB);
        $this->Data_SUBRED->AddComponent("SUBRED", $this->SUBRED);
        $this->Data_GRPSER->AddComponent("GRPSER", $this->GRPSER);
        $this->Data_UNIDAD->AddComponent("UNIDAD", $this->UNIDAD);
    }
//End Class_Initialize Event

//Initialize Method @4-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @4-4B308D17
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urls_SUBSER"] = CCGetFromGet("s_SUBSER", NULL);
        $this->DataSource->Parameters["urls_GRPSER"] = CCGetFromGet("s_GRPSER", NULL);
        $this->DataSource->Parameters["urls_DESSUB"] = CCGetFromGet("s_DESSUB", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["Data_SUBSER"] = $this->Data_SUBSER->Visible;
            $this->ControlsVisible["SUBSER"] = $this->SUBSER->Visible;
            $this->ControlsVisible["Data_DESSUB"] = $this->Data_DESSUB->Visible;
            $this->ControlsVisible["DESSUB"] = $this->DESSUB->Visible;
            $this->ControlsVisible["Data_SUBRED"] = $this->Data_SUBRED->Visible;
            $this->ControlsVisible["SUBRED"] = $this->SUBRED->Visible;
            $this->ControlsVisible["Data_GRPSER"] = $this->Data_GRPSER->Visible;
            $this->ControlsVisible["GRPSER"] = $this->GRPSER->Visible;
            $this->ControlsVisible["Data_UNIDAD"] = $this->Data_UNIDAD->Visible;
            $this->ControlsVisible["UNIDAD"] = $this->UNIDAD->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                // Parse Separator
                if($this->RowNumber) {
                    $this->Attributes->Show();
                    $Tpl->parseto("Separator", true, "Row");
                }
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->SUBSER->SetValue($this->DataSource->SUBSER->GetValue());
                $this->SUBSER->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->SUBSER->Parameters = CCAddParam($this->SUBSER->Parameters, "SUBSER", $this->DataSource->f("SUBSER"));
                $this->DESSUB->SetValue($this->DataSource->DESSUB->GetValue());
                $this->SUBRED->SetValue($this->DataSource->SUBRED->GetValue());
                $this->GRPSER->SetValue($this->DataSource->GRPSER->GetValue());
                $this->UNIDAD->SetValue($this->DataSource->UNIDAD->GetValue());
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->Data_SUBSER->Show();
                $this->Data_DESSUB->Show();
                $this->Data_SUBRED->Show();
                $this->Data_GRPSER->Show();
                $this->Data_UNIDAD->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        $this->SUBSER_TotalRecords->Show();
        $this->Header_SUBSER->Show();
        $this->Sorter_SUBSER->Show();
        $this->Header_DESSUB->Show();
        $this->Sorter_DESSUB->Show();
        $this->Header_SUBRED->Show();
        $this->Sorter_SUBRED->Show();
        $this->Header_GRPSER->Show();
        $this->Sorter_GRPSER->Show();
        $this->Header_UNIDAD->Show();
        $this->Sorter_UNIDAD->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @4-F74FEAA5
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->SUBSER->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESSUB->Errors->ToString());
        $errors = ComposeStrings($errors, $this->SUBRED->Errors->ToString());
        $errors = ComposeStrings($errors, $this->GRPSER->Errors->ToString());
        $errors = ComposeStrings($errors, $this->UNIDAD->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End SUBSER Class @4-FCB6E20C

class clsSUBSERDataSource extends clsDBFaturar {  //SUBSERDataSource Class @4-79BDC49B

//DataSource Variables @4-FFC3F93F
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $SUBSER;
    public $DESSUB;
    public $SUBRED;
    public $GRPSER;
    public $UNIDAD;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-E10582D8
    function clsSUBSERDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid SUBSER";
        $this->Initialize();
        $this->SUBSER = new clsField("SUBSER", ccsText, "");
        $this->DESSUB = new clsField("DESSUB", ccsText, "");
        $this->SUBRED = new clsField("SUBRED", ccsText, "");
        $this->GRPSER = new clsField("GRPSER", ccsText, "");
        $this->UNIDAD = new clsField("UNIDAD", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @4-10604C67
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "SUBSER.SUBSER";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_SUBSER" => array("SUBSER", ""), 
            "Sorter_DESSUB" => array("DESSUB", ""), 
            "Sorter_SUBRED" => array("SUBRED", ""), 
            "Sorter_GRPSER" => array("GRPSER", ""), 
            "Sorter_UNIDAD" => array("UNIDAD", "")));
    }
//End SetOrder Method

//Prepare Method @4-B303563A
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_SUBSER", ccsText, "", "", $this->Parameters["urls_SUBSER"], "", false);
        $this->wp->AddParameter("2", "urls_GRPSER", ccsText, "", "", $this->Parameters["urls_GRPSER"], "", false);
        $this->wp->AddParameter("3", "urls_DESSUB", ccsText, "", "", $this->Parameters["urls_DESSUB"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "SUBSER.SUBSER", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opEqual, "SUBSER.GRPSER", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsText),false);
        $this->wp->Criterion[3] = $this->wp->Operation(opContains, "SUBSER.DESSUB", $this->wp->GetDBValue("3"), $this->ToSQL($this->wp->GetDBValue("3"), ccsText),false);
        $this->Where = $this->wp->opAND(
             false, $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]), 
             $this->wp->Criterion[3]);
        $this->Where = $this->wp->opAND(false, "( (GRPSER.GRPSER = SUBSER.GRPSER) )", $this->Where);
    }
//End Prepare Method

//Open Method @4-4C96AB51
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM SUBSER,\n\n" .
        "GRPSER";
        $this->SQL = "SELECT SUBSER.*, DESSER \n\n" .
        "FROM SUBSER,\n\n" .
        "GRPSER {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @4-98550D08
    function SetValues()
    {
        $this->SUBSER->SetDBValue($this->f("SUBSER"));
        $this->DESSUB->SetDBValue($this->f("DESSUB"));
        $this->SUBRED->SetDBValue($this->f("SUBRED"));
        $this->GRPSER->SetDBValue($this->f("DESSER"));
        $this->UNIDAD->SetDBValue($this->f("UNIDAD"));
    }
//End SetValues Method

} //End SUBSERDataSource Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-094AFCBA
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "CadSubServico.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-946ECC7A
CCSecurityRedirect("1;2;3", "");
//End Authenticate User

//Include events file @1-0F823463
include("./CadSubServico_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-BC13DA4F
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$SUBSERSearch = new clsRecordSUBSERSearch("", $MainPage);
$SUBSER = new clsGridSUBSER("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->SUBSERSearch = & $SUBSERSearch;
$MainPage->SUBSER = & $SUBSER;
$MainPage->rodape = & $rodape;
$SUBSER->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-EC5120B4
$cabec->Operations();
$SUBSERSearch->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-3F3594D6
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($SUBSERSearch);
    unset($SUBSER);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-16DB83D2
$cabec->Show();
$SUBSERSearch->Show();
$SUBSER->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-5059A98E
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($SUBSERSearch);
unset($SUBSER);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
