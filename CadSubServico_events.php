<?php
//BindEvents Method @1-BEACD7FF
function BindEvents()
{
    global $SUBSERSearch;
    global $SUBSER;
    global $CCSEvents;
    $SUBSERSearch->s_SUBSER->ds->CCSEvents["BeforeBuildSelect"] = "SUBSERSearch_s_SUBSER_ds_BeforeBuildSelect";
    $SUBSER->SUBSER_TotalRecords->CCSEvents["BeforeShow"] = "SUBSER_SUBSER_TotalRecords_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//SUBSERSearch_s_SUBSER_ds_BeforeBuildSelect @7-9328D69B
function SUBSERSearch_s_SUBSER_ds_BeforeBuildSelect(& $sender)
{
    $SUBSERSearch_s_SUBSER_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $SUBSERSearch; //Compatibility
//End SUBSERSearch_s_SUBSER_ds_BeforeBuildSelect

//Custom Code @47-2A29BDB7
// -------------------------
    if ($SUBSERSearch->s_SUBSER->DataSource->Order == "")
	{
		$SUBSERSearch->s_SUBSER->DataSource->Order = "DESSUB";
	}
// -------------------------
//End Custom Code

//Close SUBSERSearch_s_SUBSER_ds_BeforeBuildSelect @7-06E830ED
    return $SUBSERSearch_s_SUBSER_ds_BeforeBuildSelect;
}
//End Close SUBSERSearch_s_SUBSER_ds_BeforeBuildSelect

//SUBSER_SUBSER_TotalRecords_BeforeShow @8-7212A250
function SUBSER_SUBSER_TotalRecords_BeforeShow(& $sender)
{
    $SUBSER_SUBSER_TotalRecords_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $SUBSER; //Compatibility
//End SUBSER_SUBSER_TotalRecords_BeforeShow

//Retrieve number of records @9-ABE656B4
    $Component->SetValue($Container->DataSource->RecordsCount);
//End Retrieve number of records

//Close SUBSER_SUBSER_TotalRecords_BeforeShow @8-63D6BA00
    return $SUBSER_SUBSER_TotalRecords_BeforeShow;
}
//End Close SUBSER_SUBSER_TotalRecords_BeforeShow

//Page_BeforeShow @1-A2705CF3
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CadSubServico; //Compatibility
//End Page_BeforeShow

//Custom Code @52-2A29BDB7
// -------------------------

        include("controle_acesso.php");
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(13,12);
		controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
