<?php
//Include Common Files @1-FE502F68
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "CadTabCol.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @5-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordTABCOL_TABDIS_TABUNI { //TABCOL_TABDIS_TABUNI Class @17-154098E9

//Variables @17-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @17-01975C14
    function clsRecordTABCOL_TABDIS_TABUNI($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record TABCOL_TABDIS_TABUNI/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "TABCOL_TABDIS_TABUNI";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_CODUNI = new clsControl(ccsListBox, "s_CODUNI", "s_CODUNI", ccsText, "", CCGetRequestParam("s_CODUNI", $Method, NULL), $this);
            $this->s_CODUNI->DSType = dsTable;
            $this->s_CODUNI->DataSource = new clsDBFaturar();
            $this->s_CODUNI->ds = & $this->s_CODUNI->DataSource;
            $this->s_CODUNI->DataSource->SQL = "SELECT * \n" .
"FROM TABUNI {SQL_Where} {SQL_OrderBy}";
            list($this->s_CODUNI->BoundColumn, $this->s_CODUNI->TextColumn, $this->s_CODUNI->DBFormat) = array("CODUNI", "DESUNI", "");
            $this->s_CODDIS = new clsControl(ccsListBox, "s_CODDIS", "s_CODDIS", ccsText, "", CCGetRequestParam("s_CODDIS", $Method, NULL), $this);
            $this->s_CODDIS->DSType = dsTable;
            $this->s_CODDIS->DataSource = new clsDBFaturar();
            $this->s_CODDIS->ds = & $this->s_CODDIS->DataSource;
            $this->s_CODDIS->DataSource->SQL = "SELECT * \n" .
"FROM TABDIS {SQL_Where} {SQL_OrderBy}";
            list($this->s_CODDIS->BoundColumn, $this->s_CODDIS->TextColumn, $this->s_CODDIS->DBFormat) = array("CODDIS", "DESDIS", "");
            $this->s_CODSET = new clsControl(ccsTextBox, "s_CODSET", "s_CODSET", ccsText, "", CCGetRequestParam("s_CODSET", $Method, NULL), $this);
            $this->Link1 = new clsControl(ccsLink, "Link1", "Link1", ccsText, "", CCGetRequestParam("Link1", $Method, NULL), $this);
            $this->Link1->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
            $this->Link1->Page = "ManutTabCol.php";
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
        }
    }
//End Class_Initialize Event

//Validate Method @17-51051AB2
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_CODUNI->Validate() && $Validation);
        $Validation = ($this->s_CODDIS->Validate() && $Validation);
        $Validation = ($this->s_CODSET->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->s_CODUNI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_CODDIS->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_CODSET->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @17-7682CCBC
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_CODUNI->Errors->Count());
        $errors = ($errors || $this->s_CODDIS->Errors->Count());
        $errors = ($errors || $this->s_CODSET->Errors->Count());
        $errors = ($errors || $this->Link1->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @17-7FD5552C
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "CadTabCol.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                $Redirect = "CadTabCol.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @17-DFCF3245
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->s_CODUNI->Prepare();
        $this->s_CODDIS->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->s_CODUNI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_CODDIS->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_CODSET->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Link1->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_CODUNI->Show();
        $this->s_CODDIS->Show();
        $this->s_CODSET->Show();
        $this->Link1->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End TABCOL_TABDIS_TABUNI Class @17-FCB6E20C

class clsGridTABCOL_TABDIS_TABUNI1 { //TABCOL_TABDIS_TABUNI1 class @6-EBA88F04

//Variables @6-0E32AB69

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_DESUNI;
    public $Sorter_DESCRI;
    public $Sorter_DESDIS;
    public $Sorter1;
    public $Sorter_FREQUE;
//End Variables

//Class_Initialize Event @6-C9C3AD06
    function clsGridTABCOL_TABDIS_TABUNI1($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "TABCOL_TABDIS_TABUNI1";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid TABCOL_TABDIS_TABUNI1";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clsTABCOL_TABDIS_TABUNI1DataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 10;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("TABCOL_TABDIS_TABUNI1Order", "");
        $this->SorterDirection = CCGetParam("TABCOL_TABDIS_TABUNI1Dir", "");

        $this->DESUNI = new clsControl(ccsLabel, "DESUNI", "DESUNI", ccsText, "", CCGetRequestParam("DESUNI", ccsGet, NULL), $this);
        $this->DESCRI = new clsControl(ccsLabel, "DESCRI", "DESCRI", ccsText, "", CCGetRequestParam("DESCRI", ccsGet, NULL), $this);
        $this->DESDIS = new clsControl(ccsLabel, "DESDIS", "DESDIS", ccsText, "", CCGetRequestParam("DESDIS", ccsGet, NULL), $this);
        $this->Label1 = new clsControl(ccsLabel, "Label1", "Label1", ccsText, "", CCGetRequestParam("Label1", ccsGet, NULL), $this);
        $this->FREQUE = new clsControl(ccsLink, "FREQUE", "FREQUE", ccsText, "", CCGetRequestParam("FREQUE", ccsGet, NULL), $this);
        $this->FREQUE->Page = "ManutTabColAlter.php";
        $this->Sorter_DESUNI = new clsSorter($this->ComponentName, "Sorter_DESUNI", $FileName, $this);
        $this->Sorter_DESCRI = new clsSorter($this->ComponentName, "Sorter_DESCRI", $FileName, $this);
        $this->Sorter_DESDIS = new clsSorter($this->ComponentName, "Sorter_DESDIS", $FileName, $this);
        $this->Sorter1 = new clsSorter($this->ComponentName, "Sorter1", $FileName, $this);
        $this->Sorter_FREQUE = new clsSorter($this->ComponentName, "Sorter_FREQUE", $FileName, $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple, $this);
    }
//End Class_Initialize Event

//Initialize Method @6-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @6-75217758
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urls_CODUNI"] = CCGetFromGet("s_CODUNI", NULL);
        $this->DataSource->Parameters["urls_CODDIS"] = CCGetFromGet("s_CODDIS", NULL);
        $this->DataSource->Parameters["urls_CODSET"] = CCGetFromGet("s_CODSET", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["DESUNI"] = $this->DESUNI->Visible;
            $this->ControlsVisible["DESCRI"] = $this->DESCRI->Visible;
            $this->ControlsVisible["DESDIS"] = $this->DESDIS->Visible;
            $this->ControlsVisible["Label1"] = $this->Label1->Visible;
            $this->ControlsVisible["FREQUE"] = $this->FREQUE->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->DESUNI->SetValue($this->DataSource->DESUNI->GetValue());
                $this->DESCRI->SetValue($this->DataSource->DESCRI->GetValue());
                $this->DESDIS->SetValue($this->DataSource->DESDIS->GetValue());
                $this->Label1->SetValue($this->DataSource->Label1->GetValue());
                $this->FREQUE->SetValue($this->DataSource->FREQUE->GetValue());
                $this->FREQUE->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->FREQUE->Parameters = CCAddParam($this->FREQUE->Parameters, "CODUNI", $this->DataSource->f("CODUNI"));
                $this->FREQUE->Parameters = CCAddParam($this->FREQUE->Parameters, "CODDIS", $this->DataSource->f("CODDIS"));
                $this->FREQUE->Parameters = CCAddParam($this->FREQUE->Parameters, "CODSET", $this->DataSource->f("CODSET"));
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->DESUNI->Show();
                $this->DESCRI->Show();
                $this->DESDIS->Show();
                $this->Label1->Show();
                $this->FREQUE->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        $this->Sorter_DESUNI->Show();
        $this->Sorter_DESCRI->Show();
        $this->Sorter_DESDIS->Show();
        $this->Sorter1->Show();
        $this->Sorter_FREQUE->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @6-A13535BC
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->DESUNI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESCRI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESDIS->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Label1->Errors->ToString());
        $errors = ComposeStrings($errors, $this->FREQUE->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End TABCOL_TABDIS_TABUNI1 Class @6-FCB6E20C

class clsTABCOL_TABDIS_TABUNI1DataSource extends clsDBFaturar {  //TABCOL_TABDIS_TABUNI1DataSource Class @6-B82E73B1

//DataSource Variables @6-E597B9CD
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $DESUNI;
    public $DESCRI;
    public $DESDIS;
    public $Label1;
    public $FREQUE;
//End DataSource Variables

//DataSourceClass_Initialize Event @6-D2A35CC0
    function clsTABCOL_TABDIS_TABUNI1DataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid TABCOL_TABDIS_TABUNI1";
        $this->Initialize();
        $this->DESUNI = new clsField("DESUNI", ccsText, "");
        $this->DESCRI = new clsField("DESCRI", ccsText, "");
        $this->DESDIS = new clsField("DESDIS", ccsText, "");
        $this->Label1 = new clsField("Label1", ccsText, "");
        $this->FREQUE = new clsField("FREQUE", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @6-02190090
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_DESUNI" => array("DESUNI", ""), 
            "Sorter_DESCRI" => array("DESCRI", ""), 
            "Sorter_DESDIS" => array("DESDIS", ""), 
            "Sorter1" => array("CODSET", ""), 
            "Sorter_FREQUE" => array("FREQUE", "")));
    }
//End SetOrder Method

//Prepare Method @6-683EDB35
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_CODUNI", ccsText, "", "", $this->Parameters["urls_CODUNI"], "", false);
        $this->wp->AddParameter("2", "urls_CODDIS", ccsText, "", "", $this->Parameters["urls_CODDIS"], "", false);
        $this->wp->AddParameter("3", "urls_CODSET", ccsText, "", "", $this->Parameters["urls_CODSET"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "TABCOL.CODUNI", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opContains, "TABCOL.CODDIS", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsText),false);
        $this->wp->Criterion[3] = $this->wp->Operation(opContains, "TABCOL.CODSET", $this->wp->GetDBValue("3"), $this->ToSQL($this->wp->GetDBValue("3"), ccsText),false);
        $this->Where = $this->wp->opAND(
             false, $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]), 
             $this->wp->Criterion[3]);
        $this->Where = $this->wp->opAND(false, "( (TABDIS.CODDIS = TABCOL.CODDIS) AND (TABUNI.CODUNI = TABCOL.CODUNI) )", $this->Where);
    }
//End Prepare Method

//Open Method @6-2DC2F4D4
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM TABCOL,\n\n" .
        "TABDIS,\n\n" .
        "TABUNI";
        $this->SQL = "SELECT TABCOL.*, DESUNI, DESCRI, DESDIS \n\n" .
        "FROM TABCOL,\n\n" .
        "TABDIS,\n\n" .
        "TABUNI {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @6-62647487
    function SetValues()
    {
        $this->DESUNI->SetDBValue($this->f("DESUNI"));
        $this->DESCRI->SetDBValue($this->f("DESCRI"));
        $this->DESDIS->SetDBValue($this->f("DESDIS"));
        $this->Label1->SetDBValue($this->f("CODSET"));
        $this->FREQUE->SetDBValue($this->f("FREQUE"));
    }
//End SetValues Method

} //End TABCOL_TABDIS_TABUNI1DataSource Class @6-FCB6E20C

//Include Page implementation @4-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-C0D75A61
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "CadTabCol.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-946ECC7A
CCSecurityRedirect("1;2;3", "");
//End Authenticate User

//Include events file @1-7B311002
include("./CadTabCol_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-483D2EF0
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$TABCOL_TABDIS_TABUNI = new clsRecordTABCOL_TABDIS_TABUNI("", $MainPage);
$TABCOL_TABDIS_TABUNI1 = new clsGridTABCOL_TABDIS_TABUNI1("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->TABCOL_TABDIS_TABUNI = & $TABCOL_TABDIS_TABUNI;
$MainPage->TABCOL_TABDIS_TABUNI1 = & $TABCOL_TABDIS_TABUNI1;
$MainPage->rodape = & $rodape;
$TABCOL_TABDIS_TABUNI1->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-26CED26B
$cabec->Operations();
$TABCOL_TABDIS_TABUNI->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-06A3AFFA
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($TABCOL_TABDIS_TABUNI);
    unset($TABCOL_TABDIS_TABUNI1);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-516FCDE2
$cabec->Show();
$TABCOL_TABDIS_TABUNI->Show();
$TABCOL_TABDIS_TABUNI1->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-3A1DAB04
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($TABCOL_TABDIS_TABUNI);
unset($TABCOL_TABDIS_TABUNI1);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
