<?php
//BindEvents Method @1-DC438564
function BindEvents()
{
    global $TABCOL_TABDIS_TABUNI;
    global $CCSEvents;
    $TABCOL_TABDIS_TABUNI->s_CODUNI->ds->CCSEvents["BeforeBuildSelect"] = "TABCOL_TABDIS_TABUNI_s_CODUNI_ds_BeforeBuildSelect";
    $TABCOL_TABDIS_TABUNI->s_CODDIS->ds->CCSEvents["BeforeBuildSelect"] = "TABCOL_TABDIS_TABUNI_s_CODDIS_ds_BeforeBuildSelect";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//TABCOL_TABDIS_TABUNI_s_CODUNI_ds_BeforeBuildSelect @19-DD2F5983
function TABCOL_TABDIS_TABUNI_s_CODUNI_ds_BeforeBuildSelect(& $sender)
{
    $TABCOL_TABDIS_TABUNI_s_CODUNI_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABCOL_TABDIS_TABUNI; //Compatibility
//End TABCOL_TABDIS_TABUNI_s_CODUNI_ds_BeforeBuildSelect

//Custom Code @44-2A29BDB7
// -------------------------
    if ($TABCOL_TABDIS_TABUNI->s_CODUNI->DataSource->Order == "")
	{
		$TABCOL_TABDIS_TABUNI->s_CODUNI->DataSource->Order = "DESUNI";
	}
// -------------------------
//End Custom Code

//Close TABCOL_TABDIS_TABUNI_s_CODUNI_ds_BeforeBuildSelect @19-CF361AC2
    return $TABCOL_TABDIS_TABUNI_s_CODUNI_ds_BeforeBuildSelect;
}
//End Close TABCOL_TABDIS_TABUNI_s_CODUNI_ds_BeforeBuildSelect

//TABCOL_TABDIS_TABUNI_s_CODDIS_ds_BeforeBuildSelect @20-34D6BE9D
function TABCOL_TABDIS_TABUNI_s_CODDIS_ds_BeforeBuildSelect(& $sender)
{
    $TABCOL_TABDIS_TABUNI_s_CODDIS_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABCOL_TABDIS_TABUNI; //Compatibility
//End TABCOL_TABDIS_TABUNI_s_CODDIS_ds_BeforeBuildSelect

//Custom Code @45-2A29BDB7
// -------------------------
    if ($TABCOL_TABDIS_TABUNI->s_CODDIS->DataSource->Order == "")
	{
		$TABCOL_TABDIS_TABUNI->s_CODDIS->DataSource->Order = "DESDIS";
	}
// -------------------------
//End Custom Code

//Close TABCOL_TABDIS_TABUNI_s_CODDIS_ds_BeforeBuildSelect @20-38749409
    return $TABCOL_TABDIS_TABUNI_s_CODDIS_ds_BeforeBuildSelect;
}
//End Close TABCOL_TABDIS_TABUNI_s_CODDIS_ds_BeforeBuildSelect

//Page_BeforeShow @1-2E1A9CA2
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CadTabCol; //Compatibility
//End Page_BeforeShow

//Custom Code @46-2A29BDB7
// -------------------------

        include("controle_acesso.php");
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(19,18);
        controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
