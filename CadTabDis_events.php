<?php
//BindEvents Method @1-6DF7554F
function BindEvents()
{
    global $TABDIS;
    global $CCSEvents;
    $TABDIS->CODDIS->CCSEvents["BeforeShow"] = "TABDIS_CODDIS_BeforeShow";
    $TABDIS->CCSEvents["BeforeShow"] = "TABDIS_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//TABDIS_CODDIS_BeforeShow @13-0D0DFEE5
function TABDIS_CODDIS_BeforeShow(& $sender)
{
    $TABDIS_CODDIS_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABDIS; //Compatibility
//End TABDIS_CODDIS_BeforeShow

//DLookup @16-8BCB2051
    if ($TABDIS->Button_Insert->Visible)
	{
       global $DBfaturar;
       $Page = CCGetParentPage($sender);
       $ccs_result = CCDLookUp("max(to_number(coddis))+1", "tabdis", "", $Page->Connections["Faturar"]);
	   if (is_null($ccs_result))
	   {
	   		$ccs_result = 1;//Para o caso da tabela estar vazia;
	   }
       $ccs_result = intval($ccs_result);
       $Component->SetValue($ccs_result);
	}
//End DLookup

//Close TABDIS_CODDIS_BeforeShow @13-CF34B8E3
    return $TABDIS_CODDIS_BeforeShow;
}
//End Close TABDIS_CODDIS_BeforeShow

//DEL  // -------------------------
//DEL     if ( !caracterInvado($Component->GetValue()) )
//DEL     {
//DEL        $TABDIS->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
//DEL     }
//DEL  // -------------------------

//TABDIS_BeforeShow @6-07CAD72F
function TABDIS_BeforeShow(& $sender)
{
    $TABDIS_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABDIS; //Compatibility
//End TABDIS_BeforeShow

//DLookup @15-A52FCD3A
    global $DBfaturar;
    $Page = CCGetParentPage($sender);
	if ($TABDIS->Button_Delete->Visible)
	{
       $ccs_result = CCDLookUp("coddis", "cadcli", "coddis ='".$TABDIS->CODDIS->Value."'", $Page->Connections["Faturar"]);
	   if ($ccs_result != '' )
	   {
	      $TABDIS->Button_Delete->Visible = false;
	   }
	}
//End DLookup

//Close TABDIS_BeforeShow @6-B22D746C
    return $TABDIS_BeforeShow;
}
//End Close TABDIS_BeforeShow

//Page_BeforeShow @1-BB8B01C4
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CadTabDis; //Compatibility
//End Page_BeforeShow

//Custom Code @22-2A29BDB7
// -------------------------

        include("controle_acesso.php");
        $Tabela = new clsDBfaturar();
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(4);
		controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
