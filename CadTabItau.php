<?php
//Include Common Files @1-A90BC500
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "CadTabItau.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsGridTABITAU { //TABITAU class @4-E0F6472E

//Variables @4-5CA47EC9

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_EMPRESA;
    public $Sorter_AGENCIA;
    public $Sorter_DV_AGENCIA;
    public $Sorter_CONTACO;
    public $Sorter_NUMECGC;
    public $Sorter_SIGLA;
    public $Sorter_ENDERECO;
    public $Sorter_CEP;
    public $Sorter_PRACA;
//End Variables

//Class_Initialize Event @4-0DCDFDAF
    function clsGridTABITAU($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "TABITAU";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid TABITAU";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clsTABITAUDataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 10;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("TABITAUOrder", "");
        $this->SorterDirection = CCGetParam("TABITAUDir", "");

        $this->EMPRESA = new clsControl(ccsLink, "EMPRESA", "EMPRESA", ccsText, "", CCGetRequestParam("EMPRESA", ccsGet, NULL), $this);
        $this->EMPRESA->Page = "ManutTabItau.php";
        $this->AGENCIA = new clsControl(ccsLabel, "AGENCIA", "AGENCIA", ccsText, "", CCGetRequestParam("AGENCIA", ccsGet, NULL), $this);
        $this->DV_AGENCIA = new clsControl(ccsLabel, "DV_AGENCIA", "DV_AGENCIA", ccsText, "", CCGetRequestParam("DV_AGENCIA", ccsGet, NULL), $this);
        $this->CONTACO = new clsControl(ccsLabel, "CONTACO", "CONTACO", ccsText, "", CCGetRequestParam("CONTACO", ccsGet, NULL), $this);
        $this->NUMECGC = new clsControl(ccsLabel, "NUMECGC", "NUMECGC", ccsText, "", CCGetRequestParam("NUMECGC", ccsGet, NULL), $this);
        $this->SIGLA = new clsControl(ccsLabel, "SIGLA", "SIGLA", ccsText, "", CCGetRequestParam("SIGLA", ccsGet, NULL), $this);
        $this->ENDERECO = new clsControl(ccsLabel, "ENDERECO", "ENDERECO", ccsText, "", CCGetRequestParam("ENDERECO", ccsGet, NULL), $this);
        $this->CEP = new clsControl(ccsLabel, "CEP", "CEP", ccsText, "", CCGetRequestParam("CEP", ccsGet, NULL), $this);
        $this->PRACA = new clsControl(ccsLabel, "PRACA", "PRACA", ccsText, "", CCGetRequestParam("PRACA", ccsGet, NULL), $this);
        $this->Sorter_EMPRESA = new clsSorter($this->ComponentName, "Sorter_EMPRESA", $FileName, $this);
        $this->Sorter_AGENCIA = new clsSorter($this->ComponentName, "Sorter_AGENCIA", $FileName, $this);
        $this->Sorter_DV_AGENCIA = new clsSorter($this->ComponentName, "Sorter_DV_AGENCIA", $FileName, $this);
        $this->Sorter_CONTACO = new clsSorter($this->ComponentName, "Sorter_CONTACO", $FileName, $this);
        $this->Sorter_NUMECGC = new clsSorter($this->ComponentName, "Sorter_NUMECGC", $FileName, $this);
        $this->Sorter_SIGLA = new clsSorter($this->ComponentName, "Sorter_SIGLA", $FileName, $this);
        $this->Sorter_ENDERECO = new clsSorter($this->ComponentName, "Sorter_ENDERECO", $FileName, $this);
        $this->Sorter_CEP = new clsSorter($this->ComponentName, "Sorter_CEP", $FileName, $this);
        $this->Sorter_PRACA = new clsSorter($this->ComponentName, "Sorter_PRACA", $FileName, $this);
    }
//End Class_Initialize Event

//Initialize Method @4-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @4-0DC12690
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;


        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["EMPRESA"] = $this->EMPRESA->Visible;
            $this->ControlsVisible["AGENCIA"] = $this->AGENCIA->Visible;
            $this->ControlsVisible["DV_AGENCIA"] = $this->DV_AGENCIA->Visible;
            $this->ControlsVisible["CONTACO"] = $this->CONTACO->Visible;
            $this->ControlsVisible["NUMECGC"] = $this->NUMECGC->Visible;
            $this->ControlsVisible["SIGLA"] = $this->SIGLA->Visible;
            $this->ControlsVisible["ENDERECO"] = $this->ENDERECO->Visible;
            $this->ControlsVisible["CEP"] = $this->CEP->Visible;
            $this->ControlsVisible["PRACA"] = $this->PRACA->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->EMPRESA->SetValue($this->DataSource->EMPRESA->GetValue());
                $this->EMPRESA->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->EMPRESA->Parameters = CCAddParam($this->EMPRESA->Parameters, "EMPRESA", $this->DataSource->f("EMPRESA"));
                $this->AGENCIA->SetValue($this->DataSource->AGENCIA->GetValue());
                $this->DV_AGENCIA->SetValue($this->DataSource->DV_AGENCIA->GetValue());
                $this->CONTACO->SetValue($this->DataSource->CONTACO->GetValue());
                $this->NUMECGC->SetValue($this->DataSource->NUMECGC->GetValue());
                $this->SIGLA->SetValue($this->DataSource->SIGLA->GetValue());
                $this->ENDERECO->SetValue($this->DataSource->ENDERECO->GetValue());
                $this->CEP->SetValue($this->DataSource->CEP->GetValue());
                $this->PRACA->SetValue($this->DataSource->PRACA->GetValue());
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->EMPRESA->Show();
                $this->AGENCIA->Show();
                $this->DV_AGENCIA->Show();
                $this->CONTACO->Show();
                $this->NUMECGC->Show();
                $this->SIGLA->Show();
                $this->ENDERECO->Show();
                $this->CEP->Show();
                $this->PRACA->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Sorter_EMPRESA->Show();
        $this->Sorter_AGENCIA->Show();
        $this->Sorter_DV_AGENCIA->Show();
        $this->Sorter_CONTACO->Show();
        $this->Sorter_NUMECGC->Show();
        $this->Sorter_SIGLA->Show();
        $this->Sorter_ENDERECO->Show();
        $this->Sorter_CEP->Show();
        $this->Sorter_PRACA->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @4-E549996E
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->EMPRESA->Errors->ToString());
        $errors = ComposeStrings($errors, $this->AGENCIA->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DV_AGENCIA->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CONTACO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->NUMECGC->Errors->ToString());
        $errors = ComposeStrings($errors, $this->SIGLA->Errors->ToString());
        $errors = ComposeStrings($errors, $this->ENDERECO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CEP->Errors->ToString());
        $errors = ComposeStrings($errors, $this->PRACA->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End TABITAU Class @4-FCB6E20C

class clsTABITAUDataSource extends clsDBFaturar {  //TABITAUDataSource Class @4-B8D0B262

//DataSource Variables @4-26D7A518
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $EMPRESA;
    public $AGENCIA;
    public $DV_AGENCIA;
    public $CONTACO;
    public $NUMECGC;
    public $SIGLA;
    public $ENDERECO;
    public $CEP;
    public $PRACA;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-2232F490
    function clsTABITAUDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid TABITAU";
        $this->Initialize();
        $this->EMPRESA = new clsField("EMPRESA", ccsText, "");
        $this->AGENCIA = new clsField("AGENCIA", ccsText, "");
        $this->DV_AGENCIA = new clsField("DV_AGENCIA", ccsText, "");
        $this->CONTACO = new clsField("CONTACO", ccsText, "");
        $this->NUMECGC = new clsField("NUMECGC", ccsText, "");
        $this->SIGLA = new clsField("SIGLA", ccsText, "");
        $this->ENDERECO = new clsField("ENDERECO", ccsText, "");
        $this->CEP = new clsField("CEP", ccsText, "");
        $this->PRACA = new clsField("PRACA", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @4-45C1DAF7
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_EMPRESA" => array("EMPRESA", ""), 
            "Sorter_AGENCIA" => array("AGENCIA", ""), 
            "Sorter_DV_AGENCIA" => array("DV_AGENCIA", ""), 
            "Sorter_CONTACO" => array("CONTACO", ""), 
            "Sorter_NUMECGC" => array("NUMECGC", ""), 
            "Sorter_SIGLA" => array("SIGLA", ""), 
            "Sorter_ENDERECO" => array("ENDERECO", ""), 
            "Sorter_CEP" => array("CEP", ""), 
            "Sorter_PRACA" => array("PRACA", "")));
    }
//End SetOrder Method

//Prepare Method @4-14D6CD9D
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
    }
//End Prepare Method

//Open Method @4-CCDD54D2
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM TABITAU";
        $this->SQL = "SELECT * \n\n" .
        "FROM TABITAU {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @4-208DA792
    function SetValues()
    {
        $this->EMPRESA->SetDBValue($this->f("EMPRESA"));
        $this->AGENCIA->SetDBValue($this->f("AGENCIA"));
        $this->DV_AGENCIA->SetDBValue($this->f("DV_AGENCIA"));
        $this->CONTACO->SetDBValue($this->f("CONTACO"));
        $this->NUMECGC->SetDBValue($this->f("NUMECGC"));
        $this->SIGLA->SetDBValue($this->f("SIGLA"));
        $this->ENDERECO->SetDBValue($this->f("ENDERECO"));
        $this->CEP->SetDBValue($this->f("CEP"));
        $this->PRACA->SetDBValue($this->f("PRACA"));
    }
//End SetValues Method

} //End TABITAUDataSource Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-BB416147
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "CadTabItau.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-946ECC7A
CCSecurityRedirect("1;2;3", "");
//End Authenticate User

//Include events file @1-B0BDFF64
include("./CadTabItau_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-0E2FF400
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$TABITAU = new clsGridTABITAU("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->TABITAU = & $TABITAU;
$MainPage->rodape = & $rodape;
$TABITAU->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-30AB84C1
$cabec->Operations();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-C9D091D5
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($TABITAU);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-2D28D097
$cabec->Show();
$TABITAU->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-433E5ACA
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($TABITAU);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
