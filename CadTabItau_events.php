<?php
//BindEvents Method @1-D40060DD
function BindEvents()
{
    global $CCSEvents;
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//Page_BeforeShow @1-09CC799B
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CadTabItau; //Compatibility
//End Page_BeforeShow

//Custom Code @27-2A29BDB7
// -------------------------


    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(23,22);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");


    global $TABITAU;
	global $Redirect;
    $Page = CCGetParentPage($sender);
    $empresa = CCDLookUp("EMPRESA", "TABITAU", "", $Page->Connections["Faturar"]);
    if (empty($empresa) || is_null($empresa))
	{
		$Redirect = "ManutTabItauIncl.php";
	}
// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
