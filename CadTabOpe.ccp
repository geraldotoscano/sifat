<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="44" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="TABOPE_TABPERFIL_TABUNI" wizardCaption=" TABOPE TABPERFIL TABUNI " wizardOrientation="Vertical" wizardFormMethod="post" returnPage="CadTabOpe.ccp">
			<Components>
				<TextBox id="46" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_CODOPE" wizardCaption="CODOPE" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<Button id="45" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="38" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="Faturar" dataSource="TABOPE, TABUNI, TABPERFIL" name="TABOPE_TABPERFIL_TABUNI1" pageSizeLimit="100" wizardCaption="List of TABOPE, TABPERFIL, TABUNI " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="Não encontrado.">
			<Components>
				<Sorter id="48" visible="True" name="Sorter_CODOPE" column="CODOPE" wizardCaption="CODOPE" wizardSortingType="SimpleDir" wizardControl="CODOPE" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="49" visible="True" name="Sorter_DESOPE" column="DESOPE" wizardCaption="DESOPE" wizardSortingType="SimpleDir" wizardControl="DESOPE" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="50" visible="True" name="Sorter_PASSWO" column="PASSWO" wizardCaption="PASSWO" wizardSortingType="SimpleDir" wizardControl="PASSWO" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="51" visible="True" name="Sorter_TABOPE_ATIVO" column="TABOPE.ATIVO" wizardCaption="ATIVO" wizardSortingType="SimpleDir" wizardControl="TABOPE_ATIVO" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="52" visible="True" name="Sorter_TABPERFIL_ATIVO" column="TABPERFIL.ATIVO" wizardCaption="ATIVO" wizardSortingType="SimpleDir" wizardControl="TABPERFIL_ATIVO" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="53" visible="True" name="Sorter_DESUNI" column="DESUNI" wizardCaption="DESUNI" wizardSortingType="SimpleDir" wizardControl="DESUNI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="54" visible="True" name="Sorter_NOMEGRUPO" column="NOMEGRUPO" wizardCaption="NOMEGRUPO" wizardSortingType="SimpleDir" wizardControl="NOMEGRUPO" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Link id="56" fieldSourceType="DBColumn" dataType="Text" html="False" name="CODOPE" fieldSource="CODOPE" wizardCaption="CODOPE" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="ManutTabOpe.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="74" sourceType="DataField" name="ID" source="ID"/>
					</LinkParameters>
				</Link>
				<Label id="58" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESOPE" fieldSource="DESOPE" wizardCaption="DESOPE" wizardSize="8" wizardMaxLength="8" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="60" fieldSourceType="DBColumn" dataType="Text" html="False" name="PASSWO" fieldSource="PASSWO" wizardCaption="PASSWO" wizardSize="16" wizardMaxLength="16" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="72"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="62" fieldSourceType="DBColumn" dataType="Text" html="False" name="TABOPE_ATIVO" fieldSource="TABOPE_ATIVO" wizardCaption="ATIVO" wizardSize="1" wizardMaxLength="1" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="64" fieldSourceType="DBColumn" dataType="Text" html="False" name="TABPERFIL_ATIVO" fieldSource="TABPERFIL_ATIVO" wizardCaption="ATIVO" wizardSize="1" wizardMaxLength="1" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="66" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESUNI" fieldSource="DESUNI" wizardCaption="DESUNI" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="68" fieldSourceType="DBColumn" dataType="Text" html="False" name="NOMEGRUPO" fieldSource="NOMEGRUPO" wizardCaption="NOMEGRUPO" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="69" size="10" type="Centered" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="of" wizardImagesScheme="Blueprint">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="47" conditionType="Parameter" useIsNull="False" field="TABOPE.CODOPE" parameterSource="s_CODOPE" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="1"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="39" tableName="TABOPE" schemaName="SL005V3" posLeft="10" posTop="10" posWidth="115" posHeight="313"/>
				<JoinTable id="40" tableName="TABUNI" schemaName="SL005V3" posLeft="146" posTop="10" posWidth="95" posHeight="104"/>
				<JoinTable id="42" tableName="TABPERFIL" schemaName="SL005V3" posLeft="200" posTop="258" posWidth="95" posHeight="104"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="41" tableLeft="TABUNI" tableRight="TABOPE" fieldLeft="TABUNI.CODUNI" fieldRight="TABOPE.CODUNI" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="43" tableLeft="TABOPE" tableRight="TABPERFIL" fieldLeft="TABOPE.IDPERFIL" fieldRight="TABPERFIL.IDPERFIL" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="55" tableName="TABOPE" fieldName="CODOPE"/>
				<Field id="57" tableName="TABOPE" fieldName="DESOPE"/>
				<Field id="59" tableName="TABOPE" fieldName="PASSWO"/>
				<Field id="61" tableName="TABOPE" fieldName="TABOPE.ATIVO" alias="TABOPE_ATIVO"/>
				<Field id="63" tableName="TABPERFIL" fieldName="TABPERFIL.ATIVO" alias="TABPERFIL_ATIVO"/>
				<Field id="65" tableName="TABUNI" fieldName="DESUNI"/>
				<Field id="67" tableName="TABPERFIL" fieldName="NOMEGRUPO"/>
				<Field id="73" tableName="TABOPE" fieldName="ID"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="CadTabOpe.php" forShow="True" url="CadTabOpe.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="CadTabOpe_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="37" groupID="1"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="71"/>
			</Actions>
		</Event>
	</Events>
</Page>
