<?php
//Include Common Files @1-81306302
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "CadTabOpe.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordTABOPE_TABPERFIL_TABUNI { //TABOPE_TABPERFIL_TABUNI Class @44-7048E1A9

//Variables @44-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @44-CAAFD8A0
    function clsRecordTABOPE_TABPERFIL_TABUNI($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record TABOPE_TABPERFIL_TABUNI/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "TABOPE_TABPERFIL_TABUNI";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_CODOPE = new clsControl(ccsTextBox, "s_CODOPE", "s_CODOPE", ccsText, "", CCGetRequestParam("s_CODOPE", $Method, NULL), $this);
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
        }
    }
//End Class_Initialize Event

//Validate Method @44-F6410F05
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_CODOPE->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->s_CODOPE->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @44-9086145C
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_CODOPE->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @44-7DD64366
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "CadTabOpe.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                $Redirect = "CadTabOpe.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @44-07E7B1D4
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->s_CODOPE->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_CODOPE->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End TABOPE_TABPERFIL_TABUNI Class @44-FCB6E20C

class clsGridTABOPE_TABPERFIL_TABUNI1 { //TABOPE_TABPERFIL_TABUNI1 class @38-668D991D

//Variables @38-2DC2B090

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_CODOPE;
    public $Sorter_DESOPE;
    public $Sorter_PASSWO;
    public $Sorter_TABOPE_ATIVO;
    public $Sorter_TABPERFIL_ATIVO;
    public $Sorter_DESUNI;
    public $Sorter_NOMEGRUPO;
//End Variables

//Class_Initialize Event @38-AD11BF0A
    function clsGridTABOPE_TABPERFIL_TABUNI1($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "TABOPE_TABPERFIL_TABUNI1";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid TABOPE_TABPERFIL_TABUNI1";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clsTABOPE_TABPERFIL_TABUNI1DataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 10;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("TABOPE_TABPERFIL_TABUNI1Order", "");
        $this->SorterDirection = CCGetParam("TABOPE_TABPERFIL_TABUNI1Dir", "");

        $this->CODOPE = new clsControl(ccsLink, "CODOPE", "CODOPE", ccsText, "", CCGetRequestParam("CODOPE", ccsGet, NULL), $this);
        $this->CODOPE->Page = "ManutTabOpe.php";
        $this->DESOPE = new clsControl(ccsLabel, "DESOPE", "DESOPE", ccsText, "", CCGetRequestParam("DESOPE", ccsGet, NULL), $this);
        $this->PASSWO = new clsControl(ccsLabel, "PASSWO", "PASSWO", ccsText, "", CCGetRequestParam("PASSWO", ccsGet, NULL), $this);
        $this->TABOPE_ATIVO = new clsControl(ccsLabel, "TABOPE_ATIVO", "TABOPE_ATIVO", ccsText, "", CCGetRequestParam("TABOPE_ATIVO", ccsGet, NULL), $this);
        $this->TABPERFIL_ATIVO = new clsControl(ccsLabel, "TABPERFIL_ATIVO", "TABPERFIL_ATIVO", ccsText, "", CCGetRequestParam("TABPERFIL_ATIVO", ccsGet, NULL), $this);
        $this->DESUNI = new clsControl(ccsLabel, "DESUNI", "DESUNI", ccsText, "", CCGetRequestParam("DESUNI", ccsGet, NULL), $this);
        $this->NOMEGRUPO = new clsControl(ccsLabel, "NOMEGRUPO", "NOMEGRUPO", ccsText, "", CCGetRequestParam("NOMEGRUPO", ccsGet, NULL), $this);
        $this->Sorter_CODOPE = new clsSorter($this->ComponentName, "Sorter_CODOPE", $FileName, $this);
        $this->Sorter_DESOPE = new clsSorter($this->ComponentName, "Sorter_DESOPE", $FileName, $this);
        $this->Sorter_PASSWO = new clsSorter($this->ComponentName, "Sorter_PASSWO", $FileName, $this);
        $this->Sorter_TABOPE_ATIVO = new clsSorter($this->ComponentName, "Sorter_TABOPE_ATIVO", $FileName, $this);
        $this->Sorter_TABPERFIL_ATIVO = new clsSorter($this->ComponentName, "Sorter_TABPERFIL_ATIVO", $FileName, $this);
        $this->Sorter_DESUNI = new clsSorter($this->ComponentName, "Sorter_DESUNI", $FileName, $this);
        $this->Sorter_NOMEGRUPO = new clsSorter($this->ComponentName, "Sorter_NOMEGRUPO", $FileName, $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpCentered, $this);
    }
//End Class_Initialize Event

//Initialize Method @38-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @38-F10C83E4
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urls_CODOPE"] = CCGetFromGet("s_CODOPE", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["CODOPE"] = $this->CODOPE->Visible;
            $this->ControlsVisible["DESOPE"] = $this->DESOPE->Visible;
            $this->ControlsVisible["PASSWO"] = $this->PASSWO->Visible;
            $this->ControlsVisible["TABOPE_ATIVO"] = $this->TABOPE_ATIVO->Visible;
            $this->ControlsVisible["TABPERFIL_ATIVO"] = $this->TABPERFIL_ATIVO->Visible;
            $this->ControlsVisible["DESUNI"] = $this->DESUNI->Visible;
            $this->ControlsVisible["NOMEGRUPO"] = $this->NOMEGRUPO->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->CODOPE->SetValue($this->DataSource->CODOPE->GetValue());
                $this->CODOPE->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->CODOPE->Parameters = CCAddParam($this->CODOPE->Parameters, "ID", $this->DataSource->f("ID"));
                $this->DESOPE->SetValue($this->DataSource->DESOPE->GetValue());
                $this->PASSWO->SetValue($this->DataSource->PASSWO->GetValue());
                $this->TABOPE_ATIVO->SetValue($this->DataSource->TABOPE_ATIVO->GetValue());
                $this->TABPERFIL_ATIVO->SetValue($this->DataSource->TABPERFIL_ATIVO->GetValue());
                $this->DESUNI->SetValue($this->DataSource->DESUNI->GetValue());
                $this->NOMEGRUPO->SetValue($this->DataSource->NOMEGRUPO->GetValue());
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->CODOPE->Show();
                $this->DESOPE->Show();
                $this->PASSWO->Show();
                $this->TABOPE_ATIVO->Show();
                $this->TABPERFIL_ATIVO->Show();
                $this->DESUNI->Show();
                $this->NOMEGRUPO->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        $this->Sorter_CODOPE->Show();
        $this->Sorter_DESOPE->Show();
        $this->Sorter_PASSWO->Show();
        $this->Sorter_TABOPE_ATIVO->Show();
        $this->Sorter_TABPERFIL_ATIVO->Show();
        $this->Sorter_DESUNI->Show();
        $this->Sorter_NOMEGRUPO->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @38-91AA53D3
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->CODOPE->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESOPE->Errors->ToString());
        $errors = ComposeStrings($errors, $this->PASSWO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TABOPE_ATIVO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TABPERFIL_ATIVO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESUNI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->NOMEGRUPO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End TABOPE_TABPERFIL_TABUNI1 Class @38-FCB6E20C

class clsTABOPE_TABPERFIL_TABUNI1DataSource extends clsDBFaturar {  //TABOPE_TABPERFIL_TABUNI1DataSource Class @38-9FCC0965

//DataSource Variables @38-1068CAC6
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $CODOPE;
    public $DESOPE;
    public $PASSWO;
    public $TABOPE_ATIVO;
    public $TABPERFIL_ATIVO;
    public $DESUNI;
    public $NOMEGRUPO;
//End DataSource Variables

//DataSourceClass_Initialize Event @38-AEC9112B
    function clsTABOPE_TABPERFIL_TABUNI1DataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid TABOPE_TABPERFIL_TABUNI1";
        $this->Initialize();
        $this->CODOPE = new clsField("CODOPE", ccsText, "");
        $this->DESOPE = new clsField("DESOPE", ccsText, "");
        $this->PASSWO = new clsField("PASSWO", ccsText, "");
        $this->TABOPE_ATIVO = new clsField("TABOPE_ATIVO", ccsText, "");
        $this->TABPERFIL_ATIVO = new clsField("TABPERFIL_ATIVO", ccsText, "");
        $this->DESUNI = new clsField("DESUNI", ccsText, "");
        $this->NOMEGRUPO = new clsField("NOMEGRUPO", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @38-BAEF2709
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_CODOPE" => array("CODOPE", ""), 
            "Sorter_DESOPE" => array("DESOPE", ""), 
            "Sorter_PASSWO" => array("PASSWO", ""), 
            "Sorter_TABOPE_ATIVO" => array("TABOPE.ATIVO", ""), 
            "Sorter_TABPERFIL_ATIVO" => array("TABPERFIL.ATIVO", ""), 
            "Sorter_DESUNI" => array("DESUNI", ""), 
            "Sorter_NOMEGRUPO" => array("NOMEGRUPO", "")));
    }
//End SetOrder Method

//Prepare Method @38-FE30AEAB
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_CODOPE", ccsText, "", "", $this->Parameters["urls_CODOPE"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "TABOPE.CODOPE", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->Where = 
             $this->wp->Criterion[1];
        $this->Where = $this->wp->opAND(false, "( (TABUNI.CODUNI = TABOPE.CODUNI) AND (TABOPE.IDPERFIL = TABPERFIL.IDPERFIL) )", $this->Where);
    }
//End Prepare Method

//Open Method @38-2AAADEE4
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM TABOPE,\n\n" .
        "TABUNI,\n\n" .
        "TABPERFIL";
        $this->SQL = "SELECT CODOPE, DESOPE, PASSWO, TABOPE.ATIVO AS TABOPE_ATIVO, TABPERFIL.ATIVO AS TABPERFIL_ATIVO, DESUNI, NOMEGRUPO, ID \n\n" .
        "FROM TABOPE,\n\n" .
        "TABUNI,\n\n" .
        "TABPERFIL {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @38-7A0DBA5E
    function SetValues()
    {
        $this->CODOPE->SetDBValue($this->f("CODOPE"));
        $this->DESOPE->SetDBValue($this->f("DESOPE"));
        $this->PASSWO->SetDBValue($this->f("PASSWO"));
        $this->TABOPE_ATIVO->SetDBValue($this->f("TABOPE_ATIVO"));
        $this->TABPERFIL_ATIVO->SetDBValue($this->f("TABPERFIL_ATIVO"));
        $this->DESUNI->SetDBValue($this->f("DESUNI"));
        $this->NOMEGRUPO->SetDBValue($this->f("NOMEGRUPO"));
    }
//End SetValues Method

} //End TABOPE_TABPERFIL_TABUNI1DataSource Class @38-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-0DA75027
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "CadTabOpe.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-DC94A87D
CCSecurityRedirect("1", "");
//End Authenticate User

//Include events file @1-F0963698
include("./CadTabOpe_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-0CEA65B4
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$TABOPE_TABPERFIL_TABUNI = new clsRecordTABOPE_TABPERFIL_TABUNI("", $MainPage);
$TABOPE_TABPERFIL_TABUNI1 = new clsGridTABOPE_TABPERFIL_TABUNI1("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->TABOPE_TABPERFIL_TABUNI = & $TABOPE_TABPERFIL_TABUNI;
$MainPage->TABOPE_TABPERFIL_TABUNI1 = & $TABOPE_TABPERFIL_TABUNI1;
$MainPage->rodape = & $rodape;
$TABOPE_TABPERFIL_TABUNI1->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-2CE93D82
$cabec->Operations();
$TABOPE_TABPERFIL_TABUNI->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-90A2384C
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($TABOPE_TABPERFIL_TABUNI);
    unset($TABOPE_TABPERFIL_TABUNI1);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-6DC85DDE
$cabec->Show();
$TABOPE_TABPERFIL_TABUNI->Show();
$TABOPE_TABPERFIL_TABUNI1->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-96B05CCF
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($TABOPE_TABPERFIL_TABUNI);
unset($TABOPE_TABPERFIL_TABUNI1);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
