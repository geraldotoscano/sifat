<?php
//BindEvents Method @1-79406509
function BindEvents()
{
    global $TABOPE_TABPERFIL_TABUNI1;
    global $CCSEvents;
    $TABOPE_TABPERFIL_TABUNI1->PASSWO->CCSEvents["BeforeShow"] = "TABOPE_TABPERFIL_TABUNI1_PASSWO_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//TABOPE_TABPERFIL_TABUNI1_PASSWO_BeforeShow @60-180ABD22
function TABOPE_TABPERFIL_TABUNI1_PASSWO_BeforeShow(& $sender)
{
    $TABOPE_TABPERFIL_TABUNI1_PASSWO_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABOPE_TABPERFIL_TABUNI1; //Compatibility
//End TABOPE_TABPERFIL_TABUNI1_PASSWO_BeforeShow

//Custom Code @72-2A29BDB7
// -------------------------
//$TABOPE->PASSWO->SetValue(CCEncryptString($TABOPE->PASSWO->GetValue(), "5"));

//$Component->SetValue(CCDecryptString($Component->GetValue(),"5"));
$Component->SetValue('*****');

// -------------------------
//End Custom Code

//Close TABOPE_TABPERFIL_TABUNI1_PASSWO_BeforeShow @60-0D344059
    return $TABOPE_TABPERFIL_TABUNI1_PASSWO_BeforeShow;
}
//End Close TABOPE_TABPERFIL_TABUNI1_PASSWO_BeforeShow

//Page_BeforeShow @1-77B9F068
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CadTabOpe; //Compatibility
//End Page_BeforeShow

//Custom Code @71-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(1);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
