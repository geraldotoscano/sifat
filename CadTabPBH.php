<?php
//Include Common Files @1-E98371AE
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "CadTabPBH.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordTABPBHSearch { //TABPBHSearch Class @5-38D25C95

//Variables @5-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @5-09C8B1E7
    function clsRecordTABPBHSearch($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record TABPBHSearch/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "TABPBHSearch";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_MESREF = new clsControl(ccsListBox, "s_MESREF", "s_MESREF", ccsText, "", CCGetRequestParam("s_MESREF", $Method, NULL), $this);
            $this->s_MESREF->DSType = dsTable;
            $this->s_MESREF->DataSource = new clsDBFaturar();
            $this->s_MESREF->ds = & $this->s_MESREF->DataSource;
            $this->s_MESREF->DataSource->SQL = "SELECT * \n" .
"FROM TABPBH {SQL_Where} {SQL_OrderBy}";
            list($this->s_MESREF->BoundColumn, $this->s_MESREF->TextColumn, $this->s_MESREF->DBFormat) = array("MESREF", "MESREF", "");
            $this->Link1 = new clsControl(ccsLink, "Link1", "Link1", ccsText, "", CCGetRequestParam("Link1", $Method, NULL), $this);
            $this->Link1->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
            $this->Link1->Page = "ManutTabPBH.php";
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
        }
    }
//End Class_Initialize Event

//Validate Method @5-D19CAF25
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_MESREF->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->s_MESREF->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @5-5D0FD704
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_MESREF->Errors->Count());
        $errors = ($errors || $this->Link1->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @5-13CD4BBB
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "CadTabPBH.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                $Redirect = "CadTabPBH.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @5-570574CA
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->s_MESREF->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->s_MESREF->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Link1->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_MESREF->Show();
        $this->Link1->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End TABPBHSearch Class @5-FCB6E20C

class clsGridTABPBH { //TABPBH class @4-146A3106

//Variables @4-B7F4986A

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_MESREF;
    public $Sorter_VALPBH;
    public $Sorter1;
    public $Sorter2;
    public $Sorter3;
    public $Sorter4;
//End Variables

//Class_Initialize Event @4-C6178DED
    function clsGridTABPBH($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "TABPBH";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid TABPBH";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clsTABPBHDataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 10;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("TABPBHOrder", "");
        $this->SorterDirection = CCGetParam("TABPBHDir", "");

        $this->MESREF = new clsControl(ccsLink, "MESREF", "MESREF", ccsText, "", CCGetRequestParam("MESREF", ccsGet, NULL), $this);
        $this->MESREF->Page = "ManutTabPBH.php";
        $this->VALPBH = new clsControl(ccsLabel, "VALPBH", "VALPBH", ccsFloat, "", CCGetRequestParam("VALPBH", ccsGet, NULL), $this);
        $this->DATINC = new clsControl(ccsLabel, "DATINC", "DATINC", ccsDate, array("dd", "/", "mm", "/", "yyyy"), CCGetRequestParam("DATINC", ccsGet, NULL), $this);
        $this->CODINC = new clsControl(ccsLabel, "CODINC", "CODINC", ccsText, "", CCGetRequestParam("CODINC", ccsGet, NULL), $this);
        $this->DATALT = new clsControl(ccsLabel, "DATALT", "DATALT", ccsDate, array("dd", "/", "mm", "/", "yyyy"), CCGetRequestParam("DATALT", ccsGet, NULL), $this);
        $this->CODALT = new clsControl(ccsLabel, "CODALT", "CODALT", ccsText, "", CCGetRequestParam("CODALT", ccsGet, NULL), $this);
        $this->TABPBH_TotalRecords = new clsControl(ccsLabel, "TABPBH_TotalRecords", "TABPBH_TotalRecords", ccsText, "", CCGetRequestParam("TABPBH_TotalRecords", ccsGet, NULL), $this);
        $this->Sorter_MESREF = new clsSorter($this->ComponentName, "Sorter_MESREF", $FileName, $this);
        $this->Sorter_VALPBH = new clsSorter($this->ComponentName, "Sorter_VALPBH", $FileName, $this);
        $this->Sorter1 = new clsSorter($this->ComponentName, "Sorter1", $FileName, $this);
        $this->Sorter2 = new clsSorter($this->ComponentName, "Sorter2", $FileName, $this);
        $this->Sorter3 = new clsSorter($this->ComponentName, "Sorter3", $FileName, $this);
        $this->Sorter4 = new clsSorter($this->ComponentName, "Sorter4", $FileName, $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple, $this);
    }
//End Class_Initialize Event

//Initialize Method @4-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @4-6E7596A8
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urls_MESREF"] = CCGetFromGet("s_MESREF", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["MESREF"] = $this->MESREF->Visible;
            $this->ControlsVisible["VALPBH"] = $this->VALPBH->Visible;
            $this->ControlsVisible["DATINC"] = $this->DATINC->Visible;
            $this->ControlsVisible["CODINC"] = $this->CODINC->Visible;
            $this->ControlsVisible["DATALT"] = $this->DATALT->Visible;
            $this->ControlsVisible["CODALT"] = $this->CODALT->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->MESREF->SetValue($this->DataSource->MESREF->GetValue());
                $this->MESREF->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->MESREF->Parameters = CCAddParam($this->MESREF->Parameters, "MESREF", $this->DataSource->f("MESREF"));
                $this->VALPBH->SetValue($this->DataSource->VALPBH->GetValue());
                $this->DATINC->SetValue($this->DataSource->DATINC->GetValue());
                $this->CODINC->SetValue($this->DataSource->CODINC->GetValue());
                $this->DATALT->SetValue($this->DataSource->DATALT->GetValue());
                $this->CODALT->SetValue($this->DataSource->CODALT->GetValue());
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->MESREF->Show();
                $this->VALPBH->Show();
                $this->DATINC->Show();
                $this->CODINC->Show();
                $this->DATALT->Show();
                $this->CODALT->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        $this->TABPBH_TotalRecords->Show();
        $this->Sorter_MESREF->Show();
        $this->Sorter_VALPBH->Show();
        $this->Sorter1->Show();
        $this->Sorter2->Show();
        $this->Sorter3->Show();
        $this->Sorter4->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @4-588C3D0D
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->MESREF->Errors->ToString());
        $errors = ComposeStrings($errors, $this->VALPBH->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DATINC->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CODINC->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DATALT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CODALT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End TABPBH Class @4-FCB6E20C

class clsTABPBHDataSource extends clsDBFaturar {  //TABPBHDataSource Class @4-856149A1

//DataSource Variables @4-DEF1B446
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $MESREF;
    public $VALPBH;
    public $DATINC;
    public $CODINC;
    public $DATALT;
    public $CODALT;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-079CFC03
    function clsTABPBHDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid TABPBH";
        $this->Initialize();
        $this->MESREF = new clsField("MESREF", ccsText, "");
        $this->VALPBH = new clsField("VALPBH", ccsFloat, "");
        $this->DATINC = new clsField("DATINC", ccsDate, $this->DateFormat);
        $this->CODINC = new clsField("CODINC", ccsText, "");
        $this->DATALT = new clsField("DATALT", ccsDate, $this->DateFormat);
        $this->CODALT = new clsField("CODALT", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @4-951CE392
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "ano, MES";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_MESREF" => array("MESREF", ""), 
            "Sorter_VALPBH" => array("VALPBH", ""), 
            "Sorter1" => array("DATINC", ""), 
            "Sorter2" => array("CODINC", ""), 
            "Sorter3" => array("DATALT", ""), 
            "Sorter4" => array("CODALT", "")));
    }
//End SetOrder Method

//Prepare Method @4-B3C20470
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_MESREF", ccsText, "", "", $this->Parameters["urls_MESREF"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "MESREF", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @4-8262A372
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM TABPBH";
        $this->SQL = "SELECT MESREF, VALPBH, CODINC, CODALT, DATINC, DATALT, substr(mesref,4,4) AS ano, substr(mesref,1,2) AS mes \n\n" .
        "FROM TABPBH {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @4-7EA53057
    function SetValues()
    {
        $this->MESREF->SetDBValue($this->f("MESREF"));
        $this->VALPBH->SetDBValue(trim($this->f("VALPBH")));
        $this->DATINC->SetDBValue(trim($this->f("DATINC")));
        $this->CODINC->SetDBValue($this->f("CODINC"));
        $this->DATALT->SetDBValue(trim($this->f("DATALT")));
        $this->CODALT->SetDBValue($this->f("CODALT"));
    }
//End SetValues Method

} //End TABPBHDataSource Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-554EFEAE
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "CadTabPBH.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-946ECC7A
CCSecurityRedirect("1;2;3", "");
//End Authenticate User

//Include events file @1-2336B022
include("./CadTabPBH_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-E4A3881A
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$TABPBHSearch = new clsRecordTABPBHSearch("", $MainPage);
$TABPBH = new clsGridTABPBH("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->TABPBHSearch = & $TABPBHSearch;
$MainPage->TABPBH = & $TABPBH;
$MainPage->rodape = & $rodape;
$TABPBH->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-9BD5D900
$cabec->Operations();
$TABPBHSearch->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-CD050468
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($TABPBHSearch);
    unset($TABPBH);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-B6CA4AF1
$cabec->Show();
$TABPBHSearch->Show();
$TABPBH->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-9E487857
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($TABPBHSearch);
unset($TABPBH);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
