<?php
//BindEvents Method @1-5EF509CE
function BindEvents()
{
    global $TABPBH;
    global $CCSEvents;
    $TABPBH->TABPBH_TotalRecords->CCSEvents["BeforeShow"] = "TABPBH_TABPBH_TotalRecords_BeforeShow";
    $TABPBH->VALPBH->CCSEvents["BeforeShow"] = "TABPBH_VALPBH_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//TABPBH_TABPBH_TotalRecords_BeforeShow @8-BBDFD3A7
function TABPBH_TABPBH_TotalRecords_BeforeShow(& $sender)
{
    $TABPBH_TABPBH_TotalRecords_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABPBH; //Compatibility
//End TABPBH_TABPBH_TotalRecords_BeforeShow

//Retrieve number of records @9-ABE656B4
    $Component->SetValue($Container->DataSource->RecordsCount);
//End Retrieve number of records

//Close TABPBH_TABPBH_TotalRecords_BeforeShow @8-C6F94DE0
    return $TABPBH_TABPBH_TotalRecords_BeforeShow;
}
//End Close TABPBH_TABPBH_TotalRecords_BeforeShow

//TABPBH_VALPBH_BeforeShow @16-D01FCA7D
function TABPBH_VALPBH_BeforeShow(& $sender)
{
    $TABPBH_VALPBH_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABPBH; //Compatibility
//End TABPBH_VALPBH_BeforeShow

//Custom Code @39-2A29BDB7
// -------------------------
	$TABPBH->VALPBH->SetValue(number_format($TABPBH->VALPBH->GetValue(), 4,',','.'));
// -------------------------
//End Custom Code

//Close TABPBH_VALPBH_BeforeShow @16-4CB4B5C4
    return $TABPBH_VALPBH_BeforeShow;
}
//End Close TABPBH_VALPBH_BeforeShow

//Page_BeforeShow @1-6D4843EF
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CadTabPBH; //Compatibility
//End Page_BeforeShow

//Custom Code @40-2A29BDB7
// -------------------------
        
		include("controle_acesso.php");
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(15,14);
		controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow

//DEL  // -------------------------
//DEL  	$mReterInss = $TABPBH->VALPBH->GetValue();	
//DEL  	$mReterInss = str_replace(".", ",", $mReterInss);
//DEL  	$TABPBH->VALPBH->GetValue($mReterInss);
//DEL  
//DEL  // -------------------------


//C-2A29BDB7
// -------------------------
    // Write your own code here.
    //$mAno = substr($TABPBH->DATINC->Value,00,04);
	//$mMes = substr($TABPBH->DATINC->Value,05,02);
	//$mDia = substr($TABPBH->DATINC->Value,-02);  // Pega os dois �ltimos bytes, ou seja, o dia.
    //$TABPBH->DATINC->SetValue($mDia.'/'.$mMes.'/'.$mAno);
// -------------------------
//End Custom Code

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL      $mAno = substr($TABPBH->DATALT->Value,00,04);
//DEL  	$mMes = substr($TABPBH->DATALT->Value,05,02);
//DEL  	$mDia = substr($TABPBH->DATALT->Value,-02);  // Pega os dois �ltimos bytes, ou seja, o dia.
//DEL      $TABPBH->DATALT->SetValue($mDia.'/'.$mMes.'/'.$mAno);
//DEL  // -------------------------

?>
