<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="3" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="14" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="EQUSER_SUBSER_TABPBH" wizardCaption="Search EQUSER SUBSER TABPBH " wizardOrientation="Vertical" wizardFormMethod="post" returnPage="CadTabPreco.ccp" debugMode="False">
			<Components>
				<ListBox id="16" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="s_MESREF" wizardCaption="MESREF" wizardSize="7" wizardMaxLength="7" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="TABPBH" boundColumn="MESREF" textColumn="MESREF" orderBy="MESREF" editable="True" hasErrorCollection="True">
					<Components/>
					<Events>
						<Event name="BeforeBuildSelect" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="49"/>
							</Actions>
						</Event>
					</Events>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="44" tableName="TABPBH" posLeft="10" posTop="10" posWidth="115" posHeight="168"/>
					</JoinTables>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<Link id="45" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link1" hrefSource="ManutTabPrecos.ccp" wizardUseTemplateBlock="False" editable="False">
					<Components/>
					<Events/>
					<LinkParameters/>
					<Attributes/>
					<Features/>
				</Link>
				<Button id="15" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="4" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="Faturar" dataSource="EQUSER, SUBSER, TABPBH" name="EQUSER_SUBSER_TABPBH1" pageSizeLimit="100" wizardCaption="List of EQUSER, SUBSER, TABPBH " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="Nenhuma Equvalência de Serviço foi encontrada">
			<Components>
				<Sorter id="18" visible="True" name="Sorter_MESREF" column="EQUSER.MESREF" wizardCaption="MESREF" wizardSortingType="SimpleDir" wizardControl="MESREF" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="19" visible="True" name="Sorter_DESSUB" column="DESSUB" wizardCaption="DESSUB" wizardSortingType="SimpleDir" wizardControl="DESSUB" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="20" visible="True" name="Sorter_EQUPBH" column="EQUPBH" wizardCaption="EQUPBH" wizardSortingType="SimpleDir" wizardControl="EQUPBH" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="21" visible="True" name="Sorter_VALEQU" column="VALEQU" wizardCaption="VALEQU" wizardSortingType="SimpleDir" wizardControl="VALEQU" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="22" visible="True" name="Sorter_VALPBH" column="VALPBH" wizardCaption="VALPBH" wizardSortingType="SimpleDir" wizardControl="VALPBH" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="25" visible="True" name="Sorter_DATINC" column="EQUSER.DATINC" wizardCaption="DATINC" wizardSortingType="SimpleDir" wizardControl="DATINC" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="23" visible="True" name="Sorter_CODINC" column="EQUSER.CODINC" wizardCaption="CODINC" wizardSortingType="SimpleDir" wizardControl="CODINC" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="26" visible="True" name="Sorter_DATALT" column="EQUSER.DATALT" wizardCaption="DATALT" wizardSortingType="SimpleDir" wizardControl="DATALT" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="24" visible="True" name="Sorter_CODALT" column="EQUSER.CODALT" wizardCaption="CODALT" wizardSortingType="SimpleDir" wizardControl="CODALT" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Link id="28" fieldSourceType="DBColumn" dataType="Text" html="False" name="MESREF" fieldSource="MESREF" wizardCaption="MESREF" wizardSize="7" wizardMaxLength="7" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="ManutTabPrecosAlter.ccp" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="47" sourceType="DataField" name="MESREF" source="MESREF"/>
						<LinkParameter id="48" sourceType="DataField" name="SUBSER" source="SUBSER"/>
					</LinkParameters>
				</Link>
				<Label id="29" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESSUB" fieldSource="DESSUB" wizardCaption="DESSUB" wizardSize="50" wizardMaxLength="75" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="31" fieldSourceType="DBColumn" dataType="Float" html="False" name="EQUPBH" fieldSource="EQUPBH" wizardCaption="EQUPBH" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="55"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="33" fieldSourceType="DBColumn" dataType="Float" html="False" name="VALEQU" fieldSource="VALEQU" wizardCaption="VALEQU" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="54"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="34" fieldSourceType="DBColumn" dataType="Float" html="False" name="VALPBH" fieldSource="VALPBH" wizardCaption="VALPBH" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="53"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="40" fieldSourceType="DBColumn" dataType="Date" html="False" name="DATINC" fieldSource="DATINC" wizardCaption="DATINC" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False" format="dd/mm/yyyy">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="36" fieldSourceType="DBColumn" dataType="Text" html="False" name="CODINC" fieldSource="CODINC" wizardCaption="CODINC" wizardSize="3" wizardMaxLength="3" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="42" fieldSourceType="DBColumn" dataType="Date" html="False" name="DATALT" fieldSource="DATALT" wizardCaption="DATALT" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False" format="dd/mm/yyyy">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="38" fieldSourceType="DBColumn" dataType="Text" html="False" name="CODALT" fieldSource="CODALT" wizardCaption="CODALT" wizardSize="3" wizardMaxLength="3" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="43" size="10" name="Navigator" wizardPagingType="Custom" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardImages="Images" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="True" wizardOfText="of" wizardImagesScheme="Apricot" type="Simple">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="17" conditionType="Parameter" useIsNull="False" field="EQUSER.MESREF" parameterSource="s_MESREF" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="1"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="5" tableName="EQUSER" posLeft="231" posTop="38" posWidth="115" posHeight="180"/>
				<JoinTable id="6" tableName="SUBSER" posLeft="53" posTop="38" posWidth="115" posHeight="152"/>
				<JoinTable id="7" tableName="TABPBH" posLeft="398" posTop="54" posWidth="115" posHeight="168"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="8" tableLeft="SUBSER" tableRight="EQUSER" fieldLeft="SUBSER.SUBSER" fieldRight="EQUSER.SUBSER" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="9" tableLeft="TABPBH" tableRight="EQUSER" fieldLeft="TABPBH.MESREF" fieldRight="EQUSER.MESREF" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="10" tableName="EQUSER" fieldName="EQUSER.*"/>
				<Field id="12" tableName="TABPBH" fieldName="VALPBH"/>
				<Field id="13" tableName="SUBSER" fieldName="DESSUB"/>
				<Field id="27" tableName="EQUSER" fieldName="MESREF" alias="MESREF"/>
				<Field id="30" tableName="EQUSER" fieldName="EQUPBH" alias="EQUPBH"/>
				<Field id="32" tableName="EQUSER" fieldName="VALEQU" alias="VALEQU"/>
				<Field id="35" tableName="EQUSER" fieldName="CODINC" alias="CODINC"/>
				<Field id="37" tableName="EQUSER" fieldName="CODALT" alias="CODALT"/>
				<Field id="39" tableName="EQUSER" fieldName="DATINC" alias="DATINC"/>
				<Field id="41" tableName="EQUSER" fieldName="DATALT" alias="DATALT"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="2" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="CadTabPreco.php" forShow="True" url="CadTabPreco.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="CadTabPreco_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="50" groupID="1"/>
		<Group id="51" groupID="2"/>
		<Group id="52" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="56"/>
			</Actions>
		</Event>
	</Events>
</Page>
