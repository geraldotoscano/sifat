<?php
//Include Common Files @1-1CC363C7
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "CadTabPreco.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @3-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordEQUSER_SUBSER_TABPBH { //EQUSER_SUBSER_TABPBH Class @14-1EC82295

//Variables @14-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @14-25F92CBD
    function clsRecordEQUSER_SUBSER_TABPBH($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record EQUSER_SUBSER_TABPBH/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "EQUSER_SUBSER_TABPBH";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_MESREF = new clsControl(ccsListBox, "s_MESREF", "s_MESREF", ccsText, "", CCGetRequestParam("s_MESREF", $Method, NULL), $this);
            $this->s_MESREF->DSType = dsTable;
            $this->s_MESREF->DataSource = new clsDBFaturar();
            $this->s_MESREF->ds = & $this->s_MESREF->DataSource;
            $this->s_MESREF->DataSource->SQL = "SELECT * \n" .
"FROM TABPBH {SQL_Where} {SQL_OrderBy}";
            $this->s_MESREF->DataSource->Order = "MESREF";
            list($this->s_MESREF->BoundColumn, $this->s_MESREF->TextColumn, $this->s_MESREF->DBFormat) = array("MESREF", "MESREF", "");
            $this->s_MESREF->DataSource->Order = "MESREF";
            $this->Link1 = new clsControl(ccsLink, "Link1", "Link1", ccsText, "", CCGetRequestParam("Link1", $Method, NULL), $this);
            $this->Link1->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
            $this->Link1->Page = "ManutTabPrecos.php";
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
        }
    }
//End Class_Initialize Event

//Validate Method @14-D19CAF25
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_MESREF->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->s_MESREF->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @14-5D0FD704
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_MESREF->Errors->Count());
        $errors = ($errors || $this->Link1->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @14-547D6FC3
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "CadTabPreco.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                $Redirect = "CadTabPreco.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @14-570574CA
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->s_MESREF->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->s_MESREF->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Link1->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_MESREF->Show();
        $this->Link1->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End EQUSER_SUBSER_TABPBH Class @14-FCB6E20C

class clsGridEQUSER_SUBSER_TABPBH1 { //EQUSER_SUBSER_TABPBH1 class @4-B2103AA9

//Variables @4-3353A561

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_MESREF;
    public $Sorter_DESSUB;
    public $Sorter_EQUPBH;
    public $Sorter_VALEQU;
    public $Sorter_VALPBH;
    public $Sorter_DATINC;
    public $Sorter_CODINC;
    public $Sorter_DATALT;
    public $Sorter_CODALT;
//End Variables

//Class_Initialize Event @4-5E8322DA
    function clsGridEQUSER_SUBSER_TABPBH1($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "EQUSER_SUBSER_TABPBH1";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid EQUSER_SUBSER_TABPBH1";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clsEQUSER_SUBSER_TABPBH1DataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 10;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("EQUSER_SUBSER_TABPBH1Order", "");
        $this->SorterDirection = CCGetParam("EQUSER_SUBSER_TABPBH1Dir", "");

        $this->MESREF = new clsControl(ccsLink, "MESREF", "MESREF", ccsText, "", CCGetRequestParam("MESREF", ccsGet, NULL), $this);
        $this->MESREF->Page = "ManutTabPrecosAlter.php";
        $this->DESSUB = new clsControl(ccsLabel, "DESSUB", "DESSUB", ccsText, "", CCGetRequestParam("DESSUB", ccsGet, NULL), $this);
        $this->EQUPBH = new clsControl(ccsLabel, "EQUPBH", "EQUPBH", ccsFloat, "", CCGetRequestParam("EQUPBH", ccsGet, NULL), $this);
        $this->VALEQU = new clsControl(ccsLabel, "VALEQU", "VALEQU", ccsFloat, "", CCGetRequestParam("VALEQU", ccsGet, NULL), $this);
        $this->VALPBH = new clsControl(ccsLabel, "VALPBH", "VALPBH", ccsFloat, "", CCGetRequestParam("VALPBH", ccsGet, NULL), $this);
        $this->DATINC = new clsControl(ccsLabel, "DATINC", "DATINC", ccsDate, array("dd", "/", "mm", "/", "yyyy"), CCGetRequestParam("DATINC", ccsGet, NULL), $this);
        $this->CODINC = new clsControl(ccsLabel, "CODINC", "CODINC", ccsText, "", CCGetRequestParam("CODINC", ccsGet, NULL), $this);
        $this->DATALT = new clsControl(ccsLabel, "DATALT", "DATALT", ccsDate, array("dd", "/", "mm", "/", "yyyy"), CCGetRequestParam("DATALT", ccsGet, NULL), $this);
        $this->CODALT = new clsControl(ccsLabel, "CODALT", "CODALT", ccsText, "", CCGetRequestParam("CODALT", ccsGet, NULL), $this);
        $this->Sorter_MESREF = new clsSorter($this->ComponentName, "Sorter_MESREF", $FileName, $this);
        $this->Sorter_DESSUB = new clsSorter($this->ComponentName, "Sorter_DESSUB", $FileName, $this);
        $this->Sorter_EQUPBH = new clsSorter($this->ComponentName, "Sorter_EQUPBH", $FileName, $this);
        $this->Sorter_VALEQU = new clsSorter($this->ComponentName, "Sorter_VALEQU", $FileName, $this);
        $this->Sorter_VALPBH = new clsSorter($this->ComponentName, "Sorter_VALPBH", $FileName, $this);
        $this->Sorter_DATINC = new clsSorter($this->ComponentName, "Sorter_DATINC", $FileName, $this);
        $this->Sorter_CODINC = new clsSorter($this->ComponentName, "Sorter_CODINC", $FileName, $this);
        $this->Sorter_DATALT = new clsSorter($this->ComponentName, "Sorter_DATALT", $FileName, $this);
        $this->Sorter_CODALT = new clsSorter($this->ComponentName, "Sorter_CODALT", $FileName, $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple, $this);
    }
//End Class_Initialize Event

//Initialize Method @4-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @4-3C9A24E4
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urls_MESREF"] = CCGetFromGet("s_MESREF", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["MESREF"] = $this->MESREF->Visible;
            $this->ControlsVisible["DESSUB"] = $this->DESSUB->Visible;
            $this->ControlsVisible["EQUPBH"] = $this->EQUPBH->Visible;
            $this->ControlsVisible["VALEQU"] = $this->VALEQU->Visible;
            $this->ControlsVisible["VALPBH"] = $this->VALPBH->Visible;
            $this->ControlsVisible["DATINC"] = $this->DATINC->Visible;
            $this->ControlsVisible["CODINC"] = $this->CODINC->Visible;
            $this->ControlsVisible["DATALT"] = $this->DATALT->Visible;
            $this->ControlsVisible["CODALT"] = $this->CODALT->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->MESREF->SetValue($this->DataSource->MESREF->GetValue());
                $this->MESREF->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->MESREF->Parameters = CCAddParam($this->MESREF->Parameters, "MESREF", $this->DataSource->f("MESREF"));
                $this->MESREF->Parameters = CCAddParam($this->MESREF->Parameters, "SUBSER", $this->DataSource->f("SUBSER"));
                $this->DESSUB->SetValue($this->DataSource->DESSUB->GetValue());
                $this->EQUPBH->SetValue($this->DataSource->EQUPBH->GetValue());
                $this->VALEQU->SetValue($this->DataSource->VALEQU->GetValue());
                $this->VALPBH->SetValue($this->DataSource->VALPBH->GetValue());
                $this->DATINC->SetValue($this->DataSource->DATINC->GetValue());
                $this->CODINC->SetValue($this->DataSource->CODINC->GetValue());
                $this->DATALT->SetValue($this->DataSource->DATALT->GetValue());
                $this->CODALT->SetValue($this->DataSource->CODALT->GetValue());
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->MESREF->Show();
                $this->DESSUB->Show();
                $this->EQUPBH->Show();
                $this->VALEQU->Show();
                $this->VALPBH->Show();
                $this->DATINC->Show();
                $this->CODINC->Show();
                $this->DATALT->Show();
                $this->CODALT->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        $this->Sorter_MESREF->Show();
        $this->Sorter_DESSUB->Show();
        $this->Sorter_EQUPBH->Show();
        $this->Sorter_VALEQU->Show();
        $this->Sorter_VALPBH->Show();
        $this->Sorter_DATINC->Show();
        $this->Sorter_CODINC->Show();
        $this->Sorter_DATALT->Show();
        $this->Sorter_CODALT->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @4-8317B73A
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->MESREF->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESSUB->Errors->ToString());
        $errors = ComposeStrings($errors, $this->EQUPBH->Errors->ToString());
        $errors = ComposeStrings($errors, $this->VALEQU->Errors->ToString());
        $errors = ComposeStrings($errors, $this->VALPBH->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DATINC->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CODINC->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DATALT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CODALT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End EQUSER_SUBSER_TABPBH1 Class @4-FCB6E20C

class clsEQUSER_SUBSER_TABPBH1DataSource extends clsDBFaturar {  //EQUSER_SUBSER_TABPBH1DataSource Class @4-8BF055F4

//DataSource Variables @4-AC755613
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $MESREF;
    public $DESSUB;
    public $EQUPBH;
    public $VALEQU;
    public $VALPBH;
    public $DATINC;
    public $CODINC;
    public $DATALT;
    public $CODALT;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-4E5CB744
    function clsEQUSER_SUBSER_TABPBH1DataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid EQUSER_SUBSER_TABPBH1";
        $this->Initialize();
        $this->MESREF = new clsField("MESREF", ccsText, "");
        $this->DESSUB = new clsField("DESSUB", ccsText, "");
        $this->EQUPBH = new clsField("EQUPBH", ccsFloat, "");
        $this->VALEQU = new clsField("VALEQU", ccsFloat, "");
        $this->VALPBH = new clsField("VALPBH", ccsFloat, "");
        $this->DATINC = new clsField("DATINC", ccsDate, $this->DateFormat);
        $this->CODINC = new clsField("CODINC", ccsText, "");
        $this->DATALT = new clsField("DATALT", ccsDate, $this->DateFormat);
        $this->CODALT = new clsField("CODALT", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @4-F808E27F
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_MESREF" => array("EQUSER.MESREF", ""), 
            "Sorter_DESSUB" => array("DESSUB", ""), 
            "Sorter_EQUPBH" => array("EQUPBH", ""), 
            "Sorter_VALEQU" => array("VALEQU", ""), 
            "Sorter_VALPBH" => array("VALPBH", ""), 
            "Sorter_DATINC" => array("EQUSER.DATINC", ""), 
            "Sorter_CODINC" => array("EQUSER.CODINC", ""), 
            "Sorter_DATALT" => array("EQUSER.DATALT", ""), 
            "Sorter_CODALT" => array("EQUSER.CODALT", "")));
    }
//End SetOrder Method

//Prepare Method @4-8E30A7C8
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_MESREF", ccsText, "", "", $this->Parameters["urls_MESREF"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "EQUSER.MESREF", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->Where = 
             $this->wp->Criterion[1];
        $this->Where = $this->wp->opAND(false, "( (SUBSER.SUBSER = EQUSER.SUBSER) AND (TABPBH.MESREF = EQUSER.MESREF) )", $this->Where);
    }
//End Prepare Method

//Open Method @4-D640E99D
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM EQUSER,\n\n" .
        "SUBSER,\n\n" .
        "TABPBH";
        $this->SQL = "SELECT EQUSER.*, VALPBH, DESSUB \n\n" .
        "FROM EQUSER,\n\n" .
        "SUBSER,\n\n" .
        "TABPBH {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @4-201AF341
    function SetValues()
    {
        $this->MESREF->SetDBValue($this->f("MESREF"));
        $this->DESSUB->SetDBValue($this->f("DESSUB"));
        $this->EQUPBH->SetDBValue(trim($this->f("EQUPBH")));
        $this->VALEQU->SetDBValue(trim($this->f("VALEQU")));
        $this->VALPBH->SetDBValue(trim($this->f("VALPBH")));
        $this->DATINC->SetDBValue(trim($this->f("DATINC")));
        $this->CODINC->SetDBValue($this->f("CODINC"));
        $this->DATALT->SetDBValue(trim($this->f("DATALT")));
        $this->CODALT->SetDBValue($this->f("CODALT"));
    }
//End SetValues Method

} //End EQUSER_SUBSER_TABPBH1DataSource Class @4-FCB6E20C

//Include Page implementation @2-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-0760C705
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "CadTabPreco.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-946ECC7A
CCSecurityRedirect("1;2;3", "");
//End Authenticate User

//Include events file @1-8B1D35E9
include("./CadTabPreco_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-CBDA9F12
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$EQUSER_SUBSER_TABPBH = new clsRecordEQUSER_SUBSER_TABPBH("", $MainPage);
$EQUSER_SUBSER_TABPBH1 = new clsGridEQUSER_SUBSER_TABPBH1("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->EQUSER_SUBSER_TABPBH = & $EQUSER_SUBSER_TABPBH;
$MainPage->EQUSER_SUBSER_TABPBH1 = & $EQUSER_SUBSER_TABPBH1;
$MainPage->rodape = & $rodape;
$EQUSER_SUBSER_TABPBH1->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-40F888BA
$cabec->Operations();
$EQUSER_SUBSER_TABPBH->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-A706677F
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($EQUSER_SUBSER_TABPBH);
    unset($EQUSER_SUBSER_TABPBH1);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-1E4A3B8D
$cabec->Show();
$EQUSER_SUBSER_TABPBH->Show();
$EQUSER_SUBSER_TABPBH1->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-F1A2F5DC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($EQUSER_SUBSER_TABPBH);
unset($EQUSER_SUBSER_TABPBH1);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
