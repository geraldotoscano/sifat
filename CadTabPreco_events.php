<?php
//BindEvents Method @1-95F93A1A
function BindEvents()
{
    global $EQUSER_SUBSER_TABPBH;
    global $EQUSER_SUBSER_TABPBH1;
    global $CCSEvents;
    $EQUSER_SUBSER_TABPBH->s_MESREF->ds->CCSEvents["BeforeBuildSelect"] = "EQUSER_SUBSER_TABPBH_s_MESREF_ds_BeforeBuildSelect";
    $EQUSER_SUBSER_TABPBH1->EQUPBH->CCSEvents["BeforeShow"] = "EQUSER_SUBSER_TABPBH1_EQUPBH_BeforeShow";
    $EQUSER_SUBSER_TABPBH1->VALEQU->CCSEvents["BeforeShow"] = "EQUSER_SUBSER_TABPBH1_VALEQU_BeforeShow";
    $EQUSER_SUBSER_TABPBH1->VALPBH->CCSEvents["BeforeShow"] = "EQUSER_SUBSER_TABPBH1_VALPBH_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//EQUSER_SUBSER_TABPBH_s_MESREF_ds_BeforeBuildSelect @16-995CC1E6
function EQUSER_SUBSER_TABPBH_s_MESREF_ds_BeforeBuildSelect(& $sender)
{
    $EQUSER_SUBSER_TABPBH_s_MESREF_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $EQUSER_SUBSER_TABPBH; //Compatibility
//End EQUSER_SUBSER_TABPBH_s_MESREF_ds_BeforeBuildSelect

//Custom Code @49-2A29BDB7
// -------------------------
    // Write your own code here.
	$EQUSER_SUBSER_TABPBH->s_MESREF->DataSource->Order = "substr(mesref,4,4),substr(mesref,1,2)";
// -------------------------
//End Custom Code

//Close EQUSER_SUBSER_TABPBH_s_MESREF_ds_BeforeBuildSelect @16-565BCECA
    return $EQUSER_SUBSER_TABPBH_s_MESREF_ds_BeforeBuildSelect;
}
//End Close EQUSER_SUBSER_TABPBH_s_MESREF_ds_BeforeBuildSelect

//EQUSER_SUBSER_TABPBH1_EQUPBH_BeforeShow @31-B0713A3F
function EQUSER_SUBSER_TABPBH1_EQUPBH_BeforeShow(& $sender)
{
    $EQUSER_SUBSER_TABPBH1_EQUPBH_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $EQUSER_SUBSER_TABPBH1; //Compatibility
//End EQUSER_SUBSER_TABPBH1_EQUPBH_BeforeShow

//Custom Code @55-2A29BDB7
// -------------------------
	$EQUSER_SUBSER_TABPBH1->EQUPBH->SetValue(number_format($EQUSER_SUBSER_TABPBH1->EQUPBH->GetValue(), 2,',','.'));
// -------------------------
//End Custom Code

//Close EQUSER_SUBSER_TABPBH1_EQUPBH_BeforeShow @31-28DC356F
    return $EQUSER_SUBSER_TABPBH1_EQUPBH_BeforeShow;
}
//End Close EQUSER_SUBSER_TABPBH1_EQUPBH_BeforeShow

//EQUSER_SUBSER_TABPBH1_VALEQU_BeforeShow @33-02E4190F
function EQUSER_SUBSER_TABPBH1_VALEQU_BeforeShow(& $sender)
{
    $EQUSER_SUBSER_TABPBH1_VALEQU_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $EQUSER_SUBSER_TABPBH1; //Compatibility
//End EQUSER_SUBSER_TABPBH1_VALEQU_BeforeShow

//Custom Code @54-2A29BDB7
// -------------------------
	$EQUSER_SUBSER_TABPBH1->VALEQU->SetValue(number_format($EQUSER_SUBSER_TABPBH1->VALEQU->GetValue(), 2,',','.'));
// -------------------------
//End Custom Code

//Close EQUSER_SUBSER_TABPBH1_VALEQU_BeforeShow @33-D8A593FD
    return $EQUSER_SUBSER_TABPBH1_VALEQU_BeforeShow;
}
//End Close EQUSER_SUBSER_TABPBH1_VALEQU_BeforeShow

//EQUSER_SUBSER_TABPBH1_VALPBH_BeforeShow @34-8D61E9D4
function EQUSER_SUBSER_TABPBH1_VALPBH_BeforeShow(& $sender)
{
    $EQUSER_SUBSER_TABPBH1_VALPBH_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $EQUSER_SUBSER_TABPBH1; //Compatibility
//End EQUSER_SUBSER_TABPBH1_VALPBH_BeforeShow

//Custom Code @53-2A29BDB7
// -------------------------
	$EQUSER_SUBSER_TABPBH1->VALPBH->SetValue(number_format($EQUSER_SUBSER_TABPBH1->VALPBH->GetValue(), 4,',','.'));
// -------------------------
//End Custom Code

//Close EQUSER_SUBSER_TABPBH1_VALPBH_BeforeShow @34-C08CC974
    return $EQUSER_SUBSER_TABPBH1_VALPBH_BeforeShow;
}
//End Close EQUSER_SUBSER_TABPBH1_VALPBH_BeforeShow

//Page_BeforeShow @1-D2C0460F
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CadTabPreco; //Compatibility
//End Page_BeforeShow

//Custom Code @56-2A29BDB7
// -------------------------

        include("controle_acesso.php");
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(17,16);
        controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
