<?php
        
//File Description @0-916F68BD
//======================================================
//
//  This file contains the following classes:
//      class clsSQLParameters
//      class clsSQLParameter
//      class clsControl
//      class clsField
//      class clsButton
//      class clsPanel
//      class clsFileUpload
//      class clsDatePicker
//      class clsErrors
//      class clsSection
//      class clsLocaleInfo
//      class clsLocale
//      class clsLocales
//      class clsAttribute
//      class clsAttributes
//
//======================================================
//End File Description

//Constant List @0-033DA0AC

// ------- Controls ---------------
define("ccsLabel",           1);
define("ccsLink",            2);
define("ccsTextBox",         3);
define("ccsTextArea",        4);
define("ccsListBox",         5);
define("ccsRadioButton",     6);
define("ccsButton",          7);
define("ccsCheckBox",        8);
define("ccsImage",           9);
define("ccsImageLink",       10);
define("ccsHidden",          11);
define("ccsCheckBoxList",    12);
define("ccsDatePicker",      13);
define("ccsReportLabel",     14);
define("ccsReportPageBreak", 15);

$ControlTypes = array(
  "", "Label","Link","TextBox","TextArea","ListBox","RadioButton",
  "Button","CheckBox","Image","ImageLink","Hidden","CheckBoxList",
  "DatePicker", "ReportLabel","ReportPageBreak"
);


// ------- Operators --------------
define("opEqual",              1);
define("opNotEqual",           2);
define("opLessThan",           3);
define("opLessThanOrEqual",    4);
define("opGreaterThan",        5);
define("opGreaterThanOrEqual", 6);
define("opBeginsWith",         7);
define("opNotBeginsWith",      8);
define("opEndsWith",           9);
define("opNotEndsWith",        10);
define("opContains",           11);
define("opNotContains",        12);
define("opIsNull",             13);
define("opNotNull",            14);
define("opIn",                 15);
define("opBetween",            16);
define("opNotIn",              17);
define("opNotBetween",         18);

// ------- Datasource types -------
define("dsTable",        1);
define("dsSQL",          2);
define("dsProcedure",    3);
define("dsListOfValues", 4);
define("dsEmpty",        5);

// ------- CheckBox states --------
define("ccsChecked", true);
define("ccsUnchecked", false);


//End Constant List

//CCCheckValue @0-962BACE6
function CCCheckValue($Value, $DataType)
{
  $result = false;
  if($DataType == ccsInteger)
    $result = is_int($Value); 
  else if($DataType == ccsFloat)
    $result = is_float($Value);
  else if($DataType == ccsDate)
    $result = (is_array($Value) || is_int($Value));
  else if($DataType == ccsBoolean)
    $result = is_bool($Value); 
  return $result;
}
//End CCCheckValue

//clsSQLParameters Class @0-F61B52A5

class clsSQLParameters
{
  
  public $Connection;
  public $Criterion;
  public $AssembledWhere;
  public $Errors;
  public $DataSource;
  public $AllParametersSet;
  public $ErrorBlock;

  public $Parameters1;

  function clsSQLParameters($ErrorBlock = "")
  {
    $this->ErrorBlock = $ErrorBlock;
  }

  function SetParameters($Name, $NewParameter)
  {
    $this->Parameters[$Name] = $NewParameter;
  }

  function AddParameter($ParameterID, $ParameterSource, $DataType, $Format, $DBFormat, $InitValue, $DefaultValue, $UseIsNull = false)
  {
    $this->Parameters[$ParameterID] = new clsSQLParameter($ParameterSource, $DataType, $Format, $DBFormat, $InitValue, $DefaultValue, $UseIsNull, $this->ErrorBlock);
  }

  function AllParamsSet()
  {
    $blnResult = true;

    if(isset($this->Parameters) && is_array($this->Parameters))
    {
      reset($this->Parameters);
      while ($blnResult && list ($key, $Parameter) = each ($this->Parameters)) 
      {
        if($Parameter->GetValue() === "" && $Parameter->GetValue() !== false && $Parameter->UseIsNull === false)
          $blnResult = false;
      }
    }
     return $blnResult;
  }

  function GetDBValue($ParameterID)
  {
    return $this->Parameters[$ParameterID]->GetDBValue();
  }

  function opAND($Brackets, $strLeft, $strRight)
  {
    $strResult = "";
    if (strlen($strLeft))
    {
      if (strlen($strRight)) 
      {
        $strResult = $strLeft . " AND " . $strRight;
        if ($Brackets) 
          $strResult = " (" . $strResult . ") ";
      }
      else
      {
        $strResult = $strLeft;
      }
    }
    else
    {
      if (strlen($strRight)) 
        $strResult = $strRight;
    }
    return $strResult;
  }

  function opOR($Brackets, $strLeft, $strRight)
  {
    $strResult = "";
    if (strlen($strLeft))
    {
      if (strlen($strRight))
      {
        $strResult = $strLeft . " OR " . $strRight;
        if ($Brackets) 
          $strResult = " (" . $strResult . ") ";
      }
      else
      {
        $strResult = $strLeft;
      }
    }
    else
    {
      if (strlen($strRight))
        $strResult = $strRight;
    }
    return $strResult;
  }

  function Operation($Operation, $FieldName, $DBValue, $SQLText, $UseIsNull = false)
  {
    $Result = "";

    if((is_array($DBValue) && count($DBValue)) && (!is_array($SQLText) || count($SQLText)) || (!is_array($DBValue) && (strlen($DBValue) || $DBValue === false)))
    {
      $SQLTextVal = $SQLValue = is_array($SQLText) ? $SQLText[0] : $SQLText;
      if(!is_array($DBValue) && CCSubStr($SQLValue, 0, 1) == "'")
        $SQLValue = CCSubStr($SQLValue, 1, CCStrLen($SQLValue) - 2);

      switch ($Operation)
      {
        case opEqual:
          $Result = $FieldName . " = " . $SQLTextVal;
          break;
        case opNotEqual:
          $Result = $FieldName . " <> " . $SQLTextVal;
          break;
        case opLessThan:
          $Result = $FieldName . " < " . $SQLTextVal;
          break;
        case opLessThanOrEqual:
          $Result = $FieldName . " <= " . $SQLTextVal;
          break;
        case opGreaterThan:
          $Result = $FieldName . " > " . $SQLTextVal;
          break;
        case opGreaterThanOrEqual:
          $Result = $FieldName . " >= " . $SQLTextVal;
          break;                                
        case opBeginsWith:
          $Result = $FieldName . " like '" . $SQLValue . "%'";
          break;
        case opNotBeginsWith:
          $Result = $FieldName . " not like '" . $SQLValue . "%'";
          break;
        case opEndsWith:
          $Result = $FieldName . " like '%" . $SQLValue . "'";
          break;
        case opNotEndsWith:
          $Result = $FieldName . " not like '%" . $SQLValue . "'";
          break;
        case opContains:
          $Result = $FieldName . " like '%" . $SQLValue . "%'";
          break;
        case opNotContains:
          $Result = $FieldName . " not like '%" . $SQLValue . "%'";
          break;
        case opIsNull:
          $Result = $FieldName . " IS NULL";
          break;
        case opNotNull:
          $Result = $FieldName . " IS NOT NULL";
          break;
        case opIn:
          if (is_array($SQLText)) 
            $Result = $FieldName . " IN (" .  implode(", ", $SQLText) . ")";
          else
            $Result = $FieldName . " IN (" .  $SQLText . ")";
          break;
        case opBetween:
          if (is_array($SQLText) && count($SQLText) > 1) 
            $Result = $FieldName . " BETWEEN " .  $SQLText[0] . " AND " . $SQLText[1];
          elseif (is_array($SQLText)) 
            $Result = $FieldName . " BETWEEN " .  $SQLText[0] . " AND " . $SQLText[0];
          else
            $Result = $FieldName . " BETWEEN " .  $SQLText . " AND " . $SQLText;
          break;
        case opNotIn:
          if (is_array($SQLText)) 
            $Result = $FieldName . " NOT IN (" .  implode(", ", $SQLText) . ")";
          else
            $Result = $FieldName . " NOT IN (" .  $SQLText . ")";
          break;
        case opNotBetween:
          if (is_array($SQLText) && count($SQLText) > 1) 
            $Result = $FieldName . " NOT BETWEEN " .  $SQLText[0] . " AND " . $SQLText[1];
          elseif (is_array($SQLText)) 
            $Result = $FieldName . " NOT BETWEEN " .  $SQLText[0] . " AND " . $SQLText[0];
          else
            $Result = $FieldName . " NOT BETWEEN " .  $SQLText . " AND " . $SQLText;
          break;

      }
    } 
    else if ($UseIsNull) 
    {
      switch ($Operation)
      {
        case opEqual:
        case opLessThan:
        case opLessThanOrEqual:
        case opGreaterThan:
        case opGreaterThanOrEqual:
        case opBeginsWith:
        case opEndsWith:
        case opContains:
        case opIsNull:
        case opIn:
          $Result = $FieldName . " IS NULL";
          break;
        case opNotEqual:
        case opNotEndsWith:
        case opNotBeginsWith:
        case opNotContains:
        case opNotNull:
          $Result = $FieldName . " IS NOT NULL";
          break;
      }

    }

    return $Result;
  }
}
//End clsSQLParameters Class

//clsSQLParameter Class @0-D977F2EE
class clsSQLParameter
{
  public $Errors;
  public $DataType;
  public $Format;
  public $DBFormat;
  public $Link;
  public $Caption;
  public $ErrorBlock;
  public $UseIsNull;

  public $Value = "";
  public $IsNull = true;
  public $DBValue;
  public $Text;
  

  function clsSQLParameter($ParameterSource, $DataType, $Format, $DBFormat, $InitValue, $DefaultValue, $UseIsNull = false, $ErrorBlock = "")
  {
    $this->Value = NULL;

    $this->Errors = new clsErrors();
    $this->ErrorBlock = $ErrorBlock;
    $this->UseIsNull = $UseIsNull;

    $this->Caption = $ParameterSource;
    $this->DataType = $DataType;
    $this->Format = $Format;
    $this->DBFormat = $DBFormat;
    if(is_array($InitValue) || strlen($InitValue))
      $this->SetText($InitValue);
    else if(!is_null($DefaultValue))
      $this->SetText($DefaultValue);
  }

  function GetParsedValue($ParsingValue, $Format, $isDbFormat = false)
  {
    global $Tpl;
    global $CCSLocales;
    $varResult = "";

    if (strlen($ParsingValue))
    {
      switch ($this->DataType)
      {
        case ccsDate:
          $DateValidation = true;
          if (CCValidateDateMask($ParsingValue, $Format)) {
            $varResult = CCParseDate($ParsingValue, $Format);
            if(!$varResult || !CCValidateDate($varResult))
            {
              $DateValidation = false;
              $varResult = "";
            }
          } else {
            $DateValidation = false;
          }
          if(!$DateValidation) {
            if (is_array($Format)) {
              $FormatString = join("", $Format);
            } else {
              $FormatString = $Format;
            }
            $this->Errors->addError($CCSLocales->GetText('CCS_IncorrectFormat', array($this->Caption, $FormatString)));
          }
          break;
        case ccsBoolean:
          if (CCValidateBoolean($ParsingValue, $Format))
            $varResult = CCParseBoolean($ParsingValue, $Format);
          else
          {
            if (is_array($Format)) {
              $FormatString = CCGetBooleanFormat($Format);;
            } else {
              $FormatString = $Format;
            }
            $this->Errors->addError($CCSLocales->GetText('CCS_IncorrectFormat', array($this->Caption, $FormatString)));
          }
          break;
        case ccsInteger:
          if (CCValidateNumber($ParsingValue, $Format, $isDbFormat))
            $varResult = CCParseInteger($ParsingValue, $Format, $isDbFormat);
          else
          {
            $this->Errors->addError($CCSLocales->GetText('CCS_IncorrectValue', $this->Caption));
          }
          break;
        case ccsFloat:
          if (CCValidateNumber($ParsingValue, $Format, $isDbFormat) )
            $varResult = CCParseFloat($ParsingValue, $Format, $isDbFormat);
          else 
          {
            $this->Errors->addError($CCSLocales->GetText('CCS_IncorrectValue', $this->Caption));
          }
          break;
        case ccsText:
        case ccsMemo:
          $varResult = strval($ParsingValue);
          break;
      }
      if($this->Errors->Count() > 0)
      {
        if(strlen($this->ErrorBlock))
          $Tpl->replaceblock($this->ErrorBlock, $this->Errors->ToString());
        else
          echo $this->Errors->ToString();
      }
    }

    return $varResult;
  }

  function GetFormattedValue($Format, $isDbFormat = false)
  {
    $strResult = "";
    switch($this->DataType)
    {
      case ccsDate:
        $strResult = CCFormatDate($this->Value, $Format);
        break;
      case ccsBoolean:
        $strResult = CCFormatBoolean($this->Value, $Format);
        break;
      case ccsInteger:
      case ccsFloat:
      case ccsSingle:
        $strResult = CCFormatNumber($this->Value, $Format, $this->DataType, $isDbFormat);
        break;
      case ccsText:
      case ccsMemo:
        $strResult = strval($this->Value);
        break;
    }
    return $strResult;
  }

  function SetValue($Value)
  {
    if (is_array($Value) && ($this->DataType != ccsDate || is_array($Value[0]))) {
      $DBValues = array();
      $Texts = array();
      foreach ($Values as $Val) {
        $this->SetValue($val);
        $DBValues[] = $this->GetDBValue(true);
        $Texts[] = $this->getText(true);
      }
      $this->Value = $Value;
      $this->Text = $Texts;
      $this->DBValue = $DBValues;
      $this->IsNull = count($Value) > 0;
      return;
    }
    if (is_null($Value)) {
      $this->Value = "";
      $this->IsNull = true;
    } else {
      $this->Value = $Value;
      $this->IsNull = false;
    }
    $this->Text = $this->GetFormattedValue($this->Format);
    $this->DBValue = $this->GetFormattedValue($this->DBFormat, true);
  }

  function SetText($Text)
  {
    if (is_array($Text) && ($this->DataType != ccsDate || !CCValidateDate($Text))) {
      $Values = array();
      $DBValues = array();
      foreach ($Text as $Txt) {
        $this->SetText($Txt);
        $Values[] = $this->GetValue(true);
        $DBValues[] = $this->GetDBValue(true);
      }
      $this->Value = $Values;
      $this->Text = $Text;
      $this->DBValue = $DBValues;          
      $this->IsNull = count($Text) > 0;
    } elseif (CCCheckValue($Text, $this->DataType)) {
      $this->SetValue($Text);
    } else {
      $this->Text = $Text;
      $this->Value = $this->GetParsedValue($this->Text, $this->Format);
      if (is_null($this->Value)) {
        $this->Value = "";
        $this->IsNull = true;
      } else {
        $this->IsNull = false;
      }
      $this->DBValue = $this->GetFormattedValue($this->DBFormat, true);
    }
  }

  function SetDBValue($DBValue)
  {
    if (is_array($DBValue)) {
      $Values = array();
      $Texts = array();
      foreach ($DBValue as $DBVal) {
        $this->SetDBValue($DBVal);
        $Values[] = $this->GetValue(true);
        $Texts[] = $this->GetText(true);
      }
      $this->DBValue = $DBValue;
      $this->Value = $Values;
      $this->Text = $Texts;
      $this->IsNull = count($DBValue) > 0;
    } else {
      $this->DBValue = $DBValue;
      $this->Value = $this->GetParsedValue($this->DBValue, $this->DBFormat, true);
      $this->Text = $this->GetFormattedValue($this->Format);
    }
  }

  function GetValue($returnNull = false)
  {
    return $returnNull && $this->IsNull ? NULL : $this->Value;
  }

  function GetText()
  {
    return $this->Text;
  }

  function GetDBValue($returnNull = false)
  {
    return $returnNull && $this->IsNull ? NULL : $this->DBValue;
  }

}

//End clsSQLParameter Class

//clsControl Class @0-8D5E0E2E
class clsControl
{
  public $ComponentType = "Control";
  public $Errors;
  public $DataType;
  public $DSType;
  public $Format;
  public $DBFormat;
  public $Caption;
  public $ControlType;
  public $ControlTypeName;
  public $Name;
  public $BlockName;
  public $HTML;
  public $Required;
  public $CheckedValue;
  public $UncheckedValue;
  public $State;
  public $BoundColumn;
  public $TextColumn;
  public $Multiple;
  public $Visible;

  public $Page;
  public $Parameters;

  public $CountValue;
  public $SumValue;
  public $ValueRelative;
  public $CountValueRelative;
  public $SumValueRelative;
  public $TotalFunction;
  public $IsPercent = false;
  public $IsEmptySource = false;

  public $isInternal = false;
  public $initialValue;
  public $prevItem = false;

  public $prevValue;
  public $prevCountValue;
  public $prevSumValue;
  public $prevValueRelative;
  public $prevCountValueRelative;
  public $prevSumValueRelative;


  public $Value = "";
  public $Text;
  public $EmptyText;
  public $Values;
  public $IsNull = true;

  public $CCSEvents;
  public $CCSEventResult;

  public $Parent;

  public $Attributes;


  function clsControl($ControlType, $Name, $Caption, $DataType, $Format, $InitValue = "", & $Parent)
  {
    global $ControlTypes;

    $this->Text = "";
    $this->Page = "";
    $this->Parameters = "";
    $this->CCSEvents = "";
    $this->Values = "";
    $this->BoundColumn = "";
    $this->TextColumn = "";
    $this->Visible = true;

    $this->Required = false;
    $this->HTML = false;
    $this->Multiple = false;

    $this->Errors = new clsErrors();

    $this->Name = $Name;
    $this->BlockName = $ControlTypes[$ControlType] . " " . $Name;
    $this->ControlType = $ControlType;
    $this->DataType = $DataType;
    $this->DSType = dsEmpty;
    $this->Format = $Format;
    $this->Caption = $Caption;
    if(is_array($InitValue)) {
      $this->Value = $InitValue;
      $this->IsNull = false;
    } else if(!is_null($InitValue))
      $this->SetText($InitValue);
    $this->Parent = & $Parent;
    $this->ComponentType = $ControlTypes[$ControlType];
    $this->Attributes = new clsAttributes($this->Name . ":");
  }

  function Validate()
  {
    global $CCSLocales;
    $validation = true;
    if($this->Required && ($this->Value === "" || is_null($this->Value)) && $this->Errors->Count() == 0)
    {
      $FieldName = strlen($this->Caption) ? $this->Caption : $this->Name;
      $this->Errors->addError($CCSLocales->GetText('CCS_RequiredField', $this->Caption));
    }
    $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
    return ($this->Errors->Count() == 0);
  }

  function GetParsedValue($ParsingValue)
  {
    global $CCSLocales;
    $varResult = "";
    if($this->Multiple && is_array($ParsingValue)) {
      $ParsingValue = $ParsingValue[0];
    }
    if(CCCheckValue($ParsingValue, $this->DataType))
      $varResult = $ParsingValue;
    else if(strlen($ParsingValue))
    {
      switch ($this->DataType)
      {
        case ccsDate:
          $DateValidation = true;
          if (CCValidateDateMask($ParsingValue, $this->Format)) {
            $varResult = CCParseDate($ParsingValue, $this->Format);
            if(!$varResult || !CCValidateDate($varResult))
            {
              $DateValidation = false;
              $varResult = "";
            }
          } else {
            $DateValidation = false;
          }
          if(!$DateValidation && $this->Errors->Count() == 0)
          {
            if (is_array($this->Format)) {
              $FormatString = join("", $this->Format);
            } else {
              $FormatString = $this->Format;
            }
            $this->Errors->addError($CCSLocales->GetText('CCS_IncorrectFormat', array($this->Caption, $FormatString)));
          }
          break;
        case ccsBoolean:
          if (CCValidateBoolean($ParsingValue, $this->Format))
            $varResult = CCParseBoolean($ParsingValue, $this->Format);
          else if($this->Errors->Count() == 0) {
            if (is_array($this->Format)) {
              $FormatString = CCGetBooleanFormat($this->Format);
            } else {
              $FormatString = $this->Format;
            }
              $this->Errors->addError($CCSLocales->GetText('CCS_IncorrectFormat', array($this->Caption, $FormatString)));          }
          break;
        case ccsInteger:
          if (CCValidateNumber($ParsingValue, $this->Format))
            $varResult = CCParseInteger($ParsingValue, $this->Format);
          else if($this->Errors->Count() == 0)
            $this->Errors->addError($CCSLocales->GetText('CCS_IncorrectValue', $this->Caption));
          break;
        case ccsFloat:
          if (CCValidateNumber($ParsingValue, $this->Format))
            $varResult = CCParseFloat($ParsingValue, $this->Format);
          else if($this->Errors->Count() == 0)
            $this->Errors->addError($CCSLocales->GetText('CCS_IncorrectValue', $this->Caption));
          break;
        case ccsText:
        case ccsMemo:
          $varResult = strval($ParsingValue);
          break;
      }
    }

    return $varResult;
  }

  function GetFormattedValue()
  {
    $strResult = "";
    switch($this->DataType)
    {
      case ccsDate:
        $strResult = CCFormatDate($this->Value, $this->Format);
        break;
      case ccsBoolean:
        $strResult = CCFormatBoolean($this->Value, $this->Format);
        break;
      case ccsInteger:
      case ccsFloat:
      case ccsSingle:
        $strResult = CCFormatNumber($this->Value, $this->Format, $this->DataType);
        break;
      case ccsText:
      case ccsMemo:
        $strResult = strval($this->Value);
        break;
    }
    return $strResult;
  }

  function Prepare()
  {
    if($this->DSType == dsTable || $this->DSType == dsSQL || $this->DSType == dsProcedure)
    {
      if(!isset($this->DataSource->CCSEvents)) $this->DataSource->CCSEvents = "";
      if(!strlen($this->BoundColumn)) $this->BoundColumn = 0;
      if(!strlen($this->TextColumn)) $this->TextColumn = 1;
      $this->EventResult = CCGetEvent($this->DataSource->CCSEvents, "BeforeBuildSelect", $this);
      $this->EventResult = CCGetEvent($this->DataSource->CCSEvents, "BeforeExecuteSelect", $this);
      $FieldName = strlen($this->Caption) ? $this->Caption : $this->Name;
      list($this->Values, $this->Errors) = CCGetListValues($this->DataSource, $this->DataSource->SQL, $this->DataSource->Where, $this->DataSource->Order, $this->BoundColumn, $this->TextColumn, $this->DBFormat, $this->DataType, $this->Errors, $FieldName, $this->DSType);
      $this->DataSource->close();
      $this->EventResult = CCGetEvent($this->DataSource->CCSEvents, "AfterExecuteSelect", $this);
    }
  }

  function Show($RowNumber = "")
  {
    global $Tpl;
    global $CCSIsXHTML;
    $this->EventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
    
    $BRValue       = $CCSIsXHTML ? "<br />" : "<BR>";
    $CheckedValue  = $CCSIsXHTML ? "checked=\"checked\"" : "CHECKED";
    $SelectedValue = $CCSIsXHTML ? "selected=\"selected\"" : "SELECTED";

    $ControlName = ($RowNumber === "") ? $this->Name : $this->Name . "_" . $RowNumber;
    if($this->Multiple) $ControlName = $ControlName . "[]";

    if(!$this->Visible) {
      $Tpl->SetVar($this->Name . "_Name", $ControlName);
      $Tpl->SetVar($this->Name, "");
      if($Tpl->BlockExists($this->BlockName))
        $Tpl->setblockvar($this->BlockName, "");
      return;
    }

    $this->Attributes->Show();

    $Tpl->SetVar($this->Name . "_Name", $ControlName);
    switch($this->ControlType)
    {
      case ccsLabel:
        $value=$this->GetText();
        if (!$this->HTML) {
          $value = CCToHTML($value);
          $value = str_replace("\n", $BRValue, $value);
        }
        $Tpl->SetVar($this->Name, $value);
        $Tpl->ParseSafe($this->BlockName, false);
        break;
      case ccsReportLabel:
        $value=$this->GetText();
        if (strlen($this->EmptyText) && !strlen($value))
          $value = $this->EmptyText;
        if (!$this->HTML) {
          $value = CCToHTML($value);
          $value = str_replace("\n", $BRValue, $value);
        }
        $Tpl->SetVar($this->Name, $value);
        $Tpl->ParseSafe($this->BlockName, false);
        break;
      case ccsTextBox:
      case ccsTextArea:
      case ccsImage:
      case ccsHidden:
        $Tpl->SetVar($this->Name, CCToHTML($this->GetText()));
        $Tpl->ParseSafe($this->BlockName, false);
        break;
      case ccsLink:
        if ($this->HTML)
          $Tpl->SetVar($this->Name, $this->GetText());
        else {
          $value = CCToHTML($this->GetText());
          $value = str_replace("\n", $BRValue, $value);
          $Tpl->SetVar($this->Name, $value);
        }
        $Tpl->SetVar($this->Name . "_Src", $this->GetLink());
        $Tpl->ParseSafe($this->BlockName, false);
        break;
      case ccsImageLink:
        $Tpl->SetVar($this->Name . "_Src", CCToHTML($this->GetText()));
        $Tpl->SetVar($this->Name, $this->GetLink());
        $Tpl->ParseSafe($this->BlockName, false);
        break;
      case ccsCheckBox:
        if($this->Value)
          $Tpl->SetVar($this->Name, $CheckedValue);
        else
          $Tpl->SetVar($this->Name, "");
        $Tpl->ParseSafe($this->BlockName, false);
        break;
      case ccsRadioButton:
        $BlockToParse = "RadioButton " . $this->Name;
        $Tpl->SetBlockVar($BlockToParse, "");
        if(is_array($this->Values))
        {
          for($i = 0; $i < sizeof($this->Values); $i++)
          {
            $Value = $this->Values[$i][0];
            $this->Attributes->SetValue("optionNumber", $i + 1);
            $this->Attributes->Objects["optionNumber"]->Show();
            $Text = $this->HTML ? $this->Values[$i][1] : CCToHTML($this->Values[$i][1]);
            $Selected = (CCCompareValues($Value,$this->Value, $this->DataType, $this->Format) == 0) ? $CheckedValue : "";
            $TextValue = CCToHTML(CCFormatValue($Value, $this->Format, $this->DataType, $this->Format));
            $Tpl->SetVar("Value", $TextValue);
            $Tpl->SetVar("Check", $Selected);
            $Tpl->SetVar("Description", $Text);
            $Tpl->Parse($BlockToParse, true);
          }
        }
        break;
      case ccsCheckBoxList:
        $BlockToParse = "CheckBoxList " . $this->Name;
        $Tpl->SetBlockVar($BlockToParse, "");
        if(is_array($this->Values))
        {
          for($i = 0; $i < sizeof($this->Values); $i++)
          {
            $Value = $this->Values[$i][0];
            $this->Attributes->SetValue("optionNumber", $i + 1);
            $this->Attributes->Objects["optionNumber"]->Show();
            $TextValue = CCToHTML(CCFormatValue($Value, $this->Format, $this->DataType));
            $Text = $this->HTML ? $this->Values[$i][1] : CCToHTML($this->Values[$i][1]);
	          if ($this->Multiple && is_array($this->Value)) {
              $Selected = "";
              foreach ($this->Value as $Val) {
                if (CCCompareValues($Value,$Val, $this->DataType, $this->Format) == 0) {
                  $Selected = " " . $CheckedValue;
                  break;  
                }
              }
	          } else {
              $Selected = (CCCompareValues($Value,$this->Value, $this->DataType, $this->Format) == 0) ? " " .$CheckedValue : "";
            }
            $Tpl->SetVar("Value", $TextValue);
            $Tpl->SetVar("Check", $Selected);
            $Tpl->SetVar("Description", $Text);
            $Tpl->Parse($BlockToParse, true);
          }
        }
        break;
      case ccsListBox:
        $Options = "";
        if(is_array($this->Values))
        {
          for($i = 0; $i < sizeof($this->Values); $i++)
          {
            $Value = $this->Values[$i][0];
            $TextValue = CCToHTML(CCFormatValue($Value, $this->Format, $this->DataType));
            $Text = CCToHTML($this->Values[$i][1]);
	    if ($this->Multiple && is_array($this->Value)) {
              $Selected = "";
              foreach ($this->Value as $Val) {
                if (CCCompareValues($Value,$Val, $this->DataType, $this->Format) == 0) {
                  $Selected = " " . $SelectedValue;
                  break;  
                }
              }
	    } else {
              $Selected = (CCCompareValues($Value,$this->Value, $this->DataType, $this->Format) == 0) ? " " . $SelectedValue : "";
            }
            $Options .= $CCSIsXHTML 
                        ? "<option value=\"" . $TextValue . "\"" . $Selected . ">" . $Text . "</option>\n"
                        : "<OPTION VALUE=\"" . $TextValue . "\"" . $Selected . ">" . $Text . "</OPTION>\n";
          }
        }
        $Tpl->SetVar($this->Name . "_Options", $Options);
        $Tpl->ParseSafe($this->BlockName, false);
        break;
      case ccsPageBreak:
          $Tpl->SetVar($this->Name, $this->Text);

    }
  }

  function SetValue($Value)
  {
    if($this->ControlType == ccsCheckBox) {
      $this->Value = CCCompareValues($Value, $this->CheckedValue, $this->DataType) == 0 || (CCCompareValues($Value, $this->UncheckedValue, $this->DataType) != 0 && (is_array($Value) || strlen($Value))) ? true : false;
      $this->IsNull = false;
    } else {
      $this->Value = $Value;
      $this->IsNull = is_null($Value);
    }
    $this->Text = $this->GetFormattedValue();
    if (!$this->isInternal) 
      $this->initialValue = $this->Value;
  }

  function SetText($Text, $RowNumber = "")
  {
    $ControlName = ($RowNumber === "") ? $this->Name : $this->Name . "_" . $RowNumber;
    if(CCCheckValue($Text, $this->DataType)) {
      $this->SetValue($Text);
    } else {
      if($this->ControlType == ccsCheckBox) {
        $RequestParameter = CCGetParam($ControlName);
        if (strlen($Text) && strlen($RequestParameter) && $Text == $RequestParameter) {
          $this->Value = true;
	  $this->IsNull = false;
        } else {
          $Value = $this->GetParsedValue($Text);
          $this->SetValue($Value);
        }
      } else {
	$this->Text = is_null($Text) ? "" : $Text;
        $this->Value = $this->GetParsedValue($this->Text);
        if (is_null($Text)) {
          $this->Value = "";
          $this->IsNull = true;
        } else {
          $this->IsNull = false;
        }
        if (!$this->isInternal) 
          $this->initialValue = $this->Value;
      }
    }
  }

  function GetValue($returnNull = false)
  {
    if($this->ControlType == ccsCheckBox)
      $value = ($this->Value) ? $this->CheckedValue : $this->UncheckedValue;
    else if($this->Multiple && is_array($this->Value))
      $value = $this->Value[0];
    else
      $value = $returnNull && $this->IsNull ? NULL : $this->Value;

    return $value;
  }

  function GetText()
  {
    if(!strlen($this->Text))
      $this->Text = $this->GetFormattedValue();
    return $this->Text;
  }

  function GetLink()
  {
    global $CCSUseAmp;
    if(CCSubStr($this->Page, 0, 2) == "./") {
      return CCSubStr($this->Page, 2);
    }
    if($this->Parameters == "") {
      return $this->Page;
    } else {
      if ($CCSUseAmp) {
        return str_replace("&", "&amp;", $this->Page . "?" . $this->Parameters);
      } else {
        return $this->Page . "?" . $this->Parameters;
      }
    }
  }

  function SetLink($Link)
  {
    if(!strlen($Link))
    {
      $this->Page = "";
      $this->Parameters = "";
    }
    else
    {
      $LinkParts = explode("?", $Link);
      $this->Page = $LinkParts[0];
      $this->Parameters = (sizeof($LinkParts) == 2) ? $LinkParts[1] : "";
    }
  }

  function GetTotalValue($mode) 
  {
    if ($mode == "GetPrevValue") {
      if ($this->TotalFunction == "Count")
        $this->prevValue += 0;
      $this->Value = $this->prevValue;
      return $this->Value;      
    }
    if ($mode == "GetNextValue" && $this->TotalFunction) {
      if ($this->TotalFunction == "Count")
        $this->prevValue += 0;
      $this->Value = $this->prevValue;
      return $this->Value;      
    }

    $this->Value = $this->initialValue;

    $newVal = $this->prevValue;
    switch ($this->TotalFunction) {
      case "Sum":
        if (strval($this->Value) == "" && strval($this->prevValue) == "")
          break;
        $newVal = $this->Value + $this->prevValue;
        if ($this->IsPercent && (strval($this->Value) != "" || strval($this->prevValueRelative) != ""))
          $this->ValueRelative = $this->Value + $this->prevValueRelative;
        break;
      case "Count":
        $newVal = $this->prevValue + ($this->IsEmptySource || ($this->DataType == ccsBoolean && is_bool($this->Value)) || ($this->DataType == ccsDate  && CCValidateDate($this->Value)) || strval($this->Value) != "" ? 1 : 0);
        if ($this->IsPercent)
          $this->ValueRelative = $this->prevValueRelative + ($this->IsEmptySource || ($this->DataType == ccsBoolean && is_bool($this->Value)) || ($this->DataType == ccsDate  && CCValidateDate($this->Value)) || strval($this->Value) != "" ? 1 : 0);
        break;
      case "Min":
        if (strval($this->Value) == "") 
          break;
        $newVal = strval($this->prevValue) == "" ? $this->Value : min($this->Value,$this->prevValue);
        if ($this->IsPercent)
          $this->ValueRelative = strval($this->prevValueRelative) == "" ? $this->Value : min($this->Value,$this->prevValueRelative);
        break;
      case "Max":
        if (strval($this->Value) == "") 
          break;
        $newVal = strval($this->prevValue) == "" ? $this->Value : max($this->Value,$this->prevValue);
        if ($this->IsPercent)
          $this->ValueRelative = strval($this->prevValueRelative) == "" ? $this->Value : max($this->Value,$this->prevValueRelative);
        break;
      case "Avg":
        if (strval($this->Value) != "") { 
          $this->CountValue = $this->prevCountValue + 1;
          $this->SumValue = $this->prevSumValue + $this->Value;
        }
        if ($this->CountValue == 0) 
          $newVal = $this->prevValue;
        else
          $newVal = $this->SumValue / $this->CountValue;
        if ($this->IsPercent) { 
          if (strval($this->Value) !="") { 
            $this->CountValueRelative = $this->prevCountValueRelative + 1;
            $this->SumValueRelative = $this->prevSumValueRelative + $this->Value;
          }
          if ($this->CountValueRelative == 0)
            $this->ValueRelative = $this->prevValueRelative;
          else
            $this->ValueRelative = $this->SumValueRelative / $this->CountValueRelative;
        }
        break;
      default: 
        if ($mode == "" && $this->IsPercent && (strval($this->Value) != "" || strval($this->prevValueRelative) != "")) {
          $this->ValueRelative = $this->Value + $this->prevValueRelative;
        }
        $newVal = $this->Value;
    }
    $this->Value = $newVal;
    if ($mode == "GetNextValue") {
      return $this->Value;
    }
    $this->prevValueRelative = $this->ValueRelative;
    $this->prevValue = $newVal;
    $this->prevCountValue = $this->CountValue;
    $this->prevSumValue = $this->SumValue;
    $this->prevCountValueRelative = $this->CountValueRelative;
    $this->prevSumValueRelative = $this->SumValueRelative;
    return $this->Value;
  }

  function Reset() 
  {
    $this->Value = "";
    $this->CountValue = "";
    $this->SumValue = "";
    $this->prevValue = "";
    $this->prevCountValue = "";
    $this->prevSumValue = "";
  }

  function ResetRelativeValues() 
  {
    $this->ValueRelative = $this->initialValue;
    $this->prevValueRelative = "";
    $this->CountValueRelative = "";
    $this->SumValueRelative = "";
    $this->prevCountValueRelative = "";
    $this->prevSumValueRelative = "";
  }


}

//End clsControl Class

//clsField Class @0-3A089A0E
class clsField
{
  public $DataType;
  public $DBFormat;
  public $Name;
  public $Errors;

  public $Value = "";
  public $IsNull = true;
  public $DBValue = "";

  function clsField($Name, $DataType, $DBFormat)
  {
    $this->Name = $Name;
    $this->DataType = $DataType;
    $this->DBFormat = $DBFormat;

    $this->Errors = new clsErrors;
  }

  function GetParsedValue()
  {
    global $CCSLocales;
    $varResult = "";

    if (strlen($this->DBValue))
    {
      switch ($this->DataType)
      {
        case ccsDate:
          $DateValidation = true;
          if (CCValidateDateMask($this->DBValue, $this->DBFormat)) {
            $varResult = CCParseDate($this->DBValue, $this->DBFormat);
            if(!$varResult || !CCValidateDate($varResult)) {
              $DateValidation = false;
              $varResult = "";
            }
          } else {
            $DateValidation = false;
          }
          if (!$DateValidation)
          {
            if (is_array($this->DBFormat)) {
              $FormatString = join("", $this->DBFormat);
            } else {
              $FormatString = $this->DBFormat;
            }
            $this->Errors->addError($CCSLocales->GetText('CCS_IncorrectFieldFormat', array($this->Name, $FormatString)));
          }
          break;
        case ccsBoolean:
          if (CCValidateBoolean($this->DBValue, $this->DBFormat)) {
            $varResult = CCParseBoolean($this->DBValue, $this->DBFormat);
          } else {
            if (is_array($this->DBFormat)) {
              $FormatString = CCGetBooleanFormat($this->DBFormat);
            } else {
              $FormatString = $this->DBFormat;
            }
            $this->Errors->addError($CCSLocales->GetText('CCS_IncorrectFieldFormat', array($this->Name, $FormatString)));
          }
          break;
        case ccsInteger:
          if (CCValidateNumber($this->DBValue, $this->DBFormat, true))
            $varResult = CCParseInteger($this->DBValue, $this->DBFormat, true);
          else 
            $this->Errors->addError($CCSLocales->GetText('CCS_IncorrectFieldFormat', array($this->Name, $this->DBFormat)));
          break;
        case ccsFloat:
          if (CCValidateNumber($this->DBValue, $this->DBFormat, true) )
            $varResult = CCParseFloat($this->DBValue, $this->DBFormat, true);
          else 
            $this->Errors->addError($CCSLocales->GetText('CCS_IncorrectFieldFormat', array($this->Name, $this->DBFormat)));
          break;
        case ccsText:
        case ccsMemo:
          $varResult = strval($this->DBValue);
          break;
      }
    }

    return $varResult;
  }

  function GetFormattedValue()
  {
    $strResult = "";
    switch($this->DataType)
    {
      case ccsDate:
        $strResult = CCFormatDate($this->Value, $this->DBFormat);
        break;
      case ccsBoolean:
        $strResult = CCFormatBoolean($this->Value, $this->DBFormat);
        break;
      case ccsInteger:
      case ccsFloat:
      case ccsSingle:
        $strResult = CCFormatNumber($this->Value, $this->DBFormat, $this->DataType, true);
        break;
      case ccsText:
      case ccsMemo:
        $strResult = strval($this->Value);
        break;
    }
    return $strResult;
  }

  function SetDBValue($DBValue)
  {
    $this->DBValue = $DBValue;
    $this->Value = $this->GetParsedValue();
  }

  function SetValue($Value)
  {
    if (is_null($Value)) {
      $this->Value = "";
      $this->IsNull = true;
    } else {
      $this->Value = $Value;
      $this->IsNull = false;
    }
    $this->DBValue = $this->GetFormattedValue();
  }

  function GetValue($returnNull = false)
  {
    return $returnNull && $this->IsNull ? NULL : $this->Value;
  }

  function GetDBValue($returnNull = false)
  {
    return $returnNull && $this->IsNull ? NULL : $this->DBValue;
  }
}

//End clsField Class

//clsButton Class @0-25354C90
class clsButton
{
  public $ComponentType = "Button";
  public $Name;
  public $Visible;
  public $Pressed;

  public $CCSEvents = "";
  public $CCSEventResult;

  public $Parent;

  public $Attributes;

  function clsButton($Name, $Method, & $Parent)
  {
    $this->Name    = $Name;
    $this->Visible = true;
    $this->Parent  = & $Parent;
    $this->Pressed = CCGetRequestParam($Name, $Method) != "" || CCGetRequestParam($Name . "_x", $Method) != "";
    $this->Attributes = new clsAttributes($this->Name . ":");
  }

  function Show($RowNumber = "")
  {
    global $Tpl;
    $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
    if($this->Visible)
    {
      $this->Attributes->Show();
      $ControlName = ($RowNumber === "") ? $this->Name : $this->Name . "_" . $RowNumber;
      $Tpl->SetVar("Button_Name", $ControlName);
      $Tpl->Parse("Button " . $this->Name, false);
    }
    else
    {
      $Tpl->setblockvar("Button " . $this->Name, "");
    }
  }

}

//End clsButton Class

//clsPanel Class @0-5B807132
class clsPanel
{
  public $ComponentType = "Panel";
  public $Name;
  public $Visible;
  public $Components = array();
  public $ComponentsArray = array();

  public $CCSEvents = "";
  public $CCSEventResult;

  public $AjaxId = false;

  function clsPanel($Name, & $Parent)
  {
    $this->Name = $Name;
    $this->Visible = true;
    $this->Parent = & $Parent;
  }
  
  function AddComponent($Name, &$Component){
    $this->Components[$Name] = & $Component;
    $this->ComponentsArray[] = & $Component;
  }

  function Show($RowNumber = "")
  {
    global $Tpl, $CCSFormFilter;
    $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
    if($this->Visible)
    {
      $ControlName = $this->Name;
      $ParentPath = $Tpl->block_path;
      $PanelPath = $ParentPath . "/Panel " . $ControlName;
      $Tpl->block_path =  $PanelPath;
      foreach($this->ComponentsArray as $num => $Component){
        if(strlen($RowNumber)) 
          $this->ComponentsArray[$num]->Show($RowNumber);
        else      
          $this->ComponentsArray[$num]->Show();
      }
      $Tpl->block_path = $ParentPath;
      $Tpl->Parse("Panel " . $this->Name, false);
      if ($this->AjaxId && $CCSFormFilter != $this->AjaxId)
        $Tpl->setblockvar("Panel " . $this->Name, "<span class=\"AjaxPanel\" id=\"" . $this->AjaxId . "\">" . $Tpl->getvar("Panel " . $this->Name) . "</span>");
    }
    else
    {
      $Tpl->setblockvar("Panel " . $this->Name, "");
    }
  }
}

//End clsPanel Class

//clsFileUpload Class @0-CEB29D14
class clsFileUpload
{
  public $ComponentType = "FileUpload";
  public $Name;
  public $Caption;
  public $Visible;
  public $Required;

  public $TemporaryFolder;
  public $FileFolder;
  public $AllowedMask; // @deprecated , use AllowedFileMasks property
  public $AllowedFileMasks;
  public $DisallowedFileMasks;
  public $FileSizeLimit;
  public $Value;
  public $Text;
  public $State;

  public $CCSEvents = "";
  public $CCSEventResult;

  public $Parent;

  public $Attributes;

  function clsFileUpload($Name, $Caption, $TemporaryFolder, $FileFolder, $AllowedFileMasks, $DisallowedFileMasks, $FileSizeLimit, & $Parent)
  {
    global $CCSLocales;

    $this->Errors = new clsErrors;

    $this->Name            = $Name;
    $this->Visible         = true;
    $this->Caption         = $Caption;
    $this->Parent          = & $Parent;

    if(CCSubStr($TemporaryFolder, 0, 1) == "%") {
      $TemporaryFolder = CCSubStr($TemporaryFolder, 1);
      $TemporaryFolder = isset($_ENV[$TemporaryFolder]) ? $_ENV[$TemporaryFolder] : getenv($TemporaryFolder);
    }
    $this->TemporaryFolder = $TemporaryFolder;
    if(CCSubStr($FileFolder, 0, 1) == "%") {
      $FileFolder = CCSubStr($FileFolder, 1);
      $FileFolder = isset($_ENV[$FileFolder]) ? $_ENV[$FileFolder] : getenv($FileFolder);
    }
    $this->FileFolder          = $FileFolder;
    $this->AllowedFileMasks    = $AllowedFileMasks;
    $this->AllowedMask         = & $this->AllowedFileMasks; 
    $this->DisallowedFileMasks = $DisallowedFileMasks;
    $this->FileSizeLimit       = $FileSizeLimit;
    $this->Value               = "";
    $this->Text                = "";
    $this->Required            = false;

    $FileName = "";
    $FieldName = $this->Caption;
    if( !is_dir($TemporaryFolder) ) {
      $this->Errors->addError($CCSLocales->GetText('CCS_TempFolderNotFound', $this->Caption));
    } else if( !is_writable($TemporaryFolder) ) {
      $this->Errors->addError($CCSLocales->GetText('CCS_TempInsufficientPermissions', $this->Caption));
    } else if( !is_dir($FileFolder) ) {
      $this->Errors->addError($CCSLocales->GetText('CCS_FilesFolderNotFound', $this->Caption));
    } else if( !is_writable($FileFolder) ) {
      $this->Errors->addError($CCSLocales->GetText('CCS_InsufficientPermissions', $this->Caption));
    } 
    $this->Attributes = new clsAttributes($this->Name . ":");

  }

  function Validate()
  {
    global $CCSLocales;
    $validation = true;
    if($this->Required && $this->Value === "" && $this->Errors->Count() == 0)
    {
      $FieldName = $this->Caption;
      $this->Errors->addError($CCSLocales->GetText('CCS_RequiredFieldUpload', $this->Caption));
    }
    $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
    return ($this->Errors->Count() == 0);
  }


  function Upload($RowNumber = "")
  {
    global $CCSLocales;
    global $TemplateEncoding;
    global $FileEncoding;
     

    $FieldName = $this->Caption;
    if(strlen($RowNumber)) {
      $ControlName = $this->Name . "_" . $RowNumber;
      $FileControl = $this->Name . "_File_" . $RowNumber;
      $DeleteControl = $this->Name . "_Delete_" . $RowNumber;
    } else {
      $ControlName = $this->Name;
      $FileControl = $this->Name . "_File";
      $DeleteControl = $this->Name . "_Delete";
    }

    $SessionName = CCGetParam($ControlName);
    $this->State = CCGetSession($SessionName, array(null, null));

    if (strlen(CCGetParam($DeleteControl))) { 
      // delete file from folder
      $ActualFileName = $this->State[0];
      if( file_exists($this->FileFolder . $ActualFileName) ) {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDeleteFile", $this);
        unlink($this->FileFolder . $ActualFileName);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDeleteFile", $this);
      } else if ( file_exists($this->TemporaryFolder . $ActualFileName) ) {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDeleteFile", $this);
        unlink($this->TemporaryFolder . $ActualFileName);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDeleteFile", $this);
      }
      $this->Value = ""; $this->Text = "";
      $this->State[0] = "";
    } else if (isset ($_FILES[$FileControl]) 
        && $_FILES[$FileControl]["tmp_name"] != "none" 
        && strlen ($_FILES[$FileControl]["tmp_name"])) {
      $this->Value = ""; $this->Text = "";
      $FileName = CCConvertEncoding(CCStrip($_FILES[$FileControl]["name"]), $TemplateEncoding, $FileEncoding);
      $GoodFileMask = 1;
      $meta_characters = array("*" => ".+", "?" => ".", "\\" => "\\\\", "^" => "\\^", "\$" => "\\\$", "." => "\\.", "[" => "\\[", "]" => "\\]", "|" => "\\|", "(" => "\\(", ")" => "\\)", "{" => "\\{", "}" => "\\}", "+" => "\\+", "-" => "\\-");
      if ($this->AllowedFileMasks != "") {
        $GoodFileMask = 0;
        $FileMasks=explode(';', $this->AllowedFileMasks);
        foreach ($FileMasks as $FileMask) {
          $FileMask = preg_replace("/(\\*|\\?|\\\\|\\^|\\\$|\\.|\\[|\\]|\\||\\(|\\)|\\{|\\}|\\+|\\-)/ei", "\$meta_characters['\\1']", $FileMask);
          if (preg_match("/^$FileMask$/i", $FileName)) {
            $GoodFileMask = 1;
            break;
          }
        }
      }


      if ($GoodFileMask && $this->DisallowedFileMasks != "") {
        $FileMasks=explode(';', $this->DisallowedFileMasks);
        foreach ($FileMasks as $FileMask) {
          $FileMask = preg_replace("/(\\*|\\?|\\\\|\\^|\\\$|\\.|\\[|\\]|\\||\\(|\\)|\\{|\\}|\\+|\\-)/ei", "\$meta_characters['\\1']", $FileMask);
          if (preg_match("/^$FileMask$/i", $FileName)) {
            $GoodFileMask = 0;
            break;
          }
        }
      }
      if($_FILES[$FileControl]["size"] > $this->FileSizeLimit) {
      $this->Errors->addError($CCSLocales->GetText('CCS_LargeFile', $this->Caption));
      } else if (!$GoodFileMask) {
      $this->Errors->addError($CCSLocales->GetText('CCS_WrongType', $this->Caption));
      } else {
        // move uploaded file to temporary folder
        $file_exists = true;
        $index = 0;
        while($file_exists) {
          $ActualFileName = date("YmdHis") . $index . "." . $FileName;
          $file_exists = file_exists($ActualFileName);
          $index++;
        }
        if( move_uploaded_file($_FILES[$FileControl]["tmp_name"], $this->TemporaryFolder . $ActualFileName) ) {
          $this->Value = $ActualFileName;
          $this->Text = $ActualFileName;
          if(isset($this->State[0]) && strlen($this->State[0])) {
            if(file_exists($this->TemporaryFolder . $this->State[0])) {
              $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDeleteFile", $this);
              unlink($this->TemporaryFolder . $this->State[0]);
              $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDeleteFile", $this);
              $this->State[0] = $ActualFileName;
            } else {
              if(!is_dir($this->TemporaryFolder . $this->State[1]) && file_exists($this->TemporaryFolder . $this->State[1])) {
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDeleteFile", $this);
                unlink($this->TemporaryFolder . $this->State[1]);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDeleteFile", $this);
              }
              $this->State[1] = $ActualFileName;
            }
          } else {
            $this->State[0] = $ActualFileName;
          }
        } else {
          $this->Errors->addError($CCSLocales->GetText('CCS_TempInsufficientPermissions', $this->Caption));
        }
      }
    } else {
      $this->SetValue(strlen($this->State[1]) ? $this->State[1] : $this->State[0]);
    }
  }

  function Move()
  {
    global $CCSLocales;
    if (strlen($this->Value) && !file_exists($this->FileFolder . $this->Value)) {
      $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeProcessFile", $this);
      $FileName = $this->GetFileName();
      $FieldName = $this->Caption;
      if (!file_exists($this->TemporaryFolder . $this->Value)) {
        $this->Errors->addError($CCSLocales->GetText('CCS_FileNotFound', array($this->TemporaryFolder . $this->Value, $this->Caption)));
      } else if (!@copy($this->TemporaryFolder . $this->Value, $this->FileFolder . $this->Value)) {
        $this->Errors->addError($CCSLocales->GetText('CCS_InsufficientPermissions', $this->Caption));
      } else if (strlen($this->State[1])) {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDeleteFile", $this);
        unlink($this->FileFolder . $this->State[0]);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDeleteFile", $this);
      }
      if($this->Errors->Count() == 0 && file_exists($this->TemporaryFolder . $this->Value)) {
        unlink($this->TemporaryFolder . $this->Value);
      }
      $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterProcessFile", $this);
    }
  }

  function Delete()
  {
    if( !is_dir($this->FileFolder . $this->State[0]) && file_exists($this->FileFolder . $this->State[0]) ) {
      $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDeleteFile", $this);
      unlink($this->FileFolder . $this->State[0]);
      $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDeleteFile", $this);
    } else if ( !is_dir($this->TemporaryFolder . $this->State[0]) && file_exists($this->TemporaryFolder . $this->State[0]) ) {
      $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDeleteFile", $this);
      unlink($this->TemporaryFolder . $this->State[0]);
      $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDeleteFile", $this);
    }
    if( !is_dir($this->FileFolder . $this->State[1]) && file_exists($this->FileFolder . $this->State[1]) ) {
      $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDeleteFile", $this);
      unlink($this->FileFolder . $this->State[1]);
      $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDeleteFile", $this);
    } else if ( !is_dir($this->TemporaryFolder . $this->State[1]) && file_exists($this->TemporaryFolder . $this->State[1]) ) {
      $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDeleteFile", $this);
      unlink($this->TemporaryFolder . $this->State[1]);
      $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDeleteFile", $this);
    }
  }

  function Show($RowNumber = "")
  {
    global $Tpl;
    if($this->Visible)
    {
      $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);

      if(!$this->Visible) {
        $Tpl->setblockvar("FileUpload " . $this->Name, "");
        return;
      }

      $this->Attributes->Show();

      if(strlen($RowNumber)) {
        $ControlName = $this->Name . "_" . $RowNumber;
        $FileControl = $this->Name . "_File_" . $RowNumber;
        $DeleteControl = $this->Name . "_Delete_" . $RowNumber;
      } else {
        $ControlName = $this->Name;
        $FileControl = $this->Name . "_File";
        $DeleteControl = $this->Name . "_Delete";
      }

      $SessionName = CCGetParam($ControlName);
      if(!strlen($SessionName)) {
        $random_value = mt_rand(100000,9999999) . mt_rand(100000,9999999);
        $SessionName = "FileUpload" . $random_value . date("dHis");
        $this->State = array($this->Value, "");
      } 

      CCSetSession($SessionName, $this->State);

      $Tpl->SetVar("State", $SessionName);
      $Tpl->SetVar("ControlName", $ControlName);
      $Tpl->SetVar("FileControl", $FileControl);
      $Tpl->SetVar("DeleteControl", $DeleteControl);
      if (strlen($this->Value) ) {
        $Tpl->SetVar("ActualFileName", $this->Value);
        $Tpl->SetVar("FileName", $this->GetFileName());
        $Tpl->SetVar("FileSize", $this->GetFileSize());
        $Tpl->parse("FileUpload " . $this->Name . "/Info", false);
        if($this->Required) {
          $Tpl->parse("FileUpload " . $this->Name . "/Upload", false);
          $Tpl->setblockvar("FileUpload " . $this->Name . "/DeleteControl", "");
        } else {
          $Tpl->setblockvar("FileUpload " . $this->Name . "/Upload", "");
          $Tpl->parse("FileUpload " . $this->Name . "/DeleteControl", false);
        }
      } else {
        $Tpl->parse("FileUpload " . $this->Name . "/Upload", false);
        $Tpl->setblockvar("FileUpload " . $this->Name . "/Info", "");
        $Tpl->setblockvar("FileUpload " . $this->Name . "/DeleteControl", "");
      }

      $Tpl->Parse("FileUpload " . $this->Name, false);
    }
    else
    {
      $Tpl->setblockvar("FileUpload " . $this->Name, "");
    }
  }

  function SetValue($Value) {
    global $CCSLocales;
    $this->Text = $Value;
    $this->Value = $Value;
    $this->State[0] = $Value;
    if(strlen($Value) 
      && !file_exists($this->TemporaryFolder . $Value) 
      && !file_exists($this->FileFolder . $Value)) {
        $FileName = $this->GetFileName();
        $FieldName = $this->Caption;
	$this->Errors->addError($CCSLocales->GetText('CCS_FileNotFound', array($Value, $this->Caption)));
    }
  }

  function SetText($Text) {
    $this->SetValue($Text);
  }

  function GetValue() {
    return $this->Value;
  }

  function GetText() {
    return $this->Text;
  }

  function GetFileName() {
    return CCGetOriginalFileName($this->Value);
  }

  function GetFileSize() {
    $filesize = 0;
    if( file_exists($this->FileFolder . $this->Value) ) {
      $filesize = filesize($this->FileFolder . $this->Value);
    } else if ( file_exists($this->TemporaryFolder . $this->Value) ) {
      $filesize = filesize($this->TemporaryFolder . $this->Value);
    }
    return $filesize;
  }

}

//End clsFileUpload Class

//clsDatePicker Class @0-F4599544
class clsDatePicker
{
  public $ComponentType = "DatePicker";
  public $Name;
  public $DateFormat;
  public $Style;
  public $FormName;
  public $ControlName;
  public $Visible;
  public $Errors;

  public $Attributes;

  public $CCSEvents = "";
  public $CCSEventResult;

  public $Parent;

  function clsDatePicker($Name, $FormName, $ControlName, & $Parent)
  {
    $this->Name        = $Name;
    $this->FormName    = $FormName;
    $this->ControlName = $ControlName;
    $this->Parent      = & $Parent;
    $this->Visible     = true;

    $this->Errors = new clsErrors;
    $this->Attributes = new clsAttributes($this->Name . ":");
  }

  function Show($RowNumber = "")
  {
    global $Tpl;
    if($this->Visible)
    {
      $this->Attributes->Show();
      $ControlName = ($RowNumber === "") ? $this->ControlName : $this->ControlName . "_" . $RowNumber;
      $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
      $Tpl->SetVar("Name",        $this->FormName . "_" . $this->Name);
      $Tpl->SetVar("FormName",    $this->FormName);
      $Tpl->SetVar("DateControl", $ControlName);

      $Tpl->Parse("DatePicker " . $this->Name, false);
    }
    else
    {
      $Tpl->setblockvar("DatePicker " . $this->Name, "");
    }
  }

}

//End clsDatePicker Class

//clsErrors Class @0-B0A95765
class clsErrors
{
  public $Errors;
  public $ErrorsCount;
  public $ErrorDelimiter;

  function clsErrors()
  {
    global $CCSIsXHTML;
    $this->Errors = array();
    $this->ErrorsCount = 0;
    $this->ErrorDelimiter = $CCSIsXHTML ? "<br />" : "<br>";
  }

  function addError($Description)
  {
    if (strlen($Description))
    {
      $this->Errors[$this->ErrorsCount] = $Description; 
      $this->ErrorsCount++;
    }
  }

  function AddErrors($Errors)
  {
    for($i = 0; $i < $Errors->Count(); $i++)
      $this->addError($Errors->Errors[$i]);
  }

  function Clear()
  {
    $this->Errors = array();
    $this->ErrorsCount = 0;
  }

  function Count()
  {
    return $this->ErrorsCount;
  }

  function ToString()
  {

    if(sizeof($this->Errors) > 0)
      return join($this->ErrorDelimiter, $this->Errors);
    else
      return "";
  }

}
//End clsErrors Class

//clsSection Class @0-AB10EA0E
class clsSection
{
  public $ComponentType = "Section";
  public $Visible = true;
  public $Height = 0;
  public $CCSEvents = array();
  public $CCSEventResult;
  public $Parent;
  public $Attributes;
  function clsSection(& $Parent) {
    $this->Parent = & $Parent;
  }

}
//End clsSection Class

//clsLocaleInfo @0-09690F86
class clsLocaleInfo {
  public $FormatInfo;
  public $Name;
  public $Language;
  public $Country;
  public $BooleanFormat;
  public $DecimalDigits;
  public $DecimalSeparator;
  public $GroupSeparator;
  public $MonthNames;
  public $MonthShortNames;
  public $WeekdayNames;
  public $WeekdayShortNames;
  public $WeekdayNarrowNames;
  public $ShortDate;
  public $LongDate;
  public $ShortTime;
  public $LongTime;
  public $GeneralDate;
  public $FirstWeekDay;
  public $OverrideNumberFormats;
  public $AMDesignator;
  public $PMDesignator;
  public $Encoding;
  public $PHPEncoding;
  public $PHPLocale;

  function clsLocaleInfo($name, $LocaleInfoArray) {
    $this->Name = $name;
    $this->Language = $LocaleInfoArray[0];
    $this->Country = $LocaleInfoArray[1];

    $this->BooleanFormat = $LocaleInfoArray[2];

    $this->DecimalDigits = $LocaleInfoArray[3];
    $this->DecimalSeparator = $LocaleInfoArray[4];
    $this->GroupSeparator = $LocaleInfoArray[5];

    $this->MonthNames = $LocaleInfoArray[6];
    $this->MonthShortNames = $LocaleInfoArray[7];

    $this->WeekdayNames = $LocaleInfoArray[8];
    $this->WeekdayShortNames = $LocaleInfoArray[9];
    $this->WeekdayNarrowNames = $LocaleInfoArray[10];

    $this->ShortDate = $LocaleInfoArray[11];
    $this->LongDate = $LocaleInfoArray[12];

    $this->ShortTime = $LocaleInfoArray[13];
    $this->LongTime = $LocaleInfoArray[14];
    $this->AMDesignator = $LocaleInfoArray[15];
    $this->PMDesignator = $LocaleInfoArray[16];

    $this->GeneralDate = array();
    foreach ($this->ShortDate as $val) {
     array_push($this->GeneralDate, $val);
    }
     array_push($this->GeneralDate, " ");
    foreach ($this->LongTime as $val) {
     array_push($this->GeneralDate, $val);
    }
    $this->FirstWeekDay = $LocaleInfoArray[17];
    $this->OverrideNumberFormats = $LocaleInfoArray[18];
    $this->PHPLocale = $LocaleInfoArray[19];
    $this->Encoding = $LocaleInfoArray[20];
    $this->PHPEncoding = $LocaleInfoArray[21];
  }

  function GetInfo($name) {
    return $this->$name;
  }
  
  function GetCCSFormatInfo() {
    if (!$this->FormatInfo)
      $this->FormatInfo = join("|" , Array($this->Name, $this->Language, $this->Country,  join(";", $this->BooleanFormat),
        $this->DecimalDigits, $this->DecimalSeparator, $this->GroupSeparator,
        join(";", $this->MonthNames) ,  join(";", $this->MonthShortNames),
        join(";", $this->WeekdayNames), join(";", $this->WeekdayShortNames),
        join("", $this->ShortDate), join("", $this->LongDate),
        join("", $this->ShortTime), join("", $this->LongTime),       
        $this->FirstWeekDay, $this->AMDesignator, $this->PMDesignator));
    return $this->FormatInfo;
  }
}

//End clsLocaleInfo

//clsLocale Class @0-4E3B1885
class clsLocale {
  public $Name;
  public $Dir;
  public $Ext = ".txt";
  public $ParentLocale;
  public $ParentLocaleName = "";
  public $IsLoaded = false;
  public $LocaleInfo;
  public $Messages;
  public $InternalEncoding = "ISO-8859-1";

  function clsLocale($name, $LocaleInfoArray, $dir = "") {
    $this->Name = $name;
    $this->Dir = $dir;
    $this->Translations = array();
    $this->LocaleInfo = new clsLocaleInfo($name, $LocaleInfoArray);
    $arr = explode("-", $name, 2);
    if (count($arr) == 2)
      $this->ParentLocaleName = $arr[0];
  }

  function LoadTranslation($filename = "") {
    $this->Messages = array();
    $this->Messages["ccs_asc"] = "Ascendente";
    $this->Messages["ccs_bytes"] = "bytes";
    $this->Messages["ccs_cancel"] = "Cancelar";
    $this->Messages["ccs_cannotseek"] = "Regitro n�o encontrado.";
    $this->Messages["ccs_charset"] = "iso-8859-1";
    $this->Messages["ccs_clear"] = "Limpar";
    $this->Messages["ccs_customlinkfield"] = "Detalhes";
    $this->Messages["ccs_customoperationerror_missingparameters"] = "One or more parameters missing to perform the Update/Delete. The application is misconfigured.";
    $this->Messages["ccs_databasecommanderror"] = "Erro em opera��o de banco de dados.";
    $this->Messages["ccs_datepickernav61"] = "Date Picker component is not compatible with Netscape 6.1";
    $this->Messages["ccs_delete"] = "Apagar";
    $this->Messages["ccs_deleteconfirmation"] = "Apagar registro?";
    $this->Messages["ccs_desc"] = "Descendente";
    $this->Messages["ccs_directoryformprefix"] = "Diret�rio";
    $this->Messages["ccs_directoryformsuffix"] = "";
    $this->Messages["ccs_filenotfound"] = "The file {0} specified in {1} was not found.";
    $this->Messages["ccs_filesfoldernotfound"] = "Unable to upload the file specified in {0} - upload folder doesn't exist.";
    $this->Messages["ccs_filter"] = "Filtro";
    $this->Messages["ccs_first"] = "Primeiro";
    $this->Messages["ccs_galleryformprefix"] = "";
    $this->Messages["ccs_galleryformsuffix"] = "Galeria";
    $this->Messages["ccs_gridformprefix"] = "Lista de ";
    $this->Messages["ccs_gridformsuffix"] = "";
    $this->Messages["ccs_gridpagenumbererror"] = "Invalid page number.";
    $this->Messages["ccs_gridpagesizeerror"] = "(CCS06) Invalid page size.";
    $this->Messages["ccs_incorrectemailformat"] = "Valor inv�lido para e-mail em {0}.";
    $this->Messages["ccs_incorrectformat"] = "Valor inv�lido para {0}. Utilize o formato: {1}.";
    $this->Messages["ccs_incorrectphoneformat"] = "N�mero inv�lido para telefone em {0}.";
    $this->Messages["ccs_incorrectvalue"] = "O valor em {0} n�o � v�lido.";
    $this->Messages["ccs_incorrectzipformat"] = "Invalid zip code format in field {0}.";
    $this->Messages["ccs_insert"] = "Adicionar";
    $this->Messages["ccs_insertlink"] = "Criar novo";
    $this->Messages["ccs_insufficientpermissions"] = "Insufficient filesystem permissions to upload the file specified in {0}.";
    $this->Messages["ccs_languageid"] = "en";
    $this->Messages["ccs_largefile"] = "The file size in field {0} is too large.";
    $this->Messages["ccs_last"] = "�ltimo";
    $this->Messages["ccs_localeid"] = "en";
    $this->Messages["ccs_login"] = "Login";
    $this->Messages["ccs_login_autologin_caption"] = "Remember me";
    $this->Messages["ccs_login_form_caption"] = "Login";
    $this->Messages["ccs_loginbtn"] = "Login";
    $this->Messages["ccs_loginerror"] = "Login ou Senha incorreto.";
    $this->Messages["ccs_logoutbtn"] = "Logout";
    $this->Messages["ccs_main"] = "Principal";
    $this->Messages["ccs_maskvalidation"] = "Email inv�lido no campo {0}.";
    $this->Messages["ccs_maximumlength"] = "O valor de {0} deve ter no m�ximo {1} caracter(es).";
    $this->Messages["ccs_maximumvalue"] = "O valor de {0} n�o deve ser maior que {1}.";
    $this->Messages["ccs_minimumlength"] = "O valor de {0} deve ter no m�nimo {1} caracter(es).";
    $this->Messages["ccs_minimumvalue"] = "O valor m�nimo de {0} n�o deve ser menor que {1}.";
    $this->Messages["ccs_more"] = "Mais...";
    $this->Messages["ccs_next"] = "Pr�x.";
    $this->Messages["ccs_nextmonthhint"] = "Pr�x. m�s";
    $this->Messages["ccs_nextquarterhint"] = "Pr�x. trimestre";
    $this->Messages["ccs_nextthreemonthshint"] = "Pr�x. tr�s m�ses";
    $this->Messages["ccs_nextyearhint"] = "Pr�x. ano";
    $this->Messages["ccs_nocategories"] = "Nenhuma catergoria encontrada.";
    $this->Messages["ccs_norecords"] = "N�o encontrado.";
    $this->Messages["ccs_of"] = "de";
    $this->Messages["ccs_operationerror"] = "N�o foi poss�vel fazer  {0}. Um ou mais par�metro(s) n�o especificado(s).";
    $this->Messages["ccs_password"] = "Senha";
    $this->Messages["ccs_previous"] = "Ant.";
    $this->Messages["ccs_prevmonthhint"] = "M�s ant.";
    $this->Messages["ccs_prevquarterhint"] = "�lt. Trimestre";
    $this->Messages["ccs_prevthreemonthshint"] = "Tr�s ult. m�ses";
    $this->Messages["ccs_prevyearhint"] = "Ano ant.";
    $this->Messages["ccs_recordformprefix"] = "Adicionar/Editar";
    $this->Messages["ccs_recordformprefix2"] = "Visualizar";
    $this->Messages["ccs_recordformsuffix"] = "";
    $this->Messages["ccs_recperpage"] = "Registros por p�gina";
    $this->Messages["ccs_rememberlogin"] = "Remember my Login and Password";
    $this->Messages["ccs_reportpagenumber1"] = "P�gina";
    $this->Messages["ccs_reportpagenumber2"] = "de";
    $this->Messages["ccs_reportprintlink"] = "Vers�o para impress�o";
    $this->Messages["ccs_reportsubtotal"] = "Subtotal";
    $this->Messages["ccs_reporttotal"] = "Total Geral";
    $this->Messages["ccs_requiredfield"] = "O valor em  {0} � obrigat�rio.";
    $this->Messages["ccs_requiredfieldupload"] = "The file attachment in field {0} is required.";
    $this->Messages["ccs_requiredsmtpserver_or_dir"] = "Please specify the SMTP server or Pickup directory for the CDO.Message email component.";
    $this->Messages["ccs_search"] = "Search";
    $this->Messages["ccs_selectfield"] = "Select Field";
    $this->Messages["ccs_selectorder"] = "Selecione a ordem";
    $this->Messages["ccs_selectvalue"] = "Selecione um valor";
    $this->Messages["ccs_sortby"] = "Ordenar por";
    $this->Messages["ccs_sortdir"] = "Sentido";
    $this->Messages["ccs_submitconfirmation"] = "Enviar registros?";
    $this->Messages["ccs_tempfoldernotfound"] = "Unable to upload the file specified in {0} - temporary upload folder doesn't exist.";
    $this->Messages["ccs_tempinsufficientpermissions"] = "Insufficient filesystem permissions to upload the file specified in {0} into temporary folder.";
    $this->Messages["ccs_today"] = "Hoje";
    $this->Messages["ccs_totalrecords"] = "Total de registros:";
    $this->Messages["ccs_uniquevalue"] = "O valor no campo {0} j� existe no banco de dados.";
    $this->Messages["ccs_update"] = "Mudar";
    $this->Messages["ccs_uploadcomponenterror"] = "Error occurred while initializing the upload component.";
    $this->Messages["ccs_uploadcomponentnotfound"] = "{0} uploading component {1} is not found. Please install the component or select another one.";
    $this->Messages["ccs_uploadingerror"] = "An error occured when uploading file specified in {0}. Error description: {1}.";
    $this->Messages["ccs_uploadingtempfoldererror"] = "An error occured when uploading file specified in {0} into temporary folder. Error description: {1}.";
    $this->Messages["ccs_wrongtype"] = "The file type specified in field {0} is not allowed.";
    $this->Messages["codcli"] = "";
    $this->Messages["codfat"] = "";
    $this->Messages["codope"] = "CODOPE";
    $this->Messages["coduni"] = "CODUNI";
    $this->Messages["datvnc"] = "";
    $this->Messages["descri"] = "DESCRI";
    $this->Messages["desope"] = "DESOPE";
    $this->Messages["desuni"] = "DESUNI";
    $this->Messages["grp_id"] = "GRP ID";
    $this->Messages["id"] = "ID";
    $this->Messages["issqn"] = "";
    $this->Messages["mesref"] = "";
    $this->Messages["neweditablegrid1"] = "NewEditableGrid1";
    $this->Messages["nrrelemisinc"] = "NRRelEmisInc";
    $this->Messages["passwo"] = "PASSWO";
    $this->Messages["ret_inss"] = "";
    $this->Messages["siste1"] = "SISTE1";
    $this->Messages["siste2"] = "SISTE2";
    $this->Messages["siste3"] = "SISTE3";
    $this->Messages["siste4"] = "SISTE4";
    $this->Messages["siste5"] = "SISTE5";
    $this->Messages["siste6"] = "SISTE6";
    $this->Messages["siste7"] = "SISTE7";
    $this->Messages["sum"] = "Sum: ";
    $this->Messages["tabope_tabuni"] = "TABOPE TABUNI";
    $this->Messages["tabopecoduni"] = "CODUNI";
    $this->Messages["tabopetabuni"] = "TABOPE, TABUNI";
    $this->Messages["tabunicoduni"] = "CODUNI";
    $this->Messages["text1"] = "F";
    $this->Messages["text2"] = "S";
    $this->Messages["text3"] = "Cliente";
    $this->Messages["text4"] = "\"55\"";
    $this->Messages["text5"] = "55";
    $this->Messages["textbox1"] = "TextBox1";
    $this->Messages["textbox2"] = "TextBox2";
    $this->Messages["valfat"] = "";
    $this->Messages["valjur"] = "";
    $this->IsLoaded = true;
  }

  function GetMessage($originalId) {
    global $CCSLocales;
    global $FileEncoding;
    $id = strtolower($originalId);
    if ($id == "ccs_localeid") return $this->Name;
    if ($id == "ccs_languageid") return $this->LocaleInfo->GetInfo("Language");
    if ($id == "ccs_formatinfo") return $this->LocaleInfo->GetCCSFormatInfo();
    
    if (!$this->IsLoaded)
      $this->LoadTranslation();
    if (array_key_exists($id,  $this->Messages)) {
      return $FileEncoding != $this->InternalEncoding && $id != "ccs_formatinfo" ? CCConvertEncoding($this->Messages[$id], $this->InternalEncoding, $FileEncoding) : $this->Messages[$id];
    } else if ($this->ParentLocale) {
      return $this->ParentLocale->GetMessage($id);
    } elseif ($this->ParentLocaleName && array_key_exists($this->ParentLocaleName, $CCSLocales->Locales)) {
      $this->ParentLocale = & $CCSLocales->Locales[$this->ParentLocaleName];
      return $this->ParentLocale->GetMessage($id);
    } elseif (strtolower($CCSLocales->DefaultLocale) != strtolower($this->Name)) {
      $DefaultLocale = $CCSLocales->Locales[$CCSLocales->DefaultLocale];
      return $DefaultLocale->GetMessage($id);  
    } else {
      return $originalId;
    }

  }
}

//End clsLocale Class

//clsLocales Class @0-755429AA
class clsLocales {
  public $Locale;
  public $DefaultLocale;
  public $Locales;
  public $Dir;

  function clsLocales($dir, $locale = "")  {
    $this->Dir = $dir;
    $this->Locale = $locale;
    $this->DefaultLocale = "";
    $this->Locales = array();
  }

  function Init() {
  }

  function AddLocale($name, $LocaleInfoArray) {
    $lname = strtolower($name);
    if (array_key_exists($lname, $this->Locales))
      return;
    $this->Locales[$lname] = new clsLocale($name, $LocaleInfoArray, $this->Dir);
  }

  function GetText($id, $params = Null, $locale = "") {
    if ($locale == "")  
      $locale = $this->Locale;
    if ($locale == "")  
      $locale = $this->DefaultLocale;
    if (!array_key_exists($locale, $this->Locales))
      return "";
    $Result = $this->Locales[$locale]->GetMessage($id);
    if ($Result != "") {
      $Result = preg_replace("/\\\\n/", "\n", $Result);
      $Result = preg_replace("/\\\\/", "\\", $Result);
      if (is_array($params)) {
        for ($i = 0; $i < count($params); $i++)
          $Result = preg_replace("/\{$i\}/", $params[$i], $Result);
      } elseif (!is_null($params)) {
          $Result = preg_replace("/\{0}/", $params, $Result);
      }
    }
    return $Result;
  }

  function GetFormatInfo($name, $locale = "") {
    if ($locale == "")  
      $locale = $this->Locale;
    if ($locale == "")  
      $locale = $this->DefaultLocale;
    return $this->Locales[$locale]->LocaleInfo->GetInfo($name);
  }

  function cmp($a, $b) {
    if ($a == $b) {
        return 0;
    }
    return ($a > $b) ? -1 : 1;
  }

  function FindLocale($locale) {
    $locale = strtolower($locale);
    if (!$this->Locale && $locale) {
      $arr = explode("-", $locale, 2);        
      $lang = $arr[0];
      $country = isset($arr[1]) ? $arr[1] : "";
      $defaultCountry = array_key_exists($lang, $this->Locales) ? strtolower($this->Locales[$lang]->LocaleInfo->GetInfo("Country")) : "";
      if (!$country && $defaultCountry && array_key_exists($lang . "-" . $defaultCountry, $this->Locales)) 
        return $lang . "-" . $defaultCountry;
      elseif ($country && !array_key_exists($locale, $this->Locales) && array_key_exists($lang . "-" . $defaultCountry, $this->Locales)) 
        return $lang . "-" . $defaultCountry;
      elseif (array_key_exists($locale, $this->Locales))
        return $locale;
      elseif (array_key_exists($lang, $this->Locales))
        return $lang;
    }
    return false;
  }

  function SetLocale($locale) {
    if (!$this->Locale && $locale) {
      $this->Locale = $this->FindLocale($locale);
      if (!$this->Locale) 
        $this->Locale = $this->DefaultLocale;
    }
  }

  function  SetLocaleFromHttpHeader($Name = "HTTP_ACCEPT_LANGUAGE") {
    if ($this->Locale)
      return false;
    $Locales = array();
    $locale = "";
    $q = "";
    if (!isset($_SERVER[$Name])) return;
    $arr = explode(",", strtolower($_SERVER[$Name]));
    foreach ($arr as $L) {
      if(preg_match("/(.+);q=(\\d+(\\.\\d+)?)/", $L, $matches)) {
        $locale = $matches[1];
        $q = doubleval($matches[2]);
      } else {
        $locale = $L;
        $q = 1;
      }
      if (!array_key_exists(strval($q), $this->Locales))
        $Locales[strval($q)] = array();
      array_push($Locales[strval($q)], $locale);
    }
    uksort($Locales, array($this, "cmp"));

    foreach ($Locales as $q) {
      foreach ($q as $locale) {
        if ($result = $this->FindLocale($locale)) {
          $this->Locale = $result;
          return;
        }
      }
    }
  }

}


//End clsLocales Class

//clsMainPage Class @0-90640A0C
class clsMainPage
{
  public $ComponentType = "Page";
  public $Parent = false;
  public $Connections = array();
  public $Attributes = array();
}
//End clsMainPage Class

//clsAttribute class @0-B817DF6F
class clsAttribute {
  public  $DataType = ccsText;
  public  $Format = "";
  public  $Name = "";
  public  $Prefix = "";

  public  $Value;
  public  $Text;

  function clsAttribute($Name, $Prefix, $DataType="", $Format = "") {
    $this->Name = $Name;
    $this->Prefix = $Prefix;
    if ($this->DataType)
      $this->DataType = $DataType;
    $this->Format = $Format;
  }

  function GetParsedValue($ParsingValue, $MaskFormat) {
    return CCParseValue($ParsingValue, $MaskFormat, $this->DataType, "", "");
  }


  function GetFormattedValue($MaskFormat) {
      return CCFormatValue($this->Value, $MaskFormat, $this->DataType);
  }  

  function Show() {
    global $Tpl;
    $Tpl->SetVar($this->Prefix . $this->Name, $this->GetText());
  }

  function SetValue($NewValue) {
    $this->Text = null;
    $this->Value = $NewValue;
  }

  function GetValue() {
    return $this->Value;
  }

  function SetText($NewText) {
    $this->Text = $NewText;
    $this->Value = $this->GetParsedValue($NewText, $this->Format);
  }

  function GetText() {
    if (is_null($this->Text))
      $this->Text = $this->GetFormattedValue($this->Format);
    return $this->Text;
  }

}
//End clsAttribute class

//clsAttributes class @0-F451BD8E
class clsAttributes {
  public $Objects = array();
  public $Block = "";
  public $Accumulate = "";
  public $Prefix = "";

  function clsAttributes($Prefix) {
    $this->Prefix = $Prefix;
  }

  function Add(& $Attr) {
    $this->Objects[$Attr->Name] = & $Attr;
  }

  function AddAttribute($Name, $DataType = "", $Format = "") {
    $this->Objects[$Name] = new clsAttribute($Name, $this->Prefix, $DataType, $Format);
  }

  function GetValue($Name) {
    return array_key_exists($Name, $this->Objects) ? $this->Objects[$Name]->GetValue() : "";
  }

  function GetText($Name) {
    return array_key_exists($Name, $this->Objects) ? $this->Objects[$Name]->GetText() : "";
  }

  function SetValue($Name, $NewValue, $DataType = "", $Format = "") {
    if (!array_key_exists($Name, $this->Objects))
      $this->AddAttribute($Name, $DataType, $Format);
    $this->Objects[$Name]->SetValue($NewValue);
  }

  function SetText($Name, $NewText) {
    if (!array_key_exists($Name, $this->Objects))
      $this->AddAttribute($Name);
    $this->Objects[$Name]->SetText($NewText);
  }

  function Show() {
    foreach ($this->Objects as $Name => $Attribute) 
        $this->Objects[$Name]->Show();
  }

  function Clear() {
    $this->Objects = array();
  }

  function GetAsArray() {
    $arr = array();
    foreach ($this->Objects as $Name => $Value) {
      $arr[$Name] = array($this->Objects[$Name]->GetValue(), $this->Objects[$Name]->GetText(), $this->Objects[$Name]->DataType, $this->Objects[$Name]->Format);
    }
    $arr["."] = $this->Prefix;
    return $arr;
  }

  function RestoreFromArray($Arr) {
    $this->Objects = array();
    $this->Prefix = $Arr["."];
    $this->AddFromArray($Arr);
  }

  function AddFromArray($Arr) {
    foreach ($Arr as $Name => $Value) {
      if ($Name != ".") {
        $this->Objects[$Name] = new clsAttribute($Name, $this->Prefix, $Value[2], $Value[3]);
        $this->Objects[$Name]->Value = $Value[0];
        $this->Objects[$Name]->Text = $Value[1];
      }
    }
  }

}
//End clsAttributes class


?>
