#File Description @0-0C2FF42A
package Classes;

#======================================================
#
#  This file contains the following classes:
#    package clsSQLParameters
#    package clsSQLParameter
#    package clsControl
#    package clsField
#    package clsButton
#    package clsDatePicker
#    package clsErrors
#
#======================================================
#End File Description

#clsSQLParameters Class @0-C632A377

package clsSQLParameters;
use Common;

sub new {
  my ($class, $ErrorBlock)  = @_;
  my $self                  = {};
  $self->{Connection}       = undef;
  $self->{Criterion}        = [];
  $self->{AssembledWhere}   = undef;
  $self->{Errors}           = undef;
  $self->{DataSource}       = undef;
  $self->{AllParametersSet} = undef;
  $self->{Parameters}       = {};
  $self->{ErrorBlock}       = $ErrorBlock;
  bless $self, $class;
  return $self;
}

sub SetParameters {
  my ($self, $Name, $NewParam) = @_;
  $self->{Parameters}->{$Name} = $NewParam;
}

sub AddParameter {
  my ($self, $ParameterID, $ParameterSource, $DataType, $Format, $DBFormat, $InitValue, $DefaultValue, $UseIsNull) = @_; 
  $self->{Parameters}->{$ParameterID} = clsSQLParameter->new($ParameterSource, $DataType, $Format, $DBFormat, $InitValue, $DefaultValue, $UseIsNull, $self->{ErrorBlock});
}

sub AllParamsSet {
  my $self = shift;
  my $blnResult = 1;
  while ( $blnResult && ( my($key, $Parameter) = each (%{$self->{Parameters}}) ) ) {
    if ( !length($Parameter->GetValue) && !$Parameter->{UseIsNull}) {
      $blnResult = 0;
    }
  }
  return $blnResult;
}

sub GetDBValue {
  my ($self, $ParameterID) = @_;
  return $self->{Parameters}->{$ParameterID}->GetDBValue;
}

sub opAND {
  shift;
  my ($Brackets, $strLeft, $strRight) = @_;
  my $strResult = "";
  if ( length($strLeft) ) {
      if ( length($strRight) ) {
        $strResult = $strLeft . " AND " . $strRight;
        if ( $Brackets ) {
          $strResult = " (" . $strResult . ") ";
        }
      } else {
        $strResult = $strLeft;
      }
  } else {
    if ( length($strRight) ) {
        $strResult = $strRight; 
    }
  }
  return $strResult;
}

sub opOR {
  shift;
  my ($Brackets, $strLeft, $strRight) = @_;
  my $strResult = "";
  if ( length($strLeft) ) {
    if (length($strRight)) {
      $strResult = $strLeft . " OR " . $strRight;
      if ($Brackets) { 
        $strResult = " (" . $strResult . ") ";
      }
    } else {
      $strResult = $strLeft;
    }
  } else {
    if (length($strRight)) {
      $strResult = $strRight;
    } 
  }
  return $strResult;
}

sub Operation { 
  my ($self, $Operation, $FieldName, $DBValue, $SQLText, $UseIsNull) = @_;
  $UseIsNull ||= 0; 
  my $Result = "";
  if( length($DBValue) ) {
    my $SQLValue = $SQLText;
    if ( substr($SQLValue, 0, 1) eq "'" ) {
      $SQLValue = substr($SQLValue, 1, length($SQLValue) - 2);
    }
    if ( $Operation eq $opEqual ) {
      $Result = $FieldName . " = " . $SQLText;
    } elsif ($Operation eq $opNotEqual ) {
      $Result = $FieldName . " <> " . $SQLText;
    } elsif ($Operation eq $opLessThan ) {
      $Result = $FieldName . " < " . $SQLText;
    } elsif ($Operation eq $opLessThanOrEqual ) {
       $Result = $FieldName . " <= " . $SQLText;
    } elsif ($Operation eq $opGreaterThan ) {
       $Result = $FieldName . " > " . $SQLText;
    } elsif ($Operation eq $opGreaterThanOrEqual ) {
       $Result = $FieldName . " >= " . $SQLText;
    } elsif ($Operation eq $opBeginsWith ) {
       $Result = $FieldName . " like '" . $SQLValue . "%'";
    } elsif ($Operation eq $opNotBeginsWith ) {
       $Result = $FieldName . " not like '" . $SQLValue . "%'";
    } elsif ($Operation eq $opEndsWith ) {
       $Result = $FieldName . " like '%" . $SQLValue . "'";
    } elsif ($Operation eq $opNotEndsWith ) {
       $Result = $FieldName . " not like '%" . $SQLValue . "'";
    } elsif ($Operation eq $opContains ) {
       $Result = $FieldName . " like '%" . $SQLValue . "%'";
    } elsif ($Operation eq $opNotContains ) {
       $Result = $FieldName . " not like '%" . $SQLValue . "%'";
    } elsif ($Operation eq $opIsNull ) {
       $Result = $FieldName . " IS NULL";
    } elsif ($Operation eq $opNotNull ) {
       $Result = $FieldName . " IS NOT NULL";
    }
  } elsif ($UseIsNull) {
    if ( $Operation eq $opEqual || $Operation eq $opLessThan || $Operation eq $opLessThanOrEqual 
      || $Operation eq $opGreaterThan || $Operation eq $opGreaterThanOrEqual 
      || $Operation eq $opBeginsWith || $Operation eq $opEndsWith 
      || $Operation eq $opContains || $Operation eq $opIsNull ) {
      $Result = $FieldName . " IS NULL";
    } elsif ($Operation eq $opNotEqual || $Operation eq $opNotBeginsWith || $Operation eq $opNotEndsWith
      || $Operation eq $opNotContains || $Operation eq $opNotNull) {
      $Result = $FieldName . " IS NOT NULL";
    } 

  }
  return $Result;
}

#End clsSQLParameters Class

#clsSQLParameter Class @0-030D8EDD

package clsSQLParameter;
use vars qw/@ISA/;
use Common;
@ISA = qw/clsErrors/;

sub new {
  my ($class, $ParameterSource, $DataType, $Format, $DBFormat, $InitValue, $DefaultValue, $UseIsNull, $ErrorBlock) = @_;
  my $self          = {};
  bless $self, $class;
  $InitValue = defined($InitValue) ? $InitValue : "";

  $self->{Errors}     = clsErrors->new();
  $self->{ErrorBlock} = $ErrorBlock;

  $self->{UseIsNull} = $UseIsNull;
  $self->{DataType}  = $DataType;
  $self->{Format}    = $Format;
  $self->{DBFormat}  = $DBFormat;
  $self->{Caption}   = $ParameterSource;
  $self->{Link}      = undef; 
  $self->{Value}     = ""; 
  $self->{DBValue}   = "";
  $self->{Text}      = "";

  if ( ref($InitValue) eq "ARRAY" && $DataType != $ccsDate) {
    $self->SetText(join(",",@{$InitValue}));
  } elsif ( length($InitValue) ) {
    $self->SetText($InitValue);
  } else {
    $self->SetText($DefaultValue);
  }

  return $self;
}

sub GetParsedValue {
  my ($self, $ParsingValue, $Format) = @_;
  my $varResult = "";
  if ( length($ParsingValue) ) {
    if ( $self->{DataType} == $ccsDate ) {
      my $DateValidation = 1;
      if (CCValidateDateMask($ParsingValue, @{$Format})) {
        $varResult = CCParseDate($ParsingValue, @{$Format});
        $error = $@;
        if($error || !CCValidateDate($varResult)) {
          $DateValidation = 0;
          $varResult = "";
        }
      } else {
        $DateValidation = 0;
      } 
      if (!$DateValidation) {
        if ( ref($self->{Format}) eq "ARRAY") {
          my $FormatString = join("", @{$self->{Format}});
          $self->{Errors}->addError("The value in field " . $self->{Caption} . " is not valid. Use the following format: " . $FormatString . ". (" . CCToHTML($ParsingValue) . ")");
        } else {
          $self->{Errors}->addError("The value in field " . $self->{Caption} . " is not valid. (" . CCToHTML($ParsingValue) . ")");
        }
      } 
    } elsif ( $self->{DataType} == $ccsBoolean ) {
      if ( CCValidateBoolean($ParsingValue, @{$Format}) ) {
        $varResult = CCParseBoolean($ParsingValue, @{$Format});
      } else {
        if ( ref($self->{Format}) eq "ARRAY") {
          my $FormatString = CCGetBooleanFormat(@{$self->{Format}});
          $self->{Errors}->addError("The value in field " . $self->{Caption} . " is not valid. Use the following format: " . $FormatString . ". (" . CCToHTML($ParsingValue) . ")");
        } else {
          $self->{Errors}->addError("The value in field " . $self->{Caption} . " is not valid. (" . CCToHTML($ParsingValue) . ")");
        }
      }
    } elsif ( $self->{DataType} == $ccsInteger ) {
       if (CCValidateNumber($ParsingValue, @{$Format})) {
         $varResult = CCParseInteger($ParsingValue, @{$Format});
       } else  {
         $self->{Errors}->addError("The value in field " . $self->{Caption} . " is not valid. (" . CCToHTML($ParsingValue) . ")");
       }
    } elsif ( $self->{DataType} == $ccsFloat ) {
      if (CCValidateNumber($ParsingValue, @{$Format}) ) {
        $varResult = CCParseFloat($ParsingValue, @{$Format});
      } else {
        $self->{Errors}->addError("The value in field " . $self->{Caption} . " is not valid. (" . CCToHTML($ParsingValue) . ")");
      }
    } elsif ( $self->{DataType} == $ccsText or $self->{DataType} == $ccsMemo ) {
      $varResult = $ParsingValue;
    }

    if ( $self->{Errors}->Count() > 0) {
      if ( length($self->{ErrorBlock}) ) {
        $Tpl->replaceblock($self->{ErrorBlock}, $self->{Errors}->ToString() );
      } else {
        print_http_header();
        print $self->{Errors}->ToString();
      }
    }
  }
  return $varResult;
}

sub GetFormattedValue {
  my ($self, $Format) = @_;
  my $strResult = "";
  if ( $self->{DataType} == $ccsDate ) {
    if( length($self->{Value}) ) {
      $strResult = CCFormatDate($self->{Value}, @{$Format} );
    }
  } elsif ( $self->{DataType} == $ccsBoolean ) {
     $strResult = CCFormatBoolean($self->{Value}, @{$Format} );
  } elsif ( $self->{DataType} == $ccsInteger or $self->{DataType} == $ccsFloat) {
     $strResult = CCFormatNumber($self->{Value}, @{$Format} );
  } elsif ( $self->{DataType} == $ccsText or $self->{DataType} == $ccsMemo ) {
     $strResult = $self->{Value};
  }
  return $strResult;
}

sub SetValue {
  my ($self, $Value) = @_;
  if ($self->{DataType} == $ccsBoolean) {
    if ($Value eq "true" || $Value eq "1") {
      $Value = 1;
    } elsif ($Value eq "false" || $Value eq "0") {
      $Value = 0;
    }
  }
  $self->{Value} = $Value;
  $self->{Text} = $self->GetFormattedValue($self->{Format});
  $self->{DBValue} = $self->GetFormattedValue($self->{DBFormat});
}

sub SetText {
  my ($self, $Text) = @_;
  if (CCCheckValue($Text, $self->{DataType})) {
    $self->SetValue($Text);
  } else {
    $self->{Text} = $Text;
    $self->{Value} = $self->GetParsedValue($self->{Text}, $self->{Format});
    $self->{DBValue} = $self->GetFormattedValue($self->{DBFormat});
  }
}

sub SetDBValue {
  my ($self, $DBValue) = @_;
  $self->{DBValue} = $DBValue;
  $self->{Value} = $self->GetParsedValue($self->{DBValue}, $self->{DBFormat});
  $self->{Text} = $self->GetFormattedValue($self->{Format});
}

sub GetValue {
  my $self = shift;
  return $self->{Value};
}

sub GetText {
  my $self = shift;
  return $self->{Text};
}

sub GetDBValue {
  my $self = shift;
  return $self->{DBValue};
}


#End clsSQLParameter Class

#clsControl Class @0-2BC6ECD1

package clsControl;
use vars qw/@ISA/;
use Common;
@ISA = qw/clsErrors/;

sub new {

  my ($class, $ControlType, $Name, $Caption, $DataType, $Format, $InitValue) = @_;
  
  my $self                = {};

  $self->{DataType}       = $DataType;
  $self->{DSType}         = $dsEmpty;
  $self->{Caption}        = $Caption;
  $self->{ControlType}    = $ControlType;
  $self->{Name}           = $Name;
  $self->{Format}         = $Format; 
  $self->{DBFormat}       = undef;   
  $self->{CheckedValue}   = undef;
  $self->{UncheckedValue} = undef;
  $self->{State}          = undef;
  $self->{Parameters}     = "";
  $self->{Page}           = "";
  $self->{Value}          = "";
  $self->{CCSEventResult} = undef;
  $self->{Text}           = "";
  $self->{BlockName}      = $ControlTypes[$ControlType] . " " . $Name;
  $self->{Visible}        = 1;
  $self->{Values}         = {};
  $self->{BoundColumn}    = "";
  $self->{TextColumn}     = "";
  $self->{CCSEvents}      = {};
  $self->{Required}       = 0;
  $self->{HTML}           = 0;
  $self->{Multiple}       = 0;
  $self->{Errors}         = clsErrors->new();
  
  bless $self, $class;

  if ( ref($InitValue) eq "ARRAY" ) {
    $self->{Value} = $InitValue;
  } elsif ( length($InitValue)) {
    if($self->{ControlType} == $ccsCheckBox) {
	  $self->{Value} = 1;
	  $self->{Text} = $InitValue;
	} else {
      $self->SetText($InitValue);
	}
  }

  return $self;
}

sub Validate {
  my $self = shift;
  my $validation = 0;
  if ( $self->{Required} && $self->{Value} eq "" && $self->{Errors}->Count() == 0 ) {
    my $FieldName = length($self->{Caption}) ? $self->{Caption} : $self->{Name};
    $self->{Errors}->addError("The value in field " . $FieldName . " is required.");
  }
  $self->{CCSEventResult} = CCGetEvent( $self->{CCSEvents}, "OnValidate" );
  return ($self->{Errors}->Count() == 0);
}

sub GetParsedValue {
  my ($self, $ParsingValue) = @_;
  $ParsingValue = defined($ParsingValue) ? $ParsingValue : "";
  my $varResult = "";

  if ( $self->{Multiple} && ref($ParsingValue) eq "ARRAY" ) {
    $ParsingValue = $ParsingValue->[0];
  }
  if ( CCCheckValue($ParsingValue, $self->{DataType}) ) {
    if($self->{DataType} == $ccsBoolean) {
      $ParsingValue = (lc($ParsingValue) eq "true" || $ParsingValue eq "1") ? 1 : 0;
    }
    $varResult = $ParsingValue;
  } elsif (length($ParsingValue)) {
    if ( $self->{DataType} == $ccsDate ) {
      my $DateValidation = 1;
      if ( CCValidateDateMask($ParsingValue, @{$self->{Format}}) ) {
        $varResult = CCParseDate($ParsingValue, @{$self->{Format}}); 
        my $error = $@;
        if($error || !CCValidateDate($varResult)) {
          $DateValidation = 0;
          $varResult = "";
        }
      } else { 
        $DateValidation = 0;
      }
      if (!$DateValidation && $self->{Errors}->Count() == 0) { 
        if ( (@{$self->{Format}}) ) { 
          my $FormatString = join("", @{$self->{Format}});
          $self->{Errors}->addError("The value in field " . $self->{Caption} . " is not valid. Use the following format: " . $FormatString . ".");
        } else {
          $self->{Errors}->addError("The value in field " . $self->{Caption} . " is not valid.");
        }
      }
    } elsif ( $self->{DataType} == $ccsBoolean ) {
      if ( CCValidateBoolean( $ParsingValue, @{$self->{Format}} ) ) {
        $varResult = CCParseBoolean( $ParsingValue, @{$self->{Format}});
      } elsif ($self->{Errors}->Count() == 0) { 
        if ( (@{$self->{Format}}) ) { 
          my $FormatString = CCGetBooleanFormat(@{$self->{Format}});
          $self->{Errors}->addError("The value in field " . $self->{Caption} . " is not valid. Use the following format: " . $FormatString . ".");
        } else {
          $self->{Errors}->addError("The value in field " . $self->{Caption} . " is not valid.");
        }
      }
    } elsif ( $self->{DataType} == $ccsInteger ) {
       if ( CCValidateNumber($ParsingValue, @{$self->{Format}}) ) {
         $varResult = CCParseInteger($ParsingValue, @{$self->{Format}})
       } elsif ($self->{Errors}->Count() == 0) { 
         $self->{Errors}->addError("The value in field " . $self->{Caption} . " is not valid.")
       }
    } elsif ( $self->{DataType} == $ccsFloat ) {
       if ( CCValidateNumber( $ParsingValue, @{$self->{Format}} ) ) {
         $varResult = CCParseFloat($ParsingValue, @{$self->{Format}})
       } elsif ($self->{Errors}->Count() == 0) { 
         $self->{Errors}->addError("The value in field " . $self->{Caption} . " is not valid.")
       }
    } elsif ( $self->{DataType} == $ccsText or $self->{DataType} == $ccsMemo ) {
      $varResult = $ParsingValue;
    }
  }
  return $varResult;
}

sub GetFormattedValue {
  my $self = shift;
  my $strResult = "";
  if ( $self->{DataType} == $ccsDate ) {
    if( length($self->{Value}) ) {
      $strResult = CCFormatDate($self->{Value}, @{$self->{Format}});
    }
  } elsif ( $self->{DataType} == $ccsBoolean ) {
    $strResult = CCFormatBoolean($self->{Value}, @{$self->{Format}});
  } elsif ( $self->{DataType} == $ccsInteger or $self->{DataType} == $ccsFloat ) {
    $strResult = CCFormatNumber($self->{Value}, @{$self->{Format}});
  } elsif ( $self->{DataType} == $ccsText or $self->{DataType} == $ccsMemo ) {
    $strResult = $self->{Value};
  }
  return $strResult;
}

sub Prepare {
  my $self = shift;
  if ( $self->{DSType} == $dsTable || $self->{DSType} == $dsSQL || $self->{DSType} == $dsProcedure) {
    $self->{BoundColumn} = 0 if(!length($self->{BoundColumn}));
    $self->{TextColumn}  = 1 if(!length($self->{TextColumn}));
    $self->{EventResult} = CCGetEvent($self->{ds}->{CCSEvents}, "BeforeBuildSelect");
    $self->{EventResult} = CCGetEvent($self->{ds}->{CCSEvents}, "BeforeExecuteSelect");
    my $FieldName = length($self->{Caption}) ? $self->{Caption} : $self->{Name};
    $self->{Values}      = CCGetListValues($self->{ds}, $self->{ds}->{SQL}, $self->{ds}->{Where}, $self->{ds}->{Order}, $self->{BoundColumn}, $self->{TextColumn}, $self->{DataType}, $self->{Errors}, $FieldName, @{$self->{DBFormat}});
    $self->{ds}->finalize();
    $self->{EventResult} = CCGetEvent($self->{ds}->{CCSEvents}, "AfterExecuteSelect");
  }
}

sub Show {
  my ($self, $RowNumber) = @_;
  my ($Selected, $SelectedValue);

  $RowNumber = defined($RowNumber) ? $RowNumber : ""; 

  $self->{EventResult} = CCGetEvent($self->{CCSEvents}, "BeforeShow" );
  if(!$self->{Visible}) {
    $Tpl->setvar($self->{Name}, "");
    if($Tpl->blockexists($self->{BlockName})) {
      $Tpl->setblockvar($self->{BlockName}, "");
    }
    return;
  }

  my $ControlName = ($RowNumber eq "") ? $self->{Name} : $self->{Name} . "_" . $RowNumber;

  $Tpl->setvar( $self->{Name} . "_Name", $ControlName);

  if ( $self->{ControlType} eq $ccsLabel) {
    if ( $self->{HTML} ) {
      $Tpl->setvar( $self->{Name}, $self->GetText );
    } else {
      my $str = CCToHTML($self->GetText);
      $str =~ s/\n/<BR>/g;
      $Tpl->setvar( $self->{Name}, $str );
    }
    $Tpl->parsesafe($self->{BlockName}, "false");
  } elsif ( $self->{ControlType} eq $ccsTextBox or $self->{ControlType} eq $ccsTextArea or $self->{ControlType} eq $ccsImage or $self->{ControlType} eq $ccsHidden ) {
    $Tpl->setvar($self->{Name}, CCToHTML($self->GetText));
    $Tpl->parsesafe($self->{BlockName}, "false");
  } elsif ( $self->{ControlType} eq $ccsLink ) {
    if ($self->{HTML}) {
      $Tpl->setvar($self->{Name}, $self->GetText)
    } else {
       my $str = CCToHTML($self->GetText);
       $str =~ s/\n/<BR>/g;
       $Tpl->setvar( $self->{Name}, $str );
    } 
    $Tpl->setvar($self->{Name} . "_Src", $self->GetLink());
    $Tpl->parsesafe($self->{BlockName}, "false");
  } elsif ( $self->{ControlType} eq $ccsImageLink ) {
    $Tpl->setvar($self->{Name} . "_Src", CCToHTML($self->GetText));
    $Tpl->setvar($self->{Name}, $self->GetLink());
    $Tpl->parsesafe($self->{BlockName}, "false");
  } elsif ( $self->{ControlType} eq $ccsCheckBox ) {
    if ( $self->{Value} ) {
      $Tpl->setvar($self->{Name}, "CHECKED")
    } else {
      $Tpl->setvar($self->{Name}, "")
    }
    $Tpl->parsesafe($self->{BlockName}, "false");

  } elsif ( $self->{ControlType} eq $ccsCheckBoxList ) {
    my $BlockToParse = "CheckBoxList " . $self->{Name};
    $Tpl->setblockvar($BlockToParse, "");
    if ( ref($self->{Values}) eq "ARRAY" ) {
      for ( my $i = 0; $i <= $#{$self->{Values}}; $i++) {
        my $Value = $self->{Values}[$i][0];
        my $TextValue = CCToHTML(CCFormatValue($Value, $self->{DataType}, @{$self->{Format}}));
        my $Text = $self->{HTML} ? $self->{Values}[$i][1] : CCToHTML($self->{Values}[$i][1]);
        my $Selected = "";
        if ($self->{Multiple} && (ref($self->{Value}) eq 'ARRAY')) {
          foreach my $Val (@{$self->{Value}}) {
            if (CCCompareValues($Value,$Val, $self->{DataType}, $self->{Format}) == 0) {
              $Selected = " CHECKED";
              last;  
            }
          }
	} else {
          $Selected = (CCCompareValues($Value,$self->{Value}, $self->{DataType}, $self->{Format}) == 0) ? " CHECKED" : "";
        }

        $Tpl->setvar("Value", $TextValue);
        $Tpl->setvar("Check", $Selected);
        $Tpl->setvar("Description", $Text);
        $Tpl->parse($BlockToParse, "true");
      }
    }
  } elsif ( $self->{ControlType} eq $ccsRadioButton ) {
     my $BlockToParse = "RadioButton " . $self->{Name};
     $Tpl->setblockvar($BlockToParse, "");
     if ( ref($self->{Values}) eq "ARRAY" ) {
       for ( my $i = 0; $i <= $#{$self->{Values}}; $i++) {
         my $Value = $self->{Values}[$i][0];
         my $TextValue = CCToHTML(CCFormatValue($Value, $self->{DataType}, @{$self->{Format}}));
         my $Text = $self->{HTML} ? $self->{Values}[$i][1] : CCToHTML($self->{Values}[$i][1]);
         my $Selected = CCCompareValues($Value, $self->{Value}, $self->{DataType}, $self->{Format}) == 0 ? " CHECKED" : "";
         $Tpl->setvar("Value", $TextValue);
         $Tpl->setvar("Check", $Selected);
         $Tpl->setvar("Description", $Text);
         $Tpl->parse($BlockToParse, "true");
       }
     }
  } elsif ( $self->{ControlType} eq $ccsListBox ) {
      my $Options = "";
      if ( ref($self->{Values}) eq "ARRAY" ) {
        for ( my $i = 0; $i <= $#{$self->{Values}}; $i++) {
          my $Value = $self->{Values}[$i][0];
          my $TextValue = CCToHTML(CCFormatValue($Value, $self->{DataType}, @{$self->{Format}}));
          my $Text  = CCToHTML($self->{Values}[$i][1]);
          my $Selected = "";
          if ($self->{Multiple} && (ref($self->{Value}) eq 'ARRAY')) {
            foreach my $Val (@{$self->{Value}}) {
              if (CCCompareValues($Value,$Val, $self->{DataType}, $self->{Format}) == 0) {
               $Selected = " SELECTED";
                last;  
              }
          }
  	  } else {
            $Selected = (CCCompareValues($Value,$self->{Value}, $self->{DataType}, $self->{Format}) == 0) ? " SELECTED" : "";
          }
          $Options .= "<OPTION VALUE=\"" . $TextValue . "\"" . $Selected . ">" . $Text . "</OPTION>\n";
        }
      }
      $Tpl->setvar($self->{Name} . "_Options", $Options);
      $Tpl->parsesafe($self->{BlockName}, "false");
  }
}

sub SetValue {
  my ($self, $Value) = @_;
  
  if($self->{ControlType} == $ccsCheckBox) {
    $self->{Value} = CCCompareValues($Value, $self->{CheckedValue}, $self->{DataType}) == 0 || (CCCompareValues($Value, $self->{UncheckedValue}, $self->{DataType}) != 0 && $Value) ? 1 : 0;
  } else {
    $self->{Value} = $Value;
  }
  
  $self->{Text} = $self->GetFormattedValue;
}

sub SetText {
  my ($self, $Text) = @_;
  $Text = defined($Text) ? $Text : ""; 
  if (CCCheckValue($Text, $self->{DataType})) {
    $self->SetValue($Text);
  } else {
    $self->{Text} = $Text;
    $self->{Value} = $self->GetParsedValue($self->{Text});
  }
}

sub GetValue {
  my $self = shift;
  my $value;
  if ( $self->{ControlType} == $ccsCheckBox ) {
    $value = ($self->{Value}) ? $self->{CheckedValue} : $self->{UncheckedValue};
  } elsif ( $self->{Multiple} && ref($self->{Value}) eq "ARRAY" ) {
    $value = $self->{Value}[0];
  } else {
    $value = $self->{Value};
  }
  return $value;
}

sub GetText {
  my $self = shift;
  if( !length($self->{Text}) ) {
    $self->{Text} = $self->GetFormattedValue;
  }
  return $self->{Text};
}

sub GetLink {
  my $self = shift;
  
  if ( substr($self->{Page}, 0, 2) eq "./" ) {
    $self->{Page} = substr($self->{Page}, 2);
  }

  if ( $self->{Parameters} eq "" ) {
    return $self->{Page};
  } else {
    return $self->{Page} . "?" . $self->{Parameters};
  }
}

sub SetLink {
  my ($self, $Link) = @_;
  my @parsedLink;
  if ( !length($Link) ) {
    $self->{Page} = "";
    $self->{Parameters} = "";
  } else {
    @parsedLink = split /"?"/, $Link;
    $self->{Page} = $parsedLink[0];
    if ( ($#parsedLink + 1) == 2) {
      $self->{Parameters} = $parsedLink[1];
    } else {
      $self->{Parameters} = "";
    }
  }
}

#End clsControl Class

#clsField Class @0-6E393104

package clsField;
use Common;

sub new {
  my ($class, $Name, $DataType, $DBFormat) = @_;
  my $self             = {};
  $self->{Value}       = "";
  $self->{DBValue}     = "";
  $self->{Name}        = $Name;
  $self->{DataType}    = $DataType;
  $self->{DBFormat}    = $DBFormat;
  $self->{Errors}      = clsErrors->new();
  bless $self, $class;
  return $self;
}

sub GetParsedValue {
  my $self = shift;
  my $varResult = "";
  if ( defined($self->{DBValue}) && length($self->{DBValue}) ) {
      if ($self->{DataType} == $ccsDate) {
        my $DateValidation = 1;
        if ( CCValidateDateMask( $self->{DBValue}, @{$self->{DBFormat}} ) ) {
          $varResult = CCParseDate($self->{DBValue}, @{$self->{DBFormat}});
          $error = $@;
          if($error || !CCValidateDate($varResult))
          {
            $DateValidation = 0;
            $varResult = "";
          }
        } else { 
          $DateValidation = 0;
        }   
        if (!$DateValidation) { 
          if (ref($self->{DBFormat}) eq "ARRAY" ) {
            my $FormatString = join("", @{$self->{DBFormat}});
            $self->{Errors}->addError("The value in field " . $self->{Name} . " is not valid. Use the following format: " . $FormatString . ".");
          } else {
            $self->{Errors}->addError("The value in field " . $self->{Name} . " is not valid.");
          }
        }   
      } elsif ($self->{DataType} == $ccsBoolean) {
        if ( CCValidateBoolean($self->{DBValue}, @{$self->{DBFormat}}) ) {
          $varResult = CCParseBoolean($self->{DBValue}, @{$self->{DBFormat}})
        } else {
          if (ref($self->{DBFormat}) eq "ARRAY" ) {
            my $FormatString = CCGetBooleanFormat(@{$self->{DBFormat}});
            $self->{Errors}->addError("The value in field " . $self->{Name} . " is not valid. Use the following format: " . $FormatString . ".");
          } else {
            $self->{Errors}->addError("The value in field " . $self->{Name} . " is not valid.");
          }
        }
      } elsif ($self->{DataType} == $ccsInteger) { 
          if ( CCValidateNumber($self->{DBValue}, @{$self->{DBFormat}} ) ) {
            $varResult = CCParseInteger($self->{DBValue}, @{$self->{DBFormat}} );
          } else {
            $self->{Errors}->addError("The value in field " . $self->{Name} . " is not valid.");
          }
      } elsif ($self->{DataType} == $ccsFloat) {
          if ( CCValidateNumber($self->{DBValue}, @{$self->{DBFormat}} ) ) {
            $varResult = CCParseFloat($self->{DBValue}, @{$self->{DBFormat}} );
          } else {
            $self->{Errors}->addError("The value in field " . $self->{Name} . " is not valid.");
          }
      } elsif ($self->{DataType} == $ccsText or $self->{DataType} == $ccsMemo) {
          $varResult = $self->{DBValue};
      }
  }
  return $varResult;
}

sub GetFormattedValue {
  my $self = shift;
  my $strResult = "";
  if ( $self->{DataType} == $ccsDate ) {
    if( length($self->{Value}) ) {
      $strResult = CCFormatDate($self->{Value}, @{$self->{DBFormat}} );
    }
  } elsif ( $self->{DataType} == $ccsBoolean ) {
    $strResult = CCFormatBoolean($self->{Value}, @{$self->{DBFormat}} );
  } elsif ( $self->{DataType} == $ccsInteger or $self->{DataType} == $ccsFloat ) {
    $strResult = CCFormatNumber($self->{Value}, @{$self->{DBFormat}} );
  } elsif ( $self->{DataType} == $ccsText or $self->{DataType} == $ccsMemo ) {
    $strResult = $self->{Value};
  }
  return $strResult;
}

sub SetDBValue {
  my ($self, $DBValue) = @_;
  $self->{DBValue} = $DBValue;
  $self->{Value} = $self->GetParsedValue;
}

sub SetValue {
  my ($self, $Value) = @_;
  $self->{Value} = $Value;
  $self->{DBValue} = $self->GetFormattedValue;
}

sub GetValue {
  my $self = shift;
  return $self->{Value};
}

sub GetDBValue {
  my $self = shift;
  return $self->{DBValue};
}


#End clsField Class

#clsButton Class @0-C0B697A4

package clsButton;
use Common;

sub new {
  my ($class, $strName) = @_; 
  my $self                        = {};
  
  $self->{Name}                   = $strName;
  $self->{Visible}                = undef;
  $self->{CCSEventResult}         = undef;
  $self->{CCSEvents}              = {};
  $self->{Visible}                = 1;

  bless $self, $class;
  return $self;
}

sub Show {
  my ($self, $RowNumber) = @_;

  $RowNumber = defined($RowNumber) ? $RowNumber : "";
  $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeShow" );
  if ( $self->{Visible} ) {
    my $ControlName = ($RowNumber eq "") ? $self->{Name} : $self->{Name} . "_" . $RowNumber;
    $Tpl->setvar("Button_Name", $ControlName);
    $Tpl->parse("Button " . $self->{Name}, "false");
  } else {
    $Tpl->setblockvar("Button " . $self->{Name}, "");
  }
}

#End clsButton Class

#clsFileUpload Class @0-FD6F7095

package clsFileUpload;
use Common;

  sub new {
    my ($class, $Name, $Caption, $TemporaryFolder, $FileFolder, $AllowedFileMasks, $DisallowedFileMasks, $FileSizeLimit) = @_; 
    my $self                        = {};
  
    $self->{CCSEventResult}         = undef;
    $self->{CCSEvents}              = {};
  
    $self->{Errors}                 = clsErrors->new();
  
    $self->{Name}                = $Name;
    $self->{Visible}             = 1;
    $self->{Caption}             = $Caption;
    if(substr($TemporaryFolder, 0, 1) eq "%") {
      $TemporaryFolder = substr($TemporaryFolder, 1);
      $TemporaryFolder = $ENV{$TemporaryFolder};
    }
    $self->{TemporaryFolder}     = $TemporaryFolder;
    if(substr($FileFolder, 0, 1) eq "%") {
      $FileFolder = substr($FileFolder, 1);
      $FileFolder = $ENV{$FileFolder};
    }
    $self->{FileFolder}          = $FileFolder;
    $self->{AllowedFileMasks}    = $AllowedFileMasks;
    $self->{DisallowedFileMasks} = $DisallowedFileMasks;
    $self->{FileSizeLimit}       = $FileSizeLimit;
  
    my $FileName = "";
    my $FieldName = $self->{Caption};
    if( ! (-d $TemporaryFolder) ) {
      $self->{Errors}->addError("Unable to upload the file specified in " . $FieldName . " - temporary upload folder doesn't exist.");
    } elsif( ! (-w $TemporaryFolder) ) {
      $self->{Errors}->addError("Insufficient filesystem permissions to upload the file specified in " . $FieldName . " into temporary folder.");
    } elsif( ! (-d $FileFolder) ) {
      $self->{Errors}->addError("Unable to upload the file specified in " . $FieldName . " - upload folder doesn't exist.");
    } elsif( ! (-w $FileFolder) ) {
      $self->{Errors}->addError("Insufficient filesystem permissions to upload the file specified in " . $FieldName . ".");
    } 
    
    bless $self, $class;
    return $self;
  }

  sub Validate {
    my $self = shift;
    my $validation = 0;
    if ( $self->{Required} && $self->{Value} eq "" && $self->{Errors}->Count() == 0 ) {
      my $FieldName = $self->{Caption};
      $self->{Errors}->addError("The value in field " . $FieldName . " is required.");
    }
    $self->{CCSEventResult} = CCGetEvent( $self->{CCSEvents}, "OnValidate" );
    return ($self->{Errors}->Count() == 0);
  }

  sub Upload {
    my ($self, $RowNumber) = @_;
    my $FieldName = $self->{Caption};
    my ($ControlName, $FileControl, $DeleteControl, $UploadedFile, $SlashIndex, $index, $ActualFileName);

    $RowNumber = defined($RowNumber) ? $RowNumber : ""; 

    if(length($RowNumber)) {
      $ControlName = $self->{Name} . "_" . $RowNumber;
      $FileControl = $self->{Name} . "_File_" . $RowNumber;
      $DeleteControl = $self->{Name} . "_Delete_" . $RowNumber;
    } else {
      $ControlName = $self->{Name};
      $FileControl = $self->{Name} . "_File";
      $DeleteControl = $self->{Name} . "_Delete";
    }

    my $SessionName = CCGetParam($ControlName);
    @{$self->{State}} = split(/:/, CCGetSession($SessionName));

    $UploadedFile = $cgi->upload($FileControl);

    if (length(CCGetParam($DeleteControl))) { 
      # delete file from the folder
      $ActualFileName = $self->{State}->[0];
      if( -e ($self->{FileFolder} . $ActualFileName) ) {
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeDeleteFile");
        unlink($self->{FileFolder} . $ActualFileName);
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "AfterDeleteFile");
      } elsif ( -e ($self->{TemporaryFolder} . $ActualFileName) ) {
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeDeleteFile");
        unlink($self->{TemporaryFolder} . $ActualFileName);
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "AfterDeleteFile");
      }
      $self->{Value} = ""; $self->{Text} = "";
      $self->{State}->[0] = "";
    } elsif ($UploadedFile ne undef) { 
      $self->{Value} = ""; $self->{Text} = "";
      my $FileName = $UploadedFile;
      $FileName =~ s/\\/\//g;
      $SlashIndex = rindex($FileName, "/");
      my $GoodFileMask = 1;
      my %meta_characters = ( "*" => ".*", "?" => ".",  "\\" => "\\\\", "^" => "\\^", "\$" => "\\\$", "." => "\\.", "[" => "\\[", "|" => "\\|", "(" => "\\(", ")" => "\\)", "+" => "\\+", "{" => "\\{", "-" => "\\-", "]" => "\\]" );
      if ($self->{AllowedFileMasks} ne "") {
        $GoodFileMask = 0;
        my @FileMasks=split(';', $self->{AllowedFileMasks});
        foreach $FileMask (@FileMasks) {
          $FileMask =~ s/(\*|\?|\^|\$|\.|\[|\||\(|\)|\+|\{|-|\])/$meta_characters{$1}/ge;
          if ($FileName =~ m/^$FileMask$/i) {
            $GoodFileMask = 1;
            last;
          }
        }
      }
      if($GoodFileMask && $self->{DisallowedFileMasks} ne "") {
        my @FileMasks = split(';' ,$self->{DisallowedFileMasks});
        foreach $FileMask (@FileMasks) {
          $FileMask =~ s/(\*|\?|\^|\$|\.|\[|\||\(|\)|\+|\{|-|\])/$meta_characters{$1}/ge;
          if ($FileName =~ m/^$FileMask$/i) {
            $GoodFileMask = 0;
            last;
          }
        }
      }

      if( $SlashIndex ne -1 ) {
        $FileName = substr($FileName, $SlashIndex + 1);
      }

      if( (-s $UploadedFile) > $self->{FileSizeLimit} ) {
        $self->{Errors}->addError("The file size in field " . $FieldName . " is too large.");
      } elsif ( !$GoodFileMask ) {
        $self->{Errors}->addError("The file type specified in field " . $FieldName . " is not allowed.");
      } else {
        # move uploaded file to temporary folder
        my $file_exists = 1;
        $index = 0;
        while($file_exists) {
          $ActualFileName = str2time("%Y%m%d%H%M%S", localtime(time())) . $index . "." . $FileName;
          $file_exists = -e $ActualFileName;
          $index++;
        }
        if( open (TMPFILE, ">>" . $self->{TemporaryFolder} . $ActualFileName) ) {
          # Copy file to temporary folder
          my $buffer;
          binmode $UploadedFile;
          binmode TMPFILE;
          while (read($UploadedFile, $buffer, 1024)) {
            print TMPFILE $buffer;
          }
          close(TMPFILE);
          close($UploadedFile);
          $self->{Value} = $ActualFileName;
          $self->{Text} = $ActualFileName;
          if( $self->{State}->[0] ne "" ) {
            if( -e ($self->{TemporaryFolder} . $self->{State}->[0]) ) {
              $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeDeleteFile");
              unlink($self->{TemporaryFolder} . $self->{State}->[0]);
              $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "AfterDeleteFile");
              $self->{State}->[0] = $ActualFileName;
            } else {
              if(! (-d ($self->{TemporaryFolder} . $self->{State}->[1])) && -e ($self->{TemporaryFolder} . $self->{State}->[1])) {
                $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeDeleteFile");
                unlink($self->{TemporaryFolder} . $self->{State}->[1]);
                $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "AfterDeleteFile");
              }
              $self->{State}->[1] = $ActualFileName;
            }
          } else {
            $self->{State}->[0] = $ActualFileName;
          }
        } else {
          $self->{Errors}->addError("Insufficient filesystem permissions to upload the file specified in " . $FieldName . " into temporary folder.");
        }
      }
    } else {
      $self->SetValue(length($self->{State}->[1]) ? $self->{State}->[1] : $self->{State}->[0]);
    }
  }

  sub Move {
    my $self = shift;
    if (length($self->{Value}) && ! -e ($self->{FileFolder} . $self->{Value})) {
      $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeProcessFile");
      my $FieldName = $self->{Caption};
      my $FileName = $self->GetFileName();
      if (! -e ($self->{TemporaryFolder} . $self->{Value})) {
        $self->{Errors}->addError("The file " . $FileName . " specified in " . $FieldName . " was not found.");
      } elsif (! rename($self->{TemporaryFolder} . $self->{Value}, $self->{FileFolder} . $self->{Value})) {
        $self->{Errors}->addError("Insufficient filesystem permissions to upload the file specified in " . $FieldName . ".");
      } elsif (length($self->{State}->[1])) {
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeDeleteFile");
        unlink($self->{FileFolder} . $self->{State}->[0]);
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "AfterDeleteFile");
      }
      $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "AfterProcessFile");
    }
  }

  sub Delete {
    my $self = shift;
    for(my $i = 0; $i <= 1; $i++) {
      if( ! -d ($self->{FileFolder} . $self->{State}->[0]) && -e ($self->{FileFolder} . $self->{State}->[0]) ) {
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeDeleteFile");
        unlink($self->{FileFolder} . $self->{State}->[0]);
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "AfterDeleteFile");
      } elsif ( ! -d ($self->{TemporaryFolder} . $self->{State}->[0]) && -e ($self->{TemporaryFolder} . $self->{State}->[0]) ) {
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeDeleteFile");
        unlink($self->{TemporaryFolder} . $self->{State}->[0]);
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "AfterDeleteFile");
      }
    }
  }



  sub Show {
    my ($self, $RowNumber) = @_;

    $RowNumber = defined($RowNumber) ? $RowNumber : ""; 

    if($self->{Visible})
    {
      my ($ControlName, $FileControl, $DeleteControl);
      $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeShow");

      if(!$self->{Visible}) {
        $Tpl->setblockvar("FileUpload " . $self->{Name}, "");
        return;
      }

      if(length($RowNumber)) {
        $ControlName = $self->{Name} . "_" . $RowNumber;
        $FileControl = $self->{Name} . "_File_" . $RowNumber;
        $DeleteControl = $self->{Name} . "_Delete_" . $RowNumber;
      } else {
        $ControlName = $self->{Name};
        $FileControl = $self->{Name} . "_File";
        $DeleteControl = $self->{Name} . "_Delete";
      }

      my $SessionName = CCGetParam($ControlName);
      if(! length($SessionName) ) {
        my $random_value = int(rand(999999999999));
        $SessionName = "FileUpload" . $random_value . str2time("%d%H%M%S", localtime(time()));
        $self->{State}->[0] = $self->{Value};
        $self->{State}->[1] = "";
      } 

      CCSetSession($SessionName, join(":", @{$self->{State}}));

      $Tpl->setvar("State", $SessionName);
      $Tpl->setvar("ControlName", $ControlName);
      $Tpl->setvar("FileControl", $FileControl);
      $Tpl->setvar("DeleteControl", $DeleteControl);
      if (length($self->{Value}) ) {
        $Tpl->setvar("ActualFileName", $self->{Value});
        $Tpl->setvar("FileName", $self->GetFileName());
        $Tpl->setvar("FileSize", $self->GetFileSize());
        $Tpl->parse("FileUpload " . $self->{Name} . "/Info", "false");
        if($self->{Required}) {
          $Tpl->parse("FileUpload " . $self->{Name} . "/Upload", "false");
          $Tpl->setblockvar("FileUpload " . $self->{Name} . "/DeleteControl", "");
        } else {
          $Tpl->setblockvar("FileUpload " . $self->{Name} . "/Upload", "");
          $Tpl->parse("FileUpload " . $self->{Name} . "/DeleteControl", "false");
        }
      } else {
        $Tpl->parse("FileUpload " . $self->{Name} . "/Upload", "false");
        $Tpl->setblockvar("FileUpload " . $self->{Name} . "/Info", "");
        $Tpl->setblockvar("FileUpload " . $self->{Name} . "/DeleteControl", "");
      }

      $Tpl->parse("FileUpload " . $self->{Name}, "false");
    }
    else
    {
      $Tpl->setblockvar("FileUpload " . $self->{Name}, "");
    }

  }

  sub SetValue {
    my ($self, $Value) = @_;
    $self->{Text} = $Value;
    $self->{Value} = $Value;
    $self->{State}->[0] = $Value;
    if(length($Value) 
      && ! -e ($self->{TemporaryFolder} . $Value) 
      && ! -e ($self->{FileFolder} . $Value)) {
        my $FileName = $self->GetFileName();
        my $FieldName = $self->{Caption};
        $self->{Errors}->addError("The file " . $FileName . " specified in " . $FieldName . " was not found.");
    }
  }

  sub SetText {
    my ($self, $Text) = @_;
    $self->SetValue($Text);
  }

  sub GetValue {
    my $self = shift;
    return $self->{Value};
  }

  sub GetText {
    my $self = shift;
    return $self->{Text};
  }

  sub GetFileName {
    my $self = shift;
    return substr($self->{Value}, index($self->{Value}, ".") + 1);
  }

  sub GetFileSize {
    my $self = shift;
    my $filesize = 0;
    if( -e ($self->{FileFolder} . $self->{Value}) ) {
      $filesize = -s ($self->{FileFolder} . $self->{Value});
    } elsif ( -e ($self->{TemporaryFolder} . $self->{Value}) ) {
      $filesize = -s ($self->{TemporaryFolder} . $self->{Value});
    }
    return $filesize;
  }




#End clsFileUpload Class

#clsDatePicker Class @0-38AECF42

package clsDatePicker;
use Common;

sub new {
  my ($class, $Name, $FormName, $ControlName) = @_; 
  my $self                        = {};
  
  $self->{Name}                   = $Name;
  $self->{FormName}               = $FormName;
  $self->{ControlName}            = $ControlName;
  $self->{CCSEventResult}         = undef;
  $self->{CCSEvents}              = {};
  $self->{Visible}                = 1;
  $self->{Errors}                 = clsErrors->new();

  bless $self, $class;
  return $self;
}

sub Show {
  my ($self, $RowNumber) = @_;

  $RowNumber = defined($RowNumber) ? $RowNumber : ""; 

  if ( $self->{Visible} ) {
    my $ControlName = ($RowNumber eq "") ? $self->{ControlName} : $self->{ControlName} . "_" . $RowNumber;
    $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeShow" );
    $Tpl->setvar("Name",        $self->{FormName} . "_" . $self->{Name});
    $Tpl->setvar("FormName",    $self->{FormName});
    $Tpl->setvar("DateControl", $ControlName);

    $Tpl->parse("DatePicker " . $self->{Name}, "false");
  } else {
    $Tpl->setblockvar("DatePicker " . $self->{Name}, "");
  }
}

#End clsDatePicker Class

#clsErrors Class @0-F5DCB5D4

package clsErrors;

sub new {
  my $self = {};
  $self->{ErrorDelimiter} = "<br>";
  $self->{ErrorsCount}    = 0;
  $self->{Errors}         = [];
  return bless $self;
}

sub addError {
 my ($self, $Description) = @_;
 if ($Description) {
   $self->{Errors}->[$self->{ErrorsCount}] = $Description; 
   $self->{ErrorsCount}++;
 }
}

sub AddErrors { 
  my ($self, @Errors) = @_;
  my @Errs;
  if (ref $Errors[0] =~ /ARRAY/) {
    @Errs = (@{$Errors});
  } elsif (ref($Errors[0]) =~ /clsErrors/){
    @Errs = @{$Errors[0]->{Errors}};
  } else {
    @Errs = ($Errors);
  }
  foreach $Error (@Errs) {
    $self->addError($Error);
  }
}

sub Clear {
  my $self = shift;
  $self->{ErrorsCount} = 0;
  @{$self->{Errors}} = ();
}

sub Count {
  my $self = shift;
  return $self->{ErrorsCount};
}

sub ToString {
  my $self = shift;
  if ( ( $#{$self->{Errors}} + 1 ) > 0) { 
    return join( $self->{ErrorDelimiter}, @{$self->{Errors}} ) . $self->{ErrorDelimiter};
  } else {
    return "";
  }
}
1;

#End clsErrors Class

