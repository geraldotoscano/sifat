#Export Common @0-908AB010
package Common;
use Template;
use POSIX qw(mktime ceil);
use Time::Local;
use CGI;

BEGIN {
  use Exporter();
  use vars qw( @ISA @EXPORT @EXPORT_OK );
  @ISA = qw( Exporter );
  @EXPORT = qw( $TemplatePath
                $ServerURL    
                $SecureURL
                $RelativePath
                $PathToCurrentPage
                $CurrentNameSpace
                $ccsInteger
                $ccsFloat
                $ccsText
                $ccsDate
                $ccsBoolean
                $ccsMemo
                $ccsPost
                $ccsGet 
                $ccsLabel
                $ccsLink
                $ccsTextBox
                $ccsTextArea
                $ccsListBox
                $ccsRadioButton
                $ccsButton
                $ccsCheckBox
                $ccsImage
                $ccsImageLink
                $ccsHidden
                $ccsCheckBoxList
                @ControlTypes
                $opEqual
                $opNotEqual
                $opLessThan
                $opLessThanOrEqual
                $opGreaterThan
                $opGreaterThanOrEqual
                $opBeginsWith
                $opNotBeginsWith
                $opEndsWith
                $opNotEndsWith
                $opContains
                $opNotContains
                $opIsNull
                $opNotNull
                $dsTable
                $dsSQL
                $dsProcedure
                $dsListOfValues
                $dsEmpty
                $ccsChecked
                $ccsUnchecked
                $Redirect
                $FileName
                $TemplateFileName
                $PathToRoot
                $WorkDir
                $tpSimple
                $tpCentered
                $tpMoving
                $Login 
                $CCSEvents
                $Tpl
                $cgi
                $isHTTPHeader
                $Charset
                @CookiesSet
                %GetParamsHash
                %PostParamsHash
                %SessionParamsHash
                &CCInit
                &CCGetParam
                &CCAddParam
                &CCRemoveParam
                &CCGetQueryString
                &CCMergeQueryStrings
                &CCGetOrder
                &CCDlookUp
                &CCDLookUp
                &CCGetUserID
                &CCGetEvent
                &CCGetDBValue
                &CCGetFromGet
                &CCGetFromPost
                &CCBuildSQL
                &CCGetRequestParam
                &CCValidateNumber
                &CCParseInteger
                &CCGetListValues
                &CCParseValue
                &CCFormatValue
                &CCToHTML
                &CCFormatNumber
                &CCFormatDate
                &CCGetDateArray
                &CCFormatBoolean
                &CCSecurityRedirect
                &CCLoginUser
                &CCValidateDate
                &CCValidateDateMask
                &CCValidateBoolean
                &CCLogoutUser
                &CCParseDate
                &CCParseBoolean
                &CCGetBooleanFormat
                &CCParseFloat
                &CCSetCookie
                &CCGetCookie
                &replace
                &CCSecurityAccessCheck
                &CCSetSession
                &CCGetSession
                &StartSession
                &SaveSession
                &DestroySession
                &CCCheckSSL
                &CCGetGroupID
                &CCUserInGroups
                &CCCheckValue
                &CCGetParamStartsWith
                &CCCompareValues
                &str2doubl
                &is_numeric
                &is_int
                &str2time
                &print_http_header
    );
}

#End Export Common

#Define Global Variables @0-EC6A8900

use vars qw($isHTTPHeader $cgi @CookiesSet %SessionParamsHash %PostParamsHash %GetParamsHash);
use vars qw($session_id $session_timeout $temp_session_dir $clean_sessions_rate $path_separator);
use vars qw($ccsGet $ccsPost);
use vars qw($TemplatePath  $ServerURL $SecureURL);
use vars qw($FileName $Redirect $Tpl $CurrentNameSpace);
use vars qw(@ShortWeekdays @Weekdays @ShortMonths @Months);
use vars qw($ccsInteger $ccsFloat $ccsText $ccsDate $ccsBoolean $ccsMemo);
use vars qw($ccsTimestamp $ccsYear $ccsMonth $ccsDay $ccsHour $ccsMinute $ccsSecond $ccsMilliSecond $ccsAmPm $ccsShortMonth $ccsFullMonth $ccsWeek $ccsGMT $ccsAppropriateYear);
use vars qw($ccsLabel $ccsLink $ccsTextBox $ccsTextArea $ccsListBox $ccsRadioButton $ccsButton $ccsCheckBox $ccsImage $ccsImageLink $ccsHidden $ccsCheckBoxList);
use vars qw(@ControlTypes);
use vars qw($opEqual $opNotEqual $opLessThan $opLessThanOrEqual $opGreaterThan $opGreaterThanOrEqual $opBeginsWith $opNotBeginsWith $opEndsWith $opNotEndsWith $opContains $opNotContains $opIsNull $opNotNull);
use vars qw($dsTable $dsSQL $dsProcedure $dsListOfValues $dsEmpty);
use vars qw($ccsChecked $ccsUnchecked);
use vars qw($tpSimple $tpCentered $tpMoving);
#End Define Global Variables

#CCInit @0-1072A038
sub CCInit {

    undef $CCSEvents;
    $isHTTPHeader = 0;
    $Charset = 'Cp1252';
    $cgi = new CGI;
    @CookiesSet = ();
    %SessionParamsHash = ();
    %PostParamsHash = ();
    %GetParamsHash = ();

    $session_id          = undef;
    $session_timeout     = 30;
    $temp_session_dir    = $PathToRoot . "session_files";
    $clean_sessions_rate = 100;
    $path_separator      = "\\";

    $ccsGet  = 1;
    $ccsPost = 2;
    GetAllFormParameters();

    $TemplatePath = $PathToRoot;
    $ServerURL = "http://localhost/faturamento5/";
    $SecureURL    = "";

    # Global Variables
    $FileName = "";
    $Redirect = "";

    # Constant List

    @ShortWeekdays = ("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
    @Weekdays      = ("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
    @ShortMonths   = ("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
    @Months        = ("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

    $ccsInteger = 1;
    $ccsFloat   = 2;
    $ccsText    = 3;
    $ccsDate    = 4;
    $ccsBoolean = 5;
    $ccsMemo    = 6;

    $ccsTimestamp       = 0;
    $ccsYear            = 1;
    $ccsMonth           = 2;
    $ccsDay             = 3;
    $ccsHour            = 4;
    $ccsMinute          = 5;
    $ccsSecond          = 6;
    $ccsMilliSecond     = 7;
    $ccsAmPm            = 8;
    $ccsShortMonth      = 9;
    $ccsFullMonth       = 10;
    $ccsWeek            = 11;
    $ccsGMT             = 12;
    $ccsAppropriateYear = 13;

    # ------- Controls ------
    $ccsLabel        = 1;
    $ccsLink         = 2;
    $ccsTextBox      = 3;
    $ccsTextArea     = 4;
    $ccsListBox      = 5;
    $ccsRadioButton  = 6;
    $ccsButton       = 7;
    $ccsCheckBox     = 8;
    $ccsImage        = 9;
    $ccsImageLink    = 10;
    $ccsHidden       = 11;
    $ccsCheckBoxList = 12;

    @ControlTypes = (
        "", "Label","Link","TextBox","TextArea","ListBox","RadioButton",
        "Button","CheckBox","Image","ImageLink","Hidden","CheckBoxList"
    );

    # ------- Operators --------------
    $opEqual              = 1;
    $opNotEqual           = 2;
    $opLessThan           = 3;
    $opLessThanOrEqual    = 4;
    $opGreaterThan        = 5;
    $opGreaterThanOrEqual = 6;
    $opBeginsWith         = 7;
    $opNotBeginsWith      = 8;
    $opEndsWith           = 9;
    $opNotEndsWith        = 10;
    $opContains           = 11;
    $opNotContains        = 12;
    $opIsNull             = 13;
    $opNotNull            = 14;

    # ------- Datasource types -------
    $dsTable        = 1;
    $dsSQL          = 2;
    $dsProcedure    = 3;
    $dsListOfValues = 4;
    $dsEmpty        = 5;

    # ------- CheckBox states --------
    $ccsChecked   = 1;
    $ccsUnchecked = 0;

    # --- Navigator Types ---
    $tpSimple   = 1;
  
    $tpCentered = 2;
  
    $tpMoving   = 3;


}
#End CCInit

#print_http_header @0-920E62FA
sub print_http_header {
    my (@HTTPHeaders) = @_;
    if (!$isHTTPHeader) {
        $isHTTPHeader = 1;
        if($Charset) {
            print $cgi->header(-cookie => [@CookiesSet], -charset => $Charset, @HTTPHeaders);
        } else {
            print $cgi->header(-cookie => [@CookiesSet], @HTTPHeaders);
        }
    }
}
#End print_http_header

#CCToHTML @0-CFA53CFE
sub CCToHTML {
  my $input_string = shift;
  if(defined($input_string)) {
    my %convert_hash = ( "<" => "&lt;", "&" => "&amp;", ">" => "&gt;", '"' => "&quot;" );
    $input_string =~ s/([<>&"])/$convert_hash{$1}/g;
  } else {
    $input_string = "";
  }
  return $input_string
}
#End CCToHTML

#CCToURL @0-531B5FEB
sub CCToURL {
  my $input_string = shift;
  $input_string = defined($input_string) ? $input_string : ""; 
  $input_string =~ s/([^\w\.])/sprintf("%%%X", ord($1))/eg;
  $input_string =~ s/%20/+/g;
  return $input_string;
}

#End CCToURL

#CCGetEvent @0-E78973EB
sub CCGetEvent {
  my ($events_hash_ref, $event_name) = @_;
  my $function_name = ( exists $events_hash_ref->{$event_name} ) ? $events_hash_ref->{$event_name} : "";
  my $result = 1;
  if ( length($function_name) > 1 ) {
    if ($function_name =~ /::/) {
      $result = &{$function_name};
    } else {
      #$result = &{"main::" . $function_name};
      $result = &{$function_name};
    }
    $result = length($result) == 0 ? 1 : $result;
  }  
  return $result;
}
#End CCGetEvent

#CCGetValueHTML @0-0D699518
sub CCGetValueHTML {
  my ( $db, $fieldname ) = @_;
  return CCToHTML( $db->f($fieldname) ); 
}
#End CCGetValueHTML

#CCGetValue @0-1CBA2042
sub CCGetValue {
  my ( $db, $fieldname ) = @_;
  return $db->f( $fieldname );
}
#End CCGetValue

#GetStrTime @0-A09DF6CC
sub GetStrTime {
  my $time = shift;
  my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = gmtime($time);
  my @months = ("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
  my @wdays = ("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
  $hour  = "0" . $hour if ($hour < 10);
  $min   = "0" . $min  if ($min < 10);
  $sec   = "0" . $sec  if ($sec < 10);
  $mday  = "0" . $mday if ($mday < 10);
  $year += 1900;
  return $wdays[$wday] . ", $mday-" . $months[$mon] . "-$year $hour:$min:$sec GMT";
}
#End GetStrTime

#StartSession @0-746753F7
sub StartSession {
  $session_id = CCGetCookie("SessionID");
  if (!defined($session_id) || ($session_id eq "") || !(-e "$temp_session_dir/$session_id.tmp")) {
    if (!(-d $temp_session_dir)) {
      mkdir($temp_session_dir, 0744);
    }
    $session_id = GenerateSessionID();
    open(SESSION_FILE, ">$temp_session_dir/$session_id.tmp");
    close(SESSION_FILE);
  } else {
    my $expiry_date = time() - (60 * $session_timeout);
    my $mtime = (stat("$temp_session_dir/$session_id.tmp"))[9];
    if($expiry_date > $mtime) {
      open(SESSION_FILE, ">$temp_session_dir/$session_id.tmp");
      close(SESSION_FILE);
    }
  }

  CCSetCookie("SessionID", $session_id, "Session");
  # read session file into hash
  my $slashcode = "\\\\" . ord("\\");
  my $coloncode = "\\\\" . ord(":");
  open(SESSION_FILE, "$temp_session_dir/$session_id.tmp");
  while(<SESSION_FILE>) {
    chomp;
    my ($type, $session_string) = /(\w):(.+)/g;
    my ($name, @values) = split(/:/, $session_string);
    $name   =~ s/$coloncode/:/;
    $name   =~ s/$slashcode/\\/;
    for ( my $i = 0; $i < @values; $i++ ) {
      $values[$i] =~ s/$coloncode/:/;
      $values[$i] =~ s/$slashcode/\\/;
    }
    if($type eq "a") {
      @{$SessionParamsHash{$name}} = @values;
    } else {
      $SessionParamsHash{$name} = $values[0];
    }
  }
  close(SESSION_FILE);
  # remove expired session files
  if (rand() < (1/$clean_sessions_rate)) {
    DelExpiredSessions();
  }
}
#End StartSession

#DestroySession @0-0A28A3B7
sub DestroySession {
  %SessionParamsHash = ();
}
#End DestroySession

#GenerateSessionID @0-909021C6
sub GenerateSessionID {
  return time() . "_" . substr(rand(), 2);
}
#End GenerateSessionID

#CCGetSession @0-3F5F0DEB
sub CCGetSession {
  my ($parameter_name) = shift;
  return exists $SessionParamsHash{$parameter_name} ? $SessionParamsHash{$parameter_name} : "";
}
#End CCGetSession

#CCSetSession @0-BAF3E8D7
sub CCSetSession {
  my ($parameter_name, $parameter_value) = @_;
  $SessionParamsHash{$parameter_name} = $parameter_value;
}
#End CCSetSession

#SaveSession @0-8143CF6B
sub SaveSession {

  my $slashcode = "\\" . ord("\\");
  my $coloncode = "\\" . ord(":");

  open(SESSION_FILE, ">$temp_session_dir/$session_id.tmp");
  while(my ($key, $value) = each(%SessionParamsHash)) {
    $key =~ s/\\/$slashcode/;
    $key =~ s/:/$coloncode/;
    if(ref($value) eq "ARRAY") {
      print SESSION_FILE "a:" . $key;
      foreach my $array_value (@{$value}) {      
        $array_value =~ s/\\/$slashcode/;
        $array_value =~ s/:/$coloncode/;
        print SESSION_FILE ":" . $array_value;
      }
      print SESSION_FILE "\n";
    } elsif (defined($value)) {
      $value =~ s/\\/$slashcode/;
      $value =~ s/:/$coloncode/;
      print SESSION_FILE "v:" . $key . ":" . $value ."\n";
    }
  }
  close(SESSION_FILE);

}
#End SaveSession

#CCGetCookie @0-18E1DF14
sub CCGetCookie {
  my $cookie_name = shift;
  return $cgi->cookie($cookie_name);
}
#End CCGetCookie

#CCSetCookie @0-A2E71A8B
sub CCSetCookie {
  my ( $name, $value, $exp_time ) = @_;
  $exp_time ||= 3600 * 24 * 366; 
  my $cookie;
  if(is_int($exp_time)) {
    $cookie = $cgi->cookie(-name=>$name, -value=>$value, -expires=>GetStrTime(time() + $exp_time));
  } else {
    $cookie = $cgi->cookie(-name=>$name, -value=>$value);
  }
  push(@CookiesSet, $cookie);
}
#End CCSetCookie

#DelExpiredSessions @0-2AB606C6
sub DelExpiredSessions {
  my $expiry_date = time() - (60 * $session_timeout);
  opendir(SESSION_TEMP_DIR, $temp_session_dir);
  my (@sessions) = readdir(SESSION_TEMP_DIR); 
  my ($file_name, $create_date);
  for $file_name (@sessions) {
    my $mtime = (stat($temp_session_dir . $path_separator . $file_name))[9];
    if ($expiry_date > $mtime && $file_name !~ /^\./) {
      unlink ($temp_session_dir . $path_separator . $file_name);
    }
  }
  closedir(SESSION_TEMP_DIR);
}
#End DelExpiredSessions

#GetAllFormParameters @0-930D0A51
sub GetAllFormParameters {
  if ( $cgi->request_method() eq 'POST' ) {
    foreach my $ParameterName ($cgi->param) {
      my @ParameterValue = $cgi->param($ParameterName);
      if(@ParameterValue > 1) {
        @{$PostParamsHash{$ParameterName}} = @ParameterValue;
      } else {
        $PostParamsHash{$ParameterName} = $ParameterValue[0];
      }
    }
  }
  foreach my $ParameterName ($cgi->url_param) {
    my @ParameterValue = $cgi->url_param($ParameterName);
    my $URLParameter = $cgi->url_param($ParameterName);
    if($ParameterName ne "keywords" || (defined($URLParameter) && $URLParameter ne "")) {
      if(@ParameterValue > 1) {
        @{$GetParamsHash{$ParameterName}} = @ParameterValue;
      } else {
        $GetParamsHash{$ParameterName} = $ParameterValue[0];
      }
    }
  }

}
#End GetAllFormParameters

#CCGetParam @0-C2DEA511
sub CCGetParam {
  my ($ParamName, $default_value) = @_;
  if ( exists $PostParamsHash{$ParamName} ) {
    return $PostParamsHash{$ParamName} 
  } elsif ( exists $GetParamsHash{$ParamName} ) {
    return $GetParamsHash{$ParamName} 
  } else {   
    return $default_value;
  }
}
#End CCGetParam

#CCGetFromPost @0-0C105630
sub CCGetFromPost {
  my ( $parameter_name, $default_value ) = @_;
  return exists $PostParamsHash{$parameter_name} ? $PostParamsHash{$parameter_name} : $default_value;
}
#End CCGetFromPost

#CCGetFromGet @0-CB7E4398
sub CCGetFromGet {
  my ( $parameter_name, $default_value ) = @_;
  return exists $GetParamsHash{$parameter_name} ? $GetParamsHash{$parameter_name} : $default_value;
}
#End CCGetFromGet

#CCGetRequestParam @0-099F7282
sub CCGetRequestParam {
  my ($ParameterName, $Method) = @_;
  if ( $Method == $ccsGet && exists($GetParamsHash{$ParameterName}) ) {
    return $GetParamsHash{$ParameterName}
  } elsif ( $Method == $ccsPost && exists($PostParamsHash{$ParameterName}) ) {
    return $PostParamsHash{$ParameterName}
  }
}
#End CCGetRequestParam

#CCGetParamStartsWith @0-0AEA019F
sub CCGetParamStartsWith {
  my $prefix = shift;
  my $parameter_name = "";
  while ( my($key,$value) = each(%PostParamsHash) ) {
    if ($key =~ m/^($prefix)_\d+$/) {
      $parameter_name = $key;
      last;
    }
  }
  if ($parameter_name eq "") {
    while ( my($key,$value) = each(%GetParamsHash) ) {
      if ($key =~ m/^($prefix)_\d+$/) {
        $parameter_name = $key;
        last;
      }
    }
  }
  return $parameter_name;
}
#End CCGetParamStartsWith

#CCDlookUp @0-16E1BFF0
sub CCDlookUp {
  my ($field_name, $table_name, $where_condition, $db) = @_;
  my $sql = "SELECT " . $field_name . ($table_name ? " FROM " . $table_name : "") . ($where_condition ? " WHERE " . $where_condition : "");
  return CCGetDBValue($sql, $db);
}
#End CCDlookUp

#CCDLookUp @0-F7883B54
sub CCDLookUp {
  my ($field_name, $table_name, $where_condition, $db) = @_;
  my $sql = "SELECT " . $field_name . ($table_name ? " FROM " . $table_name : "") . ($where_condition ? " WHERE " . $where_condition : "");
  return CCGetDBValue($sql, $db);
}
#End CCDLookUp

#CCGetDBValue @0-EBCAEF19
sub CCGetDBValue {
  my ($sql, $db) = @_;
  return $db->{dbh}->selectrow_array($sql);
}
#End CCGetDBValue

#CCGetListValues @0-7A9199DA
sub CCGetListValues {
    my ($db, $sql, $where, $order_by, $bound_column, $text_column, $datatype, $errorclass, $fieldname, @dbformat) = @_;

    $bound_column = 0 if ( !length($bound_column) );
    $text_column  = 1 if ( !length($text_column) );
    $sql .= " WHERE " . $where if ( length($where) );
    $sql .= " ORDER BY " . $order_by if ( length($order_by) );

    $db->query($sql);
    my @values;
    for (my $i = 0; $db->next_record(); $i++) { 
            my $bound_column_value = $db->f($bound_column);
            $bound_column_value = CCParseValue($bound_column_value, $datatype, $errorclass, $fieldname, @dbformat);
            $values[$i] = [ $bound_column_value, $db->f($text_column) ];
    }
    return \@values;
}
#End CCGetListValues

#CCParseValue @0-1D0EFE4B
sub CCParseValue {
  my ($ParsingValue, $DataType, $ErrorClass, $FieldName, @Format) = @_;
  my $varResult = "";

  if ( CCCheckValue($ParsingValue, $DataType) ) {
    if($DataType == $ccsBoolean) {
      if (lc($ParsingValue) eq "true") {
        $varResult = 1;
      } elsif (lc($ParsingValue) eq "false") {
        $varResult = 0;
      } else {
        $varResult = $ParsingValue;
      }
    } else {
      $varResult = $ParsingValue;
    }
  } elsif (length($ParsingValue)) {
    if ( $DataType == $ccsDate ) {
      my $DateValidation = 1;
      if ( CCValidateDateMask($ParsingValue, @Format) ) {
        $varResult = CCParseDate($ParsingValue, @Format); 
        $error = $@;
        if($error || !CCValidateDate($varResult)) {
          $DateValidation = 0;
          $varResult = "";
        }
      } else { 
        $DateValidation = 0;
      }
      if (!$DateValidation && $ErrorClass->Count() == 0) { 
        if ( (@Format) ) { 
          my $FormatString = join("", @Format);
          $ErrorClass->addError("The value in field " . $FieldName . " is not valid. Use the following format: " . $FormatString . ".");
        } else {
          $ErrorClass->addError("The value in field " . $FieldName . " is not valid.");
        }
      }
    } elsif ( $DataType == $ccsBoolean ) {
      if ( CCValidateBoolean( $ParsingValue, @Format ) ) {
        $varResult = CCParseBoolean( $ParsingValue, @Format);
      } elsif ($ErrorClass->Count() == 0) { 
        if ( (@Format) ) { 
          my $FormatString = CCGetBooleanFormat(@Format);
          $ErrorClass->addError("The value in field " . $FieldName . " is not valid. Use the following format: " . $FormatString . ".");
        } else {
          $ErrorClass->addError("The value in field " . $FieldName . " is not valid.");
        }
      }
    } elsif ( $DataType == $ccsInteger ) {
       if ( CCValidateNumber($ParsingValue, @Format) ) {
         $varResult = CCParseInteger($ParsingValue, @Format)
       } elsif ($ErrorClass->Count() == 0) { 
         $ErrorClass->addError("The value in field " . $FieldName . " is not valid.")
       }
    } elsif ( $DataType == $ccsFloat ) {
       if ( CCValidateNumber( $ParsingValue, @Format ) ) {
         $varResult = CCParseFloat($ParsingValue, @Format)
       } elsif ($ErrorClass->Count() == 0) { 
         $ErrorClass->addError("The value in field " . $FieldName . " is not valid.")
       }
    } elsif ( $DataType == $ccsText or $DataType == $ccsMemo ) {
      $varResult = $ParsingValue;
    }
  }
  return $varResult;
}

#End CCParseValue

#CCFormatValue @0-D3CE5318
sub CCFormatValue {
  my ($Value, $DataType, @Format) = @_;

  my $strResult = "";
  if ( $DataType == $ccsDate ) {
    if($Value ne "") {
      $strResult = CCFormatDate($Value, @Format);
    }
  } elsif ( $DataType == $ccsBoolean ) {
    $strResult = CCFormatBoolean($Value, @Format);
  } elsif ( $DataType == $ccsInteger or $DataType == $ccsFloat ) {
    $strResult = CCFormatNumber($Value, @Format);
  } elsif ( $DataType == $ccsText or $DataType == $ccsMemo ) {
    $strResult = $Value;
  }
  return $strResult;
}


#End CCFormatValue

#CCBuildSQL @0-43C7D444
sub CCBuildSQL {
    my ($sql, $where, $order_by) = @_;
    if ( length($where) ) { $sql .= " WHERE " . $where };
    $sql .= " ORDER BY " . $order_by if ( length($order_by) );
    return $sql;
}

#End CCBuildSQL

#CCGetQueryString @0-F3454F8C
sub CCGetQueryString {
  my ($CollectionName, @RemoveParameters) = @_;
  my $querystring = "";
  my $postdata    = "";
  if ( $CollectionName eq "Form") {
    $querystring = CCCollectionToString(\%PostParamsHash, @RemoveParameters);
  } elsif ($CollectionName eq "QueryString") {
    $querystring = CCCollectionToString(\%GetParamsHash, @RemoveParameters);
  } elsif ($CollectionName eq "All") {
    $querystring = CCCollectionToString(\%GetParamsHash, @RemoveParameters);
    $postdata = CCCollectionToString(\%PostParamsHash, @RemoveParameters);
    if ( length($postdata) > 0 && length($querystring) > 0 ) {
      $querystring .= "&" . $postdata;
    } else {
      $querystring .= $postdata;
    }
  } else {
    die("1050: Common Functions. CCGetQueryString Function. The CollectionName contains an illegal value.");
  }
  return $querystring;
}
#End CCGetQueryString

#CCCollectionToString @0-D7FDD2FA
sub CCCollectionToString {
  my ($ParametersCollection, @RemoveParameters) = @_;
  my $Result = ""; 
  while ( my ( $ItemName, $ItemValues ) = each %{$ParametersCollection} ) {
    next if ($ItemName . $ItemValues eq "");
    my $Remove = 0;
    for (my $i = 0; $i < ($#RemoveParameters + 1); $i++ ) {
      if ( $RemoveParameters[$i] eq $ItemName) {
         $Remove = 1;
         last;
      }
    }
    if ( !$Remove ) {
      if ( ref($ParametersCollection->{$ItemName}) eq 'ARRAY' ) {
        for ( my $i=0; $i<=$#{$ParametersCollection->{$ItemName}}; $i++ ) {
          $Result .= "&" . $ItemName . "=" . CCToURL($ParametersCollection->{$ItemName}->[$i]);
        }
      } else {
        $Result .= "&" . $ItemName . "=" . CCToURL($ItemValues);
      }
    }
  }
  return  length($Result) > 0 ? substr($Result, 1) : $Result;
}
#End CCCollectionToString

#CCMergeQueryStrings @0-069E05FF
sub CCMergeQueryStrings {
  my ($LeftQueryString, $RightQueryString) = @_;
  my $QueryString = $LeftQueryString; 
  if($QueryString eq "") {
    $QueryString = $RightQueryString;
  } elsif ($RightQueryString ne "") {
    $QueryString .= "&" . $RightQueryString;
  }

  return $QueryString;
}
#End CCMergeQueryStrings

#replace @0-DBEAC6C0
sub replace {
  my ($source, $find, $replace) = @_;
  $source =~ s/\Q$find\E/$replace/g;
  return $source;
}
#End replace

#CCAddParam @0-75FF8F82
sub CCAddParam {
  my ($querystring, $ParameterName, $ParameterValue) = @_;
  $querystring = $querystring ? "&" . $querystring : "";
  $querystring =~ s/&$ParameterName=[^&]*//g;
  if(ref($ParameterValue) eq "ARRAY") {
    my @ParameterValue = @{$ParameterValue};
    for (my $i = 0; $i < @ParameterValue; $i++) {
      $querystring .= "&" . $ParameterName . "=" . CCToURL($ParameterValue[$i]);
    }
  } else {
    $querystring .= "&" . $ParameterName . "=" . CCToURL($ParameterValue);
  }
  return substr($querystring, 1);
}
#End CCAddParam

#CCRemoveParam @0-F1FBCB6C
sub CCRemoveParam {
  my ($querystring, $ParameterName) = @_;
  $querystring = "&" . $querystring;
  $querystring =~ s/&$ParameterName=[^&]*//g;
  return substr($querystring, 0, 1) eq "&" ? substr($querystring, 1) : $querystring;
}
#End CCRemoveParam

#CCGetOrder @0-4B0E39F1
sub CCGetOrder {
  my ($DefaultSorting, $SorterName, $SorterDirection, %MapArray) = @_;
  $DefaultSorting ||= "";
  $SorterName ||= "";
  my $OrderValue = "";
  if ( exists $MapArray{$SorterName} && $MapArray{$SorterName} ne "" ) {
    if ( uc($SorterDirection) eq "DESC" ) {
      $OrderValue = ( $MapArray{$SorterName}[1] ne "" ) ? $MapArray{$SorterName}[1] : $MapArray{$SorterName}[0] . " DESC";
    } else {
      $OrderValue = $MapArray{$SorterName}[0];
    }
  } else {
    $OrderValue = $DefaultSorting;
  }
  return $OrderValue;
}
#End CCGetOrder

#CCFormatDate @0-95EF707E
sub CCFormatDate {
  my ($DateToFormat, @FormatMask) = @_;

  my @DateArray;
  if ( ref($DateToFormat) ne "ARRAY" && length($DateToFormat)) {
    @DateArray = CCGetDateArray($DateToFormat);
  } elsif(ref($DateToFormat) eq "ARRAY") {
    @DateArray = @{$DateToFormat};
  }

  my $FormattedDate;

  if ( $#FormatMask > -1 && @DateArray > 0 ) {

    # d     (%e) Day of the month in one or two numeric digits, as needed (1 to 31). 
    # dd    (%d) Day of the month in two numeric digits (01 to 31). 
    # ddd   (%a) First three letters of the weekday (Sun to Sat). 
    # dddd  (%A) Full name of the weekday (Sunday to Saturday). 
    # w     (%w) Day of the week (1 to 7).
    # m     (%o) Month of the year in one or two numeric digits, as needed (1 to 12).
    # mm    (%m) Month of the year in two numeric digits (01 to 12). 
    # mmm   (%b) First three letters of the month (Jan to Dec). 
    # mmmm  (%B) Full name of the month (January to December). 
    # yy    (%y) Last two digits of the year (01 to 99). 
    # yyyy  (%Y) Full year (0100 to 9999). 
    # h     (%l) 12-hour format without leading zeros (1 to 12).  
    # hh    (%I) 12-hour format with leading zeros (01 to 12). 
    # H     (%k) Hour in one or two digits, as needed (0 to 23). 
    # HH    (%H) Hour in two digits (0 to 23). 
    # n     (%t) Minute in one or two digits, as needed (0 to 59). 
    # nn    (%M) Minute in two digits (00 to 59). 
    # s     (%s) Second in one or two digits, as needed (0 to 59).
    # ss    (%S) Second in two digits (00 to 59). 
    # AM/PM (%P) Twelve-hour clock with the uppercase letters "AM" or "PM", as appropriate. 
    # am/pm (%p) Twelve-hour clock with the lowercase letters "am" or "pm", as appropriate. 
    # A/P   (%N) Twelve-hour clock with the uppercase letter "A" or "P", as appropriate. 
    # a/p   (%n) Twelve-hour clock with the lowercase letter "a" or "p", as appropriate. 
    # q     (%q) Date displayed as the quarter of the year (1 to 4). ### 

    my %masks = (
      "LongTime"    => "%H:%M:%S",
      "ShortTime"   => "%H:%M", 
      "d"           => "%e",
      "dd"          => "%d",
      "ddd"         => "%a",
      "dddd"        => "%A",
      "w"           => "%w",
      "m"           => "%o",
      "mm"          => "%m",
      "mmm"         => "%b",
      "mmmm"        => "%B",
      "h"           => "%l",
      "hh"          => "%I",
      "n"           => "%t",
      "nn"          => "%M",
      "s"           => "%s",
      "ss"          => "%S",
      "AM/PM"       => "%P",
      "am/pm"       => "%p",
      "A/P"         => "%N",
      "a/p"         => "%n",
      "q"           => "%q"
    );
    $FormattedDate = "";

    for (my $i = 0; $i < ($#FormatMask + 1); $i++) {
      if(exists($masks{$FormatMask[$i]})) {
        $FormattedDate .= str2time($masks{$FormatMask[$i]}, localtime($DateArray[$ccsTimestamp]));
      } else {
        if ($FormatMask[$i] eq "GeneralDate") {
            $FormattedDate .= str2time("%o/%e/", localtime($DateArray[$ccsTimestamp]));
            $FormattedDate .= substr($DateArray[$ccsYear], 2);
            $FormattedDate .= str2time(", %r", localtime($DateArray[$ccsTimestamp]));
        } elsif ($FormatMask[$i] eq "LongDate") {
            $FormattedDate .= str2time("%A, %B %e, ", localtime($DateArray[$ccsTimestamp]));
            $FormattedDate .= $DateArray[$ccsYear];
        } elsif ($FormatMask[$i] eq "ShortDate") {
            $FormattedDate .= str2time("%o/%e/", localtime($DateArray[$ccsTimestamp]));
            $FormattedDate .= substr($DateArray[$ccsYear], 2);
        } elsif ($FormatMask[$i] eq "yy" ) {
            $FormattedDate .= substr($DateArray[$ccsYear], 2);
        } elsif ($FormatMask[$i] eq "yyyy") {
            $FormattedDate .= $DateArray[$ccsYear];
        } elsif ($FormatMask[$i] eq "dd") {
            $FormattedDate .= $DateArray[$ccsDay];
        } elsif ($FormatMask[$i] eq "mm") {
            $FormattedDate .= $DateArray[$ccsMonth];
        } elsif ($FormatMask[$i] eq "H") {
            $FormattedDate .= $DateArray[$ccsHour] + 0;
        } elsif ($FormatMask[$i] eq "HH") {
            $FormattedDate .= sprintf("%02d", $DateArray[$ccsHour]);
            #$FormattedDate .= $DateArray[$ccsHour];
        } elsif ($FormatMask[$i] eq "nn") {
            $FormattedDate .= $DateArray[$ccsMinute];
        } elsif ($FormatMask[$i] eq "ss") {
            $FormattedDate .= $DateArray[$ccsSecond];
        } elsif ($FormatMask[$i] eq "ddd") {
            $FormattedDate .= $ShortWeekdays[str2time("%w", $DateArray[$ccsTimestamp])];
        } elsif ($FormatMask[$i] eq "dddd") {
            $FormattedDate .= $Weekdays[str2time("%w", $DateArray[$ccsTimestamp])];
        } elsif ($FormatMask[$i] eq "w") {
            $FormattedDate .= (str2time("%w", $DateArray[$ccsTimestamp]) + 1);
        } elsif ($FormatMask[$i] eq "ww") {
            $FormattedDate .= ceil((6 + str2time("%j", $DateArray[$ccsTimestamp]) - str2time("%w", $DateArray[$ccsTimestamp])) / 7);
        } elsif ($FormatMask[$i] eq "mmm") {
            $FormattedDate .= $ShortMonths[str2time("%o", $DateArray[$ccsTimestamp]) - 1];
        } elsif ($FormatMask[$i] eq "mmmm") {
            $FormattedDate .= $Months[str2time("%o", $DateArray[$ccsTimestamp]) - 1];
        } elsif ($FormatMask[$i] eq "q") {
            $FormattedDate .= ceil(str2time("%o", $DateArray[$ccsTimestamp]) / 3);
        } elsif ($FormatMask[$i] eq "y") {
            $FormattedDate .= (str2time("%j", $DateArray[$ccsTimestamp]) + 1);
        } elsif ($FormatMask[$i] eq "n") {
            $FormattedDate .= str2time("%M", $DateArray[$ccsTimestamp]);
        } elsif ($FormatMask[$i] eq "s") {
            $FormattedDate .= str2time("%S", $DateArray[$ccsTimestamp]);
        } elsif ($FormatMask[$i] eq "S") {
            $FormattedDate .= $DateArray[$ccsMilliSecond];
        } elsif ($FormatMask[$i] eq "A/P") {
            my $am = str2time("%N", $DateArray[$ccsTimestamp]);
            $FormattedDate .= $am;
        } elsif ($FormatMask[$i] eq "a/p") {
            my $am = str2time("%n", $DateArray[$ccsTimestamp]);
            $FormattedDate .= $am;
        } elsif ($FormatMask[$i] eq "GMT") {
            my $gmt = str2time("%z", $DateArray[$ccsTimestamp]) / (60 * 60);
            if ($gmt >= 0) { 
              $gmt = "+" . $gmt 
            }
            $FormattedDate .= $gmt;
        } else {
           $FormattedDate .= $FormatMask[$i];
        }
      }
    }
  } else {
    $FormattedDate = "";
  }
  return $FormattedDate;
}
#End CCFormatDate

#CCGetDateArray @0-B5B0644F
sub CCGetDateArray {
  my ($timestamp) = shift;
  my @DateArray = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
  if( !length($timestamp) && !is_int($timestamp)) {
    $timestamp = time();
  }

  $DateArray[$ccsTimestamp] = $timestamp;
  $DateArray[$ccsYear]      = str2time("%Y", localtime($timestamp));
  $DateArray[$ccsMonth]     = str2time("%o", localtime($timestamp));
  $DateArray[$ccsDay]       = str2time("%e", localtime($timestamp));
  $DateArray[$ccsHour]      = str2time("%k", localtime($timestamp));
  $DateArray[$ccsMinute]    = str2time("%M", localtime($timestamp));
  $DateArray[$ccsSecond]    = str2time("%S", localtime($timestamp));

  return @DateArray;
}
#End CCGetDateArray

#CCValidateDate @0-8EBA52F6
sub CCValidateDate {
  my ($ValidatingDate) = shift;

  my @ValidatingDate = @{$ValidatingDate};

  my $IsValid = 1;
  if($ValidatingDate[$ccsMonth] != 0 && $ValidatingDate[$ccsDay] != 0 &&  $ValidatingDate[$ccsYear] != 0) {
    if( $ValidatingDate[$ccsMonth] > 12 || $ValidatingDate[$ccsDay] > 31 ||
        (($ValidatingDate[$ccsMonth] == 4 || $ValidatingDate[$ccsMonth] == 6 || $ValidatingDate[$ccsMonth] == 9 || $ValidatingDate[$ccsMonth] == 11) && $ValidatingDate[$ccsDay] > 30) ||
        ($ValidatingDate[$ccsMonth] == 2 && ((($ValidatingDate[$ccsYear] % 4) == 0 && $ValidatingDate[$ccsDay] > 29) || (($ValidatingDate[$ccsYear] % 4) != 0 && $ValidatingDate[$ccsDay] > 28)))
    ) 
    { $IsValid = 0; }
  }
  return $IsValid;
}
#End CCValidateDate

#CCValidateDateMask @0-F6400245
sub CCValidateDateMask {
my ($ValidatingDate, @FormatMask) = @_;
  my $IsValid = 1;
  if($#FormatMask != -1 && length($ValidatingDate)) {
    my @RegExp = CCGetDateRegExp(@FormatMask);
    $IsValid = ($ValidatingDate =~ m/$RegExp[0]/);
  }
  return $IsValid;
}
#End CCValidateDateMask

#CCParseDate @0-04AA04E5
sub CCParseDate {
  my ($ParsingDate, @FormatMask) = @_;
  if ( $#FormatMask > -1 && length($ParsingDate) ) {
    my @DateArray = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    my @RegExp = CCGetDateRegExp(@FormatMask);

    my $IsValid = ( my @matches = ($ParsingDate =~ /$RegExp[0]/g));

    @matches = ($ParsingDate, @matches);

    for ( my $i = 1; $i < $#matches+1; $i++ ) {
      $DateArray[$RegExp[$i]] = $matches[$i];
    }

    if ( $DateArray[$ccsMonth] == 0 && ($DateArray[$ccsFullMonth] != 0 || $DateArray[$ccsShortMonth] != 0) ) {
      if ( $DateArray[$ccsFullMonth] != 0 ) {
        $DateArray[$ccsMonth] = CCGetIndex($DateArray[$ccsFullMonth], 1, @Months) + 1;
      } elsif ($DateArray[$ccsShortMonth] != 0) {
        $DateArray[$ccsMonth] = CCGetIndex($DateArray[$ccsShortMonth], 1, @ShortMonths) + 1;
      }
    }

    if( $DateArray[$ccsDay] < 1 ) { $DateArray[$ccsDay] = 1; }

    if ( $DateArray[$ccsHour] < 12 && uc(substr($DateArray[$ccsAmPm],0,1)) eq "P" ) {
      $DateArray[$ccsHour] += 12
    }
        
    if ( $DateArray[$ccsHour] == 12 && uc(substr($DateArray[$ccsAmPm],0,1)) eq "A" ) {
      $DateArray[$ccsHour] = 0 
    }

    if ( length($DateArray[$ccsYear]) == 2 ) {
      if ($DateArray[$ccsYear] < 70) {
        $DateArray[$ccsYear] = "20" . $DateArray[$ccsYear];
      } else {
        $DateArray[$ccsYear] = "19" . $DateArray[$ccsYear];
      }
    }  
    if ($DateArray[$ccsYear] < 1971 && $DateArray[$ccsYear] > 0) {
      $DateArray[$ccsAppropriateYear] = $DateArray[$ccsYear] + ((2000 - $DateArray[$ccsYear]) / 28) * 28;
    } elsif ($DateArray[$ccsYear] > 2030) {
      $DateArray[$ccsAppropriateYear] = $DateArray[$ccsYear] - int(($DateArray[$ccsYear] - 2000) / 28) * 28;
    } else {
      $DateArray[$ccsAppropriateYear] = $DateArray[$ccsYear];
    }
    eval {
      local $SIG{__DIE__}= sub {$ParsingDate = ""};
      $DateArray[$ccsTimestamp] = timelocal($DateArray[$ccsSecond], $DateArray[$ccsMinute], $DateArray[$ccsHour],  $DateArray[$ccsDay], $DateArray[$ccsMonth]-1, $DateArray[$ccsAppropriateYear]-1900);
    };

    if ($DateArray[$ccsTimestamp] < 0) {
      $ParsingDate = "";
    } else {
      $ParsingDate = \@DateArray;
    }
  }
  return $ParsingDate;
}
#End CCParseDate

#CCGetDateRegExp @0-FAA0F823
sub CCGetDateRegExp {
  my @FormatMask = @_;
  my @RegExp;
  if ($#FormatMask > -1) {
    my %masks = (

      "GeneralDate" => ["(\\d{1,2})\/(\\d{1,2})\/(\\d{2}),?\\s*(\\d{2})?:?(\\d{2})?:?(\\d{2})?\\s*(AM|PM)?", $ccsMonth, $ccsDay, $ccsYear, $ccsHour, $ccsMinute, $ccsSecond, $ccsAmPm],
      "LongDate"    => ["(" . join("|", @Weekdays) . "), (" . join("|", @Months) . ") (\\d{1,2}), (\\d{4})", $ccsWeek, $ccsFullMonth, $ccsDay, $ccsYear],
      "ShortDate"   => ["(\\d{1,2})\/(\\d{1,2})\/(\\d{2})", $ccsMonth, $ccsDay, $ccsYear],
      "LongTime"    => ["(\\d{1,2}):(\\d{2}):(\\d{2})\\s*(AM|PM)?", $ccsHour, $ccsMinute, $ccsSecond, $ccsAmPm],
      "ShortTime"   => ["(\\d{2}):(\\d{2})", $ccsHour, $ccsMinute],
      "d"           => ["(\\d{1,2})", $ccsDay],
      "dd"          => ["(\\d{2})", $ccsDay],
      "ddd"         => ["(" . join("|", @ShortWeekdays) . ")", $ccsWeek],
      "dddd"        => ["(" . join("|", @Weekdays) . ")", $ccsWeek],
      "w"           => ["\\d"], 
      "ww"          => ["\\d{1,2}"],
      "m"           => ["(\\d{1,2})", $ccsMonth], 
      "mm"          => ["(\\d{2})", $ccsMonth], 
      "mmm"         => ["(" . join("|", @ShortMonths) . ")", $ccsShortMonth], 
      "mmmm"        => ["(" . join("|", @Months) . ")", $ccsFullMonth],
      "y"           => ["\\d{1,3}"], 
      "yy"          => ["(\\d{2})", $ccsYear], 
      "yyyy"        => ["(\\d{4})", $ccsYear], 
      "q"           => ["\\d"],
      "h"           => ["(\\d{1,2})", $ccsHour], 
      "hh"          => ["(\\d{2})", $ccsHour], 
      "H"           => ["(\\d{1,2})", $ccsHour], 
      "HH"          => ["(\\d{2})", $ccsHour],
      "n"           => ["(\\d{1,2})", $ccsMinute], 
      "nn"          => ["(\\d{2})", $ccsMinute], 
      "s"           => ["(\\d{1,2})", $ccsSecond], 
      "ss"          => ["(\\d{2})", $ccsSecond], 
      "AM/PM"       => ["(AM|PM)", $ccsAmPm], 
      "am/pm"       => ["(am|pm)", $ccsAmPm], 
      "A/P"         => ["(A|P)", $ccsAmPm], 
      "a/p"         => ["(a|p)", $ccsAmPm],
      "GMT"         => ["([\+\-]\\d{2})", $ccsGMT],
      "S"           => ["(\\d{1,6})", $ccsMilliSecond]
    );
    $RegExp[0]   = "";
    my $RegExpIndex = 1;
    my $is_date = 0; my $is_datetime = 0;
    for ( my $i = 0; $i < ($#FormatMask + 1); $i++ ) {
      if ( exists $masks{$FormatMask[$i]} ) {

        my $MaskArray = $masks{ $FormatMask[$i] };
        if($i == 0 && ($MaskArray->[1] == $ccsYear || $MaskArray->[1] == $ccsMonth 
          || $MaskArray->[1] == $ccsFullMonth || $MaskArray->[1] == $ccsWeek || $MaskArray->[1] == $ccsDay)) {
          $is_date = 1;
        } elsif($is_date && !$is_datetime && $MaskArray->[1] == $ccsHour) {
          $is_datetime = 1;
        }

        $RegExp[0] .= $MaskArray->[0];
        if($is_datetime) { $RegExp[0] .= "?"; }
        for ( my $j = 1; $j < ( $#{$MaskArray} + 1 ); $j++ ) {
          $RegExp[$RegExpIndex++] = $MaskArray->[$j];
        }
      } else {
        if($is_date && !$is_datetime && $i < ($#FormatMask + 1) && $masks{ $FormatMask[$i + 1] }->[1] == $ccsHour) {
          $is_datetime = 1;
        }
        $RegExp[0] .= CCAddEscape($FormatMask[$i]);
        if($is_datetime) { $RegExp[0] .= "?"; }
      }
    }
    $RegExp[0] = replace($RegExp[0], " ", "\\s*");
    $RegExp[0] = "^" . $RegExp[0] . "\$";
  }
  return @RegExp;
}
#End CCGetDateRegExp

#CCAddEscape @0-1DF1E38D
sub CCAddEscape {
  my $FormatMask = shift;
  my %meta_characters = ( "\\" => "\\\\", "^" => "\^", "\$" => "\$", "." => "\.", "[" => "\[", "|" => "\|", "(" => "\(", ")" => "\)", "?" => "\?", "*" => "\*", "+" => "\+", "{" => "\{", "-" => "-", "]" => "\]" );
  $FormatMask =~ s/(\^|\$|\.|\[|\||\(|\)|\?|\*|\+|\{|-|\])+/$meta_characters{$1}/ge;
  return $FormatMask;
}
#End CCAddEscape

#CCGetIndex @0-BACBF5AC
sub CCGetIndex {
  my ($Value, $IgnoreCase, @ArrayValues) = @_;
  $IgnoreCase ||= 1;
  my $index = 0;
  for ( my $i = 0; $i < $#ArrayValues+1; $i++ ) {
    if ( ( $IgnoreCase && (uc($ArrayValues[$i]) eq uc($Value)) ) || ($ArrayValues[$i] eq $Value)) {
      $index = $i;
      last;
    }
  }
  return $index;
}
#End CCGetIndex

#ctime_format @0-2DB89EF8
sub ctime_format {
    my($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst, $TZname) = @_;
    ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst, $TZname) = localtime($sec) unless defined $min;
    $year += 1900;
    $TZname .= ' ' if $TZname;
    return sprintf("%s %s %2d %2d:%02d:%02d %s%4d", $ShortWeekdays[$wday], $ShortMonths[$mon], $mday, $hour, $min, $sec, $TZname, $year);
}
#End ctime_format

#str2time @0-82D7C80F
sub str2time {      
  my ($template, $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = @_;
  
  #  %%  Percent
  #  %a  Day of the week abbr
  #  %A  Day of the week
  #  %b  Month abbr
  #  %B   Month
  #  %d   DD
  #  %e   Numeric day of the month
  #  %h   Month abbr
  #  %H   Hour, 24 hour clock, leading 0's)
  #  %I   Hour, 12 hour clock, leading 0's)
  #  %k   Hour
  #  %l   Hour, 12 hour clock
  #  %m   Month number, starting with 1
  #  %M   Minute, leading 0's
  #  %p   AM or PM 
  #  %s   seconds, without leading 0's
  #  %S   seconds, leading 0's
  #  %y  year (2 digits)
  #  %Y  year (4 digits)
  #  %w   day of the week, numerically, Sunday == 0
  #  %o  Month of the year in one or two numeric digits, as needed (1 to 12).
  #  %t   Minute in one or two digits, as needed (0 to 59). 
  #  %W   week number, Monday as first day of week
  #  %j   day of the year
  #  %D   MM/DD/YY
  #  %c   ctime format: Sat Nov 19 21:05:57 1994
  #  %r   time format: 09:05:57 PM
  #  %R   time format: 21:05
  #  %T   time format: 21:05:57
  #  %U   week number, Sunday as first day of week
  #  %x   date format: 11/19/94
  #  %X   time format: 21:05:57
  #  %Z   timezone in ascii. eg: PST

  my %str2time_conversion = 
  (
     '%',  sub { '%' },                                
     'a',  sub { $ShortWeekdays[$wday] },              
     'A',  sub { $Weekdays[$wday] },
     'b',  sub { $ShortMonths[$mon] },
     'B',  sub { $Months[$mon] },
     'c',  sub { ctime_format($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst, "") },
     'd',  sub { sprintf("%02d", $mday); },
     'D',  sub { sprintf("%02d/%02d/%02d", $mon+1, $mday, $year % 100) },
     'e',  sub { $mday; },
     'h',  sub { $ShortMonths[$mon] },
     'H',  sub { sprintf("%02d", $hour) },
     'I',  sub { sprintf("%02d", $hour % 12 || 12) },
     'j',  sub { sprintf("%03d", $yday + 1) },
     'k',  sub { $hour },
     'l',  sub { $hour % 12 || 12 },
     'm',  sub { sprintf("%02d", $mon+1); },
     'M',  sub { sprintf("%02d", $min) },
     'N',  sub { $hour > 11 ? "P" : "A" },
     'n',  sub { $hour > 11 ? "p" : "a" },
     'o',  sub { $mon + 1 },
     'P',  sub { $hour > 11 ? "PM" : "AM" },
     'p',  sub { $hour > 11 ? "pm" : "am" },
     'q',  sub { ceil(($mon + 1) / 3) },
     'r',  sub { sprintf("%02d:%02d:%02d %s", $hour % 12 || 12, $min, $sec, $hour > 11 ? 'PM' : 'AM') },
     'R',  sub { sprintf("%02d:%02d", $hour, $min) },
     's',  sub { $sec },
     'S',  sub { sprintf("%02d", $sec) },
     't',  sub { $min },
     'T',  sub { sprintf("%02d:%02d:%02d", $hour, $min, $sec) },
     'w',  sub { $wday+1 },
     'y',  sub { sprintf("%02d",$year % 100) },
     'Y',  sub { $year + 1900 },
     'x',  sub { sprintf("%02d/%02d/%02d", $mon + 1, $mday, $year % 100) },
     'X',  sub { sprintf("%02d:%02d:%02d", $hour, $min, $sec) },
     'z',  sub { },
  );

    $template =~ s/%([%aAbBcdDehHIjklmMNnoPpqrRSstTwxXyYZ])/&{$str2time_conversion{$1}}()/egs;
    return $template;
}
#End str2time

#CCFormatBoolean @0-8E1B4CC7
sub CCFormatBoolean {
  my ($BooleanValue, @Format) = @_;
  my $Result = $BooleanValue;
  if ( $#Format > -1 ) {
    if ( $BooleanValue eq "1" || lc($BooleanValue) eq "true" ) {
      $Result = $Format[0];
    } elsif ( $BooleanValue eq "0" || lc($BooleanValue) eq "false" ) {
      $Result = $Format[1];
    } else {
      $Result = $Format[2];
    }
  }
  return $Result;
}
#End CCFormatBoolean

#CCParseBoolean @0-CCEDC22A
sub CCParseBoolean {
  my ($Value, @Format) = @_;
  if ($#Format > -1) {
    return 1 if (lc($Value) eq lc($Format[0]));
    return 0 if (lc($Value) eq lc($Format[1]));
    return "" if (lc($Value) eq lc($Format[2]));
  }
  return 0 if ($Value eq "0" || lc($Value) eq "false");
  return 1 if ($Value eq "1" || lc($Value) eq "true");
  return "";
}
#End CCParseBoolean

#CCGetBooleanFormat @0-0DFF41C8
sub CCGetBooleanFormat {
  my (@Format) = @_;
  my $FormatString = "";
  if( @Format ) {
    for(my $i = 0; $i < @Format; $i++) {
      if(length($Format[$i])) {
        if(length($FormatString)) { $FormatString .= ";"; }
        $FormatString .= $Format[$i];
      }
    }
  }
  return $FormatString;
}
#End CCGetBooleanFormat

#CCValidateBoolean @0-68318485
sub CCValidateBoolean {
  my ($BooleanValue, @Format) = @_; 
  return $BooleanValue eq ""
     || lc($BooleanValue) eq "true"
     || lc($BooleanValue) eq "false"
     || $BooleanValue eq "0"
     || $BooleanValue eq "1"
     || ($#Format > -1
        && (lc($BooleanValue) eq lc($Format[0])
            || lc($BooleanValue) eq lc($Format[1])
            || lc($BooleanValue) eq lc($Format[2]))); 
}
#End CCValidateBoolean

#CCFormatNumber @0-95B8946F
sub CCFormatNumber {
  my ($NumberToFormat, @FormatArray) = @_;
  
  my $Result;
  my $IsExtendedFormat; 
  my $IsNegative; 
  my $DecimalSeparator; 
  my $PeriodSeparator; 
  my $ObligatoryBeforeDecimal; 
  my $DigitsBeforeDecimal; 
  my $BeforeDecimal; 
  my $AfterDecimal;

  if ( length($NumberToFormat) && @FormatArray > 0) {

    $IsExtendedFormat = $FormatArray[0];
    $IsNegative       = ($NumberToFormat < 0) ? 1 : 0;
    $NumberToFormat   = abs($NumberToFormat);
    $NumberToFormat  *= $FormatArray[7] if ($FormatArray[7]);
  
    if ( $IsExtendedFormat ) { # Extended format
      $DecimalSeparator        = $FormatArray[2];
      $PeriodSeparator         = $FormatArray[3];
      $ObligatoryBeforeDecimal = 0;
      $DigitsBeforeDecimal     = 0;
      $BeforeDecimal           = $FormatArray[5];
      $AfterDecimal            = $FormatArray[6];
      if ( ref($BeforeDecimal) eq "ARRAY" ) {
        for ( my $i = 0; $i < ($#{$BeforeDecimal} + 1); $i++) {
          if ( $BeforeDecimal->[$i] eq "0" ) {
            $ObligatoryBeforeDecimal++;
            $DigitsBeforeDecimal++;
          } elsif ($BeforeDecimal->[$i] eq "#") {
            $DigitsBeforeDecimal++;
      }  
        }
      }
      my $ObligatoryAfterDecimal = 0;
      my $DigitsAfterDecimal     = 0;
      if ( ref($AfterDecimal) eq "ARRAY" ) {
        for ( my $i = 0; $i < ($#{$AfterDecimal} + 1); $i++ ) {
          if ($AfterDecimal->[$i] eq "0" ) {
            $ObligatoryAfterDecimal++;
            $DigitsAfterDecimal++;
          } elsif ( $AfterDecimal->[$i] eq "#" ) {
            $DigitsAfterDecimal++;
      }  
        }
      }
  
      $NumberToFormat = number_format($NumberToFormat, $DigitsAfterDecimal, ".", "");
      my @NumberParts = split /\./, $NumberToFormat;

      my $LeftPart  = ( $NumberParts[0] eq "O" ) ? () : $NumberParts[0];
      $LeftPart = "" if($LeftPart eq "0");
      my @LeftPart  = split //, $LeftPart;
      my $RightPart = length($NumberParts[1]) ? $NumberParts[1] : "";
      my @RightPart = split //, $RightPart;
      my $j = length($LeftPart);

      if ( ref($BeforeDecimal) eq "ARRAY" ) {
        my $RankNumber = 0;
        my $i = $#{$BeforeDecimal} + 1;
        while ($i > 0 || $j > 0) {
          if(($i > 0 && ($BeforeDecimal->[$i - 1] eq "#" || $BeforeDecimal->[$i - 1] eq "0")) || ($j > 0 && $i < 1)) {
            $RankNumber++;
            my $CurrentSeparator = ($RankNumber % 3 == 1 && $RankNumber > 3 && $j > 0) ? $PeriodSeparator : ""; 
            if ( $ObligatoryBeforeDecimal > 0 && $j < 1 ) {
              $Result = "0" . $CurrentSeparator . $Result 
            } elsif ( $j > 0 ) {
              $Result = $LeftPart[$j - 1] . $CurrentSeparator . $Result;
            }
            $j--;
            $ObligatoryBeforeDecimal--;
            $DigitsBeforeDecimal--;
            if ( $DigitsBeforeDecimal == 0 && $j > 0 ) {
              for (; $j > 0; $j-- ) {
                $RankNumber++;
                $CurrentSeparator = ($RankNumber % 3 == 1 && $RankNumber > 3 && $j > 0) ? $PeriodSeparator : "";
                $Result = $LeftPart[$j - 1] . $CurrentSeparator . $Result;
              }
            }
          } elsif ($i > 0) {
            $BeforeDecimal->[$i - 1] = replace($BeforeDecimal->[$i - 1], "##", "#");
            $BeforeDecimal->[$i - 1] = replace($BeforeDecimal->[$i - 1], "00", "0");
            $Result = $BeforeDecimal->[$i - 1] . $Result;
          }
          $i--;
        }
      }

      # Left part after decimal
      my $RightResult   = "";
      my $IsRightNumber = 0;
      if ( ref($AfterDecimal) eq "ARRAY" ) {
        my $IsZero = 1;
        for ( my $i = ( $#{$AfterDecimal} + 1 ); $i > 0; $i-- ) {
          if ( $AfterDecimal->[$i - 1] eq "#" || $AfterDecimal->[$i - 1] eq "0") {
            if ( $DigitsAfterDecimal > $ObligatoryAfterDecimal ) {
              $IsZero = 0 if ( $RightPart[$DigitsAfterDecimal - 1] ne "0" && $NumberToFormat != 0 );
              if ( !$IsZero ) {
                $RightResult = $RightPart[$DigitsAfterDecimal - 1] . $RightResult;
                $IsRightNumber = 1;
              }
            } else {
              $RightResult = $RightPart[$DigitsAfterDecimal - 1] . $RightResult;
              $IsRightNumber = 1;
            }
            $DigitsAfterDecimal--;
          } else {
            $AfterDecimal->[$i - 1] = replace($AfterDecimal->[$i - 1], "##", "#");
            $AfterDecimal->[$i - 1] = replace($AfterDecimal->[$i - 1], "00", "0");
            $RightResult = $AfterDecimal->[$i - 1] . $RightResult;
          }
        }
      }
    
      $Result .= $DecimalSeparator if ( $IsRightNumber && $NumberParts[1] ne "" );
      $Result .= $RightResult;
      $Result  = "-" . $Result if ( !$FormatArray[4] && $IsNegative && $Result );

    } else { # Simple format
      if ( !$FormatArray[4] && $IsNegative ) {
        $Result = "-" . $FormatArray[5] . number_format($NumberToFormat, $FormatArray[1], $FormatArray[2], $FormatArray[3]) . $FormatArray[6];
      } else {
        $Result = $FormatArray[5] . number_format($NumberToFormat, $FormatArray[1], $FormatArray[2], $FormatArray[3]) . $FormatArray[6];
      }
    }

    $Result = CCToHTML($Result) if ( !$FormatArray[8] );
    $Result = "<FONT COLOR=\"" . $FormatArray[9] . "\">" . $Result . "</FONT>" if ( length($FormatArray[9]) );
      
  } else {
    $Result = $NumberToFormat;
  }

  return $Result;
}
#End CCFormatNumber

#CCValidateNumber @0-A486C89C
sub CCValidateNumber {
  my ($NumberValue, @FormatArray) = @_;
  my $is_valid = 1;
  if ( length($NumberValue) ) {
    $NumberValue = CCCleanNumber($NumberValue, @FormatArray);
    $is_valid = is_numeric($NumberValue);
  }
  return $is_valid;
}
#End CCValidateNumber

#CCParseNumber @0-D90217E8
sub CCParseNumber {
  my ($NumberValue, $DataType, @FormatArray) = @_;
  $NumberValue = CCCleanNumber($NumberValue, @FormatArray);
  if ( ($#FormatArray > -1 && $FormatArray[0] ne "") && length($NumberValue) ) {
    if ($FormatArray[4]) { # Is use parenthesis
      $NumberValue = - abs(str2doubl($NumberValue));
    }
    $NumberValue /= $FormatArray[7];
  }
  if(length($NumberValue)) {
    if ($DataType == $ccsFloat) {
      $NumberValue = str2doubl($NumberValue);
    } else {
      $NumberValue = round($NumberValue,0);
    }
  }
  return $NumberValue;
}
#End CCParseNumber

#CCCleanNumber @0-3D381A0D
sub CCCleanNumber {
  my ($NumberValue, @FormatArray) = @_;
  if ( $#FormatArray > -1 ) {
    my $IsExtendedFormat = $FormatArray[0];
    if ($IsExtendedFormat) { # Extended format
      my $BeforeDecimal = $FormatArray[5];
      my $AfterDecimal = $FormatArray[6];
      if ( ref($BeforeDecimal) eq "ARRAY" ) {
        for ( my $i = $#{$BeforeDecimal}+1; $i > 0; $i--) {
          if ($BeforeDecimal->[$i - 1] ne "#" && $BeforeDecimal->[$i - 1] ne "0") {
            my $search = $BeforeDecimal->[$i - 1];
            $search = ($search eq "##" || $search eq "00") ? substr($search,0,1) : $search;
            $NumberValue = replace($NumberValue, $search, "");
          }
        }
      }
      if(ref($AfterDecimal) eq "ARRAY") {
        for(my $i = $#{$AfterDecimal}+1; $i > 0; $i--) {
          if($AfterDecimal->[$i - 1] ne "#" && $AfterDecimal->[$i - 1] ne "0") {
            my $search = $AfterDecimal->[$i - 1];
            $search = ($search eq "##" || $search eq "00") ? substr($search,0,1) : $search;
            $NumberValue = replace($NumberValue, $search, "");
          }
        }
      }
    } else { # Simple format
      if(length($FormatArray[5])) {
        $NumberValue = replace($NumberValue, $FormatArray[5], "");
      }
      if(length($FormatArray[6])) {
        $NumberValue = replace($NumberValue, $FormatArray[6], "");
      }
    }

    if(length($FormatArray[3])) {
      $NumberValue = replace($NumberValue, $FormatArray[3], ""); # Period separator
    }
    if(length($FormatArray[2])) {
      $NumberValue = replace($NumberValue, $FormatArray[2], "."); # Decimal separator
    }
    if(length($FormatArray[9]))
    {
      $NumberValue = replace($NumberValue, "<FONT COLOR=\"" . $FormatArray[9] . "\">", "");
      $NumberValue = replace($NumberValue, "</FONT>", "");
    }
  }
  $NumberValue = replace($NumberValue, ",", "."); # Decimal separator

  return $NumberValue;
}
#End CCCleanNumber

#CCParseInteger @0-58847630
sub CCParseInteger {
  my ($NumberValue, @FormatArray) = @_;
  return CCParseNumber($NumberValue, $ccsInteger, @FormatArray);
}
#End CCParseInteger

#CCParseFloat @0-87EA91B2
sub CCParseFloat {
  my ($NumberValue, @FormatArray) = @_;
  return CCParseNumber($NumberValue, $ccsFloat, @FormatArray);
}
#End CCParseFloat

#str2doubl @0-989264EF
sub str2doubl {
  my $number_str = shift;
  $number_str =~ /\.(\d+)\D*/;
  my $sprintf_mask = "%." . length($1) . "f";
  return sprintf($sprintf_mask,$number_str);
}
#End str2doubl

#is_numeric @0-E55002CA
sub is_numeric {
  my $number_str = shift;
  return ($number_str =~ /^\-?(\d+\.?\d*|\d*\.?\d+)$/);
}
#End is_numeric

#is_int @0-68E5CE84
sub is_int {
  my $number_str = shift;
  return ($number_str =~ /^\-?\d+$/);
}
#End is_int

#number_format @0-1F718AE8
sub number_format {
    my ($number, $precision, $dec_point, $thousands_sep, $trailing_zeroes,) = @_;
    # $precision       = 0 unless defined $precision;
    $trailing_zeroes = 0 unless defined $trailing_zeroes;
    my $sign = $number <=> 0;
    $number = abs($number) if $sign < 0;
    $number = round($number, $precision);
    my $integer = int($number);
    my $decimal;
    $decimal = substr($number, length($integer)+1) if (length($integer) < length($number));
    $dec_point = "." if (length($decimal) && !$dec_point );
    $decimal = '' unless defined $decimal;
    $decimal .= '0'x( $precision - length($decimal) ) if $trailing_zeroes && $precision > length($decimal);
    $integer = '0'x(3 - (length($integer) % 3)).$integer;
    $integer = join($thousands_sep, grep {$_ ne ''} split(/(...)/, $integer));
    $integer =~ s/^0+(\Q$thousands_sep\E)*//;
    # $integer =~ s/^0+\Q$thousands_sep\E?[^1..9]+//;
    # $integer =~ s/^0+\Q$thousands_sep\E?//;
    $integer = '0' if $integer eq '';
    my $result = ((defined $decimal && length $decimal) ? join($dec_point, $integer, $decimal) : $integer);
    return ($sign < 0) ? "-" . $result : $result;
}
#End number_format

#CCCheckValue @0-2A388EE1
sub CCCheckValue {
  my ($Value, $DataType, $ControlType) = @_;
  $Value ||= defined($Value) ? $Value : "";

  my $result = 0;
  if ($DataType == $ccsInteger) {
    $result = is_int($Value); 
  } elsif ($DataType == $ccsFloat) {
    $result = is_numeric($Value);
  } elsif ($DataType == $ccsDate) {
    $result = ( ref($Value) eq "ARRAY" || is_numeric($Value) ) ? 1 : 0;
  } elsif ($DataType == $ccsBoolean) {
    $result = ($Value eq "1" || $Value eq "0" || $Value eq "true" || $Value eq "false") ? 1 : 0; 
  }
  return $result;
}
#End CCCheckValue

#round @0-C471A8A7
sub round {
    my ($number, $precision) = @_;
    
    if ($precision eq "" && ($number - int($number)) > 0 ) {
      $number =~ /\.(\d+)\D*/;
      $precision = length($1);
    }
    
    $number    = 0 unless defined $number;
    my $sign = $number <=> 0;
    my $multiplier = (10 ** $precision);
    my $result = abs($number);
    $result = int(($result * $multiplier) + .5000001) / $multiplier;
    $result = -$result if $sign < 0;
    return sprintf("%." . $precision . "f",$result);
}
#End round

#CCCompareValues @0-801D2404
sub CCCompareValues{
  #CCCompareValues($Value1, $Value2, $DataTypem $Format);
  #warning! Array paramaters must be pass as reference
  my $Value1 = shift;
  my $Value2 = shift;
  my $DataType = shift;
  $DataType = defined($DataType) ? $DataType : $ccsText;
  my $Format = shift;
  if ($DataType ==  $ccsInteger || $DataType == $ccsFloat) {
    if ($Value1 eq "" ||  $Value2 eq "") {
      return $Value1 cmp  $Value2;
    } elsif ($Value1 > $Value2) {
      return 1;
    } elsif ($Value1 < $Value2) {
      return -1;
    } else {
      return 0;
    }
  } elsif ($DataType == $ccsText || $DataType ==  $ccsMemo) {
      return $Value1 cmp $Value2;
  } elsif ($DataType ==  $ccsBoolean) {
      if (($Value1 eq "1") or ($Value1 eq "0")){
        $val1=$Value1;
      } elsif (length($Value1) && CCValidateBoolean($Value1,$Format)) {
        $val1=CCParseBoolean($Value1,$Format);
      } else {
        return 1;
      }
      if (($Value2 eq "1") or ($Value2 eq "0")){
        $val2=$Value2;
      } elsif (length($Value2) && CCValidateBoolean($Value2,$Format)) {
        $val2=CCParseBoolean($Value2,$Format);
      } else {
        return 1;
      }
      return ($val1 eq $val2) ? 0 : 1;
  } elsif ($DataType == $ccsDate) {
    if (ref($Value1)  eq 'ARRAY' && ref($Value2) eq 'ARRAY') {
      if ($Value1->[$ccsTimestamp] && $Value2->[$ccsTimestamp]) {
        if ($Value1->[$ccsTimestamp] > $Value2->[$ccsTimestamp]) {
          return 1;
        } elsif ($Value1->[$ccsTimestamp] < $Value2->[$ccsTimestamp]) {
          return -1;
        } else {
          return 0;
        }
      } else {
        for(my $i=0; $i<=$#{$Value1}; $i++){
          return 1 if ($Value1->[$i] != $Value2->[$i])
        }
        return 0;
      }
    } elsif (ref($Value1) eq 'ARRAY') {
      my $FormattedValue = CCFormatValue($Value1, $DataType, @{$Format});
      return CCCompareValues($FormattedValue, $Value2);
    } elsif (ref($Value2) eq 'ARRAY') {
      my $FormattedValue = CCFormatValue($Value2, $DataType, @{$Format});
      return CCCompareValues($Value1,$FormattedValue);
    } else {
      return CCCompareValues($Value1,$Value2);
    }
  }
  return 1;
}
#End CCCompareValues

#CCCheckSSL @0-199F70C1
sub CCCheckSSL {
  if ( lc($cgi->https) ne "on") {
    print_http_header();
    print "SSL connection error. This page can be accessed only via secured connection.";
    exit;
  }
}
#End CCCheckSSL

#CCSecurityRedirect @0-30E952FF
sub CCSecurityRedirect {
    my ($GroupsAccess, $URL) = @_;
    my $ReturnPage = $ENV{"SCRIPT_NAME"};
    my $QueryString = CCGetQueryString("QueryString", "");
    my $ErrorType = CCSecurityAccessCheck($GroupsAccess);
    if ( $ErrorType ne "success" ) {
        if ( !length($URL) ) {
            $Link = $ServerURL . "senha.php";
        } else {
            $Link = $URL;
        }
        if ( length($QueryString) ) {
            $ReturnPage .= "?" . $QueryString;
        }
        SaveSession();
        print "Status: 302 Found\nLocation: " . $Link . "?ret_link=" . CCToURL($ReturnPage) . "&type=" . $ErrorType . "\n\n";
        exit;
    }
}
#End CCSecurityRedirect

#CCSecurityAccessCheck @0-621FF2E7
sub CCSecurityAccessCheck {
    my ($GroupsAccess) = shift;
    my $ErrorType = "success";
    if ( !length(CCGetUserID()) ) {
        $ErrorType = "notLogged";
    } else {
        my $GroupID = CCGetGroupID();
        if( !length($GroupID) ) {
            $ErrorType = "groupIDNotSet";
        } else {
            if ( !CCUserInGroups($GroupID, $GroupsAccess) ) {
                $ErrorType = "illegalGroup";
            }
        }
    }
    return $ErrorType;
}
#End CCSecurityAccessCheck

#CCGetUserID @0-9A97106A
sub CCGetUserID {
    return CCGetSession("UserID");
}
#End CCGetUserID

#CCGetGroupID @0-9D692D0D
sub CCGetGroupID {
    return CCGetSession("GroupID");
}
#End CCGetGroupID

#CCGetUserLogin @0-30A2669C
sub CCGetUserLogin {
    return CCGetSession("UserLogin");
}
#End CCGetUserLogin

#CCGetUserPassword @0-2C371CA9
sub CCGetUserPassword {
    return "";
}
#End CCGetUserPassword

#CCUserInGroups @0-8039E010
sub CCUserInGroups {
    my ($GroupID, $GroupsAccess) = @_;
    my $Result = "";
    if ( length($GroupsAccess) ) {
        $Result = (index(";" . $GroupsAccess . ";", ";" . $GroupID . ";") != -1);
    }
    else
    {
        $Result = 1;
    }
    return $Result;
}
#End CCUserInGroups

#CCLoginUser @0-926A81EF
sub CCLoginUser {
    my ($Login, $Password) = @_;
    $db = clsDBFaturar->new();
    my $SQL = "SELECT ID, GRP_ID FROM TABOPE WHERE CODOPE=" . $db->ToSQL($Login, $ccsText) . " AND PASSWO=" . $db->ToSQL($Password, $ccsText);
    $db->query($SQL);
    my $Result = $db->next_record();
    if ( $Result ) {
        CCSetSession("UserID", $db->f("ID"));
        CCSetSession("UserLogin", $Login);
        CCSetSession("GroupID", $db->f("GRP_ID"));
    }
    $db->{sth} = undef;
    $db->{dbh}->disconnect();
    return $Result;
}
#End CCLoginUser

#CCLogoutUser @0-BDBD65FC
sub CCLogoutUser {
    CCSetSession("UserID", "");
    CCSetSession("UserLogin", "");
    CCSetSession("GroupID", "");
}
#End CCLogoutUser

#Faturar Connection Class @23-D4718454
package clsDBFaturar;
use DBI;
use vars qw/@ISA/;
    
use Classes;
@ISA = qw/DBI clsErrors/;
use Common;

sub new {
    my $class = shift;
    my $self = {};
    bless $self, $class;
    $self->{LastSQL}      = "";
    $self->{RecordsCount} = 0;
    $self->{SQL}          = "";
    $self->{Where}        = "";
    $self->{Order}        = "";
    $self->{Parameters}   = "";
    $self->{wp}           = "";
    $self->{AbsolutePage} = 0;
    $self->{PageSize}     = 0;
    $self->{DB}            = "MySQL";
    $self->{DataSource}    = 'Sifat_Homologa';
    $self->{UserName}      = "sifat001";
    $self->{Password}      = "sifat001_hml";
    $self->{PrintError} = 0;
    $self->{RaiseError} = 0;
    $self->{RecordNumber}  = 0;
    $self->{DateFormat}    = ["mm", "/", "dd", "/", "yyyy", " ", "HH", ":", "nn", ":", "ss"];
    $self->{BooleanFormat} = ["true", "false", ""];
    $self->{Uppercase}     = 0;
    $self->{Errors}        = clsErrors->new();
    $self->{Parameters}    = undef;
    $self->{LastSQL}       = "";
    $self->{dbh} = undef;
    $self->{dbh} = DBI->connect( $self->{DataSource}, $self->{UserName}, $self->{Password}, { PrintError => $self->{PrintError}, RaiseError => $self->{RaiseError} } ) 
                or die $DBI::errstr;
    $self->{dbh}->{LongTruncOk} = 0;
    $self->{sth} = undef;
    $self->{RecordHashRef} = "";
    return $self;
}

sub MoveToPage {
    my ($self, $Page) = @_;
    if ( $self->{RecordNumber} == 0 && $self->{PageSize} != 0 && $Page != 0 ) {
        while ( $self->{RecordNumber} < ($Page - 1) * $self->{PageSize} && $self->next_record() && $Page <= $self->{RecordsCount}) {
            $self->{RecordNumber}++;
        }
    }
}

sub PageCount {
    my $self = shift;
    if ( $self->{PageSize} ) {
        my $initial = $self->{RecordsCount} / $self->{PageSize};
        my $rounded = sprintf("%.0f", $self->{RecordsCount} / $self->{PageSize});
        return $rounded < $initial ? $rounded + 1 : $rounded;
    } else {
        return 1
    }
}

sub ToSQL {
    my ($self, $Value, $ValueType) = @_;
    if ( length($Value) ) {
        if ( $ValueType == $ccsInteger || $ValueType == $ccsFloat ) {
            return ( 0 + replace($Value, ",", ".") );
        } elsif ( $ValueType == $ccsDate ) {
            if (ref($Value) eq "ARRAY") {
                $Value = CCFormatDate($Value, @{$self->{DateFormat}});
            }
            return $self->{dbh}->quote($Value);
        } elsif ( $ValueType == $ccsBoolean ) {
            $Value = CCFormatBoolean($Value, @{$self->{BooleanFormat}});
            return $Value;
        } else {
            return $self->{dbh}->quote($Value);
        }
    } else {
    return "NULL";
    }
}

sub SQLValue {
    my ($self, $Value, $ValueType) = @_;
    if ( length($Value) ) {
        if ( $ValueType == $ccsInteger || $ValueType == $ccsFloat ) {
            return ( 0 + replace($Value, ",", ".") );
        } elsif ( $ValueType == $ccsDate ) {
            if (ref($Value) eq "ARRAY") {
                $Value = CCFormatDate($Value, @{$self->{DateFormat}});
            }
            return $Value;
        } elsif ( $ValueType == $ccsBoolean ) {
            $Value = CCFormatBoolean($Value, @{$self->{BooleanFormat}});
            return $Value;
        } else {
            $Value =~ s/['\\]/\\$1/g;
            return $Value;
        }
    } else {
        return "";
    }
}

sub query {
    my ($self, $sql) = @_;
    return 0 if (!$sql);
    $self->{LastSQL} = $sql;
    $self->{sth}->finish() if ($self->{sth});
    undef $self->{sth};
    $self->{sth} = $self->{dbh}->prepare( $sql );
    if ($DBI::errstr) { 
        $self->{Errors}->addError($DBI::errstr);
        return 0
    }
    $self->{sth}->execute();
    if ($DBI::errstr) {
        $self->{Errors}->addError($DBI::errstr);
        return 0
    }
}

sub next_record {
  my $self = shift;
  $self->{RecordHashRef}  = $self->{sth}->fetchrow_hashref();
  return $self->{RecordHashRef} ? 1 : 0;
}

sub f {
  my ($self, $field_name) = @_;
  if ($field_name =~ /^\d+$/) { 
    $field_name = $self->{sth}->{NAME}->[$field_name];
    return $self->{RecordHashRef}->{$field_name};
  } else {
    $field_name = $self->{Uppercase} ? uc($field_name) : $field_name;
    return $self->{RecordHashRef}->{$field_name};
  }
}

sub n {
  my ($self, $field_number) = @_;
  return $self->f($self->{sth}->{NAME}->[$field_number]);
}

sub num_rows {
  my ($self, $sql) = @_;
  my $rec_count = 0;
  if ($self->query($sql) != 0) {
    $rec_count+=1 while ($self->{sth}->fetchrow_arrayref());
    $self->{sth} = undef;
  }
  return $rec_count;
}




sub finalize {
  my $self = shift;
  return if($self->{finalized});
  $self->{finalized} = 1;
  if($self->{dbh}){
    $self->{sth}->finish() if ($self->{sth});
    undef $self->{sth};  
    $self->{dbh}->disconnect();
    undef $self->{dbh};
  }
}

sub DESTROY {
   my $self = shift;
   $self->finalize();
}

sub OptimizeSQL {
    my $self = shift;
    my $SQL = shift;
    my $PageSize = $self->{PageSize} + 0;
    if (!$PageSize) { return $SQL; };
    my $Page = $self->{AbsolutePage} + 0 ? $self->{AbsolutePage} + 0 : 1;
    $SQL .= " LIMIT " . (($Page - 1) * $PageSize) . "," .$PageSize;
    return $SQL;
}
#End Faturar Connection Class
1;
