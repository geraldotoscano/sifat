function cdd_menu202499(){//////////////////////////Start Menu Data/////////////////////////////////

//**** NavStudio, (c) 2004, OpenCube Inc.,  -  www.opencube.com ****
//Note: This data file may be manually customized outside of the visual interface.

    //Unique Menu Id
	this.uid = 202499


/**********************************************************************************************

                               Icon Images

**********************************************************************************************/


    //Inline positioned icon images (flow with the item text)

	this.rel_icon_image0 = "squares_0.gif"
	this.rel_icon_rollover0 = "squares_0_hl.gif"
	this.rel_icon_image_wh0 = "13,8"

	this.rel_icon_image1 = "corner_arrow.gif"
	this.rel_icon_rollover1 = "corner_arrow.gif"
	this.rel_icon_image_wh1 = "13,10"

	this.rel_icon_image2 = "3d-cube.gif"
	this.rel_icon_rollover2 = "3d-cube.gif"
	this.rel_icon_image_wh2 = "13,13"

	this.rel_icon_image3 = "circle_0.gif"
	this.rel_icon_rollover3 = "circle_0.gif"
	this.rel_icon_image_wh3 = "16,14"

	this.rel_icon_image4 = "lock.gif"
	this.rel_icon_rollover4 = "lock.gif"
	this.rel_icon_image_wh4 = "7,8"



    //Absolute positioned icon images (x,y positioned)

	this.abs_icon_image0 = "arrows_6.gif"
	this.abs_icon_rollover0 = "arrows_6_hl.gif"
	this.abs_icon_image_wh0 = "10,8"
	this.abs_icon_image_xy0 = "-13,4"




/**********************************************************************************************

                              Global - Menu Container Settings

**********************************************************************************************/


	this.menu_background_color = "#dce0d6"
	this.menu_border_color = "#7d4535"
	this.menu_border_width = 1
	this.menu_padding = "4,5,4,5"
	this.menu_border_style = "solid"
	this.divider_caps = false
	this.divider_width = 1
	this.divider_height = 1
	this.divider_background_color = "#000000"
	this.divider_border_style = "none"
	this.divider_border_width = 0
	this.divider_border_color = "#000000"
	this.menu_is_horizontal = false
	this.menu_width = 100
	this.menu_xy = "-80,-2"
	this.menu_gradient = "progid:DXImageTransform.Microsoft.gradient(gradientType=0, startColorstr=#ffffff, endColorstr=#eee791)"
	this.menu_animation = "progid:DXImageTransform.Microsoft.RandomDissolve(duration=0.5)"
	this.menu_scroll_direction = 1
	this.menu_scroll_reverse_on_hide = true
	this.menu_scroll_delay = 0
	this.menu_scroll_step = 5




/**********************************************************************************************

                              Global - Menu Item Settings

**********************************************************************************************/


	this.icon_rel = 3
	this.menu_items_background_color_roll = "#e3ddac"
	this.menu_items_text_color = "#333333"
	this.menu_items_text_decoration = "none"
	this.menu_items_font_family = "Arial"
	this.menu_items_font_size = "11px"
	this.menu_items_font_style = "normal"
	this.menu_items_font_weight = "normal"
	this.menu_items_text_align = "left"
	this.menu_items_padding = "4,5,4,5"
	this.menu_items_border_style = "solid"
	this.menu_items_border_color = "#000000"
	this.menu_items_border_width = 0
	this.menu_items_width = 90
	this.menu_items_text_decoration_roll = "underline"
	this.menu_items_text_color_roll = "#db4448"




/**********************************************************************************************

                              Main Menu Settings

**********************************************************************************************/


        this.menu_items_background_color_roll_main = "#e3ddac"
        this.menu_items_text_color_roll_main = "#db4448"
        this.menu_items_width_main = 91
        this.menu_is_horizontal_main = true

        this.item0 = "Cadastrar"
        this.item1 = "Clientes"
        this.item2 = "Servi�os"
        this.item3 = "Faturamento"
        this.item4 = "Consultar"
        this.item5 = "Importa��o / Exporta��o"
        this.item6 = "Sair"

        this.item_width2 = 89
        this.icon_rel4 = 3
        this.icon_rel5 = 3
        this.item_width5 = 173
        this.icon_rel6 = 3
        this.item_width6 = 126

        this.url1 = "CadCadCli.php"
        this.url2 = "CadSer.php"
        this.url6 = "senha.php"




/**********************************************************************************************

                              Sub Menu Settings

**********************************************************************************************/


    //Sub Menu 0

        this.menu_xy0 = "-97,5"
        this.menu_width0 = 167

        this.item0_0 = "Divis�es"


            //Sub Menu 0_0

                this.menu_xy0_0 = "11,-26"
                this.menu_width0_0 = 150

                this.item0_0_0 = "Visualiza��o"
                this.item0_0_1 = "Rela��o de Clientes"

                this.url0_0_0 = "CadTabUni.php"
                this.url0_0_1 = "ManutTabUniRelCli.php"



        this.item0_1 = "Distritos"


            //Sub Menu 0_1

                this.menu_xy0_1 = "11,-20"
                this.menu_width0_1 = 145

                this.item0_1_0 = "Visualiza��o"
                this.item0_1_1 = "Rela��o de Clientes"

                this.url0_1_0 = "ManutTabDis.php"
                this.url0_1_1 = "ManutTabDisRelCli.php"



        this.item0_2 = "Atividades"


            //Sub Menu 0_2

                this.menu_xy0_2 = "11,-19"
                this.menu_width0_2 = 138

                this.item0_2_0 = "Visualiza��o"
                this.item0_2_1 = "Rela��o de Clientes"

                this.url0_2_0 = "CadGRPAti.php"
                this.url0_2_1 = "ManutGRPAtiRel.php"



        this.item0_3 = "Sub Atividades"


            //Sub Menu 0_3

                this.menu_xy0_3 = "11,-22"
                this.menu_width0_3 = 138

                this.item0_3_0 = "Visualiza��o"
                this.item0_3_1 = "Rela��o de Clientes"

                this.url0_3_0 = "CadSubAti.php"
                this.url0_3_1 = "ManutSubAtiRelCli.php"



        this.item0_4 = "Servi�os"


            //Sub Menu 0_4

                this.menu_xy0_4 = "10,-20"
                this.menu_width0_4 = 167

                this.item0_4_0 = "Visualiza��o"
                this.item0_4_1 = "Rela��o de Clientes"

                this.url0_4_0 = "CadGrpSer.php"
                this.url0_4_1 = "ManutGrpSerRelCli.php"



        this.item0_5 = "Sub Servi�os"


            //Sub Menu 0_5

                this.menu_xy0_5 = "11,-18"
                this.menu_width0_5 = 167

                this.item0_5_0 = "Visualiza��o"
                this.item0_5_1 = "Rela��o de Clientes"

                this.url0_5_0 = "CadSubServico.php"
                this.url0_5_1 = "ManutSubServicoRelCli.php"



        this.item0_6 = "UFIR's"


            //Sub Menu 0_6

                this.menu_xy0_6 = "10,-19"
                this.menu_width0_6 = 167

                this.item0_6_0 = "Visualiza��o"

                this.url0_6_0 = "CadTabPBH.php"



        this.item0_7 = "Tabela de Pre�os"


            //Sub Menu 0_7

                this.menu_xy0_7 = "10,-16"
                this.menu_width0_7 = 167

                this.item0_7_0 = "Visualiza��o"
                this.item0_7_1 = "Inclus�o Mensal"

                this.url0_7_0 = "CadTabPreco.php"
                this.url0_7_1 = "ManurEquSerIncMens.php"



        this.item0_8 = "Frequ�ncia de Coletas"


            //Sub Menu 0_8

                this.menu_xy0_8 = "11,-17"
                this.menu_width0_8 = 167

                this.item0_8_0 = "Visualiza��o"

                this.url0_8_0 = "CadTabCol.php"



        this.item0_9 = "Empresa"
        this.item0_10 = "Operadores do Sistema"
        this.item0_11 = "Par�metros"
        this.item0_12 = "Banco"

        this.url0_9 = "CadCadEmp.php"
        this.url0_10 = "CadTabOpe.php"
        this.url0_11 = "CadParam.php"
        this.url0_12 = "CadTabItau.php"



    //Sub Menu 1

        this.menu_width1 = 117




    //Sub Menu 2

        this.menu_width2 = 117




    //Sub Menu 3

        this.menu_xy3 = "-89,6"
        this.menu_width3 = 117

        this.item3_0 = "Coletivo"
        this.item3_1 = "Individual"
        this.item3_2 = "Pagamento"
        this.item3_3 = "Extrato"
        this.item3_4 = "Relat�rios"


            //Sub Menu 3_4

                this.menu_xy3_4 = "7,-75"
                this.menu_width3_4 = 202

                this.item3_4_0 = "Por Emiss�o"


                    //Sub Menu 3_4_0

                        this.menu_width3_4_0 = 202

                        this.item3_4_0_0 = "Por Inscri��o"
                        this.item3_4_0_1 = "Por Divis�o"
                        this.item3_4_0_2 = "Por Distrito"
                        this.item3_4_0_3 = "Por Atividade"
                        this.item3_4_0_4 = "Por Sub Atividade"
                        this.item3_4_0_5 = "Por Servi�o"
                        this.item3_4_0_6 = "Por Sub Servi�o"

                        this.url3_4_0_0 = "RelEmisInsc.php?opcao=411"
                        this.url3_4_0_1 = "RelEmisDivi.php?opcao=412"
                        this.url3_4_0_2 = "RelEmisDistr.php?opcao=413"
                        this.url3_4_0_3 = "RelEmisAtiv.php?opcao=414"
                        this.url3_4_0_4 = "RelEmisSubA.php?opcao=415"
                        this.url3_4_0_5 = "RelEmisServ.php?opcao=416"
                        this.url3_4_0_6 = "RelEmisSubS.php?opcao=417"



                this.item3_4_1 = "Por Receita"


                    //Sub Menu 3_4_1

                        this.menu_width3_4_1 = 202

                        this.item3_4_1_0 = "Por Inscri��o"
                        this.item3_4_1_1 = "Por Divis�o"
                        this.item3_4_1_2 = "Por Distrito"
                        this.item3_4_1_3 = "Por Atividade"
                        this.item3_4_1_4 = "Por Sub Atividade"
                        this.item3_4_1_5 = "Por Servi�o"
                        this.item3_4_1_6 = "Por Sub Servi�o"

                        this.url3_4_1_0 = "RelReceInsc.php?opcao=421"
                        this.url3_4_1_1 = "RelReceDivi.php?opcao=422"
                        this.url3_4_1_2 = "RelReceDistr.php?opcao=423"
                        this.url3_4_1_3 = "RelReceAtiv.php?opcao=424"
                        this.url3_4_1_4 = "RelReceSubA.php?opcao=425"
                        this.url3_4_1_5 = "RelReceServ.php?opcao=426"
                        this.url3_4_1_6 = "RelReceSubS.php?opcao=427"



                this.item3_4_2 = "Por Emiss�o/Arrecada��o"


                    //Sub Menu 3_4_2

                        this.menu_xy3_4_2 = "-81,-3"
                        this.menu_width3_4_2 = 202

                        this.item3_4_2_0 = "Por Inscri��o"
                        this.item3_4_2_1 = "Por Divis�o"
                        this.item3_4_2_2 = "Por Distrito"
                        this.item3_4_2_3 = "Por Atividade"
                        this.item3_4_2_4 = "Por Sub Atividade"
                        this.item3_4_2_5 = "Por Servi�o"
                        this.item3_4_2_6 = "Por Sub Servi�o"

                        this.url3_4_2_0 = "RelArrecInsc.php"
                        this.url3_4_2_1 = "RelArrecDivi.php"
                        this.url3_4_2_2 = "RelArrDist.php"
                        this.url3_4_2_3 = "RelArrecAtiv.php"
                        this.url3_4_2_4 = "RelArrecSubAti.php"
                        this.url3_4_2_5 = "RelArrecServ.php"
                        this.url3_4_2_6 = "RelArrecSubSer.php"



                this.item3_4_3 = "Por D�bito"


                    //Sub Menu 3_4_3

                        this.menu_xy3_4_3 = "-17,-108"
                        this.menu_width3_4_3 = 202

                        this.item3_4_3_0 = "Por Inscri��o"
                        this.item3_4_3_1 = "Por Divis�o"
                        this.item3_4_3_2 = "Por Distrito"
                        this.item3_4_3_3 = "Por Atividade"
                        this.item3_4_3_4 = "Por Sub Atividade"
                        this.item3_4_3_5 = "Por Servi�o"
                        this.item3_4_3_6 = "Por Sub Servi�o"
                        this.item3_4_3_7 = "Por Cliente"


                            //Sub Menu 3_4_3_7

                                this.menu_xy3_4_3_7 = "-573,-94"
                                this.menu_width3_4_3_7 = 359

                                this.item3_4_3_7_0 = "Totaliza��o de D�bitos de Todos os Clientes (Analitico)"
                                this.item3_4_3_7_1 = "Totaliza��o de D�bitos de Todos os Clientes (Sint�tico)"
                                this.item3_4_3_7_2 = "Totaliza��o de D�bitos Para um Determinado Cliente"
                                this.item3_4_3_7_3 = "Totaliza��o de D�bitos Para um Determinado Cliente no Per�odo"
                                this.item3_4_3_7_4 = "Totaliza��o de D�bitos de todos os Clientes no Per�odo (Sint�tico)"

                                this.url3_4_3_7_0 = "RelDebiGera.php"
                                this.url3_4_3_7_1 = "RelDebiGeraCliente.php"
                                this.url3_4_3_7_2 = "RelDebiParcCliente.php"
                                this.url3_4_3_7_3 = "RelDebiParcClientePeri.php"
                                this.url3_4_3_7_4 = "RelDebiGeraClientePeri.php"




                        this.url3_4_3_0 = "RelDebiEmis.php"
                        this.url3_4_3_1 = "RelDebiDivi.php"
                        this.url3_4_3_2 = "RelDebiDist.php"
                        this.url3_4_3_3 = "RelDebiAtiv.php"
                        this.url3_4_3_4 = "RelDebiSubAti.php"
                        this.url3_4_3_5 = "RelDebiServ.php"
                        this.url3_4_3_6 = "RelDebiSubSer.php"







        this.url3_0 = "FatCol.php"
        this.url3_1 = "FatInd.php"
        this.url3_3 = "Extrato.php"



    //Sub Menu 4

        this.menu_width4 = 117

        this.item4_0 = "Visualizar"

        this.url4_0 = "visualiza.php"



    //Sub Menu 5

        this.menu_xy5 = "-80,1"
        this.menu_width5 = 218

        this.item5_0 = "Retorno de Faturas(Faturas Pagas)"
        this.item5_1 = "Remessa (Faturas a Pagar)"

        this.url5_0 = "RetornoFatPagas.php"
        this.url5_1 = "GeracaoBB.php"



    //Sub Menu 6




}///////////////////////// END Menu Data /////////////////////////////////////////



//Document Level Settings

cdd__activate_onclick = false
cdd__showhide_delay = 250
cdd__url_target = "_self"
cdd__url_features = "resizable=1, scrollbars=1, titlebar=1, menubar=1, toolbar=1, location=1, status=1, directories=1, channelmode=0, fullscreen=0"
cdd__display_urls_in_status_bar = true
cdd__default_cursor = "hand"


//NavStudio Code (Warning: Do Not Alter!)

if (window.showHelp){b_type = "ie"; if (!window.attachEvent) b_type += "mac";}if (document.createElementNS) b_type = "dom";if (navigator.userAgent.indexOf("afari")>-1) b_type = "safari";if (window.opera) b_type = "opera"; qmap1 = "\<\script language=\"JavaScript\" vqptag='loader_sub' src=\""; qmap2 = ".js\">\<\/script\>";;function iesf(){};;function vqp_error(val){alert(val)}
if (b_type){document.write(qmap1+cdd__codebase+"pbrowser_"+b_type+qmap2);document.close();}

