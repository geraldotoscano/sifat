<?php
//BindEvents Method @1-E8636CE1
function BindEvents()
{
    global $NRExpurgo;
    $NRExpurgo->Button_Insert->CCSEvents["OnClick"] = "NRExpurgo_Button_Insert_OnClick";
}
//End BindEvents Method

//NRExpurgo_Button_Insert_OnClick @5-EB4E72FE
function NRExpurgo_Button_Insert_OnClick(& $sender)
{
    $NRExpurgo_Button_Insert_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRExpurgo; //Compatibility
//End NRExpurgo_Button_Insert_OnClick

//Custom Code @8-2A29BDB7
// -------------------------
   		$Tab_CadFat_Exp = new clsDBfaturar();
   		$Tab_CadFat_Exp->query("SELECT 
	    	                  		count(*) as Tot_Fat_Exp
						   		 FROM 
						      		cadfat
					   			 WHERE 
					      			valfat = valpgt and
									to_number(to_char(sysdate,'yyyy')) - to_number(to_char(datemi,'yyyy')) > 5
								 ORDER By
								    to_number(substr(mesref,4,1)),
									to_number(substr(mesref,1,2))"
								);
   		$Tab_CadFat_Exp->next_record();
		$mTotFatExp = 449 * $Tab_CadFat_Exp->f("Tot_Fat_Exp");

        // Calculo do total do movimento de faturas a serem expurgadas
   		$Tab_MovFat_Exp = new clsDBfaturar();
   		$Tab_MovFat_Exp->query("SELECT 
	    	                  		count(*) as Tot_Fat_Exp
						   		 FROM 
						      		cadfat c,
									movfat m
					   			 WHERE 
					      			c.valfat = c.valpgt and
									to_number(to_char(sysdate,'yyyy')) - to_number(to_char(c.datemi,'yyyy')) > 5 and
									c.codfat = m.codfat
								 ORDER By
								    to_number(substr(c.mesref,4,1)),
									to_number(substr(c.mesref,1,2))"
								);
   		$Tab_MovFat_Exp->next_record();
		$mTotFatExp += $Tab_CadFat_Exp->f("Tot_Fat_Exp")*106;

   		$Tab_CadFat_Exp->query("SELECT 
	    	                  		CODFAT,
                                    CODCLI,
                                    MESREF,
                                    to_char(DATEMI,'dd/mm/yyyy hh24:mi:ss') as DATEMI,
                                    to_char(DATVNC,'dd/mm/yyyy hh24:mi:ss') as DATVNC,
                                    VALFAT,
                                    VALMOV,
                                    RET_INSS,
                                    to_char(DATPGT,'dd/mm/yyyy hh24:mi:ss') as DATPGT,
                                    VALPGT,
                                    VALCOB,
                                    VALJUR,
                                    VALMUL,
                                    EXPORT,
                                    PGTOELET,
                                    CONSIST,
                                    CONSIST_M,
                                    ISSQN,
                                    LOGSER,
                                    BAISER,
                                    CIDSER,
                                    ESTSER
						   		 FROM 
						      		cadfat
					   			 WHERE 
					      			valfat = valpgt and
									to_number(to_char(sysdate,'yyyy')) - to_number(to_char(datemi,'yyyy')) > 5
								 ORDER By
								    to_number(substr(mesref,4,1)),
									to_number(substr(mesref,1,2))
								"
								);
        $mArquivo = "Expurgo".substr(CCGetSession("DataSist"),-4).".txt";
	    $tipo="application/force-download";
        header("Pragma: public"); // required
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false); // required for certain browsers
        header("Content-Type: $tipo");
        header("Content-Disposition: attachment; filename=".basename($mArquivo)); // informa ao navegador que � tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
        header("Content-Transfer-Encoding: binary");
        ob_clean();
        flush(); 
		print "CADFAT - CADASTRO DE FATURAS\r\n";
		while ($Tab_CadFat_Exp->next_record())
		{
		   $mLinha = str_pad($Tab_CadFat_Exp->f("CODFAT"),06," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("CODCLI"),06," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("MESREF"),07," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("DATEMI"),21," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("DATVNC"),21," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("VALFAT"),12," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("VALMOV"),12," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("RET_INSS"),12," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("DATPGT"),21," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("VALPGT"),12," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("VALCOB"),12," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("VALJUR"),12," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("VALMUL"),12," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("EXPORT"),01," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("PGTOELET"),01," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("CONSIST"),01," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("CONSIST_M"),01," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("ISSQN"),07," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("LOGSER"),35," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("BAISER"),20," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("CIDSER"),20," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("ESTSER"),02," ", STR_PAD_LEFT).";";
					 print $mLinha."\r\n";
		}
   		$Tab_MovFat_Exp->query("SELECT 
                                   CODFAT,
                                   CODCLI,
                                   GRPSER,
                                   SUBSER,
                                   MESREF,
                                   to_char(DATPGT,'dd/mm/yyyy hh24:mi:ss') as DATPGT,
                                   VALPGT,
                                   QTDMED,
                                   EQUPBH,
                                   VALPBH,
                                   VALSER,
                                   CONSIST
						   		 FROM 
						      		cadfat c,
									movfat m
					   			 WHERE 
					      			c.valfat = c.valpgt and
									to_number(to_char(sysdate,'yyyy')) - to_number(to_char(c.datemi,'yyyy')) > 5 and
									c.codfat = m.codfat
								 ORDER By
								    to_number(substr(c.mesref,4,1)),
									to_number(substr(c.mesref,1,2))"
								);
		print "MOVFAT - MOVIMENTO DE FATURAS\r\n";
        while ($Tab_MovFat_Exp->next_record())
		{
		   $mLinha = str_pad($Tab_CadFat_Exp->f("CODFAT"),06," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("CODCLI"),06," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("GRPSER"),02," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("SUBSER"),02," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("MESREF"),07," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("DATPGT"),21," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("VALPGT"),12," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("QTDMED"),08," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("EQUPBH"),08," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("VALPBH"),12," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("VALSER"),12," ", STR_PAD_LEFT).";".
		             str_pad($Tab_CadFat_Exp->f("CONSIST"),01," ", STR_PAD_LEFT).";";
					 print $mLinha."\r\n";
		}
/*
   		$Tab_CadFat_Exp->query("DELETE FROM 
						      		cadfat
					   			 WHERE 
					      			valfat = valpgt and
									to_number(to_char(sysdate,'yyyy')) - to_number(to_char(datemi,'yyyy')) > 5
								 ORDER By
								    to_number(substr(mesref,4,1)),
									to_number(substr(mesref,1,2))
								"
								);
   		$Tab_MovFat_Exp->query("DELETE FROM 
						      		cadfat c,
									movfat m
					   			 WHERE 
					      			c.valfat = c.valpgt and
									to_number(to_char(sysdate,'yyyy')) - to_number(to_char(c.datemi,'yyyy')) > 5 and
									c.codfat = m.codfat
								 ORDER By
								    to_number(substr(c.mesref,4,1)),
									to_number(substr(c.mesref,1,2))
								"
								);
*/
		exit;

		// Termino Expurgo
// -------------------------
//End Custom Code

//Close NRExpurgo_Button_Insert_OnClick @5-DE69EF6E
    return $NRExpurgo_Button_Insert_OnClick;
}
//End Close NRExpurgo_Button_Insert_OnClick

//DEL  // -------------------------
//DEL          // Calculo do total de faturas a serem expurgadas
//DEL     		$Tab_CadFat_Exp = new clsDBfaturar();
//DEL     		$Tab_CadFat_Exp->query("SELECT 
//DEL  	    	                  		count(*) as Tot_Fat_Exp
//DEL  						   		 FROM 
//DEL  						      		cadfat
//DEL  					   			 WHERE 
//DEL  					      			valfat = valpgt and
//DEL  									to_number(to_char(sysdate,'yyyy')) - to_number(to_char(datemi,'yyyy')) > 5
//DEL  								 ORDER By
//DEL  								    to_number(substr(mesref,4,1)),
//DEL  									to_number(substr(mesref,1,2))"
//DEL  								);
//DEL     		$Tab_CadFat_Exp->next_record();
//DEL  		$mTotFatExp = 449 * $Tab_CadFat_Exp->f("Tot_Fat_Exp");
//DEL  
//DEL          // Calculo do total do movimento de faturas a serem expurgadas
//DEL     		$Tab_MovFat_Exp = new clsDBfaturar();
//DEL     		$Tab_MovFat_Exp->query("SELECT 
//DEL  	    	                  		count(*) as Tot_Fat_Exp
//DEL  						   		 FROM 
//DEL  						      		cadfat c,
//DEL  									movfat m
//DEL  					   			 WHERE 
//DEL  					      			c.valfat = c.valpgt and
//DEL  									to_number(to_char(sysdate,'yyyy')) - to_number(to_char(c.datemi,'yyyy')) > 5 and
//DEL  									c.codfat = m.codfat
//DEL  								 ORDER By
//DEL  								    to_number(substr(c.mesref,4,1)),
//DEL  									to_number(substr(c.mesref,1,2))"
//DEL  								);
//DEL     		$Tab_MovFat_Exp->next_record();
//DEL  		$mTotFatExp += $Tab_CadFat_Exp->f("Tot_Fat_Exp")*106;
//DEL  
//DEL     		$Tab_CadFat_Exp->query("SELECT 
//DEL  	    	                  		CODFAT,
//DEL                                      CODCLI,
//DEL                                      MESREF,
//DEL                                      to_char(DATEMI,'dd/mm/yyyy hh24:mi:ss') as DATEMI,
//DEL                                      to_char(DATVNC,'dd/mm/yyyy hh24:mi:ss') as DATVNC,
//DEL                                      VALFAT,
//DEL                                      VALMOV,
//DEL                                      RET_INSS,
//DEL                                      to_char(DATPGT,'dd/mm/yyyy hh24:mi:ss') as DATPGT,
//DEL                                      VALPGT,
//DEL                                      VALCOB,
//DEL                                      VALJUR,
//DEL                                      VALMUL,
//DEL                                      EXPORT,
//DEL                                      PGTOELET,
//DEL                                      CONSIST,
//DEL                                      CONSIST_M,
//DEL                                      ISSQN,
//DEL                                      LOGSER,
//DEL                                      BAISER,
//DEL                                      CIDSER,
//DEL                                      ESTSER
//DEL  						   		 FROM 
//DEL  						      		cadfat
//DEL  					   			 WHERE 
//DEL  					      			valfat = valpgt and
//DEL  									to_number(to_char(sysdate,'yyyy')) - to_number(to_char(datemi,'yyyy')) > 5
//DEL  								 ORDER By
//DEL  								    to_number(substr(mesref,4,1)),
//DEL  									to_number(substr(mesref,1,2))
//DEL  								"
//DEL  								);
//DEL          $mArquivo = "Expurgo".substr(CCGetSession("DataSist"),-4).".txt";
//DEL  	    $tipo="application/force-download";
//DEL          header("Pragma: public"); // required
//DEL          header("Expires: 0");
//DEL          header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
//DEL          header("Cache-Control: private",false); // required for certain browsers
//DEL          header("Content-Type: $tipo");
//DEL          header("Content-Disposition: attachment; filename=".basename($mArquivo)); // informa ao navegador que � tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
//DEL          header("Content-Transfer-Encoding: binary");
//DEL          ob_clean();
//DEL          flush(); 
//DEL  		print "CADFAT - CADASTRO DE FATURAS\r\n";
//DEL  		while ($Tab_CadFat_Exp->next_record())
//DEL  		{
//DEL  		   $mLinha = str_pad($Tab_CadFat_Exp->f("CODFAT"),06," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("CODCLI"),06," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("MESREF"),07," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("DATEMI"),21," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("DATVNC"),21," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("VALFAT"),12," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("VALMOV"),12," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("RET_INSS"),12," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("DATPGT"),21," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("VALPGT"),12," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("VALCOB"),12," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("VALJUR"),12," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("VALMUL"),12," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("EXPORT"),01," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("PGTOELET"),01," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("CONSIST"),01," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("CONSIST_M"),01," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("ISSQN"),07," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("LOGSER"),35," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("BAISER"),20," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("CIDSER"),20," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("ESTSER"),02," ", STR_PAD_LEFT).";";
//DEL  					 print $mLinha."\r\n";
//DEL  		}
//DEL     		$Tab_MovFat_Exp->query("SELECT 
//DEL                                     CODFAT,
//DEL                                     CODCLI,
//DEL                                     GRPSER,
//DEL                                     SUBSER,
//DEL                                     MESREF,
//DEL                                     to_char(DATPGT,'dd/mm/yyyy hh24:mi:ss') as DATPGT,
//DEL                                     VALPGT,
//DEL                                     QTDMED,
//DEL                                     EQUPBH,
//DEL                                     VALPBH,
//DEL                                     VALSER,
//DEL                                     CONSIST
//DEL  						   		 FROM 
//DEL  						      		cadfat c,
//DEL  									movfat m
//DEL  					   			 WHERE 
//DEL  					      			c.valfat = c.valpgt and
//DEL  									to_number(to_char(sysdate,'yyyy')) - to_number(to_char(c.datemi,'yyyy')) > 5 and
//DEL  									c.codfat = m.codfat
//DEL  								 ORDER By
//DEL  								    to_number(substr(c.mesref,4,1)),
//DEL  									to_number(substr(c.mesref,1,2))"
//DEL  								);
//DEL  		print "MOVFAT - MOVIMENTO DE FATURAS\r\n";
//DEL          while ($Tab_MovFat_Exp->next_record())
//DEL  		{
//DEL  		   $mLinha = str_pad($Tab_CadFat_Exp->f("CODFAT"),06," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("CODCLI"),06," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("GRPSER"),02," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("SUBSER"),02," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("MESREF"),07," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("DATPGT"),21," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("VALPGT"),12," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("QTDMED"),08," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("EQUPBH"),08," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("VALPBH"),12," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("VALSER"),12," ", STR_PAD_LEFT).";".
//DEL  		             str_pad($Tab_CadFat_Exp->f("CONSIST"),01," ", STR_PAD_LEFT).";";
//DEL  					 print $mLinha."\r\n";
//DEL  		}
//DEL  /*
//DEL     		$Tab_CadFat_Exp->query("DELETE FROM 
//DEL  						      		cadfat
//DEL  					   			 WHERE 
//DEL  					      			valfat = valpgt and
//DEL  									to_number(to_char(sysdate,'yyyy')) - to_number(to_char(datemi,'yyyy')) > 5
//DEL  								 ORDER By
//DEL  								    to_number(substr(mesref,4,1)),
//DEL  									to_number(substr(mesref,1,2))
//DEL  								"
//DEL  								);
//DEL     		$Tab_MovFat_Exp->query("DELETE FROM 
//DEL  						      		cadfat c,
//DEL  									movfat m
//DEL  					   			 WHERE 
//DEL  					      			c.valfat = c.valpgt and
//DEL  									to_number(to_char(sysdate,'yyyy')) - to_number(to_char(c.datemi,'yyyy')) > 5 and
//DEL  									c.codfat = m.codfat
//DEL  								 ORDER By
//DEL  								    to_number(substr(c.mesref,4,1)),
//DEL  									to_number(substr(c.mesref,1,2))
//DEL  								"
//DEL  								);
//DEL  */
//DEL  		exit;
//DEL  
//DEL  		// Termino Expurgo
//DEL  // -------------------------



?>
