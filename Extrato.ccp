<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="11" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="cadcli_CADFAT" wizardCaption="{res:CCS_SearchFormPrefix} {res:cadcli_CADFAT} {res:CCS_SearchFormSuffix}" wizardOrientation="Vertical" wizardFormMethod="post" returnPage="Extrato.ccp" pasteAsReplace="pasteAsReplace">
			<Components>
				<TextBox id="13" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_CODCLI" wizardCaption="{res:CODCLI}" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" required="False" caption="Código do Cliente">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<ListBox id="105" visible="Yes" fieldSourceType="DBColumn" sourceType="SQL" dataType="Text" returnValueType="Number" name="s_DESCLI" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="select 0 nord,to_char(null) as codcli,  to_char(null) as descli from dual

UNION


SELECT
   1 nord,CODCLI,
   DESCLI
FROM
   CADCLI

ORDER BY nord,DESCLI
" boundColumn="CODCLI" textColumn="DESCLI">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<TextBox id="14" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_MESREF" wizardCaption="{res:MESREF}" wizardSize="7" wizardMaxLength="7" wizardIsPassword="False" required="False" sourceType="ListOfValues">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Validate Entry" actionCategory="Validation" id="65" inputMask="00/0000"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
				</TextBox>
				<RadioButton id="87" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" html="True" returnValueType="Number" name="s_PGTO" connection="Faturar" _valueOfList="T" _nameOfList="Todas" dataSource="N;Não pagas;P;Pagas" defaultValue="'N'">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</RadioButton>
				<Button id="12" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="{res:CCS_Search}">
					<Components/>
					<Events>
						<Event name="OnClick" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="64"/>
							</Actions>
						</Event>
						<Event name="OnClick" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="107"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Hide-Show Component" actionCategory="General" id="18" action="Hide" conditionType="Parameter" dataType="Text" condition="Equal" parameter1="Print" name1="ViewMode" sourceType1="URL" name2="&quot;Print&quot;" sourceType2="Expression"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Report id="4" secured="True" enablePrint="True" showMode="Web" sourceType="Table" returnValueType="Number" linesPerWebPage="40" connection="Faturar" dataSource="CADFAT, CADCLI" name="CADFAT_CADCLI" pageSizeLimit="100" wizardCaption="{res:CCS_ReportFormPrefix} {res:CADFATCADCLI} {res:CCS_ReportFormSuffix}" wizardLayoutType="Tabular" activeCollection="TableParameters" linesPerPhysicalPage="66">
			<Components>
				<Section id="19" visible="True" lines="1" name="Report_Header" wizardSectionType="ReportHeader">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="20" visible="True" lines="1" name="Page_Header" wizardSectionType="PageHeader" wizardAllowSorting="True">
					<Components>
						<Sorter id="58" visible="True" name="Sorter_DESCLI" wizardSortingType="SimpleDir" wizardCaption="Cliente" column="DESCLI">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="60" visible="True" name="Sorter_CGCCPF" wizardSortingType="SimpleDir" wizardCaption="CGC/CPF" column="CGCCPF">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="33" visible="True" name="Sorter_CODFAT" column="CODFAT" wizardCaption="{res:CODFAT}" wizardSortingType="SimpleDir" wizardControl="CODFAT">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="35" visible="True" name="Sorter_MESREF" column="MESREF" wizardCaption="{res:MESREF}" wizardSortingType="SimpleDir" wizardControl="MESREF">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="37" visible="True" name="Sorter_DATVNC" column="DATVNC" wizardCaption="{res:DATVNC}" wizardSortingType="SimpleDir" wizardControl="DATVNC">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="39" visible="True" name="Sorter_VALFAT" column="VALFAT" wizardCaption="{res:VALFAT}" wizardSortingType="SimpleDir" wizardControl="VALFAT">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="53" visible="True" name="Sorter2" wizardSortingType="SimpleDir" wizardCaption="Valor Pago" column="VALPGT">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="101" visible="True" name="Sorter1" wizardSortingType="SimpleDir" wizardCaption="Multa por Atrazo" column="VALMUL">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="41" visible="True" name="Sorter_VALJUR" column="VALJUR" wizardCaption="{res:VALJUR}" wizardSortingType="SimpleDir" wizardControl="VALJUR">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="43" visible="True" name="Sorter_ISSQN" column="ISSQN" wizardCaption="{res:ISSQN}" wizardSortingType="SimpleDir" wizardControl="ISSQN">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="45" visible="True" name="Sorter_RET_INSS" column="RET_INSS" wizardCaption="{res:RET_INSS}" wizardSortingType="SimpleDir" wizardControl="RET_INSS">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="24" visible="True" lines="1" name="Page_Footer" wizardSectionType="PageFooter" pageBreakAfter="True" pasteAsReplace="pasteAsReplace">
					<Components>
						<ReportLabel id="25" fieldSourceType="SpecialValue" dataType="Date" html="False" hideDuplicates="False" resetAt="Report" name="Report_CurrentDateTime" fieldSource="CurrentDateTime" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardInsertToDateTD="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Navigator id="26" size="10" name="Navigator" wizardPagingType="Custom" wizardFirst="True" wizardFirstText="{res:CCS_First}" wizardPrev="True" wizardPrevText="{res:CCS_Previous}" wizardNext="True" wizardNextText="{res:CCS_Next}" wizardLast="True" wizardLastText="{res:CCS_Last}" wizardImages="Images" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="True" wizardOfText="{res:CCS_Of}" wizardImagesScheme="Apricot" type="Simple">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Navigator>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="21" visible="True" lines="1" name="Detail" pasteAsReplace="pasteAsReplace">
					<Components>
						<ReportLabel id="59" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="DESCLI" fieldSource="DESCLI">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="69" eventType="Server"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="61" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="CGCCPF" fieldSource="CGCCPF">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="76" eventType="Server"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Link id="34" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="CODFAT" fieldSource="CODFAT" wizardCaption="CODFAT" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="None" hrefSource="PagtoFatu_Dados.ccp">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
							<LinkParameters>
								<LinkParameter id="109" sourceType="Expression" name="opcao" source="&quot;'1'&quot;"/><LinkParameter id="110" sourceType="DataField" name="CODFAT" source="CODFAT"/>
							</LinkParameters>
						</Link>
						<ReportLabel id="36" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="MESREF" fieldSource="MESREF" wizardCaption="MESREF" wizardSize="7" wizardMaxLength="7" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="52" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="SimNao">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="55" eventType="Server"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="38" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="DATVNC" fieldSource="DATVNC" wizardCaption="DATVNC" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="90"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="40" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="VALFAT" fieldSource="VALFAT" wizardCaption="VALFAT" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" format="#,##0.00;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="54" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="VALPGT" fieldSource="VALPGT" format="#,##0.00;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="103" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="VALMUL2" wizardCaption="VALJUR" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" fieldSource="VALMUL" format="#,##0.00;;;;,;.;">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="104" eventType="Server" id_oncopy="100"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="99" fieldSourceType="DBColumn" dataType="Integer" html="False" hideDuplicates="False" resetAt="Report" name="DIAS_ATRAZO" wizardCaption="VALJUR" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" fieldSource="NDIAS">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="42" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="VALJUR" wizardCaption="VALJUR" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" fieldSource="JUROS" format="#,##0.00;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Hidden id="89" fieldSourceType="DBColumn" dataType="Float" name="VALMUL" fieldSource="VALMUL" html="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Hidden>
						<ReportLabel id="44" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="ISSQN" fieldSource="ISSQN" wizardCaption="ISSQN" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" format="#,##0.00;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Hidden id="91" fieldSourceType="DBColumn" dataType="Float" name="hdd_RET_INSS" fieldSource="RET_INSS">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Hidden>
						<ReportLabel id="46" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="RET_INSS" fieldSource="RET_INSS" wizardCaption="RET_INSS" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" format="#,##0.00;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="93" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="DEBITO_ATUAL" format="#,##0.00;;;;,;.;">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="94"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="22" visible="True" lines="1" name="Report_Footer" wizardSectionType="ReportFooter">
					<Components>
						<Panel id="23" visible="True" name="NoRecords" wizardNoRecords="{res:CCS_NoRecords}">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Panel>
						<ReportLabel id="27" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_VALFAT" fieldSource="VALFAT" summarised="True" function="Sum" wizardCaption="{res:VALFAT}" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" format="R$ #,##0.00;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="57" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_VALPGT" fieldSource="VALPGT" function="Sum" format="R$ #,##0.00;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="28" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_VALJUR" fieldSource="JUROS" summarised="True" function="Sum" wizardCaption="{res:VALJUR}" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" format="R$ #,##0.00;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="29" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_ISSQN" fieldSource="ISSQN" summarised="True" function="Sum" wizardCaption="{res:ISSQN}" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" format="R$ #,##0.00;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="30" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_RET_INSS" fieldSource="RET_INSS" summarised="True" function="Sum" wizardCaption="{res:RET_INSS}" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" format="R$ #,##0.00;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="97" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="Total_DEBITO_ATUAL" format="R$ #,##0.00;;;;,;.;" function="Sum">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="98"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
			</Components>
			<Events>
				<Event name="BeforeBuildSelect" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="50" eventType="Server"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters>
				<TableParameter id="31" conditionType="Parameter" useIsNull="False" field="CADFAT.CODCLI" dataType="Text" logicOperator="Or" searchConditionType="Contains" parameterType="URL" orderNumber="1" parameterSource="s_CODCLI"/>
				<TableParameter id="32" conditionType="Parameter" useIsNull="False" field="MESREF" dataType="Text" logicOperator="Or" searchConditionType="Contains" parameterType="URL" orderNumber="2" parameterSource="s_MESREF"/>
				<TableParameter id="106" conditionType="Parameter" useIsNull="False" field="CADCLI.CODCLI" dataType="Text" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="s_DESCLI"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="5" tableName="CADFAT" posLeft="10" posTop="10" posWidth="115" posHeight="440"/>
				<JoinTable id="6" tableName="CADCLI" schemaName="SL005V3" posLeft="146" posTop="10" posWidth="262" posHeight="483"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="7" tableLeft="CADCLI" tableRight="CADFAT" fieldLeft="CADCLI.CODCLI" fieldRight="CADFAT.CODCLI" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="9" tableName="CADCLI" fieldName="DESCLI"/>
				<Field id="10" tableName="CADCLI" fieldName="CGCCPF"/>
				<Field id="112" tableName="CADFAT" fieldName="CADFAT.*"/>
				<Field id="113" fieldName="CASE WHEN VALPGT=0 THEN FLOOR(SYSDATE-DATVNC) ELSE 0 END" isExpression="True" alias="NDIAS"/>
				<Field id="114" fieldName="CASE WHEN VALPGT=0 THEN ((VALFAT-RET_INSS) * (0.01/30) * FLOOR(SYSDATE-DATVNC)) ELSE 0 END" isExpression="True" alias="JUROS"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<ReportGroups>
			</ReportGroups>
			<SecurityGroups>
				<Group id="47" groupID="1" read="True"/>
				<Group id="48" groupID="2" read="True"/>
			</SecurityGroups>
			<Attributes>
				<Attribute id="49" name="um" sourceType="Expression" source="dois"/>
			</Attributes>
			<Features/>
		</Report>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="PHPTemplates" name="Extrato_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Code" language="PHPTemplates" name="Extrato.php" forShow="True" url="Extrato.php" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="73" groupID="1"/>
		<Group id="74" groupID="2"/>
		<Group id="75" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="108"/>
			</Actions>
		</Event>
	</Events>
</Page>
