<?php
//Include Common Files @1-514A03B0
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "Extrato.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordcadcli_CADFAT { //cadcli_CADFAT Class @11-0A14EA5E

//Variables @11-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @11-D1080B18
    function clsRecordcadcli_CADFAT($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record cadcli_CADFAT/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "cadcli_CADFAT";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_CODCLI = new clsControl(ccsTextBox, "s_CODCLI", "C�digo do Cliente", ccsText, "", CCGetRequestParam("s_CODCLI", $Method, NULL), $this);
            $this->s_DESCLI = new clsControl(ccsListBox, "s_DESCLI", "s_DESCLI", ccsText, "", CCGetRequestParam("s_DESCLI", $Method, NULL), $this);
            $this->s_DESCLI->DSType = dsSQL;
            $this->s_DESCLI->DataSource = new clsDBFaturar();
            $this->s_DESCLI->ds = & $this->s_DESCLI->DataSource;
            list($this->s_DESCLI->BoundColumn, $this->s_DESCLI->TextColumn, $this->s_DESCLI->DBFormat) = array("CODCLI", "DESCLI", "");
            $this->s_DESCLI->DataSource->SQL = "select 0 nord,to_char(null) as codcli,  to_char(null) as descli from dual\n" .
            "\n" .
            "UNION\n" .
            "\n" .
            "\n" .
            "SELECT\n" .
            "   1 nord,CODCLI,\n" .
            "   DESCLI\n" .
            "FROM\n" .
            "   CADCLI {SQL_OrderBy}";
            $this->s_DESCLI->DataSource->Order = "nord,DESCLI";
            $this->s_MESREF = new clsControl(ccsTextBox, "s_MESREF", "s_MESREF", ccsText, "", CCGetRequestParam("s_MESREF", $Method, NULL), $this);
            $this->s_PGTO = new clsControl(ccsRadioButton, "s_PGTO", "s_PGTO", ccsText, "", CCGetRequestParam("s_PGTO", $Method, NULL), $this);
            $this->s_PGTO->DSType = dsListOfValues;
            $this->s_PGTO->Values = array(array("N", "N�o pagas"), array("P", "Pagas"));
            $this->s_PGTO->HTML = true;
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
            if(!$this->FormSubmitted) {
                if(!is_array($this->s_PGTO->Value) && !strlen($this->s_PGTO->Value) && $this->s_PGTO->Value !== false)
                    $this->s_PGTO->SetText('N');
            }
        }
    }
//End Class_Initialize Event

//Validate Method @11-87C0E1D1
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_CODCLI->Validate() && $Validation);
        $Validation = ($this->s_DESCLI->Validate() && $Validation);
        $Validation = ($this->s_MESREF->Validate() && $Validation);
        $Validation = ($this->s_PGTO->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->s_CODCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_DESCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_MESREF->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_PGTO->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @11-D37D5173
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_CODCLI->Errors->Count());
        $errors = ($errors || $this->s_DESCLI->Errors->Count());
        $errors = ($errors || $this->s_MESREF->Errors->Count());
        $errors = ($errors || $this->s_PGTO->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @11-1BD8EC74
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "Extrato.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                $Redirect = "Extrato.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @11-3B300AAF
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->s_DESCLI->Prepare();
        $this->s_PGTO->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->s_CODCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_DESCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_MESREF->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_PGTO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_CODCLI->Show();
        $this->s_DESCLI->Show();
        $this->s_MESREF->Show();
        $this->s_PGTO->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End cadcli_CADFAT Class @11-FCB6E20C

//CADFAT_CADCLI ReportGroup class @4-AFB07204
class clsReportGroupCADFAT_CADCLI {
    public $GroupType;
    public $mode; //1 - open, 2 - close
    public $Report_CurrentDateTime, $_Report_CurrentDateTimeAttributes;
    public $DESCLI, $_DESCLIAttributes;
    public $CGCCPF, $_CGCCPFAttributes;
    public $CODFAT, $_CODFATPage, $_CODFATParameters, $_CODFATAttributes;
    public $MESREF, $_MESREFAttributes;
    public $SimNao, $_SimNaoAttributes;
    public $DATVNC, $_DATVNCAttributes;
    public $VALFAT, $_VALFATAttributes;
    public $VALPGT, $_VALPGTAttributes;
    public $VALMUL2, $_VALMUL2Attributes;
    public $DIAS_ATRAZO, $_DIAS_ATRAZOAttributes;
    public $VALJUR, $_VALJURAttributes;
    public $VALMUL, $_VALMULAttributes;
    public $ISSQN, $_ISSQNAttributes;
    public $hdd_RET_INSS, $_hdd_RET_INSSAttributes;
    public $RET_INSS, $_RET_INSSAttributes;
    public $DEBITO_ATUAL, $_DEBITO_ATUALAttributes;
    public $TotalSum_VALFAT, $_TotalSum_VALFATAttributes;
    public $TotalSum_VALPGT, $_TotalSum_VALPGTAttributes;
    public $TotalSum_VALJUR, $_TotalSum_VALJURAttributes;
    public $TotalSum_ISSQN, $_TotalSum_ISSQNAttributes;
    public $TotalSum_RET_INSS, $_TotalSum_RET_INSSAttributes;
    public $Total_DEBITO_ATUAL, $_Total_DEBITO_ATUALAttributes;
    public $Attributes;
    public $ReportTotalIndex = 0;
    public $PageTotalIndex;
    public $PageNumber;
    public $RowNumber;
    public $Parent;

    function clsReportGroupCADFAT_CADCLI(& $parent) {
        $this->Parent = & $parent;
        $this->Attributes = $this->Parent->Attributes->GetAsArray();
    }
    function SetControls($PrevGroup = "") {
        $this->DESCLI = $this->Parent->DESCLI->Value;
        $this->CGCCPF = $this->Parent->CGCCPF->Value;
        $this->CODFAT = $this->Parent->CODFAT->Value;
        $this->MESREF = $this->Parent->MESREF->Value;
        $this->SimNao = $this->Parent->SimNao->Value;
        $this->DATVNC = $this->Parent->DATVNC->Value;
        $this->VALFAT = $this->Parent->VALFAT->Value;
        $this->VALPGT = $this->Parent->VALPGT->Value;
        $this->VALMUL2 = $this->Parent->VALMUL2->Value;
        $this->DIAS_ATRAZO = $this->Parent->DIAS_ATRAZO->Value;
        $this->VALJUR = $this->Parent->VALJUR->Value;
        $this->VALMUL = $this->Parent->VALMUL->Value;
        $this->ISSQN = $this->Parent->ISSQN->Value;
        $this->hdd_RET_INSS = $this->Parent->hdd_RET_INSS->Value;
        $this->RET_INSS = $this->Parent->RET_INSS->Value;
        $this->DEBITO_ATUAL = $this->Parent->DEBITO_ATUAL->Value;
    }

    function SetTotalControls($mode = "", $PrevGroup = "") {
        $this->TotalSum_VALFAT = $this->Parent->TotalSum_VALFAT->GetTotalValue($mode);
        $this->TotalSum_VALPGT = $this->Parent->TotalSum_VALPGT->GetTotalValue($mode);
        $this->TotalSum_VALJUR = $this->Parent->TotalSum_VALJUR->GetTotalValue($mode);
        $this->TotalSum_ISSQN = $this->Parent->TotalSum_ISSQN->GetTotalValue($mode);
        $this->TotalSum_RET_INSS = $this->Parent->TotalSum_RET_INSS->GetTotalValue($mode);
        $this->Total_DEBITO_ATUAL = $this->Parent->Total_DEBITO_ATUAL->GetTotalValue($mode);
        $this->_CODFATPage = $this->Parent->CODFAT->Page;
        $this->_CODFATParameters = $this->Parent->CODFAT->Parameters;
        $this->_Sorter_DESCLIAttributes = $this->Parent->Sorter_DESCLI->Attributes->GetAsArray();
        $this->_Sorter_CGCCPFAttributes = $this->Parent->Sorter_CGCCPF->Attributes->GetAsArray();
        $this->_Sorter_CODFATAttributes = $this->Parent->Sorter_CODFAT->Attributes->GetAsArray();
        $this->_Sorter_MESREFAttributes = $this->Parent->Sorter_MESREF->Attributes->GetAsArray();
        $this->_Sorter_DATVNCAttributes = $this->Parent->Sorter_DATVNC->Attributes->GetAsArray();
        $this->_Sorter_VALFATAttributes = $this->Parent->Sorter_VALFAT->Attributes->GetAsArray();
        $this->_Sorter2Attributes = $this->Parent->Sorter2->Attributes->GetAsArray();
        $this->_Sorter1Attributes = $this->Parent->Sorter1->Attributes->GetAsArray();
        $this->_Sorter_VALJURAttributes = $this->Parent->Sorter_VALJUR->Attributes->GetAsArray();
        $this->_Sorter_ISSQNAttributes = $this->Parent->Sorter_ISSQN->Attributes->GetAsArray();
        $this->_Sorter_RET_INSSAttributes = $this->Parent->Sorter_RET_INSS->Attributes->GetAsArray();
        $this->_Report_CurrentDateTimeAttributes = $this->Parent->Report_CurrentDateTime->Attributes->GetAsArray();
        $this->_NavigatorAttributes = $this->Parent->Navigator->Attributes->GetAsArray();
        $this->_DESCLIAttributes = $this->Parent->DESCLI->Attributes->GetAsArray();
        $this->_CGCCPFAttributes = $this->Parent->CGCCPF->Attributes->GetAsArray();
        $this->_CODFATAttributes = $this->Parent->CODFAT->Attributes->GetAsArray();
        $this->_MESREFAttributes = $this->Parent->MESREF->Attributes->GetAsArray();
        $this->_SimNaoAttributes = $this->Parent->SimNao->Attributes->GetAsArray();
        $this->_DATVNCAttributes = $this->Parent->DATVNC->Attributes->GetAsArray();
        $this->_VALFATAttributes = $this->Parent->VALFAT->Attributes->GetAsArray();
        $this->_VALPGTAttributes = $this->Parent->VALPGT->Attributes->GetAsArray();
        $this->_VALMUL2Attributes = $this->Parent->VALMUL2->Attributes->GetAsArray();
        $this->_DIAS_ATRAZOAttributes = $this->Parent->DIAS_ATRAZO->Attributes->GetAsArray();
        $this->_VALJURAttributes = $this->Parent->VALJUR->Attributes->GetAsArray();
        $this->_VALMULAttributes = $this->Parent->VALMUL->Attributes->GetAsArray();
        $this->_ISSQNAttributes = $this->Parent->ISSQN->Attributes->GetAsArray();
        $this->_hdd_RET_INSSAttributes = $this->Parent->hdd_RET_INSS->Attributes->GetAsArray();
        $this->_RET_INSSAttributes = $this->Parent->RET_INSS->Attributes->GetAsArray();
        $this->_DEBITO_ATUALAttributes = $this->Parent->DEBITO_ATUAL->Attributes->GetAsArray();
        $this->_TotalSum_VALFATAttributes = $this->Parent->TotalSum_VALFAT->Attributes->GetAsArray();
        $this->_TotalSum_VALPGTAttributes = $this->Parent->TotalSum_VALPGT->Attributes->GetAsArray();
        $this->_TotalSum_VALJURAttributes = $this->Parent->TotalSum_VALJUR->Attributes->GetAsArray();
        $this->_TotalSum_ISSQNAttributes = $this->Parent->TotalSum_ISSQN->Attributes->GetAsArray();
        $this->_TotalSum_RET_INSSAttributes = $this->Parent->TotalSum_RET_INSS->Attributes->GetAsArray();
        $this->_Total_DEBITO_ATUALAttributes = $this->Parent->Total_DEBITO_ATUAL->Attributes->GetAsArray();
    }
    function SyncWithHeader(& $Header) {
        $Header->TotalSum_VALFAT = $this->TotalSum_VALFAT;
        $Header->_TotalSum_VALFATAttributes = $this->_TotalSum_VALFATAttributes;
        $Header->TotalSum_VALPGT = $this->TotalSum_VALPGT;
        $Header->_TotalSum_VALPGTAttributes = $this->_TotalSum_VALPGTAttributes;
        $Header->TotalSum_VALJUR = $this->TotalSum_VALJUR;
        $Header->_TotalSum_VALJURAttributes = $this->_TotalSum_VALJURAttributes;
        $Header->TotalSum_ISSQN = $this->TotalSum_ISSQN;
        $Header->_TotalSum_ISSQNAttributes = $this->_TotalSum_ISSQNAttributes;
        $Header->TotalSum_RET_INSS = $this->TotalSum_RET_INSS;
        $Header->_TotalSum_RET_INSSAttributes = $this->_TotalSum_RET_INSSAttributes;
        $Header->Total_DEBITO_ATUAL = $this->Total_DEBITO_ATUAL;
        $Header->_Total_DEBITO_ATUALAttributes = $this->_Total_DEBITO_ATUALAttributes;
        $this->DESCLI = $Header->DESCLI;
        $Header->_DESCLIAttributes = $this->_DESCLIAttributes;
        $this->Parent->DESCLI->Value = $Header->DESCLI;
        $this->Parent->DESCLI->Attributes->RestoreFromArray($Header->_DESCLIAttributes);
        $this->CGCCPF = $Header->CGCCPF;
        $Header->_CGCCPFAttributes = $this->_CGCCPFAttributes;
        $this->Parent->CGCCPF->Value = $Header->CGCCPF;
        $this->Parent->CGCCPF->Attributes->RestoreFromArray($Header->_CGCCPFAttributes);
        $this->CODFAT = $Header->CODFAT;
        $this->_CODFATPage = $Header->_CODFATPage;
        $this->_CODFATParameters = $Header->_CODFATParameters;
        $Header->_CODFATAttributes = $this->_CODFATAttributes;
        $this->Parent->CODFAT->Value = $Header->CODFAT;
        $this->Parent->CODFAT->Attributes->RestoreFromArray($Header->_CODFATAttributes);
        $this->MESREF = $Header->MESREF;
        $Header->_MESREFAttributes = $this->_MESREFAttributes;
        $this->Parent->MESREF->Value = $Header->MESREF;
        $this->Parent->MESREF->Attributes->RestoreFromArray($Header->_MESREFAttributes);
        $this->SimNao = $Header->SimNao;
        $Header->_SimNaoAttributes = $this->_SimNaoAttributes;
        $this->Parent->SimNao->Value = $Header->SimNao;
        $this->Parent->SimNao->Attributes->RestoreFromArray($Header->_SimNaoAttributes);
        $this->DATVNC = $Header->DATVNC;
        $Header->_DATVNCAttributes = $this->_DATVNCAttributes;
        $this->Parent->DATVNC->Value = $Header->DATVNC;
        $this->Parent->DATVNC->Attributes->RestoreFromArray($Header->_DATVNCAttributes);
        $this->VALFAT = $Header->VALFAT;
        $Header->_VALFATAttributes = $this->_VALFATAttributes;
        $this->Parent->VALFAT->Value = $Header->VALFAT;
        $this->Parent->VALFAT->Attributes->RestoreFromArray($Header->_VALFATAttributes);
        $this->VALPGT = $Header->VALPGT;
        $Header->_VALPGTAttributes = $this->_VALPGTAttributes;
        $this->Parent->VALPGT->Value = $Header->VALPGT;
        $this->Parent->VALPGT->Attributes->RestoreFromArray($Header->_VALPGTAttributes);
        $this->VALMUL2 = $Header->VALMUL2;
        $Header->_VALMUL2Attributes = $this->_VALMUL2Attributes;
        $this->Parent->VALMUL2->Value = $Header->VALMUL2;
        $this->Parent->VALMUL2->Attributes->RestoreFromArray($Header->_VALMUL2Attributes);
        $this->DIAS_ATRAZO = $Header->DIAS_ATRAZO;
        $Header->_DIAS_ATRAZOAttributes = $this->_DIAS_ATRAZOAttributes;
        $this->Parent->DIAS_ATRAZO->Value = $Header->DIAS_ATRAZO;
        $this->Parent->DIAS_ATRAZO->Attributes->RestoreFromArray($Header->_DIAS_ATRAZOAttributes);
        $this->VALJUR = $Header->VALJUR;
        $Header->_VALJURAttributes = $this->_VALJURAttributes;
        $this->Parent->VALJUR->Value = $Header->VALJUR;
        $this->Parent->VALJUR->Attributes->RestoreFromArray($Header->_VALJURAttributes);
        $this->VALMUL = $Header->VALMUL;
        $Header->_VALMULAttributes = $this->_VALMULAttributes;
        $this->Parent->VALMUL->Value = $Header->VALMUL;
        $this->Parent->VALMUL->Attributes->RestoreFromArray($Header->_VALMULAttributes);
        $this->ISSQN = $Header->ISSQN;
        $Header->_ISSQNAttributes = $this->_ISSQNAttributes;
        $this->Parent->ISSQN->Value = $Header->ISSQN;
        $this->Parent->ISSQN->Attributes->RestoreFromArray($Header->_ISSQNAttributes);
        $this->hdd_RET_INSS = $Header->hdd_RET_INSS;
        $Header->_hdd_RET_INSSAttributes = $this->_hdd_RET_INSSAttributes;
        $this->Parent->hdd_RET_INSS->Value = $Header->hdd_RET_INSS;
        $this->Parent->hdd_RET_INSS->Attributes->RestoreFromArray($Header->_hdd_RET_INSSAttributes);
        $this->RET_INSS = $Header->RET_INSS;
        $Header->_RET_INSSAttributes = $this->_RET_INSSAttributes;
        $this->Parent->RET_INSS->Value = $Header->RET_INSS;
        $this->Parent->RET_INSS->Attributes->RestoreFromArray($Header->_RET_INSSAttributes);
        $this->DEBITO_ATUAL = $Header->DEBITO_ATUAL;
        $Header->_DEBITO_ATUALAttributes = $this->_DEBITO_ATUALAttributes;
        $this->Parent->DEBITO_ATUAL->Value = $Header->DEBITO_ATUAL;
        $this->Parent->DEBITO_ATUAL->Attributes->RestoreFromArray($Header->_DEBITO_ATUALAttributes);
    }
    function ChangeTotalControls() {
        $this->TotalSum_VALFAT = $this->Parent->TotalSum_VALFAT->GetValue();
        $this->TotalSum_VALPGT = $this->Parent->TotalSum_VALPGT->GetValue();
        $this->TotalSum_VALJUR = $this->Parent->TotalSum_VALJUR->GetValue();
        $this->TotalSum_ISSQN = $this->Parent->TotalSum_ISSQN->GetValue();
        $this->TotalSum_RET_INSS = $this->Parent->TotalSum_RET_INSS->GetValue();
        $this->Total_DEBITO_ATUAL = $this->Parent->Total_DEBITO_ATUAL->GetValue();
    }
}
//End CADFAT_CADCLI ReportGroup class

//CADFAT_CADCLI GroupsCollection class @4-56BF1B06
class clsGroupsCollectionCADFAT_CADCLI {
    public $Groups;
    public $mPageCurrentHeaderIndex;
    public $PageSize;
    public $TotalPages = 0;
    public $TotalRows = 0;
    public $CurrentPageSize = 0;
    public $Pages;
    public $Parent;
    public $LastDetailIndex;

    function clsGroupsCollectionCADFAT_CADCLI(& $parent) {
        $this->Parent = & $parent;
        $this->Groups = array();
        $this->Pages  = array();
        $this->mReportTotalIndex = 0;
        $this->mPageTotalIndex = 1;
    }

    function & InitGroup() {
        $group = new clsReportGroupCADFAT_CADCLI($this->Parent);
        $group->RowNumber = $this->TotalRows + 1;
        $group->PageNumber = $this->TotalPages;
        $group->PageTotalIndex = $this->mPageCurrentHeaderIndex;
        return $group;
    }

    function RestoreValues() {
        $this->Parent->DESCLI->Value = $this->Parent->DESCLI->initialValue;
        $this->Parent->CGCCPF->Value = $this->Parent->CGCCPF->initialValue;
        $this->Parent->CODFAT->Value = $this->Parent->CODFAT->initialValue;
        $this->Parent->MESREF->Value = $this->Parent->MESREF->initialValue;
        $this->Parent->SimNao->Value = $this->Parent->SimNao->initialValue;
        $this->Parent->DATVNC->Value = $this->Parent->DATVNC->initialValue;
        $this->Parent->VALFAT->Value = $this->Parent->VALFAT->initialValue;
        $this->Parent->VALPGT->Value = $this->Parent->VALPGT->initialValue;
        $this->Parent->VALMUL2->Value = $this->Parent->VALMUL2->initialValue;
        $this->Parent->DIAS_ATRAZO->Value = $this->Parent->DIAS_ATRAZO->initialValue;
        $this->Parent->VALJUR->Value = $this->Parent->VALJUR->initialValue;
        $this->Parent->VALMUL->Value = $this->Parent->VALMUL->initialValue;
        $this->Parent->ISSQN->Value = $this->Parent->ISSQN->initialValue;
        $this->Parent->hdd_RET_INSS->Value = $this->Parent->hdd_RET_INSS->initialValue;
        $this->Parent->RET_INSS->Value = $this->Parent->RET_INSS->initialValue;
        $this->Parent->DEBITO_ATUAL->Value = $this->Parent->DEBITO_ATUAL->initialValue;
        $this->Parent->TotalSum_VALFAT->Value = $this->Parent->TotalSum_VALFAT->initialValue;
        $this->Parent->TotalSum_VALPGT->Value = $this->Parent->TotalSum_VALPGT->initialValue;
        $this->Parent->TotalSum_VALJUR->Value = $this->Parent->TotalSum_VALJUR->initialValue;
        $this->Parent->TotalSum_ISSQN->Value = $this->Parent->TotalSum_ISSQN->initialValue;
        $this->Parent->TotalSum_RET_INSS->Value = $this->Parent->TotalSum_RET_INSS->initialValue;
        $this->Parent->Total_DEBITO_ATUAL->Value = $this->Parent->Total_DEBITO_ATUAL->initialValue;
    }

    function OpenPage() {
        $this->TotalPages++;
        $Group = & $this->InitGroup();
        $this->Parent->Page_Header->CCSEventResult = CCGetEvent($this->Parent->Page_Header->CCSEvents, "OnInitialize", $this->Parent->Page_Header);
        if ($this->Parent->Page_Header->Visible)
            $this->CurrentPageSize = $this->CurrentPageSize + $this->Parent->Page_Header->Height;
        $Group->SetTotalControls("GetNextValue");
        $this->Parent->Page_Header->CCSEventResult = CCGetEvent($this->Parent->Page_Header->CCSEvents, "OnCalculate", $this->Parent->Page_Header);
        $Group->SetControls();
        $Group->Mode = 1;
        $Group->GroupType = "Page";
        $Group->PageTotalIndex = count($this->Groups);
        $this->mPageCurrentHeaderIndex = count($this->Groups);
        $this->Groups[] =  & $Group;
        $this->Pages[] =  count($this->Groups) == 2 ? 0 : count($this->Groups) - 1;
    }

    function OpenGroup($groupName) {
        $Group = "";
        $OpenFlag = false;
        if ($groupName == "Report") {
            $Group = & $this->InitGroup(true);
            $this->Parent->Report_Header->CCSEventResult = CCGetEvent($this->Parent->Report_Header->CCSEvents, "OnInitialize", $this->Parent->Report_Header);
            if ($this->Parent->Report_Header->Visible) 
                $this->CurrentPageSize = $this->CurrentPageSize + $this->Parent->Report_Header->Height;
                $Group->SetTotalControls("GetNextValue");
            $this->Parent->Report_Header->CCSEventResult = CCGetEvent($this->Parent->Report_Header->CCSEvents, "OnCalculate", $this->Parent->Report_Header);
            $Group->SetControls();
            $Group->Mode = 1;
            $Group->GroupType = "Report";
            $this->Groups[] = & $Group;
            $this->OpenPage();
        }
    }

    function ClosePage() {
        $Group = & $this->InitGroup();
        $this->Parent->Page_Footer->CCSEventResult = CCGetEvent($this->Parent->Page_Footer->CCSEvents, "OnInitialize", $this->Parent->Page_Footer);
        $Group->SetTotalControls("GetPrevValue");
        $Group->SyncWithHeader($this->Groups[$this->mPageCurrentHeaderIndex]);
        $this->Parent->Page_Footer->CCSEventResult = CCGetEvent($this->Parent->Page_Footer->CCSEvents, "OnCalculate", $this->Parent->Page_Footer);
        $Group->SetControls();
        $this->RestoreValues();
        $this->CurrentPageSize = 0;
        $Group->Mode = 2;
        $Group->GroupType = "Page";
        $this->Groups[] = & $Group;
    }

    function CloseGroup($groupName)
    {
        $Group = "";
        if ($groupName == "Report") {
            $Group = & $this->InitGroup(true);
            $this->Parent->Report_Footer->CCSEventResult = CCGetEvent($this->Parent->Report_Footer->CCSEvents, "OnInitialize", $this->Parent->Report_Footer);
            if ($this->Parent->Page_Footer->Visible) 
                $OverSize = $this->Parent->Report_Footer->Height + $this->Parent->Page_Footer->Height;
            else
                $OverSize = $this->Parent->Report_Footer->Height;
            if (($this->PageSize > 0) and $this->Parent->Report_Footer->Visible and ($this->CurrentPageSize + $OverSize > $this->PageSize)) {
                $this->ClosePage();
                $this->OpenPage();
            }
            $Group->SetTotalControls("GetPrevValue");
            $Group->SyncWithHeader($this->Groups[0]);
            if ($this->Parent->Report_Footer->Visible)
                $this->CurrentPageSize = $this->CurrentPageSize + $this->Parent->Report_Footer->Height;
            $this->Parent->Report_Footer->CCSEventResult = CCGetEvent($this->Parent->Report_Footer->CCSEvents, "OnCalculate", $this->Parent->Report_Footer);
            $Group->SetControls();
            $this->RestoreValues();
            $Group->Mode = 2;
            $Group->GroupType = "Report";
            $this->Groups[] = & $Group;
            $this->ClosePage();
            return;
        }
    }

    function AddItem()
    {
        $Group = & $this->InitGroup(true);
        $this->Parent->Detail->CCSEventResult = CCGetEvent($this->Parent->Detail->CCSEvents, "OnInitialize", $this->Parent->Detail);
        if ($this->Parent->Page_Footer->Visible) 
            $OverSize = $this->Parent->Detail->Height + $this->Parent->Page_Footer->Height;
        else
            $OverSize = $this->Parent->Detail->Height;
        if (($this->PageSize > 0) and $this->Parent->Detail->Visible and ($this->CurrentPageSize + $OverSize > $this->PageSize)) {
            $this->ClosePage();
            $this->OpenPage();
        }
        $this->TotalRows++;
        if ($this->LastDetailIndex)
            $PrevGroup = & $this->Groups[$this->LastDetailIndex];
        else
            $PrevGroup = "";
        $Group->SetTotalControls("", $PrevGroup);
        if ($this->Parent->Detail->Visible)
            $this->CurrentPageSize = $this->CurrentPageSize + $this->Parent->Detail->Height;
        $this->Parent->Detail->CCSEventResult = CCGetEvent($this->Parent->Detail->CCSEvents, "OnCalculate", $this->Parent->Detail);
        $Group->SetControls($PrevGroup);
        $this->LastDetailIndex = count($this->Groups);
        $this->Groups[] = & $Group;
    }
}
//End CADFAT_CADCLI GroupsCollection class

class clsReportCADFAT_CADCLI { //CADFAT_CADCLI Class @4-BE049FB3

//CADFAT_CADCLI Variables @4-14A18587

    public $ComponentType = "Report";
    public $PageSize;
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $CCSEvents = array();
    public $CCSEventResult;
    public $RelativePath = "";
    public $ViewMode = "Web";
    public $TemplateBlock;
    public $PageNumber;
    public $RowNumber;
    public $TotalRows;
    public $TotalPages;
    public $ControlsVisible = array();
    public $IsEmpty;
    public $Attributes;
    public $DetailBlock, $Detail;
    public $Report_FooterBlock, $Report_Footer;
    public $Report_HeaderBlock, $Report_Header;
    public $Page_FooterBlock, $Page_Footer;
    public $Page_HeaderBlock, $Page_Header;
    public $SorterName, $SorterDirection;

    public $ds;
    public $DataSource;
    public $UseClientPaging = false;

    //Report Controls
    public $StaticControls, $RowControls, $Report_FooterControls, $Report_HeaderControls;
    public $Page_FooterControls, $Page_HeaderControls;
    public $Sorter_DESCLI;
    public $Sorter_CGCCPF;
    public $Sorter_CODFAT;
    public $Sorter_MESREF;
    public $Sorter_DATVNC;
    public $Sorter_VALFAT;
    public $Sorter2;
    public $Sorter1;
    public $Sorter_VALJUR;
    public $Sorter_ISSQN;
    public $Sorter_RET_INSS;
//End CADFAT_CADCLI Variables

//Class_Initialize Event @4-1F4617AA
    function clsReportCADFAT_CADCLI($RelativePath = "", & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "CADFAT_CADCLI";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->Detail = new clsSection($this);
        $MinPageSize = 0;
        $MaxSectionSize = 0;
        $this->Detail->Height = 1;
        $MaxSectionSize = max($MaxSectionSize, $this->Detail->Height);
        $this->Report_Footer = new clsSection($this);
        $this->Report_Footer->Height = 1;
        $MaxSectionSize = max($MaxSectionSize, $this->Report_Footer->Height);
        $this->Report_Header = new clsSection($this);
        $this->Report_Header->Height = 1;
        $MaxSectionSize = max($MaxSectionSize, $this->Report_Header->Height);
        $this->Page_Footer = new clsSection($this);
        $this->Page_Footer->Height = 1;
        $MinPageSize += $this->Page_Footer->Height;
        $this->Page_Header = new clsSection($this);
        $this->Page_Header->Height = 1;
        $MinPageSize += $this->Page_Header->Height;
        $this->Errors = new clsErrors();
        $this->DataSource = new clsCADFAT_CADCLIDataSource($this);
        $this->ds = & $this->DataSource;
        $this->ViewMode = CCGetParam("ViewMode", "Web");
        $PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(is_numeric($PageSize) && $PageSize > 0) {
            $this->PageSize = $PageSize;
        } else if($this->ViewMode == "Print") {
            if (!is_numeric($PageSize) || $PageSize < 0)
                $this->PageSize = 66;
             else if ($PageSize == "0")
                $this->PageSize = 0;
             else 
                $this->PageSize = $PageSize;
        } else {
            if (!is_numeric($PageSize) || $PageSize < 0)
                $this->PageSize = 40;
             else if ($PageSize == "0")
                $this->PageSize = 100;
             else 
                $this->PageSize = min(100, $PageSize);
        }
        $MinPageSize += $MaxSectionSize;
        if ($this->PageSize && $MinPageSize && $this->PageSize < $MinPageSize)
            $this->PageSize = $MinPageSize;
        $this->PageNumber = $this->ViewMode == "Print" ? 1 : intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0 ) {
            $this->PageNumber = 1;
        }
        $this->Visible = (CCSecurityAccessCheck("1;2") == "success");
        $this->SorterName = CCGetParam("CADFAT_CADCLIOrder", "");
        $this->SorterDirection = CCGetParam("CADFAT_CADCLIDir", "");

        $this->Sorter_DESCLI = new clsSorter($this->ComponentName, "Sorter_DESCLI", $FileName, $this);
        $this->Sorter_CGCCPF = new clsSorter($this->ComponentName, "Sorter_CGCCPF", $FileName, $this);
        $this->Sorter_CODFAT = new clsSorter($this->ComponentName, "Sorter_CODFAT", $FileName, $this);
        $this->Sorter_MESREF = new clsSorter($this->ComponentName, "Sorter_MESREF", $FileName, $this);
        $this->Sorter_DATVNC = new clsSorter($this->ComponentName, "Sorter_DATVNC", $FileName, $this);
        $this->Sorter_VALFAT = new clsSorter($this->ComponentName, "Sorter_VALFAT", $FileName, $this);
        $this->Sorter2 = new clsSorter($this->ComponentName, "Sorter2", $FileName, $this);
        $this->Sorter1 = new clsSorter($this->ComponentName, "Sorter1", $FileName, $this);
        $this->Sorter_VALJUR = new clsSorter($this->ComponentName, "Sorter_VALJUR", $FileName, $this);
        $this->Sorter_ISSQN = new clsSorter($this->ComponentName, "Sorter_ISSQN", $FileName, $this);
        $this->Sorter_RET_INSS = new clsSorter($this->ComponentName, "Sorter_RET_INSS", $FileName, $this);
        $this->Report_CurrentDateTime = new clsControl(ccsReportLabel, "Report_CurrentDateTime", "Report_CurrentDateTime", ccsText, array('ShortDate', ' ', 'ShortTime'), "", $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple, $this);
        $this->DESCLI = new clsControl(ccsReportLabel, "DESCLI", "DESCLI", ccsText, "", "", $this);
        $this->CGCCPF = new clsControl(ccsReportLabel, "CGCCPF", "CGCCPF", ccsText, "", "", $this);
        $this->CODFAT = new clsControl(ccsLink, "CODFAT", "CODFAT", ccsText, "", CCGetRequestParam("CODFAT", ccsGet, NULL), $this);
        $this->CODFAT->Page = "PagtoFatu_Dados.php";
        $this->MESREF = new clsControl(ccsReportLabel, "MESREF", "MESREF", ccsText, "", "", $this);
        $this->SimNao = new clsControl(ccsReportLabel, "SimNao", "SimNao", ccsText, "", "", $this);
        $this->SimNao->IsEmptySource = true;
        $this->DATVNC = new clsControl(ccsReportLabel, "DATVNC", "DATVNC", ccsText, "", "", $this);
        $this->VALFAT = new clsControl(ccsReportLabel, "VALFAT", "VALFAT", ccsFloat, array(False, 2, ",", ".", False, "", "", 1, True, ""), "", $this);
        $this->VALPGT = new clsControl(ccsReportLabel, "VALPGT", "VALPGT", ccsFloat, array(False, 2, ",", ".", False, "", "", 1, True, ""), "", $this);
        $this->VALMUL2 = new clsControl(ccsReportLabel, "VALMUL2", "VALMUL2", ccsFloat, array(False, 2, ",", ".", False, "", "", 1, True, ""), "", $this);
        $this->DIAS_ATRAZO = new clsControl(ccsReportLabel, "DIAS_ATRAZO", "DIAS_ATRAZO", ccsInteger, "", "", $this);
        $this->VALJUR = new clsControl(ccsReportLabel, "VALJUR", "VALJUR", ccsFloat, array(False, 2, ",", ".", False, "", "", 1, True, ""), "", $this);
        $this->VALMUL = new clsControl(ccsHidden, "VALMUL", "VALMUL", ccsFloat, "", CCGetRequestParam("VALMUL", ccsGet, NULL), $this);
        $this->ISSQN = new clsControl(ccsReportLabel, "ISSQN", "ISSQN", ccsFloat, array(False, 2, ",", ".", False, "", "", 1, True, ""), "", $this);
        $this->hdd_RET_INSS = new clsControl(ccsHidden, "hdd_RET_INSS", "hdd_RET_INSS", ccsFloat, "", CCGetRequestParam("hdd_RET_INSS", ccsGet, NULL), $this);
        $this->RET_INSS = new clsControl(ccsReportLabel, "RET_INSS", "RET_INSS", ccsFloat, array(False, 2, ",", ".", False, "", "", 1, True, ""), "", $this);
        $this->DEBITO_ATUAL = new clsControl(ccsReportLabel, "DEBITO_ATUAL", "DEBITO_ATUAL", ccsFloat, array(False, 2, ",", ".", False, "", "", 1, True, ""), "", $this);
        $this->DEBITO_ATUAL->IsEmptySource = true;
        $this->NoRecords = new clsPanel("NoRecords", $this);
        $this->TotalSum_VALFAT = new clsControl(ccsReportLabel, "TotalSum_VALFAT", "TotalSum_VALFAT", ccsFloat, array(False, 2, ",", ".", False, "R\$ ", "", 1, True, ""), "", $this);
        $this->TotalSum_VALFAT->TotalFunction = "Sum";
        $this->TotalSum_VALPGT = new clsControl(ccsReportLabel, "TotalSum_VALPGT", "TotalSum_VALPGT", ccsFloat, array(False, 2, ",", ".", False, "R\$ ", "", 1, True, ""), "", $this);
        $this->TotalSum_VALPGT->TotalFunction = "Sum";
        $this->TotalSum_VALJUR = new clsControl(ccsReportLabel, "TotalSum_VALJUR", "TotalSum_VALJUR", ccsFloat, array(False, 2, ",", ".", False, "R\$ ", "", 1, True, ""), "", $this);
        $this->TotalSum_VALJUR->TotalFunction = "Sum";
        $this->TotalSum_ISSQN = new clsControl(ccsReportLabel, "TotalSum_ISSQN", "TotalSum_ISSQN", ccsFloat, array(False, 2, ",", ".", False, "R\$ ", "", 1, True, ""), "", $this);
        $this->TotalSum_ISSQN->TotalFunction = "Sum";
        $this->TotalSum_RET_INSS = new clsControl(ccsReportLabel, "TotalSum_RET_INSS", "TotalSum_RET_INSS", ccsFloat, array(False, 2, ",", ".", False, "R\$ ", "", 1, True, ""), "", $this);
        $this->TotalSum_RET_INSS->TotalFunction = "Sum";
        $this->Total_DEBITO_ATUAL = new clsControl(ccsReportLabel, "Total_DEBITO_ATUAL", "Total_DEBITO_ATUAL", ccsFloat, array(False, 2, ",", ".", False, "R\$ ", "", 1, True, ""), "", $this);
        $this->Total_DEBITO_ATUAL->TotalFunction = "Sum";
        $this->Total_DEBITO_ATUAL->IsEmptySource = true;
    }
//End Class_Initialize Event

//Initialize Method @4-6C59EE65
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = $this->PageSize;
        $this->DataSource->AbsolutePage = $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//CheckErrors Method @4-F69F80F5
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->Report_CurrentDateTime->Errors->Count());
        $errors = ($errors || $this->DESCLI->Errors->Count());
        $errors = ($errors || $this->CGCCPF->Errors->Count());
        $errors = ($errors || $this->CODFAT->Errors->Count());
        $errors = ($errors || $this->MESREF->Errors->Count());
        $errors = ($errors || $this->SimNao->Errors->Count());
        $errors = ($errors || $this->DATVNC->Errors->Count());
        $errors = ($errors || $this->VALFAT->Errors->Count());
        $errors = ($errors || $this->VALPGT->Errors->Count());
        $errors = ($errors || $this->VALMUL2->Errors->Count());
        $errors = ($errors || $this->DIAS_ATRAZO->Errors->Count());
        $errors = ($errors || $this->VALJUR->Errors->Count());
        $errors = ($errors || $this->VALMUL->Errors->Count());
        $errors = ($errors || $this->ISSQN->Errors->Count());
        $errors = ($errors || $this->hdd_RET_INSS->Errors->Count());
        $errors = ($errors || $this->RET_INSS->Errors->Count());
        $errors = ($errors || $this->DEBITO_ATUAL->Errors->Count());
        $errors = ($errors || $this->TotalSum_VALFAT->Errors->Count());
        $errors = ($errors || $this->TotalSum_VALPGT->Errors->Count());
        $errors = ($errors || $this->TotalSum_VALJUR->Errors->Count());
        $errors = ($errors || $this->TotalSum_ISSQN->Errors->Count());
        $errors = ($errors || $this->TotalSum_RET_INSS->Errors->Count());
        $errors = ($errors || $this->Total_DEBITO_ATUAL->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//GetErrors Method @4-10AD9437
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->Report_CurrentDateTime->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESCLI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CGCCPF->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CODFAT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->MESREF->Errors->ToString());
        $errors = ComposeStrings($errors, $this->SimNao->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DATVNC->Errors->ToString());
        $errors = ComposeStrings($errors, $this->VALFAT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->VALPGT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->VALMUL2->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DIAS_ATRAZO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->VALJUR->Errors->ToString());
        $errors = ComposeStrings($errors, $this->VALMUL->Errors->ToString());
        $errors = ComposeStrings($errors, $this->ISSQN->Errors->ToString());
        $errors = ComposeStrings($errors, $this->hdd_RET_INSS->Errors->ToString());
        $errors = ComposeStrings($errors, $this->RET_INSS->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DEBITO_ATUAL->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TotalSum_VALFAT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TotalSum_VALPGT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TotalSum_VALJUR->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TotalSum_ISSQN->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TotalSum_RET_INSS->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Total_DEBITO_ATUAL->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

//Show Method @4-714482DF
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $ShownRecords = 0;

        $this->DataSource->Parameters["urls_CODCLI"] = CCGetFromGet("s_CODCLI", NULL);
        $this->DataSource->Parameters["urls_MESREF"] = CCGetFromGet("s_MESREF", NULL);
        $this->DataSource->Parameters["urls_DESCLI"] = CCGetFromGet("s_DESCLI", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();

        $Groups = new clsGroupsCollectionCADFAT_CADCLI($this);
        $Groups->PageSize = $this->PageSize > 0 ? $this->PageSize : 0;

        $is_next_record = $this->DataSource->next_record();
        $this->IsEmpty = ! $is_next_record;
        while($is_next_record) {
            $this->DataSource->SetValues();
            $this->DESCLI->SetValue($this->DataSource->DESCLI->GetValue());
            $this->CGCCPF->SetValue($this->DataSource->CGCCPF->GetValue());
            $this->CODFAT->SetValue($this->DataSource->CODFAT->GetValue());
            $this->MESREF->SetValue($this->DataSource->MESREF->GetValue());
            $this->DATVNC->SetValue($this->DataSource->DATVNC->GetValue());
            $this->VALFAT->SetValue($this->DataSource->VALFAT->GetValue());
            $this->VALPGT->SetValue($this->DataSource->VALPGT->GetValue());
            $this->VALMUL2->SetValue($this->DataSource->VALMUL2->GetValue());
            $this->DIAS_ATRAZO->SetValue($this->DataSource->DIAS_ATRAZO->GetValue());
            $this->VALJUR->SetValue($this->DataSource->VALJUR->GetValue());
            $this->VALMUL->SetValue($this->DataSource->VALMUL->GetValue());
            $this->ISSQN->SetValue($this->DataSource->ISSQN->GetValue());
            $this->hdd_RET_INSS->SetValue($this->DataSource->hdd_RET_INSS->GetValue());
            $this->RET_INSS->SetValue($this->DataSource->RET_INSS->GetValue());
            $this->TotalSum_VALFAT->SetValue($this->DataSource->TotalSum_VALFAT->GetValue());
            $this->TotalSum_VALPGT->SetValue($this->DataSource->TotalSum_VALPGT->GetValue());
            $this->TotalSum_VALJUR->SetValue($this->DataSource->TotalSum_VALJUR->GetValue());
            $this->TotalSum_ISSQN->SetValue($this->DataSource->TotalSum_ISSQN->GetValue());
            $this->TotalSum_RET_INSS->SetValue($this->DataSource->TotalSum_RET_INSS->GetValue());
            $this->CODFAT->Parameters = "";
            $this->CODFAT->Parameters = CCAddParam($this->CODFAT->Parameters, "opcao", "'1'");
            $this->CODFAT->Parameters = CCAddParam($this->CODFAT->Parameters, "CODFAT", $this->DataSource->f("CODFAT"));
            $this->Total_DEBITO_ATUAL->SetValue(1);
            $this->SimNao->SetValue("");
            $this->DEBITO_ATUAL->SetValue("");
            if (count($Groups->Groups) == 0) $Groups->OpenGroup("Report");
            $Groups->AddItem();
            $is_next_record = $this->DataSource->next_record();
        }
        if (!count($Groups->Groups)) {
            $Groups->OpenGroup("Report");
            $this->NoRecords->Visible = true;
        } else {
            $this->NoRecords->Visible = false;
        }
        $Groups->CloseGroup("Report");
        $this->TotalPages = $Groups->TotalPages;
        $this->TotalRows = $Groups->TotalRows;

        $this->Attributes->SetValue("um", dois);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $this->Attributes->Show();
        $ReportBlock = "Report " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $ReportBlock;

        if($this->CheckErrors()) {
            $Tpl->replaceblock("", $this->GetErrors());
            $Tpl->block_path = $ParentPath;
            return;
        } else {
            $items = & $Groups->Groups;
            $i = $Groups->Pages[min($this->PageNumber, $Groups->TotalPages) - 1];
            $this->ControlsVisible["DESCLI"] = $this->DESCLI->Visible;
            $this->ControlsVisible["CGCCPF"] = $this->CGCCPF->Visible;
            $this->ControlsVisible["CODFAT"] = $this->CODFAT->Visible;
            $this->ControlsVisible["MESREF"] = $this->MESREF->Visible;
            $this->ControlsVisible["SimNao"] = $this->SimNao->Visible;
            $this->ControlsVisible["DATVNC"] = $this->DATVNC->Visible;
            $this->ControlsVisible["VALFAT"] = $this->VALFAT->Visible;
            $this->ControlsVisible["VALPGT"] = $this->VALPGT->Visible;
            $this->ControlsVisible["VALMUL2"] = $this->VALMUL2->Visible;
            $this->ControlsVisible["DIAS_ATRAZO"] = $this->DIAS_ATRAZO->Visible;
            $this->ControlsVisible["VALJUR"] = $this->VALJUR->Visible;
            $this->ControlsVisible["VALMUL"] = $this->VALMUL->Visible;
            $this->ControlsVisible["ISSQN"] = $this->ISSQN->Visible;
            $this->ControlsVisible["hdd_RET_INSS"] = $this->hdd_RET_INSS->Visible;
            $this->ControlsVisible["RET_INSS"] = $this->RET_INSS->Visible;
            $this->ControlsVisible["DEBITO_ATUAL"] = $this->DEBITO_ATUAL->Visible;
            do {
                $this->Attributes->RestoreFromArray($items[$i]->Attributes);
                $this->RowNumber = $items[$i]->RowNumber;
                switch ($items[$i]->GroupType) {
                    Case "":
                        $Tpl->block_path = $ParentPath . "/" . $ReportBlock . "/Section Detail";
                        $this->DESCLI->SetValue($items[$i]->DESCLI);
                        $this->DESCLI->Attributes->RestoreFromArray($items[$i]->_DESCLIAttributes);
                        $this->CGCCPF->SetValue($items[$i]->CGCCPF);
                        $this->CGCCPF->Attributes->RestoreFromArray($items[$i]->_CGCCPFAttributes);
                        $this->CODFAT->SetValue($items[$i]->CODFAT);
                        $this->CODFAT->Page = $items[$i]->_CODFATPage;
                        $this->CODFAT->Parameters = $items[$i]->_CODFATParameters;
                        $this->CODFAT->Attributes->RestoreFromArray($items[$i]->_CODFATAttributes);
                        $this->MESREF->SetValue($items[$i]->MESREF);
                        $this->MESREF->Attributes->RestoreFromArray($items[$i]->_MESREFAttributes);
                        $this->SimNao->SetValue($items[$i]->SimNao);
                        $this->SimNao->Attributes->RestoreFromArray($items[$i]->_SimNaoAttributes);
                        $this->DATVNC->SetValue($items[$i]->DATVNC);
                        $this->DATVNC->Attributes->RestoreFromArray($items[$i]->_DATVNCAttributes);
                        $this->VALFAT->SetValue($items[$i]->VALFAT);
                        $this->VALFAT->Attributes->RestoreFromArray($items[$i]->_VALFATAttributes);
                        $this->VALPGT->SetValue($items[$i]->VALPGT);
                        $this->VALPGT->Attributes->RestoreFromArray($items[$i]->_VALPGTAttributes);
                        $this->VALMUL2->SetValue($items[$i]->VALMUL2);
                        $this->VALMUL2->Attributes->RestoreFromArray($items[$i]->_VALMUL2Attributes);
                        $this->DIAS_ATRAZO->SetValue($items[$i]->DIAS_ATRAZO);
                        $this->DIAS_ATRAZO->Attributes->RestoreFromArray($items[$i]->_DIAS_ATRAZOAttributes);
                        $this->VALJUR->SetValue($items[$i]->VALJUR);
                        $this->VALJUR->Attributes->RestoreFromArray($items[$i]->_VALJURAttributes);
                        $this->VALMUL->SetValue($items[$i]->VALMUL);
                        $this->VALMUL->Attributes->RestoreFromArray($items[$i]->_VALMULAttributes);
                        $this->ISSQN->SetValue($items[$i]->ISSQN);
                        $this->ISSQN->Attributes->RestoreFromArray($items[$i]->_ISSQNAttributes);
                        $this->hdd_RET_INSS->SetValue($items[$i]->hdd_RET_INSS);
                        $this->hdd_RET_INSS->Attributes->RestoreFromArray($items[$i]->_hdd_RET_INSSAttributes);
                        $this->RET_INSS->SetValue($items[$i]->RET_INSS);
                        $this->RET_INSS->Attributes->RestoreFromArray($items[$i]->_RET_INSSAttributes);
                        $this->DEBITO_ATUAL->SetValue($items[$i]->DEBITO_ATUAL);
                        $this->DEBITO_ATUAL->Attributes->RestoreFromArray($items[$i]->_DEBITO_ATUALAttributes);
                        $this->Detail->CCSEventResult = CCGetEvent($this->Detail->CCSEvents, "BeforeShow", $this->Detail);
                        $this->Attributes->Show();
                        $this->DESCLI->Show();
                        $this->CGCCPF->Show();
                        $this->CODFAT->Show();
                        $this->MESREF->Show();
                        $this->SimNao->Show();
                        $this->DATVNC->Show();
                        $this->VALFAT->Show();
                        $this->VALPGT->Show();
                        $this->VALMUL2->Show();
                        $this->DIAS_ATRAZO->Show();
                        $this->VALJUR->Show();
                        $this->VALMUL->Show();
                        $this->ISSQN->Show();
                        $this->hdd_RET_INSS->Show();
                        $this->RET_INSS->Show();
                        $this->DEBITO_ATUAL->Show();
                        $Tpl->block_path = $ParentPath . "/" . $ReportBlock;
                        if ($this->Detail->Visible)
                            $Tpl->parseto("Section Detail", true, "Section Detail");
                        break;
                    case "Report":
                        if ($items[$i]->Mode == 1) {
                            $this->Report_Header->CCSEventResult = CCGetEvent($this->Report_Header->CCSEvents, "BeforeShow", $this->Report_Header);
                            if ($this->Report_Header->Visible) {
                                $Tpl->block_path = $ParentPath . "/" . $ReportBlock . "/Section Report_Header";
                                $this->Attributes->Show();
                                $Tpl->block_path = $ParentPath . "/" . $ReportBlock;
                                $Tpl->parseto("Section Report_Header", true, "Section Detail");
                            }
                        }
                        if ($items[$i]->Mode == 2) {
                            $this->TotalSum_VALFAT->SetText(CCFormatNumber($items[$i]->TotalSum_VALFAT, array(False, 2, ",", ".", False, "R\$ ", "", 1, True, "")), ccsFloat);
                            $this->TotalSum_VALFAT->Attributes->RestoreFromArray($items[$i]->_TotalSum_VALFATAttributes);
                            $this->TotalSum_VALPGT->SetText(CCFormatNumber($items[$i]->TotalSum_VALPGT, array(False, 2, ",", ".", False, "R\$ ", "", 1, True, "")), ccsFloat);
                            $this->TotalSum_VALPGT->Attributes->RestoreFromArray($items[$i]->_TotalSum_VALPGTAttributes);
                            $this->TotalSum_VALJUR->SetText(CCFormatNumber($items[$i]->TotalSum_VALJUR, array(False, 2, ",", ".", False, "R\$ ", "", 1, True, "")), ccsFloat);
                            $this->TotalSum_VALJUR->Attributes->RestoreFromArray($items[$i]->_TotalSum_VALJURAttributes);
                            $this->TotalSum_ISSQN->SetText(CCFormatNumber($items[$i]->TotalSum_ISSQN, array(False, 2, ",", ".", False, "R\$ ", "", 1, True, "")), ccsFloat);
                            $this->TotalSum_ISSQN->Attributes->RestoreFromArray($items[$i]->_TotalSum_ISSQNAttributes);
                            $this->TotalSum_RET_INSS->SetText(CCFormatNumber($items[$i]->TotalSum_RET_INSS, array(False, 2, ",", ".", False, "R\$ ", "", 1, True, "")), ccsFloat);
                            $this->TotalSum_RET_INSS->Attributes->RestoreFromArray($items[$i]->_TotalSum_RET_INSSAttributes);
                            $this->Total_DEBITO_ATUAL->SetText(CCFormatNumber($items[$i]->Total_DEBITO_ATUAL, array(False, 2, ",", ".", False, "R\$ ", "", 1, True, "")), ccsFloat);
                            $this->Total_DEBITO_ATUAL->Attributes->RestoreFromArray($items[$i]->_Total_DEBITO_ATUALAttributes);
                            $this->Report_Footer->CCSEventResult = CCGetEvent($this->Report_Footer->CCSEvents, "BeforeShow", $this->Report_Footer);
                            if ($this->Report_Footer->Visible) {
                                $Tpl->block_path = $ParentPath . "/" . $ReportBlock . "/Section Report_Footer";
                                $this->NoRecords->Show();
                                $this->TotalSum_VALFAT->Show();
                                $this->TotalSum_VALPGT->Show();
                                $this->TotalSum_VALJUR->Show();
                                $this->TotalSum_ISSQN->Show();
                                $this->TotalSum_RET_INSS->Show();
                                $this->Total_DEBITO_ATUAL->Show();
                                $this->Attributes->Show();
                                $Tpl->block_path = $ParentPath . "/" . $ReportBlock;
                                $Tpl->parseto("Section Report_Footer", true, "Section Detail");
                            }
                        }
                        break;
                    case "Page":
                        if ($items[$i]->Mode == 1) {
                            $this->Page_Header->CCSEventResult = CCGetEvent($this->Page_Header->CCSEvents, "BeforeShow", $this->Page_Header);
                            if ($this->Page_Header->Visible) {
                                $Tpl->block_path = $ParentPath . "/" . $ReportBlock . "/Section Page_Header";
                                $this->Attributes->Show();
                                $this->Sorter_DESCLI->Show();
                                $this->Sorter_CGCCPF->Show();
                                $this->Sorter_CODFAT->Show();
                                $this->Sorter_MESREF->Show();
                                $this->Sorter_DATVNC->Show();
                                $this->Sorter_VALFAT->Show();
                                $this->Sorter2->Show();
                                $this->Sorter1->Show();
                                $this->Sorter_VALJUR->Show();
                                $this->Sorter_ISSQN->Show();
                                $this->Sorter_RET_INSS->Show();
                                $Tpl->block_path = $ParentPath . "/" . $ReportBlock;
                                $Tpl->parseto("Section Page_Header", true, "Section Detail");
                            }
                        }
                        if ($items[$i]->Mode == 2 && !$this->UseClientPaging || $items[$i]->Mode == 1 && $this->UseClientPaging) {
                            $this->Report_CurrentDateTime->SetValue(CCFormatDate(CCGetDateArray(), $this->Report_CurrentDateTime->Format));
                            $this->Report_CurrentDateTime->Attributes->RestoreFromArray($items[$i]->_Report_CurrentDateTimeAttributes);
                            $this->Navigator->PageNumber = $items[$i]->PageNumber;
                            $this->Navigator->TotalPages = $Groups->TotalPages;
                            $this->Navigator->Visible = ("Print" != $this->ViewMode);
                            $this->Page_Footer->CCSEventResult = CCGetEvent($this->Page_Footer->CCSEvents, "BeforeShow", $this->Page_Footer);
                            if ($this->Page_Footer->Visible) {
                                $Tpl->block_path = $ParentPath . "/" . $ReportBlock . "/Section Page_Footer";
                                $this->Report_CurrentDateTime->Show();
                                $this->Navigator->Show();
                                $this->Attributes->Show();
                                $Tpl->block_path = $ParentPath . "/" . $ReportBlock;
                                $Tpl->parseto("Section Page_Footer", true, "Section Detail");
                            }
                        }
                        break;
                }
                $i++;
            } while ($i < count($items) && ($this->ViewMode == "Print" ||  !($i > 1 && $items[$i]->GroupType == 'Page' && $items[$i]->Mode == 1)));
            $Tpl->block_path = $ParentPath;
            $Tpl->parse($ReportBlock);
            $this->DataSource->close();
        }

    }
//End Show Method

} //End CADFAT_CADCLI Class @4-FCB6E20C

class clsCADFAT_CADCLIDataSource extends clsDBFaturar {  //CADFAT_CADCLIDataSource Class @4-C44B730E

//DataSource Variables @4-F136B73D
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $wp;


    // Datasource fields
    public $DESCLI;
    public $CGCCPF;
    public $CODFAT;
    public $MESREF;
    public $DATVNC;
    public $VALFAT;
    public $VALPGT;
    public $VALMUL2;
    public $DIAS_ATRAZO;
    public $VALJUR;
    public $VALMUL;
    public $ISSQN;
    public $hdd_RET_INSS;
    public $RET_INSS;
    public $TotalSum_VALFAT;
    public $TotalSum_VALPGT;
    public $TotalSum_VALJUR;
    public $TotalSum_ISSQN;
    public $TotalSum_RET_INSS;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-FC970F0D
    function clsCADFAT_CADCLIDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Report CADFAT_CADCLI";
        $this->Initialize();
        $this->DESCLI = new clsField("DESCLI", ccsText, "");
        $this->CGCCPF = new clsField("CGCCPF", ccsText, "");
        $this->CODFAT = new clsField("CODFAT", ccsText, "");
        $this->MESREF = new clsField("MESREF", ccsText, "");
        $this->DATVNC = new clsField("DATVNC", ccsText, "");
        $this->VALFAT = new clsField("VALFAT", ccsFloat, "");
        $this->VALPGT = new clsField("VALPGT", ccsFloat, "");
        $this->VALMUL2 = new clsField("VALMUL2", ccsFloat, "");
        $this->DIAS_ATRAZO = new clsField("DIAS_ATRAZO", ccsInteger, "");
        $this->VALJUR = new clsField("VALJUR", ccsFloat, "");
        $this->VALMUL = new clsField("VALMUL", ccsFloat, "");
        $this->ISSQN = new clsField("ISSQN", ccsFloat, "");
        $this->hdd_RET_INSS = new clsField("hdd_RET_INSS", ccsFloat, "");
        $this->RET_INSS = new clsField("RET_INSS", ccsFloat, "");
        $this->TotalSum_VALFAT = new clsField("TotalSum_VALFAT", ccsFloat, "");
        $this->TotalSum_VALPGT = new clsField("TotalSum_VALPGT", ccsFloat, "");
        $this->TotalSum_VALJUR = new clsField("TotalSum_VALJUR", ccsFloat, "");
        $this->TotalSum_ISSQN = new clsField("TotalSum_ISSQN", ccsFloat, "");
        $this->TotalSum_RET_INSS = new clsField("TotalSum_RET_INSS", ccsFloat, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @4-D59EA8E4
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_DESCLI" => array("DESCLI", ""), 
            "Sorter_CGCCPF" => array("CGCCPF", ""), 
            "Sorter_CODFAT" => array("CODFAT", ""), 
            "Sorter_MESREF" => array("MESREF", ""), 
            "Sorter_DATVNC" => array("DATVNC", ""), 
            "Sorter_VALFAT" => array("VALFAT", ""), 
            "Sorter2" => array("VALPGT", ""), 
            "Sorter1" => array("VALMUL", ""), 
            "Sorter_VALJUR" => array("VALJUR", ""), 
            "Sorter_ISSQN" => array("ISSQN", ""), 
            "Sorter_RET_INSS" => array("RET_INSS", "")));
    }
//End SetOrder Method

//Prepare Method @4-2B08A55D
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_CODCLI", ccsText, "", "", $this->Parameters["urls_CODCLI"], "", false);
        $this->wp->AddParameter("2", "urls_MESREF", ccsText, "", "", $this->Parameters["urls_MESREF"], "", false);
        $this->wp->AddParameter("3", "urls_DESCLI", ccsText, "", "", $this->Parameters["urls_DESCLI"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "CADFAT.CODCLI", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opContains, "MESREF", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsText),false);
        $this->wp->Criterion[3] = $this->wp->Operation(opEqual, "CADCLI.CODCLI", $this->wp->GetDBValue("3"), $this->ToSQL($this->wp->GetDBValue("3"), ccsText),false);
        $this->Where = $this->wp->opOR(
             false, $this->wp->opOR(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]), 
             $this->wp->Criterion[3]);
        $this->Where = $this->wp->opAND(false, "( (CADCLI.CODCLI = CADFAT.CODCLI) )", $this->Where);
    }
//End Prepare Method

//Open Method @4-A0559772
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->SQL = "SELECT DESCLI, CGCCPF, CADFAT.*, CASE WHEN VALPGT=0 THEN FLOOR(SYSDATE-DATVNC) ELSE 0 END AS NDIAS, CASE WHEN VALPGT=0 THEN ((VALFAT-RET_INSS) * (0.01/30) * FLOOR(SYSDATE-DATVNC)) ELSE 0 END AS JUROS \n\n" .
        "FROM CADFAT,\n\n" .
        "CADCLI {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @4-36052F55
    function SetValues()
    {
        $this->DESCLI->SetDBValue($this->f("DESCLI"));
        $this->CGCCPF->SetDBValue($this->f("CGCCPF"));
        $this->CODFAT->SetDBValue($this->f("CODFAT"));
        $this->MESREF->SetDBValue($this->f("MESREF"));
        $this->DATVNC->SetDBValue($this->f("DATVNC"));
        $this->VALFAT->SetDBValue(trim($this->f("VALFAT")));
        $this->VALPGT->SetDBValue(trim($this->f("VALPGT")));
        $this->VALMUL2->SetDBValue(trim($this->f("VALMUL")));
        $this->DIAS_ATRAZO->SetDBValue(trim($this->f("NDIAS")));
        $this->VALJUR->SetDBValue(trim($this->f("JUROS")));
        $this->VALMUL->SetDBValue(trim($this->f("VALMUL")));
        $this->ISSQN->SetDBValue(trim($this->f("ISSQN")));
        $this->hdd_RET_INSS->SetDBValue(trim($this->f("RET_INSS")));
        $this->RET_INSS->SetDBValue(trim($this->f("RET_INSS")));
        $this->TotalSum_VALFAT->SetDBValue(trim($this->f("VALFAT")));
        $this->TotalSum_VALPGT->SetDBValue(trim($this->f("VALPGT")));
        $this->TotalSum_VALJUR->SetDBValue(trim($this->f("JUROS")));
        $this->TotalSum_ISSQN->SetDBValue(trim($this->f("ISSQN")));
        $this->TotalSum_RET_INSS->SetDBValue(trim($this->f("RET_INSS")));
    }
//End SetValues Method

} //End CADFAT_CADCLIDataSource Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation



//Initialize Page @1-BAF5635B
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "Extrato.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-946ECC7A
CCSecurityRedirect("1;2;3", "");
//End Authenticate User

//Include events file @1-88ABD7EF
include("./Extrato_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-01B89FB2
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$cadcli_CADFAT = new clsRecordcadcli_CADFAT("", $MainPage);
$CADFAT_CADCLI = new clsReportCADFAT_CADCLI("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->cadcli_CADFAT = & $cadcli_CADFAT;
$MainPage->CADFAT_CADCLI = & $CADFAT_CADCLI;
$MainPage->rodape = & $rodape;
$CADFAT_CADCLI->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-91BD17A9
$cabec->Operations();
$cadcli_CADFAT->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-83251B90
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($cadcli_CADFAT);
    unset($CADFAT_CADCLI);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-71C76D6D
$cabec->Show();
$cadcli_CADFAT->Show();
$CADFAT_CADCLI->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-BDB1E1E5
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($cadcli_CADFAT);
unset($CADFAT_CADCLI);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
