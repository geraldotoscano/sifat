<?php
//BindEvents Method @1-EED493B4
function BindEvents()
{
    global $cadcli_CADFAT;
    global $CADFAT_CADCLI;
    global $CCSEvents;
    $cadcli_CADFAT->Button_DoSearch->CCSEvents["OnClick"] = "cadcli_CADFAT_Button_DoSearch_OnClick";
    $cadcli_CADFAT->CCSEvents["BeforeShow"] = "cadcli_CADFAT_BeforeShow";
    $CADFAT_CADCLI->DESCLI->CCSEvents["BeforeShow"] = "CADFAT_CADCLI_DESCLI_BeforeShow";
    $CADFAT_CADCLI->CGCCPF->CCSEvents["BeforeShow"] = "CADFAT_CADCLI_CGCCPF_BeforeShow";
    $CADFAT_CADCLI->SimNao->CCSEvents["BeforeShow"] = "CADFAT_CADCLI_SimNao_BeforeShow";
    $CADFAT_CADCLI->DATVNC->CCSEvents["BeforeShow"] = "CADFAT_CADCLI_DATVNC_BeforeShow";
    $CADFAT_CADCLI->VALMUL2->CCSEvents["BeforeShow"] = "CADFAT_CADCLI_VALMUL2_BeforeShow";
    $CADFAT_CADCLI->DEBITO_ATUAL->CCSEvents["BeforeShow"] = "CADFAT_CADCLI_DEBITO_ATUAL_BeforeShow";
    $CADFAT_CADCLI->Total_DEBITO_ATUAL->CCSEvents["BeforeShow"] = "CADFAT_CADCLI_Total_DEBITO_ATUAL_BeforeShow";
    $CADFAT_CADCLI->ds->CCSEvents["BeforeBuildSelect"] = "CADFAT_CADCLI_ds_BeforeBuildSelect";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//cadcli_CADFAT_Button_DoSearch_OnClick @12-4FCF9FCF
function cadcli_CADFAT_Button_DoSearch_OnClick(& $sender)
{
    $cadcli_CADFAT_Button_DoSearch_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $cadcli_CADFAT; //Compatibility
//End cadcli_CADFAT_Button_DoSearch_OnClick

//Custom Code @107-2A29BDB7
// -------------------------
    $m_CodCli = $cadcli_CADFAT->s_CODCLI->GetValue();
	$m_DesCli = $cadcli_CADFAT->s_DESCLI->GetValue();
	if ($m_CodCli == "" && $m_DesCli == "")
	{
	   $cadcli_CADFAT->Errors->addError("Informe o c�digo ou o nome do cliente.");
	   $cadcli_CADFAT_Button_DoSearch_OnClick = false;
	}
	else if ($m_CodCli != "" && $m_DesCli != "")
	{
	   $cadcli_CADFAT->Errors->addError("Informe somente o c�digo ou o nome do cliente e n�o ambos.");
	   $cadcli_CADFAT_Button_DoSearch_OnClick = false;
	}
// -------------------------
//End Custom Code

//Close cadcli_CADFAT_Button_DoSearch_OnClick @12-F07AA4B7
    return $cadcli_CADFAT_Button_DoSearch_OnClick;
}
//End Close cadcli_CADFAT_Button_DoSearch_OnClick

//cadcli_CADFAT_BeforeShow @11-BBBB5DA7
function cadcli_CADFAT_BeforeShow(& $sender)
{
    $cadcli_CADFAT_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $cadcli_CADFAT; //Compatibility
//End cadcli_CADFAT_BeforeShow

//Hide-Show Component @18-286F3E6C
    $Parameter1 = CCGetFromGet("ViewMode", "");
    $Parameter2 = "Print";
    if (0 == CCCompareValues($Parameter1, $Parameter2, ccsText))
        $Component->Visible = false;
//End Hide-Show Component

//Close cadcli_CADFAT_BeforeShow @11-522C555D
    return $cadcli_CADFAT_BeforeShow;
}
//End Close cadcli_CADFAT_BeforeShow

$mDescri = "";
//CADFAT_CADCLI_DESCLI_BeforeShow @59-568B4465
function CADFAT_CADCLI_DESCLI_BeforeShow(& $sender)
{
    $CADFAT_CADCLI_DESCLI_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_DESCLI_BeforeShow
	global $mDescri;
//Custom Code @69-2A29BDB7
// -------------------------
    if ($CADFAT_CADCLI->DESCLI->GetValue() != $mDescri)
	{
		$mDescri = $CADFAT_CADCLI->DESCLI->GetValue();
		$CADFAT_CADCLI->DESCLI->Visible = true;
		$CADFAT_CADCLI->CGCCPF->Visible = true;
	}
	else
	{
		$CADFAT_CADCLI->DESCLI->Visible = false;
		$CADFAT_CADCLI->CGCCPF->Visible = false;
	}
// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_DESCLI_BeforeShow @59-6B06838A
    return $CADFAT_CADCLI_DESCLI_BeforeShow;
}
//End Close CADFAT_CADCLI_DESCLI_BeforeShow

//CADFAT_CADCLI_CGCCPF_BeforeShow @61-130A66E9
function CADFAT_CADCLI_CGCCPF_BeforeShow(& $sender)
{
    $CADFAT_CADCLI_CGCCPF_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_CGCCPF_BeforeShow

//Custom Code @76-2A29BDB7
// -------------------------
	$mCgcCpf = $CADFAT_CADCLI->CGCCPF->GetValue();
	if (strlen(trim($mCgcCpf))==14)
	{
       $mCgcCpf = substr($mCgcCpf,0,2).".".substr($mCgcCpf,2,3).".".substr($mCgcCpf,5,3).".".substr($mCgcCpf,8,3).".".substr($mCgcCpf,11,2);
    }
	else
	{
       $mCgcCpf = substr($mCgcCpf,2,3).".".substr($mCgcCpf,5,3).".".substr($mCgcCpf,8,3).".".substr($mCgcCpf,11,2);
	}
    $CADFAT_CADCLI->CGCCPF->SetValue($mCgcCpf);
// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_CGCCPF_BeforeShow @61-64BE7862
    return $CADFAT_CADCLI_CGCCPF_BeforeShow;
}
//End Close CADFAT_CADCLI_CGCCPF_BeforeShow

//CADFAT_CADCLI_SimNao_BeforeShow @52-CE002F7C
function CADFAT_CADCLI_SimNao_BeforeShow(& $sender)
{
    $CADFAT_CADCLI_SimNao_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_SimNao_BeforeShow

//Custom Code @55-2A29BDB7
// -------------------------
    if ($CADFAT_CADCLI->VALPGT->GetValue() > 0)
	{
		$CADFAT_CADCLI->SimNao->SetValue("Sim");
	}
	else
	{
		$CADFAT_CADCLI->SimNao->SetValue("N�o");
	}
// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_SimNao_BeforeShow @52-43349C45
    return $CADFAT_CADCLI_SimNao_BeforeShow;
}
//End Close CADFAT_CADCLI_SimNao_BeforeShow

//CADFAT_CADCLI_DATVNC_BeforeShow @38-B11EA928
function CADFAT_CADCLI_DATVNC_BeforeShow(& $sender)
{
    $CADFAT_CADCLI_DATVNC_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_DATVNC_BeforeShow

//Custom Code @90-2A29BDB7
// -------------------------
    $mDATVNC = $CADFAT_CADCLI->DATVNC->GetValue();
    $mDATVNC = substr($mDATVNC,0,10);
	$CADFAT_CADCLI->DATVNC->SetValue($mDATVNC);
// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_DATVNC_BeforeShow @38-A6F3A045
    return $CADFAT_CADCLI_DATVNC_BeforeShow;
}
//End Close CADFAT_CADCLI_DATVNC_BeforeShow

//CADFAT_CADCLI_VALMUL2_BeforeShow @103-1051B59F
function CADFAT_CADCLI_VALMUL2_BeforeShow(& $sender)
{
    $CADFAT_CADCLI_VALMUL2_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_VALMUL2_BeforeShow

//Custom Code @104-2A29BDB7
// -------------------------

	$mTaxa_Juros = floatval(str_replace(',','.',CCGetSession("mTAXA_JUROS")));

	$vfat  = floatval($CADFAT_CADCLI->VALFAT->GetValue());
	$mInss = floatval($CADFAT_CADCLI->RET_INSS->GetValue());
	//$mIssqn = floatval($CADFAT_CADCLI->ISSQN->GetValue());
	$mVr_Multa_dia = floatval(  round((($vfat-$mInss)*($mTaxa_Juros/100))/30 ,2  ));

	$CADFAT_CADCLI->VALMUL2->SetValue($mVr_Multa_dia);

// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_VALMUL2_BeforeShow @103-147053E8
    return $CADFAT_CADCLI_VALMUL2_BeforeShow;
}
//End Close CADFAT_CADCLI_VALMUL2_BeforeShow

//CADFAT_CADCLI_DEBITO_ATUAL_BeforeShow @93-37031B7C
function CADFAT_CADCLI_DEBITO_ATUAL_BeforeShow(& $sender)
{
    $CADFAT_CADCLI_DEBITO_ATUAL_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_DEBITO_ATUAL_BeforeShow

//Custom Code @94-2A29BDB7
// -------------------------
	$mValFat = (float)$CADFAT_CADCLI->VALFAT->GetValue();
	$mValJur = (float)$CADFAT_CADCLI->VALJUR->GetValue();
	$mINSS = (float)$CADFAT_CADCLI->RET_INSS->GetValue();
	//$mISSQN = (float)$CADFAT_CADCLI->ISSQN->GetValue();
	//$mValPgt = (float)$CADFAT_CADCLI->VALPGT->GetValue();

	$CADFAT_CADCLI->DEBITO_ATUAL->SetValue(($mValFat+$mValJur-$mINSS));
// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_DEBITO_ATUAL_BeforeShow @93-1CDCC3C0
    return $CADFAT_CADCLI_DEBITO_ATUAL_BeforeShow;
}
//End Close CADFAT_CADCLI_DEBITO_ATUAL_BeforeShow

//CADFAT_CADCLI_Total_DEBITO_ATUAL_BeforeShow @97-F198CAD1
function CADFAT_CADCLI_Total_DEBITO_ATUAL_BeforeShow(& $sender)
{
    $CADFAT_CADCLI_Total_DEBITO_ATUAL_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_Total_DEBITO_ATUAL_BeforeShow

//Custom Code @98-2A29BDB7
// -------------------------

	$sumValFat = (float)$CADFAT_CADCLI->TotalSum_VALFAT->GetValue();
	$sumValJur = (float)$CADFAT_CADCLI->TotalSum_VALJUR->GetValue();
	$sumINSS = (float)$CADFAT_CADCLI->TotalSum_RET_INSS->GetValue();
	//$sumISSQN = (float)$CADFAT_CADCLI->TotalSum_ISSQN->GetValue();
	//$sumValPgt = (float)$CADFAT_CADCLI->TotalSum_VALPGT->GetValue();
	
	$CADFAT_CADCLI->Total_DEBITO_ATUAL->SetValue($sumValFat + $sumValJur - $sumINSS);
	
// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_Total_DEBITO_ATUAL_BeforeShow @97-CBAD88CA
    return $CADFAT_CADCLI_Total_DEBITO_ATUAL_BeforeShow;
}
//End Close CADFAT_CADCLI_Total_DEBITO_ATUAL_BeforeShow

//CADFAT_CADCLI_ds_BeforeBuildSelect @4-A0FACAEB
function CADFAT_CADCLI_ds_BeforeBuildSelect(& $sender)
{
    $CADFAT_CADCLI_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_ds_BeforeBuildSelect

//Custom Code @50-2A29BDB7
// -------------------------
	global $cadcli_CADFAT;
	if ($CADFAT_CADCLI->DataSource->Where == "( (CADCLI.CODCLI = CADFAT.CODCLI) )") 
	{
 		$CADFAT_CADCLI->DataSource->Where .= " AND 1=0";
		//str_replace  ("1=0","1=1",$CADFAT_CADCLI->DataSource->Where)
	}
	//$CADFAT_CADCLI->DataSource->Where .= "1=";
	if ($cadcli_CADFAT->s_PGTO->GetValue() == 'N')
	{
 		$CADFAT_CADCLI->DataSource->Where .= " and valpgt = 0 ";
	}
	elseif ($cadcli_CADFAT->s_PGTO->GetValue() == 'P')
	{
 		$CADFAT_CADCLI->DataSource->Where .= " and valpgt <> 0 ";
	}
	$CADFAT_CADCLI->DataSource->Order = 'substr(mesref,4,4),substr(mesref,1,2)';
// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_ds_BeforeBuildSelect @4-F108017E
    return $CADFAT_CADCLI_ds_BeforeBuildSelect;
}
//End Close CADFAT_CADCLI_ds_BeforeBuildSelect

//Page_BeforeShow @1-645B127C
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Extrato; //Compatibility
//End Page_BeforeShow

//Custom Code @108-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(36);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow

?>