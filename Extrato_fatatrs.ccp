<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="11" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="cadcli_CADFAT" wizardCaption="{res:CCS_SearchFormPrefix} {res:cadcli_CADFAT} {res:CCS_SearchFormSuffix}" wizardOrientation="Vertical" wizardFormMethod="post" returnPage="Extrato_fatatrs.ccp" pasteAsReplace="pasteAsReplace">
			<Components>
				<Label id="13" visible="Yes" fieldSourceType="CodeExpression" dataType="Text" name="s_CODCLI" wizardCaption="{res:CODCLI}" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" required="False" caption="Código do Cliente" html="False">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="DLookup" actionCategory="Database" id="115" typeOfTarget="Control" expression="&quot;fatatrs.codcli&quot;" domain="&quot;fatatrs&quot;" criteria="&quot;fatatrs.codfatatrs='&quot;.CCGetParam(&quot;CODFATATRS&quot;).&quot;'&quot;" connection="Faturar" target="s_CODCLI"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="105" visible="Yes" fieldSourceType="DBColumn" sourceType="SQL" dataType="Text" returnValueType="Number" name="s_DESCLI" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="select 0 nord,to_char(null) as codcli,  to_char(null) as descli from dual

UNION


SELECT
   1 nord,CODCLI,
   DESCLI
FROM
   CADCLI

ORDER BY nord,DESCLI
" boundColumn="CODCLI" textColumn="DESCLI" html="False">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="DLookup" actionCategory="Database" id="116" typeOfTarget="Control" target="s_DESCLI" expression="&quot;cadcli.descli&quot;" domain="&quot;fatatrs, cadcli&quot;" criteria="&quot;fatatrs.codcli=cadcli.codcli and fatatrs.codfatatrs='&quot;.CCGetParam(&quot;CODFATATRS&quot;).&quot;'&quot;" connection="Faturar"/>
							</Actions>
						</Event>
					</Events>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</Label>
				<TextBox id="14" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_MESREF" wizardCaption="{res:MESREF}" wizardSize="7" wizardMaxLength="7" wizardIsPassword="False" required="False" sourceType="ListOfValues">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Validate Entry" actionCategory="Validation" id="65" inputMask="00/0000"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
				</TextBox>
				<Button id="12" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="{res:CCS_Search}">
					<Components/>
					<Events>
						<Event name="OnClick" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="64"/>
							</Actions>
						</Event>
						<Event name="OnClick" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="107"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Hide-Show Component" actionCategory="General" id="18" action="Hide" conditionType="Parameter" dataType="Text" condition="Equal" parameter1="Print" name1="ViewMode" sourceType1="URL" name2="&quot;Print&quot;" sourceType2="Expression"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Report id="4" secured="True" enablePrint="True" showMode="Web" returnValueType="Number" linesPerWebPage="40" connection="Faturar" name="CADFAT_CADCLI" pageSizeLimit="100" wizardCaption="{res:CCS_ReportFormPrefix} {res:CADFATCADCLI} {res:CCS_ReportFormSuffix}" wizardLayoutType="Tabular" activeCollection="TableParameters" linesPerPhysicalPage="66" sourceType="Table" dataSource="CADCLI, CADFAT, FATATRS_CADFAT, FATATRS">
			<Components>
				<Section id="19" visible="True" lines="1" name="Report_Header" wizardSectionType="ReportHeader">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="20" visible="True" lines="1" name="Page_Header" wizardSectionType="PageHeader" wizardAllowSorting="True">
					<Components>
						<Sorter id="58" visible="True" name="Sorter_DESCLI" wizardSortingType="SimpleDir" wizardCaption="Cliente" column="DESCLI">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="60" visible="True" name="Sorter_CGCCPF" wizardSortingType="SimpleDir" wizardCaption="CGC/CPF" column="CGCCPF">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="33" visible="True" name="Sorter_CODFAT" column="CODFAT" wizardCaption="{res:CODFAT}" wizardSortingType="SimpleDir" wizardControl="CODFAT">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="35" visible="True" name="Sorter_MESREF" column="substr(CADFAT.mesref,4,4),substr(CADFAT.mesref,1,2)" wizardCaption="{res:MESREF}" wizardSortingType="SimpleDir" wizardControl="MESREF" columnReverse="substr(CADFAT.mesref,4,4) desc,substr(CADFAT.mesref,1,2) desc">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<ReportLabel id="56" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="Pago" emptyValue="Pago ?">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Sorter id="37" visible="True" name="Sorter_DATVNC" column="CADFAT.DATVNC" wizardCaption="{res:DATVNC}" wizardSortingType="SimpleDir" wizardControl="DATVNC">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="39" visible="True" name="Sorter_VALFAT" column="CADFAT.VALFAT" wizardCaption="{res:VALFAT}" wizardSortingType="SimpleDir" wizardControl="VALFAT">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="41" visible="True" name="Sorter_VALJUR" column="TotalJurosUni" wizardCaption="{res:VALJUR}" wizardSortingType="SimpleDir" wizardControl="VALJUR">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="131" visible="True" name="Sorter_ISSQN" wizardSortingType="SimpleDir" wizardCaption="ISSQN" column="CADFAT.ISSQN">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Sorter>
<Sorter id="45" visible="True" name="Sorter_RET_INSS" column="CADFAT.RET_INSS" wizardCaption="{res:RET_INSS}" wizardSortingType="SimpleDir" wizardControl="RET_INSS">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<ReportLabel id="95" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="ReportLabel2">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="96"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Sorter id="53" visible="True" name="Sorter_VALPGT" wizardSortingType="SimpleDir" wizardCaption="Valor Pago" column="CADFAT.VALPGT">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="24" visible="True" lines="1" name="Page_Footer" wizardSectionType="PageFooter" pageBreakAfter="True" pasteAsReplace="pasteAsReplace">
					<Components>
						<ReportLabel id="25" fieldSourceType="SpecialValue" dataType="Date" html="False" hideDuplicates="False" resetAt="Report" name="Report_CurrentDateTime" fieldSource="CurrentDateTime" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardInsertToDateTD="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Navigator id="26" size="10" name="Navigator" wizardPagingType="Custom" wizardFirst="True" wizardFirstText="{res:CCS_First}" wizardPrev="True" wizardPrevText="{res:CCS_Previous}" wizardNext="True" wizardNextText="{res:CCS_Next}" wizardLast="True" wizardLastText="{res:CCS_Last}" wizardImages="Images" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="True" wizardOfText="{res:CCS_Of}" wizardImagesScheme="Apricot" type="Simple">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Navigator>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="21" visible="True" lines="1" name="Detail" pasteAsReplace="pasteAsReplace">
					<Components>
						<ReportLabel id="59" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="DESCLI" fieldSource="DESCLI">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="69" eventType="Server"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="61" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="CGCCPF" fieldSource="CGCCPF">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="76" eventType="Server"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Link id="34" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="CODFAT" fieldSource="CODFAT" wizardCaption="CODFAT" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="None" hrefSource="PagtoFatu_Dados.ccp">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
							<LinkParameters>
								<LinkParameter id="109" sourceType="Expression" name="opcao" source="&quot;'1'&quot;"/><LinkParameter id="110" sourceType="DataField" name="CODFAT" source="CODFAT"/>
							</LinkParameters>
						</Link>
						<ReportLabel id="36" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="MESREF" fieldSource="MESREF" wizardCaption="MESREF" wizardSize="7" wizardMaxLength="7" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="52" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="SimNao">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="55" eventType="Server"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="38" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="DATVNC" wizardCaption="DATVNC" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" fieldSource="CADFAT.DATVNC">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="90"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="40" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="VALFAT" fieldSource="CADFAT.VALFAT" wizardCaption="VALFAT" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" format="R$ #.##0,0;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="42" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="VALJUR" wizardCaption="VALJUR" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" fieldSource="TotalJurosUni" format="R$ #0.##0,0;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Hidden id="89" fieldSourceType="DBColumn" dataType="Float" name="VALMUL" fieldSource="VALMUL" html="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Hidden>
						<ReportLabel id="129" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="ISSQN" fieldSource="ISSQN" format="R$ #0.##0,0;;;;,;.;">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Hidden id="91" fieldSourceType="DBColumn" dataType="Float" name="hdd_RET_INSS" fieldSource="RET_INSS">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Hidden>
						<ReportLabel id="46" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="RET_INSS" fieldSource="RET_INSS" wizardCaption="RET_INSS" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" format="R$ #.##0,0;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="93" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="ReportLabel1" format="R$ #.##0,0;;;;,;.;">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="94"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="54" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="VALPGT" fieldSource="VALPGT" format="R$ #0.##0,0;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="22" visible="True" lines="1" name="Report_Footer" wizardSectionType="ReportFooter">
					<Components>
						<Panel id="23" visible="True" name="NoRecords" wizardNoRecords="{res:CCS_NoRecords}">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Panel>
						<ReportLabel id="27" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_VALFAT" fieldSource="CADFAT.VALFAT" summarised="True" function="Sum" wizardCaption="{res:VALFAT}" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" format="R$ #.##0,0;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="28" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_VALJUR" fieldSource="TotalJurosUni" summarised="True" function="Sum" wizardCaption="{res:VALJUR}" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" format="R$ #0.##0,0;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="130" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_ISSQN" fieldSource="ISSQN" format="R$ #0.##0,0;;;;,;.;" function="Sum">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="30" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_RET_INSS" fieldSource="RET_INSS" summarised="True" function="Sum" wizardCaption="{res:RET_INSS}" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" format="R$ #0.##0,0;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="97" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="ReportLabel3" format="R$ #.##0,0;;;;,;.;" function="Sum">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="128"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="57" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_VALPGT" fieldSource="VALPGT" function="Sum" format="R$ #0.##0,0;;;;,;.;">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
			</Components>
			<Events>
			</Events>
			<TableParameters>
				<TableParameter id="114" conditionType="Parameter" useIsNull="True" field="FATATRS_CADFAT.CODFATATRS" dataType="Text" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="CODFATATRS"/>
				<TableParameter id="32" conditionType="Parameter" useIsNull="False" field="CADFAT.MESREF" dataType="Text" logicOperator="Or" searchConditionType="Contains" parameterType="URL" orderNumber="2" parameterSource="s_MESREF"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="117" tableName="CADCLI" schemaName="SL005V3" posLeft="10" posTop="10" posWidth="115" posHeight="434"/>
				<JoinTable id="118" tableName="CADFAT" schemaName="SL005V3" posLeft="327" posTop="81" posWidth="115" posHeight="299"/>
				<JoinTable id="119" tableName="FATATRS_CADFAT" schemaName="SL005V3" posLeft="590" posTop="40" posWidth="98" posHeight="88"/>
				<JoinTable id="122" tableName="FATATRS" schemaName="SL005V3" posLeft="769" posTop="148" posWidth="115" posHeight="180"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="120" tableLeft="CADCLI" tableRight="CADFAT" fieldLeft="CADCLI.CODCLI" fieldRight="CADFAT.CODCLI" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="121" tableLeft="CADFAT" tableRight="FATATRS_CADFAT" fieldLeft="CADFAT.CODFAT" fieldRight="FATATRS_CADFAT.CODFAT" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="123" tableLeft="FATATRS_CADFAT" tableRight="FATATRS" fieldLeft="FATATRS_CADFAT.CODFATATRS" fieldRight="FATATRS.CODFATATRS" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="124" tableName="CADCLI" fieldName="CADCLI.*"/>
				<Field id="125" fieldName="round(((CADFAT.VALFAT-CADFAT.RET_INSS)*(1/100/30))*(( fatatrs.datvnc   -CADFAT.DATVNC)-1),2)" isExpression="True" alias="TotalJurosUni"/>
				<Field id="127" tableName="CADFAT" fieldName="CADFAT.*"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<ReportGroups>
			</ReportGroups>
			<SecurityGroups>
				<Group id="47" groupID="1" read="True"/>
				<Group id="48" groupID="2" read="True"/>
			</SecurityGroups>
			<Attributes>
				<Attribute id="49" name="um" sourceType="Expression" source="dois"/>
			</Attributes>
			<Features/>
		</Report>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="PHPTemplates" name="Extrato_fatatrs_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Code" language="PHPTemplates" name="Extrato_fatatrs.php" forShow="True" url="Extrato_fatatrs.php" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="73" groupID="1"/>
		<Group id="74" groupID="2"/>
		<Group id="75" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="108"/>
			</Actions>
		</Event>
	</Events>
</Page>
