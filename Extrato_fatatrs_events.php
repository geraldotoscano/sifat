<?php
//BindEvents Method @1-EAF64AC3
function BindEvents()
{
    global $cadcli_CADFAT;
    global $CADFAT_CADCLI;
    global $CCSEvents;
    $cadcli_CADFAT->s_CODCLI->CCSEvents["BeforeShow"] = "cadcli_CADFAT_s_CODCLI_BeforeShow";
    $cadcli_CADFAT->s_DESCLI->CCSEvents["BeforeShow"] = "cadcli_CADFAT_s_DESCLI_BeforeShow";
    $cadcli_CADFAT->Button_DoSearch->CCSEvents["OnClick"] = "cadcli_CADFAT_Button_DoSearch_OnClick";
    $cadcli_CADFAT->CCSEvents["BeforeShow"] = "cadcli_CADFAT_BeforeShow";
    $CADFAT_CADCLI->ReportLabel2->CCSEvents["BeforeShow"] = "CADFAT_CADCLI_ReportLabel2_BeforeShow";
    $CADFAT_CADCLI->DESCLI->CCSEvents["BeforeShow"] = "CADFAT_CADCLI_DESCLI_BeforeShow";
    $CADFAT_CADCLI->CGCCPF->CCSEvents["BeforeShow"] = "CADFAT_CADCLI_CGCCPF_BeforeShow";
    $CADFAT_CADCLI->SimNao->CCSEvents["BeforeShow"] = "CADFAT_CADCLI_SimNao_BeforeShow";
    $CADFAT_CADCLI->DATVNC->CCSEvents["BeforeShow"] = "CADFAT_CADCLI_DATVNC_BeforeShow";
    $CADFAT_CADCLI->ReportLabel1->CCSEvents["BeforeShow"] = "CADFAT_CADCLI_ReportLabel1_BeforeShow";
    $CADFAT_CADCLI->ReportLabel3->CCSEvents["BeforeShow"] = "CADFAT_CADCLI_ReportLabel3_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//cadcli_CADFAT_s_CODCLI_BeforeShow @13-D9FACF25
function cadcli_CADFAT_s_CODCLI_BeforeShow(& $sender)
{
    $cadcli_CADFAT_s_CODCLI_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $cadcli_CADFAT; //Compatibility
//End cadcli_CADFAT_s_CODCLI_BeforeShow

//DLookup @115-824CE44B
    global $DBFaturar;
    $Page = CCGetParentPage($sender);
    $ccs_result = CCDLookUp("fatatrs.codcli", "fatatrs", "fatatrs.codfatatrs='".CCGetParam("CODFATATRS")."'", $Page->Connections["Faturar"]);
    $Container->s_CODCLI->SetValue($ccs_result);
//End DLookup

//Close cadcli_CADFAT_s_CODCLI_BeforeShow @13-C9007447
    return $cadcli_CADFAT_s_CODCLI_BeforeShow;
}
//End Close cadcli_CADFAT_s_CODCLI_BeforeShow

//cadcli_CADFAT_s_DESCLI_BeforeShow @105-20A58DFA
function cadcli_CADFAT_s_DESCLI_BeforeShow(& $sender)
{
    $cadcli_CADFAT_s_DESCLI_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $cadcli_CADFAT; //Compatibility
//End cadcli_CADFAT_s_DESCLI_BeforeShow

//DLookup @116-4F651778
    global $DBFaturar;
    $Page = CCGetParentPage($sender);
    $ccs_result = CCDLookUp("cadcli.descli", "fatatrs, cadcli", "fatatrs.codcli=cadcli.codcli and fatatrs.codfatatrs='".CCGetParam("CODFATATRS")."'", $Page->Connections["Faturar"]);
    $Container->s_DESCLI->SetValue($ccs_result);
//End DLookup

//Close cadcli_CADFAT_s_DESCLI_BeforeShow @105-A678DFB5
    return $cadcli_CADFAT_s_DESCLI_BeforeShow;
}
//End Close cadcli_CADFAT_s_DESCLI_BeforeShow

//cadcli_CADFAT_Button_DoSearch_OnClick @12-4FCF9FCF
function cadcli_CADFAT_Button_DoSearch_OnClick(& $sender)
{
    $cadcli_CADFAT_Button_DoSearch_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $cadcli_CADFAT; //Compatibility
//End cadcli_CADFAT_Button_DoSearch_OnClick

//Custom Code @107-2A29BDB7
// -------------------------
    $m_CodCli = $cadcli_CADFAT->s_CODCLI->GetValue();
	$m_DesCli = $cadcli_CADFAT->s_DESCLI->GetValue();
	
	if ($m_CodCli == "" && $m_DesCli == "")
	{
	   $cadcli_CADFAT->Errors->addError("Informe o c�digo ou o nome do cliente.");
	   $cadcli_CADFAT_Button_DoSearch_OnClick = false;
	}
	else if ($m_CodCli != "" && $m_DesCli != "")
	{
	   $cadcli_CADFAT->Errors->addError("Informe somente o c�digo ou o nome do cliente e n�o ambos.");
	   $cadcli_CADFAT_Button_DoSearch_OnClick = false;
	}
// -------------------------
//End Custom Code

//Close cadcli_CADFAT_Button_DoSearch_OnClick @12-F07AA4B7
    return $cadcli_CADFAT_Button_DoSearch_OnClick;
}
//End Close cadcli_CADFAT_Button_DoSearch_OnClick

//cadcli_CADFAT_BeforeShow @11-BBBB5DA7
function cadcli_CADFAT_BeforeShow(& $sender)
{
    $cadcli_CADFAT_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $cadcli_CADFAT; //Compatibility
//End cadcli_CADFAT_BeforeShow

//Hide-Show Component @18-286F3E6C
    $Parameter1 = CCGetFromGet("ViewMode", "");
    $Parameter2 = "Print";
    if (0 == CCCompareValues($Parameter1, $Parameter2, ccsText))
        $Component->Visible = false;
//End Hide-Show Component

//Close cadcli_CADFAT_BeforeShow @11-522C555D
    return $cadcli_CADFAT_BeforeShow;
}
//End Close cadcli_CADFAT_BeforeShow

//CADFAT_CADCLI_ReportLabel2_BeforeShow @95-F7971EE2
function CADFAT_CADCLI_ReportLabel2_BeforeShow(& $sender)
{
    $CADFAT_CADCLI_ReportLabel2_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_ReportLabel2_BeforeShow

//Custom Code @96-2A29BDB7
// -------------------------
    // Write your own code here.Inserido
// -------------------------
//End Custom Code

//C-2A29BDB7
// -------------------------
    $CADFAT_CADCLI->ReportLabel2->SetValue("D�bito Atual");
// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_ReportLabel2_BeforeShow @95-BBA751EC
    return $CADFAT_CADCLI_ReportLabel2_BeforeShow;
}
//End Close CADFAT_CADCLI_ReportLabel2_BeforeShow
$mDescri = "";
//CADFAT_CADCLI_DESCLI_BeforeShow @59-568B4465
function CADFAT_CADCLI_DESCLI_BeforeShow(& $sender)
{
    $CADFAT_CADCLI_DESCLI_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_DESCLI_BeforeShow
	global $mDescri;
//Custom Code @69-2A29BDB7
// -------------------------
    if ($CADFAT_CADCLI->DESCLI->GetValue() != $mDescri)
	{
		$mDescri = $CADFAT_CADCLI->DESCLI->GetValue();
		$CADFAT_CADCLI->DESCLI->Visible = true;
		$CADFAT_CADCLI->CGCCPF->Visible = true;
	}
	else
	{
		$CADFAT_CADCLI->DESCLI->Visible = false;
		$CADFAT_CADCLI->CGCCPF->Visible = false;
	}
// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_DESCLI_BeforeShow @59-6B06838A
    return $CADFAT_CADCLI_DESCLI_BeforeShow;
}
//End Close CADFAT_CADCLI_DESCLI_BeforeShow

//CADFAT_CADCLI_CGCCPF_BeforeShow @61-130A66E9
function CADFAT_CADCLI_CGCCPF_BeforeShow(& $sender)
{
    $CADFAT_CADCLI_CGCCPF_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_CGCCPF_BeforeShow

//Custom Code @76-2A29BDB7
// -------------------------
	$mCgcCpf = $CADFAT_CADCLI->CGCCPF->GetValue();
	if (strlen(trim($mCgcCpf))==14)
	{
       $mCgcCpf = substr($mCgcCpf,0,2).".".substr($mCgcCpf,2,3).".".substr($mCgcCpf,5,3).".".substr($mCgcCpf,8,3).".".substr($mCgcCpf,11,2);
    }
	else
	{
       $mCgcCpf = substr($mCgcCpf,2,3).".".substr($mCgcCpf,5,3).".".substr($mCgcCpf,8,3).".".substr($mCgcCpf,11,2);
	}
    $CADFAT_CADCLI->CGCCPF->SetValue($mCgcCpf);

// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_CGCCPF_BeforeShow @61-64BE7862
    return $CADFAT_CADCLI_CGCCPF_BeforeShow;
}
//End Close CADFAT_CADCLI_CGCCPF_BeforeShow

//CADFAT_CADCLI_SimNao_BeforeShow @52-CE002F7C
function CADFAT_CADCLI_SimNao_BeforeShow(& $sender)
{
    $CADFAT_CADCLI_SimNao_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_SimNao_BeforeShow

//Custom Code @55-2A29BDB7
// -------------------------
    if ($CADFAT_CADCLI->VALPGT->GetValue() > 0)
	{
		$CADFAT_CADCLI->SimNao->SetValue("Sim");
	}
	else
	{
		$CADFAT_CADCLI->SimNao->SetValue("N�o");
	}
// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_SimNao_BeforeShow @52-43349C45
    return $CADFAT_CADCLI_SimNao_BeforeShow;
}
//End Close CADFAT_CADCLI_SimNao_BeforeShow

//CADFAT_CADCLI_DATVNC_BeforeShow @38-B11EA928
function CADFAT_CADCLI_DATVNC_BeforeShow(& $sender)
{
    $CADFAT_CADCLI_DATVNC_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_DATVNC_BeforeShow

//Custom Code @90-2A29BDB7
// -------------------------
    $mDATVNC = $CADFAT_CADCLI->DATVNC->GetValue();
    $mDATVNC = substr($mDATVNC,0,10);
	$CADFAT_CADCLI->DATVNC->SetValue($mDATVNC);
// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_DATVNC_BeforeShow @38-A6F3A045
    return $CADFAT_CADCLI_DATVNC_BeforeShow;
}
//End Close CADFAT_CADCLI_DATVNC_BeforeShow

//DEL  // -------------------------
//DEL      //number_format(, 2,',','.')
//DEL  	$mVALFAT = $CADFAT_CADCLI->VALFAT->GetValue();
//DEL  	$mVALFAT = number_format($mVALFAT, 2,',','.');
//DEL  	$CADFAT_CADCLI->VALFAT->SetValue($mVALFAT);
//DEL  // -------------------------

//DEL  // -------------------------
//DEL  	$mVALPGT = $CADFAT_CADCLI->VALPGT->GetValue();
//DEL  	$mVALPGT = number_format($mVALPGT, 2,',','.');
//DEL  	$CADFAT_CADCLI->VALPGT->SetValue($mVALPGT);
//DEL  // -------------------------

//DEL  // -------------------------
//DEL  	$mVALJUR = $CADFAT_CADCLI->VALMUL2->GetValue();
//DEL  	$mVALJUR = number_format($mVALJUR, 2,',','.');
//DEL  	$CADFAT_CADCLI->VALMUL2->SetValue($mVALJUR);
//DEL  // -------------------------

//DEL  // -------------------------
//DEL  /*
//DEL  	global $cadcli_CADFAT;
//DEL  	global $mTot_Jur_NP;
//DEL  	if ($cadcli_CADFAT->s_PGTO->GetValue() == 'N')
//DEL  	{
//DEL      	$mVenc = substr($CADFAT_CADCLI->DATVNC->GetValue(),0,10);
//DEL     		$Tab_Num_Dias = new clsDBfaturar();
//DEL     		$Tab_Num_Dias->query("SELECT floor(sysdate - to_date('$mVenc')) as mDias FROM dual");
//DEL     		$Tab_Num_Dias->next_record();
//DEL  		//CCSetSession("DataSist", $Tab_Dat_Hor_MesRef->f("DataSist"));
//DEL  		$CADFAT_CADCLI->DIAS_ATRAZO->SetValue($Tab_Num_Dias->f("mDias"));
//DEL  	}
//DEL  */
//DEL  // -------------------------

$mTot_Jur_NP = 0; // Juros n�o pagos

//DEL  // -------------------------
//DEL  
//DEL  	global $cadcli_CADFAT;
//DEL  	global $mTot_Jur_NP;
//DEL  
//DEL  
//DEL  	$mVALJUR = $CADFAT_CADCLI->VALJUR->GetValue();
//DEL  	$mVALJUR = number_format($mVALJUR, 2,',','.');
//DEL  	$CADFAT_CADCLI->VALJUR->SetValue($mVALJUR);
//DEL  	$mTot_Jur_NP+=$mVALJUR;
//DEL  // -------------------------

//DEL  // -------------------------
//DEL  	$mISSQN = $CADFAT_CADCLI->ISSQN->GetValue();
//DEL  	$mISSQN = number_format($mISSQN, 2,',','.');
//DEL  	$CADFAT_CADCLI->ISSQN->SetValue($mISSQN);
//DEL  // -------------------------

//DEL  // -------------------------
//DEL  	$mRET_INSS = $CADFAT_CADCLI->RET_INSS->GetValue();
//DEL  	$mRET_INSS = number_format($mRET_INSS, 2,',','.');
//DEL  	$CADFAT_CADCLI->RET_INSS->SetValue($mRET_INSS);
//DEL  // -------------------------

$mTotGer = 0;
//CADFAT_CADCLI_ReportLabel1_BeforeShow @93-C616179E
function CADFAT_CADCLI_ReportLabel1_BeforeShow(& $sender)
{
    $CADFAT_CADCLI_ReportLabel1_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_ReportLabel1_BeforeShow

//Custom Code @94-2A29BDB7
// -------------------------
	global $mTotGer;
	$mFat = str_replace(",", ";", $CADFAT_CADCLI->VALFAT->GetValue()."");
	$mFat = str_replace(".", "", $mFat);
	$mFat = str_replace(";", ".", $mFat);

	$mFat = $CADFAT_CADCLI->VALFAT->GetValue();

	$mJur = str_replace(",", ";", $CADFAT_CADCLI->VALJUR->GetValue()."");
	$mJur = str_replace(".", "", $mJur);
	$mJur = str_replace(";", ".", $mJur);

	$mJur = $CADFAT_CADCLI->VALJUR->GetValue();

	$mSS = str_replace(",", ";", $CADFAT_CADCLI->RET_INSS->GetValue()."");
	$mSS = str_replace(".", "", $mSS);
	$mSS = str_replace(";", ".", $mSS);

	$mSS = $CADFAT_CADCLI->RET_INSS->GetValue();

	$mQN = str_replace(",", ";", $CADFAT_CADCLI->ISSQN->GetValue());
	$mQN = str_replace(".", "", $mQN);
	$mQN = str_replace(";", ".", $mQN);

	$mQN = $CADFAT_CADCLI->ISSQN->GetValue();




	$mValFat = (float)$mFat;
	$mValJur = (float)$mJur;
	$mINSS = (float)$mSS;
	$mISSQN = (float)$mQN;

    $CADFAT_CADCLI->ReportLabel1->SetValue($mValFat+$mValJur-($mINSS+$mISSQN));

//	$mTotalSum_VALPGT = $CADFAT_CADCLI->ReportLabel1->GetValue();
//	$mTotalSum_VALPGT = number_format($mTotalSum_VALPGT, 2,',','.');
//	$CADFAT_CADCLI->ReportLabel1->SetValue($mTotalSum_VALPGT);

//	$mReportLabel3 = str_replace(",", ";", $CADFAT_CADCLI->ReportLabel3->GetValue());
//	$mReportLabel3 = str_replace(".", "", $mReportLabel3);
//	$mReportLabel3 = str_replace(";", ".", $mReportLabel3);

	
	$mTotGer += ($mValFat+$mValJur-($mINSS+$mISSQN));
	//$CADFAT_CADCLI->ReportLabel3->SetValue($mTotGer);
// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_ReportLabel1_BeforeShow @93-C7C67437
    return $CADFAT_CADCLI_ReportLabel1_BeforeShow;
}
//End Close CADFAT_CADCLI_ReportLabel1_BeforeShow

//DEL  // -------------------------
//DEL  //	$mTotalSum_VALFAT = $CADFAT_CADCLI->TotalSum_VALFAT->GetValue();
//DEL  //	$mTotalSum_VALFAT = number_format($mTotalSum_VALFAT, 2,',','.');
//DEL  //	$CADFAT_CADCLI->TotalSum_VALFAT->SetValue($mTotalSum_VALFAT);
//DEL  // -------------------------

//DEL  // -------------------------
//DEL  	$mTotalSum_VALPGT = $CADFAT_CADCLI->TotalSum_VALPGT->GetValue();
//DEL  	$mTotalSum_VALPGT = number_format($mTotalSum_VALPGT, 2,',','.');
//DEL  	$CADFAT_CADCLI->TotalSum_VALPGT->SetValue($mTotalSum_VALPGT);
//DEL  // -------------------------

//DEL  // -------------------------
//DEL  	global $cadcli_CADFAT;
//DEL  	global $mTot_Jur_NP;
//DEL  	
//DEL  /*
//DEL  	if ($cadcli_CADFAT->s_PGTO->GetValue() == 'N' )
//DEL  	{
//DEL  		$mTotalSum_VALPGT = $mTot_Jur_NP;
//DEL  	}
//DEL  	else
//DEL  	{
//DEL  		$mTotalSum_VALPGT = $CADFAT_CADCLI->TotalSum_VALJUR->GetValue();
//DEL  	}
//DEL  */
//DEL  //	$mTotalSum_VALPGT = number_format($mTotalSum_VALPGT, 2,',','.');
//DEL  //	$CADFAT_CADCLI->TotalSum_VALJUR->SetValue($mTotalSum_VALPGT);
//DEL  	$CADFAT_CADCLI->TotalSum_VALJUR->SetValue($mTot_Jur_NP);
//DEL  // -------------------------

//DEL  // -------------------------
//DEL  	$mTotalSum_VALPGT = $CADFAT_CADCLI->TotalSum_ISSQN->GetValue();
//DEL  	$mTotalSum_VALPGT = number_format($mTotalSum_VALPGT, 2,',','.');
//DEL  	$CADFAT_CADCLI->TotalSum_ISSQN->SetValue($mTotalSum_VALPGT);
//DEL  // -------------------------

//DEL  // -------------------------
//DEL  	$mTotalSum_VALPGT = $CADFAT_CADCLI->TotalSum_RET_INSS->GetValue();
//DEL  	$mTotalSum_VALPGT = number_format($mTotalSum_VALPGT, 2,',','.');
//DEL  	$CADFAT_CADCLI->TotalSum_RET_INSS->SetValue($mTotalSum_VALPGT);
//DEL  // -------------------------

//CADFAT_CADCLI_ReportLabel3_BeforeShow @97-E717E636
function CADFAT_CADCLI_ReportLabel3_BeforeShow(& $sender)
{
    $CADFAT_CADCLI_ReportLabel3_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_ReportLabel3_BeforeShow

//Custom Code @128-2A29BDB7
// -------------------------
  global $mTotGer;

  $sender->SetValue($mTotGer);

// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_ReportLabel3_BeforeShow @97-26A8B09A
    return $CADFAT_CADCLI_ReportLabel3_BeforeShow;
}
//End Close CADFAT_CADCLI_ReportLabel3_BeforeShow

	global $mTotGer;
//DEL  // -------------------------
//DEL  	$mTotalSum_VALPGT = $mTotGer;
//DEL  	$mTotalSum_VALPGT = number_format($mTotalSum_VALPGT, 2,',','.');
//DEL  	$CADFAT_CADCLI->ReportLabel3->SetValue($mTotalSum_VALPGT);
//DEL  // -------------------------

//DEL  // -------------------------
//DEL  // -------------------------

//Page_BeforeShow @1-B0092100
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Extrato_fatatrs; //Compatibility
//End Page_BeforeShow

//Custom Code @108-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(36);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");


// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
