<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0" wizardSortingType="SimpleDir">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="12" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="cadcli_CADFAT" wizardCaption=" Cadcli CADFAT " wizardOrientation="Vertical" wizardFormMethod="post" returnPage="FafSemBol.ccp" pasteAsReplace="pasteAsReplace">
			<Components>
				<TextBox id="14" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_cadcli_CODCLI" wizardCaption="CODCLI" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="15" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_DESCLI" wizardCaption="DESCLI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="16" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_CODFAT" wizardCaption="CODFAT" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="17" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_MESREF" wizardCaption="MESREF" wizardSize="7" wizardMaxLength="7" wizardIsPassword="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<RadioButton id="37" visible="Yes" fieldSourceType="CodeExpression" sourceType="ListOfValues" dataType="Text" html="True" returnValueType="Number" name="s_PGTO" connection="Faturar" _valueOfList="T" _nameOfList="Todas" dataSource="N;Não pagas;P;Pagas">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</RadioButton>
				<Button id="13" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="4" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="100" connection="Faturar" dataSource="CADFAT, CADCLI" name="cadcli_CADFAT1" pageSizeLimit="100" wizardCaption="List of Cadcli, CADFAT " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="Não encontrado." activeCollection="TableParameters" wizardAllowSorting="True" orderBy="MESREF">
			<Components>
				<Sorter id="40" visible="True" name="Sorter_CODCLI" wizardSortingType="SimpleDir" wizardCaption="CODCLI" column="CADCLI.CODCLI">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="22" visible="True" name="Sorter_DESCLI" column="DESCLI" wizardCaption="DESCLI" wizardSortingType="SimpleDir" wizardControl="DESCLI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="23" visible="True" name="Sorter_CGCCPF" column="CGCCPF" wizardCaption="CGCCPF" wizardSortingType="SimpleDir" wizardControl="CGCCPF" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="24" visible="True" name="Sorter_CODFAT" column="CODFAT" wizardCaption="CODFAT" wizardSortingType="SimpleDir" wizardControl="CODFAT" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="25" visible="True" name="Sorter_MESREF" column="MESREF" wizardCaption="MESREF" wizardSortingType="SimpleDir" wizardControl="MESREF" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="26" visible="True" name="Sorter_VALFAT" column="VALFAT" wizardCaption="VALFAT" wizardSortingType="SimpleDir" wizardControl="VALFAT" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Label id="39" fieldSourceType="DBColumn" dataType="Text" html="False" name="CODCLI" fieldSource="CODCLI">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="27" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESCLI" fieldSource="DESCLI" wizardCaption="DESCLI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="28" fieldSourceType="DBColumn" dataType="Text" html="False" name="CGCCPF" fieldSource="CGCCPF" wizardCaption="CGCCPF" wizardSize="14" wizardMaxLength="14" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="29" fieldSourceType="DBColumn" dataType="Text" html="False" name="CODFAT" fieldSource="CODFAT" wizardCaption="CODFAT" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="30" fieldSourceType="DBColumn" dataType="Text" html="False" name="MESREF" fieldSource="MESREF" wizardCaption="MESREF" wizardSize="7" wizardMaxLength="7" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="31" fieldSourceType="DBColumn" dataType="Float" html="False" name="VALFAT" fieldSource="VALFAT" wizardCaption="VALFAT" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" format="#,##0.00;;;;,;.;">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Link id="34" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="None" name="Link1" fieldSource="M" hrefSource="fatura_avulsa.php">
					<Components/>
					<Events/>
					<LinkParameters>
						<LinkParameter id="35" sourceType="DataField" format="yyyy-mm-dd" name="codfat" source="CODFAT"/>
						<LinkParameter id="36" sourceType="Expression" format="yyyy-mm-dd" name="download" source="1"/>
					</LinkParameters>
					<Attributes/>
					<Features/>
				</Link>
				<ReportLabel id="41" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_VALFAT" fieldSource="VALFAT" summarised="True" function="Sum" wizardCaption="{res:VALFAT}" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="{res:Sum}" wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" format="R$ #,##0.00;;;;,;.;">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
<Actions>
<Action actionName="Custom Code" actionCategory="General" id="42"/>
</Actions>
</Event>
</Events>
					<Attributes/>
					<Features/>
				</ReportLabel>
<Navigator id="32" size="10" type="Centered" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="of" wizardImagesScheme="Blueprint">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events>
				<Event name="BeforeBuildSelect" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="38"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters>
				<TableParameter id="18" conditionType="Parameter" useIsNull="False" field="cadcli.CODCLI" parameterSource="s_cadcli_CODCLI" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="1"/>
				<TableParameter id="19" conditionType="Parameter" useIsNull="False" field="cadcli.DESCLI" parameterSource="s_DESCLI" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="2"/>
				<TableParameter id="20" conditionType="Parameter" useIsNull="False" field="CADFAT.CODFAT" parameterSource="s_CODFAT" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="3"/>
				<TableParameter id="21" conditionType="Parameter" useIsNull="False" field="CADFAT.MESREF" parameterSource="s_MESREF" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="4"/>
				<TableParameter id="33" conditionType="Expression" useIsNull="False" searchConditionType="Equal" parameterType="URL" logicOperator="And" expression="CADFAT.GERBLTO='N'"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="5" tableName="CADFAT" schemaName="SL005V3" posLeft="10" posTop="10" posWidth="115" posHeight="467"/>
				<JoinTable id="6" tableName="CADCLI" schemaName="SL005V3" posLeft="396" posTop="26" posWidth="140" posHeight="326"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="7" tableLeft="CADCLI" tableRight="CADFAT" fieldLeft="CADCLI.CODCLI" fieldRight="CADFAT.CODCLI" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="8" tableName="CADFAT" fieldName="CADFAT.*" isExpression="False"/>
				<Field id="9" tableName="CADCLI" fieldName="CADCLI.*" isExpression="False"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="FafSemBol.php" forShow="True" url="FafSemBol.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="FafSemBol_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
