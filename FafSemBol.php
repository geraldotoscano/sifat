<?php
//Include Common Files @1-087332FE
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "FafSemBol.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordcadcli_CADFAT { //cadcli_CADFAT Class @12-0A14EA5E

//Variables @12-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @12-DC82DD5F
    function clsRecordcadcli_CADFAT($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record cadcli_CADFAT/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "cadcli_CADFAT";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_cadcli_CODCLI = new clsControl(ccsTextBox, "s_cadcli_CODCLI", "s_cadcli_CODCLI", ccsText, "", CCGetRequestParam("s_cadcli_CODCLI", $Method, NULL), $this);
            $this->s_DESCLI = new clsControl(ccsTextBox, "s_DESCLI", "s_DESCLI", ccsText, "", CCGetRequestParam("s_DESCLI", $Method, NULL), $this);
            $this->s_CODFAT = new clsControl(ccsTextBox, "s_CODFAT", "s_CODFAT", ccsText, "", CCGetRequestParam("s_CODFAT", $Method, NULL), $this);
            $this->s_MESREF = new clsControl(ccsTextBox, "s_MESREF", "s_MESREF", ccsText, "", CCGetRequestParam("s_MESREF", $Method, NULL), $this);
            $this->s_PGTO = new clsControl(ccsRadioButton, "s_PGTO", "s_PGTO", ccsText, "", CCGetRequestParam("s_PGTO", $Method, NULL), $this);
            $this->s_PGTO->DSType = dsListOfValues;
            $this->s_PGTO->Values = array(array("N", "N�o pagas"), array("P", "Pagas"));
            $this->s_PGTO->HTML = true;
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
        }
    }
//End Class_Initialize Event

//Validate Method @12-981E7B06
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_cadcli_CODCLI->Validate() && $Validation);
        $Validation = ($this->s_DESCLI->Validate() && $Validation);
        $Validation = ($this->s_CODFAT->Validate() && $Validation);
        $Validation = ($this->s_MESREF->Validate() && $Validation);
        $Validation = ($this->s_PGTO->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->s_cadcli_CODCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_DESCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_CODFAT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_MESREF->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_PGTO->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @12-92995D8B
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_cadcli_CODCLI->Errors->Count());
        $errors = ($errors || $this->s_DESCLI->Errors->Count());
        $errors = ($errors || $this->s_CODFAT->Errors->Count());
        $errors = ($errors || $this->s_MESREF->Errors->Count());
        $errors = ($errors || $this->s_PGTO->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @12-08E0C1E5
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "FafSemBol.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                $Redirect = "FafSemBol.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @12-7703DEF6
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->s_PGTO->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->s_cadcli_CODCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_DESCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_CODFAT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_MESREF->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_PGTO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_cadcli_CODCLI->Show();
        $this->s_DESCLI->Show();
        $this->s_CODFAT->Show();
        $this->s_MESREF->Show();
        $this->s_PGTO->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End cadcli_CADFAT Class @12-FCB6E20C

class clsGridcadcli_CADFAT1 { //cadcli_CADFAT1 class @4-9F53944B

//Variables @4-AD4FB540

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_CODCLI;
    public $Sorter_DESCLI;
    public $Sorter_CGCCPF;
    public $Sorter_CODFAT;
    public $Sorter_MESREF;
    public $Sorter_VALFAT;
//End Variables

//Class_Initialize Event @4-09E61D07
    function clsGridcadcli_CADFAT1($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "cadcli_CADFAT1";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid cadcli_CADFAT1";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clscadcli_CADFAT1DataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 100;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("cadcli_CADFAT1Order", "");
        $this->SorterDirection = CCGetParam("cadcli_CADFAT1Dir", "");

        $this->CODCLI = new clsControl(ccsLabel, "CODCLI", "CODCLI", ccsText, "", CCGetRequestParam("CODCLI", ccsGet, NULL), $this);
        $this->DESCLI = new clsControl(ccsLabel, "DESCLI", "DESCLI", ccsText, "", CCGetRequestParam("DESCLI", ccsGet, NULL), $this);
        $this->CGCCPF = new clsControl(ccsLabel, "CGCCPF", "CGCCPF", ccsText, "", CCGetRequestParam("CGCCPF", ccsGet, NULL), $this);
        $this->CODFAT = new clsControl(ccsLabel, "CODFAT", "CODFAT", ccsText, "", CCGetRequestParam("CODFAT", ccsGet, NULL), $this);
        $this->MESREF = new clsControl(ccsLabel, "MESREF", "MESREF", ccsText, "", CCGetRequestParam("MESREF", ccsGet, NULL), $this);
        $this->VALFAT = new clsControl(ccsLabel, "VALFAT", "VALFAT", ccsFloat, array(False, 2, ",", ".", False, "", "", 1, True, ""), CCGetRequestParam("VALFAT", ccsGet, NULL), $this);
        $this->Link1 = new clsControl(ccsLink, "Link1", "Link1", ccsText, "", CCGetRequestParam("Link1", ccsGet, NULL), $this);
        $this->Link1->Page = "fatura_avulsa.php";
        $this->Sorter_CODCLI = new clsSorter($this->ComponentName, "Sorter_CODCLI", $FileName, $this);
        $this->Sorter_DESCLI = new clsSorter($this->ComponentName, "Sorter_DESCLI", $FileName, $this);
        $this->Sorter_CGCCPF = new clsSorter($this->ComponentName, "Sorter_CGCCPF", $FileName, $this);
        $this->Sorter_CODFAT = new clsSorter($this->ComponentName, "Sorter_CODFAT", $FileName, $this);
        $this->Sorter_MESREF = new clsSorter($this->ComponentName, "Sorter_MESREF", $FileName, $this);
        $this->Sorter_VALFAT = new clsSorter($this->ComponentName, "Sorter_VALFAT", $FileName, $this);
        $this->TotalSum_VALFAT = new clsControl(ccsReportLabel, "TotalSum_VALFAT", "TotalSum_VALFAT", ccsFloat, array(False, 2, ",", ".", False, "R\$ ", "", 1, True, ""), "", $this);
        $this->TotalSum_VALFAT->TotalFunction = "Sum";
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpCentered, $this);
    }
//End Class_Initialize Event

//Initialize Method @4-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @4-F1FEEE81
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urls_cadcli_CODCLI"] = CCGetFromGet("s_cadcli_CODCLI", NULL);
        $this->DataSource->Parameters["urls_DESCLI"] = CCGetFromGet("s_DESCLI", NULL);
        $this->DataSource->Parameters["urls_CODFAT"] = CCGetFromGet("s_CODFAT", NULL);
        $this->DataSource->Parameters["urls_MESREF"] = CCGetFromGet("s_MESREF", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["CODCLI"] = $this->CODCLI->Visible;
            $this->ControlsVisible["DESCLI"] = $this->DESCLI->Visible;
            $this->ControlsVisible["CGCCPF"] = $this->CGCCPF->Visible;
            $this->ControlsVisible["CODFAT"] = $this->CODFAT->Visible;
            $this->ControlsVisible["MESREF"] = $this->MESREF->Visible;
            $this->ControlsVisible["VALFAT"] = $this->VALFAT->Visible;
            $this->ControlsVisible["Link1"] = $this->Link1->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->CODCLI->SetValue($this->DataSource->CODCLI->GetValue());
                $this->DESCLI->SetValue($this->DataSource->DESCLI->GetValue());
                $this->CGCCPF->SetValue($this->DataSource->CGCCPF->GetValue());
                $this->CODFAT->SetValue($this->DataSource->CODFAT->GetValue());
                $this->MESREF->SetValue($this->DataSource->MESREF->GetValue());
                $this->VALFAT->SetValue($this->DataSource->VALFAT->GetValue());
                $this->Link1->SetValue($this->DataSource->Link1->GetValue());
                $this->Link1->Parameters = "";
                $this->Link1->Parameters = CCAddParam($this->Link1->Parameters, "codfat", $this->DataSource->f("CODFAT"));
                $this->Link1->Parameters = CCAddParam($this->Link1->Parameters, "download", 1);
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->CODCLI->Show();
                $this->DESCLI->Show();
                $this->CGCCPF->Show();
                $this->CODFAT->Show();
                $this->MESREF->Show();
                $this->VALFAT->Show();
                $this->Link1->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->TotalSum_VALFAT->SetValue($this->DataSource->TotalSum_VALFAT->GetValue());
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        $this->Sorter_CODCLI->Show();
        $this->Sorter_DESCLI->Show();
        $this->Sorter_CGCCPF->Show();
        $this->Sorter_CODFAT->Show();
        $this->Sorter_MESREF->Show();
        $this->Sorter_VALFAT->Show();
        $this->TotalSum_VALFAT->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @4-BA834E3C
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->CODCLI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESCLI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CGCCPF->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CODFAT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->MESREF->Errors->ToString());
        $errors = ComposeStrings($errors, $this->VALFAT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Link1->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End cadcli_CADFAT1 Class @4-FCB6E20C

class clscadcli_CADFAT1DataSource extends clsDBFaturar {  //cadcli_CADFAT1DataSource Class @4-75267137

//DataSource Variables @4-F323B4F0
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $CODCLI;
    public $DESCLI;
    public $CGCCPF;
    public $CODFAT;
    public $MESREF;
    public $VALFAT;
    public $Link1;
    public $TotalSum_VALFAT;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-73A11CC1
    function clscadcli_CADFAT1DataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid cadcli_CADFAT1";
        $this->Initialize();
        $this->CODCLI = new clsField("CODCLI", ccsText, "");
        $this->DESCLI = new clsField("DESCLI", ccsText, "");
        $this->CGCCPF = new clsField("CGCCPF", ccsText, "");
        $this->CODFAT = new clsField("CODFAT", ccsText, "");
        $this->MESREF = new clsField("MESREF", ccsText, "");
        $this->VALFAT = new clsField("VALFAT", ccsFloat, "");
        $this->Link1 = new clsField("Link1", ccsText, "");
        $this->TotalSum_VALFAT = new clsField("TotalSum_VALFAT", ccsFloat, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @4-5F1D1544
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "MESREF";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_CODCLI" => array("CADCLI.CODCLI", ""), 
            "Sorter_DESCLI" => array("DESCLI", ""), 
            "Sorter_CGCCPF" => array("CGCCPF", ""), 
            "Sorter_CODFAT" => array("CODFAT", ""), 
            "Sorter_MESREF" => array("MESREF", ""), 
            "Sorter_VALFAT" => array("VALFAT", "")));
    }
//End SetOrder Method

//Prepare Method @4-A83A8B77
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_cadcli_CODCLI", ccsText, "", "", $this->Parameters["urls_cadcli_CODCLI"], "", false);
        $this->wp->AddParameter("2", "urls_DESCLI", ccsText, "", "", $this->Parameters["urls_DESCLI"], "", false);
        $this->wp->AddParameter("3", "urls_CODFAT", ccsText, "", "", $this->Parameters["urls_CODFAT"], "", false);
        $this->wp->AddParameter("4", "urls_MESREF", ccsText, "", "", $this->Parameters["urls_MESREF"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "cadcli.CODCLI", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opContains, "cadcli.DESCLI", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsText),false);
        $this->wp->Criterion[3] = $this->wp->Operation(opContains, "CADFAT.CODFAT", $this->wp->GetDBValue("3"), $this->ToSQL($this->wp->GetDBValue("3"), ccsText),false);
        $this->wp->Criterion[4] = $this->wp->Operation(opContains, "CADFAT.MESREF", $this->wp->GetDBValue("4"), $this->ToSQL($this->wp->GetDBValue("4"), ccsText),false);
        $this->wp->Criterion[5] = "( CADFAT.GERBLTO='N' )";
        $this->Where = $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]), 
             $this->wp->Criterion[3]), 
             $this->wp->Criterion[4]), 
             $this->wp->Criterion[5]);
        $this->Where = $this->wp->opAND(false, "( (CADCLI.CODCLI = CADFAT.CODCLI) )", $this->Where);
    }
//End Prepare Method

//Open Method @4-FF73796F
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM CADFAT,\n\n" .
        "CADCLI";
        $this->SQL = "SELECT * \n\n" .
        "FROM CADFAT,\n\n" .
        "CADCLI {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @4-E00FB802
    function SetValues()
    {
        $this->CODCLI->SetDBValue($this->f("CODCLI"));
        $this->DESCLI->SetDBValue($this->f("DESCLI"));
        $this->CGCCPF->SetDBValue($this->f("CGCCPF"));
        $this->CODFAT->SetDBValue($this->f("CODFAT"));
        $this->MESREF->SetDBValue($this->f("MESREF"));
        $this->VALFAT->SetDBValue(trim($this->f("VALFAT")));
        $this->Link1->SetDBValue($this->f("M"));
        $this->TotalSum_VALFAT->SetDBValue(trim($this->f("VALFAT")));
    }
//End SetValues Method

} //End cadcli_CADFAT1DataSource Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-3275243C
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "FafSemBol.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Include events file @1-7D2F9E62
include("./FafSemBol_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-7F976850
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$cadcli_CADFAT = new clsRecordcadcli_CADFAT("", $MainPage);
$cadcli_CADFAT1 = new clsGridcadcli_CADFAT1("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->cadcli_CADFAT = & $cadcli_CADFAT;
$MainPage->cadcli_CADFAT1 = & $cadcli_CADFAT1;
$MainPage->rodape = & $rodape;
$cadcli_CADFAT1->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-91BD17A9
$cabec->Operations();
$cadcli_CADFAT->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-5965F904
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($cadcli_CADFAT);
    unset($cadcli_CADFAT1);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-CC20F208
$cabec->Show();
$cadcli_CADFAT->Show();
$cadcli_CADFAT1->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-574C8786
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($cadcli_CADFAT);
unset($cadcli_CADFAT1);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
