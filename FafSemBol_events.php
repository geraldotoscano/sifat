<?php
//BindEvents Method @1-BA9F056A
function BindEvents()
{
    global $cadcli_CADFAT1;
    $cadcli_CADFAT1->TotalSum_VALFAT->CCSEvents["BeforeShow"] = "cadcli_CADFAT1_TotalSum_VALFAT_BeforeShow";
    $cadcli_CADFAT1->ds->CCSEvents["BeforeBuildSelect"] = "cadcli_CADFAT1_ds_BeforeBuildSelect";
}
//End BindEvents Method

//cadcli_CADFAT1_TotalSum_VALFAT_BeforeShow @41-78817446
function cadcli_CADFAT1_TotalSum_VALFAT_BeforeShow(& $sender)
{
    $cadcli_CADFAT1_TotalSum_VALFAT_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $cadcli_CADFAT1; //Compatibility
//End cadcli_CADFAT1_TotalSum_VALFAT_BeforeShow

//Custom Code @42-2A29BDB7
// -------------------------
	$Page = CCGetParentPage($sender);
	$soma = CCDLookUp("SUM(VALFAT)", "CADFAT, CADCLI", $cadcli_CADFAT1->DataSource->Where, $Page->Connections["Faturar"]);
 	$cadcli_CADFAT1->TotalSum_VALFAT->SetValue($soma);

// -------------------------
//End Custom Code

//Close cadcli_CADFAT1_TotalSum_VALFAT_BeforeShow @41-84C9E592
    return $cadcli_CADFAT1_TotalSum_VALFAT_BeforeShow;
}
//End Close cadcli_CADFAT1_TotalSum_VALFAT_BeforeShow

//cadcli_CADFAT1_ds_BeforeBuildSelect @4-0E12333A
function cadcli_CADFAT1_ds_BeforeBuildSelect(& $sender)
{
    $cadcli_CADFAT1_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $cadcli_CADFAT1; //Compatibility
//End cadcli_CADFAT1_ds_BeforeBuildSelect

//Custom Code @38-2A29BDB7
// -------------------------

    global $cadcli_CADFAT,$cadcli_CADFAT1;
    	
	if ($cadcli_CADFAT->s_PGTO->GetValue() == 'N')
	{
		$cadcli_CADFAT1->DataSource->Where .= " and valpgt = 0 ";
	}
	elseif ($cadcli_CADFAT->s_PGTO->GetValue() == 'P')
	{
		$cadcli_CADFAT1->DataSource->Where .= " and valpgt <> 0 ";
	}
	
	if($cadcli_CADFAT1->DataSource->Order == 'MESREF')
		$cadcli_CADFAT1->DataSource->Order = 'substr(MESREF,4,4),substr(MESREF,1,2)';


// -------------------------
//End Custom Code

//Close cadcli_CADFAT1_ds_BeforeBuildSelect @4-5B80A6DC
    return $cadcli_CADFAT1_ds_BeforeBuildSelect;
}
//End Close cadcli_CADFAT1_ds_BeforeBuildSelect


?>
