<?php
//Include Common Files @1-5F7E4416
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "FatCol.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @38-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordNRFatCol { //NRFatCol Class @4-6A5C44F3

//Variables @4-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @4-9815578B
    function clsRecordNRFatCol($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record NRFatCol/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "NRFatCol";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->TxtMes = new clsControl(ccsTextBox, "TxtMes", "TxtMes", ccsText, "", CCGetRequestParam("TxtMes", $Method, NULL), $this);
            $this->TxtDataAtual = new clsControl(ccsTextBox, "TxtDataAtual", "TxtDataAtual", ccsText, "", CCGetRequestParam("TxtDataAtual", $Method, NULL), $this);
            $this->TxtVenc = new clsControl(ccsTextBox, "TxtVenc", "TxtVenc", ccsText, "", CCGetRequestParam("TxtVenc", $Method, NULL), $this);
            $this->DatePicker_TxtVenc = new clsDatePicker("DatePicker_TxtVenc", "NRFatCol", "TxtVenc", $this);
            $this->TxtUfir = new clsControl(ccsTextBox, "TxtUfir", "TxtUfir", ccsText, "", CCGetRequestParam("TxtUfir", $Method, NULL), $this);
            $this->Faturar = new clsButton("Faturar", $Method, $this);
            $this->ReFaturar = new clsButton("ReFaturar", $Method, $this);
            $this->ReFaturar1 = new clsButton("ReFaturar1", $Method, $this);
            $this->Label1 = new clsControl(ccsLabel, "Label1", "Label1", ccsText, "", CCGetRequestParam("Label1", $Method, NULL), $this);
            $this->MsgUFIR = new clsPanel("MsgUFIR", $this);
            $this->MsgFaturasPagas = new clsPanel("MsgFaturasPagas", $this);
            $this->MsgUFIR->Visible = false;
            $this->MsgFaturasPagas->Visible = false;
        }
    }
//End Class_Initialize Event

//Validate Method @4-2F469D47
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->TxtMes->Validate() && $Validation);
        $Validation = ($this->TxtDataAtual->Validate() && $Validation);
        $Validation = ($this->TxtVenc->Validate() && $Validation);
        $Validation = ($this->TxtUfir->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->TxtMes->Errors->Count() == 0);
        $Validation =  $Validation && ($this->TxtDataAtual->Errors->Count() == 0);
        $Validation =  $Validation && ($this->TxtVenc->Errors->Count() == 0);
        $Validation =  $Validation && ($this->TxtUfir->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @4-416D129B
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->TxtMes->Errors->Count());
        $errors = ($errors || $this->TxtDataAtual->Errors->Count());
        $errors = ($errors || $this->TxtVenc->Errors->Count());
        $errors = ($errors || $this->DatePicker_TxtVenc->Errors->Count());
        $errors = ($errors || $this->TxtUfir->Errors->Count());
        $errors = ($errors || $this->Label1->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @4-245A20BE
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Faturar";
            if($this->Faturar->Pressed) {
                $this->PressedButton = "Faturar";
            } else if($this->ReFaturar->Pressed) {
                $this->PressedButton = "ReFaturar";
            } else if($this->ReFaturar1->Pressed) {
                $this->PressedButton = "ReFaturar1";
            }
        }
        $Redirect = "default.php";
        if($this->Validate()) {
            if($this->PressedButton == "Faturar") {
                if(!CCGetEvent($this->Faturar->CCSEvents, "OnClick", $this->Faturar)) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "ReFaturar") {
                if(!CCGetEvent($this->ReFaturar->CCSEvents, "OnClick", $this->ReFaturar)) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "ReFaturar1") {
                if(!CCGetEvent($this->ReFaturar1->CCSEvents, "OnClick", $this->ReFaturar1)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @4-6AC3B927
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->TxtMes->Errors->ToString());
            $Error = ComposeStrings($Error, $this->TxtDataAtual->Errors->ToString());
            $Error = ComposeStrings($Error, $this->TxtVenc->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DatePicker_TxtVenc->Errors->ToString());
            $Error = ComposeStrings($Error, $this->TxtUfir->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Label1->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->TxtMes->Show();
        $this->TxtDataAtual->Show();
        $this->TxtVenc->Show();
        $this->DatePicker_TxtVenc->Show();
        $this->TxtUfir->Show();
        $this->Faturar->Show();
        $this->ReFaturar->Show();
        $this->ReFaturar1->Show();
        $this->Label1->Show();
        $this->MsgUFIR->Show();
        $this->MsgFaturasPagas->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End NRFatCol Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-F0A61DDA
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "FatCol.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-7FACF37D
CCSecurityRedirect("1;3", "");
//End Authenticate User

//Include events file @1-04152F9B
include("./FatCol_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-FA196E17
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$NRFatCol = new clsRecordNRFatCol("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->NRFatCol = & $NRFatCol;
$MainPage->rodape = & $rodape;

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-F10B3268
$cabec->Operations();
$NRFatCol->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-209C22B2
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($NRFatCol);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-C2F24A7B
$cabec->Show();
$NRFatCol->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-4C06EFC6
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$cabec->Class_Terminate();
unset($cabec);
unset($NRFatCol);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
