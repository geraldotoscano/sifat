<?php
//BindEvents Method @1-9FF61305
function BindEvents()
{
    global $NRFatCol;
    $NRFatCol->TextBox1->CCSEvents["BeforeShow"] = "NRFatCol_TextBox1_BeforeShow";
    $NRFatCol->TextBox4->CCSEvents["BeforeShow"] = "NRFatCol_TextBox4_BeforeShow";
    $NRFatCol->TextBox2->CCSEvents["BeforeShow"] = "NRFatCol_TextBox2_BeforeShow";
    $NRFatCol->CCSEvents["BeforeShow"] = "NRFatCol_BeforeShow";
}
//End BindEvents Method

//NRFatCol_TextBox1_BeforeShow @5-2B677074
function NRFatCol_TextBox1_BeforeShow(& $sender)
{
    $NRFatCol_TextBox1_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_TextBox1_BeforeShow

//Custom Code @13-2A29BDB7
// -------------------------
    // Write your own code here.01/01/2001
	$NRFatCol->TextBox1->SetValue(substr(CCGetSession("DataMesAnt"),3,7));
// -------------------------
//End Custom Code

//Close NRFatCol_TextBox1_BeforeShow @5-88889EC9
    return $NRFatCol_TextBox1_BeforeShow;
}
//End Close NRFatCol_TextBox1_BeforeShow

//NRFatCol_TextBox4_BeforeShow @22-19CA4251
function NRFatCol_TextBox4_BeforeShow(& $sender)
{
    $NRFatCol_TextBox4_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_TextBox4_BeforeShow

//Custom Code @23-2A29BDB7
// -------------------------
    $NRFatCol->TextBox4->SetValue(CCGetSession("DataSist"));
// -------------------------
//End Custom Code

//Close NRFatCol_TextBox4_BeforeShow @22-0C2BF0A4
    return $NRFatCol_TextBox4_BeforeShow;
}
//End Close NRFatCol_TextBox4_BeforeShow

//NRFatCol_TextBox2_BeforeShow @6-3AFC6197
function NRFatCol_TextBox2_BeforeShow(& $sender)
{
    $NRFatCol_TextBox2_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_TextBox2_BeforeShow

//Custom Code @14-2A29BDB7
// -------------------------
    // Write your own code here.
    global $DBfaturar;
    $mVencimento = CCGetSession("DataFatu");
	$NRFatCol->TextBox2->SetValue($mVencimento);
// -------------------------
//End Custom Code

//Close NRFatCol_TextBox2_BeforeShow @6-F4E9BB12
    return $NRFatCol_TextBox2_BeforeShow;
}
//End Close NRFatCol_TextBox2_BeforeShow

//DEL  // -------------------------
//DEL      $mMes_Ref = $NRFatCol->TextBox1->GetValue();
//DEL  
//DEL  	//$mDUAL = new clsDBfaturar();
//DEL  	//$mDUAL->query("SELECT TO_DATE('01' || SUBSTR(TO_CHAR(SYSDATE,'DD/MM/YYYY'),4,7)) AS DAT_REF FROM DUAL");
//DEL      $mDat_Ref = '01/'.$mMes_Ref;//$mDUAL->f("DAT_REF");
//DEL  
//DEL  	/*
//DEL          **********************************************************************
//DEL          * Seleciona os servi�os do cliente verificando a vig�ncia do Servi�o *
//DEL          **********************************************************************
//DEL  	*/
//DEL  	set_time_limit(600);
//DEL     	//$mArqMov = fopen("c:\sistfat\fatura4.txt","w");
//DEL     	//fwrite($mArqMov,"Inicio\r\n");
//DEL  	//$mCADCLI = new clsDBfaturar();
//DEL  	$mSUBSER = new clsDBfaturar();
//DEL  	$mCADFAT = new clsDBfaturar();
//DEL  	$mEQUSER = new clsDBfaturar();
//DEL  	$mMOVFAT = new clsDBfaturar();
//DEL  	$mTABPBH = new clsDBfaturar();
//DEL  
//DEL  	$mCADFAT->query("SELECT LPAD(TO_CHAR(MAX(TO_NUMBER(CODFAT))),6,'0') AS COD_FAT FROM CADFAT_CANC ORDER BY CODFAT");
//DEL  	$mCADFAT->next_record();
//DEL      $mCod_Fat1 = $mCADFAT->f("COD_FAT");
//DEL  	$mCADFAT->query("SELECT LPAD(TO_CHAR(MAX(TO_NUMBER(CODFAT))),6,'0') AS COD_FAT FROM CADFAT ORDER BY CODFAT");
//DEL  	$mCADFAT->next_record();
//DEL      $mCod_Fat2 = $mCADFAT->f("COD_FAT");
//DEL  
//DEL      $mCod_Fat3 = max((int)$mCod_Fat1,(int)$mCod_Fat2);
//DEL      $mCod_Fat3 = (int)$mCod_Fat3;
//DEL  
//DEL  	$mMOVSER = new clsDBfaturar();
//DEL  	$mMOVSER->query("SELECT 
//DEL  	                   m.CODCLI    as CODCLI,
//DEL  					   m.QTDMED    as QTDMED,
//DEL  					   m.SUBSER    as SUBSER,
//DEL  					   m.DATINI    as DATINI,
//DEL  					   m.DATFIM    as DATFIM,
//DEL  					   m.INIRES    as INIRES,
//DEL  					   m.FIMRES    as FIMRES,
//DEL  					   m.LOGSER    as LOGSER,
//DEL  					   m.BAISER    as BAISER,
//DEL  					   m.CIDSER    as CIDSER,
//DEL  					   m.ESTSER    as ESTSER,
//DEL  					   c.CODSIT    as CODSIT,
//DEL  					   c.PGTOELET  as PGTOELET
//DEL  					FROM 
//DEL  					   MOVSER m,
//DEL  					   CADCLI c
//DEL  					WHERE 
//DEL  					   (
//DEL  					      TO_DATE('$mDat_Ref') BETWEEN m.DATINI AND m.DATFIM 
//DEL  					   ) AND
//DEL                         (
//DEL  					      (m.INIRES IS NULL OR TO_DATE('$mDat_Ref') < m.INIRES) OR 
//DEL  						  (m.FIMRES IS NULL OR TO_DATE('$mDat_Ref') > m.FIMRES)
//DEL  					   ) AND
//DEL  					   (
//DEL  					   		m.CODCLI = c.CODCLI AND
//DEL  							c.CODSIT <> 'I'
//DEL  					   )
//DEL  					ORDER BY 
//DEL  					   CODCLI,
//DEL  					   SUBSER
//DEL  					");
//DEL  	$mMOVSER->next_record();
//DEL  	//$mMOVSER->num_rows
//DEL  	
//DEL  	do //while ($mMOVSER->f("CODCLI") != "")//($mMOVSER->next_record())
//DEL  	{
//DEL     		//fwrite($mArqMov,"movser\r\n");
//DEL      	$mCod_Cli  = $mMOVSER->f("CODCLI");
//DEL          $wCod_Cli  = $mCod_Cli;
//DEL          $mVal_Fat  = 0;
//DEL          $mRet_INSS = 0;
//DEL          $mVal_Ser  = 0;
//DEL  		//$mCADCLI->query("SELECT CODSIT,PGTOELET FROM CADCLI WHERE CODCLI='$mCod_Cli'");
//DEL  		//$mCADCLI->next_record();
//DEL  		//$mCODSIT = $mCADCLI->f("CODSIT");
//DEL          //$mPgto_Elet = $mCADCLI->f("PGTOELET");
//DEL  		//$mCODSIT    = $mMOVSER->->f("CODSIT");
//DEL  		$mPgto_Elet = $mMOVSER->f("PGTOELET");
//DEL          //mChave     = wCod_Cli + mMes_Ref
//DEL  
//DEL  			$mCADFAT->query("SELECT CODFAT FROM CADFAT WHERE CODCLI = '$mCod_Cli' AND MESREF = '$mMes_Ref'");
//DEL  			//$mCod_Fat = '0';
//DEL  			if ($mCADFAT->next_record())
//DEL  			{
//DEL     				//fwrite($mArqMov,"Fatura(if)--> ".$mCod_Fat."\r\n");
//DEL              	$mCod_Fat = $mCADFAT->f("CODFAT");
//DEL              	$Crifat   = false;
//DEL  			}
//DEL              else
//DEL  			{
//DEL  				/*
//DEL              		*
//DEL              		* Gera Novo C�digo de Fatura
//DEL              		*
//DEL  				*/
//DEL     				//fwrite($mArqMov,"Fatura(else)--> ".$mCod_Fat."\r\n");
//DEL  				$mCod_Fat3=$mCod_Fat3 + 1;
//DEL  				//if (is_null($mCADFAT->f("COD_FAT")))
//DEL  				{
//DEL  					$mCod_Fat = str_pad((string)$mCod_Fat3,6,'0',STR_PAD_LEFT);
//DEL  				}
//DEL  				//else
//DEL  				//{
//DEL  				//	
//DEL  				//	$mCod_Fat = $mCADFAT->f("COD_FAT");
//DEL  				//}
//DEL              	$Crifat = true;
//DEL              }
//DEL  		while ($mCod_Cli == $mMOVSER->f("CODCLI"))
//DEL  		{
//DEL     			//fwrite($mArqMov,"   Cliente--> ".$mCod_Cli."\r\n");
//DEL              $mSub_Ser = $mMOVSER->f("SUBSER");
//DEL              $mQtd_Med = $mMOVSER->f("QTDMED");
//DEL  			$mQtd_Med = (float)(str_replace(",", ".", $mQtd_Med));
//DEL  			
//DEL  			$mSUBSER->query("SELECT GRPSER FROM SUBSER WHERE SUBSER='$mSub_Ser'");
//DEL  			$mSUBSER->next_record();
//DEL  			$mGrp_Ser = $mSUBSER->f("GRPSER");
//DEL  
//DEL  			/*
//DEL                 	************************************************
//DEL                 	* Seleciona Equivalencia do Servi�o
//DEL                 	*
//DEL                  	* Servi�o + M�s de Ref.
//DEL                 	* - Pega a Equival�ncia do Servi�o
//DEL                 	* - Monta chave p/ pr�xima procura
//DEL                 	************************************************
//DEL  			*/
//DEL  			$mEQUSER->query("SELECT EQUPBH FROM EQUSER WHERE SUBSER = '$mSub_Ser' AND MESREF = '$mMes_Ref' ORDER BY SUBSER,MESREF");
//DEL  			$mEQUSER->next_record();
//DEL  			$l_equpbh = $mEQUSER->f("EQUPBH");
//DEL  			//fwrite($mArqMov,"Equivalencia--> ".$l_equpbh."\r\n");
//DEL  			$l_equpbh = (float)(str_replace(",", ".", $l_equpbh));
//DEL  			//fwrite($mArqMov,"Equivalencia--> ".$l_equpbh."\r\n");
//DEL  	        /*
//DEL              *********************************
//DEL              *********************************
//DEL              **                             **
//DEL              **   Calcula valor do Servi�o  **
//DEL              **                             **
//DEL              *********************************
//DEL              *********************************
//DEL  			*/
//DEL  			$l_valpbh = $NRFatCol->TextBox3->GetValue();
//DEL  			$l_valpbh = (float)(str_replace(",", ".", $l_valpbh));
//DEL              $mVal_Uni = $l_equpbh * $l_valpbh;
//DEL              $mVal_Ser = round($mQtd_Med * $mVal_Uni,2);
//DEL              $mVal_Fat += $mVal_Ser;
//DEL     			//fwrite($mArqMov,"Equivalencia--> ".$l_equpbh."\r\n");
//DEL     			//fwrite($mArqMov,"UFIR--> ".$l_valpbh."\r\n");
//DEL     			//fwrite($mArqMov,"Medi��o--> ".$mQtd_Med."\r\n");
//DEL     			//fwrite($mArqMov,"Unitario--> ".$mVal_Uni."\r\n");
//DEL  			//fwrite($mArqMov,"Sevi�o--> ".$mVal_Ser."\r\n");
//DEL     			//fwrite($mArqMov,"Fatura--> ".$mVal_Fat."\r\n");
//DEL              /*                                                                 
//DEL              *******************************************************
//DEL             	* Grava Movimento da Fatura
//DEL             	*******************************************************
//DEL             	* Procura p/ Grupo Servi�o + Servi�o + M�s de Ref. +
//DEL              * C�digo do Cliente
//DEL              * - Caso achar:
//DEL              *   a) Grava a Medi��o e a Equival�ncia do Servi�o
//DEL              * - Caso Nao Achar:
//DEL              *   a)Grava:
//DEL              *     - Quant. da Medi��o
//DEL              *     - Valor da Equival�ncia
//DEL              ********************************************************
//DEL  			*/
//DEL  			$mMOVFAT->query("SELECT EQUPBH FROM MOVFAT WHERE GRPSER = '$mGrp_Ser' AND SUBSER = '$mSub_Ser' AND MESREF = '$mMes_Ref' AND CODCLI = '$mCod_Cli' ORDER BY SUBSER,MESREF");
//DEL  			//$mQtd_Med = str_replace(",", ".", $mQtd_Med."");
//DEL  			//$l_equpbh = str_replace(",", ".", $l_equpbh."");
//DEL  			//$mVal_Ser = str_replace(",", ".", $mVal_Ser."");
//DEL  			if ($mMOVFAT->next_record())
//DEL  			{
//DEL     				//fwrite($mArqMov,"Altera--> movfat  FATURA = $mCod_Fat CLIENTE = $mCod_Cli \r\n");
//DEL  				//fwrite($mArqMov,"UPDATE MOVFAT SET QTDMED=$mQtd_Med,EQUPBH=$l_equpbh,VALSER=$mVal_Ser  WHERE GRPSER = '$mGrp_Ser' AND SUBSER = '$mSub_Ser' AND MESREF = '$mMes_Ref' AND CODCLI = '$mCod_Cli'\r\n");
//DEL  				$mMOVFAT->query("UPDATE MOVFAT SET QTDMED=$mQtd_Med,EQUPBH=$l_equpbh,VALSER=$mVal_Ser  WHERE GRPSER = '$mGrp_Ser' AND SUBSER = '$mSub_Ser' AND MESREF = '$mMes_Ref' AND CODCLI = '$mCod_Cli'");	
//DEL  			}
//DEL  			else
//DEL  			{
//DEL     				//fwrite($mArqMov,"Insere--> movfat  FATURA = $mCod_Fat CLIENTE = $mCod_Cli \r\n");
//DEL  				//fwrite($mArqMov,"INSERT INTO MOVFAT(CODFAT,CODCLI,GRPSER,SUBSER,MESREF,QTDMED,EQUPBH,VALSER) VALUES ('$mCod_Fat','$mCod_Cli','$mGrp_Ser','$mSub_Ser','$mMes_Ref',$mQtd_Med,$l_equpbh,$mVal_Ser)\r\n");
//DEL  				$mMOVFAT->query("INSERT INTO MOVFAT(CODFAT,CODCLI,GRPSER,SUBSER,MESREF,QTDMED,EQUPBH,VALSER) VALUES ('$mCod_Fat','$mCod_Cli','$mGrp_Ser','$mSub_Ser','$mMes_Ref',$mQtd_Med,$l_equpbh,$mVal_Ser)");
//DEL  			}
//DEL  			$mQtd_Med = 0.00;
//DEL  			$l_equpbh = 0.00;
//DEL  			$mVal_Ser = 0.00;
//DEL  
//DEL  			$mLOGSER = $mMOVSER->f("LOGSER");
//DEL  			//$mLOGSER = str_replace(",", " ", $mLOGSER);
//DEL  			//$mLOGSER = str_replace("'", " ", $mLOGSER);
//DEL  
//DEL  			//$mLOGSER = (str_replace(",", " ", $mLOGSER);
//DEL  			$mBAISER = $mMOVSER->f("BAISER");
//DEL  			//$mBAISER = str_replace(",", " ", $mBAISER);
//DEL  			//$mBAISER = str_replace("'", " ", $mBAISER);
//DEL  
//DEL  			$mCIDSER = $mMOVSER->f("CIDSER");
//DEL  			//$mCIDSER = str_replace(",", " ", $mCIDSER);
//DEL  			//$mCIDSER = str_replace("'", " ", $mCIDSER);
//DEL  
//DEL  			$mESTSER = $mMOVSER->f("ESTSER");
//DEL  			//$mESTSER = str_replace(",", " ", $mESTSER);
//DEL  			//$mESTSER = str_replace("'", " ", $mESTSER);
//DEL  			
//DEL  			if ($mMOVSER->next_record())
//DEL  			{
//DEL  				$fim = true;
//DEL  			}
//DEL  			else
//DEL  			{
//DEL  				$fim = false;
//DEL  				break;
//DEL  			}
//DEL  			//fwrite($mArqMov,"Fim--> $fim\r\n");
//DEL  		}
//DEL  		$mINSS_MIM = CCGetSession("mINSS_MIM");
//DEL  		$mINSS_MIM = (float)(str_replace(",", ".", $mINSS_MIM));
//DEL  
//DEL     		$mTX_INSS = CCGetSession("mINSS");
//DEL  		$mTX_INSS = (float)(str_replace(",", ".", $mTX_INSS));
//DEL  
//DEL     		$mTX_ISSQN = CCGetSession("mISSQN");
//DEL  		$mTX_ISSQN = (float)(str_replace(",", ".", $mTX_ISSQN));
//DEL  
//DEL          if ($mVal_Fat > 0)
//DEL  		{
//DEL     			//fwrite($mArqMov,"Fatura maior que zero--> ".$mVal_Fat."\r\n");
//DEL  			$mAnoMes = 0+(substr($mMes_Ref,3,4).substr($mMes_Ref,0,2));
//DEL          	if ($mAnoMes > 199901)
//DEL  			{
//DEL             		/*
//DEL             		*******************************************
//DEL             		* RETENSAO INSS do Valor da Fatura
//DEL             		*******************************************
//DEL  				*/
//DEL             		$nInss = round(($mVal_Fat*$mTX_INSS/100),2);
//DEL     			    //fwrite($mArqMov,"inss--> ".$nInss."\r\n");
//DEL  				//fwrite($mArqMov,"inss calculado--> ".($mVal_Fat*$mTX_INSS/100)."\r\n");
//DEL  		 		if ($mAnoMes < 200812)
//DEL  		  		{
//DEL                 		$mISSQN = round(($mVal_Fat)*$mTX_ISSQN/100,2);
//DEL     			    	//fwrite($mArqMov,"issqn--> ".$mISSQN."\r\n");
//DEL             		}
//DEL  		  		else
//DEL  		  		{
//DEL                 		$mISSQN = 0;
//DEL             		}
//DEL             		if ($nInss <= $mINSS_MIM)
//DEL  		  		{
//DEL                 		$nInss=0;
//DEL             		}
//DEL  				//fwrite($mArqMov,"inss m�nimo--> ".$mINSS_MIM."\r\n");
//DEL  				//fwrite($mArqMov,"inss_2--> ".$nInss."\r\n");
//DEL          	}
//DEL          	else
//DEL  			{
//DEL             		$mISSQN = 0;
//DEL             		$nInss = 0;
//DEL          	}
//DEL          	$mRet_Inss = $nInss;
//DEL  			/*
//DEL          	****************************
//DEL          	*                          *
//DEL          	* Grava ou Regrava Fatura  *
//DEL          	*                          *
//DEL          	****************************
//DEL  			*/
//DEL  			$g_dtsist  = CCGetSession("DataSist");
//DEL  			$l_datvnc  = $NRFatCol->TextBox2->GetValue();
//DEL  			//$mVal_Fat  = str_replace(",", ".", $mVal_Fat."");
//DEL  			//$mISSQN    = str_replace(",", ".", $mISSQN."");
//DEL  			//$mRet_INSS = str_replace(",", ".", $mRet_INSS."");
//DEL  		    $mTaxa_Juros = CCGetSession("mTAXA_JUROS");
//DEL  			$mTaxa_Juros = (float)(str_replace(",", ".", $mTaxa_Juros));
//DEL  			// Juros proporcionais. A taxa de juros � mansal e proporcional ao
//DEL  			// dia.
//DEL  			// Gra�as a Deus por Jesus Cristo.
//DEL  			$mMulta = (($mVal_Fat-$mRet_Inss) * ($mTaxa_Juros/100/30));
//DEL  			//$mMulta = (($mVal_Fat * ($mTaxa_Juros/100))/30);
//DEL          	if ($Crifat)
//DEL  			{
//DEL     			    //fwrite($mArqMov,"Insere--> cadfat  FATURA = $mCod_Fat CLIENTE = $mCod_Cli \r\n");
//DEL  				//fwrite($mArqMov,"INSERT INTO CADFAT(codfat,codcli,mesref,datemi,datvnc,PgtoElet,ValFat,Ret_INSS,ISSQN,valmov,valpgt,valcob,export,valjur,valmul,LOGSER,BAISER,CIDSER,ESTSER) VALUES('$mCod_Fat','$mCod_Cli','$mMes_Ref',to_date('$g_dtsist'),to_date('$l_datvnc'),'$mPgto_Elet',$mVal_Fat,$mRet_Inss,$mISSQN,0.00,0.00,0.00,'F',0.00,$mMulta,'$mLOGSER','$mBAISER','$mCIDSER','$mESTSER')\r\n");
//DEL  				$mCADFAT->query("INSERT INTO CADFAT(codfat,codcli,mesref,datemi,datvnc,PgtoElet,ValFat,Ret_INSS,ISSQN,valmov,valpgt,valcob,export,valjur,valmul,LOGSER,BAISER,CIDSER,ESTSER) VALUES('$mCod_Fat','$mCod_Cli','$mMes_Ref',to_date('$g_dtsist'),to_date('$l_datvnc'),'$mPgto_Elet',$mVal_Fat,$mRet_Inss,$mISSQN,0.00,0.00,0.00,'F',0.00,$mMulta,'$mLOGSER','$mBAISER','$mCIDSER','$mESTSER')");
//DEL  			}
//DEL  			else
//DEL  			{
//DEL     			    //fwrite($mArqMov,"Altera--> cadfat  FATURA = $mCod_Fat CLIENTE = $mCod_Cli \r\n");
//DEL  				//fwrite($mArqMov,"UPDATE CADFAT SET codfat = '$mCod_Fat',codcli = '$mCod_Cli',mesref = '$mMes_Ref',datemi = to_date('$g_dtsist'),datvnc = to_date('$l_datvnc'),PgtoElet = '$mPgto_Elet',ValFat = $mVal_Fat,Ret_INSS = $mRet_Inss,ISSQN = $mISSQN,valmov = 0.00,valpgt = 0.00,valcob = 0.00,export = 'F',valjur = 0.00,valmul = $mMulta ,LOGSER = '$mLOGSER',BAISER = '$mBAISER',CIDSER = '$mCIDSER',ESTSER = '$mESTSER' WHERE CODCLI = '$mCod_Cli' AND MESREF = '$mMes_Ref'\r\n");
//DEL  		   		$mCADFAT->query("UPDATE CADFAT SET codfat = '$mCod_Fat',codcli = '$mCod_Cli',mesref = '$mMes_Ref',datemi = to_date('$g_dtsist'),datvnc = to_date('$l_datvnc'),PgtoElet = '$mPgto_Elet',ValFat = $mVal_Fat,Ret_INSS = $mRet_Inss,ISSQN = $mISSQN,valmov = 0.00,valpgt = 0.00,valcob = 0.00,export = 'F',valjur = 0.00,valmul = $mMulta ,LOGSER = '$mLOGSER',BAISER = '$mBAISER',CIDSER = '$mCIDSER',ESTSER = '$mESTSER' WHERE CODCLI = '$mCod_Cli' AND MESREF = '$mMes_Ref'");
//DEL  			}
//DEL  			$mVal_Fat  = 0.00;
//DEL  			$mISSQN    = 0.00;
//DEL  			$mRet_INSS = 0.00;
//DEL  			//fwrite($mArqMov,"                                                                                                 \r\n");
//DEL              //fwrite($mArqMov,"-------------------------------------------------------------------------------------------------\r\n");
//DEL  			//fwrite($mArqMov,"                                                                                                 \r\n");
//DEL          }
//DEL  	} while ($fim);
//DEL     	//fwrite($mArqMov,"F I N A L I Z A D O \r\n");
//DEL  	$mTABPBH->query("UPDATE TABPBH SET PROCES = 'T' WHERE MESREF = '$mMes_Ref'");
//DEL  	$mMOVFAT->close;
//DEL  	$mEQUSER->close;
//DEL  	$mCADFAT->close;
//DEL  	$mSUBSER->close;
//DEL  	$mMOVSER->close;
//DEL  	unset($mMOVFAT);
//DEL  	unset($mEQUSER); 
//DEL  	unset($mCADFAT);
//DEL  	unset($mSUBSER); 
//DEL  	unset($mMOVSER);
//DEL  	//unset($mDUAL);
//DEL  	unset($mTABPBH);
//DEL  	set_time_limit(30);
//DEL     	//fclose($mArqMov);
//DEL  
//DEL  // -------------------------

//DEL  // -------------------------
//DEL  // -------------------------

//DEL  // -------------------------
//DEL  	/*
//DEL         *    O cliente deste sistema as vezes gerava o faturamento coletivo e depois de gera-
//DEL         * do precisou desativar um determinado cliente. O cliente � desativado na tabela do
//DEL         * CADCLI.DBF e se esta desativa��o ocorrer antes da gera��o do faturamento, tudo vai
//DEL         * correr bem. Do contr�rio o mesmo j� foi gerado para o arquivo MOVFAT.DBF e embora
//DEL         * o sistema verifique a situa��o do cliente, este permanesse no MOVFAT.DBF em virtude
//DEL         * da primeira gera��o.
//DEL  	*/
//DEL  	// N�o testado.Favor testar e apagar este coment�rio.
//DEL  	$mMesRef = $NRFatCol->TextBox1->GetValue();
//DEL  	$Tabela = new clsDBfaturar();
//DEL  	$Tabela->query("DELETE FROM MOVFAT WHERE MESREF = '$mMesRef'");
//DEL  	$Tabela->query("DELETE FROM CADFAT WHERE MESREF = '$mMesRef'");
//DEL  	$Tabela->close;
//DEL  	unset($Tabela);
//DEL  	NRFatCol_Faturar_OnClick(& $sender);
//DEL  // -------------------------

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL      $mMes_Ref = $NRFatCol->TextBox1->GetValue();
//DEL  
//DEL  	//$mDUAL = new clsDBfaturar();
//DEL  	//$mDUAL->query("SELECT TO_DATE('01' || SUBSTR(TO_CHAR(SYSDATE,'DD/MM/YYYY'),4,7)) AS DAT_REF FROM DUAL");
//DEL      $mDat_Ref = '01/'.$mMes_Ref;//$mDUAL->f("DAT_REF");
//DEL  
//DEL  	/*
//DEL          **********************************************************************
//DEL          * Seleciona os servi�os do cliente verificando a vig�ncia do Servi�o *
//DEL          **********************************************************************
//DEL  	*/
//DEL  	set_time_limit(600);
//DEL     	$mArqMov = fopen("c:\sistfat\fatura1.txt","w");
//DEL     	fwrite($mArqMov,"Inicio\r\n");
//DEL  	$mMOVSER = new clsDBfaturar();
//DEL  	$mMOVSER->query("SELECT 
//DEL  	                   m.CODCLI    as CODCLI,
//DEL  					   m.QTDMED    as QTDMED,
//DEL  					   m.SUBSER    as SUBSER,
//DEL  					   m.DATINI    as DATINI,
//DEL  					   m.DATFIM    as DATFIM,
//DEL  					   m.INIRES    as INIRES,
//DEL  					   m.FIMRES    as FIMRES,
//DEL  					   c.CODSIT    as CODSIT,
//DEL  					   c.PGTOELET  as PGTOELET
//DEL  					FROM 
//DEL  					   MOVSER m,
//DEL  					   CADCLI c
//DEL  					WHERE 
//DEL  					   (
//DEL  					      TO_DATE('$mDat_Ref') BETWEEN m.DATINI AND m.DATFIM 
//DEL  					   ) AND
//DEL                         (
//DEL  					      (m.INIRES IS NULL OR TO_DATE('$mDat_Ref') < m.INIRES) OR 
//DEL  						  (m.FIMRES IS NULL OR TO_DATE('$mDat_Ref') > m.FIMRES)
//DEL  					   ) AND
//DEL  					   (
//DEL  					   		m.CODCLI = c.CODCLI AND
//DEL  							c.CODSIT <> 'I'
//DEL  					   )
//DEL  					ORDER BY 
//DEL  					   CODCLI,
//DEL  					   SUBSER
//DEL  					");
//DEL  	$mMOVSER->next_record();
//DEL  	//$mMOVSER->num_rows
//DEL  	
//DEL  	do //while ($mMOVSER->f("CODCLI") != "")//($mMOVSER->next_record())
//DEL  	{
//DEL  	    $mCod_Cli  = $mMOVSER->f("CODCLI");
//DEL  		while ($mCod_Cli == $mMOVSER->f("CODCLI"))
//DEL  		{
//DEL  		 	fwrite($mArqMov,"mCod_Cli--> $mCod_Cli\r\n");
//DEL  			if ($mMOVSER->next_record())
//DEL  			{
//DEL  				$fim = true;
//DEL  			}
//DEL  			else
//DEL  			{
//DEL  				$fim = false;
//DEL  				break;
//DEL  			}
//DEL  			fwrite($mArqMov,"mMOVSER->f('CODCLI'--> ".$mMOVSER->f("CODCLI")."\r\n");
//DEL  			fwrite($mArqMov,"Fim--> $fim\r\n");
//DEL  		}
//DEL  	} while ($fim);
//DEL     	fwrite($mArqMov,"F I N A L I Z A D O \r\n");
//DEL  	unset($mMOVSER);
//DEL  	set_time_limit(30);
//DEL     	fclose($mArqMov);
//DEL  // -------------------------

//NRFatCol_BeforeShow @4-A6D8211B
function NRFatCol_BeforeShow(& $sender)
{
    $NRFatCol_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_BeforeShow

//Custom Code @19-2A29BDB7
// -------------------------
   //global $DBfaturar;
   $Tabela = new clsDBfaturar();
   $mMesRef = substr(CCGetSession("DataMesAnt"),3,7);//substr(CCGetSession("DataSist"),3,7);
   // Verifica se este mes/ano j� foi faturado. N�o o Sendo, ativa o bot�o de 
   // Faturar.
   $Tabela->query("SELECT valpbh,proces FROM tabpbh where mesref = '$mMesRef'");
   $mTemUFIR = $Tabela->next_record();
   $Tabela1 = new clsDBfaturar();
   $Tabela1->query("SELECT subser,dessub FROM subser");
   $mErro = false;
   $Tabela2 = new clsDBfaturar();
   while ($Tabela1->next_record())
   {
   		$mSubSer = $Tabela1->f("subser");
		$mDesSub = $Tabela1->f("dessub");
   		$Tabela2->query("SELECT mesref FROM equser where subser = '$mSubSer' and mesref = '$mMesRef'");
		//echo $Tabela2->f("mesref ");'Servico:' + l_subser + ' Nao Existe na Tabela de Equivalencias'
		if (!$Tabela2->next_record())
		{
			//echo "Subservi�o -> $mDesSub"." mesref =$mMesRef ";
			//$NRFatCol->Errors->addError("Servico: $mDesSub N�o Existe na Tabela de Equivalencias");
			//$NRFatCol_BeforeShow = false;
			$mErro = true;
			break;
		}
   }
   if (($mTemUFIR) and (!$mErro))// Se tem UFIR e n�o esta faltando servicos
   {
   		$NRFatCol->TextBox3->SetValue($Tabela->f("valpbh"));
   		if ($Tabela->f("proces") == "T")// Se j� faturado
   		{
			$NRFatCol->Faturar->Visible = false;
			// Verifica se tem fatura j� paga neste mes/ano. Tendo � imposs�vel re-
			// Faturar.
   			$Tabela->query("select count(codcli) as ja_faturado from cadfat where mesref='$mMesRef' and valpgt <> 0");
			$Tabela->next_record();
			$mFatura_Paga = ($Tabela->f("ja_faturado")!=0);
   			if ($mFatura_Paga) // Se al�m de faturado j� tem alguma fatura paga 
			{
				// N�o refatura
				$NRFatCol->ReFaturar->Visible = false;
				//$NRFatCol->Label1->SetValue($Tabela->f("ja_faturado"));
				$NRFatCol->Panel1->Visible = true;
			}
			else // Se al�m de faturado n�o tem nenhuma fatura paga 
			{
			 	// Refatura
				$NRFatCol->ReFaturar->Visible = true;
				$NRFatCol->Panel1->Visible = false;
			}
   		}
   		else // Se ainda n�o foi faturado
   		{
			// Faturamento normal
			$NRFatCol->Faturar->Visible = true;
			$NRFatCol->ReFaturar->Visible = false;
			$NRFatCol->Panel1->Visible = false;
   		}
   }
   else
   {
   	if (!$mTemUFIR) // Se n�o tem UFIR cadastrada
   	{
        	// N�o fatura nem refatura
			$NRFatCol->Faturar->Visible = false;
			$NRFatCol->ReFaturar->Visible = false;
			$NRFatCol->Panel1->Visible = false;
			$NRFatCol->Panel2->Visible = true;
   	}
   	else // Se tem a UFIR mas n�o tem os servi�os
  	{
        	// N�o fatura nem refatura
			// N�o testado.Favor testar e apagar este coment�rio.
			$NRFatCol->Faturar->Visible = false;
			$NRFatCol->ReFaturar->Visible = false;
			$NRFatCol->Panel1->Visible = false;
			$NRFatCol->Panel2->Visible = false;
			$NRFatCol->Label1->SetValue("Servico: $mDesSub n�o existe na tabela de equival�ncias.");
   	}
   }
// -------------------------
//End Custom Code
/*
    $mMes_Ref = $NRFatCol->TextBox1->GetValue();

	//$mDUAL = new clsDBfaturar();
	//$mDUAL->query("SELECT TO_DATE('01' || SUBSTR(TO_CHAR(SYSDATE,'DD/MM/YYYY'),4,7)) AS DAT_REF FROM DUAL");
    $mDat_Ref = '01/'.$mMes_Ref;//$mDUAL->f("DAT_REF");

	//
    //    **********************************************************************
    //    * Seleciona os servi�os do cliente verificando a vig�ncia do Servi�o *
    //    **********************************************************************
	//
	set_time_limit(600);
   	//$mArqMov = fopen("c:\sistfat\fatura4.txt","w");
   	//fwrite($mArqMov,"Inicio\r\n");
	//$mCADCLI = new clsDBfaturar();
	$mSUBSER = new clsDBfaturar();
	$mCADFAT = new clsDBfaturar();
	$mEQUSER = new clsDBfaturar();
	$mMOVFAT = new clsDBfaturar();
	$mTABPBH = new clsDBfaturar();

	$mCADFAT->query("SELECT LPAD(TO_CHAR(MAX(TO_NUMBER(CODFAT))),6,'0') AS COD_FAT FROM CADFAT_CANC ORDER BY CODFAT");
	$mCADFAT->next_record();
    $mCod_Fat1 = $mCADFAT->f("COD_FAT");
	$mCADFAT->query("SELECT LPAD(TO_CHAR(MAX(TO_NUMBER(CODFAT))),6,'0') AS COD_FAT FROM CADFAT ORDER BY CODFAT");
	$mCADFAT->next_record();
    $mCod_Fat2 = $mCADFAT->f("COD_FAT");

    $mCod_Fat3 = max((int)$mCod_Fat1,(int)$mCod_Fat2);
    $mCod_Fat3 = (int)$mCod_Fat3;

	$mMOVSER = new clsDBfaturar();
	$mMOVSER->query("SELECT 
	                   m.CODCLI    as CODCLI,
					   m.QTDMED    as QTDMED,
					   m.SUBSER    as SUBSER,
					   m.DATINI    as DATINI,
					   m.DATFIM    as DATFIM,
					   m.INIRES    as INIRES,
					   m.FIMRES    as FIMRES,
					   m.LOGSER    as LOGSER,
					   m.BAISER    as BAISER,
					   m.CIDSER    as CIDSER,
					   m.ESTSER    as ESTSER,
					   c.CODSIT    as CODSIT,
					   c.PGTOELET  as PGTOELET
					FROM 
					   MOVSER m,
					   CADCLI c
					WHERE 
					   (
					      TO_DATE('$mDat_Ref') BETWEEN m.DATINI AND m.DATFIM 
					   ) AND
                       (
					      (m.INIRES IS NULL OR TO_DATE('$mDat_Ref') < m.INIRES) OR 
						  (m.FIMRES IS NULL OR TO_DATE('$mDat_Ref') > m.FIMRES)
					   ) AND
					   (
					   		m.CODCLI = c.CODCLI AND
							c.CODSIT <> 'I'
					   )
					ORDER BY 
					   CODCLI,
					   SUBSER
					");
	$mMOVSER->next_record();
	//$mMOVSER->num_rows
	
	do //while ($mMOVSER->f("CODCLI") != "")//($mMOVSER->next_record())
	{
   		//fwrite($mArqMov,"movser\r\n");
    	$mCod_Cli  = $mMOVSER->f("CODCLI");
        $wCod_Cli  = $mCod_Cli;
        $mVal_Fat  = 0;
        $mRet_INSS = 0;
        $mVal_Ser  = 0;
		//$mCADCLI->query("SELECT CODSIT,PGTOELET FROM CADCLI WHERE CODCLI='$mCod_Cli'");
		//$mCADCLI->next_record();
		//$mCODSIT = $mCADCLI->f("CODSIT");
        //$mPgto_Elet = $mCADCLI->f("PGTOELET");
		//$mCODSIT    = $mMOVSER->->f("CODSIT");
		$mPgto_Elet = $mMOVSER->f("PGTOELET");
        //mChave     = wCod_Cli + mMes_Ref

			$mCADFAT->query("SELECT CODFAT FROM CADFAT WHERE CODCLI = '$mCod_Cli' AND MESREF = '$mMes_Ref'");
			//$mCod_Fat = '0';
			if ($mCADFAT->next_record())
			{
   				//fwrite($mArqMov,"Fatura(if)--> ".$mCod_Fat."\r\n");
            	$mCod_Fat = $mCADFAT->f("CODFAT");
            	$Crifat   = false;
			}
            else
			{
				//
            	//	*
            	//	* Gera Novo C�digo de Fatura
            	//	*
				//
   				//fwrite($mArqMov,"Fatura(else)--> ".$mCod_Fat."\r\n");
				$mCod_Fat3=$mCod_Fat3 + 1;
				//if (is_null($mCADFAT->f("COD_FAT")))
				{
					$mCod_Fat = str_pad((string)$mCod_Fat3,6,'0',STR_PAD_LEFT);
				}
				//else
				//{
				//	
				//	$mCod_Fat = $mCADFAT->f("COD_FAT");
				//}
            	$Crifat = true;
            }
		while ($mCod_Cli == $mMOVSER->f("CODCLI"))
		{
   			//fwrite($mArqMov,"   Cliente--> ".$mCod_Cli."\r\n");
            $mSub_Ser = $mMOVSER->f("SUBSER");
            $mQtd_Med = $mMOVSER->f("QTDMED");
			$mQtd_Med = (float)(str_replace(",", ".", $mQtd_Med));
			
			$mSUBSER->query("SELECT GRPSER FROM SUBSER WHERE SUBSER='$mSub_Ser'");
			$mSUBSER->next_record();
			$mGrp_Ser = $mSUBSER->f("GRPSER");

			//
            //   	************************************************
            //   	* Seleciona Equivalencia do Servi�o
            //   	*
            //    	* Servi�o + M�s de Ref.
            //   	* - Pega a Equival�ncia do Servi�o
            //   	* - Monta chave p/ pr�xima procura
            //   	************************************************
			//
			$mEQUSER->query("SELECT EQUPBH FROM EQUSER WHERE SUBSER = '$mSub_Ser' AND MESREF = '$mMes_Ref' ORDER BY SUBSER,MESREF");
			$mEQUSER->next_record();
			$l_equpbh = $mEQUSER->f("EQUPBH");
			//fwrite($mArqMov,"Equivalencia--> ".$l_equpbh."\r\n");
			$l_equpbh = (float)(str_replace(",", ".", $l_equpbh));
			//fwrite($mArqMov,"Equivalencia--> ".$l_equpbh."\r\n");
	        //
            // *********************************
            // *********************************
            // **                             **
            // **   Calcula valor do Servi�o  **
            // **                             **
            // *********************************
            // *********************************
			//
			$l_valpbh = $NRFatCol->TextBox3->GetValue();
			$l_valpbh = (float)(str_replace(",", ".", $l_valpbh));
            $mVal_Uni = $l_equpbh * $l_valpbh;
            $mVal_Ser = round($mQtd_Med * $mVal_Uni,2);
            $mVal_Fat += $mVal_Ser;
   			//fwrite($mArqMov,"Equivalencia--> ".$l_equpbh."\r\n");
   			//fwrite($mArqMov,"UFIR--> ".$l_valpbh."\r\n");
   			//fwrite($mArqMov,"Medi��o--> ".$mQtd_Med."\r\n");
   			//fwrite($mArqMov,"Unitario--> ".$mVal_Uni."\r\n");
			//fwrite($mArqMov,"Sevi�o--> ".$mVal_Ser."\r\n");
   			//fwrite($mArqMov,"Fatura--> ".$mVal_Fat."\r\n");
            //                                                                 
            // *******************************************************
           	// * Grava Movimento da Fatura
           	// *******************************************************
           	// * Procura p/ Grupo Servi�o + Servi�o + M�s de Ref. +
            // * C�digo do Cliente
            // * - Caso achar:
            // *   a) Grava a Medi��o e a Equival�ncia do Servi�o
            // * - Caso Nao Achar:
            // *   a)Grava:
            // *     - Quant. da Medi��o
            // *     - Valor da Equival�ncia
            //********************************************************
			//
			$mMOVFAT->query("SELECT EQUPBH FROM MOVFAT WHERE GRPSER = '$mGrp_Ser' AND SUBSER = '$mSub_Ser' AND MESREF = '$mMes_Ref' AND CODCLI = '$mCod_Cli' ORDER BY SUBSER,MESREF");
			//$mQtd_Med = str_replace(",", ".", $mQtd_Med."");
			//$l_equpbh = str_replace(",", ".", $l_equpbh."");
			//$mVal_Ser = str_replace(",", ".", $mVal_Ser."");
			if ($mMOVFAT->next_record())
			{
   				//fwrite($mArqMov,"Altera--> movfat  FATURA = $mCod_Fat CLIENTE = $mCod_Cli \r\n");
				//fwrite($mArqMov,"UPDATE MOVFAT SET QTDMED=$mQtd_Med,EQUPBH=$l_equpbh,VALSER=$mVal_Ser  WHERE GRPSER = '$mGrp_Ser' AND SUBSER = '$mSub_Ser' AND MESREF = '$mMes_Ref' AND CODCLI = '$mCod_Cli'\r\n");
				$mMOVFAT->query("UPDATE MOVFAT SET QTDMED=$mQtd_Med,EQUPBH=$l_equpbh,VALSER=$mVal_Ser  WHERE GRPSER = '$mGrp_Ser' AND SUBSER = '$mSub_Ser' AND MESREF = '$mMes_Ref' AND CODCLI = '$mCod_Cli'");	
			}
			else
			{
   				//fwrite($mArqMov,"Insere--> movfat  FATURA = $mCod_Fat CLIENTE = $mCod_Cli \r\n");
				//fwrite($mArqMov,"INSERT INTO MOVFAT(CODFAT,CODCLI,GRPSER,SUBSER,MESREF,QTDMED,EQUPBH,VALSER) VALUES ('$mCod_Fat','$mCod_Cli','$mGrp_Ser','$mSub_Ser','$mMes_Ref',$mQtd_Med,$l_equpbh,$mVal_Ser)\r\n");
				$mMOVFAT->query("INSERT INTO MOVFAT(CODFAT,CODCLI,GRPSER,SUBSER,MESREF,QTDMED,EQUPBH,VALSER) VALUES ('$mCod_Fat','$mCod_Cli','$mGrp_Ser','$mSub_Ser','$mMes_Ref',$mQtd_Med,$l_equpbh,$mVal_Ser)");
			}
			$mQtd_Med = 0.00;
			$l_equpbh = 0.00;
			$mVal_Ser = 0.00;

			$mLOGSER = $mMOVSER->f("LOGSER");
			//$mLOGSER = str_replace(",", " ", $mLOGSER);
			//$mLOGSER = str_replace("'", " ", $mLOGSER);

			//$mLOGSER = (str_replace(",", " ", $mLOGSER);
			$mBAISER = $mMOVSER->f("BAISER");
			//$mBAISER = str_replace(",", " ", $mBAISER);
			//$mBAISER = str_replace("'", " ", $mBAISER);

			$mCIDSER = $mMOVSER->f("CIDSER");
			//$mCIDSER = str_replace(",", " ", $mCIDSER);
			//$mCIDSER = str_replace("'", " ", $mCIDSER);

			$mESTSER = $mMOVSER->f("ESTSER");
			//$mESTSER = str_replace(",", " ", $mESTSER);
			//$mESTSER = str_replace("'", " ", $mESTSER);
			
			if ($mMOVSER->next_record())
			{
				$fim = true;
			}
			else
			{
				$fim = false;
				break;
			}
			//fwrite($mArqMov,"Fim--> $fim\r\n");
		}
		$mINSS_MIM = CCGetSession("mINSS_MIM");
		$mINSS_MIM = (float)(str_replace(",", ".", $mINSS_MIM));

   		$mTX_INSS = CCGetSession("mINSS");
		$mTX_INSS = (float)(str_replace(",", ".", $mTX_INSS));

   		$mTX_ISSQN = CCGetSession("mISSQN");
		$mTX_ISSQN = (float)(str_replace(",", ".", $mTX_ISSQN));

        if ($mVal_Fat > 0)
		{
   			//fwrite($mArqMov,"Fatura maior que zero--> ".$mVal_Fat."\r\n");
			$mAnoMes = 0+(substr($mMes_Ref,3,4).substr($mMes_Ref,0,2));
        	if ($mAnoMes > 199901)
			{
           		// 
           		// *******************************************
           		// * RETENSAO INSS do Valor da Fatura
           		// *******************************************
				// 
           		$nInss = round(($mVal_Fat*$mTX_INSS/100),2);
   			    //fwrite($mArqMov,"inss--> ".$nInss."\r\n");
				//fwrite($mArqMov,"inss calculado--> ".($mVal_Fat*$mTX_INSS/100)."\r\n");
		 		if ($mAnoMes < 200812)
		  		{
               		$mISSQN = round(($mVal_Fat)*$mTX_ISSQN/100,2);
   			    	//fwrite($mArqMov,"issqn--> ".$mISSQN."\r\n");
           		}
		  		else
		  		{
               		$mISSQN = 0;
           		}
           		if ($nInss <= $mINSS_MIM)
		  		{
               		$nInss=0;
           		}
				//fwrite($mArqMov,"inss m�nimo--> ".$mINSS_MIM."\r\n");
				//fwrite($mArqMov,"inss_2--> ".$nInss."\r\n");
        	}
        	else
			{
           		$mISSQN = 0;
           		$nInss = 0;
        	}
        	$mRet_Inss = $nInss;
			// 
        	// ****************************
        	// *                          *
        	// * Grava ou Regrava Fatura  *
        	// *                          *
        	// ****************************
			// 
			$g_dtsist  = CCGetSession("DataSist");
			$l_datvnc  = $NRFatCol->TextBox2->GetValue();
			//$mVal_Fat  = str_replace(",", ".", $mVal_Fat."");
			//$mISSQN    = str_replace(",", ".", $mISSQN."");
			//$mRet_INSS = str_replace(",", ".", $mRet_INSS."");
		    $mTaxa_Juros = CCGetSession("mTAXA_JUROS");
			$mTaxa_Juros = (float)(str_replace(",", ".", $mTaxa_Juros));
			// Juros proporcionais. A taxa de juros � mansal e proporcional ao
			// dia.
			// Gra�as a Deus por Jesus Cristo.
			$mMulta = (($mVal_Fat-$mRet_Inss) * ($mTaxa_Juros/100/30));
			//$mMulta = (($mVal_Fat * ($mTaxa_Juros/100))/30);
        	if ($Crifat)
			{
   			    //fwrite($mArqMov,"Insere--> cadfat  FATURA = $mCod_Fat CLIENTE = $mCod_Cli \r\n");
				//fwrite($mArqMov,"INSERT INTO CADFAT(codfat,codcli,mesref,datemi,datvnc,PgtoElet,ValFat,Ret_INSS,ISSQN,valmov,valpgt,valcob,export,valjur,valmul,LOGSER,BAISER,CIDSER,ESTSER) VALUES('$mCod_Fat','$mCod_Cli','$mMes_Ref',to_date('$g_dtsist'),to_date('$l_datvnc'),'$mPgto_Elet',$mVal_Fat,$mRet_Inss,$mISSQN,0.00,0.00,0.00,'F',0.00,$mMulta,'$mLOGSER','$mBAISER','$mCIDSER','$mESTSER')\r\n");
				$mCADFAT->query("INSERT INTO CADFAT(codfat,codcli,mesref,datemi,datvnc,PgtoElet,ValFat,Ret_INSS,ISSQN,valmov,valpgt,valcob,export,valjur,valmul,LOGSER,BAISER,CIDSER,ESTSER) VALUES('$mCod_Fat','$mCod_Cli','$mMes_Ref',to_date('$g_dtsist'),to_date('$l_datvnc'),'$mPgto_Elet',$mVal_Fat,$mRet_Inss,$mISSQN,0.00,0.00,0.00,'F',0.00,$mMulta,'$mLOGSER','$mBAISER','$mCIDSER','$mESTSER')");
			}
			else
			{
   			    //fwrite($mArqMov,"Altera--> cadfat  FATURA = $mCod_Fat CLIENTE = $mCod_Cli \r\n");
				//fwrite($mArqMov,"UPDATE CADFAT SET codfat = '$mCod_Fat',codcli = '$mCod_Cli',mesref = '$mMes_Ref',datemi = to_date('$g_dtsist'),datvnc = to_date('$l_datvnc'),PgtoElet = '$mPgto_Elet',ValFat = $mVal_Fat,Ret_INSS = $mRet_Inss,ISSQN = $mISSQN,valmov = 0.00,valpgt = 0.00,valcob = 0.00,export = 'F',valjur = 0.00,valmul = $mMulta ,LOGSER = '$mLOGSER',BAISER = '$mBAISER',CIDSER = '$mCIDSER',ESTSER = '$mESTSER' WHERE CODCLI = '$mCod_Cli' AND MESREF = '$mMes_Ref'\r\n");
		   		$mCADFAT->query("UPDATE CADFAT SET codfat = '$mCod_Fat',codcli = '$mCod_Cli',mesref = '$mMes_Ref',datemi = to_date('$g_dtsist'),datvnc = to_date('$l_datvnc'),PgtoElet = '$mPgto_Elet',ValFat = $mVal_Fat,Ret_INSS = $mRet_Inss,ISSQN = $mISSQN,valmov = 0.00,valpgt = 0.00,valcob = 0.00,export = 'F',valjur = 0.00,valmul = $mMulta ,LOGSER = '$mLOGSER',BAISER = '$mBAISER',CIDSER = '$mCIDSER',ESTSER = '$mESTSER' WHERE CODCLI = '$mCod_Cli' AND MESREF = '$mMes_Ref'");
			}
			$mVal_Fat  = 0.00;
			$mISSQN    = 0.00;
			$mRet_INSS = 0.00;
			//fwrite($mArqMov,"                                                                                                 \r\n");
            //fwrite($mArqMov,"-------------------------------------------------------------------------------------------------\r\n");
			//fwrite($mArqMov,"                                                                                                 \r\n");
        }
	} while ($fim);
   	//fwrite($mArqMov,"F I N A L I Z A D O \r\n");
	$mTABPBH->query("UPDATE TABPBH SET PROCES = 'T' WHERE MESREF = '$mMes_Ref'");
	$mMOVFAT->close;
	$mEQUSER->close;
	$mCADFAT->close;
	$mSUBSER->close;
	$mMOVSER->close;
	unset($mMOVFAT);
	unset($mEQUSER); 
	unset($mCADFAT);
	unset($mSUBSER); 
	unset($mMOVSER);
	//unset($mDUAL);
	unset($mTABPBH);
	set_time_limit(30);
   	//fclose($mArqMov);
*/
//Close NRFatCol_BeforeShow @4-AC8B00F2
    return $NRFatCol_BeforeShow;
}
//End Close NRFatCol_BeforeShow
?>
