<?php
//BindEvents Method @1-B05BAD9B
function BindEvents()
{
    global $NRFatCol;
    global $CCSEvents;
    $NRFatCol->TxtMes->CCSEvents["BeforeShow"] = "NRFatCol_TxtMes_BeforeShow";
    $NRFatCol->TxtDataAtual->CCSEvents["BeforeShow"] = "NRFatCol_TxtDataAtual_BeforeShow";
    $NRFatCol->TxtVenc->CCSEvents["BeforeShow"] = "NRFatCol_TxtVenc_BeforeShow";
    $NRFatCol->Faturar->CCSEvents["OnClick"] = "NRFatCol_Faturar_OnClick";
    $NRFatCol->ReFaturar->CCSEvents["OnClick"] = "NRFatCol_ReFaturar_OnClick";
    $NRFatCol->CCSEvents["BeforeShow"] = "NRFatCol_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//NRFatCol_TxtMes_BeforeShow @5-4FE62D83
function NRFatCol_TxtMes_BeforeShow(& $sender)
{
    $NRFatCol_TxtMes_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_TxtMes_BeforeShow

//Custom Code @13-2A29BDB7
// -------------------------
    // Write your own code here.01/01/2001
	$NRFatCol->TxtMes->SetValue(substr(CCGetSession("DataMesAnt"),3,7));
// -------------------------
//End Custom Code

//Close NRFatCol_TxtMes_BeforeShow @5-80BBA439
    return $NRFatCol_TxtMes_BeforeShow;
}
//End Close NRFatCol_TxtMes_BeforeShow

//NRFatCol_TxtDataAtual_BeforeShow @22-2295F10C
function NRFatCol_TxtDataAtual_BeforeShow(& $sender)
{
    $NRFatCol_TxtDataAtual_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_TxtDataAtual_BeforeShow

//Custom Code @23-2A29BDB7
// -------------------------
    $NRFatCol->TxtDataAtual->SetValue(CCGetSession("DataSist"));
// -------------------------
//End Custom Code

//Close NRFatCol_TxtDataAtual_BeforeShow @22-DEBB49FF
    return $NRFatCol_TxtDataAtual_BeforeShow;
}
//End Close NRFatCol_TxtDataAtual_BeforeShow

//NRFatCol_TxtVenc_BeforeShow @6-4F46F2C4
function NRFatCol_TxtVenc_BeforeShow(& $sender)
{
    $NRFatCol_TxtVenc_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_TxtVenc_BeforeShow

//Custom Code @14-2A29BDB7
// -------------------------
    // Write your own code here.
    global $DBfaturar;
    $mVencimento = CCGetSession("DataFatu");
	$NRFatCol->TxtVenc->SetValue($mVencimento);
// -------------------------
//End Custom Code

//Close NRFatCol_TxtVenc_BeforeShow @6-B4031850
    return $NRFatCol_TxtVenc_BeforeShow;
}
//End Close NRFatCol_TxtVenc_BeforeShow

//NRFatCol_Faturar_OnClick @10-3B8FF7BB
function NRFatCol_Faturar_OnClick(& $sender)
{
    $NRFatCol_Faturar_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_Faturar_OnClick

//Custom Code @36-2A29BDB7
// -------------------------
  // -------------------------
      $mMes_Ref = $NRFatCol->TxtMes->GetValue();
  
  	//$mDUAL = new clsDBfaturar();
  	//$mDUAL->query("SELECT TO_DATE('01' || SUBSTR(TO_CHAR(SYSDATE,'DD/MM/YYYY'),4,7)) AS DAT_REF FROM DUAL");
      $mDat_Ref = '01/'.$mMes_Ref;//$mDUAL->f("DAT_REF");
  
  	/*
          **********************************************************************
          * Seleciona os servi�os do cliente verificando a vig�ncia do Servi�o *
          **********************************************************************
  	*/


  $mes_ano=explode('/',$mMes_Ref);

  $ult_dia_mes_ref=mktime(0,0,0,$mes_ano[0]+1,1,$mes_ano[1])-1;
  
  $ult_dia_mes_ref=mktime(0,0,0,date('n',$ult_dia_mes_ref),date('j',$ult_dia_mes_ref),date('Y',$ult_dia_mes_ref));
  

  $pri_dia_mes_ref=mktime(0,0,0,$mes_ano[0],1,$mes_ano[1]);
  $dias_mes_ref=floor(($ult_dia_mes_ref-$pri_dia_mes_ref+(1*86400))/86400);
  //+(1*86400) /86400

  set_time_limit(600);
     	//$mArqMov = fopen("c:\sistfat\fatura4.txt","w");
     	//fwrite($mArqMov,"Inicio\r\n");
  	//$mCADCLI = new clsDBfaturar();
  	$mSUBSER = new clsDBfaturar();
  	$mCADFAT = new clsDBfaturar();
  	$mEQUSER = new clsDBfaturar();
  	$mMOVFAT = new clsDBfaturar();
  	$mTABPBH = new clsDBfaturar();
  
  	$mCADFAT->query("SELECT LPAD(TO_CHAR(MAX(TO_NUMBER(CODFAT))),6,'0') AS COD_FAT FROM CADFAT_CANC ORDER BY CODFAT");
  	$mCADFAT->next_record();
    $mCod_Fat1 = $mCADFAT->f("COD_FAT");
  	$mCADFAT->query("SELECT LPAD(TO_CHAR(MAX(TO_NUMBER(CODFAT))),6,'0') AS COD_FAT FROM CADFAT ORDER BY CODFAT");
  	$mCADFAT->next_record();
      $mCod_Fat2 = $mCADFAT->f("COD_FAT");
  
      $mCod_Fat3 = max((int)$mCod_Fat1,(int)$mCod_Fat2);
      $mCod_Fat3 = (int)$mCod_Fat3;
  
  	$mMOVSER = new clsDBfaturar();

	$aux_data = explode('/',$mDat_Ref);
	$aux_data[2] = substr($aux_data[2],0,4);
	$mDat_Ref_fim  = date('d/m/Y',mktime(0,0,0,$aux_data[1]+1,$aux_data[0]-1,$aux_data[2]));

/*
  	$mMOVSER->query("SELECT 
  	                   m.CODCLI    as CODCLI,
  					   m.QTDMED    as QTDMED,
					   m.IDSERCLI  as IDSERCLI,
  					   m.SUBSER    as SUBSER,
  					   m.DATINI    as DATINI,
  					   m.DATFIM    as DATFIM,
  					   m.INIRES    as INIRES,
  					   m.FIMRES    as FIMRES,
  					   m.LOGSER    as LOGSER,
  					   m.BAISER    as BAISER,
  					   m.CIDSER    as CIDSER,
  					   m.ESTSER    as ESTSER,
  					   c.CODSIT    as CODSIT,
  					   c.PGTOELET  as PGTOELET,
					   c.GERBLTO   as GERBLTO
  					FROM 
  					   MOVSER m,
  					   CADCLI c
  					WHERE 
                       ((
  					      TO_DATE('$mDat_Ref') BETWEEN m.DATINI AND m.DATFIM 
  					   ) 
                       or
                       (
  					      m.DATINI BETWEEN TO_DATE('$mDat_Ref') AND TO_DATE('$mDat_Ref_fim')
  					   )
                 
                       or
                       (
  					      m.DATFIM BETWEEN TO_DATE('$mDat_Ref') AND TO_DATE('$mDat_Ref_fim')
  					   )
                 
                       )
					   
					   AND
                         (
  					      (m.INIRES IS NULL OR TO_DATE('$mDat_Ref') <= m.INIRES) OR 
  						  (m.FIMRES IS NULL OR TO_DATE('$mDat_Ref') >= m.FIMRES)
  					   ) AND
  					   (
  					   		m.CODCLI = c.CODCLI AND
  							c.CODSIT <> 'I'
  					   )
                    
  					ORDER BY 
  					   CODCLI,
  					   SUBSER
  					");
*/

  	$mMOVSER->query("SELECT


                         m.CODCLI    as CODCLI,
                         m.QTDMED    as QTDMED,
                       m.IDSERCLI  as IDSERCLI,
                         m.SUBSER    as SUBSER,
                         m.DATINI    as DATINI,
                         m.DATFIM    as DATFIM,
                         m.INIRES    as INIRES,
                         m.FIMRES    as FIMRES,
                         m.LOGSER    as LOGSER,
                         m.BAISER    as BAISER,
                         m.CIDSER    as CIDSER,
                         m.ESTSER    as ESTSER,
                         c.CODSIT    as CODSIT,
                         c.PGTOELET  as PGTOELET,
                       c.GERBLTO   as GERBLTO,
                       m.TIPSER as TIPSER,
                       m.SERENC as SERENC
                      FROM
                         MOVSER m,
                         CADCLI c
                      WHERE
                  
              (
                    ( 
                     (m.TIPSER='N' or m.TIPSER is null  )
                      and
                       ((
                            TO_DATE('$mDat_Ref') BETWEEN m.DATINI AND m.DATFIM
                         )
                       or
                       (
                            m.DATINI BETWEEN TO_DATE('$mDat_Ref') AND TO_DATE('$mDat_Ref_fim')
                         )
                
                       or
                       (
                            m.DATFIM BETWEEN TO_DATE('$mDat_Ref') AND TO_DATE('$mDat_Ref_fim')
                         )
                
                       )
                      
                       )
                       
                    or
                   
                    (
                     (m.TIPSER='S' and m.SERENC='N' )
                     and
                     m.DATINI < TO_DATE('$mDat_Ref')
                    )



                       )
                      
                       AND
                         (
                            (m.INIRES IS NULL OR TO_DATE('$mDat_Ref') <= m.INIRES) OR
                            (m.FIMRES IS NULL OR TO_DATE('$mDat_Ref') >= m.FIMRES)
                         ) AND
                         (
                                 m.CODCLI = c.CODCLI AND
                              c.CODSIT <> 'I'
                         )
                   
                      ORDER BY
                         CODCLI,
                         SUBSER
  					");

  	$mMOVSER->next_record();
  	//$mMOVSER->num_rows

    $idusuariosistema=CCGetSession("IDUsuario");


  	
  	do //while ($mMOVSER->f("CODCLI") != "")//($mMOVSER->next_record())
  	{
     		//fwrite($mArqMov,"movser\r\n");
      	$mCod_Cli  = $mMOVSER->f("CODCLI");
		$mGerBLTO  = $mMOVSER->f("GERBLTO");
          $wCod_Cli  = $mCod_Cli;
          $mVal_Fat  = 0;
          $mRet_INSS = 0;
          $mVal_Ser  = 0;
  		//$mCADCLI->query("SELECT CODSIT,PGTOELET FROM CADCLI WHERE CODCLI='$mCod_Cli'");
  		//$mCADCLI->next_record();
  		//$mCODSIT = $mCADCLI->f("CODSIT");
          //$mPgto_Elet = $mCADCLI->f("PGTOELET");
  		//$mCODSIT    = $mMOVSER->->f("CODSIT");
  		$mPgto_Elet = $mMOVSER->f("PGTOELET");
          //mChave     = wCod_Cli + mMes_Ref
  
  			$mCADFAT->query("SELECT CODFAT FROM CADFAT WHERE CODCLI = '$mCod_Cli' AND MESREF = '$mMes_Ref'");
  			//$mCod_Fat = '0';
  			if ($mCADFAT->next_record())
  			{
     				//fwrite($mArqMov,"Fatura(if)--> ".$mCod_Fat."\r\n");
              	$mCod_Fat = $mCADFAT->f("CODFAT");
              	$Crifat   = false;
  			}
              else
  			{
  				/*
              		*
              		* Gera Novo C�digo de Fatura
              		*
  				*/
     				//fwrite($mArqMov,"Fatura(else)--> ".$mCod_Fat."\r\n");
  				$mCod_Fat3=$mCod_Fat3 + 1;
  				//if (is_null($mCADFAT->f("COD_FAT")))
  				{
  					$mCod_Fat = str_pad((string)$mCod_Fat3,6,'0',STR_PAD_LEFT);
  				}
  				//else
  				//{
  				//	
  				//	$mCod_Fat = $mCADFAT->f("COD_FAT");
  				//}
              	$Crifat = true;
              }
  		while ($mCod_Cli == $mMOVSER->f("CODCLI"))
  		{
     			//fwrite($mArqMov,"   Cliente--> ".$mCod_Cli."\r\n");
            $mSub_Ser = $mMOVSER->f("SUBSER");
            $mQtd_Med = $mMOVSER->f("QTDMED");
  			$mQtd_Med = (float)(str_replace(",", ".", $mQtd_Med));
  			$mDatini  =	$mMOVSER->f("DATINI");
  			$mDatfim  =	$mMOVSER->f("DATFIM");
			$aux_data = explode('/',$mDatini);
			$aux_data[2] = substr($aux_data[2],0,4);
			$D_mDatini  = mktime(0,0,0,$aux_data[1],$aux_data[0],$aux_data[2]);
			$aux_data = explode('/',$mDatfim);
			$aux_data[2] = substr($aux_data[2],0,4);
			$D_mDatfim  = mktime(0,0,0,$aux_data[1],$aux_data[0],$aux_data[2]);
			$dias_periodo=0;


			if(($D_mDatini<=$pri_dia_mes_ref) && ($D_mDatfim>=$ult_dia_mes_ref) )
			  {
               $valor_mes_cheio=true;
			  }
             else
			  {
               $valor_mes_cheio=false;
			  }


  			
  			$mSUBSER->query("SELECT GRPSER FROM SUBSER WHERE SUBSER='$mSub_Ser'");

  			$mSUBSER->next_record();
  			$mGrp_Ser = $mSUBSER->f("GRPSER");
			$midsercli = $mMOVSER->f("IDSERCLI");
  
  			/*
                 	************************************************
                 	* Seleciona Equivalencia do Servi�o
                 	*
                  	* Servi�o + M�s de Ref.
                 	* - Pega a Equival�ncia do Servi�o
                 	* - Monta chave p/ pr�xima procura
                 	************************************************
  			*/
  			$mEQUSER->query("SELECT EQUPBH FROM EQUSER WHERE SUBSER = '$mSub_Ser' AND MESREF = '$mMes_Ref' ORDER BY SUBSER,MESREF");
  			$mEQUSER->next_record();
  			$l_equpbh = $mEQUSER->f("EQUPBH");
  			//fwrite($mArqMov,"Equivalencia--> ".$l_equpbh."\r\n");
  			$l_equpbh = (float)(str_replace(",", ".", $l_equpbh));
  			//fwrite($mArqMov,"Equivalencia--> ".$l_equpbh."\r\n");
  	        /*
              *********************************
              *********************************
              **                             **
              **   Calcula valor do Servi�o  **
              **                             **
              *********************************
              *********************************
  			*/
  			$l_valpbh = $NRFatCol->TxtUfir->GetValue();
  			$l_valpbh = (float)(str_replace(",", ".", $l_valpbh));
			

            $mVal_Uni = $l_equpbh * $l_valpbh;
			$mVal_Ser = round($mQtd_Med * $mVal_Uni,2);

            if($valor_mes_cheio==true)
			  {
               $mVal_Ser = round($mQtd_Med * $mVal_Uni,2);
			  }
             else
			  {
               $aux_ini_periodo=0;
			   $aux_fim_periodo=0;
			   if($D_mDatini>$pri_dia_mes_ref)
			     {
                  $aux_ini_periodo=$D_mDatini;
				 }
                else
				 {
                  $aux_ini_periodo=$pri_dia_mes_ref;
				 }


			   if($D_mDatfim<$ult_dia_mes_ref)
			     {
                  $aux_fim_periodo=$D_mDatfim;
				 }
                else
				 {
                  $aux_fim_periodo=$ult_dia_mes_ref;
				 }

               //Na hora desta conta a data esta no formato timestamp e nele 86400 equivale a 1 dia
			   
               $dias_periodo=floor(($aux_fim_periodo-$aux_ini_periodo+(1*86400))/86400);
			   $mVal_Ser = round($mQtd_Med * $mVal_Uni,2);
			   
			   //Calcula o valor proporcional da fatura
               $mVal_Ser = (  ($mQtd_Med * $mVal_Uni)/$dias_mes_ref)*$dias_periodo;
			   $mVal_Ser = round( $mVal_Ser   ,2);
			   

			  }

            

            
            $mVal_Fat += $mVal_Ser;
     			//fwrite($mArqMov,"Equivalencia--> ".$l_equpbh."\r\n");
     			//fwrite($mArqMov,"UFIR--> ".$l_valpbh."\r\n");
     			//fwrite($mArqMov,"Medi��o--> ".$mQtd_Med."\r\n");
     			//fwrite($mArqMov,"Unitario--> ".$mVal_Uni."\r\n");
  			//fwrite($mArqMov,"Sevi�o--> ".$mVal_Ser."\r\n");
     			//fwrite($mArqMov,"Fatura--> ".$mVal_Fat."\r\n");
              /*                                                                 
              *******************************************************
             	* Grava Movimento da Fatura
             	*******************************************************
            	* Procura p/ Grupo Servi�o + Servi�o + M�s de Ref. +
              * C�digo do Cliente
              * - Caso achar:
              *   a) Grava a Medi��o e a Equival�ncia do Servi�o
              * - Caso Nao Achar:
              *   a)Grava:
              *     - Quant. da Medi��o
              *     - Valor da Equival�ncia
              ********************************************************
  			*/
  			$mMOVFAT->query("SELECT EQUPBH FROM MOVFAT WHERE GRPSER = '$mGrp_Ser' AND SUBSER = '$mSub_Ser' AND MESREF = '$mMes_Ref' AND CODCLI = '$mCod_Cli' AND IDSERCLI=$midsercli ORDER BY SUBSER,MESREF");
  			//$mQtd_Med = str_replace(",", ".", $mQtd_Med."");
  			//$l_equpbh = str_replace(",", ".", $l_equpbh."");
  			//$mVal_Ser = str_replace(",", ".", $mVal_Ser."");
  			if ($mMOVFAT->next_record())
  			{
     				//fwrite($mArqMov,"Altera--> movfat  FATURA = $mCod_Fat CLIENTE = $mCod_Cli \r\n");
  				//fwrite($mArqMov,"UPDATE MOVFAT SET QTDMED=$mQtd_Med,EQUPBH=$l_equpbh,VALSER=$mVal_Ser  WHERE GRPSER = '$mGrp_Ser' AND SUBSER = '$mSub_Ser' AND MESREF = '$mMes_Ref' AND CODCLI = '$mCod_Cli'\r\n");
  				$mMOVFAT->query("UPDATE MOVFAT SET QTDMED=$mQtd_Med,EQUPBH=$l_equpbh,VALSER=$mVal_Ser WHERE GRPSER = '$mGrp_Ser' AND SUBSER = '$mSub_Ser' AND MESREF = '$mMes_Ref' AND CODCLI = '$mCod_Cli' ");	
  			}
  			else
  			{
     				//fwrite($mArqMov,"Insere--> movfat  FATURA = $mCod_Fat CLIENTE = $mCod_Cli \r\n");
  				//fwrite($mArqMov,"INSERT INTO MOVFAT(CODFAT,CODCLI,GRPSER,SUBSER,MESREF,QTDMED,EQUPBH,VALSER) VALUES ('$mCod_Fat','$mCod_Cli','$mGrp_Ser','$mSub_Ser','$mMes_Ref',$mQtd_Med,$l_equpbh,$mVal_Ser)\r\n");
  				$mMOVFAT->query("INSERT INTO MOVFAT(CODFAT,CODCLI,GRPSER,SUBSER,MESREF,QTDMED,EQUPBH,VALSER,IDSERCLI) VALUES ('$mCod_Fat','$mCod_Cli','$mGrp_Ser','$mSub_Ser','$mMes_Ref',$mQtd_Med,$l_equpbh,$mVal_Ser,$midsercli)");
  			}
  			$mQtd_Med = 0.00;
  			$l_equpbh = 0.00;
  			$mVal_Ser = 0.00;
  
  			$mLOGSER = $mMOVSER->f("LOGSER");
  			//$mLOGSER = str_replace(",", " ", $mLOGSER);
  			//$mLOGSER = str_replace("'", " ", $mLOGSER);
  
  			//$mLOGSER = (str_replace(",", " ", $mLOGSER);
  			$mBAISER = $mMOVSER->f("BAISER");
  			//$mBAISER = str_replace(",", " ", $mBAISER);
  			//$mBAISER = str_replace("'", " ", $mBAISER);
  
  			$mCIDSER = $mMOVSER->f("CIDSER");
  			//$mCIDSER = str_replace(",", " ", $mCIDSER);
  			//$mCIDSER = str_replace("'", " ", $mCIDSER);
  
  			$mESTSER = $mMOVSER->f("ESTSER");
  			//$mESTSER = str_replace(",", " ", $mESTSER);
  			//$mESTSER = str_replace("'", " ", $mESTSER);
  			
  			if ($mMOVSER->next_record())
  			{
  				$fim = true;
  			}
  			else
  			{
  				$fim = false;
  				break;
  			}
  			//fwrite($mArqMov,"Fim--> $fim\r\n");
  		}
  		$mINSS_MIM = CCGetSession("mINSS_MIM");
  		$mINSS_MIM = (float)(str_replace(",", ".", $mINSS_MIM));
  
     		$mTX_INSS = CCGetSession("mINSS");
  		$mTX_INSS = (float)(str_replace(",", ".", $mTX_INSS));
  
     		$mTX_ISSQN = CCGetSession("mISSQN");
  		$mTX_ISSQN = (float)(str_replace(",", ".", $mTX_ISSQN));
  
          if ($mVal_Fat > 0)
  		{
     			//fwrite($mArqMov,"Fatura maior que zero--> ".$mVal_Fat."\r\n");
  			$mAnoMes = 0+(substr($mMes_Ref,3,4).substr($mMes_Ref,0,2));
          	if ($mAnoMes > 199901)
  			{
             		/*
             		*******************************************
             		* RETENSAO INSS do Valor da Fatura
             		*******************************************
  				*/
             		$nInss = round(($mVal_Fat*$mTX_INSS/100),2);
     			    //fwrite($mArqMov,"inss--> ".$nInss."\r\n");
  				//fwrite($mArqMov,"inss calculado--> ".($mVal_Fat*$mTX_INSS/100)."\r\n");
  		 		if ($mAnoMes < 200812)
  		  		{
                 		$mISSQN = round(($mVal_Fat)*$mTX_ISSQN/100,2);
     			    	//fwrite($mArqMov,"issqn--> ".$mISSQN."\r\n");
             		}
  		  		else
  		  		{
                 		$mISSQN = 0;
             		}
             		if ($nInss <= $mINSS_MIM)
  		  		{
                 		$nInss=0;
             		}
  				//fwrite($mArqMov,"inss m�nimo--> ".$mINSS_MIM."\r\n");
  				//fwrite($mArqMov,"inss_2--> ".$nInss."\r\n");
          	}
          	else
  			{
             		$mISSQN = 0;
             		$nInss = 0;
          	}
          	$mRet_Inss = $nInss;
  			/*
          	****************************
          	*                          *
          	* Grava ou Regrava Fatura  *
          	*                          *
          	****************************
  			*/
  			$g_dtsist  = CCGetSession("DataSist");
  			$l_datvnc  = $NRFatCol->TxtVenc->GetValue();
  			//$mVal_Fat  = str_replace(",", ".", $mVal_Fat."");
  			//$mISSQN    = str_replace(",", ".", $mISSQN."");
  			//$mRet_INSS = str_replace(",", ".", $mRet_INSS."");
  		    $mTaxa_Juros = CCGetSession("mTAXA_JUROS");
  			$mTaxa_Juros = (float)(str_replace(",", ".", $mTaxa_Juros));
  			// Juros proporcionais. A taxa de juros � mansal e proporcional ao
  			// dia.
  			// Gra�as a Deus por Jesus Cristo.
  			$mMulta = (($mVal_Fat-$mRet_Inss) * ($mTaxa_Juros/100/30));
  			//$mMulta = (($mVal_Fat * ($mTaxa_Juros/100))/30);
          	if ($Crifat)
  			{
     			    //fwrite($mArqMov,"Insere--> cadfat  FATURA = $mCod_Fat CLIENTE = $mCod_Cli \r\n");
  				//fwrite($mArqMov,"INSERT INTO CADFAT(codfat,codcli,mesref,datemi,datvnc,PgtoElet,ValFat,Ret_INSS,ISSQN,valmov,valpgt,valcob,export,valjur,valmul,LOGSER,BAISER,CIDSER,ESTSER) VALUES('$mCod_Fat','$mCod_Cli','$mMes_Ref',to_date('$g_dtsist'),to_date('$l_datvnc'),'$mPgto_Elet',$mVal_Fat,$mRet_Inss,$mISSQN,0.00,0.00,0.00,'F',0.00,$mMulta,'$mLOGSER','$mBAISER','$mCIDSER','$mESTSER')\r\n");
  				$mCADFAT->query("INSERT INTO CADFAT(codfat,codcli,mesref,datemi,datvnc,PgtoElet,ValFat,Ret_INSS,ISSQN,valmov,valpgt,valcob,export,valjur,valmul,LOGSER,BAISER,CIDSER,ESTSER,GERBLTO,IDMOD,DATAMOD) VALUES('$mCod_Fat','$mCod_Cli','$mMes_Ref',to_date('$g_dtsist'),to_date('$l_datvnc'),'$mPgto_Elet',$mVal_Fat,$mRet_Inss,$mISSQN,0.00,0.00,0.00,'F',0.00,$mMulta,'$mLOGSER','$mBAISER','$mCIDSER','$mESTSER','$mGerBLTO',$idusuariosistema,SYSDATE)");
  			}
  			else
  			{
     			    //fwrite($mArqMov,"Altera--> cadfat  FATURA = $mCod_Fat CLIENTE = $mCod_Cli \r\n");
  				//fwrite($mArqMov,"UPDATE CADFAT SET codfat = '$mCod_Fat',codcli = '$mCod_Cli',mesref = '$mMes_Ref',datemi = to_date('$g_dtsist'),datvnc = to_date('$l_datvnc'),PgtoElet = '$mPgto_Elet',ValFat = $mVal_Fat,Ret_INSS = $mRet_Inss,ISSQN = $mISSQN,valmov = 0.00,valpgt = 0.00,valcob = 0.00,export = 'F',valjur = 0.00,valmul = $mMulta ,LOGSER = '$mLOGSER',BAISER = '$mBAISER',CIDSER = '$mCIDSER',ESTSER = '$mESTSER' WHERE CODCLI = '$mCod_Cli' AND MESREF = '$mMes_Ref'\r\n");
  		   		$mCADFAT->query("UPDATE CADFAT SET codfat = '$mCod_Fat',codcli = '$mCod_Cli',mesref = '$mMes_Ref',datemi = to_date('$g_dtsist'),datvnc = to_date('$l_datvnc'),PgtoElet = '$mPgto_Elet',ValFat = $mVal_Fat,Ret_INSS = $mRet_Inss,ISSQN = $mISSQN,valmov = 0.00,valpgt = 0.00,valcob = 0.00,export = 'F',valjur = 0.00,valmul = $mMulta ,LOGSER = '$mLOGSER',BAISER = '$mBAISER',CIDSER = '$mCIDSER',ESTSER = '$mESTSER', GERBLTO='$mGerBLTO' , IDMOD=$idusuariosistema , DATAMOD=SYSDATE  WHERE CODCLI = '$mCod_Cli' AND MESREF = '$mMes_Ref'");
  			}
  			$mVal_Fat  = 0.00;
  			$mISSQN    = 0.00;
  			$mRet_INSS = 0.00;
  			//fwrite($mArqMov,"                                                                                                 \r\n");
              //fwrite($mArqMov,"-------------------------------------------------------------------------------------------------\r\n");
  			//fwrite($mArqMov,"                                                                                                 \r\n");
          }
  	} while ($fim);
	
     	//fwrite($mArqMov,"F I N A L I Z A D O \r\n");
  	$mTABPBH->query("UPDATE TABPBH SET PROCES = 'T' WHERE MESREF = '$mMes_Ref'");
	
  	$mMOVFAT->close();
  	$mEQUSER->close();
  	$mCADFAT->close();
  	$mSUBSER->close();
  	$mMOVSER->close();
  	unset($mMOVFAT);
  	unset($mEQUSER); 
  	unset($mCADFAT);
  	unset($mSUBSER); 
  	unset($mMOVSER);
  	//unset($mDUAL);
  	unset($mTABPBH);
  	set_time_limit(30);
     	//fclose($mArqMov);
  
// -------------------------
//End Custom Code

//Close NRFatCol_Faturar_OnClick @10-375ACE94
    return $NRFatCol_Faturar_OnClick;
}
//End Close NRFatCol_Faturar_OnClick

//NRFatCol_ReFaturar_OnClick @16-04FA9DB7
function NRFatCol_ReFaturar_OnClick(& $sender)
{
    $NRFatCol_ReFaturar_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_ReFaturar_OnClick

//Custom Code @37-2A29BDB7
// -------------------------
  	/*
         *    O cliente deste sistema as vezes gerava o faturamento coletivo e depois de gera-
         * do precisou desativar um determinado cliente. O cliente � desativado na tabela do
         * CADCLI.DBF e se esta desativa��o ocorrer antes da gera��o do faturamento, tudo vai
         * correr bem. Do contr�rio o mesmo j� foi gerado para o arquivo MOVFAT.DBF e embora
         * o sistema verifique a situa��o do cliente, este permanece no MOVFAT.DBF em virtude
         * da primeira gera��o.
  	*/
  	// N�o testado.Favor testar e apagar este coment�rio.
  	$mMesRef = $NRFatCol->TxtMes->GetValue();
  	$Tabela = new clsDBfaturar();
  	$Tabela->query("DELETE FROM MOVFAT WHERE MESREF = '$mMesRef'");
  	$Tabela->query("DELETE FROM CADFAT WHERE MESREF = '$mMesRef'");
  	$Tabela->close();
  	unset($Tabela);
  	NRFatCol_Faturar_OnClick($sender);
// -------------------------
//End Custom Code

//Close NRFatCol_ReFaturar_OnClick @16-821F2901
    return $NRFatCol_ReFaturar_OnClick;
}
//End Close NRFatCol_ReFaturar_OnClick

//NRFatCol_BeforeShow @4-A6D8211B
function NRFatCol_BeforeShow(& $sender)
{
    $NRFatCol_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_BeforeShow

//Custom Code @19-2A29BDB7
// -------------------------
   //global $DBfaturar;

	$Tabela = new clsDBfaturar();
	$mMesRef = substr(CCGetSession('DataMesAnt'),3,7);
	//Consulta a tabela que guarda UFIR e a situa��o do processo de faturamento para o m�s
	$Tabela->query("SELECT valpbh,proces FROM tabpbh where mesref = '$mMesRef'");

	$mTemUFIR = $Tabela->next_record();
	$Tabela1 = new clsDBfaturar();
	//retorna o primeiro servi�o sem correspondente na tabela de equival�ncia do m�s
	$Tabela1->query("SELECT dessub FROM subser s LEFT JOIN equser e ON s.subser=e.subser AND mesref = '$mMesRef' WHERE e.subser IS NULL AND ROWNUM < 2");
	
    //Antes da valida��o, os bot�es de faturar e refaturar devem estar ocultos
   	$NRFatCol->Faturar->Visible = false;
   	$NRFatCol->ReFaturar->Visible = false;
   
	//se existe algum servi�o que n�o foi cadastrado na tabela de equival�ncia do m�s
	if ($Tabela1->next_record()) {
		$NRFatCol->Label1->SetText('Servico: '.$Tabela1->f('dessub').' nao existe na tabela de equivalencias.');
	}
	//se n�o tem UFIR cadastrada para o m�s
	elseif(!$mTemUFIR) {
		$NRFatCol->MsgUFIR->Visible = true;
	}
	else {
		$NRFatCol->TxtUfir->SetValue($Tabela->f("valpbh"));
		$Tabela->query("select count(codcli) as faturas from cadfat where mesref='$mMesRef'");
		$Tabela->next_record();
		//Se j� foi feito faturamento coletivo para o m�s
		if ($Tabela->f('faturas')!=0)
		{
			$Tabela->query("select count(codcli) as faturas_pagas from cadfat where mesref='$mMesRef' and valpgt <> 0");
			$Tabela->next_record();
			//Se j� tinha faturas pagas, n�o aceita refaturar
			if ($Tabela->f('faturas_pagas')!=0)
				$NRFatCol->MsgFaturasPagas->Visible = true;
			else
				$NRFatCol->ReFaturar->Visible = true;
		}
		//Se ainda n�o foi feito faturamento para o m�s
		else 
		{
			//Permite o faturamento normal
			$NRFatCol->Faturar->Visible = true;
		}
	}

// -------------------------
//End Custom Code

//Close NRFatCol_BeforeShow @4-AC8B00F2
    return $NRFatCol_BeforeShow;
}
//End Close NRFatCol_BeforeShow

//Page_BeforeShow @1-0822BF7D
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $FatCol; //Compatibility
//End Page_BeforeShow

//Custom Code @39-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(53);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow
?>
