<?php
//BindEvents Method @1-034684F9
function BindEvents()
{
    global $NRFatCol;
    $NRFatCol->CODCLI->ds->CCSEvents["BeforeBuildSelect"] = "NRFatCol_CODCLI_ds_BeforeBuildSelect";
    $NRFatCol->TextBox1->CCSEvents["BeforeShow"] = "NRFatCol_TextBox1_BeforeShow";
    $NRFatCol->TextBox4->CCSEvents["BeforeShow"] = "NRFatCol_TextBox4_BeforeShow";
    $NRFatCol->TextBox2->CCSEvents["BeforeShow"] = "NRFatCol_TextBox2_BeforeShow";
    $NRFatCol->Faturar->CCSEvents["OnClick"] = "NRFatCol_Faturar_OnClick";
    $NRFatCol->ReFaturar->CCSEvents["BeforeShow"] = "NRFatCol_ReFaturar_BeforeShow";
    $NRFatCol->ReFaturar->CCSEvents["OnClick"] = "NRFatCol_ReFaturar_OnClick";
    $NRFatCol->CCSEvents["BeforeShow"] = "NRFatCol_BeforeShow";
}
//End BindEvents Method

//NRFatCol_CODCLI_ds_BeforeBuildSelect @31-55FAD1D7
function NRFatCol_CODCLI_ds_BeforeBuildSelect(& $sender)
{
    $NRFatCol_CODCLI_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_CODCLI_ds_BeforeBuildSelect

//Custom Code @32-2A29BDB7
// -------------------------
    // Write your own code here.
  if ($NRFatCol->CODCLI->DataSource->Where <> "") {
    $NRFatCol->CODCLI->DataSource->Where .= " AND ";
  }

  $NRFatCol->CODCLI->DataSource->Where .= "CODSIT <> 'I'";

  If ($NRFatCol->CODCLI->DataSource->Order == "") 
  { 
     $NRFatCol->CODCLI->DataSource->Order = "descli";
  }
// -------------------------
//End Custom Code

//Close NRFatCol_CODCLI_ds_BeforeBuildSelect @31-979EE97A
    return $NRFatCol_CODCLI_ds_BeforeBuildSelect;
}
//End Close NRFatCol_CODCLI_ds_BeforeBuildSelect

//NRFatCol_TextBox1_BeforeShow @5-2B677074
function NRFatCol_TextBox1_BeforeShow(& $sender)
{
    $NRFatCol_TextBox1_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_TextBox1_BeforeShow

//Custom Code @13-2A29BDB7
// -------------------------
    // Write your own code here.01/01/2001
	$NRFatCol->TextBox1->SetValue(substr(CCGetSession("DataMesAnt"),3,7));
// -------------------------
//End Custom Code

//Close NRFatCol_TextBox1_BeforeShow @5-88889EC9
    return $NRFatCol_TextBox1_BeforeShow;
}
//End Close NRFatCol_TextBox1_BeforeShow

//NRFatCol_TextBox4_BeforeShow @22-19CA4251
function NRFatCol_TextBox4_BeforeShow(& $sender)
{
    $NRFatCol_TextBox4_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_TextBox4_BeforeShow

//Custom Code @23-2A29BDB7
// -------------------------
    $NRFatCol->TextBox4->SetValue(CCGetSession("DataSist"));
// -------------------------
//End Custom Code

//Close NRFatCol_TextBox4_BeforeShow @22-0C2BF0A4
    return $NRFatCol_TextBox4_BeforeShow;
}
//End Close NRFatCol_TextBox4_BeforeShow

//NRFatCol_TextBox2_BeforeShow @6-3AFC6197
function NRFatCol_TextBox2_BeforeShow(& $sender)
{
    $NRFatCol_TextBox2_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_TextBox2_BeforeShow

//Custom Code @14-2A29BDB7
// -------------------------
    // Write your own code here.
    global $DBfaturar;
    $mVencimento = CCGetSession("DataFatu");
	$NRFatCol->TextBox2->SetValue($mVencimento);
// -------------------------
//End Custom Code

//Close NRFatCol_TextBox2_BeforeShow @6-F4E9BB12
    return $NRFatCol_TextBox2_BeforeShow;
}
//End Close NRFatCol_TextBox2_BeforeShow

//NRFatCol_Faturar_OnClick @10-3B8FF7BB
function NRFatCol_Faturar_OnClick(& $sender)
{
    $NRFatCol_Faturar_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_Faturar_OnClick

//Custom Code @28-2A29BDB7
// -------------------------
    $mMes_Ref = $NRFatCol->TextBox1->GetValue();
	$codCli = $NRFatCol->CODCLI->GetValue();

	//$mDUAL = new clsDBfaturar();
	//$mDUAL->query("SELECT TO_DATE('01' || SUBSTR(TO_CHAR(SYSDATE,'DD/MM/YYYY'),4,7)) AS DAT_REF FROM DUAL");
    $mDat_Ref = '01/'.$mMes_Ref;//$mDUAL->f("DAT_REF");

	/*
        **********************************************************************
        * Seleciona os servi�os do cliente verificando a vig�ncia do Servi�o *
        **********************************************************************
	*/
	set_time_limit(600);
   	//$mArqMov = fopen("c:\sistfat\fatura4.txt","w");
   	//fwrite($mArqMov,"Inicio\r\n");
	//$mCADCLI = new clsDBfaturar();
	$mSUBSER = new clsDBfaturar();
	$mCADFAT = new clsDBfaturar();
	$mEQUSER = new clsDBfaturar();
	$mMOVFAT = new clsDBfaturar();
	$mTABPBH = new clsDBfaturar();

	$mCADFAT->query("SELECT LPAD(TO_CHAR(MAX(TO_NUMBER(CODFAT))),6,'0') AS COD_FAT FROM CADFAT_CANC ORDER BY CODFAT");
	$mCADFAT->next_record();
    $mCod_Fat1 = $mCADFAT->f("COD_FAT");
	$mCADFAT->query("SELECT LPAD(TO_CHAR(MAX(TO_NUMBER(CODFAT))),6,'0') AS COD_FAT FROM CADFAT ORDER BY CODFAT");
	$mCADFAT->next_record();
    $mCod_Fat2 = $mCADFAT->f("COD_FAT");

    $mCod_Fat3 = max((int)$mCod_Fat1,(int)$mCod_Fat2);
    $mCod_Fat3 = (int)$mCod_Fat3;

	$mMOVSER = new clsDBfaturar();
	$mMOVSER->query("SELECT 
	                   m.CODCLI    as CODCLI,
					   m.QTDMED    as QTDMED,
					   m.SUBSER    as SUBSER,
					   m.DATINI    as DATINI,
					   m.DATFIM    as DATFIM,
					   m.INIRES    as INIRES,
					   m.FIMRES    as FIMRES,
					   m.LOGSER    as LOGSER,
					   m.BAISER    as BAISER,
					   m.CIDSER    as CIDSER,
					   m.ESTSER    as ESTSER,
					   c.CODSIT    as CODSIT,
					   c.PGTOELET  as PGTOELET
					FROM 
					   MOVSER m,
					   CADCLI c
					WHERE 
					   (
					      TO_DATE('$mDat_Ref') BETWEEN m.DATINI AND m.DATFIM 
					   ) AND
                       (
					      (m.INIRES IS NULL OR TO_DATE('$mDat_Ref') < m.INIRES) OR 
						  (m.FIMRES IS NULL OR TO_DATE('$mDat_Ref') > m.FIMRES)
					   ) AND
					   (
					        c.CODCLI = '$codCli'  AND
					   		m.CODCLI = c.CODCLI   AND
							c.CODSIT <> 'I'
					   )
					ORDER BY 
					   CODCLI,
					   SUBSER
					");
	$mMOVSER->next_record();
	//$mMOVSER->num_rows
	

			$mCADFAT->query("SELECT CODFAT FROM CADFAT WHERE CODCLI = '$mCod_Cli' AND MESREF = '$mMes_Ref'");
			//$mCod_Fat = '0';
			if ($mCADFAT->next_record())
			{
   				//fwrite($mArqMov,"Fatura--> ".$mCod_Fat."\r\n");
            	$mCod_Fat = $mCADFAT->f("CODFAT");
            	$Crifat   = false;
			}
            else
			{
				/*
            		*
            		* Gera Novo C�digo de Fatura
            		*
				*/
   				//fwrite($mArqMov,"Fatura--> ".$mCod_Fat."\r\n");
				$mCod_Fat3=$mCod_Fat3 + 1;
				//if (is_null($mCADFAT->f("COD_FAT")))
				{
					$mCod_Fat = str_pad((string)$mCod_Fat3,6,'0',STR_PAD_LEFT);
				}
				//else
				//{
				//	
				//	$mCod_Fat = $mCADFAT->f("COD_FAT");
				//}
            	$Crifat = true;
            }
	do //while ($mMOVSER->f("CODCLI") != "")//($mMOVSER->next_record())
	{
   		//fwrite($mArqMov,"movser\r\n");
    	$mCod_Cli  = $mMOVSER->f("CODCLI");
        $wCod_Cli  = $mCod_Cli;
        $mVal_Fat  = 0;
        $mRet_INSS = 0;
        $mVal_Ser  = 0;
		//$mCADCLI->query("SELECT CODSIT,PGTOELET FROM CADCLI WHERE CODCLI='$mCod_Cli'");
		//$mCADCLI->next_record();
		//$mCODSIT = $mCADCLI->f("CODSIT");
        //$mPgto_Elet = $mCADCLI->f("PGTOELET");
		//$mCODSIT    = $mMOVSER->->f("CODSIT");
		$mPgto_Elet = $mMOVSER->f("PGTOELET");
        //mChave     = wCod_Cli + mMes_Ref

		while ($mCod_Cli == $mMOVSER->f("CODCLI"))
		{
   			//fwrite($mArqMov,"   Cliente--> ".$mCod_Cli."\r\n");
            $mSub_Ser = $mMOVSER->f("SUBSER");
            $mQtd_Med = $mMOVSER->f("QTDMED");
			$mQtd_Med = (float)(str_replace(",", ".", $mQtd_Med));
			
			$mSUBSER->query("SELECT GRPSER FROM SUBSER WHERE SUBSER='$mSub_Ser'");
			$mSUBSER->next_record();
			$mGrp_Ser = $mSUBSER->f("GRPSER");
			/*
               	************************************************
               	* Seleciona Equivalencia do Servi�o
               	*
                	* Servi�o + M�s de Ref.
               	* - Pega a Equival�ncia do Servi�o
               	* - Monta chave p/ pr�xima procura
               	************************************************
			*/
			$mEQUSER->query("SELECT EQUPBH FROM EQUSER WHERE SUBSER = '$mSub_Ser' AND MESREF = '$mMes_Ref' ORDER BY SUBSER,MESREF");
			$mEQUSER->next_record();
			$l_equpbh = $mEQUSER->f("EQUPBH");
			//fwrite($mArqMov,"Equivalencia--> ".$l_equpbh."\r\n");
			$l_equpbh = (float)(str_replace(",", ".", $l_equpbh));
			//fwrite($mArqMov,"Equivalencia--> ".$l_equpbh."\r\n");
	        /*
            *********************************
            *********************************
            **                             **
            **   Calcula valor do Servi�o  **
            **                             **
            *********************************
            *********************************
			*/
			$l_valpbh = $NRFatCol->TextBox3->GetValue();
			$l_valpbh = (float)(str_replace(",", ".", $l_valpbh));
            $mVal_Uni = round($l_equpbh * $l_valpbh,2);
            $mVal_Ser = round($mQtd_Med * $mVal_Uni,2);
            $mVal_Fat += $mVal_Ser;
   			//fwrite($mArqMov,"Equivalencia--> ".$l_equpbh."\r\n");
   			//fwrite($mArqMov,"UFIR--> ".$l_valpbh."\r\n");
   			//fwrite($mArqMov,"Medi��o--> ".$mQtd_Med."\r\n");
   			//fwrite($mArqMov,"Unitario--> ".$mVal_Uni."\r\n");
			//fwrite($mArqMov,"Sevi�o--> ".$mVal_Ser."\r\n");
   			//fwrite($mArqMov,"Fatura--> ".$mVal_Fat."\r\n");
            /*                                                                 
            *******************************************************
           	* Grava Movimento da Fatura
           	*******************************************************
           	* Procura p/ Grupo Servi�o + Servi�o + M�s de Ref. +
            * C�digo do Cliente
            * - Caso achar:
            *   a) Grava a Medi��o e a Equival�ncia do Servi�o
            * - Caso Nao Achar:
            *   a)Grava:
            *     - Quant. da Medi��o
            *     - Valor da Equival�ncia
            ********************************************************
			*/
			$mMOVFAT->query("SELECT EQUPBH FROM MOVFAT WHERE GRPSER = '$mGrp_Ser' AND SUBSER = '$mSub_Ser' AND MESREF = '$mMes_Ref' AND CODCLI = '$mCod_Cli' ORDER BY SUBSER,MESREF");
			//$mQtd_Med = str_replace(",", ".", $mQtd_Med."");
			//$l_equpbh = str_replace(",", ".", $l_equpbh."");
			//$mVal_Ser = str_replace(",", ".", $mVal_Ser."");
			if ($mMOVFAT->next_record())
			{
   				//fwrite($mArqMov,"Altera--> movfat\r\n");
				$mMOVFAT->query("UPDATE MOVFAT SET QTDMED=$mQtd_Med,EQUPBH=$l_equpbh,VALSER=$mVal_Ser  WHERE GRPSER = '$mGrp_Ser' AND SUBSER = '$mSub_Ser' AND MESREF = '$mMes_Ref' AND CODCLI = '$mCod_Cli'");	
			}
			else
			{
   				//fwrite($mArqMov,"Insere--> movfat\r\n");
				$mMOVFAT->query("INSERT INTO MOVFAT(CODFAT,CODCLI,GRPSER,SUBSER,MESREF,QTDMED,EQUPBH,VALSER) VALUES ('$mCod_Fat','$mCod_Cli','$mGrp_Ser','$mSub_Ser','$mMes_Ref',$mQtd_Med,$l_equpbh,$mVal_Ser)");
			}
			$mQtd_Med = 0.00;
			$l_equpbh = 0.00;
			$mVal_Ser = 0.00;

			$mLOGSER = $mMOVSER->f("LOGSER");
			$mLOGSER = str_replace(",", " ", $mLOGSER);
			$mLOGSER = str_replace("'", " ", $mLOGSER);

			//$mLOGSER = (str_replace(",", " ", $mLOGSER);
			$mBAISER = $mMOVSER->f("BAISER");
			$mBAISER = str_replace(",", " ", $mBAISER);
			$mBAISER = str_replace("'", " ", $mBAISER);

			$mCIDSER = $mMOVSER->f("CIDSER");
			$mCIDSER = str_replace(",", " ", $mCIDSER);
			$mCIDSER = str_replace("'", " ", $mCIDSER);

			$mESTSER = $mMOVSER->f("ESTSER");
			$mESTSER = str_replace(",", " ", $mESTSER);
			$mESTSER = str_replace("'", " ", $mESTSER);
			
			if ($mMOVSER->next_record())
			{
				$fim = true;
			}
			else
			{
				$fim = false;
				break;
			}
			//fwrite($mArqMov,"Fim--> $fim\r\n");
		}
		$mINSS_MIM = CCGetSession("INSS_MIM");
   		$mTX_INSS  = CCGetSession("mINSS");
   		$mTX_ISSQN = CCGetSession("mISSQN");
        if ($mVal_Fat > 0)
		{
   			//fwrite($mArqMov,"Fatura maior que zero--> ".$mVal_Fat."\r\n");
        	if ((substr($mMes_Ref,3,4).substr($mMes_Ref,0,2)) > '199901')
			{
           		/*
           		*******************************************
           		* RETENSAO INSS de 11% do Valor da Fatura
           		*******************************************
           		* Data da 1 Altera��o: 30/11/2000
           		*******************************************
           		* Retornar INSS para 11% do Valor da Fatura
           		* - e retirar o Pagto_Elet
           		*******************************************
           		* Data da 2 Altera��o: 05/02/2001
           		*******************************************
				*/
           		$nInss = round(($mVal_Fat)*$mTX_INSS/100,2);
   			    //fwrite($mArqMov,"inss--> ".$nInss."\r\n");
		 		if ((substr($l_mesref,3,4).substr($l_mesref,0,2)) < '200812')
		  		{
               		$mISSQN = round(($mVal_Fat)*$mTX_ISSQN/100,2);
   			    	//fwrite($mArqMov,"issqn--> ".$mISSQN."\r\n");
           		}
		  		else
		  		{
               		$mISSQN = 0;
           		}
           		if ($nInss <= $mINSS_MIM)
		  		{
               		$nInss=0;
           		}
        	}
        	else
			{
           		$mISSQN = 0;
           		$nInss = 0;
        	}
        	$mRet_Inss = $nInss;
			/*
        	****************************
        	*                          *
        	* Grava ou Regrava Fatura  *
        	*                          *
        	****************************
			*/
			$g_dtsist  = CCGetSession("DataSist");
			$l_datvnc  = $NRFatCol->TextBox2->GetValue();
			//$mVal_Fat  = str_replace(",", ".", $mVal_Fat."");
			//$mISSQN    = str_replace(",", ".", $mISSQN."");
			//$mRet_INSS = str_replace(",", ".", $mRet_INSS."");
			$mTaxa_Juros = CCGetSession("mTAXA_JUROS");
			$mMulta = ($mVal_Fat * $mTaxa_Juros) / 30;
        	if ($Crifat)
			{
   			    //fwrite($mArqMov,"Insere--> cadfat\r\n");
   			    //fwrite($mArqMov,"   Fatura--> $mCod_Fat\r\n");
				$mCADFAT->query("INSERT INTO CADFAT(codfat,codcli,mesref,datemi,datvnc,PgtoElet,ValFat,Ret_INSS,ISSQN,valmov,valpgt,valcob,export,valjur,valmul,LOGSER,BAISER,CIDSER,ESTSER) VALUES('$mCod_Fat','$mCod_Cli','$mMes_Ref',to_date('$g_dtsist'),to_date('$l_datvnc'),'$mPgto_Elet',$mVal_Fat,$mRet_Inss,$mISSQN,0.00,0.00,0.00,'F',0.00,$mMulta,'$mLOGSER','$mBAISER','$mCIDSER','$mESTSER')");
			}
			else
			{
   			    //fwrite($mArqMov,"Altera--> cadfat\r\n");
   			    //fwrite($mArqMov,"   Fatura--> $mCod_Fat\r\n");
		   		$mCADFAT->query("UPDATE CADFAT SET codfat = '$mCod_Fat',codcli = '$mCod_Cli',mesref = '$mMes_Ref',datemi = to_date('$g_dtsist'),datvnc = to_date('$l_datvnc'),PgtoElet = '$mPgto_Elet',ValFat = $mVal_Fat,Ret_INSS = $mRet_Inss,ISSQN = $mISSQN,valmov = 0.00,valpgt = 0.00,valcob = 0.00,export = 'F',valjur = 0.00,valmul = $mMulta ,LOGSER = '$mLOGSER',BAISER = '$mBAISER',CIDSER = '$mCIDSER',ESTSER = '$mESTSER' WHERE CODCLI = '$mCod_Cli' AND MESREF = '$mMes_Ref'");
			}
			$mVal_Fat  = 0.00;
			$mISSQN    = 0.00;
			$mRet_INSS = 0.00;
        }
	} while ($fim);
   	//fwrite($mArqMov,"F I N A L I Z A D O \r\n");
	//$mTABPBH->query("UPDATE TABPBH SET PROCES = 'T' WHERE MESREF = '$mMes_Ref'");
	$mMOVFAT->close;
	$mEQUSER->close;
	$mCADFAT->close;
	$mSUBSER->close;
	$mMOVSER->close;
	unset($mMOVFAT);
	unset($mEQUSER); 
	unset($mCADFAT);
	unset($mSUBSER); 
	unset($mMOVSER);
	//unset($mDUAL);
	unset($mTABPBH);
	set_time_limit(30);
   	//fclose($mArqMov);

// -------------------------
//End Custom Code

//Close NRFatCol_Faturar_OnClick @10-375ACE94
    return $NRFatCol_Faturar_OnClick;
}
//End Close NRFatCol_Faturar_OnClick

//DEL  // -------------------------
//DEL  // -------------------------

//NRFatCol_ReFaturar_BeforeShow @16-267545B0
function NRFatCol_ReFaturar_BeforeShow(& $sender)
{
    $NRFatCol_ReFaturar_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_ReFaturar_BeforeShow

//Custom Code @17-2A29BDB7
// -------------------------
    // Write your own code here.
// -------------------------
//End Custom Code

//Close NRFatCol_ReFaturar_BeforeShow @16-768978A6
    return $NRFatCol_ReFaturar_BeforeShow;
}
//End Close NRFatCol_ReFaturar_BeforeShow

//NRFatCol_ReFaturar_OnClick @16-04FA9DB7
function NRFatCol_ReFaturar_OnClick(& $sender)
{
    $NRFatCol_ReFaturar_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_ReFaturar_OnClick

//Custom Code @27-2A29BDB7
// -------------------------
	/*
       *    O cliente deste sistema as vezes gerava o faturamento coletivo e depois de gera-
       * do precisou desativar um determinado cliente. O cliente � desativado na tabela do
       * CADCLI.DBF e se esta desativa��o ocorrer antes da gera��o do faturamento, tudo vai
       * correr bem. Do contr�rio o mesmo j� foi gerado para o arquivo MOVFAT.DBF e embora
       * o sistema verifique a situa��o do cliente, este permanesse no MOVFAT.DBF em virtude
       * da primeira gera��o.
	*/
	// N�o testado.Favor testar e apagar este coment�rio.
	$Tabela = new clsDBfaturar();
	$codCli = $NRFatCol->CODCLI->GetValue();
	$Tabela->query("DELETE FROM MOVFAT WHERE MESREF = '$mMesRef' AND CODCLI = '$codCli'");
	$Tabela->close;
	unset($Tabela);
	NRFatCol_Faturar_OnClick(& $sender);
// -------------------------
//End Custom Code

//Close NRFatCol_ReFaturar_OnClick @16-821F2901
    return $NRFatCol_ReFaturar_OnClick;
}
//End Close NRFatCol_ReFaturar_OnClick

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL      $mMes_Ref = $NRFatCol->TextBox1->GetValue();
//DEL  
//DEL  	//$mDUAL = new clsDBfaturar();
//DEL  	//$mDUAL->query("SELECT TO_DATE('01' || SUBSTR(TO_CHAR(SYSDATE,'DD/MM/YYYY'),4,7)) AS DAT_REF FROM DUAL");
//DEL      $mDat_Ref = '01/'.$mMes_Ref;//$mDUAL->f("DAT_REF");
//DEL  
//DEL  	/*
//DEL          **********************************************************************
//DEL          * Seleciona os servi�os do cliente verificando a vig�ncia do Servi�o *
//DEL          **********************************************************************
//DEL  	*/
//DEL  	set_time_limit(600);
//DEL     	$mArqMov = fopen("c:\sistfat\fatura1.txt","w");
//DEL     	fwrite($mArqMov,"Inicio\r\n");
//DEL  	$mMOVSER = new clsDBfaturar();
//DEL  	$mMOVSER->query("SELECT 
//DEL  	                   m.CODCLI    as CODCLI,
//DEL  					   m.QTDMED    as QTDMED,
//DEL  					   m.SUBSER    as SUBSER,
//DEL  					   m.DATINI    as DATINI,
//DEL  					   m.DATFIM    as DATFIM,
//DEL  					   m.INIRES    as INIRES,
//DEL  					   m.FIMRES    as FIMRES,
//DEL  					   c.CODSIT    as CODSIT,
//DEL  					   c.PGTOELET  as PGTOELET
//DEL  					FROM 
//DEL  					   MOVSER m,
//DEL  					   CADCLI c
//DEL  					WHERE 
//DEL  					   (
//DEL  					      TO_DATE('$mDat_Ref') BETWEEN m.DATINI AND m.DATFIM 
//DEL  					   ) AND
//DEL                         (
//DEL  					      (m.INIRES IS NULL OR TO_DATE('$mDat_Ref') < m.INIRES) OR 
//DEL  						  (m.FIMRES IS NULL OR TO_DATE('$mDat_Ref') > m.FIMRES)
//DEL  					   ) AND
//DEL  					   (
//DEL  					   		m.CODCLI = c.CODCLI AND
//DEL  							c.CODSIT <> 'I'
//DEL  					   )
//DEL  					ORDER BY 
//DEL  					   CODCLI,
//DEL  					   SUBSER
//DEL  					");
//DEL  	$mMOVSER->next_record();
//DEL  	//$mMOVSER->num_rows
//DEL  	
//DEL  	do //while ($mMOVSER->f("CODCLI") != "")//($mMOVSER->next_record())
//DEL  	{
//DEL  	    $mCod_Cli  = $mMOVSER->f("CODCLI");
//DEL  		while ($mCod_Cli == $mMOVSER->f("CODCLI"))
//DEL  		{
//DEL  		 	fwrite($mArqMov,"mCod_Cli--> $mCod_Cli\r\n");
//DEL  			if ($mMOVSER->next_record())
//DEL  			{
//DEL  				$fim = true;
//DEL  			}
//DEL  			else
//DEL  			{
//DEL  				$fim = false;
//DEL  				break;
//DEL  			}
//DEL  			fwrite($mArqMov,"mMOVSER->f('CODCLI'--> ".$mMOVSER->f("CODCLI")."\r\n");
//DEL  			fwrite($mArqMov,"Fim--> $fim\r\n");
//DEL  		}
//DEL  	} while ($fim);
//DEL     	fwrite($mArqMov,"F I N A L I Z A D O \r\n");
//DEL  	unset($mMOVSER);
//DEL  	set_time_limit(30);
//DEL     	fclose($mArqMov);
//DEL  // -------------------------

//NRFatCol_BeforeShow @4-A6D8211B
function NRFatCol_BeforeShow(& $sender)
{
    $NRFatCol_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRFatCol; //Compatibility
//End NRFatCol_BeforeShow

//Custom Code @19-2A29BDB7
// -------------------------
   //global $DBfaturar;
   $Tabela = new clsDBfaturar();
   $mMesRef = substr(CCGetSession("DataMesAnt"),3,7);;
   // Verifica se este mes/ano j� foi faturado. N�o o Sendo, ativa o bot�o de 
   // Faturar.
   $Tabela->query("SELECT valpbh,proces FROM tabpbh where mesref = '$mMesRef'");
   $mTemUFIR = $Tabela->next_record();
   
   $Tabela1 = new clsDBfaturar();
   $Tabela1->query("SELECT subser,dessub FROM subser");
   $mErro = false;
   while ($Tabela1->next_record())
   {
   		$mSubSer = $Tabela1->f("subser");
		$mDesSub = $Tabela1->f("dessub");
   		$Tabela2 = new clsDBfaturar();
   		$Tabela2->query("SELECT mesref FROM equser where subser = '$mSubSer' and mesref = '$mMesRef'");
		if (!$Tabela2->next_record())
		{
			$mErro = true;
			break;
		}
   }
   if (($mTemUFIR) and (!$mErro))
   {
   		$NRFatCol->TextBox3->SetValue($Tabela->f("valpbh"));
   		if ($Tabela->f("proces") == "T")
   		{
			$NRFatCol->Faturar->Visible = false;
			// Verifica se tem fatura j� paga neste mes/ano. Tendo � imposs�vel re-
			// Faturar.
   			$Tabela->query("select count(codcli) as ja_faturado from cadfat where mesref='$mMesRef' and valpgt <> 0");
   			$Tabela->next_record();
			$mJa_Faturado = ($Tabela->f("ja_faturado")!=0);
   			if ($mJa_Faturado)
			{
				// N�o refatura
				$NRFatCol->ReFaturar->Visible = false;
				$NRFatCol->Panel1->Visible = true;
			}
			else
			{
			 	// Refatura
				$NRFatCol->ReFaturar->Visible = true;
				$NRFatCol->Panel1->Visible = false;
			}
   		}
   		else
   		{
			// Faturamento normal
			$NRFatCol->Faturar->Visible = true;
			$NRFatCol->ReFaturar->Visible = false;
			$NRFatCol->Panel1->Visible = false;
   		}
   }
   else
   {
		if (!$mTemUFIR)
   		{
        	// N�o fatura nem refatura
			$NRFatCol->Faturar->Visible = false;
			$NRFatCol->ReFaturar->Visible = false;
			$NRFatCol->Panel1->Visible = false;
			$NRFatCol->Panel2->Visible = true;
		}
   		else
   		{
        	// N�o fatura nem refatura
			// N�o testado.Favor testar e apagar este coment�rio.
			$NRFatCol->Faturar->Visible = false;
			$NRFatCol->ReFaturar->Visible = false;
			$NRFatCol->Panel1->Visible = false;
			$NRFatCol->Panel2->Visible = false;
			$NRFatCol->Label1->SetValue("Servico: $mDesSub n�o existe na tabela de equival�ncias.");
   		}
   }
// -------------------------
//End Custom Code

//Close NRFatCol_BeforeShow @4-AC8B00F2
    return $NRFatCol_BeforeShow;
}
//End Close NRFatCol_BeforeShow
?>
