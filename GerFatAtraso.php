<?php
//Include Common Files @1-5C668CB2
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "GerFatAtraso.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordGerarFaturasAtrazo { //GerarFaturasAtrazo Class @4-2187778F

//Variables @4-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @4-12608563
    function clsRecordGerarFaturasAtrazo($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record GerarFaturasAtrazo/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "GerarFaturasAtrazo";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->lblmensagem = new clsControl(ccsLabel, "lblmensagem", "lblmensagem", ccsText, "", CCGetRequestParam("lblmensagem", $Method, NULL), $this);
            $this->Dataemi_ini = new clsControl(ccsTextBox, "Dataemi_ini", "Dataemi_ini", ccsDate, array("dd", "/", "mm", "/", "yyyy"), CCGetRequestParam("Dataemi_ini", $Method, NULL), $this);
            $this->DatePicker_Dataemi_ini1 = new clsDatePicker("DatePicker_Dataemi_ini1", "GerarFaturasAtrazo", "Dataemi_ini", $this);
            $this->DataEmi_fim = new clsControl(ccsTextBox, "DataEmi_fim", "DataEmi_fim", ccsDate, array("dd", "/", "mm", "/", "yyyy"), CCGetRequestParam("DataEmi_fim", $Method, NULL), $this);
            $this->DatePicker_DataEmi_fim1 = new clsDatePicker("DatePicker_DataEmi_fim1", "GerarFaturasAtrazo", "DataEmi_fim", $this);
            $this->DataVencimento = new clsControl(ccsTextBox, "DataVencimento", "DataVencimento", ccsDate, array("dd", "/", "mm", "/", "yyyy"), CCGetRequestParam("DataVencimento", $Method, NULL), $this);
            $this->DatePicker_DataVencimento1 = new clsDatePicker("DatePicker_DataVencimento1", "GerarFaturasAtrazo", "DataVencimento", $this);
            $this->Button_Insert = new clsButton("Button_Insert", $Method, $this);
        }
    }
//End Class_Initialize Event

//Validate Method @4-8A3DAAE3
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->Dataemi_ini->Validate() && $Validation);
        $Validation = ($this->DataEmi_fim->Validate() && $Validation);
        $Validation = ($this->DataVencimento->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->Dataemi_ini->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DataEmi_fim->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DataVencimento->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @4-3FFD091C
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->lblmensagem->Errors->Count());
        $errors = ($errors || $this->Dataemi_ini->Errors->Count());
        $errors = ($errors || $this->DatePicker_Dataemi_ini1->Errors->Count());
        $errors = ($errors || $this->DataEmi_fim->Errors->Count());
        $errors = ($errors || $this->DatePicker_DataEmi_fim1->Errors->Count());
        $errors = ($errors || $this->DataVencimento->Errors->Count());
        $errors = ($errors || $this->DatePicker_DataVencimento1->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @4-E6418F6A
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_Insert";
            if($this->Button_Insert->Pressed) {
                $this->PressedButton = "Button_Insert";
            }
        }
        $Redirect = "GerFatAtraso.php" . "?" . CCGetQueryString("QueryString", array("ccsForm"));
        if($this->Validate()) {
            if($this->PressedButton == "Button_Insert") {
                $Redirect = "GerFatAtraso.php" . "?" . CCGetQueryString("QueryString", array("ccsForm"));
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick", $this->Button_Insert)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @4-1F8FD692
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->lblmensagem->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Dataemi_ini->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DatePicker_Dataemi_ini1->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataEmi_fim->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DatePicker_DataEmi_fim1->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataVencimento->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DatePicker_DataVencimento1->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->lblmensagem->Show();
        $this->Dataemi_ini->Show();
        $this->DatePicker_Dataemi_ini1->Show();
        $this->DataEmi_fim->Show();
        $this->DatePicker_DataEmi_fim1->Show();
        $this->DataVencimento->Show();
        $this->DatePicker_DataVencimento1->Show();
        $this->Button_Insert->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End GerarFaturasAtrazo Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-134E0BFE
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "GerFatAtraso.html";
$BlockToParse = "main";
$TemplateEncoding = "ISO-8859-1";
$PathToRoot = "./";
//End Initialize Page

//Include events file @1-10670303
include("./GerFatAtraso_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-082779BE
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$GerarFaturasAtrazo = new clsRecordGerarFaturasAtrazo("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->GerarFaturasAtrazo = & $GerarFaturasAtrazo;
$MainPage->rodape = & $rodape;

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "iso-8859-1";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-A8C4B9DA
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "ISO-8859-1");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-52A1FAF1
$cabec->Operations();
$GerarFaturasAtrazo->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-BBC4FB7B
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($GerarFaturasAtrazo);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-B320728A
$cabec->Show();
$GerarFaturasAtrazo->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-48D43DA1
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($GerarFaturasAtrazo);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
