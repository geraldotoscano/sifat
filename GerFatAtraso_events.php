<?php

include("util.php");
//BindEvents Method @1-4F404940
function BindEvents()
{
    global $GerarFaturasAtrazo;
    global $CCSEvents;
    $GerarFaturasAtrazo->Button_Insert->CCSEvents["OnClick"] = "GerarFaturasAtrazo_Button_Insert_OnClick";
    $GerarFaturasAtrazo->CCSEvents["BeforeShow"] = "GerarFaturasAtrazo_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method


//GerarFaturasAtrazo_Button_Insert_OnClick @7-AC6E9771
function GerarFaturasAtrazo_Button_Insert_OnClick(& $sender)
{
    $GerarFaturasAtrazo_Button_Insert_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GerarFaturasAtrazo; //Compatibility
//End GerarFaturasAtrazo_Button_Insert_OnClick

//Custom Code @11-2A29BDB7
// -------------------------



//Verifica��o de erros de inser��o
   
   $erros=false;
   global $faturas_geradas;
   $faturas_geradas=0;
   $ds = new clsDBFaturar();
   $ds_insert = new clsDBFaturar();
   $hoje=CCDLookUp("to_char(sysdate,'dd/mm/yyyy')",'dual','',$ds);


   $data_vencimento=data_tostrdata($GerarFaturasAtrazo->DataVencimento->GetValue());
   $dataemi_ini=data_tostrdata($GerarFaturasAtrazo->Dataemi_ini->GetValue());
   $dataemi_fim=data_tostrdata($GerarFaturasAtrazo->DataEmi_fim->GetValue());

   if($GerarFaturasAtrazo->DataVencimento->GetValue()=="")
     {
      $GerarFaturasAtrazo->Errors->addError('A data do vencimento tem que ser preenchida');

	  $erros=true;
	  $GerarFaturasAtrazo_Button_Insert_OnClick=false;
	 }

   if($GerarFaturasAtrazo->Dataemi_ini->GetValue()=="")
     {
      $GerarFaturasAtrazo->Errors->addError('A data de emiss�o inicial tem que ser preenchida');

	  $erros=true;
	  $GerarFaturasAtrazo_Button_Insert_OnClick=false;
	 }

   if($GerarFaturasAtrazo->DataEmi_fim->GetValue()=="")
     {
      $GerarFaturasAtrazo->Errors->addError('A data de emiss�o final tem que ser preenchida');

	  $erros=true;
	  $GerarFaturasAtrazo_Button_Insert_OnClick=false;
	 }

   if(verificaData($GerarFaturasAtrazo->Dataemi_ini->GetValue(),$GerarFaturasAtrazo->DataEmi_fim->GetValue())<0)
     {
      $GerarFaturasAtrazo->Errors->addError('A data de emiss�o inicial tem que anterior a data de emiss�o final');

	  $erros=true;
	  $GerarFaturasAtrazo_Button_Insert_OnClick=false;

	 }

   if(verificaData(data_todbdata($hoje),$GerarFaturasAtrazo->DataVencimento->GetValue())<1)
     {
      $GerarFaturasAtrazo->Errors->addError('A data de vencimento tem que ser maior que o dia '.$hoje);

	  $erros=true;
	  $GerarFaturasAtrazo_Button_Insert_OnClick=false;

	 }



   if($erros==false)
     {

	
	//    $campos_fatatrs=array();

	    $campos_fatatrs=addsqlcampos('CODCLI','S');
	    $campos_fatatrs=addsqlcampos('CODFATATRS','S',$campos_fatatrs);
	    $campos_fatatrs=addsqlcampos('DATEMI','E',$campos_fatatrs);
	    $campos_fatatrs=addsqlcampos('DATVNC','D',$campos_fatatrs);
    
	    $campos_fatatrs=addsqlcampos('RET_INSS','N',$campos_fatatrs);
	    $campos_fatatrs=addsqlcampos('VALFAT','N',$campos_fatatrs);
	    $campos_fatatrs=addsqlcampos('VALMOV','N',$campos_fatatrs);
	    $campos_fatatrs=addsqlcampos('VALPGT','N',$campos_fatatrs);
	    $campos_fatatrs=addsqlcampos('VALCOB','N',$campos_fatatrs);
	    $campos_fatatrs=addsqlcampos('VALJUR','N',$campos_fatatrs);
	    $campos_fatatrs=addsqlcampos('DATAMOD','E',$campos_fatatrs);
	    $campos_fatatrs=addsqlcampos('IDMOD','N',$campos_fatatrs);
	    $campos_fatatrs=addsqlcampos('FATPGTO','S',$campos_fatatrs);
	    $campos_fatatrs=addsqlcampos('FATATIV','S',$campos_fatatrs);

		$campos_fatatrs_cadfat=addsqlcampos('CODFATATRS','S');
		$campos_fatatrs_cadfat=addsqlcampos('CODFAT','S',$campos_fatatrs_cadfat);






		$data_vencimento='';
		$dataemi_ini='';
		$dataemi_fim='';

	    $data_vencimento=data_tostrdata($GerarFaturasAtrazo->DataVencimento->GetValue());
	    $dataemi_ini=data_tostrdata($GerarFaturasAtrazo->Dataemi_ini->GetValue());
	    $dataemi_fim=data_tostrdata($GerarFaturasAtrazo->DataEmi_fim->GetValue());
   


	    $ds->query("SELECT
	              round(((F.VALFAT-F.RET_INSS)*(1/100/30))*((to_date('$data_vencimento','dd/mm/yyyy')-F.DATVNC)-1),2) AS TotalJurosUni,
	              F.DATVNC,
	              F.CODFAT,
	              F.CODCLI,
	              F.VALFAT,
	              F.VALMOV,
	              F.RET_INSS,
	              F.VALPGT,
	              F.VALCOB,
	              F.VALJUR,
	              F.VALMUL
							FROM
								CADFAT F,
								CADCLI C
							WHERE 
								(
									F.VALPGT IS NULL     OR
									F.VALPGT = 0
								)
                  and c.codcli= f.codcli
                  and c.recobr='S'
	              and (F.datvnc+62)<sysdate
	              and F.datemi>=to_date('$dataemi_ini','dd/mm/yyyy')
	              and F.datemi<=to_date('$dataemi_fim','dd/mm/yyyy')

							ORDER BY
								F.CODCLI");



	    $ds->next_record();

		$usuario_sistema=CCGetUserID();
	    $datavencimento=data_tostrdata($GerarFaturasAtrazo->DataVencimento->GetValue());

		$codcli=$ds->f("CODCLI");
		$codfat=array();
		$ret_inss=0;
		$valfat=0;
		$valmov=0;
		$valpgt=0;
		$valcob=0;
		$valjur=0;

		set_time_limit(540);

		$ds_insert->query("delete from fatatrs_cadfat");
		$ds_insert->query("delete from fatatrs");

		while( true )
		  {
	       if($codcli!=$ds->f("CODCLI"))
		     {

	          $faturas_geradas=$faturas_geradas+1;

			  $ds_insert->query("select LPAD(count(codfatatrs)+1,6,'0') codfatatrz from fatatrs");

			  $ds_insert->next_record();

		  
			  $codfatatrz=$ds_insert->f("codfatatrz");

	          valorsqlcampos('CODFATATRS',$codfatatrz,$campos_fatatrs);
	          valorsqlcampos('CODCLI',$codcli,$campos_fatatrs);
          
	          valorsqlcampos('DATEMI','sysdate',$campos_fatatrs);
		  
	          valorsqlcampos('DATVNC',$datavencimento,$campos_fatatrs);
          
	          valorsqlcampos('RET_INSS',$ret_inss,$campos_fatatrs);
	          valorsqlcampos('VALFAT',$valfat,$campos_fatatrs);
	          valorsqlcampos('VALMOV',$valmov,$campos_fatatrs);
	          valorsqlcampos('VALPGT',$valpgt,$campos_fatatrs);
	          valorsqlcampos('VALCOB',$valcob,$campos_fatatrs);
	          valorsqlcampos('VALJUR',$valjur,$campos_fatatrs);
	          valorsqlcampos('DATAMOD','sysdate',$campos_fatatrs);
	          valorsqlcampos('IDMOD',$usuario_sistema,$campos_fatatrs);
	          valorsqlcampos('FATPGTO','N',$campos_fatatrs);
	          valorsqlcampos('FATATIV','S',$campos_fatatrs);

	          $ds_insert->query("insert into FATATRS ".campossqlcampos($campos_fatatrs)." values ".valoressqlcampos($campos_fatatrs)); 

	          //Registar as faturas que compoem a fatura em atraso
			  for($i=0;$i<count($codfat);$i++)
			    {

				 valorsqlcampos('CODFATATRS',$codfatatrz,$campos_fatatrs_cadfat);
				 valorsqlcampos('CODFAT',$codfat[$i],$campos_fatatrs_cadfat);
				 $ds_insert->query("insert into FATATRS_CADFAT ".campossqlcampos($campos_fatatrs_cadfat)." values ".valoressqlcampos($campos_fatatrs_cadfat)); 

				}
		  

	          $codfat=array();
	          $ret_inss=0;
	          $valfat=0;
	          $valmov=0;
	          $valpgt=0;
	          $valcob=0;
	          $valjur=0;
		  
			  $codcli=$ds->f("CODCLI");

			 }
	       $codfat[]=$ds->f("CODFAT");
	       $ret_inss+=(float)(str_replace(",", ".",$ds->f("RET_INSS")));
	       $valfat+=(float)(str_replace(",", ".",$ds->f("VALFAT")));
	       $valmov+=(float)(str_replace(",", ".",$ds->f("VALMOV")));
	       $valpgt+=(float)(str_replace(",", ".",$ds->f("VALPGT")));
	       $valcob+=(float)(str_replace(",", ".",$ds->f("VALCOB")));
	       $valjur+=(float)(str_replace(",", ".",$ds->f("TotalJurosUni")));

	   

		   if($ds->next_record()==false) break;

		  }

	$_SESSION['_MENSAGEM_PROX_TELA']='Foram geradas '.$faturas_geradas.' faturas';
  }

// -------------------------
//End Custom Code

//Close GerarFaturasAtrazo_Button_Insert_OnClick @7-EE81C57D
    return $GerarFaturasAtrazo_Button_Insert_OnClick;
}
//End Close GerarFaturasAtrazo_Button_Insert_OnClick

//GerarFaturasAtrazo_BeforeShow @4-14FE45C6
function GerarFaturasAtrazo_BeforeShow(& $sender)
{
    $GerarFaturasAtrazo_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GerarFaturasAtrazo; //Compatibility
//End GerarFaturasAtrazo_BeforeShow

//Custom Code @13-2A29BDB7
// -------------------------
    global $faturas_geradas;
    $ds=new clsDBFaturar();
	//select floor(to_date('06/11/2014','dd/mm/yyyy')-sysdate) from dual
	$dias=intval(CCDLookUp('floor(max(datvnc)-sysdate)','fatatrs','',$ds));

	$data_nova_emissao=CCDLookUp("to_char(max(datvnc),'dd/mm/yyyy')",'fatatrs','',$ds);
	if($dias>=-1 && $data_nova_emissao!='')
	  {
       $GerarFaturasAtrazo->lblmensagem->SetValue('A pr�xima gera��o de faturas em atraso s� poder� ser feita ap�s o dia '.$data_nova_emissao);
	   $GerarFaturasAtrazo->Button_Insert->Visible=false;
	  }
// -------------------------
//End Custom Code

//Close GerarFaturasAtrazo_BeforeShow @4-915CCDE9
    return $GerarFaturasAtrazo_BeforeShow;
}
//End Close GerarFaturasAtrazo_BeforeShow

//Page_BeforeShow @1-407C1C2C
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GerFatAtraso; //Compatibility
//End Page_BeforeShow

//Custom Code @20-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(54);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
