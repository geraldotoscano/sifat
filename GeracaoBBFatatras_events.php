<?php
include("filtrostring.php");
//BindEvents Method @1-AF57DCF4
function BindEvents()
{
    global $GridExportar;
    global $CCSEvents;
    $GridExportar->BTNExportar->CCSEvents["OnClick"] = "GridExportar_BTNExportar_OnClick";
    $GridExportar->Button1->CCSEvents["OnClick"] = "GridExportar_Button1_OnClick";
    $GridExportar->CCSEvents["BeforeShow"] = "GridExportar_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//GridExportar_BTNExportar_OnClick @10-00480608
function GridExportar_BTNExportar_OnClick(& $sender)
{
    $GridExportar_BTNExportar_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GridExportar; //Compatibility
//End GridExportar_BTNExportar_OnClick

//Custom Code @17-2A29BDB7
// -------------------------
    // Write your own code here.
       global $DBfaturar;
	   $mtem = True;
       $Page = CCGetParentPage($sender);
	   $mTotRegs = 0;
	   //                            45        14        4        1       5        60        8     20       10          5
   $Tabela = new clsDBfaturar();
   $Tabela->query("SELECT empresa,sigla,endereco,cep,praca,seq_arq FROM tabitau");
   //$articles = array();
   $Tabela->next_record();
   {
	   $mNomeCedente     = $Tabela->f("empresa");
	   $mSiglaCedente    = $Tabela->f("sigla");
	   $mEnderecoCedente = $Tabela->f("endereco");
       $mCEPCedente      = $Tabela->f("cep");
	   $mPracaCedente    = $Tabela->f("praca");
	   $mSeqRemCedente   = number_format($Tabela->f("seq_arq"), 0,'','');//Elimina o ponto decimal.
   }
	   $mEspacos = "                                              "; // 46 espacos
	   //str_pad ( string $input , int $pad_length [, string $pad_string [, int $pad_type ]] )
       $mNomeCedente     = str_pad ($mNomeCedente,45);
       $mSiglaCedente    = str_pad ($mSiglaCedente,10);
       $mEnderecoCedente = str_pad ($mEnderecoCedente,60);
       $mCEPCedente      = str_pad ($mCEPCedente,8);
       $mPracaCedente    = str_pad ($mPracaCedente,20);
       $mSeqRemCedente   = str_pad($mSeqRemCedente,07,"0", STR_PAD_LEFT);
	   $mRegTipo1Header = "01"                  . // (001-002/002)Tipo registro                                                              
	                      "1615"                . // (003-006/004)Prefixo ag�ncia
                          "2"                   . // (007-007/001)DV - Ag�ncia
						  "000021162"           . // (008-016/009)C�digo do cedente
						  "1"                   . // (017-017/001)DV - Cedente
						  "017"                 . // (018-020/003)Carteira
						  "019"                 . // (021-023/003)Varia��o
						  "000000"              . // (024-029/006)Convenio
	                      $mNomeCedente         . // (030-074/045)Nome do cedente
						  $mSiglaCedente        . // (075-084/010)Sigla do cedente
						  "01"                  . // (085-086/002)Tipo de Impress�o = 01 = Boleto
						  $mEnderecoCedente     . // (087-146/060)Endere�o p/ Devolu��o
						  $mCEPCedente          . // (147-154/008)CEP 
                          $mPracaCedente        . // (155-174/020)Pra�a
                          $mSeqRemCedente       . // (175-181/007)Sequencial de Remessa
						  "N"                   . // (182-182/001)N - Nao conferir o sequencial acima
						  "    "                . // (183-186/004)Brancos
						  "TST454  "            . // (187-194/008)TST454 =  ARQUIVO DE TESTE -  CBR454 = ARQUIVO PARA PROCESSAMENTO
                          $mEspacos             . // (195-240/046) Campos 20 ao 22
						  str_pad ("1479953",10). // (241-250/010)N�mero conv�nio e espa�os
						  "\r\n"                ;
	$mDt = CCGetSession("DataSist");
	$mDia = substr($mDt,0,2);
	$mMes = substr($mDt,3,2);
	$mAno = substr($mDt,-4);
	$mArquivo = "EN".$mDia.$mMes.$mAno.".txt";
	$mArqMov = fopen($mArquivo,"w",1);
   //$mArqMov = fopen("lpt1:","w");
   fwrite($mArqMov,$mRegTipo1Header);
   $mTotRegs++;


   //$Tab_CADFAT = new clsDBfaturar();
   //$Tab_CADFAT->query("CREATE INDEX CADFAT_IDX ON CADFAT(CODFAT)");
   //$Tab_CADFAT->execute;
   //$Tab_CADFAT->query("SELECT codfat,codcli,datemi,mesref,datvnc,ValFat,datvnc+30 as limite FROM cadfat where export = false order by codfat");

   
   $Tab_CADFAT = new clsDBfaturar();
   $Tab_CADFAT->query("SELECT 0 as valmul, codfatatrs as codfat,codcli,datemi,ret_inss,'Faturas em atraso' as mesref,datvnc,ValFat+ valjur as ValFat ,datvnc+30 as limite,(select max(LOGSER) from movser where movser.codcli=fatatrs.codcli) as LOGSER,(select max(BAISER) from movser where movser.codcli=fatatrs.codcli) as BAISER,(select max(CIDSER) from movser where movser.codcli=fatatrs.codcli) as CIDSER,(select max(estser) from movser where movser.codcli=fatatrs.codcli) as ESTSER,(ValFat-ret_inss) as ValLiq FROM fatatrs");
   //$articles = array();
   //                    1         2         3         4         5         6         7         8         9        10 
   //           1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
   //linha07 = 'Inscricao do Cliente: 999999 Numero da Fatura: 999999 Mes de Referencia: 55/4444'
   //linha07 = 'Inscricao do Cliente: ' + $mCodCli + ' Numero da Fatura: ' + $mCodFat + ' Mes de Referencia: ' + $mMesRef
   $mRegTipo02 = "";
   $mRegTipo03 = "";
   while ($Tab_CADFAT->next_record())
   {
       //echo "cadfat.dbf";
	   //$a = array();
	   $mCodFat  = $Tab_CADFAT->f("codfat");
	   $mCodCli  = $Tab_CADFAT->f("codcli");
	   $mDatEmi  = $Tab_CADFAT->f("datemi");
       $mMesRef  = $Tab_CADFAT->f("mesref");
	   $mDatVnc  = $Tab_CADFAT->f("datvnc");
	   $mValFat  = $Tab_CADFAT->f("ValFat");
	   $mRetINSS = $Tab_CADFAT->f("ret_inss");
	   $mlimite  = $Tab_CADFAT->f("limite");
	   $mISSQN   = $Tab_CADFAT->f("issqn");;
	   $mtaxames = $Tab_CADFAT->f("valmul");;
	   //$articles[] = $a;
       //fwrite($mArqMov,"Inscricao do Cliente: " . $mCodCli . " Numero da Fatura: " . $mCodFat . " Mes de Referencia: " . $mMesRef."\r\n");

      $Tab_CADCLI = new clsDBfaturar();
      $Tab_CADCLI->query("SELECT descli,cgccpf,logcob,baicob,cidcob,estcob,poscob FROM cadcli where codcli = '".$mCodCli."' order by codcli");
      //$articles = array();
      $Tab_CADCLI->next_record();
      {
	      //$a = array();
	      $mdescli  = $Tab_CADCLI->f("descli");
	      $mcgccpf  = $Tab_CADCLI->f("cgccpf");
	      $mlogcob  = $Tab_CADCLI->f("logcob");
          $mbaicob  = $Tab_CADCLI->f("baicob");
	      $mcidcob  = $Tab_CADCLI->f("cidcob");
	      $mestcob  = $Tab_CADCLI->f("estcob");
	      $mposcob  = $Tab_CADCLI->f("poscob");
		  $l_valfat = 0;
	      //$articles[] = $a;
      }

      $Tab_MOVFAT = new clsDBfaturar();
      $Tab_MOVFAT->query("SELECT equpbh,qtdmed,subser,ValSer,idsercli FROM movfat where codfat='".$mCodFat."' and valser > 0 order by codfat");
      //$articles = array();
	  /*
	     Montagem das linhas tipo 04 dos itens da fatura
	  */
	  $mLinhasReg04 = array();
	  $mIndice = 0;
      while ($Tab_MOVFAT->next_record())
      {
	      //$a = array();
	      $mequpbh = $Tab_MOVFAT->f("equpbh");
	      $mqtdmed = $Tab_MOVFAT->f("qtdmed");
	      $msubser = $Tab_MOVFAT->f("subser");
          $mValSer = $Tab_MOVFAT->f("ValSer");
		  $midsercli = $Tab_MOVFAT->f("idsercli");
	      //$articles[] = $a;

         $Tab_SUBSER = new clsDBfaturar();
         $Tab_SUBSER->query("SELECT subred,unidad FROM subser where subser = '".$msubser."' order by subser");
         //$articles = array();
         $Tab_SUBSER->next_record();
         { 
	         //$a = array();
	         $msubred  = $Tab_SUBSER->f("subred");
	         $munidad  = $Tab_SUBSER->f("unidad");
	         //$articles[] = $a;
         }
         $Tab_TABPBH = new clsDBfaturar();
         $Tab_TABPBH->query("SELECT valpbh from tabpbh where mesref = '".$mMesRef."' order by mesref");
         //$articles = array();
         $Tab_TABPBH->next_record();
         { 
	         //$a = array();
	         $mvalpbh  = $Tab_TABPBH->f("valpbh");
			 $mvaluni = round ($mequpbh * $mvalpbh, 2);
			 $mvaluni = number_format($mvaluni, 2,',','.');
			 $l_valfat += $mValSer;
			 $mValSer = number_format($mValSer, 2,',','.');
			 $mqtdmed = number_format($mqtdmed, 2,',','.');
	         //$articles[] = $a;
         }
		 $mLinhasReg04[$mIndice] = str_pad (substr($msubred,0,40),40). 
		                           "  "                              . 
								   str_pad (substr($mqtdmed."",-7),7). 
								   "  "                              . 
								   str_pad ($munidad,6)              . 
								   " "                               . 
								   str_pad (substr($mvaluni."",-9),9). 
								   "  "                              . 
								   str_pad (substr($mValSer."",-11),11);
		 $mIndice++;
      }
	  //if ($l_valfat > 0)
	  //{
         //fwrite($mArqMov,"04111Inscricao do Cliente: " . $mCodCli . " Numero da Fatura: " . $mCodFat . " Mes de Referencia: " . $mMesRef.);
          //if ($mtem)
		  //{
          //   echo "movfat.dbf";
		  //   $mtem = false;
		  //}
         $Tab_MOVSER = new clsDBfaturar();
         $Tab_MOVSER->query("SELECT logser,baiser,cidser,estser from MOVSER where codcli = '".$mCodCli."' and idsercli = '".$midsercli."' order by codcli,subser");
         //$articles = array();
         $Tab_MOVSER->next_record();
         { 
	         //$a = array();
	         $mlogser  = $Tab_MOVSER->f("logser");
			 $mlogser  = str_pad (substr($mlogser,0,32),32);

	         $mbaiser  = $Tab_MOVSER->f("baiser");
			 $mbaiser  = str_pad (substr($mbaiser,0,18),18);

	         $mcidser  = $Tab_MOVSER->f("cidser");
             $mcidser  = str_pad (substr($mcidser,0,18),18);

	         $mestser  = $Tab_MOVSER->f("estser");
	         //$articles[] = $a;
         }
		 //                     REGISTRO TIPO 02 - I N I C I O
		 if ($mRegTipo02 == "") // Grava esta linha uma �nica vez no arquivo remessa.
		 {
		    $mlimite = substr($mlimite,-2)."/".substr($mlimite,05,02)."/".substr($mlimite,00,04);
		    $mRegTipo02 = "021111"                                                         .
		                  str_pad ("NAO RECEBER APOS ".$mlimite,60)                        .
                          str_pad ("DEVOLVER EM ".$mlimite,60)                             .
                          str_pad ("COBRANCA ESCRITURAL ",60)                              .
                          str_pad ("ISSQN IMUNE PARA AS ENTIDADES DA ESFERA MUNICIPAL.",64).
		                  "\r\n"; 
            fwrite($mArqMov,$mRegTipo02);

		    $mRegTipo02 = "021111"                                 .
			              //        1234567890123456789012345678901234567890123456789012345678901234567890
		                  str_pad ("ATENCAO:Sr CAIXA, DEDUZIR DO VALOR TOTAL, AS RETENCOES ISSQN",60).
                          str_pad ("E INSS.",60)                         .
                          str_pad (" ",60)                         .
                          str_pad (" ",64)                         .
		                  "\r\n"; 
            fwrite($mArqMov,$mRegTipo02);
            $mTotRegs += 2;
		 }
		 //                     REGISTRO TIPO 02 - F    I    M

		 //                     REGISTRO TIPO 03 - I N I C I O
		 if ($mRegTipo03 == "") // Grava esta linha uma �nica vez no arquivo remessa.
		 {
		    $mRegTipo03 = "03111"                                     .
		                  str_pad ($mNomeCedente,80)                  .
                          str_pad ($mEnderecoCedente,80)              .
                          str_pad ($mCEPCedente." ".$mPracaCedente,85).
		                  "\r\n"; 
            fwrite($mArqMov,$mRegTipo03);
            $mTotRegs++;
		 }
		 //                     REGISTRO TIPO 03 - F    I    M


		 //                     REGISTRO TIPO 04 - I N I C I O
		 $mMensagem1 = "Inscricao do Cliente: "     .// Tamanho = 22
		               str_pad($mCodCli,06)         .// Tamanho = 06 total 028
					   " Numero da Fatura: "        .// Tamanho = 19 total 047
					   str_pad($mCodFat,06)         .// Tamanho = 06 total 053
					   " Mes de Referencia: "       .// Tamanho = 20 total 073
					   str_pad($mMesRef,07)         ;// Tamanho = 07 total 080

         $mMensagem2 = "Coleta:"                    .// Tamanho = 07
					   $mlogser                     .// Tamanho = 32 total 039
					   " "                          .// Tamanho = 01 total 040
					   $mbaiser                     .// Tamanho = 18 total 058
					   " "                          .// Tamanho = 01 total 059 
					   $mcidser                     .// Tamanho = 18 total 077 
					   "/"                          .// Tamanho = 01 total 078  
					   str_pad($mestser,2)          ;// Tamanho = 02 total 080 
         //                      1         2         3         4         5         6         7         8 
		 //             12345678901234567890123456789012345678901234567890123456789012345678901234567890
         $mMensagem3 = "DISCRIMINACAO DOS SERVICOS/PRODUTOS       MEDICAO  UNIDAD VR.UNITAR  VALOR TOTAL";

	     $mRegTipo04 ="04"                         .//(001-002/002)Tipo do Registro
		              "1"                          .//(003-003/001)Fonte da Mensagem 1
					  "1"                          .//(004-004/001)Fonte da Mensagem 2
					  "1"                          .//(005-005/001)Fonte da Mensagem 3
					  $mMensagem1                  .//(006-085/080)Mensagem 1
					  $mMensagem2                  .//(086-165/080)Mensagem 2
					  $mMensagem3                  .//(166-245/080)Mensagem 3
					  "     "                      .//(246-250/005)Espa�os
					  "\r\n"                       ;
         fwrite($mArqMov,$mRegTipo04);
         $mTotRegs++;
		 $mRegTipo04 = "04111";
		 for ($i = 0; $i < count($mLinhasReg04); $i++) 
		 {
		    $mRegTipo04 .= $mLinhasReg04[$i];
			if( ($i+1) / 3 == intval(($i+1) / 3) )
			{
			   $mRegTipo04 .= "     "."\r\n";
               fwrite($mArqMov,$mRegTipo04);
               $mTotRegs++;
		       $mRegTipo04 = "04111";
			}
		 }
		 /*
		     Completar aqui o que falta no registro tipo 04. Pode faltar 1, 2 ou 3 sequ�ncias
			 de 80 bytes(caracteres).
		 */
		 //$mISSQN   = round($l_valfat*2/100,2);
	     //$mtaxames = ($l_valfat * 0.01)/30;
		 $mtaxames = number_format($mtaxames, 2,',','.');
		 if (strlen($mRegTipo04) == 5) // Completar com 3 sequ�ncias
		 {
            $mRegTipo04 .= "SUPERINTENDENCIA DE LIMPEZA URBANA-SLU - SECAO COMERCIAL - TELEFONE 3277-9360   ";
		    $mRegTipo04 .= str_pad ("APOS O VENCIMENTO COBRAR MORA DE R$ ".$mtaxames." AO DIA",80); //
            $mRegTipo04 .= "RETENCAO PARA A PREVIDENCIA SOCIAL (INSS) : ".str_pad(number_format($mRetINSS, 2,',','.')."",36);
		    $mRegTipo04 .= "     "."\r\n";
            fwrite($mArqMov,$mRegTipo04);
            $mRegTipo04  = "04111INSS(Inst. Normativa MPS/SRP No.3 de 14/07/2005 - art. 148 - inciso I).";
            $mRegTipo04 .= "RETENCAO DE ISSQN: R$ ".str_pad(number_format($mISSQN, 2,',','.')."",58) ;
			$mRegTipo04 .= "ISSQN IMUNE PARA AS ENTIDADES DA ESFERA MUNICIPAL.                                   "."\r\n";
            fwrite($mArqMov,$mRegTipo04);
            $mRegTipo04  = "04111ATENCAO:Sr CAIXA, DEDUZIR DO VALOR TOTAL, AS RETENCOES ISSQN E INSS        ";
            $mRegTipo04 .= "OBS: E OBRIGATORIO APRESENTAR JUNTO A SLU A COMPROVACAO DO PAGAMENTO DAS RETEN- ";
			$mRegTipo04 .= "COES ACIMA.VIA FAX ATRAVES DO NUMERO 3277-9400.                                      "."\r\n";
			fwrite($mArqMov,$mRegTipo04);
            $mTotRegs += 3;
		 }
		 else
		 {
		    if (strlen($mRegTipo04) == 85) // Completar com 2 sequ�ncias
			{
               $mRegTipo04 .= "SUPERINTENDENCIA DE LIMPEZA URBANA-SLU - SECAO COMERCIAL - TELEFONE 3277-9360   ";
		       $mRegTipo04 .= str_pad ("APOS O VENCIMENTO COBRAR MORA DE R$ ".$mtaxames." AO DIA",80); //
		       $mRegTipo04 .= "     "."\r\n";
               fwrite($mArqMov,$mRegTipo04);
               $mRegTipo04  = "04111RETENCAO PARA A PREVIDENCIA SOCIAL (INSS) : ".str_pad(number_format($mRetINSS, 2,',','.')."",36);
               $mRegTipo04 .= "INSS(Inst. Normativa MPS/SRP No.3 de 14/07/2005 - art. 148 - inciso I).         ";
               $mRegTipo04 .= "RETENCAO DE ISSQN: R$ ".str_pad(number_format($mISSQN, 2,',','.')."",58)."     "."\r\n";
			   fwrite($mArqMov,$mRegTipo04);
			   $mRegTipo04  = "04111ISSQN IMUNE PARA AS ENTIDADES DA ESFERA MUNICIPAL.                              ";
               $mRegTipo04 .= "ATENCAO:Sr CAIXA, DEDUZIR DO VALOR TOTAL, AS RETENCOES ISSQN E INSS             ";
               $mRegTipo04 .= "OBS: E OBRIGATORIO APRESENTAR JUNTO A SLU A COMPROVACAO DO PAGAMENTO DAS RETEN-      "."\r\n";
			   fwrite($mArqMov,$mRegTipo04);
			   $mRegTipo04 = "04111COES ACIMA.VIA FAX ATRAVES DO NUMERO 3277-9400.                                 ";
			   $mRegTipo04 .= str_pad (" ",80); //
			   $mRegTipo04 .= str_pad (" ",85)."\r\n"; //
			   fwrite($mArqMov,$mRegTipo04);
               $mTotRegs += 4;
			}
			else // Completar com 1 sequ�ncia. Tamanho aqui = 85 + 80 = 165
			{
		       $mRegTipo04 .= str_pad ("SUPERINTENDENCIA DE LIMPEZA URBANA-SLU - SECAO COMERCIAL - TELEFONE 3277-9360",85)."\r\n"; //
               fwrite($mArqMov,$mRegTipo04);
		       $mRegTipo04 = str_pad ("04111APOS O VENCIMENTO COBRAR MORA DE R$ ".$mtaxames." AO DIA",85); //
               $mRegTipo04 .= "RETENCAO PARA A PREVIDENCIA SOCIAL (INSS) : ".str_pad(number_format($mRetINSS, 2,',','.')."",36);
               $mRegTipo04 .= "INSS(Inst. Normativa MPS/SRP No.3 de 14/07/2005 - art. 148 - inciso I).              "."\r\n";
			   fwrite($mArqMov,$mRegTipo04);
               $mRegTipo04  = "04111RETENCAO DE ISSQN: R$ ".str_pad(number_format($mISSQN, 2,',','.')."",58);
			   $mRegTipo04 .= "ISSQN IMUNE PARA AS ENTIDADES DA ESFERA MUNICIPAL.                              ";
               $mRegTipo04 .= "ATENCAO:Sr CAIXA, DEDUZIR DO VALOR TOTAL, AS RETENCOES ISSQN E INSS                  "."\r\n";
			   fwrite($mArqMov,$mRegTipo04);
               $mRegTipo04  = "04111OBS: E OBRIGATORIO APRESENTAR JUNTO A SLU A COMPROVACAO DO PAGAMENTO DAS RETEN- ";
			   $mRegTipo04 .= "COES ACIMA.VIA FAX ATRAVES DO NUMERO 3277-9400.                                 ";
			   $mRegTipo04 .= str_pad (" ",85)."\r\n"; //
			   fwrite($mArqMov,$mRegTipo04);
               $mTotRegs += 4;
			}
		 }
		 //                     REGISTRO TIPO 04 - F    I    M

		 $mTipoDoc = (strlen($mcgccpf) == 14)?("2"):("1");
	     $mDatEmi  = substr($mDatEmi,-2).substr($mDatEmi,05,02).substr($mDatEmi,02,02);
		 $mDatVnc  = substr($mDatVnc,-2).substr($mDatVnc,05,02).substr($mDatVnc,02,02);
		 $l_valfat = number_format($l_valfat, 2,'','');
		 //                     REGISTRO TIPO 05 - I N � C I O 
         $mRegTipo05 = "051111".
					   str_pad ("APOS O VENCIMENTO COBRAR MORA DE R$ ".$mtaxames." AO DIA",60)                                           .
                       str_pad ("RETER: INSS R$ ".number_format($mRetINSS, 2,',','.').", ISSQN R$ ".number_format($mISSQN, 2,',','.'),60). 
                       str_pad (" ",60)                                                                                                  .
                       str_pad (" ",64)                                                                                                  .
					   "\r\n";
         fwrite($mArqMov,$mRegTipo05);
         $mTotRegs++;
		 //                     REGISTRO TIPO 05 - F    I    M
		 //                 IN�CIO DA GERA��O DO REGISTRO TIPO 11
		 $mposcob = substr($mposcob,0,5).substr($mposcob,-3);
		 $mcidcob = substr($mcidcob,0,18);
         $mRegTipo11 = "11"                                            .//(001-002/002)Tipo de Registro
		               $mTipoDoc                                       .//(003-003/001)Tipo de documento do sacado 1 = CPF, 2 = CGC
                       str_pad ($mcgccpf,15,"0", STR_PAD_LEFT)         .//(004-018/015)CGC/CPF do sacado.
					   str_pad ($mdescli,60)                           .//(019-078/060)Nome do sacado.
                       str_pad ($mlogcob,60)                           .//(079-138/060)Endereco do sacado.
                       str_pad ($mposcob,08)                           .//(139-146/008)CEP do sacado
                       str_pad ($mcidcob,18)                           .//(147-164/018)Pra�a(cidade) do sacado.
                       str_pad ($mestcob,02)                           .//(165-166/002)UF pra�a do sacado.
                       str_pad ($mDatEmi,06)                           .//(167-172/006)Data da emiss�o DDMMAA 2007-01-17
					   str_pad ($mDatVnc,06)                           .//(173-178/006)Data do vencimento.
					   "N"                                             .//(179-179/001)Aceite.
					   "RC"                                            .//(180-181/002)Esp�cie do titulo. RC = Recibo. ?
                       "1479953".str_pad($mCodFat,10,"0", STR_PAD_LEFT).//(182-198/017)Nosso n�mero.                   ?
                       str_pad ($mCodFat,10)                           .//(199-208/010)N�m do t�t. dado pelo cedente.  ?
					   "09"                                            .//(209-210/002)Tipo de moeda. 09 = Real.
					   "000000000000000"                               .//(211-225/015)Quant. Moeda Variavel.
                       str_pad($l_valfat,15,"0", STR_PAD_LEFT)         .//(226-240/015)Valor do titulo.
					   "00"                                            .//(241-242/002)Prazo para protesto. 00 = n�o h�.
					   "      "                                        .//(243-248/006)Uso do banco.
					   "00"                                            .//(249-250/002)Total de parcelas.              ?
					   "\r\n"                                          ;//(2+1+15+60+60+8+18+2+6+6+1+2+11+1+15+2+10+13+2+6+2).
					   
		 //                 FINAL DA GERA��O DO REGISTRO TIPO 11
		fwrite($mArqMov,$mRegTipo11);
        $mTotRegs++;
		 //$mRegTipo04 = $mRegTipo04 . "12345678901234567890123456789012345678901234567890123456789012345678901234567890     "."\r\n";
         //fwrite($mArqMov,$mRegTipo04);
         //                      1         2         3         4         5         6         7         8         9        10 
         //             1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
         //  linha09 = 'Coleta:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA BBBBBBBBBBBBBBBBBB CCCCCCCCCCCCCCCCCC/EE  
		 //  linha11 = 'DISCRIMINACAO DOS SERVICOS/PRODUTOS       MEDICAO  UNIDAD VR.UNITAR  VALOR TOTAL'
         //             aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa  9999,99  999999 999999,99  99999999,99
         //	 linha&item. = substr(l_subred,0,40) . ' ' . substr($mqtdmed."",-7) . '  ' . $munidad . '   ' . substr($mvaluni."",-9) . '  ' . substr($mValSer."",-11)
         //	 linha&item. = l_subred + ' ' + $mqtdmed + '  ' + l_unidad + '   ' + str (l_valuni, 7, 2) + '  ' + str (l_valser, 11, 2) + space (2)

	  //}
   }
   //                     REGISTRO TIPO 99 - I N � C I O
   $mTotRegs = str_pad($mTotRegs,15,"0", STR_PAD_LEFT);
   $mRegTipo99 = "99".str_pad ($mTotRegs."",248);
   fwrite($mArqMov,$mRegTipo99);
   //                     REGISTRO TIPO 99 - F    I    M

   fclose($mArqMov);
   $Tab_CADFAT_Update = new clsDBfaturar();
   $Tab_CADFAT_Update->query("UPDATE CADFAT SET EXPORT = 'T' WHERE EXPORT = 'F'");
   $Tabela->close;
   $Tab_CADFAT->close;
   $Tab_CADCLI->close;
   $Tab_MOVFAT->close;
   $Tab_SUBSER->close;
   $Tab_TABPBH->close;
   $Tab_MOVSER->close;
   $Tab_CADFAT_Update->close;
   unset($Tabela);
   unset($Tab_CADFAT);
   unset($Tab_CADCLI);
   unset($Tab_MOVFAT);
   unset($Tab_SUBSER);
   unset($Tab_TABPBH);
   unset($Tab_MOVSER);
   unset($Tab_CADFAT_Update);
// -------------------------
//End Custom Code

//Close GridExportar_BTNExportar_OnClick @10-ACB27D29
    return $GridExportar_BTNExportar_OnClick;
}
//End Close GridExportar_BTNExportar_OnClick

//GridExportar_Button1_OnClick @23-820DF594
function GridExportar_Button1_OnClick(& $sender)
{
    $GridExportar_Button1_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GridExportar; //Compatibility
//End GridExportar_Button1_OnClick

//Custom Code @24-2A29BDB7
// -------------------------
    // Write your own code here.
// -------------------------
//End Custom Code

	$Tab_CADFAT = new clsDBfaturar();
/*
	$Tab_CADFAT->query("SELECT COUNT(*) AS l_total_ex FROM cadfat where export = 'F'");
	$Tab_CADFAT->next_record();
*/
//	if (((float)($Tab_CADFAT->f("l_total_ex"))) != 0)

    $linha_detalhe=new TFiltrostring();
	$linha_detalhe->add('registro',1,'1');
    $linha_detalhe->add('codinscricao',2,'0',true);
	$linha_detalhe->add('numinscricao',14,'0',true);
	$linha_detalhe->add('agencia',4,'0',true);
	$linha_detalhe->add('zeros',2,'0',true);
	$linha_detalhe->add('conta',5,'0',true);
	$linha_detalhe->add('dac',1,'0',true);

	$linha_detalhe->add('brancos',4,' ',true);
	$linha_detalhe->add('instrucao',4,'0',true);
	$linha_detalhe->add('usoempresa',25,' ',false);
	$linha_detalhe->add('nossonumero',8,'0',true);
	$linha_detalhe->add('qtemodea_i',8,'0',true);
	$linha_detalhe->add('qtemodea_d',5,'0',false);
	$linha_detalhe->add('ncarteira',3,'0',true);
	$linha_detalhe->add('usobanco',21,' ',false);
	$linha_detalhe->add('carteira',1,' ',false);
	$linha_detalhe->add('codocorrencia',2,'0',false);


	$linha_detalhe->add('ndocumento',10,' ',false);
	$linha_detalhe->add('vencimento',6,'0',true);
	$linha_detalhe->add('valortitulo_i',11,'0',true);
	$linha_detalhe->add('valortitulo_d',2,'0',false);
	$linha_detalhe->add('codigobanco',3,'0',true);
	$linha_detalhe->add('agenciacobradora',5,'0',true);
	$linha_detalhe->add('especie',2,'0',true);
	$linha_detalhe->add('aceite',1,' ',true);
	$linha_detalhe->add('dataemissao',6,'0',true);
	$linha_detalhe->add('instrucao1',2,' ',false);
	$linha_detalhe->add('instrucao2',2,' ',false);
	$linha_detalhe->add('juros1dia_i',11,'0',true);
	$linha_detalhe->add('juros1dia_d',2,'0',false);
	$linha_detalhe->add('descontoate',6,'0',false);
	$linha_detalhe->add('valordesconto_i',11,'0',true);
	$linha_detalhe->add('valordesconto_d',2,'0',false);
	$linha_detalhe->add('valoriof_i',11,'0',true);
	$linha_detalhe->add('valoriof_d',2,'0',false);
	$linha_detalhe->add('abatimento_i',11,'0',true);
	$linha_detalhe->add('abatimento_d',2,'0',false);
	$linha_detalhe->add('codigoinscricao',2,'0',true);
	$linha_detalhe->add('numeroinscricao',14,'0',true);

	$linha_detalhe->add('nome',30,' ',false);
	$linha_detalhe->add('brancos',10,' ',false);
	$linha_detalhe->add('logradouro',40,' ',false);
	$linha_detalhe->add('bairro',12,' ',false);
	$linha_detalhe->add('cep',8,'0',true);
	$linha_detalhe->add('cidade',15,' ',false);
	$linha_detalhe->add('estado',2,' ',false);
	$linha_detalhe->add('sacador',30,' ',false);
	$linha_detalhe->add('brancos',4,' ',false);
	$linha_detalhe->add('datamora',6,'0',true);
	$linha_detalhe->add('prazo',2,'0',true);
	$linha_detalhe->add('brancos',1,' ',false);
	$linha_detalhe->add('sequencial',6,'0',true);








	if (true )
	{
		 //$mArqMov = fopen("c:\sistfat\movimeto.txt","w");
		//$mArqMov = fopen("http://sifatslu-hm.pbh.gov.br/movimeto.txt","w");.:/php/includes
		set_time_limit(600);
		$mDt = CCGetSession("DataSist");
		$mDia = substr($mDt,0,2);
		$mMes = substr($mDt,3,2);
		$mAno = substr($mDt,-2);
		$mArquivo = "AT".$mDia.$mMes.$mAno.".txt";

	    $tipo="application/force-download";

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false); // required for certain browsers
    header("Content-Type: $tipo");
    header("Content-Disposition: attachment; filename=".basename($mArquivo)); // informa ao navegador que � tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
    header("Content-Transfer-Encoding: binary");

    //header("Content-Length: ".filesize($mArquivo));

    ob_clean();
    flush();


    //readfile( $mArquivo );


        //header("Content-Type: ".$tipo); // informa o tipo do arquivo ao navegador
        //header("Content-Disposition: attachment; filename=".basename($mArquivo)); // informa ao navegador que � tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
		//$mArqMov = fopen($mArquivo,"w",1);
		$l_taceite = 'N';

    	$mCodUni = CCGetSession("mCOD_UNI");
		/*
		$TabUni = new clsDBfaturar();
		$TabUni->query("SELECT descri FROM tabuni where coduni='$mCodUni'");
		$TabUni->next_record();
		$mUnidade = $TabUni->f("descri");
		$mUnidade = substr($mUnidade,0,32);
		$mUnidade = str_pad($mUnidade,32);
		*/
		$TabItau = new clsDBfaturar();
		$TabItau->query("SELECT numecgc,empresa,sigla,endereco,cep,praca,seq_arq,agencia,CONTACO,DAC FROM tabitau");
		$TabItau->next_record();
		$l_agencia = str_pad(substr ($TabItau->f("agencia"),0,4),4);
		$l_contaco = str_pad(substr ($TabItau->f("CONTACO"),0,5),5);
		$l_dac     = str_pad(substr ($TabItau->f("DAC"),0,1),1);
		$l_empresa = str_pad (substr ($TabItau->f("empresa"),0,30),30);
		$e_numregi = $TabItau->f("numecgc");
		$l_codbanc = "341"; // ou colocar em tabela em codbanc

		if (strlen(trim($TabItau->f("numecgc"))) == 14)
		{
			$e_codinsc = '02';
		}
		else 
		{
			$e_codinsc = '01';
		}

		$g_dtr     = CCGetSession("DataSist");

		$mInicio   = '01REMESSA01COBRANCA       ' . $l_agencia . '00' . $l_contaco . $l_dac . '        ' . $l_empresa . '341BANCO ITAU SA  ' . substr ($g_dtr, 0, 2) . substr ($g_dtr, 3, 2) . substr ($g_dtr, 8, 2) . str_repeat(' ',294).'000001';
		////     fwrite($mArqMov,$mInicio."\r\n");
		print $mInicio."\r\n";

		$CadCli = new clsDBfaturar();
		$MFSSTP = new clsDBfaturar();
		$CadEmp = new clsDBfaturar();

		$Tab_CADFAT->query("SELECT 0 as valmul, codfatatrs as codfat,codcli,datemi,ret_inss,'Faturas em atraso' as mesref,datvnc,ValFat+ valjur as ValFat ,datvnc as limite,(select max(LOGSER) from movser where movser.codcli=fatatrs.codcli) as LOGSER,(select max(BAISER) from movser where movser.codcli=fatatrs.codcli) as BAISER,(select max(CIDSER) from movser where movser.codcli=fatatrs.codcli) as CIDSER,(select max(estser) from movser where movser.codcli=fatatrs.codcli) as ESTSER,(ValFat+valjur-ret_inss) as ValLiq FROM fatatrs");
		$reg = 1;
		while ($Tab_CADFAT->next_record())
		{
		    //                 12   +              6   +                      6  +1 =  25 <-TAMANHO
			$l_titempr = 'NOSSA FATURA' .$Tab_CADFAT->f("codcli") . $Tab_CADFAT->f("codfat").'1';
			$l_numdupl = $Tab_CADFAT->f("codfat") . '1   ';
			$l_datvenc  = substr($Tab_CADFAT->f("datvnc"), 0, 2) . substr ($Tab_CADFAT->f("datvnc"), 3, 2) . substr ($Tab_CADFAT->f("datvnc"), 8, 2);

			$l_valtitu = $Tab_CADFAT->f("ValFat");
			
			$mTemVirgula = strpos($l_valtitu,",");
			if (is_bool($mTemVirgula) && !$mTemVirgula)
			{
				$l_valtitu = $l_valtitu . "00";
			}
			else
			{
				$l_valtitu = trim(substr($l_valtitu, 0, strpos($l_valtitu,","))) . str_pad(substr($l_valtitu, strpos($l_valtitu,",")+1, 2),2,"0", STR_PAD_RIGHT);
			}
			$l_valtitu = str_pad(trim($l_valtitu),13,"0", STR_PAD_LEFT);//str_repeat('0',13 - strlen(trim($l_valtitu)))  . $l_valtitu;//replicate ('0', 13 - strlen($l_valtitu)) . $l_valtitu;

			$mValLiq = $Tab_CADFAT->f("ValLiq"); // Valor montado para a linha tipo 1(Sem ponto e sem v�rgula)
			$lValLiq = number_format((float)str_replace(',','.',$mValLiq), 2,',','.'); // Valor montado para a linha tipo 7(com ponto e v�rgula)

			$mTemVirgula = strpos($mValLiq,",");
			if (is_bool($mTemVirgula) && !$mTemVirgula)
			{
				$mValLiq_i=$mValLiq;
				$mValLiq_d="00";
				$mValLiq = $mValLiq . "00";

			}
			else
			{
				
				$mValLiq_i=trim(substr($mValLiq, 0, strpos($mValLiq,",")));
				$mValLiq_d=str_pad(substr($mValLiq, strpos($mValLiq,",")+1, 2),2,"0", STR_PAD_RIGHT);

				$mValLiq = trim(substr($mValLiq, 0, strpos($mValLiq,","))) . str_pad(substr($mValLiq, strpos($mValLiq,",")+1, 2),2,"0", STR_PAD_RIGHT);


			}

			$mValLiq = str_pad(trim($mValLiq),13,"0", STR_PAD_LEFT);//str_repeat('0',13 - strlen(trim($l_valtitu)))  . $l_valtitu;//replicate ('0', 13 - strlen($l_valtitu)) . $l_valtitu;

			$l_datemis =  substr($Tab_CADFAT->f("datemi"), 0, 2) . substr ($Tab_CADFAT->f("datemi"), 3, 2) . substr ($Tab_CADFAT->f("datemi"), 8, 2);
			
			$l_taxames = $Tab_CADFAT->f("valmul");
			/*
			   O trecho abaixo pega os n primeiros numeros do valor da multa antes da 
			   v�rgula e concatena com os 2 �ltimos numeros depois da v�rgula.
			*/
			$mTemVirgula = strpos($l_taxames,",");
			if (is_bool($mTemVirgula) && !$mTemVirgula)
			{
				$l_taxames_i=$l_taxames;
				$l_taxames_d="00";
				$l_taxames = $l_taxames ."00";

			}
			else
			{
				$l_taxames_i=substr($l_taxames, 0, strpos($l_taxames,","));
				$l_taxames_d=str_pad(substr($l_taxames, strpos($l_taxames,",")+1, 2),2,"0", STR_PAD_RIGHT);
				$l_taxames = substr($l_taxames, 0, strpos($l_taxames,",")) . str_pad(substr($l_taxames, strpos($l_taxames,",")+1, 2),2,"0", STR_PAD_RIGHT);


			}
			$l_taxames = str_pad($l_taxames,13,"0", STR_PAD_LEFT); //str_repeat('0',13 - strlen(trim($l_taxames)))  . $l_taxames;
					
			$mCodCli = $Tab_CADFAT->f("codcli");
			//$CadCli = new clsDBfaturar();
			$CadCli->query("SELECT cgccpf,descli,logcob,baicob,cidcob,estcob,poscob FROM cadcli where codcli='$mCodCli'");
			$CadCli->next_record();

			if (strlen(trim($CadCli->f("cgccpf"))) == 14)
			{
				$s_codinsc = '02';
			}
			else 
			{
				$s_codinsc = '01';
			}
			$s_numregi = str_pad($CadCli->f("cgccpf"),14);
			$s_nomesac = str_pad(substr($CadCli->f("descli"), 0, 30),30);
			$s_logrsac = str_pad($CadCli->f("logcob"),35).'     ';
			$s_bairsac = str_pad(substr ($CadCli->f("baicob"), 0, 12),12," ");
			$s_cidasac = str_pad(substr ($CadCli->f("cidcob"), 0, 15),15);
			$s_estasac = $CadCli->f("estcob");
			$s_estasac = str_pad($s_estasac,2); // corrigido em 07/11/2011 - n�o publicado
			$s_cepsaca = substr($CadCli->f("poscob"),0,5) . substr($CadCli->f("poscob"),6,3);
            $s_cepsaca = str_pad($s_cepsaca,8);// corrigido em 07/11/2011 - n�o publicado
			//$s_suficep = substr(l_cepcob, 6, 3);
			//fwrite($mArqMov,$s_logrsac."\r\n");
			
			$mReterInss = $Tab_CADFAT->f("ret_inss");
			/*
			   O trecho abaixo pega os n primeiros numeros do valor do inss antes da 
			   v�rgula e concatena com os 2 �ltimos numeros depois da v�rgula.
			*/
			$mTemVirgula = strpos($mReterInss,",");
			//if (is_bool($mTemVirgula) && !$mTemVirgula)
			//{
			//	$mReterInss = $mReterInss.".00";
			//}
			//else
			{
				$mReterInss = substr($mReterInss, 0, strpos($mReterInss,",")) ."," .str_pad(substr($mReterInss, strpos($mReterInss,",")+1, 2),2,"0", STR_PAD_RIGHT);
			}

			//$cmensage = 'RETER:INSS R$' . $mReterInss;
			$cmensage = 'VALOR DO INSS JA DEDUZIDO';
			$cmensage = $cmensage . str_repeat(' ',40 - strlen($cmensage));

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
/*
			$l_campo01 = '1' . $e_codinsc . $e_numregi . $l_agencia . '00' . $l_contaco . $l_dac . '    ' . '0000' . $l_titempr . '00000000'.'0000000000000' . '112' . str_repeat(' ',21) . '1'.'01' . $l_numdupl . 
						$l_datvenc . $mValLiq . $l_codbanc . '0000099N' . $l_datemis . '94  ' . $l_taxames . '000000' . str_repeat('0',39) . $s_codinsc . $s_numregi . $s_nomesac . 
						'          ' . $s_logrsac . $s_bairsac . $s_cepsaca . $s_cidasac . $s_estasac . $cmensage . '00 ' . $registro;
*/
			
            
			$linha_detalhe->setvalor('codinscricao',$e_codinsc);
			$linha_detalhe->setvalor('numinscricao',$e_numregi);
			$linha_detalhe->setvalor('agencia',$l_agencia);
			$linha_detalhe->setvalor('conta',$l_contaco);
			$linha_detalhe->setvalor('dac',$l_dac);
			$linha_detalhe->setvalor('usoempresa',$l_titempr);

			$linha_detalhe->setvalor('ncarteira','112');
			$linha_detalhe->setvalor('carteira','1');
			$linha_detalhe->setvalor('codocorrencia','01');
			$linha_detalhe->setvalor('ndocumento',$l_numdupl);
			$linha_detalhe->setvalor('vencimento',$l_datvenc);
			$linha_detalhe->setvalor('valortitulo_i',$mValLiq_i);
			$linha_detalhe->setvalor('valortitulo_d',$mValLiq_d);
			$linha_detalhe->setvalor('codigobanco',$l_codbanc);

			
			$linha_detalhe->setvalor('especie','99');
			$linha_detalhe->setvalor('aceite','N');

			$linha_detalhe->setvalor('dataemissao',$l_datemis);
			$linha_detalhe->setvalor('instrucao1','94');

			$linha_detalhe->setvalor('juros1dia_i',$l_taxames_i);
			$linha_detalhe->setvalor('juros1dia_d',$l_taxames_d);

			$linha_detalhe->setvalor('codigoinscricao',$s_codinsc);

			$linha_detalhe->setvalor('numeroinscricao',$s_numregi);
			$linha_detalhe->setvalor('nome',$s_nomesac);

			$linha_detalhe->setvalor('logradouro',$s_logrsac);
			$linha_detalhe->setvalor('bairro',$s_bairsac);
			$linha_detalhe->setvalor('cep',$s_cepsaca);
			$linha_detalhe->setvalor('cidade',$s_cidasac);
			$linha_detalhe->setvalor('estado',$s_estasac);

			

			$linha_detalhe->setvalor('sacador',$cmensage);

			$linha_detalhe->setvalor('sequencial',$registro);
			
			
			

			////     fwrite($mArqMov,$l_campo01."\r\n");

			//print $l_campo01."\r\n";

            $l_campo01=$linha_detalhe->getstring()."\r\n";

			

			print $l_campo01;

//			$linha_detalhe->print_diagrama();



			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;

			$l_campo01 = '7KJB01' . substr($l_empresa . ' CNPJ: ' . substr($e_numregi,0,2) . '.' . substr ($e_numregi,2,3) . '.' . substr ($e_numregi, 5, 3) . '/' . substr ($e_numregi, 8, 4) . '-' . substr($e_numregi, -2) . str_repeat(' ',30) . 
						substr($l_datvenc,0,2) . '/' . substr ($l_datvenc,2,2) . '/' . substr($l_datvenc,-2) . str_repeat(' ',35),0,128) . '02' . substr(str_repeat(' ',128),0,128) . '03' . substr(str_repeat(' ',19) . '112' . str_repeat(' ',06) . 'R$' . str_repeat(' ',46) . $l_agencia . '/' . $l_contaco . 
						'-' . $l_dac . str_repeat(' ',40),0,128) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$l_valor_ti = substr($l_valtitu,0,strlen($l_valtitu) - 2) . '.' . substr($l_valtitu, -2);
			//fwrite($mArqMov,$l_valor_ti."\r\n");
			$l_valor_ti = number_format((float)$l_valor_ti, 2,',','.');
			//fwrite($mArqMov,$l_valor_ti."\r\n");
			$l_valor_ti = str_pad($l_valor_ti,10,' ',STR_PAD_LEFT);//transform (val ($l_valor_ti), '@E 999,999.99')
			//fwrite($mArqMov,$l_valor_ti."\r\n");
			$mItau05 = '    ' . substr($l_datemis,0,2) . '/' . substr($l_datemis,2,2) . '/' . substr($l_datemis, -2) . str_repeat(' ',7) . $l_numdupl . str_repeat(' ',9) . 'DV' . str_repeat(' ',11) . $l_taceite . str_repeat(' ',31) . 
						$l_valor_ti;

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;

			$l_campo01 = '7KJB04' .substr( str_repeat(' ',128),0,128) . '05'.substr('    ' . substr($l_datemis,0,2) . '/' . substr($l_datemis,2,2) . '/' . substr($l_datemis, -2) . str_repeat(' ',7) . $l_numdupl . str_repeat(' ',9) . 'DV' . str_repeat(' ',11) . $l_taceite . str_repeat(' ',31) . 
						$l_valor_ti . str_repeat(' ',128 - strlen($mItau05)),0,128) . '06' . substr(str_repeat(' ',128),0,128) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$l_logser = $Tab_CADFAT->f("logser");
			$l_logser = str_pad($l_logser,35);
			$l_baiser = $Tab_CADFAT->f("baiser");
			$l_baiser = str_pad($l_baiser,20);
			$l_cidser = $Tab_CADFAT->f("cidser");
			$l_cidser = str_pad($l_cidser,20);
			$l_estser = $Tab_CADFAT->f("estser");
			$l_mesref = $Tab_CADFAT->f("mesref");

			$linha07 = 'Inscricao do Cliente: ' . $mCodCli . '           Numero da Fatura: ' . $l_numdupl . '     Referencia: ' . $l_mesref.'  ';
			$linha08 = str_repeat(' ',100);
			$linha09 = 'Local da Coleta:' . $l_logser . '  ' . $l_baiser . '  ' . $l_cidser . '/' . $l_estser . '  ';

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
			
			$l_campo01  = '7KJB07' .substr( $linha07.str_repeat(' ',128),0,128);
			
			$l_campo01 .= '08' . substr($linha08. str_repeat(' ',128),0,128) . '09' .substr( $linha09.  str_repeat(' ',128),0,128) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";
			/*
			   As duas primeiras letras representam as iniciais das tabelas a saber
			   MF = MovFat.
			   SS = SubSer.
			   TP = TabPbh

			*/
			$l_codfat = $Tab_CADFAT->f("codfat");
			//$MFSSTP = new clsDBfaturar();
			$MFSSTP->query("select c.codfat, c.mesref,to_char(c.datvnc,'dd/mm/yyyy') as datvnc from fatatrs_cadfat fc ,cadfat c where fc.codfat= c.codfat and fc.codfatatrs='".$Tab_CADFAT->f("codfat")."'  order by substr(c.mesref,4,4),mesref");

            for($T=10;$T<19;$T++)
			{
				$lin[$T] = str_repeat(' ',100);
			}

			$cnt = 12;

			while ($MFSSTP->next_record())
			{

/*
				$l_equpbh = $MFSSTP->f("equpbh");
				//fwrite($mArqMov,"Equival�ncia ->$l_equpbh\r\n");
				$l_equpbh = (float)((str_replace(",", ".", $l_equpbh)));
				$l_equpbh = round($l_equpbh,2);
				//fwrite($mArqMov,"Equival�ncia ->$l_equpbh\r\n");

				$l_qtdmed = $MFSSTP->f("qtdmed");
				$l_qtdmed = (float)((str_replace(",", ".", $l_qtdmed)));
				$l_qtdmed = round($l_qtdmed,2);

				$l_subser = $MFSSTP->f("subser");

				$mval_ser = $MFSSTP->f("valser");
				$mval_ser = (float)((str_replace(",", ".", $mval_ser)));
				$mval_ser = round($mval_ser,2);

				$l_subred = str_pad($MFSSTP->f("subred"),58);

				$l_unidad = $MFSSTP->f("unidad");

				$l_valpbh = $MFSSTP->f("valpbh");
				$l_valpbh = (float)((str_replace(",", ".", $l_valpbh)));

				$l_valuni = round ($l_equpbh * $l_valpbh, 2);

				$l_valser = $mval_ser;
				$l_valfat += $l_valser;
*/
//				$lin[$cnt] = $l_subred . ' ' . str_pad(number_format($l_qtdmed, 2,'.',''),8,' ',STR_PAD_LEFT) . '  ' . str_pad($l_unidad,6) . '   ' . str_pad(number_format($l_valuni, 2,'.',''),7,' ',STR_PAD_LEFT) . '  ' . str_pad(number_format($l_valser, 2,'.',''),11,' ',STR_PAD_LEFT) . '  ';
				$lin[$cnt] = 'FATURA '.$MFSSTP->f('codfat').' MES REFER�NCIA '.$MFSSTP->f('mesref').' VENCIMENTO '.$MFSSTP->f('datvnc');
			
				$cnt++;
				if($cnt>=18) break;
			}

			//$lin[10] = str_repeat(' ',100);

			//$CadEmp = new clsDBfaturar();
			$CadEmp->query("SELECT fax,telefo,razsoc,email FROM cademp");
			$CadEmp->next_record();

			$mRazSoc = $CadEmp->f("razsoc");
			//$mRazSoc = substr($mRazSoc,0,32);
			$mRazSoc = str_pad($mRazSoc,40);

			$mTelefo = $CadEmp->f("telefo");
			$mTelefo = str_pad($mTelefo,20);

			$mEmail = $CadEmp->f("email");

	      //$lin[11] = 'DISCRIMINACAO DOS SERVICOS/PRODUTOS                         MEDICAO  UNIDAD VR.UNITAR  VALOR TOTAL  '; Alterado em 

		    $MFSSTP->query("select count(fc.codfat) total from fatatrs_cadfat fc where fc.codfatatrs='".$Tab_CADFAT->f("codfat")."'");
			$MFSSTP->next_record();

			$lin[11] = 'RESUMO DA(S) '.$MFSSTP->f('total').' FATURA(S)';
			//$lin[18] = $mRazSoc . str_repeat(' ',27). ' - ' . 'Telefone: '.$mTelefo;//'SUPERINTENDENCIA DE LIMPEZA URBANA-SLU   -  SECAO COMERCIAL   -  Telefone: 3277.9360                ';
			$lin[18] = str_repeat(' ',100);
			//$lin[$cnt] = $mRazSoc . str_repeat(' ',27). ' - ' . 'Telefone: '.$mTelefo;//'SUPERINTENDENCIA DE LIMPEZA URBANA-SLU   -  SECAO COMERCIAL   -  Telefone: 3277.9360                ';

			$i = 10;
			//           Cliente pode ter at� 7 servi�os - comenteddo em 14/03/2012 - Miguel.
			while ($i<18)
			//while ($i<$cnt)
			{
				$reg++;
				$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;

				$l_campo01  = '7KJB' . $i . substr($lin[$i].str_repeat(' ',128),0,128);
				$i++;
				$l_campo01 .=  $i .substr( $lin[$i]. str_repeat(' ',128),0,128);
				$i++;
				$l_campo01 .=  $i .substr($lin[$i] .  str_repeat(' ',128),0,128) . $registro;
				$i++;

				////     fwrite($mArqMov,$l_campo01."\r\n");
				print $l_campo01."\r\n";
			}

			$naux = $Tab_CADFAT->f("ret_inss");
			$naux = (float)(str_replace(",", ".", $naux));//substitui v�rgula por ponto
			$naux = number_format($naux,2,',','.');
			$naux = str_pad($naux,10,' ',STR_PAD_LEFT);

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;

			//$l_campo01 = '7KJB19' . 'RETENCAO PARA A PREVIDENCIA SOCIAL (INSS): R$' . $naux . str_repeat(' ',128 - strlen('RETENCAO PARA A PREVIDENCIA SOCIAL (INSS): R$' . $naux)).
			//'20                                                                       ' . str_repeat(' ',128 - strlen('INSS(Inst. Normativa MPS/SRP No.3 de 14/07/2005 - art. 148 - inciso I).')) . '21                                '.
			//str_repeat(' ',96) . $registro;
			$mReterInss = str_pad($mReterInss,10,' ',STR_PAD_LEFT);
			$l_campo01 = '7KJB19'. substr('N�o receber ap�s o vencimento'.str_repeat(' ',128),0,128) .
			'20' . substr(str_repeat(' ',128),0,128) . 
			'21'.substr('                                                       VALOR TOTAL BRUTO DA FATURA : R$ ' . $l_valor_ti . str_repeat(' ',128 - strlen('                                                       VALOR TOTAL BRUTO DA FATURA : R$ ' . $l_valor_ti)),0,128).	$registro;
			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;

			//$l_campo01 = '7KJB22A SLU E IMUNE DO IMPOSTO(ISS/QN) DE ACORDO COM O ART. 150 DA CONSTITUICAO FEDERAL.                  ' . str_repeat(' ',28) . '23' .
			//'A T E N C A O: Sr CAIXA, DEDUZIR DO VALOR TOTAL DO TITULO, A RETENCAO DO INSS.                      ' . str_repeat(' ',28) . 
			//'24OBS: E OBRIGATORIO APRESENTAR JUNTO A SLU A COMPROVACAO DO PAGAMENTO DA RETENCAO ACIMA.             ' . str_repeat(' ',28) . $registro;
			////     fwrite($mArqMov,$l_campo01."\r\n");
			//'                                                               VALOR A PAGAR A SLU : R$      50,00                              '
			$lValLiq = str_pad($lValLiq,10,' ',STR_PAD_LEFT);

			$l_campo01 = '7KJB22'. substr(str_repeat(' ',128),0,128) . 
			'23'.substr(str_repeat(' ',128),0,128) .
			'24' . substr('                                 * * RETENCAO PARA A PREVIDENCIA SOCIAL ' .CCGetSession("mINSS").'% (INSS) : R$ '.$mReterInss.
			str_repeat(' ',128-strlen('                                 * * RETENCAO PARA A PREVIDENCIA SOCIAL ' .CCGetSession("mINSS").'% (INSS) : R$ '.$mReterInss)),0,128) . $registro;
			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;

			$mFax = str_pad($CadEmp->f("fax").".",21);

			$l_campo01 = '7KJB25'.substr(str_repeat(' ',128),0,128). 
			'26' . substr('                                                               VALOR A PAGAR A SLU : R$ ' . $lValLiq .
			str_repeat(' ',128-strlen('                                                               VALOR A PAGAR A SLU : R$ ' . $lValLiq)),0,128) . 
			'27' . substr(str_repeat(' ',128),0,128) . $registro;
			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;

			//                  1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890


			$l_campo01 = '7KJB28' . substr(str_repeat(' ',128),0,128) . 
			             '29'.substr(str_repeat(' ',128),0,128) .
						 '30'.substr(str_repeat(' ',128),0,128) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;



			$l_campo01 = '7KJB31' . substr(str_repeat(' ',128),0,128) . 
			             '32'.substr('* * ATENCAO: O PAGAMENTO DA RETENCAO DO INSS DE R$ '.$mReterInss.',  E DE RESPONSABILIDADE DO CLIENTE,                              ' . 
						 str_repeat(' ',128-strlen('* * ATENCAO: O PAGAMENTO DA RETENCAO DO INSS DE R$ '.$mReterInss.',  E DE RESPONSABILIDADE DO CLIENTE,                              ')),0,128) . 
						 '33'.substr('             CONFORME INSTRUCAO NORMATIVA RFB  N. 1238,  DE 11 DE JANEIRO DE 2012 - "ART. 398".                                 ' . 
						 str_repeat(' ',128-strlen('             CONFORME INSTRUCAO NORMATIVA RFB  N. 1238,  DE 11 DE JANEIRO DE 2012 - "ART. 398".                                 ')),0,128) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;

			//                  1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
			$l_campo01 = '7KJB34' .substr( str_repeat(' ',128),0,128) . 
			             '35'.substr('             O COMPROVANTE DO PAGAMENTO DA RENTENCAO DEVERA SER ENVIADO VIA FAX OU E-MAIL PARA                                  ' . 
						 str_repeat(' ',128-strlen('             O COMPROVANTE DO PAGAMENTO DA RENTENCAO DEVERA SER ENVIADO VIA FAX OU E-MAIL PARA                                  ')),0,128) . 
						 '36'.substr('             A SLU: FAX '.$mFax.'  -  E-MAIL:  '.$mEmail.
						 str_repeat(' ',128-strlen('             A SLU: FAX '.$mFax.'  -  E-MAIL:  '.$mEmail)),0,128) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;

			//                  1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
			$l_campo01 = '7KJB37' . substr(str_repeat(' ',128),0,128) .
			'38'. substr(str_repeat(' ',128),0,128). 
			'39'.substr('A SLU E IMUNE DO IMPOSTO(ISS/QN) DE ACORDO COM O ART. 150 DA CONSTITUICAO FEDERAL.                                              ' . 
			str_repeat(' ',128-strlen('A SLU E IMUNE DO IMPOSTO(ISS/QN) DE ACORDO COM O ART. 150 DA CONSTITUICAO FEDERAL.                                              ')),0,128) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;

			//                  1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
			$l_campo01 = '7KJB40' . substr(str_repeat(' ',128),0,128) .
			'41'. substr(str_repeat(' ',128),0,128). 
			'42'.substr('Informacoes, duvidas ou contestacoes, deverao ser protocoladas no guiche da SLU no BH RESOLVE' . 
			str_repeat(' ',128-strlen('Informacoes, duvidas ou contestacoes, deverao ser protocoladas no guiche da SLU no BH RESOLVE')),0,128) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
			//                          10        20        30        40        50        60        70        80        90       100       110       120       130       140       150       160                                                 
			//                  12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
			$l_campo01 = '7KJB43'.substr('Av. Santos Dumont,  363 - Centro  ou Rua dos Caetes,   342 - Centro.   (guiche limpeza urbana)                                  ',0,128).
			'44'. substr(str_repeat(' ',128),0,128). 
			'45' . substr(str_repeat(' ',128),0,128) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";


         
		}
		$reg++;
		$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;

		$l_campo01 = '9'  . str_repeat(' ',393) . $registro;
		
		////     fwrite($mArqMov,$l_campo01."\r\n");
		print $l_campo01."\r\n";

		////     fclose($mArqMov);


//	    $Tab_CADFAT_Update = new clsDBfaturar();
//	   	$Tab_CADFAT_Update->query("UPDATE CADFAT SET EXPORT = 'T' WHERE EXPORT = 'F'");
//		$Tab_CADFAT_Update->close;
//		unset($Tab_CADFAT_Update);
	   /*
   		$arquivo = $_GET["arquivo"];
   		//$arquivo = $mArquivo;
   		if(isset($arquivo) && file_exists($arquivo)){ // faz o teste se a variavel n�o esta vazia e se o arquivo realmente existe
      		switch(strtolower(substr(strrchr(basename($arquivo),"."),1))){ // verifica a extens�o do arquivo para pegar o tipo
         		case "pdf": $tipo="application/pdf"; break;
         		case "txt": $tipo="application/txt"; break;
         		case "exe": $tipo="application/octet-stream"; break;
         		case "zip": $tipo="application/zip"; break;
         		case "doc": $tipo="application/msword"; break;
         		case "xls": $tipo="application/vnd.ms-excel"; break;
         		case "ppt": $tipo="application/vnd.ms-powerpoint"; break;
         		case "gif": $tipo="image/gif"; break;
         		case "png": $tipo="image/png"; break;
         		case "jpg": $tipo="image/jpg"; break;
         		case "mp3": $tipo="audio/mpeg"; break;
         		case "php": // deixar vazio por seuran�a
         		case "htm": // deixar vazio por seuran�a
         		case "html": // deixar vazio por seuran�a
      		}
      		header("Content-Type: ".$tipo); // informa o tipo do arquivo ao navegador
      		header("Content-Length: ".filesize($arquivo)); // informa o tamanho do arquivo ao navegador
      		header("Content-Disposition: attachment; filename=".basename($arquivo)); // informa ao navegador que � tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
      		readfile($arquivo); // l� o arquivo
      		//exit; // aborta p�s-a��es
   		}*/
		$TabItau->close;
		$TabUni->close;
		$CadCli->close;
		$MFSSTP->close;
		$CadEmp->close;

		unset($TabItau);
		unset($TabUni);
		unset($CadCli);
		unset($MFSSTP);
		unset($CadEmp);
		set_time_limit(30);
     
	}
	$Tab_CADFAT->close;
	unset($Tab_CADFAT);
	exit;
//Close GridExportar_Button1_OnClick @23-8B16BD30
    return $GridExportar_Button1_OnClick;
}
//End Close GridExportar_Button1_OnClick

//GridExportar_BeforeShow @2-3CFCF87E
function GridExportar_BeforeShow(& $sender)
{
    $GridExportar_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GridExportar; //Compatibility
//End GridExportar_BeforeShow

//Custom Code @14-2A29BDB7
// -------------------------
    $GridExportar->BTNExportar->Visible = false;
// -------------------------
//End Custom Code

//DLookup @15-7D8FE7D8
    global $DBfaturar;
    $Page = CCGetParentPage($sender);
    $mTotFatNaoExpo = CCDLookUp("count(codfatatrs)", "fatatrs", "", $Page->Connections["Faturar"]);
    $mTotFatNaoExpo = intval($mTotFatNaoExpo);

       //$GridExportar->BTNExportar->Visible = True;
	$GridExportar->Button1->Visible     = True;
    $GridExportar->LBLToaFat->SetValue($mTotFatNaoExpo);
	$GridExportar->LBLMensagem->SetValue("Clique no bot�o Exportar para iniciar.");

//End DLookup

//Close GridExportar_BeforeShow @2-9A5B05BE
    return $GridExportar_BeforeShow;
}
//End Close GridExportar_BeforeShow

//Page_BeforeShow @1-44CFE50C
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GeracaoBBFatatras; //Compatibility
//End Page_BeforeShow

//Custom Code @28-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(57);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow

//DEL  // -------------------------
//DEL  // -------------------------

//DEL  // -------------------------
//DEL  // -------------------------



?>