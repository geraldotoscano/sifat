<?php
include("filtrostring.php");
//BindEvents Method @1-8A7810B4
function BindEvents()
{
    global $GridExportar;
    global $CCSEvents;
    $GridExportar->btnBBCobrancaCNAB240->CCSEvents["OnClick"] = "GridExportar_btnBBCobrancaCNAB240_OnClick";
    $GridExportar->BTNExportar->CCSEvents["OnClick"] = "GridExportar_BTNExportar_OnClick";
    $GridExportar->Button1->CCSEvents["OnClick"] = "GridExportar_Button1_OnClick";
    $GridExportar->CCSEvents["BeforeShow"] = "GridExportar_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//GridExportar_btnBBCobrancaCNAB240_OnClick @30-178A0C54
function GridExportar_btnBBCobrancaCNAB240_OnClick(& $sender)
{
    $GridExportar_btnBBCobrancaCNAB240_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GridExportar; //Compatibility
//End GridExportar_btnBBCobrancaCNAB240_OnClick

//Custom Code @31-2A29BDB7
// -------------------------
    // Write your own code here.

	//Defini��o do objeto de header de arquivo
	$linha_header_arquivo=new TFiltrostring();
	$linha_header_arquivo->add('Banco',3,'0',true);
	$linha_header_arquivo->add('Lote',4,'0000');					//valor fixo
	$linha_header_arquivo->add('Registro',1,'0');					//valor fixo
	$linha_header_arquivo->add('CNAB',9,' ');						//valor fixo
	$linha_header_arquivo->add('TipoInscricaoEmpresa',1,'0',true);
	$linha_header_arquivo->add('NumInscricaoEmpresa',14,'0',true);
	$linha_header_arquivo->add('CodigoConvenioBanco',20,' '); 		//valor fixo
	$linha_header_arquivo->add('AgenciaMantenedoraConta',5,'0',true);
	$linha_header_arquivo->add('DigitoVerificadorAgencia',1,' ');
	$linha_header_arquivo->add('NumeroContaCorrente',12,'0',true);
	$linha_header_arquivo->add('DigitoVerificadorConta',1,' ');
	$linha_header_arquivo->add('DigitoVerificadorAgConta',1,' ');
	$linha_header_arquivo->add('NomeEmpresa',30,' ');
	$linha_header_arquivo->add('NomeBanco',30,' ');
	$linha_header_arquivo->add('CNAB2',10,' ');						//valor fixo	//Uso Exclusivo FEBRABAN / CNAB
	$linha_header_arquivo->add('CodigoRemessa',1,'1');				//valor fixo	//C�digo Remessa / Retorno: '1' = Remessa (Cliente ? Banco)  '2' = Retorno (Banco ? Cliente)
	$linha_header_arquivo->add('DataGeracaoArquivo',8,'0',true);
	$linha_header_arquivo->add('HoraGeracaoArquivo',6,'0',true);
	$linha_header_arquivo->add('NumeroSequencialArquivo',6,'0',true);
	$linha_header_arquivo->add('NumVersaoLayoutArquivo',3,'087');		//valor fixo	//Vers�o do layout FEBRABAN utilizado
	$linha_header_arquivo->add('DensidadeGravacaoArquivo',5,'01600');	//valor fixo	//Densidade de grava��o (BPI), do arquivo encaminhado. Dom�nio: 1600 BPI ou 6250 BPI
	$linha_header_arquivo->add('ParaUsoReservadoBanco',20,' ');			//valor fixo	//Para Uso Reservado do Banco
	$linha_header_arquivo->add('ParaUsoReservadoEmpresa',20,' ');		//valor fixo	//Para Uso Reservado da Empresa
	$linha_header_arquivo->add('UsoExclusivoFEBRABAN_CNAB',29,' ');		//valor fixo	//Uso Exclusivo FEBRABAN / CNAB


	//Defini��o do objeto de header de lote
    $linha_header_lote=new TFiltrostring();
	$linha_header_lote->add('Banco',3,'0',true);
	$linha_header_lote->add('Lote',4,'0001');			//valor fixo
	$linha_header_lote->add('Registro',1,'1');			//valor fixo
	$linha_header_lote->add('Operacao',1,'R');			//valor fixo
	$linha_header_lote->add('Servico',2,'01');			//valor fixo
	$linha_header_lote->add('CNAB',2,' ');				//valor fixo
	$linha_header_lote->add('LayoutdoLote',3,'045');	//valor fixo
	$linha_header_lote->add('CNAB2',1,' ');				//valor fixo
	$linha_header_lote->add('TipoInscricaoEmpresa',1,' ');
	$linha_header_lote->add('NumInscricaoEmpresa',15,'0',true);
	$linha_header_lote->add('Convenio',20,'0',true);
	$linha_header_lote->add('Agencia',5,'0',true);
	$linha_header_lote->add('AgenciaDV',1,'0',true);
	$linha_header_lote->add('ContaNum',12,'0',true);
	$linha_header_lote->add('ContaDV',1,'0',true);
	$linha_header_lote->add('AgenciaContaDV',1,'0',true);
	$linha_header_lote->add('NomeEmpresa',30,' ',false);
	$linha_header_lote->add('Informa��o1',40,' ');
	$linha_header_lote->add('Informa��o2',40,' ');
	$linha_header_lote->add('NumRemessaRetorno',8,'0',true);
	$linha_header_lote->add('DataGravacao',8,'0');
	$linha_header_lote->add('DataCredito',8,'0');		//valor fixo
	$linha_header_lote->add('CNAB3',33,' ');			//valor fixo


	//Defini��o do objeto de registro de detalhe Segmento P
	$linha_detalhe_p=new TFiltrostring();
	$linha_detalhe_p->add('Banco',3,'0',true);
	$linha_detalhe_p->add('Lote',4,'0001');				//valor fixo
	$linha_detalhe_p->add('Registro',1,'3');			//valor fixo
	$linha_detalhe_p->add('NumRegistro',5,'0',true);
	$linha_detalhe_p->add('Segmento',1,'P');			//valor fixo
	$linha_detalhe_p->add('CNAB',1,' ');				//valor fixo
	$linha_detalhe_p->add('CodMov',2,'01');				//valor fixo
	$linha_detalhe_p->add('Agencia',5,'0',true);
	$linha_detalhe_p->add('AgenciaDV',1,' ');
	$linha_detalhe_p->add('ContaNum',12,'0',true);
	$linha_detalhe_p->add('ContaDV',1,' ');
	$linha_detalhe_p->add('AgenciaContaDV',1,' ');
	$linha_detalhe_p->add('NossoNumero',20,' ');			//valor fixo
	$linha_detalhe_p->add('Carteira',1,'1',true);			//valor fixo	//D�vida
	$linha_detalhe_p->add('Cadastramento',1,'1',true);		//valor fixo	//D�vida
	$linha_detalhe_p->add('Documento',1,'1');				//valor fixo	//D�vida
	$linha_detalhe_p->add('EmissaoBloqueto',1,'1',true);	//valor fixo	//D�vida
	$linha_detalhe_p->add('DistribBloqueto',1,'1');			//valor fixo	//D�vida
	$linha_detalhe_p->add('NumDocumento',15,'0',true);
	$linha_detalhe_p->add('Vencimento',8,'0',true);
	$linha_detalhe_p->add('Valortitulo_i',15,'0',true);
	//$linha_detalhe_p->add('Valortitulo_d',2,'0',true);
	$linha_detalhe_p->add('AgCobradora',5,'0',true);		//valor fixo
	$linha_detalhe_p->add('AgCobradoraDV',1,' ');			//valor fixo
	$linha_detalhe_p->add('EspecieTitulo',2,'04');			//valor fixo	//D�vida
	$linha_detalhe_p->add('Aceite',1,'N');					//valor fixo	//D�vida
	$linha_detalhe_p->add('DataEmissaoTitulo',8,'0',true);	//valor fixo
	$linha_detalhe_p->add('CodJurosMora',1,'1');				//valor fixo
	$linha_detalhe_p->add('DataJurosMora',8,'0',true);  		//valor fixo
	$linha_detalhe_p->add('Juros1dia_i',15,'0',true);			//13 inteiros
	//$linha_detalhe_p->add('Juros1dia_d',2,'0',true);			//2 decimais
	$linha_detalhe_p->add('CodDesc1',1,'0');					//valor fixo
	$linha_detalhe_p->add('DataDesc1',8,'0',true);				//valor fixo
	$linha_detalhe_p->add('Desconto1',15,'0',true);				//valor fixo
	$linha_detalhe_p->add('VlrIOF',15,'0',true);				//valor fixo
	$linha_detalhe_p->add('VlrAbatimento',15,'0',true);			//valor fixo
	$linha_detalhe_p->add('UsoEmpresaCedente',25,' ');			//valor fixo
	$linha_detalhe_p->add('CodigoProtesto',1,'3');				//valor fixo
	$linha_detalhe_p->add('PrazoProtesto',2,'0',true);			//valor fixo
	$linha_detalhe_p->add('CodigoDevolucao',1,'0');				//valor fixo
	$linha_detalhe_p->add('PrazoDevolucao',3,'0',true);			//valor fixo
	$linha_detalhe_p->add('CodigoMoeda',2,'09');				//valor fixo
	$linha_detalhe_p->add('NumeroContrato',10,'0',true);		//valor fixo
	$linha_detalhe_p->add('Usolivre',1,' ');					//valor fixo


	//Defini��o do objeto de registro de detalhe Segmento Q
	$linha_detalhe_q=new TFiltrostring();
	$linha_detalhe_q->add('Banco',3,'0',true);
	$linha_detalhe_q->add('Lote',4,'0001');				//valor fixo
	$linha_detalhe_q->add('Registro',1,'3');			//valor fixo
	$linha_detalhe_q->add('NumRegistro',5,'0',true);
	$linha_detalhe_q->add('Segmento',1,'Q');			//valor fixo
	$linha_detalhe_q->add('CNAB',1,' ');				//valor fixo
	$linha_detalhe_q->add('CodMov',2,'01');				//valor fixo
	$linha_detalhe_q->add('TipoInscricao',1,'0',true);
	$linha_detalhe_q->add('NumeroInscricao',15,'0',true);
	$linha_detalhe_q->add('nome',34,' '); //O campo nome foi divido. Suas 40 posi��es ficam com 34 para nome e 6 para n�m. inscri��o do cliente (tabela CADCLI, campo CODCLI).
	$linha_detalhe_q->add('codcli',6,'0',true); //O campo nome foi divido. Suas 40 posi��es ficam com 34 para nome e 6 para n�m. inscri��o do cliente (tabela CADCLI, campo CODCLI).
	$linha_detalhe_q->add('logradouro',40,' ');
	$linha_detalhe_q->add('bairro',15,' ');
	$linha_detalhe_q->add('cep',8,'0',true);
	$linha_detalhe_q->add('cidade',15,' ');
	$linha_detalhe_q->add('estado',2,' ');
	$linha_detalhe_q->add('TipoInscricaoAvalista',1,'0');			//valor fixo
	$linha_detalhe_q->add('NumeroInscricaoAvalista',15,'0',true);	//valor fixo
	$linha_detalhe_q->add('NomeAvalista',40,' ');					//valor fixo
	$linha_detalhe_q->add('BancoCorrespondente',3,'0',true);		//valor fixo
	$linha_detalhe_q->add('NossoNumBcoCorrepondente',20,' ');		//valor fixo
	$linha_detalhe_q->add('CNAB',8,'0',true);						//valor fixo				


	//Defini��o do objeto de registro de detalhe Segmento R
	$linha_detalhe_r=new TFiltrostring();
	$linha_detalhe_r->add('Banco',3,'0',true);
	$linha_detalhe_r->add('Lote',4,'0001');						//valor fixo
	$linha_detalhe_r->add('Registro',1,'3');					//valor fixo
	$linha_detalhe_r->add('NumRegistro',5,'0',true);
	$linha_detalhe_r->add('Segmento',1,'R');					//valor fixo
	$linha_detalhe_r->add('CNAB',1,' ');						//valor fixo
	$linha_detalhe_r->add('CodMov',2,'01');						//valor fixo
	$linha_detalhe_r->add('CodDesconto2',1,'0');				//valor fixo
	$linha_detalhe_r->add('DataDesconto2',8,'0', true);			//valor fixo
	$linha_detalhe_r->add('DescontoPercentual2',15,'0', true);	//valor fixo
	$linha_detalhe_r->add('CodDesconto3',1,'0');				//valor fixo
	$linha_detalhe_r->add('DataDesconto3',8,'0', true);			//valor fixo
	$linha_detalhe_r->add('DescontoPercentual3',15,'0', true);	//valor fixo
	$linha_detalhe_r->add('CodMulta',1,'0');					//valor fixo
	$linha_detalhe_r->add('DataMulta',8,'0',true);				//valor fixo
	$linha_detalhe_r->add('PercentualAplicado',15,'0',true);	//valor fixo
	$linha_detalhe_r->add('InformacaoSacado',10,' ');			//valor fixo
	$linha_detalhe_r->add('Mensagem3',40,' ');		
	$linha_detalhe_r->add('Mensagem4',40,' ');					//valor fixo
	$linha_detalhe_r->add('CNAB',20,' ');						//valor fixo
	$linha_detalhe_r->add('CodOcorSacado',8,'0');				//valor fixo
	$linha_detalhe_r->add('CodBancoContaDebito',3,'0');			//valor fixo
	$linha_detalhe_r->add('CodAgDebito',5,'0');					//valor fixo
	$linha_detalhe_r->add('VerificadorAg',1,' ');				//valor fixo
	$linha_detalhe_r->add('ContaCorrenteDebito',12,'0');		//valor fixo
	$linha_detalhe_r->add('DigitoVerificadorConta',1,' ');		//valor fixo
	$linha_detalhe_r->add('DigitoVerificadorAgConta',1,' ');	//valor fixo
	$linha_detalhe_r->add('AvisoDebitoAutomatico',1,'0');		//valor fixo
	$linha_detalhe_r->add('CNAB2',9,' ');						//valor fixo


	/*ESTE SEGUIMENTO N�O � UTILIZADO PELO BANCO DO BRASIL, COMO EXPLICADO NO LAYOUT FEBRABAN CNAB240 DO BB, SEGUIMENTO S
	//Defini��o do objeto de registro de detalhe Segmento S
	$linha_detalhe_s=new TFiltrostring();
	$linha_detalhe_s->add('Banco',3,'0',true);
	$linha_detalhe_s->add('Lote',4,'0001');						//valor fixo
	$linha_detalhe_s->add('Registro',1,'3');					//valor fixo
	$linha_detalhe_s->add('NumRegistro',5,'0',true);
	$linha_detalhe_s->add('Segmento',1,'S');					//valor fixo
	$linha_detalhe_s->add('CNAB',1,' ');						//valor fixo
	$linha_detalhe_s->add('CodMov',2,'01');						//valor fixo
	$linha_detalhe_s->add('IdentificacaoImpressao',1,'1');		//valor fixo
	$linha_detalhe_s->add('IdentificacaoImpressao',1,'1');		//valor fixo*/


	//Defini��o do objeto de registro trailer de lote
	$linha_trailer_lote=new TFiltrostring();
	$linha_trailer_lote->add('Banco',3,'0',true);
	$linha_trailer_lote->add('Lote',4,'0001');			//valor fixo
	$linha_trailer_lote->add('Registro',1,'5');			//valor fixo
	$linha_trailer_lote->add('CNAB',9,' ');				//valor fixo
	$linha_trailer_lote->add('QtdeRegistros',6,'0',true);
	$linha_trailer_lote->add('QtdeTitulosCobrancaSimples',6,'0',true);				//valor fixo
	$linha_trailer_lote->add('ValorTotalTitulosCobrancaSimples',17,'0',true);		//valor fixo
	$linha_trailer_lote->add('QtdeTitulosCobrancaVinculada',6,'0',true);			//valor fixo
	$linha_trailer_lote->add('ValorTotalTitulosCobrancaVinculada',17,'0',true);		//valor fixo
	$linha_trailer_lote->add('QtdeTitulosCobrancaCaucionada',6,'0',true);			//valor fixo
	$linha_trailer_lote->add('ValorTotalTitulosCobrancaCaucionada',17,'0',true);	//valor fixo
	$linha_trailer_lote->add('QtdeTitulosCobrancaDescontada',6,'0',true);			//valor fixo
	$linha_trailer_lote->add('ValorTotalTitulosCobrancaDescontada',17,'0',true);	//valor fixo
	$linha_trailer_lote->add('NumAviso',8,' ');										//valor fixo
	$linha_trailer_lote->add('CNAB2',117,' ');										//valor fixo


	//Defini��o do objeto de registro trailer de arquivo
	$linha_trailer_arquivo=new TFiltrostring();
	$linha_trailer_arquivo->add('Banco',3,'0',true);
	$linha_trailer_arquivo->add('Lote',4,'9999');			//valor fixo
	$linha_trailer_arquivo->add('Registro',1,'9');			//valor fixo
	$linha_trailer_arquivo->add('CNAB',9,' ');				//valor fixo
	$linha_trailer_arquivo->add('QtdeLotes',6,'000001');	//valor fixo
	$linha_trailer_arquivo->add('QtdeRegistros',6,'0',true);
	$linha_trailer_arquivo->add('QtdeContasConcLotes',6,'0',true);	//valor fixo
	$linha_trailer_arquivo->add('CNAB2',205,'0',true);				//valor fixo



	$Tab_CADFAT = new clsDBfaturar();
	$Tab_CADFAT->query("SELECT COUNT(*) AS l_total_ex FROM cadfat where export = 'F'");
	$Tab_CADFAT->next_record();
	if (((float)($Tab_CADFAT->f("l_total_ex"))) != 0)
	{
		 //$mArqMov = fopen("c:\sistfat\movimeto.txt","w");
		//$mArqMov = fopen("http://sifatslu-hm.pbh.gov.br/movimeto.txt","w");.:/php/includes
		set_time_limit(600);
		$mDt = CCGetSession("DataSist");
		$mDia = substr($mDt,0,2);
		$mMes = substr($mDt,3,2);
		$mAno = substr($mDt,-2);
		$mArquivo = "ENBBCNAB240_".$mDia.$mMes.$mAno.".txt";

	    $tipo="application/force-download";

	    header("Pragma: public"); // required
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: private",false); // required for certain browsers
	    header("Content-Type: $tipo");
	    header("Content-Disposition: attachment; filename=".basename($mArquivo)); // informa ao navegador que � tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
	    header("Content-Transfer-Encoding: binary");
	    //header("Content-Length: ".filesize($mArquivo));
	    ob_clean();
	    flush();
	    //readfile( $mArquivo );

		$l_taceite = 'N';

    	$mCodUni = CCGetSession("mCOD_UNI");
		$TabItau = new clsDBfaturar();
		$TabItau->query("SELECT numecgc,empresa,sigla,endereco,cep,praca,seq_arq,agencia, DV_AGENCIA, CONTACO, DAC, SEQ_ARQUIVO FROM tabitau");
		$TabItau->next_record();

		$i_proxSeqNumArquivo=$TabItau->f('SEQ_ARQUIVO') + 1;

		//$l_agencia = str_pad(substr ($TabItau->f("agencia"),0,5),5);
		$l_agencia = $TabItau->f("agencia");
		$l_dvagencia = substr ($TabItau->f("dv_agencia"),0,1);
		$l_dvAgencia_2aPosicao = substr ($TabItau->f("dv_agencia"),1,1);
		$l_contaco = $TabItau->f("CONTACO");
		$l_dac     = substr ($TabItau->f("DAC"),0,1);
		$l_empresa = str_pad(substr ($TabItau->f("empresa"),0,30),30);
		$e_numregi = str_pad(substr ($TabItau->f("numecgc"),0,14),14);
		$l_codbanc = "001"; // ou colocar na tabela TABITAU em codbanc
		$l_nomeBanco = str_pad('BANCO DO BRASIL S/A', 30); //ou buscar da tabela TAB_BANCO
		$l_convenio= str_pad('002897081001417019  ', 20, '0', STR_PAD_LEFT);
		$l_data		= date('dmY');
		$l_hora		= date('his');

		if (strlen(trim($TabItau->f("numecgc"))) == 14)
		{
			$e_codinsc = '2';
		}
		else 
		{
			$e_codinsc = '1';
		}

		$g_dtr     = CCGetSession("DataSist");

		//Header do arquivo
		$linha_header_arquivo->setvalor('Banco',$l_codbanc);
		//Lote		str_repeat('0', 4)	//valor fixo
		//Registro	'0'					//valor fixo
		//CNAB		str_repeat(' ', 9)	//valor fixo
		$linha_header_arquivo->setvalor('TipoInscricaoEmpresa',$e_codinsc);
		$linha_header_arquivo->setvalor('NumInscricaoEmpresa',$e_numregi);
		$linha_header_arquivo->setvalor('CodigoConvenioBanco', $l_convenio);
		$linha_header_arquivo->setvalor('AgenciaMantenedoraConta',$l_agencia);
		$linha_header_arquivo->setvalor('DigitoVerificadorAgencia',$l_dvagencia);
		$linha_header_arquivo->setvalor('NumeroContaCorrente',$l_contaco);
		$linha_header_arquivo->setvalor('DigitoVerificadorConta',$l_dac);
		$linha_header_arquivo->setvalor('DigitoVerificadorAgConta',$l_dvAgencia_2aPosicao);
		$linha_header_arquivo->setvalor('NomeEmpresa',$l_empresa);
		$linha_header_arquivo->setvalor('NomeBanco',$l_nomeBanco);
		//CNAB2				str_repeat(' ', 10).	//valor fixo	//Uso Exclusivo FEBRABAN / CNAB
		//CodigoRemessa		'1'				//valor fixo	//C�digo Remessa / Retorno: '1' = Remessa (Cliente ? Banco)  '2' = Retorno (Banco ? Cliente)
		$linha_header_arquivo->setvalor('DataGeracaoArquivo',$l_data);
		$linha_header_arquivo->setvalor('HoraGeracaoArquivo',$l_hora);
		$linha_header_arquivo->setvalor('NumeroSequencialArquivo',$i_proxSeqNumArquivo);
		//NumVersaoLayoutArquivo		'087'.					//valor fixo	//Vers�o do layout FEBRABAN utilizado
		//DensidadeGravacaoArquivo		'01600'					//valor fixo	//Densidade de grava��o (BPI), do arquivo encaminhado. Dom�nio: 1600 BPI ou 6250 BPI
		//ParaUsoReservadoBanco			str_repeat(' ', 20)		//valor fixo	//Para Uso Reservado do Banco
		//ParaUsoReservadoEmpresa		str_repeat(' ', 20)		//valor fixo	//Para Uso Reservado da Empresa
		//UsoExclusivoFEBRABAN_CNAB		str_repeat(' ', 29)		//valor fixo	//Uso Exclusivo FEBRABAN / CNAB

		$mInicio=$linha_header_arquivo->getstring();

		print $mInicio."\r\n";


		//Header do lote: atribuindo valores que n�o foram atribu�dos na cria��o dos campos
		$linha_header_lote->setvalor('Banco',$l_codbanc);
		//Lote				//valor fixo
		//Registro			//valor fixo
		//Operacao			//valor fixo
		//Servico			//valor fixo
		//CNAB				//valor fixo
		//LayoutdoLote		//valor fixo
		//CNAB2				//valor fixo
		$linha_header_lote->setvalor('TipoInscricaoEmpresa',$e_codinsc);
		$linha_header_lote->setvalor('NumInscricaoEmpresa',$e_numregi);
		$linha_header_lote->setvalor('Convenio',$l_convenio);
		$linha_header_lote->setvalor('Agencia',$l_agencia);
		$linha_header_lote->setvalor('AgenciaDV',$l_dvagencia);
		$linha_header_lote->setvalor('ContaNum',$l_contaco);
		$linha_header_lote->setvalor('ContaDV',$l_dac);
		$linha_header_lote->setvalor('AgenciaContaDV',$l_dvAgencia_2aPosicao);
		$linha_header_lote->setvalor('NomeEmpresa',$l_empresa);
		//Informa��o1		//valor fixo
		//Informa��o2		//valor fixo
		$linha_header_lote->setvalor('NumRemessaRetorno',$i_proxSeqNumArquivo);
		$linha_header_lote->setvalor('DataGravacao',$l_data);
		//DataCredito		//valor fixo
		//CNAB3				//valor fixo

		$string_linha_header_lote=$linha_header_lote->getstring();

		print $string_linha_header_lote."\r\n";


		$CadCli = new clsDBfaturar();
		$MFSSTP = new clsDBfaturar();
		$CadEmp = new clsDBfaturar();

		$Tab_CADFAT->query("SELECT valmul,codfat,codcli,to_char(datemi, 'DD/MM/YYYY') as datemi,ret_inss,mesref,to_char(datvnc, 'DD/MM/YYYY') as datvnc,ValFat,datvnc+30 as limite,LOGSER,BAISER,CIDSER,ESTSER,(ValFat-ret_inss) as ValLiq FROM cadfat where export = 'F' and gerblto='S' order by codfat");
		$reg = 1;
		$regDetalhe = 0;
		while ($Tab_CADFAT->next_record())
		{
		    //                 13   +              6   +                      6 =  25 <-TAMANHO
			$l_titempr = 'NOSSA FATURA:' .$Tab_CADFAT->f("codcli") . $Tab_CADFAT->f("codfat");
			$l_numdupl = $Tab_CADFAT->f("codfat") . '    ';
			$l_datvenc  = substr($Tab_CADFAT->f("datvnc"), 0, 2) . substr ($Tab_CADFAT->f("datvnc"), 3, 2) . substr ($Tab_CADFAT->f("datvnc"), 6, 4);
			//$l_datvenc  = substr(date_format($Tab_CADFAT->f('datvnc'), 'd/m/Y'), 0, 8);
			//$l_datvenc  = substr($Tab_CADFAT->f('datvnc'), 0, 8);

			$l_valtitu = $Tab_CADFAT->f("ValFat");
			
			$mTemVirgula = strpos($l_valtitu,",");
			if (is_bool($mTemVirgula) && !$mTemVirgula)
			{
				$l_valtitu = $l_valtitu . "00";
			}
			else
			{
				$l_valtitu = trim(substr($l_valtitu, 0, strpos($l_valtitu,","))) . str_pad(substr($l_valtitu, strpos($l_valtitu,",")+1, 2),2,"0", STR_PAD_RIGHT);
			}
			$l_valtitu = str_pad(trim($l_valtitu),13,"0", STR_PAD_LEFT);//str_repeat('0',13 - strlen(trim($l_valtitu)))  . $l_valtitu;//replicate ('0', 13 - strlen($l_valtitu)) . $l_valtitu;

			
			$mValFatSemSeparadores =  number_format((float)str_replace(',','.',$Tab_CADFAT->f('ValFat')),   2, '', ''); // Valor sem separadores de milhar e decimal
			$mRetINSSSemSeparadores = number_format((float)str_replace(',','.',$Tab_CADFAT->f('ret_inss')), 2, '', ''); // Valor sem separadores de milhar e decimal
			$mValLiqSemSeparadores =  number_format((float)str_replace(',','.',$Tab_CADFAT->f('ValLiq')),   2, '', ''); // Valor sem separadores de milhar e decimal

			$l_mensagem3 = 'Bruto R$' . number_format((float)str_replace(',','.',$Tab_CADFAT->f('ValFat')), 2, ',', '') . ' DescINSS 13% R$' . 
				number_format((float)str_replace(',','.',$Tab_CADFAT->f('ret_inss')), 2, ',', '');
			$l_mensagem3 = substr($l_mensagem3, 0, 40);


			$l_datemis =  substr($Tab_CADFAT->f("datemi"), 0, 2) . substr ($Tab_CADFAT->f("datemi"), 3, 2) . substr ($Tab_CADFAT->f("datemi"), 6, 4);
			//$l_datemis =  $Tab_CADFAT->f("datemi");
			
    		$l_taxamesSemSeparadores = number_format((float)str_replace(',','.',$Tab_CADFAT->f('valmul')), 2, '', ''); // Valor sem separadores de milhar e decimal
					
			$mCodCli = $Tab_CADFAT->f("codcli");


			//$CadCli = new clsDBfaturar();
			$CadCli->query("SELECT cgccpf,descli,logcob,baicob,cidcob,estcob,poscob FROM cadcli where codcli='$mCodCli'");
			$CadCli->next_record();

			$s_numregi = trim($CadCli->f("cgccpf"));
			if (strlen($s_numregi) == 14)
			{
				$s_codinsc = '2';
			}
			else 
			{
				$s_codinsc = '1';
			}
			$s_nomesac = str_pad(substr($CadCli->f("descli"), 0, 30),30);
			$s_logrsac = str_pad($CadCli->f("logcob"),35).'     ';
			$s_bairsac = str_pad(substr ($CadCli->f("baicob"), 0, 12),12," ");
			$s_cidasac = str_pad(substr ($CadCli->f("cidcob"), 0, 15),15);
			$s_estasac = $CadCli->f("estcob");
			$s_estasac = str_pad($s_estasac,2); // corrigido em 07/11/2011 - n�o publicado
			$s_cepsaca = substr($CadCli->f("poscob"),0,5) . substr($CadCli->f("poscob"),6,3);
            $s_cepsaca = str_pad($s_cepsaca,8);// corrigido em 07/11/2011 - n�o publicado
			//$s_suficep = substr(l_cepcob, 6, 3);
			//fwrite($mArqMov,$s_logrsac."\r\n");
			
			$mReterInss = $Tab_CADFAT->f("ret_inss");
			/*
			   O trecho abaixo pega os n primeiros numeros do valor do inss antes da 
			   v�rgula e concatena com os 2 �ltimos numeros depois da v�rgula.
			*/
			$mTemVirgula = strpos($mReterInss,",");
			//if (is_bool($mTemVirgula) && !$mTemVirgula)
			//{
			//	$mReterInss = $mReterInss.".00";
			//}
			//else
			{
				$mReterInss = substr($mReterInss, 0, strpos($mReterInss,",")) ."," .str_pad(substr($mReterInss, strpos($mReterInss,",")+1, 2),2,"0", STR_PAD_RIGHT);
			}

			//$cmensage = 'RETER:INSS R$' . $mReterInss;
			$cmensage = 'VALOR DO INSS JA DEDUZIDO';
			$cmensage = $cmensage . str_repeat(' ',40 - strlen($cmensage));


			$l_codfat = $Tab_CADFAT->f("codfat");


			//Atribuindo valores ao registro de detalhe Seguimento P
			$regDetalhe++;
			$linha_detalhe_p->setvalor('Banco',$l_codbanc);
			//Lote				//valor fixo
			//Registro			//valor fixo
			$linha_detalhe_p->setvalor('NumRegistro',$regDetalhe);
			//Segmento			//valor fixo
			//CNAB				//valor fixo
			//CodMov			//valor fixo
			$linha_detalhe_p->setvalor('Agencia',$l_agencia);
			$linha_detalhe_p->setvalor('AgenciaDV',$l_dvagencia);
			$linha_detalhe_p->setvalor('ContaNum',$l_contaco);
			$linha_detalhe_p->setvalor('ContaDV',$l_dac);
			$linha_detalhe_p->setvalor('AgenciaContaDV',$l_dvAgencia_2aPosicao);
			//$linha_detalhe_p->add('NossoNumero',20,'0',true);		//valor fixo
			//$linha_detalhe_p->add('Carteira',1,'1',true);			//valor fixo	//D�vida
			//$linha_detalhe_p->add('Cadastramento',1,'1',true);	//valor fixo	//D�vida
			//$linha_detalhe_p->add('Documento',1,'1',true);		//valor fixo	//D�vida
			//$linha_detalhe_p->add('EmissaoBloqueto',1,'1',true);	//valor fixo	//D�vida
			//$linha_detalhe_p->add('DistribBloqueto',1,'1',true);	//valor fixo	//D�vida
			$linha_detalhe_p->setvalor('NumDocumento',$l_codfat);
			$linha_detalhe_p->setvalor('Vencimento',$l_datvenc);
			$linha_detalhe_p->setvalor('Valortitulo_i',$mValLiqSemSeparadores);
			//$linha_detalhe_p->add('AgCobradora',15,'0',true);				//valor fixo
			//$linha_detalhe_p->add('AgCobradoraDV',1,'0',true);			//valor fixo
			//$linha_detalhe_p->setvalor('EspecieTitulo',2,'99');		//valor fixo	//D�vida
			//$linha_detalhe_p->setvalor('Aceite',1,'N');				//valor fixo	//D�vida
			$linha_detalhe_p->setvalor('DataEmissaoTitulo',$l_datemis);
			//$linha_detalhe_p->setvalor('CodJurosMora', '1');			//valor fixo
			//$linha_detalhe_p->setvalor('DataJurosMora', str_repeat('0', 8));  //valor fixo
			$linha_detalhe_p->setvalor('Juros1dia_i',$l_taxamesSemSeparadores);
			//$linha_detalhe_p->add('CodDesc1',1,'0');							//valor fixo
			//$linha_detalhe_p->add('DataDesc1',8,'0',true);					//valor fixo
			//$linha_detalhe_p->add('Desconto1',15,'0',true);					//valor fixo
			//$linha_detalhe_p->add('VlrIOF',15,'0',true);				//valor fixo
			//$linha_detalhe_p->add('VlrAbatimento',15,'0',true);		//valor fixo
			//$linha_detalhe_p->add('UsoEmpresaCedente',25,' ');		//valor fixo
			//$linha_detalhe_p->add('CodigoProtesto',1,'0');			//valor fixo
			//$linha_detalhe_p->add('PrazoProtesto',2,'0',true);		//valor fixo
			//$linha_detalhe_p->add('CodigoDevolucao',1,'0');			//valor fixo
			//$linha_detalhe_p->add('PrazoDevolucao',3,'0',true);		//valor fixo
			//$linha_detalhe_p->add('CodigoMoeda',2,'09');				//valor fixo
			//$linha_detalhe_p->add('NumeroContrato',10,'0',true);		//valor fixo
			//$linha_detalhe_p->add('Usolivre',1,' ');					//valor fixo
			

			$string_linha_detalhe_p=$linha_detalhe_p->getstring();
           
			print $string_linha_detalhe_p."\r\n";

			
			$regDetalhe++;
			//Atribuindo valores ao registro de detalhe Seguimento Q
			$linha_detalhe_q->setvalor('Banco',$l_codbanc);
			//Lote				//valor fixo
			//Registro			//valor fixo
			$linha_detalhe_q->setvalor('NumRegistro',$regDetalhe);
			//Segmento			//valor fixo
			//CNAB				//valor fixo
			//CodMov			//valor fixo
			$linha_detalhe_q->setvalor('TipoInscricao',$s_codinsc);
			$linha_detalhe_q->setvalor('NumeroInscricao',$s_numregi);
			$linha_detalhe_q->setvalor('nome',$s_nomesac); //O campo nome foi divido. Suas 40 posi��es ficam com 34 para nome e 6 para n�m. inscri��o do cliente (tabela CADCLI, campo CODCLI).
			$linha_detalhe_q->setvalor('codcli',$mCodCli); //O campo nome foi divido. Suas 40 posi��es ficam com 34 para nome e 6 para n�m. inscri��o do cliente (tabela CADCLI, campo CODCLI).
			$linha_detalhe_q->setvalor('logradouro',$s_logrsac);
			$linha_detalhe_q->setvalor('bairro',$s_bairsac);
			$linha_detalhe_q->setvalor('cep',$s_cepsaca);
			$linha_detalhe_q->setvalor('cidade',$s_cidasac);
			$linha_detalhe_q->setvalor('estado',$s_estasac);
			//TipoInscricaoAvalista			//valor fixo
			//NumeroInscricaoAvalista		//valor fixo
			//NomeAvalista					//valor fixo
			//BancoCorrespondente			//valor fixo
			//NossoNumBcoCorrepondente		//valor fixo
			//CNAB							//valor fixo


			$string_linha_detalhe_q=$linha_detalhe_q->getstring();
           
			print $string_linha_detalhe_q."\r\n";

			

			$regDetalhe++;
			//Atribuindo valores ao registro de detalhe Seguimento R
			$linha_detalhe_r->setvalor('Banco',$l_codbanc);
			//$linha_detalhe_r->setvalor('Lote',4,'0001');						//valor fixo
			//$linha_detalhe_r->setvalor('Registro',1,'3');					//valor fixo
			$linha_detalhe_r->setvalor('NumRegistro',$regDetalhe);
			//$linha_detalhe_r->setvalor('Segmento',1,'R');					//valor fixo
			//$linha_detalhe_r->setvalor('CNAB',1,' ');						//valor fixo
			//$linha_detalhe_r->setvalor('CodMov',2,'01');						//valor fixo
			//$linha_detalhe_r->setvalor('CodDesconto2',1,'0');				//valor fixo
			//$linha_detalhe_r->setvalor('DataDesconto2',8,'0', true);			//valor fixo
			//$linha_detalhe_r->setvalor('DescontoPercentual2',15,'0', true);	//valor fixo
			//$linha_detalhe_r->setvalor('CodDesconto3',1,'0');				//valor fixo
			//$linha_detalhe_r->setvalor('DataDesconto3',8,'0', true);			//valor fixo
			//$linha_detalhe_r->setvalor('DescontoPercentual3',15,'0', true);	//valor fixo
			//$linha_detalhe_r->setvalor('CodMulta',1,'0');					//valor fixo
			//$linha_detalhe_r->setvalor('DataMulta',8,'0',true);				//valor fixo
			//$linha_detalhe_r->setvalor('PercentualAplicado',15,'0',true);	//valor fixo
			//$linha_detalhe_r->setvalor('InformacaoSacado',10,' ');			//valor fixo
			$linha_detalhe_r->setvalor('Mensagem3',$l_mensagem3);		
			//$linha_detalhe_r->setvalor('Mensagem4',40,' ');					//valor fixo
			//$linha_detalhe_r->setvalor('CNAB',20,' ');						//valor fixo
			//$linha_detalhe_r->setvalor('CodOcorSacado',8,'0');				//valor fixo
			//$linha_detalhe_r->setvalor('CodBancoContaDebito',3,'0');			//valor fixo
			//$linha_detalhe_r->setvalor('CodAgDebito',5,'0');					//valor fixo
			//$linha_detalhe_r->setvalor('VerificadorAg',1,' ');				//valor fixo
			//$linha_detalhe_r->setvalor('ContaCorrenteDebito',12,'0');		//valor fixo
			//$linha_detalhe_r->setvalor('DigitoVerificadorConta',1,' ');		//valor fixo
			//$linha_detalhe_r->setvalor('DigitoVerificadorAgConta',1,' ');	//valor fixo
			//$linha_detalhe_r->setvalor('AvisoDebitoAutomatico',1,'0');		//valor fixo
			//$linha_detalhe_r->setvalor('CNAB2',9,' ');						//valor fixo

			$string_linha_detalhe_r=$linha_detalhe_r->getstring();
           
			print $string_linha_detalhe_r."\r\n";




			$l_valor_ti = substr($l_valtitu,0,strlen($l_valtitu) - 2) . '.' . substr($l_valtitu, -2);
			//fwrite($mArqMov,$l_valor_ti."\r\n");
			$l_valor_ti = number_format((float)$l_valor_ti, 2,',','.');
			//fwrite($mArqMov,$l_valor_ti."\r\n");
			$l_valor_ti = str_pad($l_valor_ti,10,' ',STR_PAD_LEFT);//transform (val ($l_valor_ti), '@E 999,999.99')
			//fwrite($mArqMov,$l_valor_ti."\r\n");
			$mItau05 = '    ' . substr($l_datemis,0,2) . '/' . substr($l_datemis,2,2) . '/' . substr($l_datemis, -2) . str_repeat(' ',7) . $l_numdupl . str_repeat(' ',9) . 'DV' . str_repeat(' ',11) . $l_taceite . str_repeat(' ',31) . 
						$l_valor_ti;



			$l_logser = $Tab_CADFAT->f("logser");
			$l_logser = str_pad($l_logser,35);
			$l_baiser = $Tab_CADFAT->f("baiser");
			$l_baiser = str_pad($l_baiser,20);
			$l_cidser = $Tab_CADFAT->f("cidser");
			$l_cidser = str_pad($l_cidser,20);
			$l_estser = $Tab_CADFAT->f("estser");
			$l_mesref = $Tab_CADFAT->f("mesref");

			$linha07 = 'Inscricao do Cliente: ' . $mCodCli . '           Numero da Fatura: ' . $l_numdupl . '     Mes de Referencia: ' . $l_mesref.'  ';
			$linha08 = str_repeat(' ',100);
			$linha09 = 'Local da Coleta:' . $l_logser . '  ' . $l_baiser . '  ' . $l_cidser . '/' . $l_estser . '  ';

			
			
			$l_campo01  = '7KJB07' . $linha07;
			$l_campo01 .=  str_repeat(' ',28) . '08' . $linha08;
			$l_campo01 .=  str_repeat(' ',28) . '09' . $linha09.  str_repeat(' ',28) . $registro;

			$l_codfat = $Tab_CADFAT->f("codfat");
			//$MFSSTP = new clsDBfaturar();
			$MFSSTP->query("SELECT m.equpbh,m.qtdmed,m.subser,m.valser,substr(s.subred,1,58) AS subred,s.unidad,t.valpbh FROM movfat m,subser s,tabpbh t WHERE codfat='$l_codfat' AND m.subser=s.subser and m.mesref=t.mesref ORDER BY m.codfat,m.grpser,m.subser");

            for($T=10;$T<19;$T++)
			{
				$lin[$T] = str_repeat(' ',100);
			}

			$cnt = 12;
			while ($MFSSTP->next_record())
			{
				$l_equpbh = $MFSSTP->f("equpbh");
				//fwrite($mArqMov,"Equival�ncia ->$l_equpbh\r\n");
				$l_equpbh = (float)((str_replace(",", ".", $l_equpbh)));
				$l_equpbh = round($l_equpbh,2);
				//fwrite($mArqMov,"Equival�ncia ->$l_equpbh\r\n");

				$l_qtdmed = $MFSSTP->f("qtdmed");
				$l_qtdmed = (float)((str_replace(",", ".", $l_qtdmed)));
				$l_qtdmed = round($l_qtdmed,2);

				$l_subser = $MFSSTP->f("subser");

				$mval_ser = $MFSSTP->f("valser");
				$mval_ser = (float)((str_replace(",", ".", $mval_ser)));
				$mval_ser = round($mval_ser,2);

				$l_subred = str_pad($MFSSTP->f("subred"),58);

				$l_unidad = $MFSSTP->f("unidad");

				$l_valpbh = $MFSSTP->f("valpbh");
				$l_valpbh = (float)((str_replace(",", ".", $l_valpbh)));

				$l_valuni = round ($l_equpbh * $l_valpbh, 2);

				$l_valser = $mval_ser;
				$l_valfat += $l_valser;

				$lin[$cnt] = $l_subred . ' ' . str_pad(number_format($l_qtdmed, 2,'.',''),8,' ',STR_PAD_LEFT) . '  ' . str_pad($l_unidad,6) . '   ' . str_pad(number_format($l_valuni, 2,'.',''),7,' ',STR_PAD_LEFT) . '  ' . str_pad(number_format($l_valser, 2,'.',''),11,' ',STR_PAD_LEFT) . '  ';
			
				$cnt++;
			}
			//$lin[10] = str_repeat(' ',100);

			//$CadEmp = new clsDBfaturar();
			$CadEmp->query("SELECT fax,telefo,razsoc,email FROM cademp");
			$CadEmp->next_record();

			$mRazSoc = $CadEmp->f("razsoc");
			//$mRazSoc = substr($mRazSoc,0,32);
			$mRazSoc = str_pad($mRazSoc,40);

			$mTelefo = $CadEmp->f("telefo");
			$mTelefo = str_pad($mTelefo,20);

			$mEmail = $CadEmp->f("email");

	      //$lin[11] = 'DISCRIMINACAO DOS SERVICOS/PRODUTOS                         MEDICAO  UNIDAD VR.UNITAR  VALOR TOTAL  '; Alterado em 
			$lin[11] = 'DISCRIMINA��O DAS FATURAS';
			//$lin[18] = $mRazSoc . str_repeat(' ',27). ' - ' . 'Telefone: '.$mTelefo;//'SUPERINTENDENCIA DE LIMPEZA URBANA-SLU   -  SECAO COMERCIAL   -  Telefone: 3277.9360                ';
			$lin[18] = str_repeat(' ',100);
			//$lin[$cnt] = $mRazSoc . str_repeat(' ',27). ' - ' . 'Telefone: '.$mTelefo;//'SUPERINTENDENCIA DE LIMPEZA URBANA-SLU   -  SECAO COMERCIAL   -  Telefone: 3277.9360                ';

			$i = 10;
			//           Cliente pode ter at� 7 servi�os - comenteddo em 14/03/2012 - Miguel.


			$naux = $Tab_CADFAT->f("ret_inss");
			$naux = (float)(str_replace(",", ".", $naux));//substitui v�rgula por ponto
			$naux = number_format($naux,2,',','.');
			$naux = str_pad($naux,10,' ',STR_PAD_LEFT);


			$mReterInss = str_pad($mReterInss,10,' ',STR_PAD_LEFT);
	
			$lValLiq = str_pad($lValLiq,10,' ',STR_PAD_LEFT);

			$mFax = str_pad($CadEmp->f("fax").".",21);


		}

		//Trailer do lote: atribuindo valores que n�o foram atribu�dos na cria��o dos campos
		$linha_trailer_lote->setvalor('Banco',$l_codbanc);
		//Lote				//valor fixo
		//Registro			//valor fixo
		//CNAB				//valor fixo
		$linha_trailer_lote->setvalor('QtdeRegistros',$regDetalhe+2);//Quantidade de registros Detalhe + 2 = 1 Header de Lote + 1 Trailer de lote
		//QtdeTitulosCobrancaSimples			//valor fixo
		//ValorTotalTitulosCobrancaSimples		//valor fixo
		//QtdeTitulosCobrancaVinculada			//valor fixo
		//ValorTotalTitulosCobrancaVinculada	//valor fixo
		//QtdeTitulosCobrancaCaucionada			//valor fixo
		//ValorTotalTitulosCobrancaCaucionada	//valor fixo
		//QtdeTitulosCobrancaDescontada			//valor fixo
		//ValorTotalTitulosCobrancaDescontada	//valor fixo
		//NumAviso								//valor fixo
		//CNAB2									//valor fixo

		$string_linha_trailer_lote=$linha_trailer_lote->getstring();

		print $string_linha_trailer_lote."\r\n";


		//Trailer do arquivo: atribuindo valores que n�o foram atribu�dos na cria��o dos campos
		$linha_trailer_arquivo->setvalor('Banco',$l_codbanc);
		//Lote				//valor fixo
		//Registro			//valor fixo
		//CNAB				//valor fixo
		//Qtde. de Lotes	//valor fixo
		$linha_trailer_arquivo->setvalor('QtdeRegistros',$regDetalhe+4);//Quantidade de registros Detalhe + 4 = 1 Header de arquivo + 1 Header de Lote + 1 Trailer de lote + 1 Trailer de arquivo
		//QtdeContasConcLotes					//valor fixo
		//CNAB2									//valor fixo

		$string_linha_trailer_arquivo=$linha_trailer_arquivo->getstring();

		print $string_linha_trailer_arquivo."\r\n";


		

		$l_campo01 = '9'  . str_repeat(' ',393) . $registro;
		

	    $Tab_CADFAT_Update = new clsDBfaturar();
	   	$Tab_CADFAT_Update->query("UPDATE CADFAT SET EXPORT = 'T' WHERE EXPORT = 'F'");
		$Tab_CADFAT_Update->close;
		unset($Tab_CADFAT_Update);

		//A TABITAU foi criada para se ter somente 1 registro. Para n�o se correr risco, ser� atualizado somente o primeiro.
	   	$TabItau->query("UPDATE TABITAU SET SEQ_ARQUIVO = $i_proxSeqNumArquivo WHERE ROWNUM = 1");

		$TabItau->close;
		$TabUni->close;
		$CadCli->close;
		$MFSSTP->close;
		$CadEmp->close;

		unset($TabItau);
		unset($TabUni);
		unset($CadCli);
		unset($MFSSTP);
		unset($CadEmp);
		set_time_limit(30);
     
	}
	$Tab_CADFAT->close;
	unset($Tab_CADFAT);
	exit;
// -------------------------
//End Custom Code

//Close GridExportar_btnBBCobrancaCNAB240_OnClick @30-885FB35E
    return $GridExportar_btnBBCobrancaCNAB240_OnClick;
}
//End Close GridExportar_btnBBCobrancaCNAB240_OnClick

//GridExportar_BTNExportar_OnClick @10-00480608
function GridExportar_BTNExportar_OnClick(& $sender)
{
    $GridExportar_BTNExportar_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GridExportar; //Compatibility
//End GridExportar_BTNExportar_OnClick

//Custom Code @17-2A29BDB7
// -------------------------
    // Write your own code here.
       global $DBfaturar;
	   $mtem = True;
       $Page = CCGetParentPage($sender);
	   $mTotRegs = 0;
	   //                            45        14        4        1       5        60        8     20       10          5
   $Tabela = new clsDBfaturar();
   $Tabela->query("SELECT empresa,sigla,endereco,cep,praca,seq_arq FROM tabitau");
   //$articles = array();
   $Tabela->next_record();
   {
	   $mNomeCedente     = $Tabela->f("empresa");
	   $mSiglaCedente    = $Tabela->f("sigla");
	   $mEnderecoCedente = $Tabela->f("endereco");
       $mCEPCedente      = $Tabela->f("cep");
	   $mPracaCedente    = $Tabela->f("praca");
	   $mSeqRemCedente   = number_format($Tabela->f("seq_arq"), 0,'','');//Elimina o ponto decimal.
   }
	   $mEspacos = "                                              "; // 46 espacos
	   //str_pad ( string $input , int $pad_length [, string $pad_string [, int $pad_type ]] )
       $mNomeCedente     = str_pad ($mNomeCedente,45);
       $mSiglaCedente    = str_pad ($mSiglaCedente,10);
       $mEnderecoCedente = str_pad ($mEnderecoCedente,60);
       $mCEPCedente      = str_pad ($mCEPCedente,8);
       $mPracaCedente    = str_pad ($mPracaCedente,20);
       $mSeqRemCedente   = str_pad($mSeqRemCedente,07,"0", STR_PAD_LEFT);
	   $mRegTipo1Header = "01"                  . // (001-002/002)Tipo registro                                                              
	                      "1615"                . // (003-006/004)Prefixo ag�ncia
                          "2"                   . // (007-007/001)DV - Ag�ncia
						  "000021162"           . // (008-016/009)C�digo do cedente
						  "1"                   . // (017-017/001)DV - Cedente
						  "017"                 . // (018-020/003)Carteira
						  "019"                 . // (021-023/003)Varia��o
						  "000000"              . // (024-029/006)Convenio
	                      $mNomeCedente         . // (030-074/045)Nome do cedente
						  $mSiglaCedente        . // (075-084/010)Sigla do cedente
						  "01"                  . // (085-086/002)Tipo de Impress�o = 01 = Boleto
						  $mEnderecoCedente     . // (087-146/060)Endere�o p/ Devolu��o
						  $mCEPCedente          . // (147-154/008)CEP 
                          $mPracaCedente        . // (155-174/020)Pra�a
                          $mSeqRemCedente       . // (175-181/007)Sequencial de Remessa
						  "N"                   . // (182-182/001)N - Nao conferir o sequencial acima
						  "    "                . // (183-186/004)Brancos
						  "TST454  "            . // (187-194/008)TST454 =  ARQUIVO DE TESTE -  CBR454 = ARQUIVO PARA PROCESSAMENTO
                          $mEspacos             . // (195-240/046) Campos 20 ao 22
						  str_pad ("1479953",10). // (241-250/010)N�mero conv�nio e espa�os
						  "\r\n"                ;
	$mDt = CCGetSession("DataSist");
	$mDia = substr($mDt,0,2);
	$mMes = substr($mDt,3,2);
	$mAno = substr($mDt,-4);
	$mArquivo = "EN".$mDia.$mMes.$mAno.".txt";
	$mArqMov = fopen($mArquivo,"w",1);
   //$mArqMov = fopen("lpt1:","w");
   fwrite($mArqMov,$mRegTipo1Header);
   $mTotRegs++;


   //$Tab_CADFAT = new clsDBfaturar();
   //$Tab_CADFAT->query("CREATE INDEX CADFAT_IDX ON CADFAT(CODFAT)");
   //$Tab_CADFAT->execute;
   //$Tab_CADFAT->query("SELECT codfat,codcli,datemi,mesref,datvnc,ValFat,datvnc+30 as limite FROM cadfat where export = false order by codfat");

   
   $Tab_CADFAT = new clsDBfaturar();
   $Tab_CADFAT->query("SELECT codfat,codcli,datemi,mesref,datvnc,ValFat,datvnc+30 as limite,ret_inss,issqn,valmul FROM cadfat where export = 'F' and gerblto='S' order by codfat");
   //$articles = array();
   //                    1         2         3         4         5         6         7         8         9        10 
   //           1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
   //linha07 = 'Inscricao do Cliente: 999999 Numero da Fatura: 999999 Mes de Referencia: 55/4444'
   //linha07 = 'Inscricao do Cliente: ' + $mCodCli + ' Numero da Fatura: ' + $mCodFat + ' Mes de Referencia: ' + $mMesRef
   $mRegTipo02 = "";
   $mRegTipo03 = "";
   while ($Tab_CADFAT->next_record())
   {
       //echo "cadfat.dbf";
	   //$a = array();
	   $mCodFat  = $Tab_CADFAT->f("codfat");
	   $mCodCli  = $Tab_CADFAT->f("codcli");
	   $mDatEmi  = $Tab_CADFAT->f("datemi");
       $mMesRef  = $Tab_CADFAT->f("mesref");
	   $mDatVnc  = $Tab_CADFAT->f("datvnc");
	   $mValFat  = $Tab_CADFAT->f("ValFat");
	   $mRetINSS = $Tab_CADFAT->f("ret_inss");
	   $mlimite  = $Tab_CADFAT->f("limite");
	   $mISSQN   = $Tab_CADFAT->f("issqn");;
	   $mtaxames = $Tab_CADFAT->f("valmul");;
	   //$articles[] = $a;
       //fwrite($mArqMov,"Inscricao do Cliente: " . $mCodCli . " Numero da Fatura: " . $mCodFat . " Mes de Referencia: " . $mMesRef."\r\n");

      $Tab_CADCLI = new clsDBfaturar();
      $Tab_CADCLI->query("SELECT descli,cgccpf,logcob,baicob,cidcob,estcob,poscob FROM cadcli where codcli = '".$mCodCli."' order by codcli");
      //$articles = array();
      $Tab_CADCLI->next_record();
      {
	      //$a = array();
	      $mdescli  = $Tab_CADCLI->f("descli");
	      $mcgccpf  = $Tab_CADCLI->f("cgccpf");
	      $mlogcob  = $Tab_CADCLI->f("logcob");
          $mbaicob  = $Tab_CADCLI->f("baicob");
	      $mcidcob  = $Tab_CADCLI->f("cidcob");
	      $mestcob  = $Tab_CADCLI->f("estcob");
	      $mposcob  = $Tab_CADCLI->f("poscob");
		  $l_valfat = 0;
	      //$articles[] = $a;
      }

      $Tab_MOVFAT = new clsDBfaturar();
      $Tab_MOVFAT->query("SELECT equpbh,qtdmed,subser,ValSer,idsercli FROM movfat where codfat='".$mCodFat."' and valser > 0 order by codfat");
      //$articles = array();
	  /*
	     Montagem das linhas tipo 04 dos itens da fatura
	  */
	  $mLinhasReg04 = array();
	  $mIndice = 0;
      while ($Tab_MOVFAT->next_record())
      {
	      //$a = array();
	      $mequpbh = $Tab_MOVFAT->f("equpbh");
	      $mqtdmed = $Tab_MOVFAT->f("qtdmed");
	      $msubser = $Tab_MOVFAT->f("subser");
          $mValSer = $Tab_MOVFAT->f("ValSer");
		  $midsercli = $Tab_MOVFAT->f("idsercli");
	      //$articles[] = $a;

         $Tab_SUBSER = new clsDBfaturar();
         $Tab_SUBSER->query("SELECT subred,unidad FROM subser where subser = '".$msubser."' order by subser");
         //$articles = array();
         $Tab_SUBSER->next_record();
         { 
	         //$a = array();
	         $msubred  = $Tab_SUBSER->f("subred");
	         $munidad  = $Tab_SUBSER->f("unidad");
	         //$articles[] = $a;
         }
         $Tab_TABPBH = new clsDBfaturar();
         $Tab_TABPBH->query("SELECT valpbh from tabpbh where mesref = '".$mMesRef."' order by mesref");
         //$articles = array();
         $Tab_TABPBH->next_record();
         { 
	         //$a = array();
	         $mvalpbh  = $Tab_TABPBH->f("valpbh");
			 $mvaluni = round ($mequpbh * $mvalpbh, 2);
			 $mvaluni = number_format($mvaluni, 2,',','.');
			 $l_valfat += $mValSer;
			 $mValSer = number_format($mValSer, 2,',','.');
			 $mqtdmed = number_format($mqtdmed, 2,',','.');
	         //$articles[] = $a;
         }
		 $mLinhasReg04[$mIndice] = str_pad (substr($msubred,0,40),40). 
		                           "  "                              . 
								   str_pad (substr($mqtdmed."",-7),7). 
								   "  "                              . 
								   str_pad ($munidad,6)              . 
								   " "                               . 
								   str_pad (substr($mvaluni."",-9),9). 
								   "  "                              . 
								   str_pad (substr($mValSer."",-11),11);
		 $mIndice++;
      }
	  //if ($l_valfat > 0)
	  //{
         //fwrite($mArqMov,"04111Inscricao do Cliente: " . $mCodCli . " Numero da Fatura: " . $mCodFat . " Mes de Referencia: " . $mMesRef.);
          //if ($mtem)
		  //{
          //   echo "movfat.dbf";
		  //   $mtem = false;
		  //}
         $Tab_MOVSER = new clsDBfaturar();
         $Tab_MOVSER->query("SELECT logser,baiser,cidser,estser from MOVSER where codcli = '".$mCodCli."' and idsercli = '".$midsercli."' order by codcli,subser");
         //$articles = array();
         $Tab_MOVSER->next_record();
         { 
	         //$a = array();
	         $mlogser  = $Tab_MOVSER->f("logser");
			 $mlogser  = str_pad (substr($mlogser,0,32),32);

	         $mbaiser  = $Tab_MOVSER->f("baiser");
			 $mbaiser  = str_pad (substr($mbaiser,0,18),18);

	         $mcidser  = $Tab_MOVSER->f("cidser");
             $mcidser  = str_pad (substr($mcidser,0,18),18);

	         $mestser  = $Tab_MOVSER->f("estser");
	         //$articles[] = $a;
         }
		 //                     REGISTRO TIPO 02 - I N I C I O
		 if ($mRegTipo02 == "") // Grava esta linha uma �nica vez no arquivo remessa.
		 {
		    $mlimite = substr($mlimite,-2)."/".substr($mlimite,05,02)."/".substr($mlimite,00,04);
		    $mRegTipo02 = "021111"                                                         .
		                  str_pad ("NAO RECEBER APOS ".$mlimite,60)                        .
                          str_pad ("DEVOLVER EM ".$mlimite,60)                             .
                          str_pad ("COBRANCA ESCRITURAL ",60)                              .
                          str_pad ("ISSQN IMUNE PARA AS ENTIDADES DA ESFERA MUNICIPAL.",64).
		                  "\r\n"; 
            fwrite($mArqMov,$mRegTipo02);

		    $mRegTipo02 = "021111"                                 .
			              //        1234567890123456789012345678901234567890123456789012345678901234567890
		                  str_pad ("ATENCAO:Sr CAIXA, DEDUZIR DO VALOR TOTAL, AS RETENCOES ISSQN",60).
                          str_pad ("E INSS.",60)                         .
                          str_pad (" ",60)                         .
                          str_pad (" ",64)                         .
		                  "\r\n"; 
            fwrite($mArqMov,$mRegTipo02);
            $mTotRegs += 2;
		 }
		 //                     REGISTRO TIPO 02 - F    I    M

		 //                     REGISTRO TIPO 03 - I N I C I O
		 if ($mRegTipo03 == "") // Grava esta linha uma �nica vez no arquivo remessa.
		 {
		    $mRegTipo03 = "03111"                                     .
		                  str_pad ($mNomeCedente,80)                  .
                          str_pad ($mEnderecoCedente,80)              .
                          str_pad ($mCEPCedente." ".$mPracaCedente,85).
		                  "\r\n"; 
            fwrite($mArqMov,$mRegTipo03);
            $mTotRegs++;
		 }
		 //                     REGISTRO TIPO 03 - F    I    M


		 //                     REGISTRO TIPO 04 - I N I C I O
		 $mMensagem1 = "Inscricao do Cliente: "     .// Tamanho = 22
		               str_pad($mCodCli,06)         .// Tamanho = 06 total 028
					   " Numero da Fatura: "        .// Tamanho = 19 total 047
					   str_pad($mCodFat,06)         .// Tamanho = 06 total 053
					   " Mes de Referencia: "       .// Tamanho = 20 total 073
					   str_pad($mMesRef,07)         ;// Tamanho = 07 total 080

         $mMensagem2 = "Coleta:"                    .// Tamanho = 07
					   $mlogser                     .// Tamanho = 32 total 039
					   " "                          .// Tamanho = 01 total 040
					   $mbaiser                     .// Tamanho = 18 total 058
					   " "                          .// Tamanho = 01 total 059 
					   $mcidser                     .// Tamanho = 18 total 077 
					   "/"                          .// Tamanho = 01 total 078  
					   str_pad($mestser,2)          ;// Tamanho = 02 total 080 
         //                      1         2         3         4         5         6         7         8 
		 //             12345678901234567890123456789012345678901234567890123456789012345678901234567890
         $mMensagem3 = "DISCRIMINACAO DOS SERVICOS/PRODUTOS       MEDICAO  UNIDAD VR.UNITAR  VALOR TOTAL";

	     $mRegTipo04 ="04"                         .//(001-002/002)Tipo do Registro
		              "1"                          .//(003-003/001)Fonte da Mensagem 1
					  "1"                          .//(004-004/001)Fonte da Mensagem 2
					  "1"                          .//(005-005/001)Fonte da Mensagem 3
					  $mMensagem1                  .//(006-085/080)Mensagem 1
					  $mMensagem2                  .//(086-165/080)Mensagem 2
					  $mMensagem3                  .//(166-245/080)Mensagem 3
					  "     "                      .//(246-250/005)Espa�os
					  "\r\n"                       ;
         fwrite($mArqMov,$mRegTipo04);
         $mTotRegs++;
		 $mRegTipo04 = "04111";
		 for ($i = 0; $i < count($mLinhasReg04); $i++) 
		 {
		    $mRegTipo04 .= $mLinhasReg04[$i];
			if( ($i+1) / 3 == intval(($i+1) / 3) )
			{
			   $mRegTipo04 .= "     "."\r\n";
               fwrite($mArqMov,$mRegTipo04);
               $mTotRegs++;
		       $mRegTipo04 = "04111";
			}
		 }
		 /*
		     Completar aqui o que falta no registro tipo 04. Pode faltar 1, 2 ou 3 sequ�ncias
			 de 80 bytes(caracteres).
		 */
		 //$mISSQN   = round($l_valfat*2/100,2);
	     //$mtaxames = ($l_valfat * 0.01)/30;
		 $mtaxames = number_format($mtaxames, 2,',','.');
		 if (strlen($mRegTipo04) == 5) // Completar com 3 sequ�ncias
		 {
            $mRegTipo04 .= "SUPERINTENDENCIA DE LIMPEZA URBANA-SLU - SECAO COMERCIAL - TELEFONE 3277-9360   ";
		    $mRegTipo04 .= str_pad ("APOS O VENCIMENTO COBRAR MORA DE R$ ".$mtaxames." AO DIA",80); //
            $mRegTipo04 .= "RETENCAO PARA A PREVIDENCIA SOCIAL (INSS) : ".str_pad(number_format($mRetINSS, 2,',','.')."",36);
		    $mRegTipo04 .= "     "."\r\n";
            fwrite($mArqMov,$mRegTipo04);
            $mRegTipo04  = "04111INSS(Inst. Normativa MPS/SRP No.3 de 14/07/2005 - art. 148 - inciso I).";
            $mRegTipo04 .= "RETENCAO DE ISSQN: R$ ".str_pad(number_format($mISSQN, 2,',','.')."",58) ;
			$mRegTipo04 .= "ISSQN IMUNE PARA AS ENTIDADES DA ESFERA MUNICIPAL.                                   "."\r\n";
            fwrite($mArqMov,$mRegTipo04);
            $mRegTipo04  = "04111ATENCAO:Sr CAIXA, DEDUZIR DO VALOR TOTAL, AS RETENCOES ISSQN E INSS        ";
            $mRegTipo04 .= "OBS: E OBRIGATORIO APRESENTAR JUNTO A SLU A COMPROVACAO DO PAGAMENTO DAS RETEN- ";
			$mRegTipo04 .= "COES ACIMA.VIA FAX ATRAVES DO NUMERO 3277-9400.                                      "."\r\n";
			fwrite($mArqMov,$mRegTipo04);
            $mTotRegs += 3;
		 }
		 else
		 {
		    if (strlen($mRegTipo04) == 85) // Completar com 2 sequ�ncias
			{
               $mRegTipo04 .= "SUPERINTENDENCIA DE LIMPEZA URBANA-SLU - SECAO COMERCIAL - TELEFONE 3277-9360   ";
		       $mRegTipo04 .= str_pad ("APOS O VENCIMENTO COBRAR MORA DE R$ ".$mtaxames." AO DIA",80); //
		       $mRegTipo04 .= "     "."\r\n";
               fwrite($mArqMov,$mRegTipo04);
               $mRegTipo04  = "04111RETENCAO PARA A PREVIDENCIA SOCIAL (INSS) : ".str_pad(number_format($mRetINSS, 2,',','.')."",36);
               $mRegTipo04 .= "INSS(Inst. Normativa MPS/SRP No.3 de 14/07/2005 - art. 148 - inciso I).         ";
               $mRegTipo04 .= "RETENCAO DE ISSQN: R$ ".str_pad(number_format($mISSQN, 2,',','.')."",58)."     "."\r\n";
			   fwrite($mArqMov,$mRegTipo04);
			   $mRegTipo04  = "04111ISSQN IMUNE PARA AS ENTIDADES DA ESFERA MUNICIPAL.                              ";
               $mRegTipo04 .= "ATENCAO:Sr CAIXA, DEDUZIR DO VALOR TOTAL, AS RETENCOES ISSQN E INSS             ";
               $mRegTipo04 .= "OBS: E OBRIGATORIO APRESENTAR JUNTO A SLU A COMPROVACAO DO PAGAMENTO DAS RETEN-      "."\r\n";
			   fwrite($mArqMov,$mRegTipo04);
			   $mRegTipo04 = "04111COES ACIMA.VIA FAX ATRAVES DO NUMERO 3277-9400.                                 ";
			   $mRegTipo04 .= str_pad (" ",80); //
			   $mRegTipo04 .= str_pad (" ",85)."\r\n"; //
			   fwrite($mArqMov,$mRegTipo04);
               $mTotRegs += 4;
			}
			else // Completar com 1 sequ�ncia. Tamanho aqui = 85 + 80 = 165
			{
		       $mRegTipo04 .= str_pad ("SUPERINTENDENCIA DE LIMPEZA URBANA-SLU - SECAO COMERCIAL - TELEFONE 3277-9360",85)."\r\n"; //
               fwrite($mArqMov,$mRegTipo04);
		       $mRegTipo04 = str_pad ("04111APOS O VENCIMENTO COBRAR MORA DE R$ ".$mtaxames." AO DIA",85); //
               $mRegTipo04 .= "RETENCAO PARA A PREVIDENCIA SOCIAL (INSS) : ".str_pad(number_format($mRetINSS, 2,',','.')."",36);
               $mRegTipo04 .= "INSS(Inst. Normativa MPS/SRP No.3 de 14/07/2005 - art. 148 - inciso I).              "."\r\n";
			   fwrite($mArqMov,$mRegTipo04);
               $mRegTipo04  = "04111RETENCAO DE ISSQN: R$ ".str_pad(number_format($mISSQN, 2,',','.')."",58);
			   $mRegTipo04 .= "ISSQN IMUNE PARA AS ENTIDADES DA ESFERA MUNICIPAL.                              ";
               $mRegTipo04 .= "ATENCAO:Sr CAIXA, DEDUZIR DO VALOR TOTAL, AS RETENCOES ISSQN E INSS                  "."\r\n";
			   fwrite($mArqMov,$mRegTipo04);
               $mRegTipo04  = "04111OBS: E OBRIGATORIO APRESENTAR JUNTO A SLU A COMPROVACAO DO PAGAMENTO DAS RETEN- ";
			   $mRegTipo04 .= "COES ACIMA.VIA FAX ATRAVES DO NUMERO 3277-9400.                                 ";
			   $mRegTipo04 .= str_pad (" ",85)."\r\n"; //
			   fwrite($mArqMov,$mRegTipo04);
               $mTotRegs += 4;
			}
		 }
		 //                     REGISTRO TIPO 04 - F    I    M

		 $mTipoDoc = (strlen($mcgccpf) == 14)?("2"):("1");
	     $mDatEmi  = substr($mDatEmi,-2).substr($mDatEmi,05,02).substr($mDatEmi,02,02);
		 $mDatVnc  = substr($mDatVnc,-2).substr($mDatVnc,05,02).substr($mDatVnc,02,02);
		 $l_valfat = number_format($l_valfat, 2,'','');
		 //                     REGISTRO TIPO 05 - I N � C I O 
         $mRegTipo05 = "051111".
					   str_pad ("APOS O VENCIMENTO COBRAR MORA DE R$ ".$mtaxames." AO DIA",60)                                           .
                       str_pad ("RETER: INSS R$ ".number_format($mRetINSS, 2,',','.').", ISSQN R$ ".number_format($mISSQN, 2,',','.'),60). 
                       str_pad (" ",60)                                                                                                  .
                       str_pad (" ",64)                                                                                                  .
					   "\r\n";
         fwrite($mArqMov,$mRegTipo05);
         $mTotRegs++;
		 //                     REGISTRO TIPO 05 - F    I    M
		 //                 IN�CIO DA GERA��O DO REGISTRO TIPO 11
		 $mposcob = substr($mposcob,0,5).substr($mposcob,-3);
		 $mcidcob = substr($mcidcob,0,18);
         $mRegTipo11 = "11"                                            .//(001-002/002)Tipo de Registro
		               $mTipoDoc                                       .//(003-003/001)Tipo de documento do sacado 1 = CPF, 2 = CGC
                       str_pad ($mcgccpf,15,"0", STR_PAD_LEFT)         .//(004-018/015)CGC/CPF do sacado.
					   str_pad ($mdescli,60)                           .//(019-078/060)Nome do sacado.
                       str_pad ($mlogcob,60)                           .//(079-138/060)Endereco do sacado.
                       str_pad ($mposcob,08)                           .//(139-146/008)CEP do sacado
                       str_pad ($mcidcob,18)                           .//(147-164/018)Pra�a(cidade) do sacado.
                       str_pad ($mestcob,02)                           .//(165-166/002)UF pra�a do sacado.
                       str_pad ($mDatEmi,06)                           .//(167-172/006)Data da emiss�o DDMMAA 2007-01-17
					   str_pad ($mDatVnc,06)                           .//(173-178/006)Data do vencimento.
					   "N"                                             .//(179-179/001)Aceite.
					   "RC"                                            .//(180-181/002)Esp�cie do titulo. RC = Recibo. ?
                       "1479953".str_pad($mCodFat,10,"0", STR_PAD_LEFT).//(182-198/017)Nosso n�mero.                   ?
                       str_pad ($mCodFat,10)                           .//(199-208/010)N�m do t�t. dado pelo cedente.  ?
					   "09"                                            .//(209-210/002)Tipo de moeda. 09 = Real.
					   "000000000000000"                               .//(211-225/015)Quant. Moeda Variavel.
                       str_pad($l_valfat,15,"0", STR_PAD_LEFT)         .//(226-240/015)Valor do titulo.
					   "00"                                            .//(241-242/002)Prazo para protesto. 00 = n�o h�.
					   "      "                                        .//(243-248/006)Uso do banco.
					   "00"                                            .//(249-250/002)Total de parcelas.              ?
					   "\r\n"                                          ;//(2+1+15+60+60+8+18+2+6+6+1+2+11+1+15+2+10+13+2+6+2).
					   
		 //                 FINAL DA GERA��O DO REGISTRO TIPO 11
		fwrite($mArqMov,$mRegTipo11);
        $mTotRegs++;
		 //$mRegTipo04 = $mRegTipo04 . "12345678901234567890123456789012345678901234567890123456789012345678901234567890     "."\r\n";
         //fwrite($mArqMov,$mRegTipo04);
         //                      1         2         3         4         5         6         7         8         9        10 
         //             1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
         //  linha09 = 'Coleta:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA BBBBBBBBBBBBBBBBBB CCCCCCCCCCCCCCCCCC/EE  
		 //  linha11 = 'DISCRIMINACAO DOS SERVICOS/PRODUTOS       MEDICAO  UNIDAD VR.UNITAR  VALOR TOTAL'
         //             aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa  9999,99  999999 999999,99  99999999,99
         //	 linha&item. = substr(l_subred,0,40) . ' ' . substr($mqtdmed."",-7) . '  ' . $munidad . '   ' . substr($mvaluni."",-9) . '  ' . substr($mValSer."",-11)
         //	 linha&item. = l_subred + ' ' + $mqtdmed + '  ' + l_unidad + '   ' + str (l_valuni, 7, 2) + '  ' + str (l_valser, 11, 2) + space (2)

	  //}
   }
   //                     REGISTRO TIPO 99 - I N � C I O
   $mTotRegs = str_pad($mTotRegs,15,"0", STR_PAD_LEFT);
   $mRegTipo99 = "99".str_pad ($mTotRegs."",248);
   fwrite($mArqMov,$mRegTipo99);
   //                     REGISTRO TIPO 99 - F    I    M

   fclose($mArqMov);
   $Tab_CADFAT_Update = new clsDBfaturar();
   $Tab_CADFAT_Update->query("UPDATE CADFAT SET EXPORT = 'T' WHERE EXPORT = 'F'");
   $Tabela->close;
   $Tab_CADFAT->close;
   $Tab_CADCLI->close;
   $Tab_MOVFAT->close;
   $Tab_SUBSER->close;
   $Tab_TABPBH->close;
   $Tab_MOVSER->close;
   $Tab_CADFAT_Update->close;
   unset($Tabela);
   unset($Tab_CADFAT);
   unset($Tab_CADCLI);
   unset($Tab_MOVFAT);
   unset($Tab_SUBSER);
   unset($Tab_TABPBH);
   unset($Tab_MOVSER);
   unset($Tab_CADFAT_Update);
// -------------------------
//End Custom Code

//Close GridExportar_BTNExportar_OnClick @10-ACB27D29
    return $GridExportar_BTNExportar_OnClick;
}
//End Close GridExportar_BTNExportar_OnClick

//GridExportar_Button1_OnClick @23-820DF594
function GridExportar_Button1_OnClick(& $sender)
{
    $GridExportar_Button1_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GridExportar; //Compatibility
//End GridExportar_Button1_OnClick

//Custom Code @24-2A29BDB7
// -------------------------
    // Write your own code here.
// -------------------------
//End Custom Code



    $linha_detalhe=new TFiltrostring();
	$linha_detalhe->add('registro',1,'1');
    $linha_detalhe->add('codinscricao',2,'0',true);
	$linha_detalhe->add('numinscricao',14,'0',true);
	$linha_detalhe->add('agencia',4,'0',true);
	$linha_detalhe->add('zeros',2,'0',true);
	$linha_detalhe->add('conta',5,'0',true);
	$linha_detalhe->add('dac',1,'0',true);

	$linha_detalhe->add('brancos',4,' ',true);
	$linha_detalhe->add('instrucao',4,'0',true);
	$linha_detalhe->add('usoempresa',25,' ',false);
	$linha_detalhe->add('nossonumero',8,'0',true);
	$linha_detalhe->add('qtemodea_i',8,'0',true);
	$linha_detalhe->add('qtemodea_d',5,'0',false);
	$linha_detalhe->add('ncarteira',3,'0',true);
	$linha_detalhe->add('usobanco',21,' ',false);
	$linha_detalhe->add('carteira',1,' ',false);
	$linha_detalhe->add('codocorrencia',2,'0',false);


	$linha_detalhe->add('ndocumento',10,' ',false);
	$linha_detalhe->add('vencimento',6,'0',true);
	$linha_detalhe->add('valortitulo_i',11,'0',true);
	$linha_detalhe->add('valortitulo_d',2,'0',false);
	$linha_detalhe->add('codigobanco',3,'0',true);
	$linha_detalhe->add('agenciacobradora',5,'0',true);
	$linha_detalhe->add('especie',2,'0',true);
	$linha_detalhe->add('aceite',1,' ',true);
	$linha_detalhe->add('dataemissao',6,'0',true);
	$linha_detalhe->add('instrucao1',2,' ',false);
	$linha_detalhe->add('instrucao2',2,' ',false);
	$linha_detalhe->add('juros1dia_i',11,'0',true);
	$linha_detalhe->add('juros1dia_d',2,'0',false);
	$linha_detalhe->add('descontoate',6,'0',false);
	$linha_detalhe->add('valordesconto_i',11,'0',true);
	$linha_detalhe->add('valordesconto_d',2,'0',false);
	$linha_detalhe->add('valoriof_i',11,'0',true);
	$linha_detalhe->add('valoriof_d',2,'0',false);
	$linha_detalhe->add('abatimento_i',11,'0',true);
	$linha_detalhe->add('abatimento_d',2,'0',false);
	$linha_detalhe->add('codigoinscricao',2,'0',true);
	$linha_detalhe->add('numeroinscricao',14,'0',true);

	$linha_detalhe->add('nome',30,' ',false);
	$linha_detalhe->add('brancos',10,' ',false);
	$linha_detalhe->add('logradouro',40,' ',false);
	$linha_detalhe->add('bairro',12,' ',false);
	$linha_detalhe->add('cep',8,'0',true);
	$linha_detalhe->add('cidade',15,' ',false);
	$linha_detalhe->add('estado',2,' ',false);
	$linha_detalhe->add('sacador',30,' ',false);
	$linha_detalhe->add('brancos',4,' ',false);
	$linha_detalhe->add('datamora',6,'0',true);
	$linha_detalhe->add('prazo',2,'0',true);
	$linha_detalhe->add('brancos',1,' ',false);
	$linha_detalhe->add('sequencial',6,'0',true);


	$Tab_CADFAT = new clsDBfaturar();
	$Tab_CADFAT->query("SELECT COUNT(*) AS l_total_ex FROM cadfat where export = 'F'");
	$Tab_CADFAT->next_record();
	if (((float)($Tab_CADFAT->f("l_total_ex"))) != 0)
	{
		 //$mArqMov = fopen("c:\sistfat\movimeto.txt","w");
		//$mArqMov = fopen("http://sifatslu-hm.pbh.gov.br/movimeto.txt","w");.:/php/includes
		set_time_limit(600);
		$mDt = CCGetSession("DataSist");
		$mDia = substr($mDt,0,2);
		$mMes = substr($mDt,3,2);
		$mAno = substr($mDt,-2);
		$mArquivo = "EN".$mDia.$mMes.$mAno.".txt";

	    $tipo="application/force-download";

	    header("Pragma: public"); // required
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: private",false); // required for certain browsers
	    header("Content-Type: $tipo");
	    header("Content-Disposition: attachment; filename=".basename($mArquivo)); // informa ao navegador que � tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
	    header("Content-Transfer-Encoding: binary");
	    //header("Content-Length: ".filesize($mArquivo));
	    ob_clean();
	    flush();
	    //readfile( $mArquivo );


        //header("Content-Type: ".$tipo); // informa o tipo do arquivo ao navegador
        //header("Content-Disposition: attachment; filename=".basename($mArquivo)); // informa ao navegador que � tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
		//$mArqMov = fopen($mArquivo,"w",1);
		$l_taceite = 'N';

    	$mCodUni = CCGetSession("mCOD_UNI");
		/*
		$TabUni = new clsDBfaturar();
		$TabUni->query("SELECT descri FROM tabuni where coduni='$mCodUni'");
		$TabUni->next_record();
		$mUnidade = $TabUni->f("descri");
		$mUnidade = substr($mUnidade,0,32);
		$mUnidade = str_pad($mUnidade,32);
		*/
		$TabItau = new clsDBfaturar();
		$TabItau->query("SELECT numecgc,empresa,sigla,endereco,cep,praca,seq_arq,agencia,CONTACO,DAC FROM tabitau");
		$TabItau->next_record();
		$l_agencia = str_pad(substr ($TabItau->f("agencia"),0,4),4);
		$l_contaco = str_pad(substr ($TabItau->f("CONTACO"),0,5),5);
		$l_dac     = str_pad(substr ($TabItau->f("DAC"),0,1),1);
		$l_empresa = str_pad (substr ($TabItau->f("empresa"),0,30),30);
		$e_numregi = $TabItau->f("numecgc");
		$l_codbanc = "341"; // ou colocar em tabela em codbanc

		if (strlen(trim($TabItau->f("numecgc"))) == 14)
		{
			$e_codinsc = '02';
		}
		else 
		{
			$e_codinsc = '01';
		}

		$g_dtr     = CCGetSession("DataSist");

		$mInicio   = '01REMESSA01COBRANCA       ' . $l_agencia . '00' . $l_contaco . $l_dac . '        ' . $l_empresa . '341BANCO ITAU SA  ' . substr ($g_dtr, 0, 2) . substr ($g_dtr, 3, 2) . substr ($g_dtr, 8, 2) . str_repeat(' ',294).'000001';
		////     fwrite($mArqMov,$mInicio."\r\n");
		print $mInicio."\r\n";

		$CadCli = new clsDBfaturar();
		$MFSSTP = new clsDBfaturar();
		$CadEmp = new clsDBfaturar();

		$Tab_CADFAT->query("SELECT valmul,codfat,codcli,datemi,ret_inss,mesref,datvnc,ValFat,datvnc+30 as limite,LOGSER,BAISER,CIDSER,ESTSER,(ValFat-ret_inss) as ValLiq FROM cadfat where export = 'F' and gerblto='S' order by codfat");
		$reg = 1;
		while ($Tab_CADFAT->next_record())
		{
		    //                 13   +              6   +                      6 =  25 <-TAMANHO
			$l_titempr = 'NOSSA FATURA:' .$Tab_CADFAT->f("codcli") . $Tab_CADFAT->f("codfat");
			$l_numdupl = $Tab_CADFAT->f("codfat") . '    ';
			$l_datvenc  = substr($Tab_CADFAT->f("datvnc"), 0, 2) . substr ($Tab_CADFAT->f("datvnc"), 3, 2) . substr ($Tab_CADFAT->f("datvnc"), 8, 2);

			$l_valtitu = $Tab_CADFAT->f("ValFat");
			
			$mTemVirgula = strpos($l_valtitu,",");
			if (is_bool($mTemVirgula) && !$mTemVirgula)
			{
				$l_valtitu = $l_valtitu . "00";
			}
			else
			{
				$l_valtitu = trim(substr($l_valtitu, 0, strpos($l_valtitu,","))) . str_pad(substr($l_valtitu, strpos($l_valtitu,",")+1, 2),2,"0", STR_PAD_RIGHT);
			}
			$l_valtitu = str_pad(trim($l_valtitu),13,"0", STR_PAD_LEFT);//str_repeat('0',13 - strlen(trim($l_valtitu)))  . $l_valtitu;//replicate ('0', 13 - strlen($l_valtitu)) . $l_valtitu;

			$mValLiq = $Tab_CADFAT->f("ValLiq"); // Valor montado para a linha tipo 1(Sem ponto e sem v�rgula)
			$lValLiq = number_format((float)str_replace(',','.',$mValLiq), 2,',','.'); // Valor montado para a linha tipo 7(com ponto e v�rgula)


			$mTemVirgula = strpos($mValLiq,",");
			if (is_bool($mTemVirgula) && !$mTemVirgula)
			{
				$mValLiq_i=$mValLiq;
				$mValLiq_d="00";
				$mValLiq = $mValLiq . "00";

			}
			else
			{
				
				$mValLiq_i=trim(substr($mValLiq, 0, strpos($mValLiq,",")));
				$mValLiq_d=str_pad(substr($mValLiq, strpos($mValLiq,",")+1, 2),2,"0", STR_PAD_RIGHT);

				$mValLiq = trim(substr($mValLiq, 0, strpos($mValLiq,","))) . str_pad(substr($mValLiq, strpos($mValLiq,",")+1, 2),2,"0", STR_PAD_RIGHT);


			}

			$mValLiq = str_pad(trim($mValLiq),13,"0", STR_PAD_LEFT);//str_repeat('0',13 - strlen(trim($l_valtitu)))  . $l_valtitu;//replicate ('0', 13 - strlen($l_valtitu)) . $l_valtitu;



			$l_datemis =  substr($Tab_CADFAT->f("datemi"), 0, 2) . substr ($Tab_CADFAT->f("datemi"), 3, 2) . substr ($Tab_CADFAT->f("datemi"), 8, 2);
			
			$l_taxames = $Tab_CADFAT->f("valmul");
			/*
			   O trecho abaixo pega os n primeiros numeros do valor da multa antes da 
			   v�rgula e concatena com os 2 �ltimos numeros depois da v�rgula.
			*/
			$mTemVirgula = strpos($l_taxames,",");
			if (is_bool($mTemVirgula) && !$mTemVirgula)
			{
				$l_taxames_i=$l_taxames;
				$l_taxames_d="00";
				$l_taxames = $l_taxames ."00";

			}
			else
			{
				$l_taxames_i=substr($l_taxames, 0, strpos($l_taxames,","));
				$l_taxames_d=str_pad(substr($l_taxames, strpos($l_taxames,",")+1, 2),2,"0", STR_PAD_RIGHT);
				$l_taxames = substr($l_taxames, 0, strpos($l_taxames,",")) . str_pad(substr($l_taxames, strpos($l_taxames,",")+1, 2),2,"0", STR_PAD_RIGHT);


			}
			$l_taxames = str_pad($l_taxames,13,"0", STR_PAD_LEFT); //str_repeat('0',13 - strlen(trim($l_taxames)))  . $l_taxames;
					
			$mCodCli = $Tab_CADFAT->f("codcli");


			//$CadCli = new clsDBfaturar();
			$CadCli->query("SELECT cgccpf,descli,logcob,baicob,cidcob,estcob,poscob FROM cadcli where codcli='$mCodCli'");
			$CadCli->next_record();

			if (strlen(trim($CadCli->f("cgccpf"))) == 14)
			{
				$s_codinsc = '02';
			}
			else 
			{
				$s_codinsc = '01';
			}
			$s_numregi = str_pad($CadCli->f("cgccpf"),14);
			$s_nomesac = str_pad(substr($CadCli->f("descli"), 0, 30),30);
			$s_logrsac = str_pad($CadCli->f("logcob"),35).'     ';
			$s_bairsac = str_pad(substr ($CadCli->f("baicob"), 0, 12),12," ");
			$s_cidasac = str_pad(substr ($CadCli->f("cidcob"), 0, 15),15);
			$s_estasac = $CadCli->f("estcob");
			$s_estasac = str_pad($s_estasac,2); // corrigido em 07/11/2011 - n�o publicado
			$s_cepsaca = substr($CadCli->f("poscob"),0,5) . substr($CadCli->f("poscob"),6,3);
            $s_cepsaca = str_pad($s_cepsaca,8);// corrigido em 07/11/2011 - n�o publicado
			//$s_suficep = substr(l_cepcob, 6, 3);
			//fwrite($mArqMov,$s_logrsac."\r\n");
			
			$mReterInss = $Tab_CADFAT->f("ret_inss");
			/*
			   O trecho abaixo pega os n primeiros numeros do valor do inss antes da 
			   v�rgula e concatena com os 2 �ltimos numeros depois da v�rgula.
			*/
			$mTemVirgula = strpos($mReterInss,",");
			//if (is_bool($mTemVirgula) && !$mTemVirgula)
			//{
			//	$mReterInss = $mReterInss.".00";
			//}
			//else
			{
				$mReterInss = substr($mReterInss, 0, strpos($mReterInss,",")) ."," .str_pad(substr($mReterInss, strpos($mReterInss,",")+1, 2),2,"0", STR_PAD_RIGHT);
			}

			//$cmensage = 'RETER:INSS R$' . $mReterInss;
			$cmensage = 'VALOR DO INSS JA DEDUZIDO';
			$cmensage = $cmensage . str_repeat(' ',40 - strlen($cmensage));

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
			
/*
			$l_campo01 = '1' . $e_codinsc . $e_numregi . $l_agencia . '00' . $l_contaco . $l_dac . '    ' . '0000' . $l_titempr . '00000000'.'0000000000000' . '112' . str_repeat(' ',21) . '1'.'01' . $l_numdupl . 
						$l_datvenc . $mValLiq . $l_codbanc . '0000099N' . $l_datemis . '94  ' . $l_taxames . '000000' . str_repeat('0',39) . $s_codinsc . $s_numregi . $s_nomesac . 
						'          ' . $s_logrsac . $s_bairsac . $s_cepsaca . $s_cidasac . $s_estasac . $cmensage . '00 ' . $registro;
*/


			$linha_detalhe->setvalor('codinscricao',$e_codinsc);
			$linha_detalhe->setvalor('numinscricao',$e_numregi);
			$linha_detalhe->setvalor('agencia',$l_agencia);
			$linha_detalhe->setvalor('conta',$l_contaco);
			$linha_detalhe->setvalor('dac',$l_dac);
			$linha_detalhe->setvalor('usoempresa',$l_titempr);

			$linha_detalhe->setvalor('ncarteira','112');
			$linha_detalhe->setvalor('carteira','1');
			$linha_detalhe->setvalor('codocorrencia','01');
			$linha_detalhe->setvalor('ndocumento',$l_numdupl);
			$linha_detalhe->setvalor('vencimento',$l_datvenc);
			$linha_detalhe->setvalor('valortitulo_i',$mValLiq_i);
			$linha_detalhe->setvalor('valortitulo_d',$mValLiq_d);
			$linha_detalhe->setvalor('codigobanco',$l_codbanc);

			
			$linha_detalhe->setvalor('especie','99');
			$linha_detalhe->setvalor('aceite','N');

			$linha_detalhe->setvalor('dataemissao',$l_datemis);
			$linha_detalhe->setvalor('instrucao1','94');

			$linha_detalhe->setvalor('juros1dia_i',$l_taxames_i);
			$linha_detalhe->setvalor('juros1dia_d',$l_taxames_d);

			$linha_detalhe->setvalor('codigoinscricao',$s_codinsc);

			$linha_detalhe->setvalor('numeroinscricao',$s_numregi);
			$linha_detalhe->setvalor('nome',$s_nomesac);

			$linha_detalhe->setvalor('logradouro',$s_logrsac);
			$linha_detalhe->setvalor('bairro',$s_bairsac);
			$linha_detalhe->setvalor('cep',$s_cepsaca);
			$linha_detalhe->setvalor('cidade',$s_cidasac);
			$linha_detalhe->setvalor('estado',$s_estasac);

			

			$linha_detalhe->setvalor('sacador',$cmensage);

			$linha_detalhe->setvalor('sequencial',$registro);
			

			$l_campo01=$linha_detalhe->getstring();


			////     fwrite($mArqMov,$l_campo01."\r\n");
            
			print $l_campo01."\r\n";

			

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
			

			$l_campo01 = '7KJB01' . $l_empresa . ' CNPJ: ' . substr($e_numregi,0,2) . '.' . substr ($e_numregi,2,3) . '.' . substr ($e_numregi, 5, 3) . '/' . substr ($e_numregi, 8, 4) . '-' . substr($e_numregi, -2) . str_repeat(' ',30) . 
						substr($l_datvenc,0,2) . '/' . substr ($l_datvenc,2,2) . '/' . substr($l_datvenc,-2) . str_repeat(' ',35) . '02' . str_repeat(' ',128) . '03' . str_repeat(' ',19) . '112' . str_repeat(' ',06) . 'R$' . str_repeat(' ',46) . $l_agencia . '/' . $l_contaco . 
						'-' . $l_dac . str_repeat(' ',40) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$l_valor_ti = substr($l_valtitu,0,strlen($l_valtitu) - 2) . '.' . substr($l_valtitu, -2);
			//fwrite($mArqMov,$l_valor_ti."\r\n");
			$l_valor_ti = number_format((float)$l_valor_ti, 2,',','.');
			//fwrite($mArqMov,$l_valor_ti."\r\n");
			$l_valor_ti = str_pad($l_valor_ti,10,' ',STR_PAD_LEFT);//transform (val ($l_valor_ti), '@E 999,999.99')
			//fwrite($mArqMov,$l_valor_ti."\r\n");
			$mItau05 = '    ' . substr($l_datemis,0,2) . '/' . substr($l_datemis,2,2) . '/' . substr($l_datemis, -2) . str_repeat(' ',7) . $l_numdupl . str_repeat(' ',9) . 'DV' . str_repeat(' ',11) . $l_taceite . str_repeat(' ',31) . 
						$l_valor_ti;

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
			

			$l_campo01 = '7KJB04' . str_repeat(' ',128) . '05    ' . substr($l_datemis,0,2) . '/' . substr($l_datemis,2,2) . '/' . substr($l_datemis, -2) . str_repeat(' ',7) . $l_numdupl . str_repeat(' ',9) . 'DV' . str_repeat(' ',11) . $l_taceite . str_repeat(' ',31) . 
						$l_valor_ti . str_repeat(' ',128 - strlen($mItau05)) . '06' . str_repeat(' ',128) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$l_logser = $Tab_CADFAT->f("logser");
			$l_logser = str_pad($l_logser,35);
			$l_baiser = $Tab_CADFAT->f("baiser");
			$l_baiser = str_pad($l_baiser,20);
			$l_cidser = $Tab_CADFAT->f("cidser");
			$l_cidser = str_pad($l_cidser,20);
			$l_estser = $Tab_CADFAT->f("estser");
			$l_mesref = $Tab_CADFAT->f("mesref");

			$linha07 = 'Inscricao do Cliente: ' . $mCodCli . '           Numero da Fatura: ' . $l_numdupl . '     Mes de Referencia: ' . $l_mesref.'  ';
			$linha08 = str_repeat(' ',100);
			$linha09 = 'Local da Coleta:' . $l_logser . '  ' . $l_baiser . '  ' . $l_cidser . '/' . $l_estser . '  ';

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
			
			
			$l_campo01  = '7KJB07' . $linha07;
			$l_campo01 .=  str_repeat(' ',28) . '08' . $linha08;
			$l_campo01 .=  str_repeat(' ',28) . '09' . $linha09.  str_repeat(' ',28) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";
			/*
			   As duas primeiras letras representam as iniciais das tabelas a saber
			   MF = MovFat.
			   SS = SubSer.
			   TP = TabPbh

			*/
			$l_codfat = $Tab_CADFAT->f("codfat");
			//$MFSSTP = new clsDBfaturar();
			$MFSSTP->query("SELECT m.equpbh,m.qtdmed,m.subser,m.valser,substr(s.subred,1,58) AS subred,s.unidad,t.valpbh FROM movfat m,subser s,tabpbh t WHERE codfat='$l_codfat' AND m.subser=s.subser and m.mesref=t.mesref ORDER BY m.codfat,m.grpser,m.subser");

            for($T=10;$T<19;$T++)
			{
				$lin[$T] = str_repeat(' ',100);
			}

			$cnt = 12;
			while ($MFSSTP->next_record())
			{
				$l_equpbh = $MFSSTP->f("equpbh");
				//fwrite($mArqMov,"Equival�ncia ->$l_equpbh\r\n");
				$l_equpbh = (float)((str_replace(",", ".", $l_equpbh)));
				$l_equpbh = round($l_equpbh,2);
				//fwrite($mArqMov,"Equival�ncia ->$l_equpbh\r\n");

				$l_qtdmed = $MFSSTP->f("qtdmed");
				$l_qtdmed = (float)((str_replace(",", ".", $l_qtdmed)));
				$l_qtdmed = round($l_qtdmed,2);

				$l_subser = $MFSSTP->f("subser");

				$mval_ser = $MFSSTP->f("valser");
				$mval_ser = (float)((str_replace(",", ".", $mval_ser)));
				$mval_ser = round($mval_ser,2);

				$l_subred = str_pad($MFSSTP->f("subred"),58);

				$l_unidad = $MFSSTP->f("unidad");

				$l_valpbh = $MFSSTP->f("valpbh");
				$l_valpbh = (float)((str_replace(",", ".", $l_valpbh)));

				$l_valuni = round ($l_equpbh * $l_valpbh, 2);

				$l_valser = $mval_ser;
				$l_valfat += $l_valser;

				$lin[$cnt] = $l_subred . ' ' . str_pad(number_format($l_qtdmed, 2,'.',''),8,' ',STR_PAD_LEFT) . '  ' . str_pad($l_unidad,6) . '   ' . str_pad(number_format($l_valuni, 2,'.',''),7,' ',STR_PAD_LEFT) . '  ' . str_pad(number_format($l_valser, 2,'.',''),11,' ',STR_PAD_LEFT) . '  ';
			
				$cnt++;
			}
			//$lin[10] = str_repeat(' ',100);

			//$CadEmp = new clsDBfaturar();
			$CadEmp->query("SELECT fax,telefo,razsoc,email FROM cademp");
			$CadEmp->next_record();

			$mRazSoc = $CadEmp->f("razsoc");
			//$mRazSoc = substr($mRazSoc,0,32);
			$mRazSoc = str_pad($mRazSoc,40);

			$mTelefo = $CadEmp->f("telefo");
			$mTelefo = str_pad($mTelefo,20);

			$mEmail = $CadEmp->f("email");

	      //$lin[11] = 'DISCRIMINACAO DOS SERVICOS/PRODUTOS                         MEDICAO  UNIDAD VR.UNITAR  VALOR TOTAL  '; Alterado em 
			$lin[11] = 'DISCRIMINA��O DAS FATURAS';
			//$lin[18] = $mRazSoc . str_repeat(' ',27). ' - ' . 'Telefone: '.$mTelefo;//'SUPERINTENDENCIA DE LIMPEZA URBANA-SLU   -  SECAO COMERCIAL   -  Telefone: 3277.9360                ';
			$lin[18] = str_repeat(' ',100);
			//$lin[$cnt] = $mRazSoc . str_repeat(' ',27). ' - ' . 'Telefone: '.$mTelefo;//'SUPERINTENDENCIA DE LIMPEZA URBANA-SLU   -  SECAO COMERCIAL   -  Telefone: 3277.9360                ';

			$i = 10;
			//           Cliente pode ter at� 7 servi�os - comenteddo em 14/03/2012 - Miguel.
			while ($i<18)
			//while ($i<$cnt)
			{
				$reg++;
				$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
				

				$l_campo01  = '7KJB' . $i . substr($lin[$i].str_repeat(' ',128),0,128);
				$i++;
				$l_campo01 .=  $i .substr( $lin[$i]. str_repeat(' ',128),0,128);
				$i++;
				$l_campo01 .=  $i .substr($lin[$i] .  str_repeat(' ',128),0,128) . $registro;
				$i++;

				////     fwrite($mArqMov,$l_campo01."\r\n");
				print $l_campo01."\r\n";
			}

			$naux = $Tab_CADFAT->f("ret_inss");
			$naux = (float)(str_replace(",", ".", $naux));//substitui v�rgula por ponto
			$naux = number_format($naux,2,',','.');
			$naux = str_pad($naux,10,' ',STR_PAD_LEFT);

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
			

			//$l_campo01 = '7KJB19' . 'RETENCAO PARA A PREVIDENCIA SOCIAL (INSS): R$' . $naux . str_repeat(' ',128 - strlen('RETENCAO PARA A PREVIDENCIA SOCIAL (INSS): R$' . $naux)).
			//'20                                                                       ' . str_repeat(' ',128 - strlen('INSS(Inst. Normativa MPS/SRP No.3 de 14/07/2005 - art. 148 - inciso I).')) . '21                                '.
			//str_repeat(' ',96) . $registro;
			$mReterInss = str_pad($mReterInss,10,' ',STR_PAD_LEFT);
			$l_campo01 = '7KJB19'. str_repeat(' ',128) .
			'20' . str_repeat(' ',128) . 
			'21'.'                                                       VALOR TOTAL BRUTO DA FATURA : R$ ' . $l_valor_ti . str_repeat(' ',128 - strlen('                                                       VALOR TOTAL BRUTO DA FATURA : R$ ' . $l_valor_ti)).
			$registro;
			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
			

			//$l_campo01 = '7KJB22A SLU E IMUNE DO IMPOSTO(ISS/QN) DE ACORDO COM O ART. 150 DA CONSTITUICAO FEDERAL.                  ' . str_repeat(' ',28) . '23' .
			//'A T E N C A O: Sr CAIXA, DEDUZIR DO VALOR TOTAL DO TITULO, A RETENCAO DO INSS.                      ' . str_repeat(' ',28) . 
			//'24OBS: E OBRIGATORIO APRESENTAR JUNTO A SLU A COMPROVACAO DO PAGAMENTO DA RETENCAO ACIMA.             ' . str_repeat(' ',28) . $registro;
			////     fwrite($mArqMov,$l_campo01."\r\n");
			//'                                                               VALOR A PAGAR A SLU : R$      50,00                              '
			$lValLiq = str_pad($lValLiq,10,' ',STR_PAD_LEFT);

			$l_campo01 = '7KJB22'. str_repeat(' ',128) . 
			'23'.str_repeat(' ',128) .
			'24' . '                                 * * RETENCAO PARA A PREVIDENCIA SOCIAL ' .CCGetSession("mINSS").'% (INSS) : R$ '.$mReterInss.
			str_repeat(' ',128-strlen('                                 * * RETENCAO PARA A PREVIDENCIA SOCIAL ' .CCGetSession("mINSS").'% (INSS) : R$ '.$mReterInss)) . $registro;
			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
			

			$mFax = str_pad($CadEmp->f("fax").".",21);

			$l_campo01 = '7KJB25'.str_repeat(' ',128). 
			'26' . '                                                               VALOR A PAGAR A SLU : R$ ' . $lValLiq .
			str_repeat(' ',128-strlen('                                                               VALOR A PAGAR A SLU : R$ ' . $lValLiq)) . 
			'27' . str_repeat(' ',128) . $registro;
			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
			

			//                  1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890


			$l_campo01 = '7KJB28' . str_repeat(' ',128) . 
			             '29'.str_repeat(' ',128) .
						 '30'.str_repeat(' ',128) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
			



			$l_campo01 = '7KJB31' . str_repeat(' ',128) . 
			             '32'.'* * ATENCAO: O PAGAMENTO DA RETENCAO DO INSS DE R$ '.$mReterInss.',  E DE RESPONSABILIDADE DO CLIENTE,                              ' . 
						 str_repeat(' ',128-strlen('* * ATENCAO: O PAGAMENTO DA RETENCAO DO INSS DE R$ '.$mReterInss.',  E DE RESPONSABILIDADE DO CLIENTE,                              ')) . 
						 '33'.'             CONFORME INSTRUCAO NORMATIVA RFB  N. 1238,  DE 11 DE JANEIRO DE 2012 - "ART. 398".                                 ' . 
						 str_repeat(' ',128-strlen('             CONFORME INSTRUCAO NORMATIVA RFB  N. 1238,  DE 11 DE JANEIRO DE 2012 - "ART. 398".                                 ')) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
			

			//                  1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
			$l_campo01 = '7KJB34' . str_repeat(' ',128) . 
			             '35'.'             O COMPROVANTE DO PAGAMENTO DA RENTENCAO DEVERA SER ENVIADO VIA FAX OU E-MAIL PARA                                  ' . 
						 str_repeat(' ',128-strlen('             O COMPROVANTE DO PAGAMENTO DA RENTENCAO DEVERA SER ENVIADO VIA FAX OU E-MAIL PARA                                  ')) . 
						 '36'.'             A SLU: FAX '.$mFax.'  -  E-MAIL:  '.$mEmail.
						 str_repeat(' ',128-strlen('             A SLU: FAX '.$mFax.'  -  E-MAIL:  '.$mEmail)) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
			

			//                  1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
			$l_campo01 = '7KJB37' . str_repeat(' ',128) .
			'38'. str_repeat(' ',128). 
			'39'.'A SLU E IMUNE DO IMPOSTO(ISS/QN) DE ACORDO COM O ART. 150 DA CONSTITUICAO FEDERAL.                                              ' . 
			str_repeat(' ',128-strlen('A SLU E IMUNE DO IMPOSTO(ISS/QN) DE ACORDO COM O ART. 150 DA CONSTITUICAO FEDERAL.                                              ')) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
			

			//                  1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
			$l_campo01 = '7KJB40' . str_repeat(' ',128) .
			'41'. str_repeat(' ',128). 
			'42'.'Informacoes, duvidas ou contestacoes, deverao ser protocoladas no guiche da SLU no BH RESOLVE' . 
			str_repeat(' ',128-strlen('Informacoes, duvidas ou contestacoes, deverao ser protocoladas no guiche da SLU no BH RESOLVE')) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";

			$reg++;
			$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
			
			//                          10        20        30        40        50        60        70        80        90       100       110       120       130       140       150       160                                                 
			//                  12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
			$l_campo01 = '7KJB43'.'Av. Santos Dumont,  363 - Centro  ou Rua dos Caetes,   342 - Centro.   (guiche limpeza urbana)                                  '.
			'44'. str_repeat(' ',128). 
			'45' . str_repeat(' ',128) . $registro;

			////     fwrite($mArqMov,$l_campo01."\r\n");
			print $l_campo01."\r\n";
		}

		$reg++;
		$registro = str_repeat('0',6 - strlen((string)$reg)).(string)$reg;
		

		$l_campo01 = '9'  . str_repeat(' ',393) . $registro;
		
		////     fwrite($mArqMov,$l_campo01."\r\n");
		print $l_campo01."\r\n";

		////     fclose($mArqMov);


	    $Tab_CADFAT_Update = new clsDBfaturar();
	   	$Tab_CADFAT_Update->query("UPDATE CADFAT SET EXPORT = 'T' WHERE EXPORT = 'F'");
		$Tab_CADFAT_Update->close;
		unset($Tab_CADFAT_Update);
	   /*
   		$arquivo = $_GET["arquivo"];
   		//$arquivo = $mArquivo;
   		if(isset($arquivo) && file_exists($arquivo)){ // faz o teste se a variavel n�o esta vazia e se o arquivo realmente existe
      		switch(strtolower(substr(strrchr(basename($arquivo),"."),1))){ // verifica a extens�o do arquivo para pegar o tipo
         		case "pdf": $tipo="application/pdf"; break;
         		case "txt": $tipo="application/txt"; break;
         		case "exe": $tipo="application/octet-stream"; break;
         		case "zip": $tipo="application/zip"; break;
         		case "doc": $tipo="application/msword"; break;
         		case "xls": $tipo="application/vnd.ms-excel"; break;
         		case "ppt": $tipo="application/vnd.ms-powerpoint"; break;
         		case "gif": $tipo="image/gif"; break;
         		case "png": $tipo="image/png"; break;
         		case "jpg": $tipo="image/jpg"; break;
         		case "mp3": $tipo="audio/mpeg"; break;
         		case "php": // deixar vazio por seuran�a
         		case "htm": // deixar vazio por seuran�a
         		case "html": // deixar vazio por seuran�a
      		}
      		header("Content-Type: ".$tipo); // informa o tipo do arquivo ao navegador
      		header("Content-Length: ".filesize($arquivo)); // informa o tamanho do arquivo ao navegador
      		header("Content-Disposition: attachment; filename=".basename($arquivo)); // informa ao navegador que � tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
      		readfile($arquivo); // l� o arquivo
      		//exit; // aborta p�s-a��es
   		}*/
		$TabItau->close;
		$TabUni->close;
		$CadCli->close;
		$MFSSTP->close;
		$CadEmp->close;

		unset($TabItau);
		unset($TabUni);
		unset($CadCli);
		unset($MFSSTP);
		unset($CadEmp);
		set_time_limit(30);
     
	}
	$Tab_CADFAT->close;
	unset($Tab_CADFAT);
	exit;
//Close GridExportar_Button1_OnClick @23-8B16BD30
    return $GridExportar_Button1_OnClick;
}
//End Close GridExportar_Button1_OnClick

//GridExportar_BeforeShow @2-3CFCF87E
function GridExportar_BeforeShow(& $sender)
{
    $GridExportar_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GridExportar; //Compatibility
//End GridExportar_BeforeShow

//Custom Code @14-2A29BDB7
// -------------------------
    //$GridExportar->BTNExportar->Visible = false;
// -------------------------
//End Custom Code

//DLookup @15-7D8FE7D8
    global $DBfaturar;
    $Page = CCGetParentPage($sender);
    $mTotFatNaoExpo = CCDLookUp("count(codfat)", "CADFAT", "Export = 'F' and gerblto='S'", $Page->Connections["Faturar"]);
    $mTotFatNaoExpo = intval($mTotFatNaoExpo);
    if ($mTotFatNaoExpo == 0)
	{
       $GridExportar->BTNExportar->Visible = False;
	   $GridExportar->Button1->Visible     = False;
	   $GridExportar->btnBBCobrancaCNAB240->Visible = False;
       $GridExportar->LBLToaFat->SetValue(0);
	   $GridExportar->LBLMensagem->SetValue("Nenhuma fatura foi encontrada.");
	}
	else
	{
       $GridExportar->BTNExportar->Visible = False;
	   $GridExportar->Button1->Visible     = False;
	   $GridExportar->btnBBCobrancaCNAB240->Visible = True;
       $GridExportar->LBLToaFat->SetValue($mTotFatNaoExpo);
	   $GridExportar->LBLMensagem->SetValue("Clique no bot�o Exportar para iniciar.");
	}
//End DLookup

//Close GridExportar_BeforeShow @2-9A5B05BE
    return $GridExportar_BeforeShow;
}
//End Close GridExportar_BeforeShow

//Page_BeforeShow @1-76B20401
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GeracaoBB; //Compatibility
//End Page_BeforeShow

//Custom Code @28-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(45);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow

//DEL  // -------------------------
//DEL  // -------------------------

//DEL  // -------------------------
//DEL  // -------------------------



?>