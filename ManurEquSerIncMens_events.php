<?php
//BindEvents Method @1-603E614B
function BindEvents()
{
    global $InclusaoMensalTabPre;
    global $CCSEvents;
    $InclusaoMensalTabPre->MesAnoInc->CCSEvents["BeforeShow"] = "InclusaoMensalTabPre_MesAnoInc_BeforeShow";
    $InclusaoMensalTabPre->ufir->CCSEvents["BeforeShow"] = "InclusaoMensalTabPre_ufir_BeforeShow";
    $InclusaoMensalTabPre->Button_Insert->CCSEvents["OnClick"] = "InclusaoMensalTabPre_Button_Insert_OnClick";
    $InclusaoMensalTabPre->Button_Insert->CCSEvents["BeforeShow"] = "InclusaoMensalTabPre_Button_Insert_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//InclusaoMensalTabPre_MesAnoInc_BeforeShow @5-686986A5
function InclusaoMensalTabPre_MesAnoInc_BeforeShow(& $sender)
{
    $InclusaoMensalTabPre_MesAnoInc_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $InclusaoMensalTabPre; //Compatibility
//End InclusaoMensalTabPre_MesAnoInc_BeforeShow

//Custom Code @12-2A29BDB7
// -------------------------
    // Write your own code here.
        global $DBfaturar;
        $Page = CCGetParentPage($sender);
        $ccs_result = CCDLookUp("max(to_number(substr(mesref,4,4)))", "equser", "", $Page->Connections["Faturar"]);
        $ccs_result = substr($ccs_result."",0,4);
  	  //echo $ccs_result;
  
        $Tabela = new clsDBfaturar();
        $Tabela->query("SELECT mesref FROM equser where substr(mesref,4,4)='".$ccs_result."' order by mesref desc");
        $Tabela->next_record();
        {
           $mAno = $ccs_result;
           //$ccs_result = CCDLookUp("mesref ", "equser", "mid(mesref,4,4)='".$mAno."' order by mesref desc", $Page->Connections["Faturar"]);
           $ccs_result = $Tabela->f("mesref");
        }
        $ccs_result = substr($ccs_result,0,2);
        $mMes = $ccs_result;
  	  $InclusaoMensalTabPre->BaseMesRef->SetValue($mMes."/".$mAno);
      $mMes +=0;
  	  if ($mMes < 12)
  	  {
  	     $mMes+=1;
  		 if ($mMes < 10)
  		 {
  		    $mMes = "0".$mMes;
  		 }   
  	  }
  	  else
  	  {
  	     $mMes = "01";
  		 $mAno +=1;
  	  }
  	  $InclusaoMensalTabPre->MesAnoInc->SetValue($mMes.'/'.$mAno);
      $mValPBH  = CCDLookUp("valpbh", "tabpbh", "mesref ='".$InclusaoMensalTabPre->BaseMesRef->Value."'", $Page->Connections["Faturar"]);
	  $InclusaoMensalTabPre->ufir->SetValue($mValPBH);
// -------------------------
//End Custom Code

//Close InclusaoMensalTabPre_MesAnoInc_BeforeShow @5-2328EB08
    return $InclusaoMensalTabPre_MesAnoInc_BeforeShow;
}
//End Close InclusaoMensalTabPre_MesAnoInc_BeforeShow

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL      global $DBfaturar;
//DEL      $Page = CCGetParentPage($sender);
//DEL      $mDataAtu  = CCDLookUp("date()", "", "", $Page->Connections["Faturar"]);
//DEL      //$mDataAtu  = strval($ccs_result);
//DEL  	$InclusaoMensalTabPre->MesAnoInc->SetValue(substr($mDataAtu,5,2).'/'.substr($mDataAtu,0,4));
//DEL  
//DEL  
//DEL  // -------------------------

//InclusaoMensalTabPre_ufir_BeforeShow @14-84C76D2F
function InclusaoMensalTabPre_ufir_BeforeShow(& $sender)
{
    $InclusaoMensalTabPre_ufir_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $InclusaoMensalTabPre; //Compatibility
//End InclusaoMensalTabPre_ufir_BeforeShow

//Custom Code @15-2A29BDB7
// -------------------------
    // Write your own code here.
// -------------------------
//End Custom Code

//Close InclusaoMensalTabPre_ufir_BeforeShow @14-0AC80ECB
    return $InclusaoMensalTabPre_ufir_BeforeShow;
}
//End Close InclusaoMensalTabPre_ufir_BeforeShow

//InclusaoMensalTabPre_Button_Insert_OnClick @7-03EC8446
function InclusaoMensalTabPre_Button_Insert_OnClick(& $sender)
{
    $InclusaoMensalTabPre_Button_Insert_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $InclusaoMensalTabPre; //Compatibility
//End InclusaoMensalTabPre_Button_Insert_OnClick

//Custom Code @13-2A29BDB7
// -------------------------
    // Write your own code here.
    global $DBfaturar;
    $Page = CCGetParentPage($sender);
	//0123456789
	//2008-06-20

    //$mValPBH  = CCDLookUp("valpbh", "tabpbh", "mesref ='".$InclusaoMensalTabPre->BaseMesRef->Value."'", $Page->Connections["Faturar"]);
    $mValPBH =$InclusaoMensalTabPre->ufir->Value;

    $mCodInc = CCGetSession("IDUsuario");

    $mDataAtu  = CCDLookUp("to_char(sysdate,'dd/mm/yyyy')", "dual", "", $Page->Connections["Faturar"]);
    $Tabela = new clsDBfaturar();
    $Tabela->query("SELECT subser,equpbh FROM equser where mesref='".$InclusaoMensalTabPre->BaseMesRef->Value."' order by mesref,subser");
    $Tabela_Insert = new clsDBfaturar();
	//$handle = fopen('equser.txt', 'w');
    while ($Tabela->next_record())
    {
	   //fwrite  ($handle  ,  "mEquPBH (antes) = $mEquPBH");
	   //fwrite  ($handle  ,  "mValEqu (antes) = $mValEqu");
	   $mSubSer = $Tabela->f("subser");
	   $mMesRef = $InclusaoMensalTabPre->MesAnoInc->Value;
	   if (is_float($Tabela->f("equpbh")) and (strrpos($Tabela->f("equpbh"),',') === false))
	   {
	   		$mEquPBH = $Tabela->f("equpbh");

	   }
	   else
	   {
	   		$mEquPBH = str_replace  (',','.',$Tabela->f("equpbh") );
	   		$mEquPBH = floatval($mEquPBH);
		    //fwrite  ($handle  ,  "mEquPBH  = ".$mEquPBH."\r\n");
	   }
	   //$mEquPBH = floatval($Tabela->f("equpbh"));
	   //$mCodInc = $Tabela->f("codinc");
	   $mValEqu = floatval(round($mValPBH * $mEquPBH,2));
	   //fwrite  ($handle  ,  "mEquPBH  = ".$Tabela->f("equpbh")."\r\n");
	   //fwrite  ($handle  ,  "mEquPBH+0= ".($Tabela->f("equpbh")+0)."\r\n");
	   //fwrite  ($handle  ,  "mEquPBH  = ".$mEquPBH."\r\n");

	   //fwrite  ($handle  ,  "mValEqu (depois) = $mValEqu\r\n");
	   $Tabela_Insert->query("insert into equser (subser,mesref,equpbh,valequ,codinc,datinc) values ('$mSubSer','$mMesRef',$mEquPBH,$mValEqu,'$mCodInc','$mDataAtu')");
	}
	//fclose($handle);
// -------------------------
//End Custom Code

//Close InclusaoMensalTabPre_Button_Insert_OnClick @7-2079DEF4
    return $InclusaoMensalTabPre_Button_Insert_OnClick;
}
//End Close InclusaoMensalTabPre_Button_Insert_OnClick

//InclusaoMensalTabPre_Button_Insert_BeforeShow @7-38058788
function InclusaoMensalTabPre_Button_Insert_BeforeShow(& $sender)
{
    $InclusaoMensalTabPre_Button_Insert_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $InclusaoMensalTabPre; //Compatibility
//End InclusaoMensalTabPre_Button_Insert_BeforeShow

//Custom Code @16-2A29BDB7
// -------------------------
    // Write your own code here.
      global $DBfaturar;
      $Page = CCGetParentPage($sender);
      $mValPBH  = CCDLookUp("valpbh", "tabpbh", "mesref ='".$InclusaoMensalTabPre->BaseMesRef->Value."'", $Page->Connections["Faturar"]);
	  $InclusaoMensalTabPre->ufir->SetValue($mValPBH);
	  if ($mValPBH == null or $mValPBH == 0)
	  {
	     $InclusaoMensalTabPre->Button_Insert->Visible = false;
	  }
// -------------------------
//End Custom Code

//Close InclusaoMensalTabPre_Button_Insert_BeforeShow @7-3AAA7A28
    return $InclusaoMensalTabPre_Button_Insert_BeforeShow;
}
//End Close InclusaoMensalTabPre_Button_Insert_BeforeShow

//Page_BeforeShow @1-60DEBFD1
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManurEquSerIncMens; //Compatibility
//End Page_BeforeShow

//Custom Code @20-2A29BDB7
// -------------------------

        include("controle_acesso.php");
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(16);
        controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL        global $DBfaturar;
//DEL        $Page = CCGetParentPage($sender);
//DEL        $ccs_result = CCDLookUp("max(val(mid(mesref,4,4)))", "equser", "", $Page->Connections["Faturar"]);
//DEL        $ccs_result = substr($ccs_result."",0,4);
//DEL  	  //echo $ccs_result;
//DEL  
//DEL        $Tabela = new clsDBfaturar();
//DEL        $Tabela->query("SELECT mesref FROM equser where mid(mesref,4,4)='".$ccs_result."' order by mesref desc");
//DEL        $Tabela->next_record();
//DEL        {
//DEL           $mAno = $ccs_result;
//DEL           //$ccs_result = CCDLookUp("mesref ", "equser", "mid(mesref,4,4)='".$mAno."' order by mesref desc", $Page->Connections["Faturar"]);
//DEL           $ccs_result = $Tabela->f("mesref");
//DEL        }
//DEL        $ccs_result = substr($ccs_result,0,2);
//DEL        $mMes = $ccs_result;
//DEL  	  $InclusaoMensalTabPre->BaseMesRef->SetValue($mMes."/".$mAno);
//DEL  	  if ($mMes < 12)
//DEL  	  {
//DEL  	     $mMes+=1;
//DEL  		 if ($mMes < 10)
//DEL  		 {
//DEL  		    $mMes = "0".$mMes;
//DEL  		 }   
//DEL  	  }
//DEL  	  else
//DEL  	  {
//DEL  	     $mMes = "01";
//DEL  		 $mAno +=1;
//DEL  	  }
//DEL  	  $InclusaoMensalTabPre->MesAnoInc->SetValue($mMes.'/'.$mAno);
//DEL  
//DEL  // -------------------------



?>
