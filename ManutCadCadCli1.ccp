<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="4" sourceType="Table" urlType="Relative" secured="False" allowInsert="True" allowUpdate="True" allowDelete="True" validateData="True" preserveParameters="GET" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" connection="Faturar" name="CADCLI" dataSource="CADCLI" errorSummator="Error" wizardCaption="Add/Edit CADCLI " wizardFormMethod="post" pasteAsReplace="pasteAsReplace" removeParameters="CODCLI" returnPage="CadCadCli.ccp">
			<Components>
				<Label id="152" fieldSourceType="DBColumn" dataType="Date" html="False" name="lblDATINC" fieldSource="DATINC" format="dd/mm/yyyy">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Hidden id="185" fieldSourceType="DBColumn" dataType="Text" name="DATINC">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<Label id="153" fieldSourceType="DBColumn" dataType="Date" html="False" name="lblDATALT" fieldSource="DATALT" format="dd/mm/yyyy">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Hidden id="184" fieldSourceType="DBColumn" dataType="Date" name="DATALT" fieldSource="DATALT" format="dd/mm/yyyy">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<TextBox id="145" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="OPUS" fieldSource="OPUS">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="11" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="CODCLI" fieldSource="CODCLI" required="True" caption="Número de inscrição" wizardCaption="CODCLI" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="DLookup" actionCategory="Database" id="44" typeOfTarget="Control" expression="&quot;max(codcli)+1&quot;" domain="&quot;cadcli&quot;" connection="Faturar" target="CODCLI" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="12" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="DESCLI" fieldSource="DESCLI" required="True" caption="Cliente" wizardCaption="DESCLI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="45" eventType="Client"/>
							</Actions>
						</Event>
						<Event name="OnValidate" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="80" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="13" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="DESFAN" fieldSource="DESFAN" required="True" caption="Nome Fantasia" wizardCaption="DESFAN" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="46" eventType="Client"/>
							</Actions>
						</Event>
						<Event name="OnValidate" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="81" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<RadioButton id="19" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" html="True" returnValueType="Number" name="CODTIP" fieldSource="CODTIP" required="True" caption="Tipo do Cliente" wizardCaption="CODTIP" wizardSize="1" wizardMaxLength="1" wizardIsPassword="False" wizardUseTemplateBlock="False" connection="Faturar" _valueOfList="F" _nameOfList="Pessoa Física" dataSource="J;Pessoa Jurídica;F;Pessoa Física">
					<Components/>
					<Events>
						<Event name="OnClick" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="51" eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</RadioButton>
				<TextBox id="20" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="CGCCPF" fieldSource="CGCCPF" required="True" caption="CGC/CPF" wizardCaption="CGCCPF" wizardSize="14" wizardMaxLength="14" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="50" eventType="Client"/>
							</Actions>
						</Event>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="89" eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<ListBox id="14" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="CODUNI" fieldSource="CODUNI" required="True" caption="Divisão" wizardCaption="CODUNI" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="TABUNI" boundColumn="CODUNI" textColumn="DESUNI">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<ListBox id="15" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="CODDIS" fieldSource="CODDIS" required="True" caption="Distrito" wizardCaption="CODDIS" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="TABDIS" boundColumn="CODDIS" textColumn="DESDIS">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<ListBox id="16" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" returnValueType="Number" name="CODSET" fieldSource="CODSET" required="False" caption="Setor" wizardCaption="CODSET" wizardSize="1" wizardMaxLength="1" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardEmptyCaption="Select Value" connection="Faturar" _valueOfList="Z" _nameOfList="Z" dataSource="A;A;B;B;C;C;D;D;E;E;F;F;G;G;H;H;I;I;J;J;K;K;L;L;M;M;N;N;P;P;Q;Q;R;R;S;S;T;T;U;U;V;V;X;X;Y;Y;W;W;Z;Z">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<ListBox id="17" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="GRPATI" required="True" caption="Atividade" wizardCaption="GRPATI" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardEmptyCaption="Select Value" fieldSource="GRPATI" connection="Faturar" activeCollection="TableParameters" dataSource="GRPATI" boundColumn="GRPATI" textColumn="DESATI" orderBy="DESATI">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="48" eventType="Client"/>
							</Actions>
						</Event>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="178" eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<TableParameters>
					</TableParameters>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="92" tableName="GRPATI" posWidth="144" posHeight="108" posLeft="10" posTop="10"/>
					</JoinTables>
					<JoinLinks>
					</JoinLinks>
					<Fields>
						<Field id="97" tableName="GRPATI" fieldName="DESATI"/>
						<Field id="177" tableName="GRPATI" fieldName="GRPATI"/>
					</Fields>
					<Attributes/>
					<Features/>
				</ListBox>
				<ListBox id="18" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="SUBATI" fieldSource="SUBATI" required="True" caption="Sub-Atividade" wizardCaption="SUBATI" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="SUBATI" textColumn="DESSUB" boundColumn="SUBATI" activeCollection="TableParameters" orderBy="DESSUB">
					<Components/>
					<Events>
					</Events>
					<TableParameters>
						<TableParameter id="179" conditionType="Parameter" useIsNull="False" field="GRPATI" dataType="Text" searchConditionType="Equal" parameterType="Form" logicOperator="And" parameterSource="GRPATI"/>
					</TableParameters>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="175" tableName="SUBATI" posLeft="10" posTop="10" posWidth="116" posHeight="174"/>
					</JoinTables>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<TextBox id="21" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="LOGRAD" fieldSource="LOGRAD" required="True" caption="Logradouro" wizardCaption="LOGRAD" wizardSize="35" wizardMaxLength="35" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="52" eventType="Client"/>
							</Actions>
						</Event>
						<Event name="OnValidate" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="82" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="22" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="BAIRRO" fieldSource="BAIRRO" required="True" caption="Bairro" wizardCaption="BAIRRO" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="53" eventType="Client"/>
							</Actions>
						</Event>
						<Event name="OnValidate" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="83" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="23" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="CIDADE" fieldSource="CIDADE" required="True" caption="Cidade" wizardCaption="CIDADE" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="54" eventType="Client"/>
							</Actions>
						</Event>
						<Event name="OnValidate" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="84" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<ListBox id="24" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="ESTADO" fieldSource="ESTADO" required="True" caption="Estado" wizardCaption="ESTADO" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" sourceType="ListOfValues" connection="Faturar" _valueOfList="ES" _nameOfList="ESPÍRITO SANTO" dataSource="AC;ACRE;AL;ALAGOAS;AM;AMAZONAS;AP;AMAPÁ;BA;BAHIA;CE;CEARÁ;DF;DISTRITO FEDERAL;ES;ESPÍRITO SANTO;GO;GOIÁS;MA;MARANHÃO;MG;MINAS GERAIS;MS;MATO GROSSO DO SUL;MT;MATO GROSSO;PA;PARÁ;PB;PARAÍBA;PE;PERNAMBUCO;PI;PIAUÍ;PR;PARANÁ;RJ;RIO DE JANEIRO;RN;RIO GRANDE DO NORTE;RO;RONDÔNIA;RR;RORAIMA;RS;RIO GRANDE DO SUL;SC;SANTA CATARINA;SE;SERGIPE;SP;SÃO PAULO;TO;TOCANTINS">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="55" eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
				</ListBox>
				<TextBox id="25" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="CODPOS" fieldSource="CODPOS" required="True" caption="Código Postal" wizardCaption="CODPOS" wizardSize="9" wizardMaxLength="9" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Validate Entry" actionCategory="Validation" id="78" required="True" minimumLength="9" maximumLength="9" inputMask="00000-000" eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="26" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="TELEFO" fieldSource="TELEFO" required="False" caption="Telefone" wizardCaption="TELEFO" wizardSize="13" wizardMaxLength="13" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnKeyPress" type="Client">
							<Actions>
								<Action actionName="Validate Entry" actionCategory="Validation" id="189" minimumLength="12" maximumLength="13" inputMask="(00)000000000"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="27" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="TELFAX" fieldSource="TELFAX" required="False" caption="Número do Fax" wizardCaption="TELFAX" wizardSize="13" wizardMaxLength="13" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnKeyPress" type="Client">
							<Actions>
								<Action actionName="Validate Entry" actionCategory="Validation" id="190" minimumLength="12" maximumLength="13" inputMask="(00)000000000"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="28" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="CONTAT" fieldSource="CONTAT" required="False" caption="Contato" wizardCaption="CONTAT" wizardSize="35" wizardMaxLength="35" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="59" eventType="Client"/>
							</Actions>
						</Event>
						<Event name="OnValidate" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="85" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="158" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="EMAIL" fieldSource="EMAIL">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="29" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="LOGCOB" fieldSource="LOGCOB" required="True" caption="Logradouro de Cobrança" wizardCaption="LOGCOB" wizardSize="35" wizardMaxLength="35" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="60" eventType="Client"/>
							</Actions>
						</Event>
						<Event name="OnValidate" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="86" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="30" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="BAICOB" fieldSource="BAICOB" required="True" caption="Bairro de Cobrança" wizardCaption="BAICOB" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="61" eventType="Client"/>
							</Actions>
						</Event>
						<Event name="OnValidate" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="87" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="31" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="CIDCOB" fieldSource="CIDCOB" required="True" caption="Cidade de Cobrança" wizardCaption="CIDCOB" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="62" eventType="Client"/>
							</Actions>
						</Event>
						<Event name="OnValidate" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="88" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<ListBox id="32" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="ESTCOB" fieldSource="ESTCOB" required="True" caption="Estado de Cobrança" wizardCaption="ESTCOB" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" sourceType="ListOfValues" dataSource="AC;ACRE;AL;ALAGOAS;AM;AMAZONAS;AP;AMAPÁ;BA;BAHIA;CE;CEARÁ;DF;DISTRITO FEDERAL;ES;ESPÍRITO SANTO;GO;GOIÁS;MA;MARANHÃO;MG;MINAS GERAIS;MS;MATO GROSSO DO SUL;MT;MATO GROSSO;PA;PARÁ;PB;PARAÍBA;PE;PERNAMBUCO;PI;PIAUÍ;PR;PARANÁ;RJ;RIO DE JANEIRO;RN;RIO GRANDE DO NORTE;RO;RONDÔNIA;RR;RORAIMA;RS;RIO GRANDE DO SUL;SC;SANTA CATARINA;SE;SERGIPE;SP;SÃO PAULO;TO;TOCANTINS">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="63" eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
				</ListBox>
				<TextBox id="33" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="POSCOB" fieldSource="POSCOB" required="True" caption="Código Postal de Cobrança" wizardCaption="POSCOB" wizardSize="9" wizardMaxLength="9" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Validate Entry" actionCategory="Validation" id="79" required="True" minimumLength="9" maximumLength="9" inputMask="00000-000" eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="34" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="TELCOB" fieldSource="TELCOB" required="False" caption="Telefone de Cobrança" wizardCaption="TELCOB" wizardSize="13" wizardMaxLength="13" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnKeyPress" type="Client">
							<Actions>
								<Action actionName="Validate Entry" actionCategory="Validation" id="191" minimumLength="12" maximumLength="13" inputMask="(00)000000000"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<RadioButton id="35" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" html="False" returnValueType="Number" name="CODSIT" fieldSource="CODSIT" required="True" caption="Situação" wizardCaption="CODSIT" wizardSize="1" wizardMaxLength="1" wizardIsPassword="False" wizardUseTemplateBlock="False" connection="Faturar" _valueOfList="I" _nameOfList="Inativo" dataSource="A;Ativo;I;Inativo">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</RadioButton>
				<Label id="159" fieldSourceType="DBColumn" dataType="Date" html="False" name="LblDATINATIV" format="dd/mm/yyyy" fieldSource="DATINATIV">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Hidden id="160" fieldSourceType="DBColumn" dataType="Date" name="DATINATIV" fieldSource="DATINATIV" format="dd/mm/yyyy">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<ListBox id="154" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="CODINATIV" wizardEmptyCaption="Select Value" fieldSource="CODINATIV" connection="Faturar" dataSource="SITINATIV" boundColumn="CODINATIV" textColumn="DESCSIT">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="155" tableName="SITINATIV" schemaName="SL005V3" posLeft="10" posTop="10" posWidth="95" posHeight="88"/>
					</JoinTables>
					<JoinLinks/>
					<Fields>
						<Field id="156" tableName="SITINATIV" fieldName="DESCSIT"/>
						<Field id="157" tableName="SITINATIV" fieldName="CODINATIV"/>
					</Fields>
					<Attributes/>
					<Features/>
				</ListBox>
				<RadioButton id="42" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" html="False" returnValueType="Number" name="PGTOELET" fieldSource="PGTOELET" required="True" caption="Pagamento Eletrônico?" wizardCaption="PGTOELET" wizardSize="1" wizardMaxLength="1" wizardIsPassword="False" wizardUseTemplateBlock="False" connection="Faturar" _valueOfList="N" _nameOfList="Não" dataSource="S;Sim;N;Não">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</RadioButton>
				<RadioButton id="43" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" html="False" returnValueType="Number" name="ESFERA" fieldSource="ESFERA" required="True" caption="Esfera Municipal, Estadual ou Federal?" wizardCaption="ESFERA" wizardSize="5" wizardMaxLength="5" wizardIsPassword="False" wizardUseTemplateBlock="False" connection="Faturar" _valueOfList="N" _nameOfList="Não" dataSource="S;Sim;N;Não">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</RadioButton>
				<Hidden id="37" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" name="CODALT" fieldSource="CODALT" required="False" caption="CODALT" wizardCaption="CODALT" wizardSize="3" wizardMaxLength="3" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<Hidden id="71" fieldSourceType="DBColumn" dataType="Integer" name="CODINC" fieldSource="CODINC">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<RadioButton id="181" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" html="False" returnValueType="Number" name="GERBLTO" fieldSource="GERBLTO" connection="Faturar" _valueOfList="S" _nameOfList="Sim" dataSource="S;Sim;N;Não" required="True">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</RadioButton>
				<RadioButton id="187" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" html="True" returnValueType="Number" name="RECOBR" fieldSource="RECOBR" _valueOfList="N" _nameOfList="Não" dataSource="S;Sim;N;Não" connection="Faturar" required="True" defaultValue="&quot;S&quot;">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</RadioButton>
				<Button id="5" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Insert" operation="Insert" wizardCaption="Adicionar">
					<Components/>
					<Events>
						<Event name="OnClick" type="Server">
							<Actions>
								<Action actionName="Declare Variable" actionCategory="General" id="74" name="mCGCCPF" type="Text" initialValue="$CADCLI-&gt;CGCCPF-&gt;Value" eventType="Server"/>
								<Action actionName="DLookup" actionCategory="Database" id="73" typeOfTarget="Variable" expression="&quot;CGCCPF&quot;" domain="&quot;CADCLI&quot;" criteria="&quot;CGCCPF='$mCGCCPF'&quot;" connection="Faturar" target="mCGC_CPF" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="6" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Update" operation="Update" wizardCaption="Mudar">
					<Components/>
					<Events>
						<Event name="OnClick" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="72" eventType="Client"/>
							</Actions>
						</Event>
						<Event name="OnClick" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="77" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="7" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Delete" operation="Delete" wizardCaption="Apagar">
					<Components/>
					<Events>
						<Event name="OnClick" type="Client">
							<Actions>
								<Action actionName="Confirmation Message" actionCategory="General" id="8" message="Delete record?" eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="9" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Cancel" operation="Cancel" wizardCaption="Cancelar">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Hidden id="186" fieldSourceType="CodeExpression" dataType="Text" name="desativou" defaultValue="&quot;N&quot;">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<TextArea id="103" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="OBS" fieldSource="OBS">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="144" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextArea>
			</Components>
			<Events>
				<Event name="BeforeInsert" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="66" eventType="Server"/>
					</Actions>
				</Event>
				<Event name="BeforeUpdate" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="67" eventType="Server"/>
					</Actions>
				</Event>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Declare Variable" actionCategory="General" id="70" name="mCODCLI" type="Text" initialValue="$CADCLI-&gt;CODCLI-&gt;Value" eventType="Server"/>
						<Action actionName="DLookup" actionCategory="Database" id="68" typeOfTarget="Variable" expression="&quot;CODCLI&quot;" domain="&quot;CADFAT&quot;" criteria="&quot;CODCLI='$mCODCLI'&quot;" connection="Faturar" target="mFatura" eventType="Server"/>
						<Action actionName="Custom Code" actionCategory="General" id="91" eventType="Server"/>
					</Actions>
				</Event>
				<Event name="AfterUpdate" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="143" eventType="Server"/>
					</Actions>
				</Event>
				<Event name="OnLoad" type="Client">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="180" eventType="Client"/>
					</Actions>
				</Event>
				<Event name="OnValidate" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="192"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters>
				<TableParameter id="10" conditionType="Parameter" useIsNull="False" field="CODCLI" parameterSource="CODCLI" dataType="Text" logicOperator="And" searchConditionType="Equal" parameterType="URL" orderNumber="1"/>
			</TableParameters>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables>
				<JoinTable id="104" tableName="CADCLI" posLeft="10" posTop="10" posWidth="450" posHeight="425"/>
			</JoinTables>
			<JoinLinks/>
			<Fields>
				<Field id="105" tableName="CADCLI" fieldName="OBS"/>
				<Field id="108" tableName="CADCLI" fieldName="CODSIT"/>
				<Field id="109" tableName="CADCLI" fieldName="EMAIL"/>
				<Field id="110" tableName="CADCLI" fieldName="OPUS"/>
				<Field id="111" tableName="CADCLI" fieldName="ESFERA"/>
				<Field id="112" tableName="CADCLI" fieldName="PGTOELET"/>
				<Field id="115" tableName="CADCLI" fieldName="CODALT"/>
				<Field id="116" tableName="CADCLI" fieldName="CODINC"/>
				<Field id="117" tableName="CADCLI" fieldName="TELCOB"/>
				<Field id="118" tableName="CADCLI" fieldName="POSCOB"/>
				<Field id="119" tableName="CADCLI" fieldName="ESTCOB"/>
				<Field id="120" tableName="CADCLI" fieldName="CIDCOB"/>
				<Field id="121" tableName="CADCLI" fieldName="BAICOB"/>
				<Field id="122" tableName="CADCLI" fieldName="LOGCOB"/>
				<Field id="123" tableName="CADCLI" fieldName="CONTAT"/>
				<Field id="124" tableName="CADCLI" fieldName="TELFAX"/>
				<Field id="125" tableName="CADCLI" fieldName="TELEFO"/>
				<Field id="126" tableName="CADCLI" fieldName="CODPOS"/>
				<Field id="127" tableName="CADCLI" fieldName="ESTADO"/>
				<Field id="128" tableName="CADCLI" fieldName="CIDADE"/>
				<Field id="129" tableName="CADCLI" fieldName="BAIRRO"/>
				<Field id="130" tableName="CADCLI" fieldName="LOGRAD"/>
				<Field id="131" tableName="CADCLI" fieldName="CGCCPF"/>
				<Field id="132" tableName="CADCLI" fieldName="CODTIP"/>
				<Field id="133" tableName="CADCLI" fieldName="SUBATI"/>
				<Field id="134" tableName="CADCLI" fieldName="GRPATI"/>
				<Field id="135" tableName="CADCLI" fieldName="CODSET"/>
				<Field id="136" tableName="CADCLI" fieldName="CODDIS"/>
				<Field id="137" tableName="CADCLI" fieldName="CODUNI"/>
				<Field id="138" tableName="CADCLI" fieldName="DESFAN"/>
				<Field id="139" tableName="CADCLI" fieldName="DESCLI"/>
				<Field id="140" tableName="CADCLI" fieldName="CODCLI"/>
				<Field id="141" tableName="CADCLI" fieldName="IDINC"/>
				<Field id="142" tableName="CADCLI" fieldName="IDALT"/>
				<Field id="148" tableName="CADCLI" fieldName="DATALT"/>
				<Field id="150" tableName="CADCLI" fieldName="DATINATIV"/>
				<Field id="151" tableName="CADCLI" fieldName="DATINC"/>
				<Field id="182" tableName="CADCLI" fieldName="GERBLTO"/>
				<Field id="183" tableName="CADCLI" fieldName="CODINATIV"/>
				<Field id="188" tableName="CADCLI" fieldName="RECOBR"/>
			</Fields>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="161" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="Faturar" dataSource="MOVSER, SUBSER" name="MOVSER_SUBSER" pageSizeLimit="100" wizardCaption="List of MOVSER, SUBSER " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="Não encontrado." activeCollection="TableParameters">
			<Components>
				<Sorter id="165" visible="True" name="Sorter_DESSUB" column="DESSUB" wizardCaption="DESSUB" wizardSortingType="SimpleDir" wizardControl="DESSUB" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="166" visible="True" name="Sorter_DATINI" column="DATINI" wizardCaption="DATINI" wizardSortingType="SimpleDir" wizardControl="DATINI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="167" visible="True" name="Sorter_DATFIM" column="DATFIM" wizardCaption="DATFIM" wizardSortingType="SimpleDir" wizardControl="DATFIM" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="168" visible="True" name="Sorter_QTDMED" column="QTDMED" wizardCaption="QTDMED" wizardSortingType="SimpleDir" wizardControl="QTDMED" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Label id="169" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESSUB" fieldSource="DESSUB" wizardCaption="DESSUB" wizardSize="50" wizardMaxLength="75" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="170" fieldSourceType="DBColumn" dataType="Date" html="False" name="DATINI" fieldSource="DATINI" wizardCaption="DATINI" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="171" fieldSourceType="DBColumn" dataType="Date" html="False" name="DATFIM" fieldSource="DATFIM" wizardCaption="DATFIM" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="172" fieldSourceType="DBColumn" dataType="Float" html="False" name="QTDMED" fieldSource="QTDMED" wizardCaption="QTDMED" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="173" size="10" type="Centered" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="of" wizardImagesScheme="Blueprint">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="174" conditionType="Parameter" useIsNull="False" field="MOVSER.CODCLI" dataType="Text" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="CODCLI"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="162" tableName="MOVSER" schemaName="SL005V3" posLeft="10" posTop="10" posWidth="115" posHeight="401"/>
				<JoinTable id="163" tableName="SUBSER" schemaName="SL005V3" posLeft="354" posTop="48" posWidth="115" posHeight="247"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="164" tableLeft="MOVSER" tableRight="SUBSER" fieldLeft="MOVSER.SUBSER" fieldRight="SUBSER.SUBSER" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields/>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="ManutCadCadCli1.php" forShow="True" url="ManutCadCadCli1.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="ManutCadCadCli1_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="75" groupID="1"/>
		<Group id="76" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="90"/>
			</Actions>
		</Event>
	</Events>
</Page>
