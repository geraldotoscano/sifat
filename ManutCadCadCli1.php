<?php
//Include Common Files @1-F89F9DD0
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "ManutCadCadCli1.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files
include_once 'util.php';

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordCADCLI { //CADCLI Class @4-3C33833D

//Variables @4-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @4-5BF27DC0
    function clsRecordCADCLI($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record CADCLI/Error";
        $this->DataSource = new clsCADCLIDataSource($this);
        $this->ds = & $this->DataSource;
        $this->InsertAllowed = true;
        $this->UpdateAllowed = true;
        $this->DeleteAllowed = true;
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "CADCLI";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->lblDATINC = new clsControl(ccsLabel, "lblDATINC", "lblDATINC", ccsDate, array("dd", "/", "mm", "/", "yyyy"), CCGetRequestParam("lblDATINC", $Method, NULL), $this);
            $this->DATINC = new clsControl(ccsHidden, "DATINC", "DATINC", ccsText, "", CCGetRequestParam("DATINC", $Method, NULL), $this);
            $this->lblDATALT = new clsControl(ccsLabel, "lblDATALT", "lblDATALT", ccsDate, array("dd", "/", "mm", "/", "yyyy"), CCGetRequestParam("lblDATALT", $Method, NULL), $this);
            $this->DATALT = new clsControl(ccsHidden, "DATALT", "DATALT", ccsDate, array("dd", "/", "mm", "/", "yyyy"), CCGetRequestParam("DATALT", $Method, NULL), $this);
            $this->OPUS = new clsControl(ccsTextBox, "OPUS", "OPUS", ccsText, "", CCGetRequestParam("OPUS", $Method, NULL), $this);
            $this->CODCLI = new clsControl(ccsTextBox, "CODCLI", "N�mero de inscri��o", ccsText, "", CCGetRequestParam("CODCLI", $Method, NULL), $this);
            $this->CODCLI->Required = true;
            $this->DESCLI = new clsControl(ccsTextBox, "DESCLI", "Cliente", ccsText, "", CCGetRequestParam("DESCLI", $Method, NULL), $this);
            $this->DESCLI->Required = true;
            $this->DESFAN = new clsControl(ccsTextBox, "DESFAN", "Nome Fantasia", ccsText, "", CCGetRequestParam("DESFAN", $Method, NULL), $this);
            $this->DESFAN->Required = true;
            $this->CODTIP = new clsControl(ccsRadioButton, "CODTIP", "Tipo do Cliente", ccsText, "", CCGetRequestParam("CODTIP", $Method, NULL), $this);
            $this->CODTIP->DSType = dsListOfValues;
            $this->CODTIP->Values = array(array("J", "Pessoa Jur�dica"), array("F", "Pessoa F�sica"));
            $this->CODTIP->HTML = true;
            $this->CODTIP->Required = true;
            $this->CGCCPF = new clsControl(ccsTextBox, "CGCCPF", "CGC/CPF", ccsText, "", CCGetRequestParam("CGCCPF", $Method, NULL), $this);
            $this->CGCCPF->Required = true;
            $this->CODUNI = new clsControl(ccsListBox, "CODUNI", "Divis�o", ccsText, "", CCGetRequestParam("CODUNI", $Method, NULL), $this);
            $this->CODUNI->DSType = dsTable;
            $this->CODUNI->DataSource = new clsDBFaturar();
            $this->CODUNI->ds = & $this->CODUNI->DataSource;
            $this->CODUNI->DataSource->SQL = "SELECT * \n" .
"FROM TABUNI {SQL_Where} {SQL_OrderBy}";
            list($this->CODUNI->BoundColumn, $this->CODUNI->TextColumn, $this->CODUNI->DBFormat) = array("CODUNI", "DESUNI", "");
            $this->CODUNI->Required = true;
            $this->CODDIS = new clsControl(ccsListBox, "CODDIS", "Distrito", ccsText, "", CCGetRequestParam("CODDIS", $Method, NULL), $this);
            $this->CODDIS->DSType = dsTable;
            $this->CODDIS->DataSource = new clsDBFaturar();
            $this->CODDIS->ds = & $this->CODDIS->DataSource;
            $this->CODDIS->DataSource->SQL = "SELECT * \n" .
"FROM TABDIS {SQL_Where} {SQL_OrderBy}";
            list($this->CODDIS->BoundColumn, $this->CODDIS->TextColumn, $this->CODDIS->DBFormat) = array("CODDIS", "DESDIS", "");
            $this->CODDIS->Required = true;
            $this->CODSET = new clsControl(ccsListBox, "CODSET", "Setor", ccsText, "", CCGetRequestParam("CODSET", $Method, NULL), $this);
            $this->CODSET->DSType = dsListOfValues;
            $this->CODSET->Values = array(array("A", "A"), array("B", "B"), array("C", "C"), array("D", "D"), array("E", "E"), array("F", "F"), array("G", "G"), array("H", "H"), array("I", "I"), array("J", "J"), array("K", "K"), array("L", "L"), array("M", "M"), array("N", "N"), array("P", "P"), array("Q", "Q"), array("R", "R"), array("S", "S"), array("T", "T"), array("U", "U"), array("V", "V"), array("X", "X"), array("Y", "Y"), array("W", "W"), array("Z", "Z"));
            $this->GRPATI = new clsControl(ccsListBox, "GRPATI", "Atividade", ccsText, "", CCGetRequestParam("GRPATI", $Method, NULL), $this);
            $this->GRPATI->DSType = dsTable;
            $this->GRPATI->DataSource = new clsDBFaturar();
            $this->GRPATI->ds = & $this->GRPATI->DataSource;
            $this->GRPATI->DataSource->SQL = "SELECT DESATI, GRPATI \n" .
"FROM GRPATI {SQL_Where} {SQL_OrderBy}";
            $this->GRPATI->DataSource->Order = "DESATI";
            list($this->GRPATI->BoundColumn, $this->GRPATI->TextColumn, $this->GRPATI->DBFormat) = array("GRPATI", "DESATI", "");
            $this->GRPATI->DataSource->Order = "DESATI";
            $this->GRPATI->Required = true;
            $this->SUBATI = new clsControl(ccsListBox, "SUBATI", "Sub-Atividade", ccsText, "", CCGetRequestParam("SUBATI", $Method, NULL), $this);
            $this->SUBATI->DSType = dsTable;
            $this->SUBATI->DataSource = new clsDBFaturar();
            $this->SUBATI->ds = & $this->SUBATI->DataSource;
            $this->SUBATI->DataSource->SQL = "SELECT * \n" .
"FROM SUBATI {SQL_Where} {SQL_OrderBy}";
            $this->SUBATI->DataSource->Order = "DESSUB";
            list($this->SUBATI->BoundColumn, $this->SUBATI->TextColumn, $this->SUBATI->DBFormat) = array("SUBATI", "DESSUB", "");
            $this->SUBATI->DataSource->Parameters["postGRPATI"] = CCGetFromPost("GRPATI", NULL);
            $this->SUBATI->DataSource->wp = new clsSQLParameters();
            $this->SUBATI->DataSource->wp->AddParameter("1", "postGRPATI", ccsText, "", "", $this->SUBATI->DataSource->Parameters["postGRPATI"], "", false);
            $this->SUBATI->DataSource->wp->Criterion[1] = $this->SUBATI->DataSource->wp->Operation(opEqual, "GRPATI", $this->SUBATI->DataSource->wp->GetDBValue("1"), $this->SUBATI->DataSource->ToSQL($this->SUBATI->DataSource->wp->GetDBValue("1"), ccsText),false);
            $this->SUBATI->DataSource->Where = 
                 $this->SUBATI->DataSource->wp->Criterion[1];
            $this->SUBATI->DataSource->Order = "DESSUB";
            $this->SUBATI->Required = true;
            $this->LOGRAD = new clsControl(ccsTextBox, "LOGRAD", "Logradouro", ccsText, "", CCGetRequestParam("LOGRAD", $Method, NULL), $this);
            $this->LOGRAD->Required = true;
            $this->BAIRRO = new clsControl(ccsTextBox, "BAIRRO", "Bairro", ccsText, "", CCGetRequestParam("BAIRRO", $Method, NULL), $this);
            $this->BAIRRO->Required = true;
            $this->CIDADE = new clsControl(ccsTextBox, "CIDADE", "Cidade", ccsText, "", CCGetRequestParam("CIDADE", $Method, NULL), $this);
            $this->CIDADE->Required = true;
            $this->ESTADO = new clsControl(ccsListBox, "ESTADO", "Estado", ccsText, "", CCGetRequestParam("ESTADO", $Method, NULL), $this);
            $this->ESTADO->DSType = dsListOfValues;
            $this->ESTADO->Values = array(array("AC", "ACRE"), array("AL", "ALAGOAS"), array("AM", "AMAZONAS"), array("AP", "AMAP�"), array("BA", "BAHIA"), array("CE", "CEAR�"), array("DF", "DISTRITO FEDERAL"), array("ES", "ESP�RITO SANTO"), array("GO", "GOI�S"), array("MA", "MARANH�O"), array("MG", "MINAS GERAIS"), array("MS", "MATO GROSSO DO SUL"), array("MT", "MATO GROSSO"), array("PA", "PAR�"), array("PB", "PARA�BA"), array("PE", "PERNAMBUCO"), array("PI", "PIAU�"), array("PR", "PARAN�"), array("RJ", "RIO DE JANEIRO"), array("RN", "RIO GRANDE DO NORTE"), array("RO", "ROND�NIA"), array("RR", "RORAIMA"), array("RS", "RIO GRANDE DO SUL"), array("SC", "SANTA CATARINA"), array("SE", "SERGIPE"), array("SP", "S�O PAULO"), array("TO", "TOCANTINS"));
            $this->ESTADO->Required = true;
            $this->CODPOS = new clsControl(ccsTextBox, "CODPOS", "C�digo Postal", ccsText, "", CCGetRequestParam("CODPOS", $Method, NULL), $this);
            $this->CODPOS->Required = true;
            $this->TELEFO = new clsControl(ccsTextBox, "TELEFO", "Telefone", ccsText, "", CCGetRequestParam("TELEFO", $Method, NULL), $this);
            $this->TELFAX = new clsControl(ccsTextBox, "TELFAX", "N�mero do Fax", ccsText, "", CCGetRequestParam("TELFAX", $Method, NULL), $this);
            $this->CONTAT = new clsControl(ccsTextBox, "CONTAT", "Contato", ccsText, "", CCGetRequestParam("CONTAT", $Method, NULL), $this);
            $this->EMAIL = new clsControl(ccsTextBox, "EMAIL", "EMAIL", ccsText, "", CCGetRequestParam("EMAIL", $Method, NULL), $this);
            $this->LOGCOB = new clsControl(ccsTextBox, "LOGCOB", "Logradouro de Cobran�a", ccsText, "", CCGetRequestParam("LOGCOB", $Method, NULL), $this);
            $this->LOGCOB->Required = true;
            $this->BAICOB = new clsControl(ccsTextBox, "BAICOB", "Bairro de Cobran�a", ccsText, "", CCGetRequestParam("BAICOB", $Method, NULL), $this);
            $this->BAICOB->Required = true;
            $this->CIDCOB = new clsControl(ccsTextBox, "CIDCOB", "Cidade de Cobran�a", ccsText, "", CCGetRequestParam("CIDCOB", $Method, NULL), $this);
            $this->CIDCOB->Required = true;
            $this->ESTCOB = new clsControl(ccsListBox, "ESTCOB", "Estado de Cobran�a", ccsText, "", CCGetRequestParam("ESTCOB", $Method, NULL), $this);
            $this->ESTCOB->DSType = dsListOfValues;
            $this->ESTCOB->Values = array(array("AC", "ACRE"), array("AL", "ALAGOAS"), array("AM", "AMAZONAS"), array("AP", "AMAP�"), array("BA", "BAHIA"), array("CE", "CEAR�"), array("DF", "DISTRITO FEDERAL"), array("ES", "ESP�RITO SANTO"), array("GO", "GOI�S"), array("MA", "MARANH�O"), array("MG", "MINAS GERAIS"), array("MS", "MATO GROSSO DO SUL"), array("MT", "MATO GROSSO"), array("PA", "PAR�"), array("PB", "PARA�BA"), array("PE", "PERNAMBUCO"), array("PI", "PIAU�"), array("PR", "PARAN�"), array("RJ", "RIO DE JANEIRO"), array("RN", "RIO GRANDE DO NORTE"), array("RO", "ROND�NIA"), array("RR", "RORAIMA"), array("RS", "RIO GRANDE DO SUL"), array("SC", "SANTA CATARINA"), array("SE", "SERGIPE"), array("SP", "S�O PAULO"), array("TO", "TOCANTINS"));
            $this->ESTCOB->Required = true;
            $this->POSCOB = new clsControl(ccsTextBox, "POSCOB", "C�digo Postal de Cobran�a", ccsText, "", CCGetRequestParam("POSCOB", $Method, NULL), $this);
            $this->POSCOB->Required = true;
            $this->TELCOB = new clsControl(ccsTextBox, "TELCOB", "Telefone de Cobran�a", ccsText, "", CCGetRequestParam("TELCOB", $Method, NULL), $this);
            $this->CODSIT = new clsControl(ccsRadioButton, "CODSIT", "Situa��o", ccsText, "", CCGetRequestParam("CODSIT", $Method, NULL), $this);
            $this->CODSIT->DSType = dsListOfValues;
            $this->CODSIT->Values = array(array("A", "Ativo"), array("I", "Inativo"));
            $this->CODSIT->Required = true;
            $this->LblDATINATIV = new clsControl(ccsLabel, "LblDATINATIV", "LblDATINATIV", ccsDate, array("dd", "/", "mm", "/", "yyyy"), CCGetRequestParam("LblDATINATIV", $Method, NULL), $this);
            $this->DATINATIV = new clsControl(ccsHidden, "DATINATIV", "DATINATIV", ccsDate, array("dd", "/", "mm", "/", "yyyy"), CCGetRequestParam("DATINATIV", $Method, NULL), $this);
            $this->CODINATIV = new clsControl(ccsListBox, "CODINATIV", "CODINATIV", ccsText, "", CCGetRequestParam("CODINATIV", $Method, NULL), $this);
            $this->CODINATIV->DSType = dsTable;
            $this->CODINATIV->DataSource = new clsDBFaturar();
            $this->CODINATIV->ds = & $this->CODINATIV->DataSource;
            $this->CODINATIV->DataSource->SQL = "SELECT DESCSIT, CODINATIV \n" .
"FROM SITINATIV {SQL_Where} {SQL_OrderBy}";
            list($this->CODINATIV->BoundColumn, $this->CODINATIV->TextColumn, $this->CODINATIV->DBFormat) = array("CODINATIV", "DESCSIT", "");
            $this->PGTOELET = new clsControl(ccsRadioButton, "PGTOELET", "Pagamento Eletr�nico?", ccsText, "", CCGetRequestParam("PGTOELET", $Method, NULL), $this);
            $this->PGTOELET->DSType = dsListOfValues;
            $this->PGTOELET->Values = array(array("S", "Sim"), array("N", "N�o"));
            $this->PGTOELET->Required = true;
            $this->ESFERA = new clsControl(ccsRadioButton, "ESFERA", "Esfera Municipal, Estadual ou Federal?", ccsText, "", CCGetRequestParam("ESFERA", $Method, NULL), $this);
            $this->ESFERA->DSType = dsListOfValues;
            $this->ESFERA->Values = array(array("S", "Sim"), array("N", "N�o"));
            $this->ESFERA->Required = true;
            $this->CODALT = new clsControl(ccsHidden, "CODALT", "CODALT", ccsInteger, "", CCGetRequestParam("CODALT", $Method, NULL), $this);
            $this->CODINC = new clsControl(ccsHidden, "CODINC", "CODINC", ccsInteger, "", CCGetRequestParam("CODINC", $Method, NULL), $this);
            $this->GERBLTO = new clsControl(ccsRadioButton, "GERBLTO", "GERBLTO", ccsText, "", CCGetRequestParam("GERBLTO", $Method, NULL), $this);
            $this->GERBLTO->DSType = dsListOfValues;
            $this->GERBLTO->Values = array(array("S", "Sim"), array("N", "N�o"));
            $this->GERBLTO->Required = true;
            $this->RECOBR = new clsControl(ccsRadioButton, "RECOBR", "RECOBR", ccsText, "", CCGetRequestParam("RECOBR", $Method, NULL), $this);
            $this->RECOBR->DSType = dsListOfValues;
            $this->RECOBR->Values = array(array("S", "Sim"), array("N", "N�o"));
            $this->RECOBR->HTML = true;
            $this->RECOBR->Required = true;
            $this->Button_Insert = new clsButton("Button_Insert", $Method, $this);
            $this->Button_Update = new clsButton("Button_Update", $Method, $this);
            $this->Button_Delete = new clsButton("Button_Delete", $Method, $this);
            $this->Button_Cancel = new clsButton("Button_Cancel", $Method, $this);
            $this->desativou = new clsControl(ccsHidden, "desativou", "desativou", ccsText, "", CCGetRequestParam("desativou", $Method, NULL), $this);
            $this->OBS = new clsControl(ccsTextArea, "OBS", "OBS", ccsText, "", CCGetRequestParam("OBS", $Method, NULL), $this);
            if(!$this->FormSubmitted) {
                if(!is_array($this->RECOBR->Value) && !strlen($this->RECOBR->Value) && $this->RECOBR->Value !== false)
                    $this->RECOBR->SetText("S");
                if(!is_array($this->desativou->Value) && !strlen($this->desativou->Value) && $this->desativou->Value !== false)
                    $this->desativou->SetText("N");
            }
        }
    }
//End Class_Initialize Event

//Initialize Method @4-B2F3EC7E
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->DataSource->Parameters["urlCODCLI"] = CCGetFromGet("CODCLI", NULL);
    }
//End Initialize Method

//Validate Method @4-4BCCBC5B
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->DATINC->Validate() && $Validation);
        $Validation = ($this->DATALT->Validate() && $Validation);
        $Validation = ($this->OPUS->Validate() && $Validation);
        $Validation = ($this->CODCLI->Validate() && $Validation);
        $Validation = ($this->DESCLI->Validate() && $Validation);
        $Validation = ($this->DESFAN->Validate() && $Validation);
        $Validation = ($this->CODTIP->Validate() && $Validation);
        $Validation = ($this->CGCCPF->Validate() && $Validation);
        $Validation = ($this->CODUNI->Validate() && $Validation);
        $Validation = ($this->CODDIS->Validate() && $Validation);
        $Validation = ($this->CODSET->Validate() && $Validation);
        $Validation = ($this->GRPATI->Validate() && $Validation);
        $Validation = ($this->SUBATI->Validate() && $Validation);
        $Validation = ($this->LOGRAD->Validate() && $Validation);
        $Validation = ($this->BAIRRO->Validate() && $Validation);
        $Validation = ($this->CIDADE->Validate() && $Validation);
        $Validation = ($this->ESTADO->Validate() && $Validation);
        $Validation = ($this->CODPOS->Validate() && $Validation);
        $Validation = ($this->TELEFO->Validate() && $Validation);
        $Validation = ($this->TELFAX->Validate() && $Validation);
        $Validation = ($this->CONTAT->Validate() && $Validation);
        $Validation = ($this->EMAIL->Validate() && $Validation);
        $Validation = ($this->LOGCOB->Validate() && $Validation);
        $Validation = ($this->BAICOB->Validate() && $Validation);
        $Validation = ($this->CIDCOB->Validate() && $Validation);
        $Validation = ($this->ESTCOB->Validate() && $Validation);
        $Validation = ($this->POSCOB->Validate() && $Validation);
        $Validation = ($this->TELCOB->Validate() && $Validation);
        $Validation = ($this->CODSIT->Validate() && $Validation);
        $Validation = ($this->DATINATIV->Validate() && $Validation);
        $Validation = ($this->CODINATIV->Validate() && $Validation);
        $Validation = ($this->PGTOELET->Validate() && $Validation);
        $Validation = ($this->ESFERA->Validate() && $Validation);
        $Validation = ($this->CODALT->Validate() && $Validation);
        $Validation = ($this->CODINC->Validate() && $Validation);
        $Validation = ($this->GERBLTO->Validate() && $Validation);
        $Validation = ($this->RECOBR->Validate() && $Validation);
        $Validation = ($this->desativou->Validate() && $Validation);
        $Validation = ($this->OBS->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->DATINC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DATALT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->OPUS->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DESCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DESFAN->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODTIP->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CGCCPF->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODUNI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODDIS->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODSET->Errors->Count() == 0);
        $Validation =  $Validation && ($this->GRPATI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->SUBATI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->LOGRAD->Errors->Count() == 0);
        $Validation =  $Validation && ($this->BAIRRO->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CIDADE->Errors->Count() == 0);
        $Validation =  $Validation && ($this->ESTADO->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODPOS->Errors->Count() == 0);
        $Validation =  $Validation && ($this->TELEFO->Errors->Count() == 0);
        $Validation =  $Validation && ($this->TELFAX->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CONTAT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->EMAIL->Errors->Count() == 0);
        $Validation =  $Validation && ($this->LOGCOB->Errors->Count() == 0);
        $Validation =  $Validation && ($this->BAICOB->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CIDCOB->Errors->Count() == 0);
        $Validation =  $Validation && ($this->ESTCOB->Errors->Count() == 0);
        $Validation =  $Validation && ($this->POSCOB->Errors->Count() == 0);
        $Validation =  $Validation && ($this->TELCOB->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODSIT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DATINATIV->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODINATIV->Errors->Count() == 0);
        $Validation =  $Validation && ($this->PGTOELET->Errors->Count() == 0);
        $Validation =  $Validation && ($this->ESFERA->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODALT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODINC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->GERBLTO->Errors->Count() == 0);
        $Validation =  $Validation && ($this->RECOBR->Errors->Count() == 0);
        $Validation =  $Validation && ($this->desativou->Errors->Count() == 0);
        $Validation =  $Validation && ($this->OBS->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @4-E66FF668
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->lblDATINC->Errors->Count());
        $errors = ($errors || $this->DATINC->Errors->Count());
        $errors = ($errors || $this->lblDATALT->Errors->Count());
        $errors = ($errors || $this->DATALT->Errors->Count());
        $errors = ($errors || $this->OPUS->Errors->Count());
        $errors = ($errors || $this->CODCLI->Errors->Count());
        $errors = ($errors || $this->DESCLI->Errors->Count());
        $errors = ($errors || $this->DESFAN->Errors->Count());
        $errors = ($errors || $this->CODTIP->Errors->Count());
        $errors = ($errors || $this->CGCCPF->Errors->Count());
        $errors = ($errors || $this->CODUNI->Errors->Count());
        $errors = ($errors || $this->CODDIS->Errors->Count());
        $errors = ($errors || $this->CODSET->Errors->Count());
        $errors = ($errors || $this->GRPATI->Errors->Count());
        $errors = ($errors || $this->SUBATI->Errors->Count());
        $errors = ($errors || $this->LOGRAD->Errors->Count());
        $errors = ($errors || $this->BAIRRO->Errors->Count());
        $errors = ($errors || $this->CIDADE->Errors->Count());
        $errors = ($errors || $this->ESTADO->Errors->Count());
        $errors = ($errors || $this->CODPOS->Errors->Count());
        $errors = ($errors || $this->TELEFO->Errors->Count());
        $errors = ($errors || $this->TELFAX->Errors->Count());
        $errors = ($errors || $this->CONTAT->Errors->Count());
        $errors = ($errors || $this->EMAIL->Errors->Count());
        $errors = ($errors || $this->LOGCOB->Errors->Count());
        $errors = ($errors || $this->BAICOB->Errors->Count());
        $errors = ($errors || $this->CIDCOB->Errors->Count());
        $errors = ($errors || $this->ESTCOB->Errors->Count());
        $errors = ($errors || $this->POSCOB->Errors->Count());
        $errors = ($errors || $this->TELCOB->Errors->Count());
        $errors = ($errors || $this->CODSIT->Errors->Count());
        $errors = ($errors || $this->LblDATINATIV->Errors->Count());
        $errors = ($errors || $this->DATINATIV->Errors->Count());
        $errors = ($errors || $this->CODINATIV->Errors->Count());
        $errors = ($errors || $this->PGTOELET->Errors->Count());
        $errors = ($errors || $this->ESFERA->Errors->Count());
        $errors = ($errors || $this->CODALT->Errors->Count());
        $errors = ($errors || $this->CODINC->Errors->Count());
        $errors = ($errors || $this->GERBLTO->Errors->Count());
        $errors = ($errors || $this->RECOBR->Errors->Count());
        $errors = ($errors || $this->desativou->Errors->Count());
        $errors = ($errors || $this->OBS->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @4-554C1E67
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->DataSource->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->DataSource->AllParametersSet;
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = $this->EditMode ? "Button_Update" : "Button_Insert";
            if($this->Button_Insert->Pressed) {
                $this->PressedButton = "Button_Insert";
            } else if($this->Button_Update->Pressed) {
                $this->PressedButton = "Button_Update";
            } else if($this->Button_Delete->Pressed) {
                $this->PressedButton = "Button_Delete";
            } else if($this->Button_Cancel->Pressed) {
                $this->PressedButton = "Button_Cancel";
            }
        }
        $Redirect = "CadCadCli.php" . "?" . CCGetQueryString("QueryString", array("ccsForm", "CODCLI"));
        if($this->PressedButton == "Button_Delete") {
            if(!CCGetEvent($this->Button_Delete->CCSEvents, "OnClick", $this->Button_Delete) || !$this->DeleteRow()) {
                $Redirect = "";
            }
        } else if($this->PressedButton == "Button_Cancel") {
            if(!CCGetEvent($this->Button_Cancel->CCSEvents, "OnClick", $this->Button_Cancel)) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button_Insert") {
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick", $this->Button_Insert) || !$this->InsertRow()) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "Button_Update") {
                if(!CCGetEvent($this->Button_Update->CCSEvents, "OnClick", $this->Button_Update) || !$this->UpdateRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
        if ($Redirect)
            $this->DataSource->close();
    }
//End Operation Method

//InsertRow Method @4-C283F265
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert", $this);
        if(!$this->InsertAllowed) return false;
        $this->DataSource->lblDATINC->SetValue($this->lblDATINC->GetValue(true));
        $this->DataSource->DATINC->SetValue($this->DATINC->GetValue(true));
        $this->DataSource->lblDATALT->SetValue($this->lblDATALT->GetValue(true));
        $this->DataSource->DATALT->SetValue($this->DATALT->GetValue(true));
        $this->DataSource->OPUS->SetValue($this->OPUS->GetValue(true));
        $this->DataSource->CODCLI->SetValue($this->CODCLI->GetValue(true));
        $this->DataSource->DESCLI->SetValue($this->DESCLI->GetValue(true));
        $this->DataSource->DESFAN->SetValue($this->DESFAN->GetValue(true));
        $this->DataSource->CODTIP->SetValue($this->CODTIP->GetValue(true));
        $this->DataSource->CGCCPF->SetValue($this->CGCCPF->GetValue(true));
        $this->DataSource->CODUNI->SetValue($this->CODUNI->GetValue(true));
        $this->DataSource->CODDIS->SetValue($this->CODDIS->GetValue(true));
        $this->DataSource->CODSET->SetValue($this->CODSET->GetValue(true));
        $this->DataSource->GRPATI->SetValue($this->GRPATI->GetValue(true));
        $this->DataSource->SUBATI->SetValue($this->SUBATI->GetValue(true));
        $this->DataSource->LOGRAD->SetValue($this->LOGRAD->GetValue(true));
        $this->DataSource->BAIRRO->SetValue($this->BAIRRO->GetValue(true));
        $this->DataSource->CIDADE->SetValue($this->CIDADE->GetValue(true));
        $this->DataSource->ESTADO->SetValue($this->ESTADO->GetValue(true));
        $this->DataSource->CODPOS->SetValue($this->CODPOS->GetValue(true));
        $this->DataSource->TELEFO->SetValue($this->TELEFO->GetValue(true));
        $this->DataSource->TELFAX->SetValue($this->TELFAX->GetValue(true));
        $this->DataSource->CONTAT->SetValue($this->CONTAT->GetValue(true));
        $this->DataSource->EMAIL->SetValue($this->EMAIL->GetValue(true));
        $this->DataSource->LOGCOB->SetValue($this->LOGCOB->GetValue(true));
        $this->DataSource->BAICOB->SetValue($this->BAICOB->GetValue(true));
        $this->DataSource->CIDCOB->SetValue($this->CIDCOB->GetValue(true));
        $this->DataSource->ESTCOB->SetValue($this->ESTCOB->GetValue(true));
        $this->DataSource->POSCOB->SetValue($this->POSCOB->GetValue(true));
        $this->DataSource->TELCOB->SetValue($this->TELCOB->GetValue(true));
        $this->DataSource->CODSIT->SetValue($this->CODSIT->GetValue(true));
        $this->DataSource->LblDATINATIV->SetValue($this->LblDATINATIV->GetValue(true));
        $this->DataSource->DATINATIV->SetValue($this->DATINATIV->GetValue(true));
        $this->DataSource->CODINATIV->SetValue($this->CODINATIV->GetValue(true));
        $this->DataSource->PGTOELET->SetValue($this->PGTOELET->GetValue(true));
        $this->DataSource->ESFERA->SetValue($this->ESFERA->GetValue(true));
        $this->DataSource->CODALT->SetValue($this->CODALT->GetValue(true));
        $this->DataSource->CODINC->SetValue($this->CODINC->GetValue(true));
        $this->DataSource->GERBLTO->SetValue($this->GERBLTO->GetValue(true));
        $this->DataSource->RECOBR->SetValue($this->RECOBR->GetValue(true));
        $this->DataSource->OBS->SetValue($this->OBS->GetValue(true));
        $this->DataSource->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert", $this);
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//UpdateRow Method @4-1C698748
    function UpdateRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeUpdate", $this);
        if(!$this->UpdateAllowed) return false;
        $this->DataSource->lblDATINC->SetValue($this->lblDATINC->GetValue(true));
        $this->DataSource->DATINC->SetValue($this->DATINC->GetValue(true));
        $this->DataSource->lblDATALT->SetValue($this->lblDATALT->GetValue(true));
        $this->DataSource->DATALT->SetValue($this->DATALT->GetValue(true));
        $this->DataSource->OPUS->SetValue($this->OPUS->GetValue(true));
        $this->DataSource->CODCLI->SetValue($this->CODCLI->GetValue(true));
        $this->DataSource->DESCLI->SetValue($this->DESCLI->GetValue(true));
        $this->DataSource->DESFAN->SetValue($this->DESFAN->GetValue(true));
        $this->DataSource->CODTIP->SetValue($this->CODTIP->GetValue(true));
        $this->DataSource->CGCCPF->SetValue($this->CGCCPF->GetValue(true));
        $this->DataSource->CODUNI->SetValue($this->CODUNI->GetValue(true));
        $this->DataSource->CODDIS->SetValue($this->CODDIS->GetValue(true));
        $this->DataSource->CODSET->SetValue($this->CODSET->GetValue(true));
        $this->DataSource->GRPATI->SetValue($this->GRPATI->GetValue(true));
        $this->DataSource->SUBATI->SetValue($this->SUBATI->GetValue(true));
        $this->DataSource->LOGRAD->SetValue($this->LOGRAD->GetValue(true));
        $this->DataSource->BAIRRO->SetValue($this->BAIRRO->GetValue(true));
        $this->DataSource->CIDADE->SetValue($this->CIDADE->GetValue(true));
        $this->DataSource->ESTADO->SetValue($this->ESTADO->GetValue(true));
        $this->DataSource->CODPOS->SetValue($this->CODPOS->GetValue(true));
        $this->DataSource->TELEFO->SetValue($this->TELEFO->GetValue(true));
        $this->DataSource->TELFAX->SetValue($this->TELFAX->GetValue(true));
        $this->DataSource->CONTAT->SetValue($this->CONTAT->GetValue(true));
        $this->DataSource->EMAIL->SetValue($this->EMAIL->GetValue(true));
        $this->DataSource->LOGCOB->SetValue($this->LOGCOB->GetValue(true));
        $this->DataSource->BAICOB->SetValue($this->BAICOB->GetValue(true));
        $this->DataSource->CIDCOB->SetValue($this->CIDCOB->GetValue(true));
        $this->DataSource->ESTCOB->SetValue($this->ESTCOB->GetValue(true));
        $this->DataSource->POSCOB->SetValue($this->POSCOB->GetValue(true));
        $this->DataSource->TELCOB->SetValue($this->TELCOB->GetValue(true));
        $this->DataSource->CODSIT->SetValue($this->CODSIT->GetValue(true));
        $this->DataSource->LblDATINATIV->SetValue($this->LblDATINATIV->GetValue(true));
        $this->DataSource->DATINATIV->SetValue($this->DATINATIV->GetValue(true));
        $this->DataSource->CODINATIV->SetValue($this->CODINATIV->GetValue(true));
        $this->DataSource->PGTOELET->SetValue($this->PGTOELET->GetValue(true));
        $this->DataSource->ESFERA->SetValue($this->ESFERA->GetValue(true));
        $this->DataSource->CODALT->SetValue($this->CODALT->GetValue(true));
        $this->DataSource->CODINC->SetValue($this->CODINC->GetValue(true));
        $this->DataSource->GERBLTO->SetValue($this->GERBLTO->GetValue(true));
        $this->DataSource->RECOBR->SetValue($this->RECOBR->GetValue(true));
        $this->DataSource->OBS->SetValue($this->OBS->GetValue(true));
        $this->DataSource->Update();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterUpdate", $this);
        return (!$this->CheckErrors());
    }
//End UpdateRow Method

//DeleteRow Method @4-299D98C3
    function DeleteRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDelete", $this);
        if(!$this->DeleteAllowed) return false;
        $this->DataSource->Delete();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDelete", $this);
        return (!$this->CheckErrors());
    }
//End DeleteRow Method

//Show Method @4-BA9D4D4B
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->CODTIP->Prepare();
        $this->CODUNI->Prepare();
        $this->CODDIS->Prepare();
        $this->CODSET->Prepare();
        $this->GRPATI->Prepare();
        $this->SUBATI->Prepare();
        $this->ESTADO->Prepare();
        $this->ESTCOB->Prepare();
        $this->CODSIT->Prepare();
        $this->CODINATIV->Prepare();
        $this->PGTOELET->Prepare();
        $this->ESFERA->Prepare();
        $this->GERBLTO->Prepare();
        $this->RECOBR->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->DataSource->Errors->Count()){
                $this->Errors->AddErrors($this->DataSource->Errors);
                $this->DataSource->Errors->clear();
            }
            $this->DataSource->Open();
            if($this->DataSource->Errors->Count() == 0 && $this->DataSource->next_record()) {
                $this->DataSource->SetValues();
                $this->lblDATINC->SetValue($this->DataSource->lblDATINC->GetValue());
                $this->lblDATALT->SetValue($this->DataSource->lblDATALT->GetValue());
                $this->LblDATINATIV->SetValue($this->DataSource->LblDATINATIV->GetValue());
                if(!$this->FormSubmitted){
                    $this->DATALT->SetValue($this->DataSource->DATALT->GetValue());
                    $this->OPUS->SetValue($this->DataSource->OPUS->GetValue());
                    $this->CODCLI->SetValue($this->DataSource->CODCLI->GetValue());
                    $this->DESCLI->SetValue($this->DataSource->DESCLI->GetValue());
                    $this->DESFAN->SetValue($this->DataSource->DESFAN->GetValue());
                    $this->CODTIP->SetValue($this->DataSource->CODTIP->GetValue());
                    $this->CGCCPF->SetValue($this->DataSource->CGCCPF->GetValue());
                    $this->CODUNI->SetValue($this->DataSource->CODUNI->GetValue());
                    $this->CODDIS->SetValue($this->DataSource->CODDIS->GetValue());
                    $this->CODSET->SetValue($this->DataSource->CODSET->GetValue());
                    $this->GRPATI->SetValue($this->DataSource->GRPATI->GetValue());
                    $this->SUBATI->SetValue($this->DataSource->SUBATI->GetValue());
                    $this->LOGRAD->SetValue($this->DataSource->LOGRAD->GetValue());
                    $this->BAIRRO->SetValue($this->DataSource->BAIRRO->GetValue());
                    $this->CIDADE->SetValue($this->DataSource->CIDADE->GetValue());
                    $this->ESTADO->SetValue($this->DataSource->ESTADO->GetValue());
                    $this->CODPOS->SetValue($this->DataSource->CODPOS->GetValue());
                    $this->TELEFO->SetValue($this->DataSource->TELEFO->GetValue());
                    $this->TELFAX->SetValue($this->DataSource->TELFAX->GetValue());
                    $this->CONTAT->SetValue($this->DataSource->CONTAT->GetValue());
                    $this->EMAIL->SetValue($this->DataSource->EMAIL->GetValue());
                    $this->LOGCOB->SetValue($this->DataSource->LOGCOB->GetValue());
                    $this->BAICOB->SetValue($this->DataSource->BAICOB->GetValue());
                    $this->CIDCOB->SetValue($this->DataSource->CIDCOB->GetValue());
                    $this->ESTCOB->SetValue($this->DataSource->ESTCOB->GetValue());
                    $this->POSCOB->SetValue($this->DataSource->POSCOB->GetValue());
                    $this->TELCOB->SetValue($this->DataSource->TELCOB->GetValue());
                    $this->CODSIT->SetValue($this->DataSource->CODSIT->GetValue());
                    $this->DATINATIV->SetValue($this->DataSource->DATINATIV->GetValue());
                    $this->CODINATIV->SetValue($this->DataSource->CODINATIV->GetValue());
                    $this->PGTOELET->SetValue($this->DataSource->PGTOELET->GetValue());
                    $this->ESFERA->SetValue($this->DataSource->ESFERA->GetValue());
                    $this->CODALT->SetValue($this->DataSource->CODALT->GetValue());
                    $this->CODINC->SetValue($this->DataSource->CODINC->GetValue());
                    $this->GERBLTO->SetValue($this->DataSource->GERBLTO->GetValue());
                    $this->RECOBR->SetValue($this->DataSource->RECOBR->GetValue());
                    $this->OBS->SetValue($this->DataSource->OBS->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->lblDATINC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DATINC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->lblDATALT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DATALT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->OPUS->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DESCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DESFAN->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODTIP->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CGCCPF->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODUNI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODDIS->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODSET->Errors->ToString());
            $Error = ComposeStrings($Error, $this->GRPATI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->SUBATI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->LOGRAD->Errors->ToString());
            $Error = ComposeStrings($Error, $this->BAIRRO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CIDADE->Errors->ToString());
            $Error = ComposeStrings($Error, $this->ESTADO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODPOS->Errors->ToString());
            $Error = ComposeStrings($Error, $this->TELEFO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->TELFAX->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CONTAT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->EMAIL->Errors->ToString());
            $Error = ComposeStrings($Error, $this->LOGCOB->Errors->ToString());
            $Error = ComposeStrings($Error, $this->BAICOB->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CIDCOB->Errors->ToString());
            $Error = ComposeStrings($Error, $this->ESTCOB->Errors->ToString());
            $Error = ComposeStrings($Error, $this->POSCOB->Errors->ToString());
            $Error = ComposeStrings($Error, $this->TELCOB->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODSIT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->LblDATINATIV->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DATINATIV->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODINATIV->Errors->ToString());
            $Error = ComposeStrings($Error, $this->PGTOELET->Errors->ToString());
            $Error = ComposeStrings($Error, $this->ESFERA->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODALT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODINC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->GERBLTO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->RECOBR->Errors->ToString());
            $Error = ComposeStrings($Error, $this->desativou->Errors->ToString());
            $Error = ComposeStrings($Error, $this->OBS->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataSource->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Insert->Visible = !$this->EditMode && $this->InsertAllowed;
        $this->Button_Update->Visible = $this->EditMode && $this->UpdateAllowed;
        $this->Button_Delete->Visible = $this->EditMode && $this->DeleteAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->lblDATINC->Show();
        $this->DATINC->Show();
        $this->lblDATALT->Show();
        $this->DATALT->Show();
        $this->OPUS->Show();
        $this->CODCLI->Show();
        $this->DESCLI->Show();
        $this->DESFAN->Show();
        $this->CODTIP->Show();
        $this->CGCCPF->Show();
        $this->CODUNI->Show();
        $this->CODDIS->Show();
        $this->CODSET->Show();
        $this->GRPATI->Show();
        $this->SUBATI->Show();
        $this->LOGRAD->Show();
        $this->BAIRRO->Show();
        $this->CIDADE->Show();
        $this->ESTADO->Show();
        $this->CODPOS->Show();
        $this->TELEFO->Show();
        $this->TELFAX->Show();
        $this->CONTAT->Show();
        $this->EMAIL->Show();
        $this->LOGCOB->Show();
        $this->BAICOB->Show();
        $this->CIDCOB->Show();
        $this->ESTCOB->Show();
        $this->POSCOB->Show();
        $this->TELCOB->Show();
        $this->CODSIT->Show();
        $this->LblDATINATIV->Show();
        $this->DATINATIV->Show();
        $this->CODINATIV->Show();
        $this->PGTOELET->Show();
        $this->ESFERA->Show();
        $this->CODALT->Show();
        $this->CODINC->Show();
        $this->GERBLTO->Show();
        $this->RECOBR->Show();
        $this->Button_Insert->Show();
        $this->Button_Update->Show();
        $this->Button_Delete->Show();
        $this->Button_Cancel->Show();
        $this->desativou->Show();
        $this->OBS->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

} //End CADCLI Class @4-FCB6E20C

class clsCADCLIDataSource extends clsDBFaturar {  //CADCLIDataSource Class @4-C5FD973D

//DataSource Variables @4-776AAA9D
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $InsertParameters;
    public $UpdateParameters;
    public $DeleteParameters;
    public $wp;
    public $AllParametersSet;

    public $InsertFields = array();
    public $UpdateFields = array();

    // Datasource fields
    public $lblDATINC;
    public $DATINC;
    public $lblDATALT;
    public $DATALT;
    public $OPUS;
    public $CODCLI;
    public $DESCLI;
    public $DESFAN;
    public $CODTIP;
    public $CGCCPF;
    public $CODUNI;
    public $CODDIS;
    public $CODSET;
    public $GRPATI;
    public $SUBATI;
    public $LOGRAD;
    public $BAIRRO;
    public $CIDADE;
    public $ESTADO;
    public $CODPOS;
    public $TELEFO;
    public $TELFAX;
    public $CONTAT;
    public $EMAIL;
    public $LOGCOB;
    public $BAICOB;
    public $CIDCOB;
    public $ESTCOB;
    public $POSCOB;
    public $TELCOB;
    public $CODSIT;
    public $LblDATINATIV;
    public $DATINATIV;
    public $CODINATIV;
    public $PGTOELET;
    public $ESFERA;
    public $CODALT;
    public $CODINC;
    public $GERBLTO;
    public $RECOBR;
    public $desativou;
    public $OBS;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-DA7E3786
    function clsCADCLIDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Record CADCLI/Error";
        $this->Initialize();
        $this->lblDATINC = new clsField("lblDATINC", ccsDate, $this->DateFormat);
        $this->DATINC = new clsField("DATINC", ccsText, "");
        $this->lblDATALT = new clsField("lblDATALT", ccsDate, $this->DateFormat);
        $this->DATALT = new clsField("DATALT", ccsDate, $this->DateFormat);
        $this->OPUS = new clsField("OPUS", ccsText, "");
        $this->CODCLI = new clsField("CODCLI", ccsText, "");
        $this->DESCLI = new clsField("DESCLI", ccsText, "");
        $this->DESFAN = new clsField("DESFAN", ccsText, "");
        $this->CODTIP = new clsField("CODTIP", ccsText, "");
        $this->CGCCPF = new clsField("CGCCPF", ccsText, "");
        $this->CODUNI = new clsField("CODUNI", ccsText, "");
        $this->CODDIS = new clsField("CODDIS", ccsText, "");
        $this->CODSET = new clsField("CODSET", ccsText, "");
        $this->GRPATI = new clsField("GRPATI", ccsText, "");
        $this->SUBATI = new clsField("SUBATI", ccsText, "");
        $this->LOGRAD = new clsField("LOGRAD", ccsText, "");
        $this->BAIRRO = new clsField("BAIRRO", ccsText, "");
        $this->CIDADE = new clsField("CIDADE", ccsText, "");
        $this->ESTADO = new clsField("ESTADO", ccsText, "");
        $this->CODPOS = new clsField("CODPOS", ccsText, "");
        $this->TELEFO = new clsField("TELEFO", ccsText, "");
        $this->TELFAX = new clsField("TELFAX", ccsText, "");
        $this->CONTAT = new clsField("CONTAT", ccsText, "");
        $this->EMAIL = new clsField("EMAIL", ccsText, "");
        $this->LOGCOB = new clsField("LOGCOB", ccsText, "");
        $this->BAICOB = new clsField("BAICOB", ccsText, "");
        $this->CIDCOB = new clsField("CIDCOB", ccsText, "");
        $this->ESTCOB = new clsField("ESTCOB", ccsText, "");
        $this->POSCOB = new clsField("POSCOB", ccsText, "");
        $this->TELCOB = new clsField("TELCOB", ccsText, "");
        $this->CODSIT = new clsField("CODSIT", ccsText, "");
        $this->LblDATINATIV = new clsField("LblDATINATIV", ccsDate, $this->DateFormat);
        $this->DATINATIV = new clsField("DATINATIV", ccsDate, $this->DateFormat);
        $this->CODINATIV = new clsField("CODINATIV", ccsText, "");
        $this->PGTOELET = new clsField("PGTOELET", ccsText, "");
        $this->ESFERA = new clsField("ESFERA", ccsText, "");
        $this->CODALT = new clsField("CODALT", ccsInteger, "");
        $this->CODINC = new clsField("CODINC", ccsInteger, "");
        $this->GERBLTO = new clsField("GERBLTO", ccsText, "");
        $this->RECOBR = new clsField("RECOBR", ccsText, "");
        $this->desativou = new clsField("desativou", ccsText, "");
        $this->OBS = new clsField("OBS", ccsText, "");

        $this->InsertFields["DATALT"] = array("Name" => "DATALT", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->InsertFields["OPUS"] = array("Name" => "OPUS", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODCLI"] = array("Name" => "CODCLI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DESCLI"] = array("Name" => "DESCLI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DESFAN"] = array("Name" => "DESFAN", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODTIP"] = array("Name" => "CODTIP", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CGCCPF"] = array("Name" => "CGCCPF", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODUNI"] = array("Name" => "CODUNI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODDIS"] = array("Name" => "CODDIS", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODSET"] = array("Name" => "CODSET", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["GRPATI"] = array("Name" => "GRPATI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["SUBATI"] = array("Name" => "SUBATI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["LOGRAD"] = array("Name" => "LOGRAD", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["BAIRRO"] = array("Name" => "BAIRRO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CIDADE"] = array("Name" => "CIDADE", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["ESTADO"] = array("Name" => "ESTADO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODPOS"] = array("Name" => "CODPOS", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["TELEFO"] = array("Name" => "TELEFO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["TELFAX"] = array("Name" => "TELFAX", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CONTAT"] = array("Name" => "CONTAT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["EMAIL"] = array("Name" => "EMAIL", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["LOGCOB"] = array("Name" => "LOGCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["BAICOB"] = array("Name" => "BAICOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CIDCOB"] = array("Name" => "CIDCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["ESTCOB"] = array("Name" => "ESTCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["POSCOB"] = array("Name" => "POSCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["TELCOB"] = array("Name" => "TELCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODSIT"] = array("Name" => "CODSIT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DATINATIV"] = array("Name" => "DATINATIV", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->InsertFields["CODINATIV"] = array("Name" => "CODINATIV", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["PGTOELET"] = array("Name" => "PGTOELET", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["ESFERA"] = array("Name" => "ESFERA", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODALT"] = array("Name" => "CODALT", "Value" => "", "DataType" => ccsInteger, "OmitIfEmpty" => 1);
        $this->InsertFields["CODINC"] = array("Name" => "CODINC", "Value" => "", "DataType" => ccsInteger, "OmitIfEmpty" => 1);
        $this->InsertFields["GERBLTO"] = array("Name" => "GERBLTO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["RECOBR"] = array("Name" => "RECOBR", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["OBS"] = array("Name" => "OBS", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["DATALT"] = array("Name" => "DATALT", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->UpdateFields["OPUS"] = array("Name" => "OPUS", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODCLI"] = array("Name" => "CODCLI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["DESCLI"] = array("Name" => "DESCLI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["DESFAN"] = array("Name" => "DESFAN", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODTIP"] = array("Name" => "CODTIP", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CGCCPF"] = array("Name" => "CGCCPF", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODUNI"] = array("Name" => "CODUNI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODDIS"] = array("Name" => "CODDIS", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODSET"] = array("Name" => "CODSET", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["GRPATI"] = array("Name" => "GRPATI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["SUBATI"] = array("Name" => "SUBATI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["LOGRAD"] = array("Name" => "LOGRAD", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["BAIRRO"] = array("Name" => "BAIRRO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CIDADE"] = array("Name" => "CIDADE", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["ESTADO"] = array("Name" => "ESTADO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODPOS"] = array("Name" => "CODPOS", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["TELEFO"] = array("Name" => "TELEFO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["TELFAX"] = array("Name" => "TELFAX", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CONTAT"] = array("Name" => "CONTAT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["EMAIL"] = array("Name" => "EMAIL", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["LOGCOB"] = array("Name" => "LOGCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["BAICOB"] = array("Name" => "BAICOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CIDCOB"] = array("Name" => "CIDCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["ESTCOB"] = array("Name" => "ESTCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["POSCOB"] = array("Name" => "POSCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["TELCOB"] = array("Name" => "TELCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODSIT"] = array("Name" => "CODSIT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["DATINATIV"] = array("Name" => "DATINATIV", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODINATIV"] = array("Name" => "CODINATIV", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["PGTOELET"] = array("Name" => "PGTOELET", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["ESFERA"] = array("Name" => "ESFERA", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODALT"] = array("Name" => "CODALT", "Value" => "", "DataType" => ccsInteger, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODINC"] = array("Name" => "CODINC", "Value" => "", "DataType" => ccsInteger, "OmitIfEmpty" => 1);
        $this->UpdateFields["GERBLTO"] = array("Name" => "GERBLTO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["RECOBR"] = array("Name" => "RECOBR", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["OBS"] = array("Name" => "OBS", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
    }
//End DataSourceClass_Initialize Event

//Prepare Method @4-48075FFD
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlCODCLI", ccsText, "", "", $this->Parameters["urlCODCLI"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "CODCLI", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @4-9EBC0B74
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->SQL = "SELECT OBS, CODSIT, EMAIL, OPUS, ESFERA, PGTOELET, CODALT, CODINC, TELCOB, POSCOB, ESTCOB, CIDCOB, BAICOB, LOGCOB, CONTAT, TELFAX,\n\n" .
        "TELEFO, CODPOS, ESTADO, CIDADE, BAIRRO, LOGRAD, CGCCPF, CODTIP, SUBATI, GRPATI, CODSET, CODDIS, CODUNI, DESFAN, DESCLI,\n\n" .
        "CODCLI, IDINC, IDALT, DATALT, DATINATIV, DATINC, GERBLTO, CODINATIV, RECOBR \n\n" .
        "FROM CADCLI {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @4-2D034FAA
    function SetValues()
    {
        $this->lblDATINC->SetDBValue(trim($this->f("DATINC")));
        $this->lblDATALT->SetDBValue(trim($this->f("DATALT")));
        $this->DATALT->SetDBValue(trim($this->f("DATALT")));
        $this->OPUS->SetDBValue($this->f("OPUS"));
        $this->CODCLI->SetDBValue($this->f("CODCLI"));
        $this->DESCLI->SetDBValue($this->f("DESCLI"));
        $this->DESFAN->SetDBValue($this->f("DESFAN"));
        $this->CODTIP->SetDBValue($this->f("CODTIP"));
        $this->CGCCPF->SetDBValue($this->f("CGCCPF"));
        $this->CODUNI->SetDBValue($this->f("CODUNI"));
        $this->CODDIS->SetDBValue($this->f("CODDIS"));
        $this->CODSET->SetDBValue($this->f("CODSET"));
        $this->GRPATI->SetDBValue($this->f("GRPATI"));
        $this->SUBATI->SetDBValue($this->f("SUBATI"));
        $this->LOGRAD->SetDBValue($this->f("LOGRAD"));
        $this->BAIRRO->SetDBValue($this->f("BAIRRO"));
        $this->CIDADE->SetDBValue($this->f("CIDADE"));
        $this->ESTADO->SetDBValue($this->f("ESTADO"));
        $this->CODPOS->SetDBValue($this->f("CODPOS"));
        $this->TELEFO->SetDBValue($this->f("TELEFO"));
        $this->TELFAX->SetDBValue($this->f("TELFAX"));
        $this->CONTAT->SetDBValue($this->f("CONTAT"));
        $this->EMAIL->SetDBValue($this->f("EMAIL"));
        $this->LOGCOB->SetDBValue($this->f("LOGCOB"));
        $this->BAICOB->SetDBValue($this->f("BAICOB"));
        $this->CIDCOB->SetDBValue($this->f("CIDCOB"));
        $this->ESTCOB->SetDBValue($this->f("ESTCOB"));
        $this->POSCOB->SetDBValue($this->f("POSCOB"));
        $this->TELCOB->SetDBValue($this->f("TELCOB"));
        $this->CODSIT->SetDBValue($this->f("CODSIT"));
        $this->LblDATINATIV->SetDBValue(trim($this->f("DATINATIV")));
        $this->DATINATIV->SetDBValue(trim($this->f("DATINATIV")));
        $this->CODINATIV->SetDBValue($this->f("CODINATIV"));
        $this->PGTOELET->SetDBValue($this->f("PGTOELET"));
        $this->ESFERA->SetDBValue($this->f("ESFERA"));
        $this->CODALT->SetDBValue(trim($this->f("CODALT")));
        $this->CODINC->SetDBValue(trim($this->f("CODINC")));
        $this->GERBLTO->SetDBValue($this->f("GERBLTO"));
        $this->RECOBR->SetDBValue($this->f("RECOBR"));
        $this->OBS->SetDBValue($this->f("OBS"));
    }
//End SetValues Method

//Insert Method @4-FECA3F1B
    function Insert()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert", $this->Parent);
        $this->InsertFields["DATALT"]["Value"] = $this->DATALT->GetDBValue(true);
        $this->InsertFields["OPUS"]["Value"] = $this->OPUS->GetDBValue(true);
        $this->InsertFields["CODCLI"]["Value"] = $this->CODCLI->GetDBValue(true);
        $this->InsertFields["DESCLI"]["Value"] = $this->DESCLI->GetDBValue(true);
        $this->InsertFields["DESFAN"]["Value"] = $this->DESFAN->GetDBValue(true);
        $this->InsertFields["CODTIP"]["Value"] = $this->CODTIP->GetDBValue(true);
        $this->InsertFields["CGCCPF"]["Value"] = $this->CGCCPF->GetDBValue(true);
        $this->InsertFields["CODUNI"]["Value"] = $this->CODUNI->GetDBValue(true);
        $this->InsertFields["CODDIS"]["Value"] = $this->CODDIS->GetDBValue(true);
        $this->InsertFields["CODSET"]["Value"] = $this->CODSET->GetDBValue(true);
        $this->InsertFields["GRPATI"]["Value"] = $this->GRPATI->GetDBValue(true);
        $this->InsertFields["SUBATI"]["Value"] = $this->SUBATI->GetDBValue(true);
        $this->InsertFields["LOGRAD"]["Value"] = $this->LOGRAD->GetDBValue(true);
        $this->InsertFields["BAIRRO"]["Value"] = $this->BAIRRO->GetDBValue(true);
        $this->InsertFields["CIDADE"]["Value"] = $this->CIDADE->GetDBValue(true);
        $this->InsertFields["ESTADO"]["Value"] = $this->ESTADO->GetDBValue(true);
        $this->InsertFields["CODPOS"]["Value"] = $this->CODPOS->GetDBValue(true);
        $this->InsertFields["TELEFO"]["Value"] = $this->TELEFO->GetDBValue(true);
        $this->InsertFields["TELFAX"]["Value"] = $this->TELFAX->GetDBValue(true);
        $this->InsertFields["CONTAT"]["Value"] = $this->CONTAT->GetDBValue(true);
        $this->InsertFields["EMAIL"]["Value"] = $this->EMAIL->GetDBValue(true);
        $this->InsertFields["LOGCOB"]["Value"] = $this->LOGCOB->GetDBValue(true);
        $this->InsertFields["BAICOB"]["Value"] = $this->BAICOB->GetDBValue(true);
        $this->InsertFields["CIDCOB"]["Value"] = $this->CIDCOB->GetDBValue(true);
        $this->InsertFields["ESTCOB"]["Value"] = $this->ESTCOB->GetDBValue(true);
        $this->InsertFields["POSCOB"]["Value"] = $this->POSCOB->GetDBValue(true);
        $this->InsertFields["TELCOB"]["Value"] = $this->TELCOB->GetDBValue(true);
        $this->InsertFields["CODSIT"]["Value"] = $this->CODSIT->GetDBValue(true);
        $this->InsertFields["DATINATIV"]["Value"] = $this->DATINATIV->GetDBValue(true);
        $this->InsertFields["CODINATIV"]["Value"] = $this->CODINATIV->GetDBValue(true);
        $this->InsertFields["PGTOELET"]["Value"] = $this->PGTOELET->GetDBValue(true);
        $this->InsertFields["ESFERA"]["Value"] = $this->ESFERA->GetDBValue(true);
        $this->InsertFields["CODALT"]["Value"] = $this->CODALT->GetDBValue(true);
        $this->InsertFields["CODINC"]["Value"] = $this->CODINC->GetDBValue(true);
        $this->InsertFields["GERBLTO"]["Value"] = $this->GERBLTO->GetDBValue(true);
        $this->InsertFields["RECOBR"]["Value"] = $this->RECOBR->GetDBValue(true);
        $this->InsertFields["OBS"]["Value"] = $this->OBS->GetDBValue(true);
        $this->SQL = CCBuildInsert("CADCLI", $this->InsertFields, $this);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert", $this->Parent);
        }
    }
//End Insert Method

//Update Method @4-BAF3FCCB
    function Update()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildUpdate", $this->Parent);
        $this->UpdateFields["DATALT"]["Value"] = $this->DATALT->GetDBValue(true);
        $this->UpdateFields["OPUS"]["Value"] = $this->OPUS->GetDBValue(true);
        $this->UpdateFields["CODCLI"]["Value"] = $this->CODCLI->GetDBValue(true);
        $this->UpdateFields["DESCLI"]["Value"] = $this->DESCLI->GetDBValue(true);
        $this->UpdateFields["DESFAN"]["Value"] = $this->DESFAN->GetDBValue(true);
        $this->UpdateFields["CODTIP"]["Value"] = $this->CODTIP->GetDBValue(true);
        $this->UpdateFields["CGCCPF"]["Value"] = $this->CGCCPF->GetDBValue(true);
        $this->UpdateFields["CODUNI"]["Value"] = $this->CODUNI->GetDBValue(true);
        $this->UpdateFields["CODDIS"]["Value"] = $this->CODDIS->GetDBValue(true);
        $this->UpdateFields["CODSET"]["Value"] = $this->CODSET->GetDBValue(true);
        $this->UpdateFields["GRPATI"]["Value"] = $this->GRPATI->GetDBValue(true);
        $this->UpdateFields["SUBATI"]["Value"] = $this->SUBATI->GetDBValue(true);
        $this->UpdateFields["LOGRAD"]["Value"] = $this->LOGRAD->GetDBValue(true);
        $this->UpdateFields["BAIRRO"]["Value"] = $this->BAIRRO->GetDBValue(true);
        $this->UpdateFields["CIDADE"]["Value"] = $this->CIDADE->GetDBValue(true);
        $this->UpdateFields["ESTADO"]["Value"] = $this->ESTADO->GetDBValue(true);
        $this->UpdateFields["CODPOS"]["Value"] = $this->CODPOS->GetDBValue(true);
        $this->UpdateFields["TELEFO"]["Value"] = $this->TELEFO->GetDBValue(true);
        $this->UpdateFields["TELFAX"]["Value"] = $this->TELFAX->GetDBValue(true);
        $this->UpdateFields["CONTAT"]["Value"] = $this->CONTAT->GetDBValue(true);
        $this->UpdateFields["EMAIL"]["Value"] = $this->EMAIL->GetDBValue(true);
        $this->UpdateFields["LOGCOB"]["Value"] = $this->LOGCOB->GetDBValue(true);
        $this->UpdateFields["BAICOB"]["Value"] = $this->BAICOB->GetDBValue(true);
        $this->UpdateFields["CIDCOB"]["Value"] = $this->CIDCOB->GetDBValue(true);
        $this->UpdateFields["ESTCOB"]["Value"] = $this->ESTCOB->GetDBValue(true);
        $this->UpdateFields["POSCOB"]["Value"] = $this->POSCOB->GetDBValue(true);
        $this->UpdateFields["TELCOB"]["Value"] = $this->TELCOB->GetDBValue(true);
        $this->UpdateFields["CODSIT"]["Value"] = $this->CODSIT->GetDBValue(true);
        $this->UpdateFields["DATINATIV"]["Value"] = $this->DATINATIV->GetDBValue(true);
        $this->UpdateFields["CODINATIV"]["Value"] = $this->CODINATIV->GetDBValue(true);
        $this->UpdateFields["PGTOELET"]["Value"] = $this->PGTOELET->GetDBValue(true);
        $this->UpdateFields["ESFERA"]["Value"] = $this->ESFERA->GetDBValue(true);
        $this->UpdateFields["CODALT"]["Value"] = $this->CODALT->GetDBValue(true);
        $this->UpdateFields["CODINC"]["Value"] = $this->CODINC->GetDBValue(true);
        $this->UpdateFields["GERBLTO"]["Value"] = $this->GERBLTO->GetDBValue(true);
        $this->UpdateFields["RECOBR"]["Value"] = $this->RECOBR->GetDBValue(true);
        $this->UpdateFields["OBS"]["Value"] = $this->OBS->GetDBValue(true);
        $this->SQL = CCBuildUpdate("CADCLI", $this->UpdateFields, $this);
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteUpdate", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteUpdate", $this->Parent);
        }
    }
//End Update Method

//Delete Method @4-45CC94DC
    function Delete()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildDelete", $this->Parent);
        $this->SQL = "DELETE FROM CADCLI";
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteDelete", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteDelete", $this->Parent);
        }
    }
//End Delete Method

} //End CADCLIDataSource Class @4-FCB6E20C

class clsGridMOVSER_SUBSER { //MOVSER_SUBSER class @161-B36D480E

//Variables @161-8D667888

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_DESSUB;
    public $Sorter_DATINI;
    public $Sorter_DATFIM;
    public $Sorter_QTDMED;
//End Variables

//Class_Initialize Event @161-FEFA140B
    function clsGridMOVSER_SUBSER($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "MOVSER_SUBSER";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid MOVSER_SUBSER";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clsMOVSER_SUBSERDataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 10;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("MOVSER_SUBSEROrder", "");
        $this->SorterDirection = CCGetParam("MOVSER_SUBSERDir", "");

        $this->DESSUB = new clsControl(ccsLabel, "DESSUB", "DESSUB", ccsText, "", CCGetRequestParam("DESSUB", ccsGet, NULL), $this);
        $this->DATINI = new clsControl(ccsLabel, "DATINI", "DATINI", ccsDate, $DefaultDateFormat, CCGetRequestParam("DATINI", ccsGet, NULL), $this);
        $this->DATFIM = new clsControl(ccsLabel, "DATFIM", "DATFIM", ccsDate, $DefaultDateFormat, CCGetRequestParam("DATFIM", ccsGet, NULL), $this);
        $this->QTDMED = new clsControl(ccsLabel, "QTDMED", "QTDMED", ccsFloat, "", CCGetRequestParam("QTDMED", ccsGet, NULL), $this);
        $this->Sorter_DESSUB = new clsSorter($this->ComponentName, "Sorter_DESSUB", $FileName, $this);
        $this->Sorter_DATINI = new clsSorter($this->ComponentName, "Sorter_DATINI", $FileName, $this);
        $this->Sorter_DATFIM = new clsSorter($this->ComponentName, "Sorter_DATFIM", $FileName, $this);
        $this->Sorter_QTDMED = new clsSorter($this->ComponentName, "Sorter_QTDMED", $FileName, $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpCentered, $this);
    }
//End Class_Initialize Event

//Initialize Method @161-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @161-58243D6F
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urlCODCLI"] = CCGetFromGet("CODCLI", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["DESSUB"] = $this->DESSUB->Visible;
            $this->ControlsVisible["DATINI"] = $this->DATINI->Visible;
            $this->ControlsVisible["DATFIM"] = $this->DATFIM->Visible;
            $this->ControlsVisible["QTDMED"] = $this->QTDMED->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->DESSUB->SetValue($this->DataSource->DESSUB->GetValue());
                $this->DATINI->SetValue($this->DataSource->DATINI->GetValue());
                $this->DATFIM->SetValue($this->DataSource->DATFIM->GetValue());
                $this->QTDMED->SetValue($this->DataSource->QTDMED->GetValue());
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->DESSUB->Show();
                $this->DATINI->Show();
                $this->DATFIM->Show();
                $this->QTDMED->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        $this->Sorter_DESSUB->Show();
        $this->Sorter_DATINI->Show();
        $this->Sorter_DATFIM->Show();
        $this->Sorter_QTDMED->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @161-6CAD7AD2
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->DESSUB->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DATINI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DATFIM->Errors->ToString());
        $errors = ComposeStrings($errors, $this->QTDMED->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End MOVSER_SUBSER Class @161-FCB6E20C

class clsMOVSER_SUBSERDataSource extends clsDBFaturar {  //MOVSER_SUBSERDataSource Class @161-BDFA6AEF

//DataSource Variables @161-62D5DA60
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $DESSUB;
    public $DATINI;
    public $DATFIM;
    public $QTDMED;
//End DataSource Variables

//DataSourceClass_Initialize Event @161-B157197B
    function clsMOVSER_SUBSERDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid MOVSER_SUBSER";
        $this->Initialize();
        $this->DESSUB = new clsField("DESSUB", ccsText, "");
        $this->DATINI = new clsField("DATINI", ccsDate, $this->DateFormat);
        $this->DATFIM = new clsField("DATFIM", ccsDate, $this->DateFormat);
        $this->QTDMED = new clsField("QTDMED", ccsFloat, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @161-AB479924
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_DESSUB" => array("DESSUB", ""), 
            "Sorter_DATINI" => array("DATINI", ""), 
            "Sorter_DATFIM" => array("DATFIM", ""), 
            "Sorter_QTDMED" => array("QTDMED", "")));
    }
//End SetOrder Method

//Prepare Method @161-275FDF60
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlCODCLI", ccsText, "", "", $this->Parameters["urlCODCLI"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "MOVSER.CODCLI", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->Where = 
             $this->wp->Criterion[1];
        $this->Where = $this->wp->opAND(false, "( (MOVSER.SUBSER = SUBSER.SUBSER) )", $this->Where);
    }
//End Prepare Method

//Open Method @161-0698F978
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM MOVSER,\n\n" .
        "SUBSER";
        $this->SQL = "SELECT * \n\n" .
        "FROM MOVSER,\n\n" .
        "SUBSER {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @161-6F45F385
    function SetValues()
    {
        $this->DESSUB->SetDBValue($this->f("DESSUB"));
        $this->DATINI->SetDBValue(trim($this->f("DATINI")));
        $this->DATFIM->SetDBValue(trim($this->f("DATFIM")));
        $this->QTDMED->SetDBValue(trim($this->f("QTDMED")));
    }
//End SetValues Method

} //End MOVSER_SUBSERDataSource Class @161-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-C4BFE166
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "ManutCadCadCli1.html";
$BlockToParse = "main";
$TemplateEncoding = "ISO-8859-1";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-7FACF37D
CCSecurityRedirect("1;3", "");
//End Authenticate User

//Include events file @1-98F57781
include("./ManutCadCadCli1_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-D93E5CEB
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$CADCLI = new clsRecordCADCLI("", $MainPage);
$MOVSER_SUBSER = new clsGridMOVSER_SUBSER("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->CADCLI = & $CADCLI;
$MainPage->MOVSER_SUBSER = & $MOVSER_SUBSER;
$MainPage->rodape = & $rodape;
$CADCLI->Initialize();
$MOVSER_SUBSER->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "iso-8859-1";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-A8C4B9DA
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "ISO-8859-1");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-AF1ED153
$cabec->Operations();
$CADCLI->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-4199FAE4
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($CADCLI);
    unset($MOVSER_SUBSER);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-50B64DF8
$cabec->Show();
$CADCLI->Show();
$MOVSER_SUBSER->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-0EECF5FA
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($CADCLI);
unset($MOVSER_SUBSER);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
