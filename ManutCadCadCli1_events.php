<?php
$cache_OBS="";

	function contardigitos($telefone)
	  {
	   $digitos=0;
       for($i=0;$i<strlen($telefone);$i++)
	     {
          if($telefone[$i]>='0' && $telefone[$i]<='9')
		    {
             $digitos++;
			}
		 }
       return($digitos);
	  }

//BindEvents Method @1-B3AA7FF6
function BindEvents()
{
    global $CADCLI;
    global $CCSEvents;
    $CADCLI->CODCLI->CCSEvents["BeforeShow"] = "CADCLI_CODCLI_BeforeShow";
    $CADCLI->DESCLI->CCSEvents["OnValidate"] = "CADCLI_DESCLI_OnValidate";
    $CADCLI->DESFAN->CCSEvents["OnValidate"] = "CADCLI_DESFAN_OnValidate";
    $CADCLI->LOGRAD->CCSEvents["OnValidate"] = "CADCLI_LOGRAD_OnValidate";
    $CADCLI->BAIRRO->CCSEvents["OnValidate"] = "CADCLI_BAIRRO_OnValidate";
    $CADCLI->CIDADE->CCSEvents["OnValidate"] = "CADCLI_CIDADE_OnValidate";
    $CADCLI->CONTAT->CCSEvents["OnValidate"] = "CADCLI_CONTAT_OnValidate";
    $CADCLI->LOGCOB->CCSEvents["OnValidate"] = "CADCLI_LOGCOB_OnValidate";
    $CADCLI->BAICOB->CCSEvents["OnValidate"] = "CADCLI_BAICOB_OnValidate";
    $CADCLI->CIDCOB->CCSEvents["OnValidate"] = "CADCLI_CIDCOB_OnValidate";
    $CADCLI->Button_Insert->CCSEvents["OnClick"] = "CADCLI_Button_Insert_OnClick";
    $CADCLI->Button_Update->CCSEvents["OnClick"] = "CADCLI_Button_Update_OnClick";
    $CADCLI->OBS->CCSEvents["BeforeShow"] = "CADCLI_OBS_BeforeShow";
    $CADCLI->CCSEvents["BeforeInsert"] = "CADCLI_BeforeInsert";
    $CADCLI->CCSEvents["BeforeUpdate"] = "CADCLI_BeforeUpdate";
    $CADCLI->CCSEvents["BeforeShow"] = "CADCLI_BeforeShow";
    $CADCLI->CCSEvents["AfterUpdate"] = "CADCLI_AfterUpdate";
    $CADCLI->CCSEvents["OnValidate"] = "CADCLI_OnValidate";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//DEL  // -------------------------
//DEL      print_r($Container);
//DEL  //    print "\n<br> DATINC='".$DATINC->GetValue()."'";
//DEL  //	print "\n<br> DATALT='".$DATALT->GetValue()."'";
//DEL  // -------------------------

//CADCLI_CODCLI_BeforeShow @11-1D29B7BF
function CADCLI_CODCLI_BeforeShow(& $sender)
{
    $CADCLI_CODCLI_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_CODCLI_BeforeShow

//DLookup @44-80CBD978
    global $DBfaturar;
    $Page = CCGetParentPage($sender);
	if ($CADCLI->Button_Insert->Visible)
	{
    	$ccs_result = CCDLookUp("max(to_number(codcli))+1", "cadcli", "", $Page->Connections["Faturar"]);
	   	if (is_null($ccs_result) || $ccs_result == 0)
	   	{
	   		$ccs_result = "1";//Para o caso da tabela estar vazia;
	   	}
    	$Container->CODCLI->SetValue(str_pad($ccs_result,06,"0", STR_PAD_LEFT));
	}
//End DLookup

//Close CADCLI_CODCLI_BeforeShow @11-C0811F6D
    return $CADCLI_CODCLI_BeforeShow;
}
//End Close CADCLI_CODCLI_BeforeShow

//CADCLI_DESCLI_OnValidate @12-42E7E030
function CADCLI_DESCLI_OnValidate(& $sender)
{
    $CADCLI_DESCLI_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_DESCLI_OnValidate
   
//Custom Code @80-2A29BDB7
// -------------------------
   if ( !caracterInvado($Component->GetValue()) )
   {
      $CADCLI->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
   }
// -------------------------
//End Custom Code

//Close CADCLI_DESCLI_OnValidate @12-9002D016
    return $CADCLI_DESCLI_OnValidate;
}
//End Close CADCLI_DESCLI_OnValidate
function caracterInvado($mDescri)
{
   $Result = true;
   ///$mDescri = strtolower($mDescri);
   //$mArqMov = fopen("c:\sistfat\fatura4.txt","w");
   //fwrite($mArqMov,"Inicio\r\n");
   ///$caracterNao = str_split("'!�@�#�$�%���&*()_-+=�/?�{[�]}�~^,.;:<>�`����������������������".chr(34));
   if (!empty($mDescri))
   {
      $caracterNao = str_split($mDescri);
      //fwrite($mArqMov,"M�ximo = \r\n");
      for($x=0;$x<=(count($caracterNao)-1);$x++)
      {
         //if ( !(strpos($mDescri, $caracterNao[$x])===false) )
	     if ( !((ord($caracterNao[$x]) > 34) && (ord($caracterNao[$x]) < 39)) &&  
              !((ord($caracterNao[$x]) > 39) && (ord($caracterNao[$x]) < 127)) &&
              (ord($caracterNao[$x]) != 32) &&
              (ord($caracterNao[$x]) != 33)
		    )
	     {
            //fwrite($mArqMov,"Caractere encontrado = $caracterNao[$x] \r\n");
		    //fwrite($mArqMov,"Encontrado em $mDescri \r\n");
		    $Result = false;
		    break;
	     }
	     //else
	     //{
         //   fwrite($mArqMov,"Caractere n�o encontrado = $caracterNao[$x] \r\n");
	     //}
      }
      //fclose($mArqMov);
   }
   return $Result;
}
//CADCLI_DESFAN_OnValidate @13-947AA45D
function CADCLI_DESFAN_OnValidate(& $sender)
{
    $CADCLI_DESFAN_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_DESFAN_OnValidate

//Custom Code @81-2A29BDB7
// -------------------------
   if ( !caracterInvado($Component->GetValue()) )
   {
      $CADCLI->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
   }
// -------------------------
//End Custom Code

//Close CADCLI_DESFAN_OnValidate @13-7398D5CC
    return $CADCLI_DESFAN_OnValidate;
}
//End Close CADCLI_DESFAN_OnValidate

//CADCLI_LOGRAD_OnValidate @21-163BC547
function CADCLI_LOGRAD_OnValidate(& $sender)
{
    $CADCLI_LOGRAD_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_LOGRAD_OnValidate

//Custom Code @82-2A29BDB7
// -------------------------
   if ( !caracterInvado($Component->GetValue()) )
   {
      $CADCLI->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
   }
// -------------------------
//End Custom Code

//Close CADCLI_LOGRAD_OnValidate @21-ECC41BF6
    return $CADCLI_LOGRAD_OnValidate;
}
//End Close CADCLI_LOGRAD_OnValidate

//CADCLI_BAIRRO_OnValidate @22-6C8800AA
function CADCLI_BAIRRO_OnValidate(& $sender)
{
    $CADCLI_BAIRRO_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_BAIRRO_OnValidate

//Custom Code @83-2A29BDB7
// -------------------------
   if ( !caracterInvado($Component->GetValue()) )
   {
      $CADCLI->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
   }
// -------------------------
//End Custom Code

//Close CADCLI_BAIRRO_OnValidate @22-3A9729A4
    return $CADCLI_BAIRRO_OnValidate;
}
//End Close CADCLI_BAIRRO_OnValidate

//CADCLI_CIDADE_OnValidate @23-49B6321B
function CADCLI_CIDADE_OnValidate(& $sender)
{
    $CADCLI_CIDADE_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_CIDADE_OnValidate

//Custom Code @84-2A29BDB7
// -------------------------
   if ( !caracterInvado($Component->GetValue()) )
   {
      $CADCLI->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
   }
// -------------------------
//End Custom Code

//Close CADCLI_CIDADE_OnValidate @23-93DABFA8
    return $CADCLI_CIDADE_OnValidate;
}
//End Close CADCLI_CIDADE_OnValidate

//CADCLI_CONTAT_OnValidate @28-67092108
function CADCLI_CONTAT_OnValidate(& $sender)
{
    $CADCLI_CONTAT_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_CONTAT_OnValidate

//Custom Code @85-2A29BDB7
// -------------------------
   if ( !caracterInvado($Component->GetValue()) )
   {
      $CADCLI->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
   }
// -------------------------
//End Custom Code

//Close CADCLI_CONTAT_OnValidate @28-FBCE5C6D
    return $CADCLI_CONTAT_OnValidate;
}
//End Close CADCLI_CONTAT_OnValidate

//CADCLI_LOGCOB_OnValidate @29-CFF3D357
function CADCLI_LOGCOB_OnValidate(& $sender)
{
    $CADCLI_LOGCOB_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_LOGCOB_OnValidate

//Custom Code @86-2A29BDB7
// -------------------------
   if ( !caracterInvado($Component->GetValue()) )
   {
      $CADCLI->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
   }
// -------------------------
//End Custom Code

//Close CADCLI_LOGCOB_OnValidate @29-B13AE73A
    return $CADCLI_LOGCOB_OnValidate;
}
//End Close CADCLI_LOGCOB_OnValidate

//CADCLI_BAICOB_OnValidate @30-73C5D033
function CADCLI_BAICOB_OnValidate(& $sender)
{
    $CADCLI_BAICOB_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_BAICOB_OnValidate

//Custom Code @87-2A29BDB7
// -------------------------
   if ( !caracterInvado($Component->GetValue()) )
   {
      $CADCLI->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
   }
// -------------------------
//End Custom Code

//Close CADCLI_BAICOB_OnValidate @30-7D4E1BB3
    return $CADCLI_BAICOB_OnValidate;
}
//End Close CADCLI_BAICOB_OnValidate

//CADCLI_CIDCOB_OnValidate @31-1B2C1B9C
function CADCLI_CIDCOB_OnValidate(& $sender)
{
    $CADCLI_CIDCOB_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_CIDCOB_OnValidate

//Custom Code @88-2A29BDB7
// -------------------------
   if ( !caracterInvado($Component->GetValue()) )
   {
      $CADCLI->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
   }
// -------------------------
//End Custom Code

//Close CADCLI_CIDCOB_OnValidate @31-AF4396F5
    return $CADCLI_CIDCOB_OnValidate;
}
//End Close CADCLI_CIDCOB_OnValidate

//DEL  // -------------------------
//DEL      if($CADCLI->CODSIT->Value!='I')
//DEL  	  {
//DEL  
//DEL         $CADCLI->DATINATIV->Text='';
//DEL  	  }
//DEL  // -------------------------

//CADCLI_Button_Insert_OnClick @5-185C3BDA
function CADCLI_Button_Insert_OnClick(& $sender)
{
    $CADCLI_Button_Insert_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_Button_Insert_OnClick

//Declare Variable @74-EBCAD0EF
    global $mCGCCPF;
    $mCGCCPF = $CADCLI->CGCCPF->Value;
//End Declare Variable
/*
//DLookup @73-7B77A690
    global $DBFaturar;
    global $mCGC_CPF;
    $Page = CCGetParentPage($sender);
    $ccs_result = CCDLookUp("CGCCPF", "CADCLI", "CGCCPF='$mCGCCPF'", $Page->Connections["Faturar"]);
    $mCGC_CPF = $ccs_result;
//End DLookup
	if ($mCGC_CPF != "")
	{
		$CADCLI->Errors->addError("Cliente com o CGCCPF $mCGC_CPF j� cadastrado");
		$CADCLI_Button_Insert_OnClick = false;
	}*/
	$mCEP = $CADCLI->POSCOB->GetValue();
	$mCodPos = $CADCLI->CODPOS->GetValue();
	$mTeleCob = $CADCLI->TELCOB->GetValue();
	$mTeleFo = $CADCLI->TELEFO->GetValue();
	$mTeleFax = $CADCLI->TELFAX->GetValue();

	if (strlen($mCEP) != 9 || strlen($mCodPos) != 9)
	{
		$CADCLI->Errors->addError("C�digo postal inv�lido.");
		$CADCLI_Button_Insert_OnClick = false;
	}
    
    if(contardigitos($mTeleCob) < 8 && contardigitos($mTeleCob) != 0)
		{
		 $CADCLI->Errors->addError("N�mero de telefone de cobran�a inv�lido.");
		 $CADCLI_Button_Insert_OnClick = false;
		}
    
	if( contardigitos($mTeleFo) < 8 && contardigitos($mTeleFo) != 0 )
		{
		 $CADCLI->Errors->addError("N�mero de telefone inv�lido.");
		 $CADCLI_Button_Insert_OnClick = false;
		}
    
	if(contardigitos($mTeleFax) < 8 && contardigitos($mTeleFax) != 0)
		{
		 $CADCLI->Errors->addError("N�mero de Fax inv�lido.");
		 $CADCLI_Button_Insert_OnClick = false;
		}


//Close CADCLI_Button_Insert_OnClick @5-31A1314F
    return $CADCLI_Button_Insert_OnClick;
}
//End Close CADCLI_Button_Insert_OnClick

//CADCLI_Button_Update_OnClick @6-AC00DFBB
function CADCLI_Button_Update_OnClick(& $sender)
{
    $CADCLI_Button_Update_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_Button_Update_OnClick

//Custom Code @77-2A29BDB7
// -------------------------
	$mCEP = $CADCLI->POSCOB->GetValue();
	$mCodPos = $CADCLI->CODPOS->GetValue();
	$mTeleCob = $CADCLI->TELCOB->GetValue();
	$mTeleFo = $CADCLI->TELEFO->GetValue();
	$mTeleFax = $CADCLI->TELFAX->GetValue(); 
	if (strlen($mCEP) != 9 || strlen($mCodPos) != 9)
	{
		$CADCLI->Errors->addError("C�digo postal inv�lido.");
		$CADCLI_Button_Insert_OnClick = false;
	}


    
    if(contardigitos($mTeleCob) < 8 && contardigitos($mTeleCob) != 0 )
		{
		 $CADCLI->Errors->addError("N�mero de telefone de cobran�a inv�lido.");
		 $CADCLI_Button_Insert_OnClick = false;
		}

    
	if( contardigitos($mTeleFo) < 8 && contardigitos($mTeleFo) != 0)
		{
		 $CADCLI->Errors->addError("N�mero de telefone inv�lido.");
		 $CADCLI_Button_Insert_OnClick = false;
		}

    
	if(contardigitos($mTeleFax) < 8 && contardigitos($mTeleFax) != 0)
		{
		 $CADCLI->Errors->addError("N�mero de Fax inv�lido.");
		 $CADCLI_Button_Insert_OnClick = false;
		}



//$CADCLI_Button_Insert_OnClick = false;


// -------------------------
//End Custom Code

//Close CADCLI_Button_Update_OnClick @6-9A3619F2
    return $CADCLI_Button_Update_OnClick;
}
//End Close CADCLI_Button_Update_OnClick

//CADCLI_OBS_BeforeShow @103-DB4FC17D
function CADCLI_OBS_BeforeShow(& $sender)
{
    $CADCLI_OBS_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_OBS_BeforeShow

//Custom Code @144-2A29BDB7
// -------------------------
    //$CADCLI->OBS->Value=str_replace("#&aspassimp&#","'",$CADCLI->OBS->Value);
	$CADCLI->OBS->SetText(html_entity_decode(str_replace("#&aspassimp&#","'",$CADCLI->OBS->Value)));
// -------------------------
//End Custom Code

//Close CADCLI_OBS_BeforeShow @103-D3A29785
    return $CADCLI_OBS_BeforeShow;
}
//End Close CADCLI_OBS_BeforeShow

//CADCLI_BeforeInsert @4-C99F2E06
function CADCLI_BeforeInsert(& $sender)
{
    $CADCLI_BeforeInsert = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_BeforeInsert

//Custom Code @66-2A29BDB7
// -------------------------
    // Write your own code here.
	$CADCLI->CODINC->Value = CCGetSession("IDUsuario");

    $datasist=substr(CCDLookUp("SYSDATE","dual","",$CADCLI->ds),0,11);
	
	$datasist=explode('/',$datasist);
    $CADCLI->DATINC->Value[1]=$datasist[2];
	$CADCLI->DATINC->Value[2]=$datasist[1];
	$CADCLI->DATINC->Value[3]=$datasist[0];

	if( strlen($CADCLI->DATINC->Value[1])<4) 
	  {
	   $CADCLI->DATINC->Value[1]="20".$CADCLI->DATINC->Value[1];
	  }



    if($CADCLI->CODSIT->Value=='I')
	  {
	    
	    $CADCLI->DATINATIV->Value[1]=$datasist[2];
		$CADCLI->DATINATIV->Value[2]=$datasist[1];
		$CADCLI->DATINATIV->Value[3]=$datasist[0];
	    if( strlen($CADCLI->DATINATIV->Value[1])<4) 
	       {
	         $CADCLI->DATINATIV->Value[1]="20".$CADCLI->DATINATIV->Value[1];
	       }

	  }


    
// -------------------------
//End Custom Code

//Close CADCLI_BeforeInsert @4-7C661CFB
    return $CADCLI_BeforeInsert;
}
//End Close CADCLI_BeforeInsert

//CADCLI_BeforeUpdate @4-3913140A
function CADCLI_BeforeUpdate(& $sender)
{
    $CADCLI_BeforeUpdate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_BeforeUpdate

//Custom Code @67-2A29BDB7
// -------------------------
    // Write your own code here.
	global $cache_OBS;
    $cache_OBS="";
    $CADCLI->OBS->Value=str_replace("'","#&aspassimp&#",htmlentities($CADCLI->OBS->Value));
    

	$CADCLI->CODALT->Value = CCGetSession("IDUsuario");
	
    $datasist=substr(CCDLookUp("SYSDATE","dual","",$CADCLI->ds),0,11);
	
	$datasist=explode('/',$datasist);

    $CADCLI->DATALT->Value[1]=$datasist[2];
	$CADCLI->DATALT->Value[2]=$datasist[1];
	$CADCLI->DATALT->Value[3]=$datasist[0];
	if( strlen($CADCLI->DATALT->Value[1])<4) 
	  {
	   $CADCLI->DATALT->Value[1]="20".$CADCLI->DATALT->Value[1];
	  }

	


	if($CADCLI->desativou->Value=='S')
	  {
       if($CADCLI->CODSIT->Value=='I')
	    {
	      $CADCLI->DATINATIV->Value[1]=$datasist[2];
	      $CADCLI->DATINATIV->Value[2]=$datasist[1];
		  $CADCLI->DATINATIV->Value[3]=$datasist[0];
		
		  if( strlen($CADCLI->DATINATIV->Value[1])<4) 
		    {
		     $CADCLI->DATINATIV->Value[1]="20".$CADCLI->DATINATIV->Value[1];
		    }
	    }

      }
	

    
    
	if ( strlen($CADCLI->OBS->Value) >3500 )
	  {
       $cache_OBS=$CADCLI->OBS->Value;
	   $CADCLI->OBS->Value=substr($CADCLI->OBS->Value,0,3500);
       
	  }
// -------------------------
//End Custom Code

//Close CADCLI_BeforeUpdate @4-B34FDD74
    return $CADCLI_BeforeUpdate;
}
//End Close CADCLI_BeforeUpdate

//CADCLI_BeforeShow @4-BB9E428D
function CADCLI_BeforeShow(& $sender)
{
    $CADCLI_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_BeforeShow

//Declare Variable @70-3481E098
    global $mCODCLI;
    $mCODCLI = $CADCLI->CODCLI->Value;
//End Declare Variable

    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(24,26);
    
    if( verificaacesso($perfil,$permissao_requerida)==false )
	  {
	   $CADCLI->Button_Insert->Visible=false;
       $CADCLI->Button_Update->Visible=false;
	  }

	if ($CADCLI->Button_Delete->Visible)
	{
	    if( verificaacesso($perfil,$permissao_requerida)==false )
		  {
	       $CADCLI->Button_Delete->Visible=false;
		  }


//DLookup @68-C3C74616
    global $DBFaturar;
    global $mFatura;
    $Page = CCGetParentPage($sender);
    $ccs_result = CCDLookUp("CODCLI", "CADFAT", "CODCLI='$mCODCLI'", $Page->Connections["Faturar"]);
    $mFatura = $ccs_result;
//End DLookup

//Custom Code @91-2A29BDB7
// -------------------------




// -------------------------
//End Custom Code
		if ($mFatura !="")
		{
			$CADCLI->Button_Delete->Visible = false;
		}
	}
	else
	{
    	global $DBfaturar;
    	global $mFatura;
    	$Page = CCGetParentPage($sender);
    	$ccs_result = CCDLookUp("max(to_number(CODCLI)+1)", "CADFAT", "CODCLI='$mCODCLI'", $Page->Connections["Faturar"]);
		$CADCLI->CODCLI->SetValue($ccs_result);
	}

	//formata telefones antigos de acordo com a nova m�scara (10/2015)
	mascaraTelefone($CADCLI->TELEFO);
	mascaraTelefone($CADCLI->TELCOB);
	mascaraTelefone($CADCLI->TELFAX);

//Close CADCLI_BeforeShow @4-369ED32C
    return $CADCLI_BeforeShow;
}
//End Close CADCLI_BeforeShow

//CADCLI_AfterUpdate @4-DAFF8301
function CADCLI_AfterUpdate(& $sender)
{
    $CADCLI_AfterUpdate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_AfterUpdate

//Custom Code @143-2A29BDB7
// -------------------------
   global $cache_OBS;
   
   for($i=3500;$i<strlen($cache_OBS);$i+=3500)
     {
      $CADCLI->ds->query("update CADCLI SET OBS=OBS||'".substr($cache_OBS,$i,3500)."' where CODCLI='".$CADCLI->CODCLI->Value."'");

     }

// -------------------------
//End Custom Code

//Close CADCLI_AfterUpdate @4-846A0970
    return $CADCLI_AfterUpdate;
}
//End Close CADCLI_AfterUpdate

//CADCLI_OnValidate @4-CB6D786A
function CADCLI_OnValidate(& $sender)
{
    $CADCLI_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI; //Compatibility
//End CADCLI_OnValidate

//Custom Code @192-2A29BDB7
// -------------------------
    validaTelefone($CADCLI->TELEFO);
	validaTelefone($CADCLI->TELFAX);
	validaTelefone($CADCLI->TELCOB);
// -------------------------
//End Custom Code

//Close CADCLI_OnValidate @4-0965B7A5
    return $CADCLI_OnValidate;
}
//End Close CADCLI_OnValidate

//Page_BeforeShow @1-FBAC803F
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutCadCadCli1; //Compatibility
//End Page_BeforeShow

//Custom Code @90-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(24,25,26);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
