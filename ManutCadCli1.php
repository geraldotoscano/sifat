<?php
//Include Common Files @1-AB1C82B5
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "ManutCadCli1.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordCADCLI { //CADCLI Class @4-3C33833D

//Variables @4-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @4-E9FA2E91
    function clsRecordCADCLI($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record CADCLI/Error";
        $this->DataSource = new clsCADCLIDataSource($this);
        $this->ds = & $this->DataSource;
        $this->InsertAllowed = true;
        $this->UpdateAllowed = true;
        $this->DeleteAllowed = true;
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "CADCLI";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->CODCLI = new clsControl(ccsTextBox, "CODCLI", "CODCLI", ccsText, "", CCGetRequestParam("CODCLI", $Method, NULL), $this);
            $this->DESCLI = new clsControl(ccsTextBox, "DESCLI", "DESCLI", ccsText, "", CCGetRequestParam("DESCLI", $Method, NULL), $this);
            $this->DESFAN = new clsControl(ccsTextBox, "DESFAN", "DESFAN", ccsText, "", CCGetRequestParam("DESFAN", $Method, NULL), $this);
            $this->CODUNI = new clsControl(ccsListBox, "CODUNI", "CODUNI", ccsText, "", CCGetRequestParam("CODUNI", $Method, NULL), $this);
            $this->CODUNI->DSType = dsTable;
            $this->CODUNI->DataSource = new clsDBFaturar();
            $this->CODUNI->ds = & $this->CODUNI->DataSource;
            $this->CODUNI->DataSource->SQL = "SELECT * \n" .
"FROM TABUNI {SQL_Where} {SQL_OrderBy}";
            list($this->CODUNI->BoundColumn, $this->CODUNI->TextColumn, $this->CODUNI->DBFormat) = array("CODUNI", "DESUNI", "");
            $this->CODDIS = new clsControl(ccsListBox, "CODDIS", "CODDIS", ccsText, "", CCGetRequestParam("CODDIS", $Method, NULL), $this);
            $this->CODDIS->DSType = dsTable;
            $this->CODDIS->DataSource = new clsDBFaturar();
            $this->CODDIS->ds = & $this->CODDIS->DataSource;
            $this->CODDIS->DataSource->SQL = "SELECT * \n" .
"FROM TABDIS {SQL_Where} {SQL_OrderBy}";
            list($this->CODDIS->BoundColumn, $this->CODDIS->TextColumn, $this->CODDIS->DBFormat) = array("CODDIS", "DESDIS", "");
            $this->CODSET = new clsControl(ccsTextBox, "CODSET", "CODSET", ccsText, "", CCGetRequestParam("CODSET", $Method, NULL), $this);
            $this->GRPATI = new clsControl(ccsListBox, "GRPATI", "GRPATI", ccsText, "", CCGetRequestParam("GRPATI", $Method, NULL), $this);
            $this->GRPATI->DSType = dsTable;
            $this->GRPATI->DataSource = new clsDBFaturar();
            $this->GRPATI->ds = & $this->GRPATI->DataSource;
            $this->GRPATI->DataSource->SQL = "SELECT * \n" .
"FROM GRPATI {SQL_Where} {SQL_OrderBy}";
            list($this->GRPATI->BoundColumn, $this->GRPATI->TextColumn, $this->GRPATI->DBFormat) = array("GRPATI", "DESATI", "");
            $this->SUBATI = new clsControl(ccsListBox, "SUBATI", "SUBATI", ccsText, "", CCGetRequestParam("SUBATI", $Method, NULL), $this);
            $this->SUBATI->DSType = dsTable;
            $this->SUBATI->DataSource = new clsDBFaturar();
            $this->SUBATI->ds = & $this->SUBATI->DataSource;
            $this->SUBATI->DataSource->SQL = "SELECT * \n" .
"FROM SUBATI {SQL_Where} {SQL_OrderBy}";
            list($this->SUBATI->BoundColumn, $this->SUBATI->TextColumn, $this->SUBATI->DBFormat) = array("SUBATI", "DESSUB", "");
            $this->RadioButton1 = new clsControl(ccsRadioButton, "RadioButton1", "RadioButton1", ccsText, "", CCGetRequestParam("RadioButton1", $Method, NULL), $this);
            $this->RadioButton1->DSType = dsTable;
            $this->RadioButton1->DataSource = new clsDBFaturar();
            $this->RadioButton1->ds = & $this->RadioButton1->DataSource;
            $this->RadioButton1->DataSource->SQL = "SELECT * \n" .
"FROM cadtipo {SQL_Where} {SQL_OrderBy}";
            list($this->RadioButton1->BoundColumn, $this->RadioButton1->TextColumn, $this->RadioButton1->DBFormat) = array("CODTIP", "DECRICAO", "");
            $this->RadioButton1->HTML = true;
            $this->CGCCPF = new clsControl(ccsTextBox, "CGCCPF", "CGCCPF", ccsText, "", CCGetRequestParam("CGCCPF", $Method, NULL), $this);
            $this->LOGRAD = new clsControl(ccsTextBox, "LOGRAD", "LOGRAD", ccsText, "", CCGetRequestParam("LOGRAD", $Method, NULL), $this);
            $this->BAIRRO = new clsControl(ccsTextBox, "BAIRRO", "BAIRRO", ccsText, "", CCGetRequestParam("BAIRRO", $Method, NULL), $this);
            $this->CIDADE = new clsControl(ccsTextBox, "CIDADE", "CIDADE", ccsText, "", CCGetRequestParam("CIDADE", $Method, NULL), $this);
            $this->ESTADO = new clsControl(ccsTextBox, "ESTADO", "ESTADO", ccsText, "", CCGetRequestParam("ESTADO", $Method, NULL), $this);
            $this->CODPOS = new clsControl(ccsTextBox, "CODPOS", "CODPOS", ccsText, "", CCGetRequestParam("CODPOS", $Method, NULL), $this);
            $this->TELEFO = new clsControl(ccsTextBox, "TELEFO", "TELEFO", ccsText, "", CCGetRequestParam("TELEFO", $Method, NULL), $this);
            $this->TELFAX = new clsControl(ccsTextBox, "TELFAX", "TELFAX", ccsText, "", CCGetRequestParam("TELFAX", $Method, NULL), $this);
            $this->CONTAT = new clsControl(ccsTextBox, "CONTAT", "CONTAT", ccsText, "", CCGetRequestParam("CONTAT", $Method, NULL), $this);
            $this->LOGCOB = new clsControl(ccsTextBox, "LOGCOB", "LOGCOB", ccsText, "", CCGetRequestParam("LOGCOB", $Method, NULL), $this);
            $this->BAICOB = new clsControl(ccsTextBox, "BAICOB", "BAICOB", ccsText, "", CCGetRequestParam("BAICOB", $Method, NULL), $this);
            $this->CIDCOB = new clsControl(ccsTextBox, "CIDCOB", "CIDCOB", ccsText, "", CCGetRequestParam("CIDCOB", $Method, NULL), $this);
            $this->ESTCOB = new clsControl(ccsTextBox, "ESTCOB", "ESTCOB", ccsText, "", CCGetRequestParam("ESTCOB", $Method, NULL), $this);
            $this->CODINC = new clsControl(ccsHidden, "CODINC", "CODINC", ccsText, "", CCGetRequestParam("CODINC", $Method, NULL), $this);
            $this->POSCOB = new clsControl(ccsTextBox, "POSCOB", "POSCOB", ccsText, "", CCGetRequestParam("POSCOB", $Method, NULL), $this);
            $this->CODALT = new clsControl(ccsHidden, "CODALT", "CODALT", ccsText, "", CCGetRequestParam("CODALT", $Method, NULL), $this);
            $this->TELCOB = new clsControl(ccsTextBox, "TELCOB", "TELCOB", ccsText, "", CCGetRequestParam("TELCOB", $Method, NULL), $this);
            $this->DATINC = new clsControl(ccsHidden, "DATINC", "DATINC", ccsDate, $DefaultDateFormat, CCGetRequestParam("DATINC", $Method, NULL), $this);
            $this->CODSIT = new clsControl(ccsTextBox, "CODSIT", "CODSIT", ccsText, "", CCGetRequestParam("CODSIT", $Method, NULL), $this);
            $this->DATALT = new clsControl(ccsHidden, "DATALT", "DATALT", ccsDate, $DefaultDateFormat, CCGetRequestParam("DATALT", $Method, NULL), $this);
            $this->PGTOELET = new clsControl(ccsCheckBox, "PGTOELET", "PGTOELET", ccsText, "", CCGetRequestParam("PGTOELET", $Method, NULL), $this);
            $this->PGTOELET->CheckedValue = $this->PGTOELET->GetParsedValue("Y");
            $this->PGTOELET->UncheckedValue = $this->PGTOELET->GetParsedValue("N");
            $this->ESFERA = new clsControl(ccsCheckBox, "ESFERA", "ESFERA", ccsBoolean, $CCSLocales->GetFormatInfo("BooleanFormat"), CCGetRequestParam("ESFERA", $Method, NULL), $this);
            $this->ESFERA->CheckedValue = true;
            $this->ESFERA->UncheckedValue = false;
            $this->Button_Insert = new clsButton("Button_Insert", $Method, $this);
            $this->Button_Update = new clsButton("Button_Update", $Method, $this);
            $this->Button_Delete = new clsButton("Button_Delete", $Method, $this);
            $this->Button_Cancel = new clsButton("Button_Cancel", $Method, $this);
            if(!$this->FormSubmitted) {
                if(!is_array($this->PGTOELET->Value) && !strlen($this->PGTOELET->Value) && $this->PGTOELET->Value !== false)
                    $this->PGTOELET->SetValue(true);
                if(!is_array($this->ESFERA->Value) && !strlen($this->ESFERA->Value) && $this->ESFERA->Value !== false)
                    $this->ESFERA->SetValue(true);
            }
        }
    }
//End Class_Initialize Event

//Initialize Method @4-B2F3EC7E
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->DataSource->Parameters["urlCODCLI"] = CCGetFromGet("CODCLI", NULL);
    }
//End Initialize Method

//Validate Method @4-FDDADA23
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->CODCLI->Validate() && $Validation);
        $Validation = ($this->DESCLI->Validate() && $Validation);
        $Validation = ($this->DESFAN->Validate() && $Validation);
        $Validation = ($this->CODUNI->Validate() && $Validation);
        $Validation = ($this->CODDIS->Validate() && $Validation);
        $Validation = ($this->CODSET->Validate() && $Validation);
        $Validation = ($this->GRPATI->Validate() && $Validation);
        $Validation = ($this->SUBATI->Validate() && $Validation);
        $Validation = ($this->RadioButton1->Validate() && $Validation);
        $Validation = ($this->CGCCPF->Validate() && $Validation);
        $Validation = ($this->LOGRAD->Validate() && $Validation);
        $Validation = ($this->BAIRRO->Validate() && $Validation);
        $Validation = ($this->CIDADE->Validate() && $Validation);
        $Validation = ($this->ESTADO->Validate() && $Validation);
        $Validation = ($this->CODPOS->Validate() && $Validation);
        $Validation = ($this->TELEFO->Validate() && $Validation);
        $Validation = ($this->TELFAX->Validate() && $Validation);
        $Validation = ($this->CONTAT->Validate() && $Validation);
        $Validation = ($this->LOGCOB->Validate() && $Validation);
        $Validation = ($this->BAICOB->Validate() && $Validation);
        $Validation = ($this->CIDCOB->Validate() && $Validation);
        $Validation = ($this->ESTCOB->Validate() && $Validation);
        $Validation = ($this->CODINC->Validate() && $Validation);
        $Validation = ($this->POSCOB->Validate() && $Validation);
        $Validation = ($this->CODALT->Validate() && $Validation);
        $Validation = ($this->TELCOB->Validate() && $Validation);
        $Validation = ($this->DATINC->Validate() && $Validation);
        $Validation = ($this->CODSIT->Validate() && $Validation);
        $Validation = ($this->DATALT->Validate() && $Validation);
        $Validation = ($this->PGTOELET->Validate() && $Validation);
        $Validation = ($this->ESFERA->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->CODCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DESCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DESFAN->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODUNI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODDIS->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODSET->Errors->Count() == 0);
        $Validation =  $Validation && ($this->GRPATI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->SUBATI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->RadioButton1->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CGCCPF->Errors->Count() == 0);
        $Validation =  $Validation && ($this->LOGRAD->Errors->Count() == 0);
        $Validation =  $Validation && ($this->BAIRRO->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CIDADE->Errors->Count() == 0);
        $Validation =  $Validation && ($this->ESTADO->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODPOS->Errors->Count() == 0);
        $Validation =  $Validation && ($this->TELEFO->Errors->Count() == 0);
        $Validation =  $Validation && ($this->TELFAX->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CONTAT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->LOGCOB->Errors->Count() == 0);
        $Validation =  $Validation && ($this->BAICOB->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CIDCOB->Errors->Count() == 0);
        $Validation =  $Validation && ($this->ESTCOB->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODINC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->POSCOB->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODALT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->TELCOB->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DATINC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODSIT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DATALT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->PGTOELET->Errors->Count() == 0);
        $Validation =  $Validation && ($this->ESFERA->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @4-BFD2C74D
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->CODCLI->Errors->Count());
        $errors = ($errors || $this->DESCLI->Errors->Count());
        $errors = ($errors || $this->DESFAN->Errors->Count());
        $errors = ($errors || $this->CODUNI->Errors->Count());
        $errors = ($errors || $this->CODDIS->Errors->Count());
        $errors = ($errors || $this->CODSET->Errors->Count());
        $errors = ($errors || $this->GRPATI->Errors->Count());
        $errors = ($errors || $this->SUBATI->Errors->Count());
        $errors = ($errors || $this->RadioButton1->Errors->Count());
        $errors = ($errors || $this->CGCCPF->Errors->Count());
        $errors = ($errors || $this->LOGRAD->Errors->Count());
        $errors = ($errors || $this->BAIRRO->Errors->Count());
        $errors = ($errors || $this->CIDADE->Errors->Count());
        $errors = ($errors || $this->ESTADO->Errors->Count());
        $errors = ($errors || $this->CODPOS->Errors->Count());
        $errors = ($errors || $this->TELEFO->Errors->Count());
        $errors = ($errors || $this->TELFAX->Errors->Count());
        $errors = ($errors || $this->CONTAT->Errors->Count());
        $errors = ($errors || $this->LOGCOB->Errors->Count());
        $errors = ($errors || $this->BAICOB->Errors->Count());
        $errors = ($errors || $this->CIDCOB->Errors->Count());
        $errors = ($errors || $this->ESTCOB->Errors->Count());
        $errors = ($errors || $this->CODINC->Errors->Count());
        $errors = ($errors || $this->POSCOB->Errors->Count());
        $errors = ($errors || $this->CODALT->Errors->Count());
        $errors = ($errors || $this->TELCOB->Errors->Count());
        $errors = ($errors || $this->DATINC->Errors->Count());
        $errors = ($errors || $this->CODSIT->Errors->Count());
        $errors = ($errors || $this->DATALT->Errors->Count());
        $errors = ($errors || $this->PGTOELET->Errors->Count());
        $errors = ($errors || $this->ESFERA->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @4-288F0419
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->DataSource->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->DataSource->AllParametersSet;
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = $this->EditMode ? "Button_Update" : "Button_Insert";
            if($this->Button_Insert->Pressed) {
                $this->PressedButton = "Button_Insert";
            } else if($this->Button_Update->Pressed) {
                $this->PressedButton = "Button_Update";
            } else if($this->Button_Delete->Pressed) {
                $this->PressedButton = "Button_Delete";
            } else if($this->Button_Cancel->Pressed) {
                $this->PressedButton = "Button_Cancel";
            }
        }
        $Redirect = $FileName . "?" . CCGetQueryString("QueryString", array("ccsForm"));
        if($this->PressedButton == "Button_Delete") {
            if(!CCGetEvent($this->Button_Delete->CCSEvents, "OnClick", $this->Button_Delete) || !$this->DeleteRow()) {
                $Redirect = "";
            }
        } else if($this->PressedButton == "Button_Cancel") {
            if(!CCGetEvent($this->Button_Cancel->CCSEvents, "OnClick", $this->Button_Cancel)) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button_Insert") {
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick", $this->Button_Insert) || !$this->InsertRow()) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "Button_Update") {
                if(!CCGetEvent($this->Button_Update->CCSEvents, "OnClick", $this->Button_Update) || !$this->UpdateRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
        if ($Redirect)
            $this->DataSource->close();
    }
//End Operation Method

//InsertRow Method @4-901CAE3D
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert", $this);
        if(!$this->InsertAllowed) return false;
        $this->DataSource->CODCLI->SetValue($this->CODCLI->GetValue(true));
        $this->DataSource->DESCLI->SetValue($this->DESCLI->GetValue(true));
        $this->DataSource->DESFAN->SetValue($this->DESFAN->GetValue(true));
        $this->DataSource->CODUNI->SetValue($this->CODUNI->GetValue(true));
        $this->DataSource->CODDIS->SetValue($this->CODDIS->GetValue(true));
        $this->DataSource->CODSET->SetValue($this->CODSET->GetValue(true));
        $this->DataSource->GRPATI->SetValue($this->GRPATI->GetValue(true));
        $this->DataSource->SUBATI->SetValue($this->SUBATI->GetValue(true));
        $this->DataSource->RadioButton1->SetValue($this->RadioButton1->GetValue(true));
        $this->DataSource->CGCCPF->SetValue($this->CGCCPF->GetValue(true));
        $this->DataSource->LOGRAD->SetValue($this->LOGRAD->GetValue(true));
        $this->DataSource->BAIRRO->SetValue($this->BAIRRO->GetValue(true));
        $this->DataSource->CIDADE->SetValue($this->CIDADE->GetValue(true));
        $this->DataSource->ESTADO->SetValue($this->ESTADO->GetValue(true));
        $this->DataSource->CODPOS->SetValue($this->CODPOS->GetValue(true));
        $this->DataSource->TELEFO->SetValue($this->TELEFO->GetValue(true));
        $this->DataSource->TELFAX->SetValue($this->TELFAX->GetValue(true));
        $this->DataSource->CONTAT->SetValue($this->CONTAT->GetValue(true));
        $this->DataSource->LOGCOB->SetValue($this->LOGCOB->GetValue(true));
        $this->DataSource->BAICOB->SetValue($this->BAICOB->GetValue(true));
        $this->DataSource->CIDCOB->SetValue($this->CIDCOB->GetValue(true));
        $this->DataSource->ESTCOB->SetValue($this->ESTCOB->GetValue(true));
        $this->DataSource->CODINC->SetValue($this->CODINC->GetValue(true));
        $this->DataSource->POSCOB->SetValue($this->POSCOB->GetValue(true));
        $this->DataSource->CODALT->SetValue($this->CODALT->GetValue(true));
        $this->DataSource->TELCOB->SetValue($this->TELCOB->GetValue(true));
        $this->DataSource->DATINC->SetValue($this->DATINC->GetValue(true));
        $this->DataSource->CODSIT->SetValue($this->CODSIT->GetValue(true));
        $this->DataSource->DATALT->SetValue($this->DATALT->GetValue(true));
        $this->DataSource->PGTOELET->SetValue($this->PGTOELET->GetValue(true));
        $this->DataSource->ESFERA->SetValue($this->ESFERA->GetValue(true));
        $this->DataSource->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert", $this);
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//UpdateRow Method @4-2B8E1743
    function UpdateRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeUpdate", $this);
        if(!$this->UpdateAllowed) return false;
        $this->DataSource->CODCLI->SetValue($this->CODCLI->GetValue(true));
        $this->DataSource->DESCLI->SetValue($this->DESCLI->GetValue(true));
        $this->DataSource->DESFAN->SetValue($this->DESFAN->GetValue(true));
        $this->DataSource->CODUNI->SetValue($this->CODUNI->GetValue(true));
        $this->DataSource->CODDIS->SetValue($this->CODDIS->GetValue(true));
        $this->DataSource->CODSET->SetValue($this->CODSET->GetValue(true));
        $this->DataSource->GRPATI->SetValue($this->GRPATI->GetValue(true));
        $this->DataSource->SUBATI->SetValue($this->SUBATI->GetValue(true));
        $this->DataSource->RadioButton1->SetValue($this->RadioButton1->GetValue(true));
        $this->DataSource->CGCCPF->SetValue($this->CGCCPF->GetValue(true));
        $this->DataSource->LOGRAD->SetValue($this->LOGRAD->GetValue(true));
        $this->DataSource->BAIRRO->SetValue($this->BAIRRO->GetValue(true));
        $this->DataSource->CIDADE->SetValue($this->CIDADE->GetValue(true));
        $this->DataSource->ESTADO->SetValue($this->ESTADO->GetValue(true));
        $this->DataSource->CODPOS->SetValue($this->CODPOS->GetValue(true));
        $this->DataSource->TELEFO->SetValue($this->TELEFO->GetValue(true));
        $this->DataSource->TELFAX->SetValue($this->TELFAX->GetValue(true));
        $this->DataSource->CONTAT->SetValue($this->CONTAT->GetValue(true));
        $this->DataSource->LOGCOB->SetValue($this->LOGCOB->GetValue(true));
        $this->DataSource->BAICOB->SetValue($this->BAICOB->GetValue(true));
        $this->DataSource->CIDCOB->SetValue($this->CIDCOB->GetValue(true));
        $this->DataSource->ESTCOB->SetValue($this->ESTCOB->GetValue(true));
        $this->DataSource->CODINC->SetValue($this->CODINC->GetValue(true));
        $this->DataSource->POSCOB->SetValue($this->POSCOB->GetValue(true));
        $this->DataSource->CODALT->SetValue($this->CODALT->GetValue(true));
        $this->DataSource->TELCOB->SetValue($this->TELCOB->GetValue(true));
        $this->DataSource->DATINC->SetValue($this->DATINC->GetValue(true));
        $this->DataSource->CODSIT->SetValue($this->CODSIT->GetValue(true));
        $this->DataSource->DATALT->SetValue($this->DATALT->GetValue(true));
        $this->DataSource->PGTOELET->SetValue($this->PGTOELET->GetValue(true));
        $this->DataSource->ESFERA->SetValue($this->ESFERA->GetValue(true));
        $this->DataSource->Update();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterUpdate", $this);
        return (!$this->CheckErrors());
    }
//End UpdateRow Method

//DeleteRow Method @4-299D98C3
    function DeleteRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDelete", $this);
        if(!$this->DeleteAllowed) return false;
        $this->DataSource->Delete();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDelete", $this);
        return (!$this->CheckErrors());
    }
//End DeleteRow Method

//Show Method @4-E1120FA9
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->CODUNI->Prepare();
        $this->CODDIS->Prepare();
        $this->GRPATI->Prepare();
        $this->SUBATI->Prepare();
        $this->RadioButton1->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->DataSource->Errors->Count()){
                $this->Errors->AddErrors($this->DataSource->Errors);
                $this->DataSource->Errors->clear();
            }
            $this->DataSource->Open();
            if($this->DataSource->Errors->Count() == 0 && $this->DataSource->next_record()) {
                $this->DataSource->SetValues();
                if(!$this->FormSubmitted){
                    $this->CODCLI->SetValue($this->DataSource->CODCLI->GetValue());
                    $this->DESCLI->SetValue($this->DataSource->DESCLI->GetValue());
                    $this->DESFAN->SetValue($this->DataSource->DESFAN->GetValue());
                    $this->CODUNI->SetValue($this->DataSource->CODUNI->GetValue());
                    $this->CODDIS->SetValue($this->DataSource->CODDIS->GetValue());
                    $this->CODSET->SetValue($this->DataSource->CODSET->GetValue());
                    $this->GRPATI->SetValue($this->DataSource->GRPATI->GetValue());
                    $this->SUBATI->SetValue($this->DataSource->SUBATI->GetValue());
                    $this->RadioButton1->SetValue($this->DataSource->RadioButton1->GetValue());
                    $this->CGCCPF->SetValue($this->DataSource->CGCCPF->GetValue());
                    $this->LOGRAD->SetValue($this->DataSource->LOGRAD->GetValue());
                    $this->BAIRRO->SetValue($this->DataSource->BAIRRO->GetValue());
                    $this->CIDADE->SetValue($this->DataSource->CIDADE->GetValue());
                    $this->ESTADO->SetValue($this->DataSource->ESTADO->GetValue());
                    $this->CODPOS->SetValue($this->DataSource->CODPOS->GetValue());
                    $this->TELEFO->SetValue($this->DataSource->TELEFO->GetValue());
                    $this->TELFAX->SetValue($this->DataSource->TELFAX->GetValue());
                    $this->CONTAT->SetValue($this->DataSource->CONTAT->GetValue());
                    $this->LOGCOB->SetValue($this->DataSource->LOGCOB->GetValue());
                    $this->BAICOB->SetValue($this->DataSource->BAICOB->GetValue());
                    $this->CIDCOB->SetValue($this->DataSource->CIDCOB->GetValue());
                    $this->ESTCOB->SetValue($this->DataSource->ESTCOB->GetValue());
                    $this->CODINC->SetValue($this->DataSource->CODINC->GetValue());
                    $this->POSCOB->SetValue($this->DataSource->POSCOB->GetValue());
                    $this->CODALT->SetValue($this->DataSource->CODALT->GetValue());
                    $this->TELCOB->SetValue($this->DataSource->TELCOB->GetValue());
                    $this->DATINC->SetValue($this->DataSource->DATINC->GetValue());
                    $this->CODSIT->SetValue($this->DataSource->CODSIT->GetValue());
                    $this->DATALT->SetValue($this->DataSource->DATALT->GetValue());
                    $this->PGTOELET->SetValue($this->DataSource->PGTOELET->GetValue());
                    $this->ESFERA->SetValue($this->DataSource->ESFERA->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->CODCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DESCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DESFAN->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODUNI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODDIS->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODSET->Errors->ToString());
            $Error = ComposeStrings($Error, $this->GRPATI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->SUBATI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->RadioButton1->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CGCCPF->Errors->ToString());
            $Error = ComposeStrings($Error, $this->LOGRAD->Errors->ToString());
            $Error = ComposeStrings($Error, $this->BAIRRO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CIDADE->Errors->ToString());
            $Error = ComposeStrings($Error, $this->ESTADO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODPOS->Errors->ToString());
            $Error = ComposeStrings($Error, $this->TELEFO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->TELFAX->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CONTAT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->LOGCOB->Errors->ToString());
            $Error = ComposeStrings($Error, $this->BAICOB->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CIDCOB->Errors->ToString());
            $Error = ComposeStrings($Error, $this->ESTCOB->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODINC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->POSCOB->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODALT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->TELCOB->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DATINC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODSIT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DATALT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->PGTOELET->Errors->ToString());
            $Error = ComposeStrings($Error, $this->ESFERA->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataSource->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Insert->Visible = !$this->EditMode && $this->InsertAllowed;
        $this->Button_Update->Visible = $this->EditMode && $this->UpdateAllowed;
        $this->Button_Delete->Visible = $this->EditMode && $this->DeleteAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->CODCLI->Show();
        $this->DESCLI->Show();
        $this->DESFAN->Show();
        $this->CODUNI->Show();
        $this->CODDIS->Show();
        $this->CODSET->Show();
        $this->GRPATI->Show();
        $this->SUBATI->Show();
        $this->RadioButton1->Show();
        $this->CGCCPF->Show();
        $this->LOGRAD->Show();
        $this->BAIRRO->Show();
        $this->CIDADE->Show();
        $this->ESTADO->Show();
        $this->CODPOS->Show();
        $this->TELEFO->Show();
        $this->TELFAX->Show();
        $this->CONTAT->Show();
        $this->LOGCOB->Show();
        $this->BAICOB->Show();
        $this->CIDCOB->Show();
        $this->ESTCOB->Show();
        $this->CODINC->Show();
        $this->POSCOB->Show();
        $this->CODALT->Show();
        $this->TELCOB->Show();
        $this->DATINC->Show();
        $this->CODSIT->Show();
        $this->DATALT->Show();
        $this->PGTOELET->Show();
        $this->ESFERA->Show();
        $this->Button_Insert->Show();
        $this->Button_Update->Show();
        $this->Button_Delete->Show();
        $this->Button_Cancel->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

} //End CADCLI Class @4-FCB6E20C

class clsCADCLIDataSource extends clsDBFaturar {  //CADCLIDataSource Class @4-C5FD973D

//DataSource Variables @4-9EF48AE0
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $InsertParameters;
    public $UpdateParameters;
    public $DeleteParameters;
    public $wp;
    public $AllParametersSet;

    public $InsertFields = array();
    public $UpdateFields = array();

    // Datasource fields
    public $CODCLI;
    public $DESCLI;
    public $DESFAN;
    public $CODUNI;
    public $CODDIS;
    public $CODSET;
    public $GRPATI;
    public $SUBATI;
    public $RadioButton1;
    public $CGCCPF;
    public $LOGRAD;
    public $BAIRRO;
    public $CIDADE;
    public $ESTADO;
    public $CODPOS;
    public $TELEFO;
    public $TELFAX;
    public $CONTAT;
    public $LOGCOB;
    public $BAICOB;
    public $CIDCOB;
    public $ESTCOB;
    public $CODINC;
    public $POSCOB;
    public $CODALT;
    public $TELCOB;
    public $DATINC;
    public $CODSIT;
    public $DATALT;
    public $PGTOELET;
    public $ESFERA;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-85676584
    function clsCADCLIDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Record CADCLI/Error";
        $this->Initialize();
        $this->CODCLI = new clsField("CODCLI", ccsText, "");
        $this->DESCLI = new clsField("DESCLI", ccsText, "");
        $this->DESFAN = new clsField("DESFAN", ccsText, "");
        $this->CODUNI = new clsField("CODUNI", ccsText, "");
        $this->CODDIS = new clsField("CODDIS", ccsText, "");
        $this->CODSET = new clsField("CODSET", ccsText, "");
        $this->GRPATI = new clsField("GRPATI", ccsText, "");
        $this->SUBATI = new clsField("SUBATI", ccsText, "");
        $this->RadioButton1 = new clsField("RadioButton1", ccsText, "");
        $this->CGCCPF = new clsField("CGCCPF", ccsText, "");
        $this->LOGRAD = new clsField("LOGRAD", ccsText, "");
        $this->BAIRRO = new clsField("BAIRRO", ccsText, "");
        $this->CIDADE = new clsField("CIDADE", ccsText, "");
        $this->ESTADO = new clsField("ESTADO", ccsText, "");
        $this->CODPOS = new clsField("CODPOS", ccsText, "");
        $this->TELEFO = new clsField("TELEFO", ccsText, "");
        $this->TELFAX = new clsField("TELFAX", ccsText, "");
        $this->CONTAT = new clsField("CONTAT", ccsText, "");
        $this->LOGCOB = new clsField("LOGCOB", ccsText, "");
        $this->BAICOB = new clsField("BAICOB", ccsText, "");
        $this->CIDCOB = new clsField("CIDCOB", ccsText, "");
        $this->ESTCOB = new clsField("ESTCOB", ccsText, "");
        $this->CODINC = new clsField("CODINC", ccsText, "");
        $this->POSCOB = new clsField("POSCOB", ccsText, "");
        $this->CODALT = new clsField("CODALT", ccsText, "");
        $this->TELCOB = new clsField("TELCOB", ccsText, "");
        $this->DATINC = new clsField("DATINC", ccsDate, $this->DateFormat);
        $this->CODSIT = new clsField("CODSIT", ccsText, "");
        $this->DATALT = new clsField("DATALT", ccsDate, $this->DateFormat);
        $this->PGTOELET = new clsField("PGTOELET", ccsText, "");
        $this->ESFERA = new clsField("ESFERA", ccsBoolean, $this->BooleanFormat);

        $this->InsertFields["CODCLI"] = array("Name" => "CODCLI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DESCLI"] = array("Name" => "DESCLI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DESFAN"] = array("Name" => "DESFAN", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODUNI"] = array("Name" => "CODUNI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODDIS"] = array("Name" => "CODDIS", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODSET"] = array("Name" => "CODSET", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["GRPATI"] = array("Name" => "GRPATI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["SUBATI"] = array("Name" => "SUBATI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODTIP"] = array("Name" => "CODTIP", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CGCCPF"] = array("Name" => "CGCCPF", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["LOGRAD"] = array("Name" => "LOGRAD", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["BAIRRO"] = array("Name" => "BAIRRO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CIDADE"] = array("Name" => "CIDADE", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["ESTADO"] = array("Name" => "ESTADO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODPOS"] = array("Name" => "CODPOS", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["TELEFO"] = array("Name" => "TELEFO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["TELFAX"] = array("Name" => "TELFAX", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CONTAT"] = array("Name" => "CONTAT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["LOGCOB"] = array("Name" => "LOGCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["BAICOB"] = array("Name" => "BAICOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CIDCOB"] = array("Name" => "CIDCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["ESTCOB"] = array("Name" => "ESTCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODINC"] = array("Name" => "CODINC", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["POSCOB"] = array("Name" => "POSCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODALT"] = array("Name" => "CODALT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["TELCOB"] = array("Name" => "TELCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DATINC"] = array("Name" => "DATINC", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->InsertFields["CODSIT"] = array("Name" => "CODSIT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DATALT"] = array("Name" => "DATALT", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->InsertFields["PGTOELET"] = array("Name" => "PGTOELET", "Value" => "", "DataType" => ccsText);
        $this->InsertFields["ESFERA"] = array("Name" => "ESFERA", "Value" => "", "DataType" => ccsBoolean);
        $this->UpdateFields["CODCLI"] = array("Name" => "CODCLI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["DESCLI"] = array("Name" => "DESCLI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["DESFAN"] = array("Name" => "DESFAN", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODUNI"] = array("Name" => "CODUNI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODDIS"] = array("Name" => "CODDIS", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODSET"] = array("Name" => "CODSET", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["GRPATI"] = array("Name" => "GRPATI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["SUBATI"] = array("Name" => "SUBATI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODTIP"] = array("Name" => "CODTIP", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CGCCPF"] = array("Name" => "CGCCPF", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["LOGRAD"] = array("Name" => "LOGRAD", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["BAIRRO"] = array("Name" => "BAIRRO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CIDADE"] = array("Name" => "CIDADE", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["ESTADO"] = array("Name" => "ESTADO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODPOS"] = array("Name" => "CODPOS", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["TELEFO"] = array("Name" => "TELEFO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["TELFAX"] = array("Name" => "TELFAX", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CONTAT"] = array("Name" => "CONTAT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["LOGCOB"] = array("Name" => "LOGCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["BAICOB"] = array("Name" => "BAICOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CIDCOB"] = array("Name" => "CIDCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["ESTCOB"] = array("Name" => "ESTCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODINC"] = array("Name" => "CODINC", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["POSCOB"] = array("Name" => "POSCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODALT"] = array("Name" => "CODALT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["TELCOB"] = array("Name" => "TELCOB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["DATINC"] = array("Name" => "DATINC", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODSIT"] = array("Name" => "CODSIT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["DATALT"] = array("Name" => "DATALT", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->UpdateFields["PGTOELET"] = array("Name" => "PGTOELET", "Value" => "", "DataType" => ccsText);
        $this->UpdateFields["ESFERA"] = array("Name" => "ESFERA", "Value" => "", "DataType" => ccsBoolean);
    }
//End DataSourceClass_Initialize Event

//Prepare Method @4-48075FFD
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlCODCLI", ccsText, "", "", $this->Parameters["urlCODCLI"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "CODCLI", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @4-DF02772C
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->SQL = "SELECT * \n\n" .
        "FROM Cadcli {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @4-CF09DD19
    function SetValues()
    {
        $this->CODCLI->SetDBValue($this->f("CODCLI"));
        $this->DESCLI->SetDBValue($this->f("DESCLI"));
        $this->DESFAN->SetDBValue($this->f("DESFAN"));
        $this->CODUNI->SetDBValue($this->f("CODUNI"));
        $this->CODDIS->SetDBValue($this->f("CODDIS"));
        $this->CODSET->SetDBValue($this->f("CODSET"));
        $this->GRPATI->SetDBValue($this->f("GRPATI"));
        $this->SUBATI->SetDBValue($this->f("SUBATI"));
        $this->RadioButton1->SetDBValue($this->f("CODTIP"));
        $this->CGCCPF->SetDBValue($this->f("CGCCPF"));
        $this->LOGRAD->SetDBValue($this->f("LOGRAD"));
        $this->BAIRRO->SetDBValue($this->f("BAIRRO"));
        $this->CIDADE->SetDBValue($this->f("CIDADE"));
        $this->ESTADO->SetDBValue($this->f("ESTADO"));
        $this->CODPOS->SetDBValue($this->f("CODPOS"));
        $this->TELEFO->SetDBValue($this->f("TELEFO"));
        $this->TELFAX->SetDBValue($this->f("TELFAX"));
        $this->CONTAT->SetDBValue($this->f("CONTAT"));
        $this->LOGCOB->SetDBValue($this->f("LOGCOB"));
        $this->BAICOB->SetDBValue($this->f("BAICOB"));
        $this->CIDCOB->SetDBValue($this->f("CIDCOB"));
        $this->ESTCOB->SetDBValue($this->f("ESTCOB"));
        $this->CODINC->SetDBValue($this->f("CODINC"));
        $this->POSCOB->SetDBValue($this->f("POSCOB"));
        $this->CODALT->SetDBValue($this->f("CODALT"));
        $this->TELCOB->SetDBValue($this->f("TELCOB"));
        $this->DATINC->SetDBValue(trim($this->f("DATINC")));
        $this->CODSIT->SetDBValue($this->f("CODSIT"));
        $this->DATALT->SetDBValue(trim($this->f("DATALT")));
        $this->PGTOELET->SetDBValue($this->f("PGTOELET"));
        $this->ESFERA->SetDBValue(trim($this->f("ESFERA")));
    }
//End SetValues Method

//Insert Method @4-17F16C2E
    function Insert()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert", $this->Parent);
        $this->InsertFields["CODCLI"]["Value"] = $this->CODCLI->GetDBValue(true);
        $this->InsertFields["DESCLI"]["Value"] = $this->DESCLI->GetDBValue(true);
        $this->InsertFields["DESFAN"]["Value"] = $this->DESFAN->GetDBValue(true);
        $this->InsertFields["CODUNI"]["Value"] = $this->CODUNI->GetDBValue(true);
        $this->InsertFields["CODDIS"]["Value"] = $this->CODDIS->GetDBValue(true);
        $this->InsertFields["CODSET"]["Value"] = $this->CODSET->GetDBValue(true);
        $this->InsertFields["GRPATI"]["Value"] = $this->GRPATI->GetDBValue(true);
        $this->InsertFields["SUBATI"]["Value"] = $this->SUBATI->GetDBValue(true);
        $this->InsertFields["CODTIP"]["Value"] = $this->RadioButton1->GetDBValue(true);
        $this->InsertFields["CGCCPF"]["Value"] = $this->CGCCPF->GetDBValue(true);
        $this->InsertFields["LOGRAD"]["Value"] = $this->LOGRAD->GetDBValue(true);
        $this->InsertFields["BAIRRO"]["Value"] = $this->BAIRRO->GetDBValue(true);
        $this->InsertFields["CIDADE"]["Value"] = $this->CIDADE->GetDBValue(true);
        $this->InsertFields["ESTADO"]["Value"] = $this->ESTADO->GetDBValue(true);
        $this->InsertFields["CODPOS"]["Value"] = $this->CODPOS->GetDBValue(true);
        $this->InsertFields["TELEFO"]["Value"] = $this->TELEFO->GetDBValue(true);
        $this->InsertFields["TELFAX"]["Value"] = $this->TELFAX->GetDBValue(true);
        $this->InsertFields["CONTAT"]["Value"] = $this->CONTAT->GetDBValue(true);
        $this->InsertFields["LOGCOB"]["Value"] = $this->LOGCOB->GetDBValue(true);
        $this->InsertFields["BAICOB"]["Value"] = $this->BAICOB->GetDBValue(true);
        $this->InsertFields["CIDCOB"]["Value"] = $this->CIDCOB->GetDBValue(true);
        $this->InsertFields["ESTCOB"]["Value"] = $this->ESTCOB->GetDBValue(true);
        $this->InsertFields["CODINC"]["Value"] = $this->CODINC->GetDBValue(true);
        $this->InsertFields["POSCOB"]["Value"] = $this->POSCOB->GetDBValue(true);
        $this->InsertFields["CODALT"]["Value"] = $this->CODALT->GetDBValue(true);
        $this->InsertFields["TELCOB"]["Value"] = $this->TELCOB->GetDBValue(true);
        $this->InsertFields["DATINC"]["Value"] = $this->DATINC->GetDBValue(true);
        $this->InsertFields["CODSIT"]["Value"] = $this->CODSIT->GetDBValue(true);
        $this->InsertFields["DATALT"]["Value"] = $this->DATALT->GetDBValue(true);
        $this->InsertFields["PGTOELET"]["Value"] = $this->PGTOELET->GetDBValue(true);
        $this->InsertFields["ESFERA"]["Value"] = $this->ESFERA->GetDBValue(true);
        $this->SQL = CCBuildInsert("Cadcli", $this->InsertFields, $this);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert", $this->Parent);
        }
    }
//End Insert Method

//Update Method @4-E93F025D
    function Update()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildUpdate", $this->Parent);
        $this->UpdateFields["CODCLI"]["Value"] = $this->CODCLI->GetDBValue(true);
        $this->UpdateFields["DESCLI"]["Value"] = $this->DESCLI->GetDBValue(true);
        $this->UpdateFields["DESFAN"]["Value"] = $this->DESFAN->GetDBValue(true);
        $this->UpdateFields["CODUNI"]["Value"] = $this->CODUNI->GetDBValue(true);
        $this->UpdateFields["CODDIS"]["Value"] = $this->CODDIS->GetDBValue(true);
        $this->UpdateFields["CODSET"]["Value"] = $this->CODSET->GetDBValue(true);
        $this->UpdateFields["GRPATI"]["Value"] = $this->GRPATI->GetDBValue(true);
        $this->UpdateFields["SUBATI"]["Value"] = $this->SUBATI->GetDBValue(true);
        $this->UpdateFields["CODTIP"]["Value"] = $this->RadioButton1->GetDBValue(true);
        $this->UpdateFields["CGCCPF"]["Value"] = $this->CGCCPF->GetDBValue(true);
        $this->UpdateFields["LOGRAD"]["Value"] = $this->LOGRAD->GetDBValue(true);
        $this->UpdateFields["BAIRRO"]["Value"] = $this->BAIRRO->GetDBValue(true);
        $this->UpdateFields["CIDADE"]["Value"] = $this->CIDADE->GetDBValue(true);
        $this->UpdateFields["ESTADO"]["Value"] = $this->ESTADO->GetDBValue(true);
        $this->UpdateFields["CODPOS"]["Value"] = $this->CODPOS->GetDBValue(true);
        $this->UpdateFields["TELEFO"]["Value"] = $this->TELEFO->GetDBValue(true);
        $this->UpdateFields["TELFAX"]["Value"] = $this->TELFAX->GetDBValue(true);
        $this->UpdateFields["CONTAT"]["Value"] = $this->CONTAT->GetDBValue(true);
        $this->UpdateFields["LOGCOB"]["Value"] = $this->LOGCOB->GetDBValue(true);
        $this->UpdateFields["BAICOB"]["Value"] = $this->BAICOB->GetDBValue(true);
        $this->UpdateFields["CIDCOB"]["Value"] = $this->CIDCOB->GetDBValue(true);
        $this->UpdateFields["ESTCOB"]["Value"] = $this->ESTCOB->GetDBValue(true);
        $this->UpdateFields["CODINC"]["Value"] = $this->CODINC->GetDBValue(true);
        $this->UpdateFields["POSCOB"]["Value"] = $this->POSCOB->GetDBValue(true);
        $this->UpdateFields["CODALT"]["Value"] = $this->CODALT->GetDBValue(true);
        $this->UpdateFields["TELCOB"]["Value"] = $this->TELCOB->GetDBValue(true);
        $this->UpdateFields["DATINC"]["Value"] = $this->DATINC->GetDBValue(true);
        $this->UpdateFields["CODSIT"]["Value"] = $this->CODSIT->GetDBValue(true);
        $this->UpdateFields["DATALT"]["Value"] = $this->DATALT->GetDBValue(true);
        $this->UpdateFields["PGTOELET"]["Value"] = $this->PGTOELET->GetDBValue(true);
        $this->UpdateFields["ESFERA"]["Value"] = $this->ESFERA->GetDBValue(true);
        $this->SQL = CCBuildUpdate("Cadcli", $this->UpdateFields, $this);
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteUpdate", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteUpdate", $this->Parent);
        }
    }
//End Update Method

//Delete Method @4-3386AC11
    function Delete()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildDelete", $this->Parent);
        $this->SQL = "DELETE FROM \"Cadcli\"";
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteDelete", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteDelete", $this->Parent);
        }
    }
//End Delete Method

} //End CADCLIDataSource Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-E9BA337C
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "ManutCadCli1.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Include events file @1-BBD7AFDC
include("./ManutCadCli1_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-88B3E35B
$DBfaturar = new clsDBfaturar();
$MainPage->Connections["Faturar"] = & $DBfaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = & new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$CADCLI = & new clsRecordCADCLI("", $MainPage);
$rodape = & new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->CADCLI = & $CADCLI;
$MainPage->rodape = & $rodape;
$CADCLI->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-AF1ED153
$cabec->Operations();
$CADCLI->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-5E46C693
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($CADCLI);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-EFB85551
$cabec->Show();
$CADCLI->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-BA2326AF
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($CADCLI);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
