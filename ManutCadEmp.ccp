<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="4" sourceType="Table" urlType="Relative" secured="False" allowInsert="True" allowUpdate="True" allowDelete="False" validateData="True" preserveParameters="GET" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" connection="Faturar" name="CADEMP" dataSource="CADEMP" errorSummator="Error" wizardCaption="Add/Edit CADEMP " wizardFormMethod="post" returnPage="CadCadEmp.ccp">
			<Components>
				<TextBox id="9" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="FANTAS" fieldSource="FANTAS" required="True" caption="Nome Fantasia" wizardCaption="FANTAS" wizardSize="40" wizardMaxLength="40" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="22"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="10" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="RAZSOC" fieldSource="RAZSOC" required="True" caption="Razão Social" wizardCaption="RAZSOC" wizardSize="40" wizardMaxLength="40" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="23"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="11" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="LOGRAD" fieldSource="LOGRAD" required="True" caption="Logradouro" wizardCaption="LOGRAD" wizardSize="35" wizardMaxLength="35" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="24"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="12" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="DESBAI" fieldSource="DESBAI" required="True" caption="Bairro" wizardCaption="DESBAI" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="25"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="13" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="DESCID" fieldSource="DESCID" required="True" caption="Cidade" wizardCaption="DESCID" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="26"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<ListBox id="14" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="ESTADO" fieldSource="ESTADO" required="True" caption="Estado" wizardCaption="ESTADO" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" sourceType="ListOfValues" dataSource="AC;ACRE;AL;ALAGOAS;AM;AMAZONAS;AP;AMAPÁ;BA;BAHIA;CE;CEARÁ;DF;DISTRITO FEDERAL;ES;ESPÍRITO SANTO;GO;GOIÁS;MA;MARANHÃO;MG;MINAS GERAIS;MS;MATO GROSSO DO SUL;MT;MATO GROSSO;PA;PARÁ;PB;PARAÍBA;PE;PERNAMBUCO;PI;PIAUÍ;PR;PARANÁ;RJ;RIO DE JANEIRO;RN;RIO GRANDE DO NORTE;RO;RONDÔNIA;RR;RORAIMA;RS;RIO GRANDE DO SUL;SC;SANTA CATARINA;SE;SERGIPE;SP;SÃO PAULO;TO;TOCANTINS">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
				</ListBox>
				<TextBox id="15" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="CODPOS" fieldSource="CODPOS" required="True" caption="CODPOS" wizardCaption="CODPOS" wizardSize="9" wizardMaxLength="9" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="16" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="TELEFO" fieldSource="TELEFO" required="True" caption="Telefone" wizardCaption="TELEFO" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
<Event name="OnKeyPress" type="Client">
<Actions>
<Action actionName="Validate Entry" actionCategory="Validation" id="35" minimumLength="12" maximumLength="13" inputMask="(00)000000000"/>
</Actions>
</Event>
</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="17" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="COMPLE" fieldSource="COMPLE" required="True" caption="Complemento" wizardCaption="COMPLE" wizardSize="8" wizardMaxLength="8" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="27"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="18" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="MENSAG" fieldSource="MENSAG" required="True" caption="Mensagem" wizardCaption="MENSAG" wizardSize="50" wizardMaxLength="75" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="28"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="19" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="FAX" fieldSource="FAX" required="True" caption="Fax" wizardCaption="FAX" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
<Event name="OnKeyPress" type="Client">
<Actions>
<Action actionName="Validate Entry" actionCategory="Validation" id="36" minimumLength="12" maximumLength="13" inputMask="(00)000000000"/>
</Actions>
</Event>
</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="31" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="EMAIL" fieldSource="EMAIL" required="True" caption="Email" inputMask="^[\w\.-]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]+$">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<Button id="6" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Update" operation="Update" wizardCaption="Mudar">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="7" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Cancel" operation="Cancel" wizardCaption="Cancelar">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events>
<Event name="BeforeShow" type="Server">
<Actions>
<Action actionName="Custom Code" actionCategory="General" id="33"/>
</Actions>
</Event>
<Event name="OnValidate" type="Server">
<Actions>
<Action actionName="Custom Code" actionCategory="General" id="34"/>
</Actions>
</Event>
</Events>
			<TableParameters>
				<TableParameter id="8" conditionType="Parameter" useIsNull="False" field="FANTAS" parameterSource="FANTAS" dataType="Text" logicOperator="And" searchConditionType="Equal" parameterType="URL" orderNumber="1"/>
			</TableParameters>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="ManutCadEmp.php" forShow="True" url="ManutCadEmp.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="ManutCadEmp_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="29" groupID="1"/>
		<Group id="30" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="32"/>
			</Actions>
		</Event>
	</Events>
</Page>
