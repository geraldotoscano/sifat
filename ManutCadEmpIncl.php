<?php
//Include Common Files @1-7DA0231B
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "ManutCadEmpIncl.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files
include_once 'util.php';

class clsRecordCADEMP { //CADEMP Class @4-839A1FCE

//Variables @4-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @4-5DD62D45
    function clsRecordCADEMP($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record CADEMP/Error";
        $this->DataSource = new clsCADEMPDataSource($this);
        $this->ds = & $this->DataSource;
        $this->InsertAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "CADEMP";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->FANTAS = new clsControl(ccsTextBox, "FANTAS", "Nome Fantasia", ccsText, "", CCGetRequestParam("FANTAS", $Method, NULL), $this);
            $this->FANTAS->Required = true;
            $this->RAZSOC = new clsControl(ccsTextBox, "RAZSOC", "Raz�o Social", ccsText, "", CCGetRequestParam("RAZSOC", $Method, NULL), $this);
            $this->RAZSOC->Required = true;
            $this->LOGRAD = new clsControl(ccsTextBox, "LOGRAD", "Logradouro", ccsText, "", CCGetRequestParam("LOGRAD", $Method, NULL), $this);
            $this->LOGRAD->Required = true;
            $this->DESBAI = new clsControl(ccsTextBox, "DESBAI", "Bairro", ccsText, "", CCGetRequestParam("DESBAI", $Method, NULL), $this);
            $this->DESBAI->Required = true;
            $this->DESCID = new clsControl(ccsTextBox, "DESCID", "Cidade", ccsText, "", CCGetRequestParam("DESCID", $Method, NULL), $this);
            $this->DESCID->Required = true;
            $this->ESTADO = new clsControl(ccsListBox, "ESTADO", "Estado", ccsText, "", CCGetRequestParam("ESTADO", $Method, NULL), $this);
            $this->ESTADO->DSType = dsListOfValues;
            $this->ESTADO->Values = array(array("AC", "ACRE"), array("AL", "ALAGOAS"), array("AM", "AMAZONAS"), array("AP", "AMAP�"), array("BA", "BAHIA"), array("CE", "CEAR�"), array("DF", "DISTRITO FEDERAL"), array("ES", "ESP�RITO SANTO"), array("GO", "GOI�S"), array("MA", "MARANH�O"), array("MG", "MINAS GERAIS"), array("MS", "MATO GROSSO DO SUL"), array("MT", "MATO GROSSO"), array("PA", "PAR�"), array("PB", "PARA�BA"), array("PE", "PERNAMBUCO"), array("PI", "PIAU�"), array("PR", "PARAN�"), array("RJ", "RIO DE JANEIRO"), array("RN", "RIO GRANDE DO NORTE"), array("RO", "ROND�NIA"), array("RR", "RORAIMA"), array("RS", "RIO GRANDE DO SUL"), array("SC", "SANTA CATARINA"), array("SE", "SERGIPE"), array("SP", "S�O PAULO"), array("TO", "TOCANTINS"));
            $this->ESTADO->Required = true;
            $this->CODPOS = new clsControl(ccsTextBox, "CODPOS", "CODPOS", ccsText, "", CCGetRequestParam("CODPOS", $Method, NULL), $this);
            $this->CODPOS->Required = true;
            $this->TELEFO = new clsControl(ccsTextBox, "TELEFO", "Telefone", ccsText, "", CCGetRequestParam("TELEFO", $Method, NULL), $this);
            $this->TELEFO->Required = true;
            $this->COMPLE = new clsControl(ccsTextBox, "COMPLE", "Complemento", ccsText, "", CCGetRequestParam("COMPLE", $Method, NULL), $this);
            $this->COMPLE->Required = true;
            $this->MENSAG = new clsControl(ccsTextBox, "MENSAG", "Mensagem", ccsText, "", CCGetRequestParam("MENSAG", $Method, NULL), $this);
            $this->MENSAG->Required = true;
            $this->FAX = new clsControl(ccsTextBox, "FAX", "Fax", ccsText, "", CCGetRequestParam("FAX", $Method, NULL), $this);
            $this->FAX->Required = true;
            $this->Button_Update = new clsButton("Button_Update", $Method, $this);
        }
    }
//End Class_Initialize Event

//Initialize Method @4-11D4E436
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->DataSource->Parameters["urlFANTAS"] = CCGetFromGet("FANTAS", NULL);
    }
//End Initialize Method

//Validate Method @4-E1AECCE6
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->FANTAS->Validate() && $Validation);
        $Validation = ($this->RAZSOC->Validate() && $Validation);
        $Validation = ($this->LOGRAD->Validate() && $Validation);
        $Validation = ($this->DESBAI->Validate() && $Validation);
        $Validation = ($this->DESCID->Validate() && $Validation);
        $Validation = ($this->ESTADO->Validate() && $Validation);
        $Validation = ($this->CODPOS->Validate() && $Validation);
        $Validation = ($this->TELEFO->Validate() && $Validation);
        $Validation = ($this->COMPLE->Validate() && $Validation);
        $Validation = ($this->MENSAG->Validate() && $Validation);
        $Validation = ($this->FAX->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->FANTAS->Errors->Count() == 0);
        $Validation =  $Validation && ($this->RAZSOC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->LOGRAD->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DESBAI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DESCID->Errors->Count() == 0);
        $Validation =  $Validation && ($this->ESTADO->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODPOS->Errors->Count() == 0);
        $Validation =  $Validation && ($this->TELEFO->Errors->Count() == 0);
        $Validation =  $Validation && ($this->COMPLE->Errors->Count() == 0);
        $Validation =  $Validation && ($this->MENSAG->Errors->Count() == 0);
        $Validation =  $Validation && ($this->FAX->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @4-C95C9CA9
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->FANTAS->Errors->Count());
        $errors = ($errors || $this->RAZSOC->Errors->Count());
        $errors = ($errors || $this->LOGRAD->Errors->Count());
        $errors = ($errors || $this->DESBAI->Errors->Count());
        $errors = ($errors || $this->DESCID->Errors->Count());
        $errors = ($errors || $this->ESTADO->Errors->Count());
        $errors = ($errors || $this->CODPOS->Errors->Count());
        $errors = ($errors || $this->TELEFO->Errors->Count());
        $errors = ($errors || $this->COMPLE->Errors->Count());
        $errors = ($errors || $this->MENSAG->Errors->Count());
        $errors = ($errors || $this->FAX->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @4-947E3FBA
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->DataSource->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->DataSource->AllParametersSet;
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_Update";
            if($this->Button_Update->Pressed) {
                $this->PressedButton = "Button_Update";
            }
        }
        $Redirect = "CadCadEmp.php" . "?" . CCGetQueryString("QueryString", array("ccsForm"));
        if($this->Validate()) {
            if($this->PressedButton == "Button_Update") {
                if(!CCGetEvent($this->Button_Update->CCSEvents, "OnClick", $this->Button_Update) || !$this->InsertRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
        if ($Redirect)
            $this->DataSource->close();
    }
//End Operation Method

//InsertRow Method @4-08C79F5D
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert", $this);
        if(!$this->InsertAllowed) return false;
        $this->DataSource->FANTAS->SetValue($this->FANTAS->GetValue(true));
        $this->DataSource->RAZSOC->SetValue($this->RAZSOC->GetValue(true));
        $this->DataSource->LOGRAD->SetValue($this->LOGRAD->GetValue(true));
        $this->DataSource->DESBAI->SetValue($this->DESBAI->GetValue(true));
        $this->DataSource->DESCID->SetValue($this->DESCID->GetValue(true));
        $this->DataSource->ESTADO->SetValue($this->ESTADO->GetValue(true));
        $this->DataSource->CODPOS->SetValue($this->CODPOS->GetValue(true));
        $this->DataSource->TELEFO->SetValue($this->TELEFO->GetValue(true));
        $this->DataSource->COMPLE->SetValue($this->COMPLE->GetValue(true));
        $this->DataSource->MENSAG->SetValue($this->MENSAG->GetValue(true));
        $this->DataSource->FAX->SetValue($this->FAX->GetValue(true));
        $this->DataSource->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert", $this);
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//Show Method @4-8878BAFF
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->ESTADO->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->DataSource->Errors->Count()){
                $this->Errors->AddErrors($this->DataSource->Errors);
                $this->DataSource->Errors->clear();
            }
            $this->DataSource->Open();
            if($this->DataSource->Errors->Count() == 0 && $this->DataSource->next_record()) {
                $this->DataSource->SetValues();
                if(!$this->FormSubmitted){
                    $this->FANTAS->SetValue($this->DataSource->FANTAS->GetValue());
                    $this->RAZSOC->SetValue($this->DataSource->RAZSOC->GetValue());
                    $this->LOGRAD->SetValue($this->DataSource->LOGRAD->GetValue());
                    $this->DESBAI->SetValue($this->DataSource->DESBAI->GetValue());
                    $this->DESCID->SetValue($this->DataSource->DESCID->GetValue());
                    $this->ESTADO->SetValue($this->DataSource->ESTADO->GetValue());
                    $this->CODPOS->SetValue($this->DataSource->CODPOS->GetValue());
                    $this->TELEFO->SetValue($this->DataSource->TELEFO->GetValue());
                    $this->COMPLE->SetValue($this->DataSource->COMPLE->GetValue());
                    $this->MENSAG->SetValue($this->DataSource->MENSAG->GetValue());
                    $this->FAX->SetValue($this->DataSource->FAX->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->FANTAS->Errors->ToString());
            $Error = ComposeStrings($Error, $this->RAZSOC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->LOGRAD->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DESBAI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DESCID->Errors->ToString());
            $Error = ComposeStrings($Error, $this->ESTADO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODPOS->Errors->ToString());
            $Error = ComposeStrings($Error, $this->TELEFO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->COMPLE->Errors->ToString());
            $Error = ComposeStrings($Error, $this->MENSAG->Errors->ToString());
            $Error = ComposeStrings($Error, $this->FAX->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataSource->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Update->Visible = !$this->EditMode && $this->InsertAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->FANTAS->Show();
        $this->RAZSOC->Show();
        $this->LOGRAD->Show();
        $this->DESBAI->Show();
        $this->DESCID->Show();
        $this->ESTADO->Show();
        $this->CODPOS->Show();
        $this->TELEFO->Show();
        $this->COMPLE->Show();
        $this->MENSAG->Show();
        $this->FAX->Show();
        $this->Button_Update->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

} //End CADEMP Class @4-FCB6E20C

class clsCADEMPDataSource extends clsDBFaturar {  //CADEMPDataSource Class @4-15AED3F4

//DataSource Variables @4-97FD6DD0
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $InsertParameters;
    public $wp;
    public $AllParametersSet;

    public $InsertFields = array();

    // Datasource fields
    public $FANTAS;
    public $RAZSOC;
    public $LOGRAD;
    public $DESBAI;
    public $DESCID;
    public $ESTADO;
    public $CODPOS;
    public $TELEFO;
    public $COMPLE;
    public $MENSAG;
    public $FAX;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-5AB93549
    function clsCADEMPDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Record CADEMP/Error";
        $this->Initialize();
        $this->FANTAS = new clsField("FANTAS", ccsText, "");
        $this->RAZSOC = new clsField("RAZSOC", ccsText, "");
        $this->LOGRAD = new clsField("LOGRAD", ccsText, "");
        $this->DESBAI = new clsField("DESBAI", ccsText, "");
        $this->DESCID = new clsField("DESCID", ccsText, "");
        $this->ESTADO = new clsField("ESTADO", ccsText, "");
        $this->CODPOS = new clsField("CODPOS", ccsText, "");
        $this->TELEFO = new clsField("TELEFO", ccsText, "");
        $this->COMPLE = new clsField("COMPLE", ccsText, "");
        $this->MENSAG = new clsField("MENSAG", ccsText, "");
        $this->FAX = new clsField("FAX", ccsText, "");

        $this->InsertFields["FANTAS"] = array("Name" => "FANTAS", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["RAZSOC"] = array("Name" => "RAZSOC", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["LOGRAD"] = array("Name" => "LOGRAD", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DESBAI"] = array("Name" => "DESBAI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DESCID"] = array("Name" => "DESCID", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["ESTADO"] = array("Name" => "ESTADO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODPOS"] = array("Name" => "CODPOS", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["TELEFO"] = array("Name" => "TELEFO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["COMPLE"] = array("Name" => "COMPLE", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["MENSAG"] = array("Name" => "MENSAG", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["FAX"] = array("Name" => "FAX", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
    }
//End DataSourceClass_Initialize Event

//Prepare Method @4-77A194F4
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlFANTAS", ccsText, "", "", $this->Parameters["urlFANTAS"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "FANTAS", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @4-7FF30A75
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->SQL = "SELECT * \n\n" .
        "FROM CADEMP {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @4-AFC5A34D
    function SetValues()
    {
        $this->FANTAS->SetDBValue($this->f("FANTAS"));
        $this->RAZSOC->SetDBValue($this->f("RAZSOC"));
        $this->LOGRAD->SetDBValue($this->f("LOGRAD"));
        $this->DESBAI->SetDBValue($this->f("DESBAI"));
        $this->DESCID->SetDBValue($this->f("DESCID"));
        $this->ESTADO->SetDBValue($this->f("ESTADO"));
        $this->CODPOS->SetDBValue($this->f("CODPOS"));
        $this->TELEFO->SetDBValue($this->f("TELEFO"));
        $this->COMPLE->SetDBValue($this->f("COMPLE"));
        $this->MENSAG->SetDBValue($this->f("MENSAG"));
        $this->FAX->SetDBValue($this->f("FAX"));
    }
//End SetValues Method

//Insert Method @4-E36DE493
    function Insert()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert", $this->Parent);
        $this->InsertFields["FANTAS"]["Value"] = $this->FANTAS->GetDBValue(true);
        $this->InsertFields["RAZSOC"]["Value"] = $this->RAZSOC->GetDBValue(true);
        $this->InsertFields["LOGRAD"]["Value"] = $this->LOGRAD->GetDBValue(true);
        $this->InsertFields["DESBAI"]["Value"] = $this->DESBAI->GetDBValue(true);
        $this->InsertFields["DESCID"]["Value"] = $this->DESCID->GetDBValue(true);
        $this->InsertFields["ESTADO"]["Value"] = $this->ESTADO->GetDBValue(true);
        $this->InsertFields["CODPOS"]["Value"] = $this->CODPOS->GetDBValue(true);
        $this->InsertFields["TELEFO"]["Value"] = $this->TELEFO->GetDBValue(true);
        $this->InsertFields["COMPLE"]["Value"] = $this->COMPLE->GetDBValue(true);
        $this->InsertFields["MENSAG"]["Value"] = $this->MENSAG->GetDBValue(true);
        $this->InsertFields["FAX"]["Value"] = $this->FAX->GetDBValue(true);
        $this->SQL = CCBuildInsert("CADEMP", $this->InsertFields, $this);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert", $this->Parent);
        }
    }
//End Insert Method

} //End CADEMPDataSource Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-BD012DED
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "ManutCadEmpIncl.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-7FACF37D
CCSecurityRedirect("1;3", "");
//End Authenticate User

//Include events file @1-0749001E
include("./ManutCadEmpIncl_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-3AC830A2
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$CADEMP = new clsRecordCADEMP("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->CADEMP = & $CADEMP;
$MainPage->rodape = & $rodape;
$CADEMP->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-7CAC53E7
$CADEMP->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-BEDFC20D
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    unset($CADEMP);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-EDA16089
$CADEMP->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-26AC35BE
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
unset($CADEMP);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
