<?php
//BindEvents Method @1-5EACF99D
function BindEvents()
{
    global $CADEMP;
    $CADEMP->CCSEvents["OnValidate"] = "CADEMP_OnValidate";
}
//End BindEvents Method

//CADEMP_OnValidate @4-E1FE84F5
function CADEMP_OnValidate(& $sender)
{
    $CADEMP_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADEMP; //Compatibility
//End CADEMP_OnValidate

//Custom Code @33-2A29BDB7
// -------------------------
    validaTelefone($CADEMP->TELEFO);
	validaTelefone($CADEMP->FAX);
// -------------------------
//End Custom Code

//Close CADEMP_OnValidate @4-2BEBC685
    return $CADEMP_OnValidate;
}
//End Close CADEMP_OnValidate


?>
