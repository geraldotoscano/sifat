<?php
//BindEvents Method @1-E8B00716
function BindEvents()
{
    global $CADEMP;
    global $CCSEvents;
    $CADEMP->CCSEvents["BeforeShow"] = "CADEMP_BeforeShow";
    $CADEMP->CCSEvents["OnValidate"] = "CADEMP_OnValidate";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//CADEMP_BeforeShow @4-910DBE12
function CADEMP_BeforeShow(& $sender)
{
    $CADEMP_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADEMP; //Compatibility
//End CADEMP_BeforeShow

//Custom Code @33-2A29BDB7
// -------------------------
    mascaraTelefone($CADEMP->TELEFO);
	mascaraTelefone($CADEMP->FAX);
// -------------------------
//End Custom Code

//Close CADEMP_BeforeShow @4-1410A20C
    return $CADEMP_BeforeShow;
}
//End Close CADEMP_BeforeShow

//CADEMP_OnValidate @4-E1FE84F5
function CADEMP_OnValidate(& $sender)
{
    $CADEMP_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADEMP; //Compatibility
//End CADEMP_OnValidate

//Custom Code @34-2A29BDB7
// -------------------------
    validaTelefone($CADEMP->TELEFO);
	validaTelefone($CADEMP->FAX);
// -------------------------
//End Custom Code

//Close CADEMP_OnValidate @4-2BEBC685
    return $CADEMP_OnValidate;
}
//End Close CADEMP_OnValidate

//Page_BeforeShow @1-A3069492
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutCadEmp; //Compatibility
//End Page_BeforeShow

//Custom Code @32-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(20);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
