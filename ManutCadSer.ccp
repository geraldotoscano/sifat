<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="4" sourceType="Table" urlType="Relative" secured="False" allowInsert="True" allowUpdate="True" allowDelete="True" validateData="True" preserveParameters="GET" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" connection="Faturar" name="MOVSER" dataSource="MOVSER" errorSummator="Error" wizardCaption="Add/Edit MOVSER " wizardFormMethod="post" pasteAsReplace="pasteAsReplace" removeParameters="CODCLI;IDSERCLI" activeCollection="TableParameters" returnPage="CadSer.ccp">
			<Components>
				<Hidden id="31" fieldSourceType="DBColumn" dataType="Text" name="CODINC" fieldSource="CODINC" required="False" caption="CODINC" wizardCaption="CODINC" wizardSize="3" wizardMaxLength="3" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<Hidden id="32" fieldSourceType="DBColumn" dataType="Text" name="CODALT" fieldSource="CODALT" required="False" caption="CODALT" wizardCaption="CODALT" wizardSize="3" wizardMaxLength="3" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<Hidden id="33" fieldSourceType="DBColumn" dataType="Text" name="DATINC" fieldSource="DATINC" required="False" caption="DATINC" wizardCaption="DATINC" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<Hidden id="34" fieldSourceType="DBColumn" dataType="Text" name="DATALT" fieldSource="DATALT" required="False" caption="DATALT" wizardCaption="DATALT" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<ListBox id="67" visible="Yes" fieldSourceType="DBColumn" sourceType="SQL" dataType="Text" returnValueType="Number" name="CODCLI" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="select
   codcli,
   descli||'('||CODCLI||')' AS descli
from
   cadcli
order by
   descli" textColumn="DESCLI" boundColumn="CODCLI" fieldSource="CODCLI" caption="Cliente">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="68"/>
							</Actions>
						</Event>
					</Events>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<TextBox id="76" fieldSourceType="DBColumn" dataType="Integer" html="False" name="IDSERCLI" fieldSource="IDSERCLI" defaultValue="0" visible="Yes">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<ListBox id="75" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="GRPSER" wizardEmptyCaption="Select Value" caption="Serviço" connection="Faturar" fieldSource="GRPSER" dataSource="GRPSER" boundColumn="GRPSER" textColumn="DESSER">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="81"/>
							</Actions>
						</Event>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="82"/>
							</Actions>
						</Event>
					</Events>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<ListBox id="12" visible="Yes" fieldSourceType="DBColumn" sourceType="SQL" dataType="Text" returnValueType="Number" name="SUBSER" fieldSource="SUBSER" required="True" caption="Sub-Serviço" wizardCaption="SUBSER" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="select
   SUBSER || ' - ' || DESSUB ||', MEDIDO EM '|| UNIDAD AS DESCRICAO,
   SUBSER
from
   SUBSER
order by
   SUBSER" boundColumn="SUBSER" textColumn="DESCRICAO">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="35" eventType="Client"/>
							</Actions>
						</Event>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="38" eventType="Client"/>
							</Actions>
						</Event>
						<Event name="BeforeBuildSelect" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="64" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<Button id="83" urlType="Relative" enableValidation="True" isDefault="False" name="Button1">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<TextBox id="14" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="LOGSER" fieldSource="LOGSER" required="True" caption="Logradouro" wizardCaption="LOGSER" wizardSize="35" wizardMaxLength="35" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="40"/>
							</Actions>
						</Event>
						<Event name="OnValidate" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="70"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="15" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="BAISER" fieldSource="BAISER" required="True" caption="Bairro" wizardCaption="BAISER" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="41"/>
							</Actions>
						</Event>
						<Event name="OnValidate" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="71"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="16" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="CIDSER" fieldSource="CIDSER" required="True" caption="Cidade" wizardCaption="CIDSER" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="42"/>
							</Actions>
						</Event>
						<Event name="OnValidate" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="72"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<ListBox id="17" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" returnValueType="Number" name="ESTSER" fieldSource="ESTSER" required="True" caption="Estado" wizardCaption="ESTSER" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardEmptyCaption="Select Value" dataSource="MG;MINAS GERAIS;AC;ACRE;AL;ALAGOAS;AM;AMAZONAS;AP;AMAPÁ;BA;BAHIA;CE;CEARÁ;DF;DISTRITO FEDERAL;ES;ESPÍRITO SANTO;GO;GOIÁS;MA;MARANHÃO;MS;MATO GROSSO DO SUL;MT;MATO GROSSO;PA;PARÁ;PB;PARAÍBA;PE;PERNAMBUCO;PI;PIAUÍ;PR;PARANÁ;RJ;RIO DE JANEIRO;RN;RIO GRANDE DO NORTE;RO;RONDÔNIA;RR;RORAIMA;RS;RIO GRANDE DO SUL;SC;SANTA CATARINA;SE;SERGIPE;SP;SÃO PAULO;TO;TOCANTINS">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<TextBox id="18" visible="Yes" fieldSourceType="DBColumn" dataType="Float" name="QTDMED" fieldSource="QTDMED" required="True" caption="Medição" wizardCaption="QTDMED" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="65"/>
							</Actions>
						</Event>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="66"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<CheckBox id="77" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" html="True" returnValueType="Number" name="TIPSER" caption="Sim" fieldSource="TIPSER" connection="Faturar" checkedValue="'S'" uncheckedValue="'N'"><Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</CheckBox>
				<CheckBox id="78" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="SERENC" fieldSource="SERENC" checkedValue="'S'" uncheckedValue="'N'"><Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</CheckBox>
				<CheckBox id="79" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="CONTRATO" fieldSource="CONTRATO" checkedValue="'S'" uncheckedValue="'N'"><Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</CheckBox>
				<TextBox id="19" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="DATINI" fieldSource="DATINI" required="True" caption="Validade" wizardCaption="DATINI" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="20" name="DatePicker_DATINI" control="DATINI" wizardSatellite="True" wizardControl="DATINI" wizardDatePickerType="Image" wizardPicture="Styles/Blueprint/Images/DatePicker.gif" style="Styles/Blueprint/Style.css">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
				<TextBox id="21" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="DATFIM" fieldSource="DATFIM" required="False" caption="Validade" wizardCaption="DATFIM" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="22" name="DatePicker_DATFIM" control="DATFIM" wizardSatellite="True" wizardControl="DATFIM" wizardDatePickerType="Image" wizardPicture="Styles/Blueprint/Images/DatePicker.gif" style="Styles/Blueprint/Style.css">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
				<TextBox id="23" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="INIMAN" fieldSource="INIMAN" required="False" caption="Executar" wizardCaption="INIMAN" wizardSize="5" wizardMaxLength="5" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Validate Entry" actionCategory="Validation" id="46" minimumLength="5" maximumLength="5" inputMask="00:00" errorMessage="Dado incorreto."/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="24" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="FIMMAN" fieldSource="FIMMAN" required="False" caption="Executar" wizardCaption="FIMMAN" wizardSize="5" wizardMaxLength="5" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Validate Entry" actionCategory="Validation" id="47" required="True" minimumLength="5" maximumLength="5" inputMask="00:00" errorMessage="Dado Incorreto."/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="25" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="INITAR" fieldSource="INITAR" required="False" caption="Executar" wizardCaption="INITAR" wizardSize="5" wizardMaxLength="5" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Validate Entry" actionCategory="Validation" id="48" required="True" minimumLength="5" maximumLength="5" inputMask="00:00" errorMessage="Dado Incorreto."/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="26" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="FIMTAR" fieldSource="FIMTAR" required="False" caption="Executar" wizardCaption="FIMTAR" wizardSize="5" wizardMaxLength="5" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Validate Entry" actionCategory="Validation" id="49" required="True" minimumLength="5" maximumLength="5" inputMask="00:00" errorMessage="Dado Incorreto."/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="27" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="INIRES" fieldSource="INIRES" required="False" caption="Suspenso" wizardCaption="INIRES" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="28" name="DatePicker_INIRES" control="INIRES" wizardSatellite="True" wizardControl="INIRES" wizardDatePickerType="Image" wizardPicture="Styles/Blueprint/Images/DatePicker.gif" style="Styles/Blueprint/Style.css">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
				<TextBox id="29" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="FIMRES" fieldSource="FIMRES" required="False" caption="Suspenso" wizardCaption="FIMRES" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="30" name="DatePicker_FIMRES" control="FIMRES" wizardSatellite="True" wizardControl="FIMRES" wizardDatePickerType="Image" wizardPicture="Styles/Blueprint/Images/DatePicker.gif" style="Styles/Blueprint/Style.css">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
				<TextArea id="80" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="DESCSER" fieldSource="DESCSER">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextArea>
				<Button id="5" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Insert" operation="Insert" wizardCaption="Adicionar" returnPage="ManutCadSer.ccp">
					<Components/>
					<Events>
						<Event name="OnClick" type="Server">
							<Actions>
								<Action actionName="Declare Variable" actionCategory="General" id="57" name="mCODCLI" type="Text" initialValue="$MOVSER-&gt;CODCLI-&gt;GetValue()"/>
								<Action actionName="Declare Variable" actionCategory="General" id="58" name="mSUBSER" type="Text" initialValue="$MOVSER-&gt;SUBSER-&gt;GetValue()"/>
								<Action actionName="DLookup" actionCategory="Database" id="59" typeOfTarget="Variable" expression="&quot;codcli&quot;" domain="&quot;movser&quot;" criteria="&quot;codcli='$mCODCLI' and subser='$mSUBSER'&quot;" connection="Faturar" target="mCOD_CLI"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="6" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Update" operation="Update" wizardCaption="Mudar">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="55"/>
							</Actions>
						</Event>
						<Event name="OnClick" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="56"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="7" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Delete" operation="Delete" wizardCaption="Apagar">
					<Components/>
					<Events>
						<Event name="OnClick" type="Client">
							<Actions>
								<Action actionName="Confirmation Message" actionCategory="General" id="8" message="Delete record?"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="9" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Cancel" operation="Cancel" wizardCaption="Cancelar">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events>
				<Event name="OnSubmit" type="Client">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="44"/>
					</Actions>
				</Event>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Declare Variable" actionCategory="General" id="50" name="mCODCLI" type="Text" initialValue="$MOVSER-&gt;CODCLI-&gt;GetValue()"/>
						<Action actionName="Declare Variable" actionCategory="General" id="51" name="mSUBSER" type="Text" initialValue="$MOVSER-&gt;SUBSER-&gt;GetValue()"/>
						<Action actionName="DLookup" actionCategory="Database" id="52" typeOfTarget="Variable" expression="&quot;c.codcli&quot;" domain="&quot;cadfat c,movfat m&quot;" criteria="&quot;c.codcli = $mCODCLI and c.codfat = m.codfat and m.subser = $mSUBSER&quot;" connection="Faturar" target="mCOD_CLI"/>
						<Action actionName="Custom Code" actionCategory="General" id="74"/>
					</Actions>
				</Event>
				<Event name="BeforeInsert" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="53"/>
					</Actions>
				</Event>
				<Event name="BeforeUpdate" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="54"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters>
				<TableParameter id="10" conditionType="Parameter" useIsNull="False" field="CODCLI" dataType="Text" logicOperator="And" searchConditionType="Equal" parameterType="URL" orderNumber="1" parameterSource="CODCLI"/>
				<TableParameter id="39" conditionType="Parameter" useIsNull="False" field="IDSERCLI" dataType="Integer" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="IDSERCLI"/>
			</TableParameters>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables>
				<JoinTable id="37" tableName="MOVSER" posLeft="10" posTop="10" posWidth="115" posHeight="180"/>
			</JoinTables>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups>
				<Group id="69" groupID="2" read="True"/>
			</SecurityGroups>
			<Attributes/>
			<Features/>
		</Record>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="ManutCadSer.php" forShow="True" url="ManutCadSer.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="ManutCadSer_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="62" groupID="1"/>
		<Group id="63" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="73"/>
			</Actions>
		</Event>
	</Events>
</Page>
