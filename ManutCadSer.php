<?php
//Include Common Files @1-D6507324
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "ManutCadSer.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordMOVSER { //MOVSER Class @4-BAD8FA78

//Variables @4-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @4-F453ACCE
    function clsRecordMOVSER($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record MOVSER/Error";
        $this->DataSource = new clsMOVSERDataSource($this);
        $this->ds = & $this->DataSource;
        $this->InsertAllowed = true;
        $this->UpdateAllowed = true;
        $this->DeleteAllowed = true;
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "MOVSER";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->CODINC = new clsControl(ccsHidden, "CODINC", "CODINC", ccsText, "", CCGetRequestParam("CODINC", $Method, NULL), $this);
            $this->CODALT = new clsControl(ccsHidden, "CODALT", "CODALT", ccsText, "", CCGetRequestParam("CODALT", $Method, NULL), $this);
            $this->DATINC = new clsControl(ccsHidden, "DATINC", "DATINC", ccsText, "", CCGetRequestParam("DATINC", $Method, NULL), $this);
            $this->DATALT = new clsControl(ccsHidden, "DATALT", "DATALT", ccsText, "", CCGetRequestParam("DATALT", $Method, NULL), $this);
            $this->CODCLI = new clsControl(ccsListBox, "CODCLI", "Cliente", ccsText, "", CCGetRequestParam("CODCLI", $Method, NULL), $this);
            $this->CODCLI->DSType = dsSQL;
            $this->CODCLI->DataSource = new clsDBFaturar();
            $this->CODCLI->ds = & $this->CODCLI->DataSource;
            list($this->CODCLI->BoundColumn, $this->CODCLI->TextColumn, $this->CODCLI->DBFormat) = array("CODCLI", "DESCLI", "");
            $this->CODCLI->DataSource->SQL = "select\n" .
            "   codcli,\n" .
            "   descli||'('||CODCLI||')' AS descli\n" .
            "from\n" .
            "   cadcli {SQL_OrderBy}";
            $this->CODCLI->DataSource->Order = "   descli";
            $this->IDSERCLI = new clsControl(ccsTextBox, "IDSERCLI", "IDSERCLI", ccsInteger, "", CCGetRequestParam("IDSERCLI", $Method, NULL), $this);
            $this->GRPSER = new clsControl(ccsListBox, "GRPSER", "Servi�o", ccsText, "", CCGetRequestParam("GRPSER", $Method, NULL), $this);
            $this->GRPSER->DSType = dsTable;
            $this->GRPSER->DataSource = new clsDBFaturar();
            $this->GRPSER->ds = & $this->GRPSER->DataSource;
            $this->GRPSER->DataSource->SQL = "SELECT * \n" .
"FROM GRPSER {SQL_Where} {SQL_OrderBy}";
            list($this->GRPSER->BoundColumn, $this->GRPSER->TextColumn, $this->GRPSER->DBFormat) = array("GRPSER", "DESSER", "");
            $this->SUBSER = new clsControl(ccsListBox, "SUBSER", "Sub-Servi�o", ccsText, "", CCGetRequestParam("SUBSER", $Method, NULL), $this);
            $this->SUBSER->DSType = dsSQL;
            $this->SUBSER->DataSource = new clsDBFaturar();
            $this->SUBSER->ds = & $this->SUBSER->DataSource;
            list($this->SUBSER->BoundColumn, $this->SUBSER->TextColumn, $this->SUBSER->DBFormat) = array("SUBSER", "DESCRICAO", "");
            $this->SUBSER->DataSource->SQL = "select\n" .
            "   SUBSER || ' - ' || DESSUB ||', MEDIDO EM '|| UNIDAD AS DESCRICAO,\n" .
            "   SUBSER\n" .
            "from\n" .
            "   SUBSER {SQL_OrderBy}";
            $this->SUBSER->DataSource->Order = "   SUBSER";
            $this->SUBSER->Required = true;
            $this->Button1 = new clsButton("Button1", $Method, $this);
            $this->LOGSER = new clsControl(ccsTextBox, "LOGSER", "Logradouro", ccsText, "", CCGetRequestParam("LOGSER", $Method, NULL), $this);
            $this->LOGSER->Required = true;
            $this->BAISER = new clsControl(ccsTextBox, "BAISER", "Bairro", ccsText, "", CCGetRequestParam("BAISER", $Method, NULL), $this);
            $this->BAISER->Required = true;
            $this->CIDSER = new clsControl(ccsTextBox, "CIDSER", "Cidade", ccsText, "", CCGetRequestParam("CIDSER", $Method, NULL), $this);
            $this->CIDSER->Required = true;
            $this->ESTSER = new clsControl(ccsListBox, "ESTSER", "Estado", ccsText, "", CCGetRequestParam("ESTSER", $Method, NULL), $this);
            $this->ESTSER->DSType = dsListOfValues;
            $this->ESTSER->Values = array(array("MG", "MINAS GERAIS"), array("AC", "ACRE"), array("AL", "ALAGOAS"), array("AM", "AMAZONAS"), array("AP", "AMAP�"), array("BA", "BAHIA"), array("CE", "CEAR�"), array("DF", "DISTRITO FEDERAL"), array("ES", "ESP�RITO SANTO"), array("GO", "GOI�S"), array("MA", "MARANH�O"), array("MS", "MATO GROSSO DO SUL"), array("MT", "MATO GROSSO"), array("PA", "PAR�"), array("PB", "PARA�BA"), array("PE", "PERNAMBUCO"), array("PI", "PIAU�"), array("PR", "PARAN�"), array("RJ", "RIO DE JANEIRO"), array("RN", "RIO GRANDE DO NORTE"), array("RO", "ROND�NIA"), array("RR", "RORAIMA"), array("RS", "RIO GRANDE DO SUL"), array("SC", "SANTA CATARINA"), array("SE", "SERGIPE"), array("SP", "S�O PAULO"), array("TO", "TOCANTINS"));
            $this->ESTSER->Required = true;
            $this->QTDMED = new clsControl(ccsTextBox, "QTDMED", "Medi��o", ccsFloat, "", CCGetRequestParam("QTDMED", $Method, NULL), $this);
            $this->QTDMED->Required = true;
            $this->TIPSER = new clsControl(ccsCheckBox, "TIPSER", "Sim", ccsText, "", CCGetRequestParam("TIPSER", $Method, NULL), $this);
            $this->TIPSER->HTML = true;
            $this->TIPSER->CheckedValue = $this->TIPSER->GetParsedValue('S');
            $this->TIPSER->UncheckedValue = $this->TIPSER->GetParsedValue('N');
            $this->SERENC = new clsControl(ccsCheckBox, "SERENC", "SERENC", ccsText, "", CCGetRequestParam("SERENC", $Method, NULL), $this);
            $this->SERENC->CheckedValue = $this->SERENC->GetParsedValue('S');
            $this->SERENC->UncheckedValue = $this->SERENC->GetParsedValue('N');
            $this->CONTRATO = new clsControl(ccsCheckBox, "CONTRATO", "CONTRATO", ccsText, "", CCGetRequestParam("CONTRATO", $Method, NULL), $this);
            $this->CONTRATO->CheckedValue = $this->CONTRATO->GetParsedValue('S');
            $this->CONTRATO->UncheckedValue = $this->CONTRATO->GetParsedValue('N');
            $this->DATINI = new clsControl(ccsTextBox, "DATINI", "Validade", ccsDate, $DefaultDateFormat, CCGetRequestParam("DATINI", $Method, NULL), $this);
            $this->DATINI->Required = true;
            $this->DatePicker_DATINI = new clsDatePicker("DatePicker_DATINI", "MOVSER", "DATINI", $this);
            $this->DATFIM = new clsControl(ccsTextBox, "DATFIM", "Validade", ccsDate, $DefaultDateFormat, CCGetRequestParam("DATFIM", $Method, NULL), $this);
            $this->DatePicker_DATFIM = new clsDatePicker("DatePicker_DATFIM", "MOVSER", "DATFIM", $this);
            $this->INIMAN = new clsControl(ccsTextBox, "INIMAN", "Executar", ccsText, "", CCGetRequestParam("INIMAN", $Method, NULL), $this);
            $this->FIMMAN = new clsControl(ccsTextBox, "FIMMAN", "Executar", ccsText, "", CCGetRequestParam("FIMMAN", $Method, NULL), $this);
            $this->INITAR = new clsControl(ccsTextBox, "INITAR", "Executar", ccsText, "", CCGetRequestParam("INITAR", $Method, NULL), $this);
            $this->FIMTAR = new clsControl(ccsTextBox, "FIMTAR", "Executar", ccsText, "", CCGetRequestParam("FIMTAR", $Method, NULL), $this);
            $this->INIRES = new clsControl(ccsTextBox, "INIRES", "Suspenso", ccsDate, $DefaultDateFormat, CCGetRequestParam("INIRES", $Method, NULL), $this);
            $this->DatePicker_INIRES = new clsDatePicker("DatePicker_INIRES", "MOVSER", "INIRES", $this);
            $this->FIMRES = new clsControl(ccsTextBox, "FIMRES", "Suspenso", ccsDate, $DefaultDateFormat, CCGetRequestParam("FIMRES", $Method, NULL), $this);
            $this->DatePicker_FIMRES = new clsDatePicker("DatePicker_FIMRES", "MOVSER", "FIMRES", $this);
            $this->DESCSER = new clsControl(ccsTextArea, "DESCSER", "DESCSER", ccsText, "", CCGetRequestParam("DESCSER", $Method, NULL), $this);
            $this->Button_Insert = new clsButton("Button_Insert", $Method, $this);
            $this->Button_Update = new clsButton("Button_Update", $Method, $this);
            $this->Button_Delete = new clsButton("Button_Delete", $Method, $this);
            $this->Button_Cancel = new clsButton("Button_Cancel", $Method, $this);
            if(!$this->FormSubmitted) {
                if(!is_array($this->IDSERCLI->Value) && !strlen($this->IDSERCLI->Value) && $this->IDSERCLI->Value !== false)
                    $this->IDSERCLI->SetText(0);
            }
        }
    }
//End Class_Initialize Event

//Initialize Method @4-EB0F6EAB
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->DataSource->Parameters["urlCODCLI"] = CCGetFromGet("CODCLI", NULL);
        $this->DataSource->Parameters["urlIDSERCLI"] = CCGetFromGet("IDSERCLI", NULL);
    }
//End Initialize Method

//Validate Method @4-8D8A5AF2
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->CODINC->Validate() && $Validation);
        $Validation = ($this->CODALT->Validate() && $Validation);
        $Validation = ($this->DATINC->Validate() && $Validation);
        $Validation = ($this->DATALT->Validate() && $Validation);
        $Validation = ($this->CODCLI->Validate() && $Validation);
        $Validation = ($this->IDSERCLI->Validate() && $Validation);
        $Validation = ($this->GRPSER->Validate() && $Validation);
        $Validation = ($this->SUBSER->Validate() && $Validation);
        $Validation = ($this->LOGSER->Validate() && $Validation);
        $Validation = ($this->BAISER->Validate() && $Validation);
        $Validation = ($this->CIDSER->Validate() && $Validation);
        $Validation = ($this->ESTSER->Validate() && $Validation);
        $Validation = ($this->QTDMED->Validate() && $Validation);
        $Validation = ($this->TIPSER->Validate() && $Validation);
        $Validation = ($this->SERENC->Validate() && $Validation);
        $Validation = ($this->CONTRATO->Validate() && $Validation);
        $Validation = ($this->DATINI->Validate() && $Validation);
        $Validation = ($this->DATFIM->Validate() && $Validation);
        $Validation = ($this->INIMAN->Validate() && $Validation);
        $Validation = ($this->FIMMAN->Validate() && $Validation);
        $Validation = ($this->INITAR->Validate() && $Validation);
        $Validation = ($this->FIMTAR->Validate() && $Validation);
        $Validation = ($this->INIRES->Validate() && $Validation);
        $Validation = ($this->FIMRES->Validate() && $Validation);
        $Validation = ($this->DESCSER->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->CODINC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODALT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DATINC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DATALT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->IDSERCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->GRPSER->Errors->Count() == 0);
        $Validation =  $Validation && ($this->SUBSER->Errors->Count() == 0);
        $Validation =  $Validation && ($this->LOGSER->Errors->Count() == 0);
        $Validation =  $Validation && ($this->BAISER->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CIDSER->Errors->Count() == 0);
        $Validation =  $Validation && ($this->ESTSER->Errors->Count() == 0);
        $Validation =  $Validation && ($this->QTDMED->Errors->Count() == 0);
        $Validation =  $Validation && ($this->TIPSER->Errors->Count() == 0);
        $Validation =  $Validation && ($this->SERENC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CONTRATO->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DATINI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DATFIM->Errors->Count() == 0);
        $Validation =  $Validation && ($this->INIMAN->Errors->Count() == 0);
        $Validation =  $Validation && ($this->FIMMAN->Errors->Count() == 0);
        $Validation =  $Validation && ($this->INITAR->Errors->Count() == 0);
        $Validation =  $Validation && ($this->FIMTAR->Errors->Count() == 0);
        $Validation =  $Validation && ($this->INIRES->Errors->Count() == 0);
        $Validation =  $Validation && ($this->FIMRES->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DESCSER->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @4-5D68CF7A
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->CODINC->Errors->Count());
        $errors = ($errors || $this->CODALT->Errors->Count());
        $errors = ($errors || $this->DATINC->Errors->Count());
        $errors = ($errors || $this->DATALT->Errors->Count());
        $errors = ($errors || $this->CODCLI->Errors->Count());
        $errors = ($errors || $this->IDSERCLI->Errors->Count());
        $errors = ($errors || $this->GRPSER->Errors->Count());
        $errors = ($errors || $this->SUBSER->Errors->Count());
        $errors = ($errors || $this->LOGSER->Errors->Count());
        $errors = ($errors || $this->BAISER->Errors->Count());
        $errors = ($errors || $this->CIDSER->Errors->Count());
        $errors = ($errors || $this->ESTSER->Errors->Count());
        $errors = ($errors || $this->QTDMED->Errors->Count());
        $errors = ($errors || $this->TIPSER->Errors->Count());
        $errors = ($errors || $this->SERENC->Errors->Count());
        $errors = ($errors || $this->CONTRATO->Errors->Count());
        $errors = ($errors || $this->DATINI->Errors->Count());
        $errors = ($errors || $this->DatePicker_DATINI->Errors->Count());
        $errors = ($errors || $this->DATFIM->Errors->Count());
        $errors = ($errors || $this->DatePicker_DATFIM->Errors->Count());
        $errors = ($errors || $this->INIMAN->Errors->Count());
        $errors = ($errors || $this->FIMMAN->Errors->Count());
        $errors = ($errors || $this->INITAR->Errors->Count());
        $errors = ($errors || $this->FIMTAR->Errors->Count());
        $errors = ($errors || $this->INIRES->Errors->Count());
        $errors = ($errors || $this->DatePicker_INIRES->Errors->Count());
        $errors = ($errors || $this->FIMRES->Errors->Count());
        $errors = ($errors || $this->DatePicker_FIMRES->Errors->Count());
        $errors = ($errors || $this->DESCSER->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @4-ADA97F6C
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->DataSource->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->DataSource->AllParametersSet;
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = $this->EditMode ? "Button_Update" : "Button_Insert";
            if($this->Button1->Pressed) {
                $this->PressedButton = "Button1";
            } else if($this->Button_Insert->Pressed) {
                $this->PressedButton = "Button_Insert";
            } else if($this->Button_Update->Pressed) {
                $this->PressedButton = "Button_Update";
            } else if($this->Button_Delete->Pressed) {
                $this->PressedButton = "Button_Delete";
            } else if($this->Button_Cancel->Pressed) {
                $this->PressedButton = "Button_Cancel";
            }
        }
        $Redirect = "CadSer.php" . "?" . CCGetQueryString("QueryString", array("ccsForm", "CODCLI", "IDSERCLI"));
        if($this->PressedButton == "Button_Delete") {
            if(!CCGetEvent($this->Button_Delete->CCSEvents, "OnClick", $this->Button_Delete) || !$this->DeleteRow()) {
                $Redirect = "";
            }
        } else if($this->PressedButton == "Button_Cancel") {
            if(!CCGetEvent($this->Button_Cancel->CCSEvents, "OnClick", $this->Button_Cancel)) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button1") {
                if(!CCGetEvent($this->Button1->CCSEvents, "OnClick", $this->Button1)) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "Button_Insert") {
                $Redirect = "ManutCadSer.php" . "?" . CCGetQueryString("QueryString", array("ccsForm", "CODCLI", "IDSERCLI"));
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick", $this->Button_Insert) || !$this->InsertRow()) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "Button_Update") {
                if(!CCGetEvent($this->Button_Update->CCSEvents, "OnClick", $this->Button_Update) || !$this->UpdateRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
        if ($Redirect)
            $this->DataSource->close();
    }
//End Operation Method

//InsertRow Method @4-47866B1F
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert", $this);
        if(!$this->InsertAllowed) return false;
        $this->DataSource->CODINC->SetValue($this->CODINC->GetValue(true));
        $this->DataSource->CODALT->SetValue($this->CODALT->GetValue(true));
        $this->DataSource->DATINC->SetValue($this->DATINC->GetValue(true));
        $this->DataSource->DATALT->SetValue($this->DATALT->GetValue(true));
        $this->DataSource->CODCLI->SetValue($this->CODCLI->GetValue(true));
        $this->DataSource->IDSERCLI->SetValue($this->IDSERCLI->GetValue(true));
        $this->DataSource->GRPSER->SetValue($this->GRPSER->GetValue(true));
        $this->DataSource->SUBSER->SetValue($this->SUBSER->GetValue(true));
        $this->DataSource->LOGSER->SetValue($this->LOGSER->GetValue(true));
        $this->DataSource->BAISER->SetValue($this->BAISER->GetValue(true));
        $this->DataSource->CIDSER->SetValue($this->CIDSER->GetValue(true));
        $this->DataSource->ESTSER->SetValue($this->ESTSER->GetValue(true));
        $this->DataSource->QTDMED->SetValue($this->QTDMED->GetValue(true));
        $this->DataSource->TIPSER->SetValue($this->TIPSER->GetValue(true));
        $this->DataSource->SERENC->SetValue($this->SERENC->GetValue(true));
        $this->DataSource->CONTRATO->SetValue($this->CONTRATO->GetValue(true));
        $this->DataSource->DATINI->SetValue($this->DATINI->GetValue(true));
        $this->DataSource->DATFIM->SetValue($this->DATFIM->GetValue(true));
        $this->DataSource->INIMAN->SetValue($this->INIMAN->GetValue(true));
        $this->DataSource->FIMMAN->SetValue($this->FIMMAN->GetValue(true));
        $this->DataSource->INITAR->SetValue($this->INITAR->GetValue(true));
        $this->DataSource->FIMTAR->SetValue($this->FIMTAR->GetValue(true));
        $this->DataSource->INIRES->SetValue($this->INIRES->GetValue(true));
        $this->DataSource->FIMRES->SetValue($this->FIMRES->GetValue(true));
        $this->DataSource->DESCSER->SetValue($this->DESCSER->GetValue(true));
        $this->DataSource->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert", $this);
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//UpdateRow Method @4-DEA43FF2
    function UpdateRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeUpdate", $this);
        if(!$this->UpdateAllowed) return false;
        $this->DataSource->CODINC->SetValue($this->CODINC->GetValue(true));
        $this->DataSource->CODALT->SetValue($this->CODALT->GetValue(true));
        $this->DataSource->DATINC->SetValue($this->DATINC->GetValue(true));
        $this->DataSource->DATALT->SetValue($this->DATALT->GetValue(true));
        $this->DataSource->CODCLI->SetValue($this->CODCLI->GetValue(true));
        $this->DataSource->IDSERCLI->SetValue($this->IDSERCLI->GetValue(true));
        $this->DataSource->GRPSER->SetValue($this->GRPSER->GetValue(true));
        $this->DataSource->SUBSER->SetValue($this->SUBSER->GetValue(true));
        $this->DataSource->LOGSER->SetValue($this->LOGSER->GetValue(true));
        $this->DataSource->BAISER->SetValue($this->BAISER->GetValue(true));
        $this->DataSource->CIDSER->SetValue($this->CIDSER->GetValue(true));
        $this->DataSource->ESTSER->SetValue($this->ESTSER->GetValue(true));
        $this->DataSource->QTDMED->SetValue($this->QTDMED->GetValue(true));
        $this->DataSource->TIPSER->SetValue($this->TIPSER->GetValue(true));
        $this->DataSource->SERENC->SetValue($this->SERENC->GetValue(true));
        $this->DataSource->CONTRATO->SetValue($this->CONTRATO->GetValue(true));
        $this->DataSource->DATINI->SetValue($this->DATINI->GetValue(true));
        $this->DataSource->DATFIM->SetValue($this->DATFIM->GetValue(true));
        $this->DataSource->INIMAN->SetValue($this->INIMAN->GetValue(true));
        $this->DataSource->FIMMAN->SetValue($this->FIMMAN->GetValue(true));
        $this->DataSource->INITAR->SetValue($this->INITAR->GetValue(true));
        $this->DataSource->FIMTAR->SetValue($this->FIMTAR->GetValue(true));
        $this->DataSource->INIRES->SetValue($this->INIRES->GetValue(true));
        $this->DataSource->FIMRES->SetValue($this->FIMRES->GetValue(true));
        $this->DataSource->DESCSER->SetValue($this->DESCSER->GetValue(true));
        $this->DataSource->Update();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterUpdate", $this);
        return (!$this->CheckErrors());
    }
//End UpdateRow Method

//DeleteRow Method @4-299D98C3
    function DeleteRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDelete", $this);
        if(!$this->DeleteAllowed) return false;
        $this->DataSource->Delete();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDelete", $this);
        return (!$this->CheckErrors());
    }
//End DeleteRow Method

//Show Method @4-61B6205D
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->CODCLI->Prepare();
        $this->GRPSER->Prepare();
        $this->SUBSER->Prepare();
        $this->ESTSER->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->DataSource->Errors->Count()){
                $this->Errors->AddErrors($this->DataSource->Errors);
                $this->DataSource->Errors->clear();
            }
            $this->DataSource->Open();
            if($this->DataSource->Errors->Count() == 0 && $this->DataSource->next_record()) {
                $this->DataSource->SetValues();
                if(!$this->FormSubmitted){
                    $this->CODINC->SetValue($this->DataSource->CODINC->GetValue());
                    $this->CODALT->SetValue($this->DataSource->CODALT->GetValue());
                    $this->DATINC->SetValue($this->DataSource->DATINC->GetValue());
                    $this->DATALT->SetValue($this->DataSource->DATALT->GetValue());
                    $this->CODCLI->SetValue($this->DataSource->CODCLI->GetValue());
                    $this->IDSERCLI->SetValue($this->DataSource->IDSERCLI->GetValue());
                    $this->GRPSER->SetValue($this->DataSource->GRPSER->GetValue());
                    $this->SUBSER->SetValue($this->DataSource->SUBSER->GetValue());
                    $this->LOGSER->SetValue($this->DataSource->LOGSER->GetValue());
                    $this->BAISER->SetValue($this->DataSource->BAISER->GetValue());
                    $this->CIDSER->SetValue($this->DataSource->CIDSER->GetValue());
                    $this->ESTSER->SetValue($this->DataSource->ESTSER->GetValue());
                    $this->QTDMED->SetValue($this->DataSource->QTDMED->GetValue());
                    $this->TIPSER->SetValue($this->DataSource->TIPSER->GetValue());
                    $this->SERENC->SetValue($this->DataSource->SERENC->GetValue());
                    $this->CONTRATO->SetValue($this->DataSource->CONTRATO->GetValue());
                    $this->DATINI->SetValue($this->DataSource->DATINI->GetValue());
                    $this->DATFIM->SetValue($this->DataSource->DATFIM->GetValue());
                    $this->INIMAN->SetValue($this->DataSource->INIMAN->GetValue());
                    $this->FIMMAN->SetValue($this->DataSource->FIMMAN->GetValue());
                    $this->INITAR->SetValue($this->DataSource->INITAR->GetValue());
                    $this->FIMTAR->SetValue($this->DataSource->FIMTAR->GetValue());
                    $this->INIRES->SetValue($this->DataSource->INIRES->GetValue());
                    $this->FIMRES->SetValue($this->DataSource->FIMRES->GetValue());
                    $this->DESCSER->SetValue($this->DataSource->DESCSER->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->CODINC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODALT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DATINC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DATALT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->IDSERCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->GRPSER->Errors->ToString());
            $Error = ComposeStrings($Error, $this->SUBSER->Errors->ToString());
            $Error = ComposeStrings($Error, $this->LOGSER->Errors->ToString());
            $Error = ComposeStrings($Error, $this->BAISER->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CIDSER->Errors->ToString());
            $Error = ComposeStrings($Error, $this->ESTSER->Errors->ToString());
            $Error = ComposeStrings($Error, $this->QTDMED->Errors->ToString());
            $Error = ComposeStrings($Error, $this->TIPSER->Errors->ToString());
            $Error = ComposeStrings($Error, $this->SERENC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CONTRATO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DATINI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DatePicker_DATINI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DATFIM->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DatePicker_DATFIM->Errors->ToString());
            $Error = ComposeStrings($Error, $this->INIMAN->Errors->ToString());
            $Error = ComposeStrings($Error, $this->FIMMAN->Errors->ToString());
            $Error = ComposeStrings($Error, $this->INITAR->Errors->ToString());
            $Error = ComposeStrings($Error, $this->FIMTAR->Errors->ToString());
            $Error = ComposeStrings($Error, $this->INIRES->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DatePicker_INIRES->Errors->ToString());
            $Error = ComposeStrings($Error, $this->FIMRES->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DatePicker_FIMRES->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DESCSER->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataSource->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Insert->Visible = !$this->EditMode && $this->InsertAllowed;
        $this->Button_Update->Visible = $this->EditMode && $this->UpdateAllowed;
        $this->Button_Delete->Visible = $this->EditMode && $this->DeleteAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->CODINC->Show();
        $this->CODALT->Show();
        $this->DATINC->Show();
        $this->DATALT->Show();
        $this->CODCLI->Show();
        $this->IDSERCLI->Show();
        $this->GRPSER->Show();
        $this->SUBSER->Show();
        $this->Button1->Show();
        $this->LOGSER->Show();
        $this->BAISER->Show();
        $this->CIDSER->Show();
        $this->ESTSER->Show();
        $this->QTDMED->Show();
        $this->TIPSER->Show();
        $this->SERENC->Show();
        $this->CONTRATO->Show();
        $this->DATINI->Show();
        $this->DatePicker_DATINI->Show();
        $this->DATFIM->Show();
        $this->DatePicker_DATFIM->Show();
        $this->INIMAN->Show();
        $this->FIMMAN->Show();
        $this->INITAR->Show();
        $this->FIMTAR->Show();
        $this->INIRES->Show();
        $this->DatePicker_INIRES->Show();
        $this->FIMRES->Show();
        $this->DatePicker_FIMRES->Show();
        $this->DESCSER->Show();
        $this->Button_Insert->Show();
        $this->Button_Update->Show();
        $this->Button_Delete->Show();
        $this->Button_Cancel->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

} //End MOVSER Class @4-FCB6E20C

class clsMOVSERDataSource extends clsDBFaturar {  //MOVSERDataSource Class @4-57D171F1

//DataSource Variables @4-DABCCEAC
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $InsertParameters;
    public $UpdateParameters;
    public $DeleteParameters;
    public $wp;
    public $AllParametersSet;

    public $InsertFields = array();
    public $UpdateFields = array();

    // Datasource fields
    public $CODINC;
    public $CODALT;
    public $DATINC;
    public $DATALT;
    public $CODCLI;
    public $IDSERCLI;
    public $GRPSER;
    public $SUBSER;
    public $LOGSER;
    public $BAISER;
    public $CIDSER;
    public $ESTSER;
    public $QTDMED;
    public $TIPSER;
    public $SERENC;
    public $CONTRATO;
    public $DATINI;
    public $DATFIM;
    public $INIMAN;
    public $FIMMAN;
    public $INITAR;
    public $FIMTAR;
    public $INIRES;
    public $FIMRES;
    public $DESCSER;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-FCEFA92F
    function clsMOVSERDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Record MOVSER/Error";
        $this->Initialize();
        $this->CODINC = new clsField("CODINC", ccsText, "");
        $this->CODALT = new clsField("CODALT", ccsText, "");
        $this->DATINC = new clsField("DATINC", ccsText, "");
        $this->DATALT = new clsField("DATALT", ccsText, "");
        $this->CODCLI = new clsField("CODCLI", ccsText, "");
        $this->IDSERCLI = new clsField("IDSERCLI", ccsInteger, "");
        $this->GRPSER = new clsField("GRPSER", ccsText, "");
        $this->SUBSER = new clsField("SUBSER", ccsText, "");
        $this->LOGSER = new clsField("LOGSER", ccsText, "");
        $this->BAISER = new clsField("BAISER", ccsText, "");
        $this->CIDSER = new clsField("CIDSER", ccsText, "");
        $this->ESTSER = new clsField("ESTSER", ccsText, "");
        $this->QTDMED = new clsField("QTDMED", ccsFloat, "");
        $this->TIPSER = new clsField("TIPSER", ccsText, "");
        $this->SERENC = new clsField("SERENC", ccsText, "");
        $this->CONTRATO = new clsField("CONTRATO", ccsText, "");
        $this->DATINI = new clsField("DATINI", ccsDate, $this->DateFormat);
        $this->DATFIM = new clsField("DATFIM", ccsDate, $this->DateFormat);
        $this->INIMAN = new clsField("INIMAN", ccsText, "");
        $this->FIMMAN = new clsField("FIMMAN", ccsText, "");
        $this->INITAR = new clsField("INITAR", ccsText, "");
        $this->FIMTAR = new clsField("FIMTAR", ccsText, "");
        $this->INIRES = new clsField("INIRES", ccsDate, $this->DateFormat);
        $this->FIMRES = new clsField("FIMRES", ccsDate, $this->DateFormat);
        $this->DESCSER = new clsField("DESCSER", ccsText, "");

        $this->InsertFields["CODINC"] = array("Name" => "CODINC", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODALT"] = array("Name" => "CODALT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DATINC"] = array("Name" => "DATINC", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DATALT"] = array("Name" => "DATALT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODCLI"] = array("Name" => "CODCLI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["IDSERCLI"] = array("Name" => "IDSERCLI", "Value" => "", "DataType" => ccsInteger, "OmitIfEmpty" => 1);
        $this->InsertFields["GRPSER"] = array("Name" => "GRPSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["SUBSER"] = array("Name" => "SUBSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["LOGSER"] = array("Name" => "LOGSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["BAISER"] = array("Name" => "BAISER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CIDSER"] = array("Name" => "CIDSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["ESTSER"] = array("Name" => "ESTSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["QTDMED"] = array("Name" => "QTDMED", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->InsertFields["TIPSER"] = array("Name" => "TIPSER", "Value" => "", "DataType" => ccsText);
        $this->InsertFields["SERENC"] = array("Name" => "SERENC", "Value" => "", "DataType" => ccsText);
        $this->InsertFields["CONTRATO"] = array("Name" => "CONTRATO", "Value" => "", "DataType" => ccsText);
        $this->InsertFields["DATINI"] = array("Name" => "DATINI", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->InsertFields["DATFIM"] = array("Name" => "DATFIM", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->InsertFields["INIMAN"] = array("Name" => "INIMAN", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["FIMMAN"] = array("Name" => "FIMMAN", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["INITAR"] = array("Name" => "INITAR", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["FIMTAR"] = array("Name" => "FIMTAR", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["INIRES"] = array("Name" => "INIRES", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->InsertFields["FIMRES"] = array("Name" => "FIMRES", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->InsertFields["DESCSER"] = array("Name" => "DESCSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODINC"] = array("Name" => "CODINC", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODALT"] = array("Name" => "CODALT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["DATINC"] = array("Name" => "DATINC", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["DATALT"] = array("Name" => "DATALT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODCLI"] = array("Name" => "CODCLI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["IDSERCLI"] = array("Name" => "IDSERCLI", "Value" => "", "DataType" => ccsInteger, "OmitIfEmpty" => 1);
        $this->UpdateFields["GRPSER"] = array("Name" => "GRPSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["SUBSER"] = array("Name" => "SUBSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["LOGSER"] = array("Name" => "LOGSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["BAISER"] = array("Name" => "BAISER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CIDSER"] = array("Name" => "CIDSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["ESTSER"] = array("Name" => "ESTSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["QTDMED"] = array("Name" => "QTDMED", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->UpdateFields["TIPSER"] = array("Name" => "TIPSER", "Value" => "", "DataType" => ccsText);
        $this->UpdateFields["SERENC"] = array("Name" => "SERENC", "Value" => "", "DataType" => ccsText);
        $this->UpdateFields["CONTRATO"] = array("Name" => "CONTRATO", "Value" => "", "DataType" => ccsText);
        $this->UpdateFields["DATINI"] = array("Name" => "DATINI", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->UpdateFields["DATFIM"] = array("Name" => "DATFIM", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->UpdateFields["INIMAN"] = array("Name" => "INIMAN", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["FIMMAN"] = array("Name" => "FIMMAN", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["INITAR"] = array("Name" => "INITAR", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["FIMTAR"] = array("Name" => "FIMTAR", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["INIRES"] = array("Name" => "INIRES", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->UpdateFields["FIMRES"] = array("Name" => "FIMRES", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->UpdateFields["DESCSER"] = array("Name" => "DESCSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
    }
//End DataSourceClass_Initialize Event

//Prepare Method @4-BD1F4E36
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlCODCLI", ccsText, "", "", $this->Parameters["urlCODCLI"], "", false);
        $this->wp->AddParameter("2", "urlIDSERCLI", ccsInteger, "", "", $this->Parameters["urlIDSERCLI"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "CODCLI", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opEqual, "IDSERCLI", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsInteger),false);
        $this->Where = $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]);
    }
//End Prepare Method

//Open Method @4-D134D424
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->SQL = "SELECT * \n\n" .
        "FROM MOVSER {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @4-9877221E
    function SetValues()
    {
        $this->CODINC->SetDBValue($this->f("CODINC"));
        $this->CODALT->SetDBValue($this->f("CODALT"));
        $this->DATINC->SetDBValue($this->f("DATINC"));
        $this->DATALT->SetDBValue($this->f("DATALT"));
        $this->CODCLI->SetDBValue($this->f("CODCLI"));
        $this->IDSERCLI->SetDBValue(trim($this->f("IDSERCLI")));
        $this->GRPSER->SetDBValue($this->f("GRPSER"));
        $this->SUBSER->SetDBValue($this->f("SUBSER"));
        $this->LOGSER->SetDBValue($this->f("LOGSER"));
        $this->BAISER->SetDBValue($this->f("BAISER"));
        $this->CIDSER->SetDBValue($this->f("CIDSER"));
        $this->ESTSER->SetDBValue($this->f("ESTSER"));
        $this->QTDMED->SetDBValue(trim($this->f("QTDMED")));
        $this->TIPSER->SetDBValue($this->f("TIPSER"));
        $this->SERENC->SetDBValue($this->f("SERENC"));
        $this->CONTRATO->SetDBValue($this->f("CONTRATO"));
        $this->DATINI->SetDBValue(trim($this->f("DATINI")));
        $this->DATFIM->SetDBValue(trim($this->f("DATFIM")));
        $this->INIMAN->SetDBValue($this->f("INIMAN"));
        $this->FIMMAN->SetDBValue($this->f("FIMMAN"));
        $this->INITAR->SetDBValue($this->f("INITAR"));
        $this->FIMTAR->SetDBValue($this->f("FIMTAR"));
        $this->INIRES->SetDBValue(trim($this->f("INIRES")));
        $this->FIMRES->SetDBValue(trim($this->f("FIMRES")));
        $this->DESCSER->SetDBValue($this->f("DESCSER"));
    }
//End SetValues Method

//Insert Method @4-2C9F7F7C
    function Insert()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert", $this->Parent);
        $this->InsertFields["CODINC"]["Value"] = $this->CODINC->GetDBValue(true);
        $this->InsertFields["CODALT"]["Value"] = $this->CODALT->GetDBValue(true);
        $this->InsertFields["DATINC"]["Value"] = $this->DATINC->GetDBValue(true);
        $this->InsertFields["DATALT"]["Value"] = $this->DATALT->GetDBValue(true);
        $this->InsertFields["CODCLI"]["Value"] = $this->CODCLI->GetDBValue(true);
        $this->InsertFields["IDSERCLI"]["Value"] = $this->IDSERCLI->GetDBValue(true);
        $this->InsertFields["GRPSER"]["Value"] = $this->GRPSER->GetDBValue(true);
        $this->InsertFields["SUBSER"]["Value"] = $this->SUBSER->GetDBValue(true);
        $this->InsertFields["LOGSER"]["Value"] = $this->LOGSER->GetDBValue(true);
        $this->InsertFields["BAISER"]["Value"] = $this->BAISER->GetDBValue(true);
        $this->InsertFields["CIDSER"]["Value"] = $this->CIDSER->GetDBValue(true);
        $this->InsertFields["ESTSER"]["Value"] = $this->ESTSER->GetDBValue(true);
        $this->InsertFields["QTDMED"]["Value"] = $this->QTDMED->GetDBValue(true);
        $this->InsertFields["TIPSER"]["Value"] = $this->TIPSER->GetDBValue(true);
        $this->InsertFields["SERENC"]["Value"] = $this->SERENC->GetDBValue(true);
        $this->InsertFields["CONTRATO"]["Value"] = $this->CONTRATO->GetDBValue(true);
        $this->InsertFields["DATINI"]["Value"] = $this->DATINI->GetDBValue(true);
        $this->InsertFields["DATFIM"]["Value"] = $this->DATFIM->GetDBValue(true);
        $this->InsertFields["INIMAN"]["Value"] = $this->INIMAN->GetDBValue(true);
        $this->InsertFields["FIMMAN"]["Value"] = $this->FIMMAN->GetDBValue(true);
        $this->InsertFields["INITAR"]["Value"] = $this->INITAR->GetDBValue(true);
        $this->InsertFields["FIMTAR"]["Value"] = $this->FIMTAR->GetDBValue(true);
        $this->InsertFields["INIRES"]["Value"] = $this->INIRES->GetDBValue(true);
        $this->InsertFields["FIMRES"]["Value"] = $this->FIMRES->GetDBValue(true);
        $this->InsertFields["DESCSER"]["Value"] = $this->DESCSER->GetDBValue(true);
        $this->SQL = CCBuildInsert("MOVSER", $this->InsertFields, $this);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert", $this->Parent);
        }
    }
//End Insert Method

//Update Method @4-3BB56040
    function Update()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildUpdate", $this->Parent);
        $this->UpdateFields["CODINC"]["Value"] = $this->CODINC->GetDBValue(true);
        $this->UpdateFields["CODALT"]["Value"] = $this->CODALT->GetDBValue(true);
        $this->UpdateFields["DATINC"]["Value"] = $this->DATINC->GetDBValue(true);
        $this->UpdateFields["DATALT"]["Value"] = $this->DATALT->GetDBValue(true);
        $this->UpdateFields["CODCLI"]["Value"] = $this->CODCLI->GetDBValue(true);
        $this->UpdateFields["IDSERCLI"]["Value"] = $this->IDSERCLI->GetDBValue(true);
        $this->UpdateFields["GRPSER"]["Value"] = $this->GRPSER->GetDBValue(true);
        $this->UpdateFields["SUBSER"]["Value"] = $this->SUBSER->GetDBValue(true);
        $this->UpdateFields["LOGSER"]["Value"] = $this->LOGSER->GetDBValue(true);
        $this->UpdateFields["BAISER"]["Value"] = $this->BAISER->GetDBValue(true);
        $this->UpdateFields["CIDSER"]["Value"] = $this->CIDSER->GetDBValue(true);
        $this->UpdateFields["ESTSER"]["Value"] = $this->ESTSER->GetDBValue(true);
        $this->UpdateFields["QTDMED"]["Value"] = $this->QTDMED->GetDBValue(true);
        $this->UpdateFields["TIPSER"]["Value"] = $this->TIPSER->GetDBValue(true);
        $this->UpdateFields["SERENC"]["Value"] = $this->SERENC->GetDBValue(true);
        $this->UpdateFields["CONTRATO"]["Value"] = $this->CONTRATO->GetDBValue(true);
        $this->UpdateFields["DATINI"]["Value"] = $this->DATINI->GetDBValue(true);
        $this->UpdateFields["DATFIM"]["Value"] = $this->DATFIM->GetDBValue(true);
        $this->UpdateFields["INIMAN"]["Value"] = $this->INIMAN->GetDBValue(true);
        $this->UpdateFields["FIMMAN"]["Value"] = $this->FIMMAN->GetDBValue(true);
        $this->UpdateFields["INITAR"]["Value"] = $this->INITAR->GetDBValue(true);
        $this->UpdateFields["FIMTAR"]["Value"] = $this->FIMTAR->GetDBValue(true);
        $this->UpdateFields["INIRES"]["Value"] = $this->INIRES->GetDBValue(true);
        $this->UpdateFields["FIMRES"]["Value"] = $this->FIMRES->GetDBValue(true);
        $this->UpdateFields["DESCSER"]["Value"] = $this->DESCSER->GetDBValue(true);
        $this->SQL = CCBuildUpdate("MOVSER", $this->UpdateFields, $this);
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteUpdate", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteUpdate", $this->Parent);
        }
    }
//End Update Method

//Delete Method @4-504FC8BC
    function Delete()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildDelete", $this->Parent);
        $this->SQL = "DELETE FROM MOVSER";
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteDelete", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteDelete", $this->Parent);
        }
    }
//End Delete Method

} //End MOVSERDataSource Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-422250F4
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "ManutCadSer.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-7FACF37D
CCSecurityRedirect("1;3", "");
//End Authenticate User

//Include events file @1-B0C9E743
include("./ManutCadSer_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-F20BE8AB
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$MOVSER = new clsRecordMOVSER("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->MOVSER = & $MOVSER;
$MainPage->rodape = & $rodape;
$MOVSER->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-7AD341FB
$cabec->Operations();
$MOVSER->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-E60AAB06
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($MOVSER);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-C20A290F
$cabec->Show();
$MOVSER->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-3CDE8187
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($MOVSER);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
