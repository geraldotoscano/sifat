<?php
//BindEvents Method @1-AD7004E1
function BindEvents()
{
    global $MOVSER;
    global $CCSEvents;
    $MOVSER->SUBSER->ds->CCSEvents["BeforeBuildSelect"] = "MOVSER_SUBSER_ds_BeforeBuildSelect";
    $MOVSER->LOGSER->CCSEvents["OnValidate"] = "MOVSER_LOGSER_OnValidate";
    $MOVSER->BAISER->CCSEvents["OnValidate"] = "MOVSER_BAISER_OnValidate";
    $MOVSER->CIDSER->CCSEvents["OnValidate"] = "MOVSER_CIDSER_OnValidate";
    $MOVSER->QTDMED->CCSEvents["BeforeShow"] = "MOVSER_QTDMED_BeforeShow";
    $MOVSER->Button_Insert->CCSEvents["OnClick"] = "MOVSER_Button_Insert_OnClick";
    $MOVSER->CCSEvents["BeforeShow"] = "MOVSER_BeforeShow";
    $MOVSER->CCSEvents["BeforeInsert"] = "MOVSER_BeforeInsert";
    $MOVSER->CCSEvents["BeforeUpdate"] = "MOVSER_BeforeUpdate";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//MOVSER_SUBSER_ds_BeforeBuildSelect @12-9D45E8CA
function MOVSER_SUBSER_ds_BeforeBuildSelect(& $sender)
{
    $MOVSER_SUBSER_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $MOVSER; //Compatibility
//End MOVSER_SUBSER_ds_BeforeBuildSelect

//Custom Code @64-2A29BDB7
// -------------------------
    // Write your own code here.
// -------------------------
//End Custom Code
 If ($MOVSER->SUBSER->DataSource->Order == "") { 
     $MOVSER->SUBSER->DataSource->Order = "dessub";
  }

//Close MOVSER_SUBSER_ds_BeforeBuildSelect @12-B0D2D858
    return $MOVSER_SUBSER_ds_BeforeBuildSelect;
}
//End Close MOVSER_SUBSER_ds_BeforeBuildSelect

//MOVSER_LOGSER_OnValidate @14-3E66D58D
function MOVSER_LOGSER_OnValidate(& $sender)
{
    $MOVSER_LOGSER_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $MOVSER; //Compatibility
//End MOVSER_LOGSER_OnValidate

//Custom Code @70-2A29BDB7
// -------------------------
   if ( !caracterInvado($Component->GetValue()) )
   {
      $MOVSER->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
   }
// -------------------------
//End Custom Code

//Close MOVSER_LOGSER_OnValidate @14-67C67905
    return $MOVSER_LOGSER_OnValidate;
}
//End Close MOVSER_LOGSER_OnValidate

//MOVSER_BAISER_OnValidate @15-8250D6E9
function MOVSER_BAISER_OnValidate(& $sender)
{
    $MOVSER_BAISER_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $MOVSER; //Compatibility
//End MOVSER_BAISER_OnValidate

//Custom Code @71-2A29BDB7
// -------------------------
   if ( !caracterInvado($Component->GetValue()) )
   {
      $MOVSER->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
   }
// -------------------------
//End Custom Code

//Close MOVSER_BAISER_OnValidate @15-ABB2858C
    return $MOVSER_BAISER_OnValidate;
}
//End Close MOVSER_BAISER_OnValidate

//MOVSER_CIDSER_OnValidate @16-EAB91D46
function MOVSER_CIDSER_OnValidate(& $sender)
{
    $MOVSER_CIDSER_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $MOVSER; //Compatibility
//End MOVSER_CIDSER_OnValidate

//Custom Code @72-2A29BDB7
// -------------------------
   if ( !caracterInvado($Component->GetValue()) )
   {
      $MOVSER->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
   }
// -------------------------
//End Custom Code

//Close MOVSER_CIDSER_OnValidate @16-79BF08CA
    return $MOVSER_CIDSER_OnValidate;
}
//End Close MOVSER_CIDSER_OnValidate
function caracterInvado($mDescri)
{
   $Result = true;
   ///$mDescri = strtolower($mDescri);
   //$mArqMov = fopen("c:\sistfat\fatura4.txt","w");
   //fwrite($mArqMov,"Inicio\r\n");
   ///$caracterNao = str_split("'!�@�#�$�%���&*()_-+=�/?�{[�]}�~^,.;:<>�`����������������������".chr(34));
   if (!empty($mDescri))
   {
      $caracterNao = str_split($mDescri);
      //fwrite($mArqMov,"M�ximo = \r\n");
      for($x=0;$x<=(count($caracterNao)-1);$x++)
      {
         //if ( !(strpos($mDescri, $caracterNao[$x])===false) )
	     if ( !((ord($caracterNao[$x]) > 34) && (ord($caracterNao[$x]) < 39)) &&  
              !((ord($caracterNao[$x]) > 39) && (ord($caracterNao[$x]) < 127)) &&
              (ord($caracterNao[$x]) != 32) &&
              (ord($caracterNao[$x]) != 33)
		    )
	     {
            //fwrite($mArqMov,"Caractere encontrado = $caracterNao[$x] \r\n");
		    //fwrite($mArqMov,"Encontrado em $mDescri \r\n");
		    $Result = false;
		    break;
	     }
	     //else
	     //{
         //   fwrite($mArqMov,"Caractere n�o encontrado = $caracterNao[$x] \r\n");
	     //}
      }
      //fclose($mArqMov);
   }
   return $Result;
}
//MOVSER_QTDMED_BeforeShow @18-BBCF96BB
function MOVSER_QTDMED_BeforeShow(& $sender)
{
    $MOVSER_QTDMED_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $MOVSER; //Compatibility
//End MOVSER_QTDMED_BeforeShow

//Custom Code @66-2A29BDB7
// -------------------------
	$MOVSER->QTDMED->SetValue(number_format($MOVSER->QTDMED->GetValue(), 3,',','.'));
// -------------------------
//End Custom Code

//Close MOVSER_QTDMED_BeforeShow @18-A46F7DA4
    return $MOVSER_QTDMED_BeforeShow;
}
//End Close MOVSER_QTDMED_BeforeShow

//MOVSER_Button_Insert_OnClick @5-581BD2A3
function MOVSER_Button_Insert_OnClick(& $sender)
{
    $MOVSER_Button_Insert_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $MOVSER; //Compatibility
//End MOVSER_Button_Insert_OnClick

//Declare Variable @57-A6A91EE1
    global $mCODCLI;
    $mCODCLI = $MOVSER->CODCLI->GetValue();
//End Declare Variable

//Declare Variable @58-2DFD8FE8
    global $mSUBSER;
    $mSUBSER = $MOVSER->SUBSER->GetValue();
//End Declare Variable

//DLookup @59-EA12A738
    global $DBFaturar;
    global $mCOD_CLI;
    $Page = CCGetParentPage($sender);
    $ccs_result = CCDLookUp("codcli", "movser", "codcli='$mCODCLI' and subser='$mSUBSER'", $Page->Connections["Faturar"]);
    $mCOD_CLI = $ccs_result;
//End DLookup

//DEL  // -------------------------
//DEL  // -------------------------

    if (!Empty($mCOD_CLI))
	{
//		$MOVSER->Errors->addError("Servi�o j� cadastrado no cliente.");
//		$MOVSER_Button_Insert_OnClick = false;
	}
//Close MOVSER_Button_Insert_OnClick @5-0AAD795B
    return $MOVSER_Button_Insert_OnClick;
}
//End Close MOVSER_Button_Insert_OnClick

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	if ($MOVSER->Button_Update->Visible)
//DEL  	{
//DEL  		$MOVSER->SUBSER->
//DEL  	}
//DEL  // -------------------------

//MOVSER_BeforeShow @4-421BAA2B
function MOVSER_BeforeShow(& $sender)
{
    $MOVSER_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $MOVSER; //Compatibility
//End MOVSER_BeforeShow

    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(50,52);
    
    if( verificaacesso($perfil,$permissao_requerida)==false )
	  {
	   $MOVSER->Button_Insert->Visible=false;
       $MOVSER->Button_Update->Visible=false;
	   $MOVSER->Button_Delete->Visible=false;
	  }

/*
if ($MOVSER->Button_Delete->Visible)
{
//Declare Variable @50-A6A91EE1
    global $mCODCLI;
    $mCODCLI = $MOVSER->CODCLI->GetValue();
//End Declare Variable

//Declare Variable @51-2DFD8FE8
    global $mSUBSER;
    $mSUBSER = $MOVSER->SUBSER->GetValue();
//End Declare Variable

//DLookup @52-5098C9BE
    global $DBFaturar;
    global $mCOD_CLI;
    $Page = CCGetParentPage($sender);
    $ccs_result = CCDLookUp("c.codcli", "cadfat c,movfat m", "c.codcli = $mCODCLI and c.codfat = m.codfat and m.subser = $mSUBSER", $Page->Connections["Faturar"]);
    $mCOD_CLI = $ccs_result;
//End DLookup

//Custom Code @74-2A29BDB7
// -------------------------




// -------------------------
//End Custom Code
	if ($mCOD_CLI != "")
	{
		$MOVSER->Button_Delete->Visible = false;
	}
}
*/
//Close MOVSER_BeforeShow @4-C17CF0F9
    return $MOVSER_BeforeShow;
}
//End Close MOVSER_BeforeShow

//MOVSER_BeforeInsert @4-9C8FFF03
function MOVSER_BeforeInsert(& $sender)
{
    $MOVSER_BeforeInsert = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $MOVSER; //Compatibility
//End MOVSER_BeforeInsert

//Custom Code @53-2A29BDB7
// -------------------------
    // Write your own code here.

	$MOVSER->CODINC->SetValue(CCGetSession("IDUsuario"));
    $MOVSER->DATINC->SetValue(CCGetSession("DataSist"));
    
    $MOVSER->IDSERCLI->SetValue(CCDLookUp('nvl(max(idsercli),-1)+1 novoid','movser',"codcli='".$MOVSER->CODCLI->Value."'"	, $MOVSER->DataSource ));
// -------------------------
//End Custom Code

//Close MOVSER_BeforeInsert @4-224EAB31
    return $MOVSER_BeforeInsert;
}
//End Close MOVSER_BeforeInsert

//MOVSER_BeforeUpdate @4-6C03C50F
function MOVSER_BeforeUpdate(& $sender)
{
    $MOVSER_BeforeUpdate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $MOVSER; //Compatibility
//End MOVSER_BeforeUpdate

//Custom Code @54-2A29BDB7
// -------------------------
    // Write your own code here.
	$MOVSER->CODALT->Value = CCGetSession("IDUsuario");
	$MOVSER->DATALT->Value = CCGetSession("DataSist"); 
// -------------------------
//End Custom Code

//Close MOVSER_BeforeUpdate @4-ED676ABE
    return $MOVSER_BeforeUpdate;
}
//End Close MOVSER_BeforeUpdate

//Page_BeforeShow @1-EEF59A40
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutCadSer; //Compatibility
//End Page_BeforeShow

//Custom Code @73-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(50,51,52);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
