<?php
//Include Common Files @1-17C35F80
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "ManutGRPAtiRel.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordCADCLI_GRPATI_TABUNI { //CADCLI_GRPATI_TABUNI Class @22-EA884FE9

//Variables @22-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @22-6CB2697E
    function clsRecordCADCLI_GRPATI_TABUNI($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record CADCLI_GRPATI_TABUNI/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "CADCLI_GRPATI_TABUNI";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_GRPATI = new clsControl(ccsListBox, "s_GRPATI", "s_GRPATI", ccsText, "", CCGetRequestParam("s_GRPATI", $Method, NULL), $this);
            $this->s_GRPATI->DSType = dsTable;
            $this->s_GRPATI->DataSource = new clsDBFaturar();
            $this->s_GRPATI->ds = & $this->s_GRPATI->DataSource;
            $this->s_GRPATI->DataSource->SQL = "SELECT * \n" .
"FROM GRPATI {SQL_Where} {SQL_OrderBy}";
            list($this->s_GRPATI->BoundColumn, $this->s_GRPATI->TextColumn, $this->s_GRPATI->DBFormat) = array("GRPATI", "DESATI", "");
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
        }
    }
//End Class_Initialize Event

//Validate Method @22-32C5F482
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_GRPATI->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->s_GRPATI->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @22-C047CB9B
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_GRPATI->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @22-D51E7E0C
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "ManutGRPAtiRel.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                $Redirect = "ManutGRPAtiRel.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @22-8EB8BC62
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->s_GRPATI->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->s_GRPATI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_GRPATI->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End CADCLI_GRPATI_TABUNI Class @22-FCB6E20C

class clsGridCADCLI_GRPATI_TABUNI1 { //CADCLI_GRPATI_TABUNI1 class @4-EB5747D3

//Variables @4-9F945B54

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_GRPATI;
    public $Sorter_DESATI;
    public $Sorter_DESCLI;
    public $Sorter_LOGRAD;
    public $Sorter_BAIRRO;
    public $Sorter_CIDADE;
    public $Sorter_ESTADO;
    public $Sorter_CODPOS;
    public $Sorter_TELEFO;
    public $Sorter_TELFAX;
    public $Sorter_CONTAT;
    public $Sorter_DESUNI;
//End Variables

//Class_Initialize Event @4-53552634
    function clsGridCADCLI_GRPATI_TABUNI1($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "CADCLI_GRPATI_TABUNI1";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid CADCLI_GRPATI_TABUNI1";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clsCADCLI_GRPATI_TABUNI1DataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 10;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("CADCLI_GRPATI_TABUNI1Order", "");
        $this->SorterDirection = CCGetParam("CADCLI_GRPATI_TABUNI1Dir", "");

        $this->GRPATI = new clsControl(ccsLabel, "GRPATI", "GRPATI", ccsText, "", CCGetRequestParam("GRPATI", ccsGet, NULL), $this);
        $this->DESATI = new clsControl(ccsLabel, "DESATI", "DESATI", ccsText, "", CCGetRequestParam("DESATI", ccsGet, NULL), $this);
        $this->DESCLI = new clsControl(ccsLabel, "DESCLI", "DESCLI", ccsText, "", CCGetRequestParam("DESCLI", ccsGet, NULL), $this);
        $this->LOGRAD = new clsControl(ccsLabel, "LOGRAD", "LOGRAD", ccsText, "", CCGetRequestParam("LOGRAD", ccsGet, NULL), $this);
        $this->BAIRRO = new clsControl(ccsLabel, "BAIRRO", "BAIRRO", ccsText, "", CCGetRequestParam("BAIRRO", ccsGet, NULL), $this);
        $this->CIDADE = new clsControl(ccsLabel, "CIDADE", "CIDADE", ccsText, "", CCGetRequestParam("CIDADE", ccsGet, NULL), $this);
        $this->ESTADO = new clsControl(ccsLabel, "ESTADO", "ESTADO", ccsText, "", CCGetRequestParam("ESTADO", ccsGet, NULL), $this);
        $this->CODPOS = new clsControl(ccsLabel, "CODPOS", "CODPOS", ccsText, "", CCGetRequestParam("CODPOS", ccsGet, NULL), $this);
        $this->TELEFO = new clsControl(ccsLabel, "TELEFO", "TELEFO", ccsText, "", CCGetRequestParam("TELEFO", ccsGet, NULL), $this);
        $this->TELFAX = new clsControl(ccsLabel, "TELFAX", "TELFAX", ccsText, "", CCGetRequestParam("TELFAX", ccsGet, NULL), $this);
        $this->CONTAT = new clsControl(ccsLabel, "CONTAT", "CONTAT", ccsText, "", CCGetRequestParam("CONTAT", ccsGet, NULL), $this);
        $this->DESUNI = new clsControl(ccsLabel, "DESUNI", "DESUNI", ccsText, "", CCGetRequestParam("DESUNI", ccsGet, NULL), $this);
        $this->Sorter_GRPATI = new clsSorter($this->ComponentName, "Sorter_GRPATI", $FileName, $this);
        $this->Sorter_DESATI = new clsSorter($this->ComponentName, "Sorter_DESATI", $FileName, $this);
        $this->Sorter_DESCLI = new clsSorter($this->ComponentName, "Sorter_DESCLI", $FileName, $this);
        $this->Sorter_LOGRAD = new clsSorter($this->ComponentName, "Sorter_LOGRAD", $FileName, $this);
        $this->Sorter_BAIRRO = new clsSorter($this->ComponentName, "Sorter_BAIRRO", $FileName, $this);
        $this->Sorter_CIDADE = new clsSorter($this->ComponentName, "Sorter_CIDADE", $FileName, $this);
        $this->Sorter_ESTADO = new clsSorter($this->ComponentName, "Sorter_ESTADO", $FileName, $this);
        $this->Sorter_CODPOS = new clsSorter($this->ComponentName, "Sorter_CODPOS", $FileName, $this);
        $this->Sorter_TELEFO = new clsSorter($this->ComponentName, "Sorter_TELEFO", $FileName, $this);
        $this->Sorter_TELFAX = new clsSorter($this->ComponentName, "Sorter_TELFAX", $FileName, $this);
        $this->Sorter_CONTAT = new clsSorter($this->ComponentName, "Sorter_CONTAT", $FileName, $this);
        $this->Sorter_DESUNI = new clsSorter($this->ComponentName, "Sorter_DESUNI", $FileName, $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple, $this);
    }
//End Class_Initialize Event

//Initialize Method @4-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @4-588C6BAB
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urls_GRPATI"] = CCGetFromGet("s_GRPATI", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["GRPATI"] = $this->GRPATI->Visible;
            $this->ControlsVisible["DESATI"] = $this->DESATI->Visible;
            $this->ControlsVisible["DESCLI"] = $this->DESCLI->Visible;
            $this->ControlsVisible["LOGRAD"] = $this->LOGRAD->Visible;
            $this->ControlsVisible["BAIRRO"] = $this->BAIRRO->Visible;
            $this->ControlsVisible["CIDADE"] = $this->CIDADE->Visible;
            $this->ControlsVisible["ESTADO"] = $this->ESTADO->Visible;
            $this->ControlsVisible["CODPOS"] = $this->CODPOS->Visible;
            $this->ControlsVisible["TELEFO"] = $this->TELEFO->Visible;
            $this->ControlsVisible["TELFAX"] = $this->TELFAX->Visible;
            $this->ControlsVisible["CONTAT"] = $this->CONTAT->Visible;
            $this->ControlsVisible["DESUNI"] = $this->DESUNI->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->GRPATI->SetValue($this->DataSource->GRPATI->GetValue());
                $this->DESATI->SetValue($this->DataSource->DESATI->GetValue());
                $this->DESCLI->SetValue($this->DataSource->DESCLI->GetValue());
                $this->LOGRAD->SetValue($this->DataSource->LOGRAD->GetValue());
                $this->BAIRRO->SetValue($this->DataSource->BAIRRO->GetValue());
                $this->CIDADE->SetValue($this->DataSource->CIDADE->GetValue());
                $this->ESTADO->SetValue($this->DataSource->ESTADO->GetValue());
                $this->CODPOS->SetValue($this->DataSource->CODPOS->GetValue());
                $this->TELEFO->SetValue($this->DataSource->TELEFO->GetValue());
                $this->TELFAX->SetValue($this->DataSource->TELFAX->GetValue());
                $this->CONTAT->SetValue($this->DataSource->CONTAT->GetValue());
                $this->DESUNI->SetValue($this->DataSource->DESUNI->GetValue());
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->GRPATI->Show();
                $this->DESATI->Show();
                $this->DESCLI->Show();
                $this->LOGRAD->Show();
                $this->BAIRRO->Show();
                $this->CIDADE->Show();
                $this->ESTADO->Show();
                $this->CODPOS->Show();
                $this->TELEFO->Show();
                $this->TELFAX->Show();
                $this->CONTAT->Show();
                $this->DESUNI->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        $this->Sorter_GRPATI->Show();
        $this->Sorter_DESATI->Show();
        $this->Sorter_DESCLI->Show();
        $this->Sorter_LOGRAD->Show();
        $this->Sorter_BAIRRO->Show();
        $this->Sorter_CIDADE->Show();
        $this->Sorter_ESTADO->Show();
        $this->Sorter_CODPOS->Show();
        $this->Sorter_TELEFO->Show();
        $this->Sorter_TELFAX->Show();
        $this->Sorter_CONTAT->Show();
        $this->Sorter_DESUNI->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @4-26994A0A
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->GRPATI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESATI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESCLI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->LOGRAD->Errors->ToString());
        $errors = ComposeStrings($errors, $this->BAIRRO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CIDADE->Errors->ToString());
        $errors = ComposeStrings($errors, $this->ESTADO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CODPOS->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TELEFO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TELFAX->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CONTAT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESUNI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End CADCLI_GRPATI_TABUNI1 Class @4-FCB6E20C

class clsCADCLI_GRPATI_TABUNI1DataSource extends clsDBFaturar {  //CADCLI_GRPATI_TABUNI1DataSource Class @4-232203D5

//DataSource Variables @4-F369ADFA
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $GRPATI;
    public $DESATI;
    public $DESCLI;
    public $LOGRAD;
    public $BAIRRO;
    public $CIDADE;
    public $ESTADO;
    public $CODPOS;
    public $TELEFO;
    public $TELFAX;
    public $CONTAT;
    public $DESUNI;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-7AD5A62C
    function clsCADCLI_GRPATI_TABUNI1DataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid CADCLI_GRPATI_TABUNI1";
        $this->Initialize();
        $this->GRPATI = new clsField("GRPATI", ccsText, "");
        $this->DESATI = new clsField("DESATI", ccsText, "");
        $this->DESCLI = new clsField("DESCLI", ccsText, "");
        $this->LOGRAD = new clsField("LOGRAD", ccsText, "");
        $this->BAIRRO = new clsField("BAIRRO", ccsText, "");
        $this->CIDADE = new clsField("CIDADE", ccsText, "");
        $this->ESTADO = new clsField("ESTADO", ccsText, "");
        $this->CODPOS = new clsField("CODPOS", ccsText, "");
        $this->TELEFO = new clsField("TELEFO", ccsText, "");
        $this->TELFAX = new clsField("TELFAX", ccsText, "");
        $this->CONTAT = new clsField("CONTAT", ccsText, "");
        $this->DESUNI = new clsField("DESUNI", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @4-2507074F
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_GRPATI" => array("GRPATI.GRPATI", ""), 
            "Sorter_DESATI" => array("DESATI", ""), 
            "Sorter_DESCLI" => array("DESCLI", ""), 
            "Sorter_LOGRAD" => array("LOGRAD", ""), 
            "Sorter_BAIRRO" => array("BAIRRO", ""), 
            "Sorter_CIDADE" => array("CIDADE", ""), 
            "Sorter_ESTADO" => array("ESTADO", ""), 
            "Sorter_CODPOS" => array("CODPOS", ""), 
            "Sorter_TELEFO" => array("TELEFO", ""), 
            "Sorter_TELFAX" => array("TELFAX", ""), 
            "Sorter_CONTAT" => array("CONTAT", ""), 
            "Sorter_DESUNI" => array("DESUNI", "")));
    }
//End SetOrder Method

//Prepare Method @4-384CDD98
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_GRPATI", ccsText, "", "", $this->Parameters["urls_GRPATI"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "GRPATI.GRPATI", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->Where = 
             $this->wp->Criterion[1];
        $this->Where = $this->wp->opAND(false, "( (CADCLI.CODUNI = TABUNI.CODUNI) AND (CADCLI.GRPATI = GRPATI.GRPATI) )", $this->Where);
    }
//End Prepare Method

//Open Method @4-BF0DF0BE
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM GRPATI,\n\n" .
        "CADCLI,\n\n" .
        "TABUNI";
        $this->SQL = "SELECT GRPATI.*, DESCLI, LOGRAD, BAIRRO, CIDADE, ESTADO, TELEFO, CONTAT, TELFAX, CODPOS, DESUNI \n\n" .
        "FROM GRPATI,\n\n" .
        "CADCLI,\n\n" .
        "TABUNI {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @4-DA1B2290
    function SetValues()
    {
        $this->GRPATI->SetDBValue($this->f("GRPATI"));
        $this->DESATI->SetDBValue($this->f("DESATI"));
        $this->DESCLI->SetDBValue($this->f("DESCLI"));
        $this->LOGRAD->SetDBValue($this->f("LOGRAD"));
        $this->BAIRRO->SetDBValue($this->f("BAIRRO"));
        $this->CIDADE->SetDBValue($this->f("CIDADE"));
        $this->ESTADO->SetDBValue($this->f("ESTADO"));
        $this->CODPOS->SetDBValue($this->f("CODPOS"));
        $this->TELEFO->SetDBValue($this->f("TELEFO"));
        $this->TELFAX->SetDBValue($this->f("TELFAX"));
        $this->CONTAT->SetDBValue($this->f("CONTAT"));
        $this->DESUNI->SetDBValue($this->f("DESUNI"));
    }
//End SetValues Method

} //End CADCLI_GRPATI_TABUNI1DataSource Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-AE6058D1
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "ManutGRPAtiRel.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-946ECC7A
CCSecurityRedirect("1;2;3", "");
//End Authenticate User

//Include events file @1-A7F5A1C4
include("./ManutGRPAtiRel_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-480027AA
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$CADCLI_GRPATI_TABUNI = new clsRecordCADCLI_GRPATI_TABUNI("", $MainPage);
$CADCLI_GRPATI_TABUNI1 = new clsGridCADCLI_GRPATI_TABUNI1("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->CADCLI_GRPATI_TABUNI = & $CADCLI_GRPATI_TABUNI;
$MainPage->CADCLI_GRPATI_TABUNI1 = & $CADCLI_GRPATI_TABUNI1;
$MainPage->rodape = & $rodape;
$CADCLI_GRPATI_TABUNI1->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-BAEC09CF
$cabec->Operations();
$CADCLI_GRPATI_TABUNI->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-1792D712
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($CADCLI_GRPATI_TABUNI);
    unset($CADCLI_GRPATI_TABUNI1);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-F0AB88D8
$cabec->Show();
$CADCLI_GRPATI_TABUNI->Show();
$CADCLI_GRPATI_TABUNI1->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-F6106230
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($CADCLI_GRPATI_TABUNI);
unset($CADCLI_GRPATI_TABUNI1);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
