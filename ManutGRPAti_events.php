<?php
//BindEvents Method @1-9BB2BF5E
function BindEvents()
{
    global $GRPATI;
    global $CCSEvents;
    $GRPATI->GRPATI->CCSEvents["BeforeShow"] = "GRPATI_GRPATI_BeforeShow";
    $GRPATI->CCSEvents["BeforeShow"] = "GRPATI_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//GRPATI_GRPATI_BeforeShow @11-6AE0D23E
function GRPATI_GRPATI_BeforeShow(& $sender)
{
    $GRPATI_GRPATI_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GRPATI; //Compatibility
//End GRPATI_GRPATI_BeforeShow

//Custom Code @16-2A29BDB7
// -------------------------
    // Write your own code here.
	if ($GRPATI->Button_Insert->Visible)
	{
// -------------------------
//End Custom Code

//DLookup @17-601F8E6F
       global $DBfaturar;
       $Page = CCGetParentPage($sender);
       $ccs_result = CCDLookUp("max(to_number(grpati))+1", "grpati", "", $Page->Connections["Faturar"]);
	   if (is_null($ccs_result))
	   {
	   		$ccs_result = 1;//Para o caso da tabela estar vazia;
	   }
       $ccs_result = intval($ccs_result);
       $Component->SetValue($ccs_result);
	}
   
//End DLookup

//Close GRPATI_GRPATI_BeforeShow @11-30D2104C
    return $GRPATI_GRPATI_BeforeShow;
}
//End Close GRPATI_GRPATI_BeforeShow

//DEL  // -------------------------
//DEL     if ( !caracterInvado($Component->GetValue()) )
//DEL     {
//DEL        $GRPATI->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
//DEL     }
//DEL  // -------------------------

//GRPATI_BeforeShow @4-D2B13446
function GRPATI_BeforeShow(& $sender)
{
    $GRPATI_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GRPATI; //Compatibility
//End GRPATI_BeforeShow

//Custom Code @15-2A29BDB7
// -------------------------
    // Write your own code here.
    global $DBfaturar;
    $Page = CCGetParentPage($sender);
	if ($GRPATI->Button_Delete->Visible)
	{
       $ccs_result = CCDLookUp("coduni", "cadcli", "grpati='".$GRPATI->GRPATI->Value."'", $Page->Connections["Faturar"]);
       //$Component->SetValue($ccs_result);
	   if ($ccs_result != "")
	   {
	      $GRPATI->Button_Delete->Visible = false;
	   }
	   else
	   {
          $ccs_result = CCDLookUp("grpati", "subati", "grpati='".$GRPATI->GRPATI->Value."'", $Page->Connections["Faturar"]);
	      if ($ccs_result != "")
	      {
	         $GRPATI->Button_Delete->Visible = false;
	      }
	   }
	}
// -------------------------
//End Custom Code

//Close GRPATI_BeforeShow @4-73C197AD
    return $GRPATI_BeforeShow;
}
//End Close GRPATI_BeforeShow

//Page_BeforeShow @1-A56DF685
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutGRPAti; //Compatibility
//End Page_BeforeShow

//Custom Code @21-2A29BDB7
// -------------------------

        include("controle_acesso.php");
        $Tabela = new clsDBfaturar();
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(6);
		controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
