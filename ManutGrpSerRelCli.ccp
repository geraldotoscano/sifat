<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="3" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="36" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="CADCLI_GRPSER_MOVSER_SUBS" wizardCaption="Search CADCLI GRPSER MOVSER SUBS " wizardOrientation="Vertical" wizardFormMethod="post" returnPage="ManutGrpSerRelCli.ccp" debugMode="False">
			<Components>
				<ListBox id="38" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="s_GRPSER" wizardCaption="GRPSER" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="GRPSER" boundColumn="GRPSER" textColumn="DESSER" editable="True" hasErrorCollection="True">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<ListBox id="39" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="s_DESUNI" wizardCaption="DESUNI" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="TABUNI" boundColumn="CODUNI" textColumn="DESUNI" editable="True" hasErrorCollection="True">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<Button id="37" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="4" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="Faturar" dataSource="GRPSER, CADCLI, MOVSER, SUBSER, TABUNI" name="CADCLI_GRPSER_MOVSER_SUBS1" pageSizeLimit="100" wizardCaption="List of CADCLI, GRPSER, MOVSER, SUBSER, TABUNI " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="Nenhum Grupo de Serviço foi encontrado" orderBy="MOVSER.GRPSER, TABUNI.CODUNI" activeCollection="TableParameters">
			<Components>
				<Sorter id="42" visible="True" name="Sorter_GRPSER" column="GRPSER.GRPSER" wizardCaption="GRPSER" wizardSortingType="SimpleDir" wizardControl="GRPSER" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="43" visible="True" name="Sorter_DESSER" column="DESSER" wizardCaption="DESSER" wizardSortingType="SimpleDir" wizardControl="DESSER" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="51" visible="True" name="Sorter_DESUNI" column="DESUNI" wizardCaption="DESUNI" wizardSortingType="SimpleDir" wizardControl="DESUNI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="52" visible="True" name="Sorter_DESCRI" column="DESCRI" wizardCaption="DESCRI" wizardSortingType="SimpleDir" wizardControl="DESCRI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="44" visible="True" name="Sorter_CADCLI_CODCLI" column="CADCLI.CODCLI" wizardCaption="CODCLI" wizardSortingType="SimpleDir" wizardControl="CADCLI_CODCLI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="45" visible="True" name="Sorter_DESCLI" column="DESCLI" wizardCaption="DESCLI" wizardSortingType="SimpleDir" wizardControl="DESCLI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="46" visible="True" name="Sorter_CGCCPF" column="CGCCPF" wizardCaption="CGCCPF" wizardSortingType="SimpleDir" wizardControl="CGCCPF" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="47" visible="True" name="Sorter_QTDMED" column="QTDMED" wizardCaption="QTDMED" wizardSortingType="SimpleDir" wizardControl="QTDMED" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="48" visible="True" name="Sorter_DATINI" column="DATINI" wizardCaption="DATINI" wizardSortingType="SimpleDir" wizardControl="DATINI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="49" visible="True" name="Sorter_DATFIM" column="DATFIM" wizardCaption="DATFIM" wizardSortingType="SimpleDir" wizardControl="DATFIM" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="50" visible="True" name="Sorter_DESSUB" column="DESSUB" wizardCaption="DESSUB" wizardSortingType="SimpleDir" wizardControl="DESSUB" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Label id="54" fieldSourceType="DBColumn" dataType="Text" html="False" name="GRPSER" fieldSource="GRPSER" wizardCaption="GRPSER" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="ManutGrpSer.ccp" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="74" sourceType="DataField" name="GRPSER" source="GRPSER"/>
					</LinkParameters>
				</Label>
				<Label id="56" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESSER" fieldSource="DESSER" wizardCaption="DESSER" wizardSize="50" wizardMaxLength="75" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" visible="Yes" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="64" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESUNI" fieldSource="DESUNI" wizardCaption="DESUNI" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="65" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESCRI" fieldSource="DESCRI" wizardCaption="DESCRI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="57" fieldSourceType="DBColumn" dataType="Text" html="False" name="CADCLI_CODCLI" fieldSource="CADCLI_CODCLI" wizardCaption="CODCLI" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="58" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESCLI" fieldSource="DESCLI" wizardCaption="DESCLI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="59" fieldSourceType="DBColumn" dataType="Text" html="False" name="CGCCPF" fieldSource="CGCCPF" wizardCaption="CGCCPF" wizardSize="14" wizardMaxLength="14" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="70"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="60" fieldSourceType="DBColumn" dataType="Float" html="False" name="QTDMED" fieldSource="QTDMED" wizardCaption="QTDMED" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" format="#,##0.00" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="61" fieldSourceType="DBColumn" dataType="Text" html="False" name="DATINI" fieldSource="DATINI" wizardCaption="DATINI" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" format="dd/mm/yyyy" editable="False">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="71"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="62" fieldSourceType="DBColumn" dataType="Text" html="True" name="DATFIM" fieldSource="DATFIM" wizardCaption="DATFIM" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" format="dd/mm/yyyy" editable="False">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="72"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="63" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESSUB" fieldSource="DESSUB" wizardCaption="DESSUB" wizardSize="50" wizardMaxLength="75" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="66" size="10" name="Navigator" wizardPagingType="Custom" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardImages="Images" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="True" wizardOfText="of" wizardImagesScheme="Table" type="Simple">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="40" conditionType="Parameter" useIsNull="False" field="GRPSER.GRPSER" parameterSource="s_GRPSER" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="1"/>
				<TableParameter id="41" conditionType="Parameter" useIsNull="False" field="TABUNI.CODUNI" dataType="Text" logicOperator="And" searchConditionType="Equal" parameterType="URL" orderNumber="2" parameterSource="s_DESUNI"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="5" tableName="GRPSER" posLeft="165" posTop="10" posWidth="95" posHeight="88"/>
				<JoinTable id="6" tableName="CADCLI" posLeft="428" posTop="99" posWidth="115" posHeight="137"/>
				<JoinTable id="7" tableName="MOVSER" posLeft="292" posTop="10" posWidth="115" posHeight="245"/>
				<JoinTable id="8" tableName="SUBSER" posLeft="156" posTop="114" posWidth="115" posHeight="128"/>
				<JoinTable id="9" tableName="TABUNI" posLeft="565" posTop="147" posWidth="95" posHeight="104"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="27" tableLeft="CADCLI" tableRight="MOVSER" fieldLeft="CADCLI.CODCLI" fieldRight="MOVSER.CODCLI" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="28" tableLeft="CADCLI" tableRight="TABUNI" fieldLeft="CADCLI.CODUNI" fieldRight="TABUNI.CODUNI" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="29" tableLeft="MOVSER" tableRight="GRPSER" fieldLeft="MOVSER.GRPSER" fieldRight="GRPSER.GRPSER" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="30" tableLeft="SUBSER" tableRight="MOVSER" fieldLeft="SUBSER.SUBSER" fieldRight="MOVSER.SUBSER" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="16" tableName="MOVSER" fieldName="QTDMED"/>
				<Field id="23" tableName="SUBSER" fieldName="DESSUB"/>
				<Field id="25" tableName="MOVSER" fieldName="DATINI"/>
				<Field id="26" tableName="MOVSER" fieldName="DATFIM"/>
				<Field id="31" tableName="CADCLI" fieldName="CADCLI.CODCLI" alias="CADCLI_CODCLI"/>
				<Field id="32" tableName="CADCLI" fieldName="DESCLI"/>
				<Field id="33" tableName="CADCLI" fieldName="CGCCPF"/>
				<Field id="34" tableName="TABUNI" fieldName="DESUNI"/>
				<Field id="35" tableName="TABUNI" fieldName="DESCRI"/>
				<Field id="68" tableName="GRPSER" fieldName="GRPSER.*"/>
				<Field id="69" tableName="TABUNI" fieldName="TABUNI.CODUNI" alias="TABUNI_CODUNI"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="2" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="ManutGrpSerRelCli.php" forShow="True" url="ManutGrpSerRelCli.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="ManutGrpSerRelCli_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="75" groupID="1"/>
		<Group id="76" groupID="2"/>
		<Group id="77" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="78"/>
			</Actions>
		</Event>
	</Events>
</Page>
