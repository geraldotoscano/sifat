<?php
//Include Common Files @1-3A0242DC
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "ManutGrpSerRelCli.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @3-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordCADCLI_GRPSER_MOVSER_SUBS { //CADCLI_GRPSER_MOVSER_SUBS Class @36-429B916A

//Variables @36-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @36-C0F19357
    function clsRecordCADCLI_GRPSER_MOVSER_SUBS($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record CADCLI_GRPSER_MOVSER_SUBS/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "CADCLI_GRPSER_MOVSER_SUBS";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_GRPSER = new clsControl(ccsListBox, "s_GRPSER", "s_GRPSER", ccsText, "", CCGetRequestParam("s_GRPSER", $Method, NULL), $this);
            $this->s_GRPSER->DSType = dsTable;
            $this->s_GRPSER->DataSource = new clsDBFaturar();
            $this->s_GRPSER->ds = & $this->s_GRPSER->DataSource;
            $this->s_GRPSER->DataSource->SQL = "SELECT * \n" .
"FROM GRPSER {SQL_Where} {SQL_OrderBy}";
            list($this->s_GRPSER->BoundColumn, $this->s_GRPSER->TextColumn, $this->s_GRPSER->DBFormat) = array("GRPSER", "DESSER", "");
            $this->s_DESUNI = new clsControl(ccsListBox, "s_DESUNI", "s_DESUNI", ccsText, "", CCGetRequestParam("s_DESUNI", $Method, NULL), $this);
            $this->s_DESUNI->DSType = dsTable;
            $this->s_DESUNI->DataSource = new clsDBFaturar();
            $this->s_DESUNI->ds = & $this->s_DESUNI->DataSource;
            $this->s_DESUNI->DataSource->SQL = "SELECT * \n" .
"FROM TABUNI {SQL_Where} {SQL_OrderBy}";
            list($this->s_DESUNI->BoundColumn, $this->s_DESUNI->TextColumn, $this->s_DESUNI->DBFormat) = array("CODUNI", "DESUNI", "");
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
        }
    }
//End Class_Initialize Event

//Validate Method @36-6FE81E1C
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_GRPSER->Validate() && $Validation);
        $Validation = ($this->s_DESUNI->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->s_GRPSER->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_DESUNI->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @36-67C630FE
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_GRPSER->Errors->Count());
        $errors = ($errors || $this->s_DESUNI->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @36-E2F2AF89
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "ManutGrpSerRelCli.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                $Redirect = "ManutGrpSerRelCli.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @36-36FAB98B
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->s_GRPSER->Prepare();
        $this->s_DESUNI->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->s_GRPSER->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_DESUNI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_GRPSER->Show();
        $this->s_DESUNI->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End CADCLI_GRPSER_MOVSER_SUBS Class @36-FCB6E20C

class clsGridCADCLI_GRPSER_MOVSER_SUBS1 { //CADCLI_GRPSER_MOVSER_SUBS1 class @4-3F874B85

//Variables @4-E0C512FA

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_GRPSER;
    public $Sorter_DESSER;
    public $Sorter_DESUNI;
    public $Sorter_DESCRI;
    public $Sorter_CADCLI_CODCLI;
    public $Sorter_DESCLI;
    public $Sorter_CGCCPF;
    public $Sorter_QTDMED;
    public $Sorter_DATINI;
    public $Sorter_DATFIM;
    public $Sorter_DESSUB;
//End Variables

//Class_Initialize Event @4-08C2A845
    function clsGridCADCLI_GRPSER_MOVSER_SUBS1($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "CADCLI_GRPSER_MOVSER_SUBS1";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid CADCLI_GRPSER_MOVSER_SUBS1";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clsCADCLI_GRPSER_MOVSER_SUBS1DataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 10;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("CADCLI_GRPSER_MOVSER_SUBS1Order", "");
        $this->SorterDirection = CCGetParam("CADCLI_GRPSER_MOVSER_SUBS1Dir", "");

        $this->GRPSER = new clsControl(ccsLabel, "GRPSER", "GRPSER", ccsText, "", CCGetRequestParam("GRPSER", ccsGet, NULL), $this);
        $this->DESSER = new clsControl(ccsLabel, "DESSER", "DESSER", ccsText, "", CCGetRequestParam("DESSER", ccsGet, NULL), $this);
        $this->DESUNI = new clsControl(ccsLabel, "DESUNI", "DESUNI", ccsText, "", CCGetRequestParam("DESUNI", ccsGet, NULL), $this);
        $this->DESCRI = new clsControl(ccsLabel, "DESCRI", "DESCRI", ccsText, "", CCGetRequestParam("DESCRI", ccsGet, NULL), $this);
        $this->CADCLI_CODCLI = new clsControl(ccsLabel, "CADCLI_CODCLI", "CADCLI_CODCLI", ccsText, "", CCGetRequestParam("CADCLI_CODCLI", ccsGet, NULL), $this);
        $this->DESCLI = new clsControl(ccsLabel, "DESCLI", "DESCLI", ccsText, "", CCGetRequestParam("DESCLI", ccsGet, NULL), $this);
        $this->CGCCPF = new clsControl(ccsLabel, "CGCCPF", "CGCCPF", ccsText, "", CCGetRequestParam("CGCCPF", ccsGet, NULL), $this);
        $this->QTDMED = new clsControl(ccsLabel, "QTDMED", "QTDMED", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), CCGetRequestParam("QTDMED", ccsGet, NULL), $this);
        $this->DATINI = new clsControl(ccsLabel, "DATINI", "DATINI", ccsText, "", CCGetRequestParam("DATINI", ccsGet, NULL), $this);
        $this->DATFIM = new clsControl(ccsLabel, "DATFIM", "DATFIM", ccsText, "", CCGetRequestParam("DATFIM", ccsGet, NULL), $this);
        $this->DATFIM->HTML = true;
        $this->DESSUB = new clsControl(ccsLabel, "DESSUB", "DESSUB", ccsText, "", CCGetRequestParam("DESSUB", ccsGet, NULL), $this);
        $this->Sorter_GRPSER = new clsSorter($this->ComponentName, "Sorter_GRPSER", $FileName, $this);
        $this->Sorter_DESSER = new clsSorter($this->ComponentName, "Sorter_DESSER", $FileName, $this);
        $this->Sorter_DESUNI = new clsSorter($this->ComponentName, "Sorter_DESUNI", $FileName, $this);
        $this->Sorter_DESCRI = new clsSorter($this->ComponentName, "Sorter_DESCRI", $FileName, $this);
        $this->Sorter_CADCLI_CODCLI = new clsSorter($this->ComponentName, "Sorter_CADCLI_CODCLI", $FileName, $this);
        $this->Sorter_DESCLI = new clsSorter($this->ComponentName, "Sorter_DESCLI", $FileName, $this);
        $this->Sorter_CGCCPF = new clsSorter($this->ComponentName, "Sorter_CGCCPF", $FileName, $this);
        $this->Sorter_QTDMED = new clsSorter($this->ComponentName, "Sorter_QTDMED", $FileName, $this);
        $this->Sorter_DATINI = new clsSorter($this->ComponentName, "Sorter_DATINI", $FileName, $this);
        $this->Sorter_DATFIM = new clsSorter($this->ComponentName, "Sorter_DATFIM", $FileName, $this);
        $this->Sorter_DESSUB = new clsSorter($this->ComponentName, "Sorter_DESSUB", $FileName, $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple, $this);
    }
//End Class_Initialize Event

//Initialize Method @4-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @4-A80EA69E
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urls_GRPSER"] = CCGetFromGet("s_GRPSER", NULL);
        $this->DataSource->Parameters["urls_DESUNI"] = CCGetFromGet("s_DESUNI", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["GRPSER"] = $this->GRPSER->Visible;
            $this->ControlsVisible["DESSER"] = $this->DESSER->Visible;
            $this->ControlsVisible["DESUNI"] = $this->DESUNI->Visible;
            $this->ControlsVisible["DESCRI"] = $this->DESCRI->Visible;
            $this->ControlsVisible["CADCLI_CODCLI"] = $this->CADCLI_CODCLI->Visible;
            $this->ControlsVisible["DESCLI"] = $this->DESCLI->Visible;
            $this->ControlsVisible["CGCCPF"] = $this->CGCCPF->Visible;
            $this->ControlsVisible["QTDMED"] = $this->QTDMED->Visible;
            $this->ControlsVisible["DATINI"] = $this->DATINI->Visible;
            $this->ControlsVisible["DATFIM"] = $this->DATFIM->Visible;
            $this->ControlsVisible["DESSUB"] = $this->DESSUB->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->GRPSER->SetValue($this->DataSource->GRPSER->GetValue());
                $this->DESSER->SetValue($this->DataSource->DESSER->GetValue());
                $this->DESUNI->SetValue($this->DataSource->DESUNI->GetValue());
                $this->DESCRI->SetValue($this->DataSource->DESCRI->GetValue());
                $this->CADCLI_CODCLI->SetValue($this->DataSource->CADCLI_CODCLI->GetValue());
                $this->DESCLI->SetValue($this->DataSource->DESCLI->GetValue());
                $this->CGCCPF->SetValue($this->DataSource->CGCCPF->GetValue());
                $this->QTDMED->SetValue($this->DataSource->QTDMED->GetValue());
                $this->DATINI->SetValue($this->DataSource->DATINI->GetValue());
                $this->DATFIM->SetValue($this->DataSource->DATFIM->GetValue());
                $this->DESSUB->SetValue($this->DataSource->DESSUB->GetValue());
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->GRPSER->Show();
                $this->DESSER->Show();
                $this->DESUNI->Show();
                $this->DESCRI->Show();
                $this->CADCLI_CODCLI->Show();
                $this->DESCLI->Show();
                $this->CGCCPF->Show();
                $this->QTDMED->Show();
                $this->DATINI->Show();
                $this->DATFIM->Show();
                $this->DESSUB->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        $this->Sorter_GRPSER->Show();
        $this->Sorter_DESSER->Show();
        $this->Sorter_DESUNI->Show();
        $this->Sorter_DESCRI->Show();
        $this->Sorter_CADCLI_CODCLI->Show();
        $this->Sorter_DESCLI->Show();
        $this->Sorter_CGCCPF->Show();
        $this->Sorter_QTDMED->Show();
        $this->Sorter_DATINI->Show();
        $this->Sorter_DATFIM->Show();
        $this->Sorter_DESSUB->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @4-D0B3824C
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->GRPSER->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESSER->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESUNI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESCRI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CADCLI_CODCLI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESCLI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CGCCPF->Errors->ToString());
        $errors = ComposeStrings($errors, $this->QTDMED->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DATINI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DATFIM->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESSUB->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End CADCLI_GRPSER_MOVSER_SUBS1 Class @4-FCB6E20C

class clsCADCLI_GRPSER_MOVSER_SUBS1DataSource extends clsDBFaturar {  //CADCLI_GRPSER_MOVSER_SUBS1DataSource Class @4-42D233CA

//DataSource Variables @4-8CDE3D7E
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $GRPSER;
    public $DESSER;
    public $DESUNI;
    public $DESCRI;
    public $CADCLI_CODCLI;
    public $DESCLI;
    public $CGCCPF;
    public $QTDMED;
    public $DATINI;
    public $DATFIM;
    public $DESSUB;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-0FA99353
    function clsCADCLI_GRPSER_MOVSER_SUBS1DataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid CADCLI_GRPSER_MOVSER_SUBS1";
        $this->Initialize();
        $this->GRPSER = new clsField("GRPSER", ccsText, "");
        $this->DESSER = new clsField("DESSER", ccsText, "");
        $this->DESUNI = new clsField("DESUNI", ccsText, "");
        $this->DESCRI = new clsField("DESCRI", ccsText, "");
        $this->CADCLI_CODCLI = new clsField("CADCLI_CODCLI", ccsText, "");
        $this->DESCLI = new clsField("DESCLI", ccsText, "");
        $this->CGCCPF = new clsField("CGCCPF", ccsText, "");
        $this->QTDMED = new clsField("QTDMED", ccsFloat, "");
        $this->DATINI = new clsField("DATINI", ccsText, "");
        $this->DATFIM = new clsField("DATFIM", ccsText, "");
        $this->DESSUB = new clsField("DESSUB", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @4-26447782
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "MOVSER.GRPSER, TABUNI.CODUNI";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_GRPSER" => array("GRPSER.GRPSER", ""), 
            "Sorter_DESSER" => array("DESSER", ""), 
            "Sorter_DESUNI" => array("DESUNI", ""), 
            "Sorter_DESCRI" => array("DESCRI", ""), 
            "Sorter_CADCLI_CODCLI" => array("CADCLI.CODCLI", ""), 
            "Sorter_DESCLI" => array("DESCLI", ""), 
            "Sorter_CGCCPF" => array("CGCCPF", ""), 
            "Sorter_QTDMED" => array("QTDMED", ""), 
            "Sorter_DATINI" => array("DATINI", ""), 
            "Sorter_DATFIM" => array("DATFIM", ""), 
            "Sorter_DESSUB" => array("DESSUB", "")));
    }
//End SetOrder Method

//Prepare Method @4-5F57E9B2
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_GRPSER", ccsText, "", "", $this->Parameters["urls_GRPSER"], "", false);
        $this->wp->AddParameter("2", "urls_DESUNI", ccsText, "", "", $this->Parameters["urls_DESUNI"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "GRPSER.GRPSER", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opEqual, "TABUNI.CODUNI", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsText),false);
        $this->Where = $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]);
        $this->Where = $this->wp->opAND(false, "( (CADCLI.CODCLI = MOVSER.CODCLI) AND (CADCLI.CODUNI = TABUNI.CODUNI) AND (MOVSER.GRPSER = GRPSER.GRPSER) AND (SUBSER.SUBSER = MOVSER.SUBSER) )", $this->Where);
    }
//End Prepare Method

//Open Method @4-879A06BF
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM GRPSER,\n\n" .
        "CADCLI,\n\n" .
        "MOVSER,\n\n" .
        "SUBSER,\n\n" .
        "TABUNI";
        $this->SQL = "SELECT QTDMED, DESSUB, DATINI, DATFIM, CADCLI.CODCLI AS CADCLI_CODCLI, DESCLI, CGCCPF, DESUNI, DESCRI, GRPSER.*, TABUNI.CODUNI AS TABUNI_CODUNI \n\n" .
        "FROM GRPSER,\n\n" .
        "CADCLI,\n\n" .
        "MOVSER,\n\n" .
        "SUBSER,\n\n" .
        "TABUNI {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @4-57996D84
    function SetValues()
    {
        $this->GRPSER->SetDBValue($this->f("GRPSER"));
        $this->DESSER->SetDBValue($this->f("DESSER"));
        $this->DESUNI->SetDBValue($this->f("DESUNI"));
        $this->DESCRI->SetDBValue($this->f("DESCRI"));
        $this->CADCLI_CODCLI->SetDBValue($this->f("CADCLI_CODCLI"));
        $this->DESCLI->SetDBValue($this->f("DESCLI"));
        $this->CGCCPF->SetDBValue($this->f("CGCCPF"));
        $this->QTDMED->SetDBValue(trim($this->f("QTDMED")));
        $this->DATINI->SetDBValue($this->f("DATINI"));
        $this->DATFIM->SetDBValue($this->f("DATFIM"));
        $this->DESSUB->SetDBValue($this->f("DESSUB"));
    }
//End SetValues Method

} //End CADCLI_GRPSER_MOVSER_SUBS1DataSource Class @4-FCB6E20C

//Include Page implementation @2-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-A677DD7F
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "ManutGrpSerRelCli.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-946ECC7A
CCSecurityRedirect("1;2;3", "");
//End Authenticate User

//Include events file @1-56462611
include("./ManutGrpSerRelCli_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-BD37F205
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$CADCLI_GRPSER_MOVSER_SUBS = new clsRecordCADCLI_GRPSER_MOVSER_SUBS("", $MainPage);
$CADCLI_GRPSER_MOVSER_SUBS1 = new clsGridCADCLI_GRPSER_MOVSER_SUBS1("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->CADCLI_GRPSER_MOVSER_SUBS = & $CADCLI_GRPSER_MOVSER_SUBS;
$MainPage->CADCLI_GRPSER_MOVSER_SUBS1 = & $CADCLI_GRPSER_MOVSER_SUBS1;
$MainPage->rodape = & $rodape;
$CADCLI_GRPSER_MOVSER_SUBS1->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-65D169D6
$cabec->Operations();
$CADCLI_GRPSER_MOVSER_SUBS->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-BF628FBE
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($CADCLI_GRPSER_MOVSER_SUBS);
    unset($CADCLI_GRPSER_MOVSER_SUBS1);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-9B9FA555
$cabec->Show();
$CADCLI_GRPSER_MOVSER_SUBS->Show();
$CADCLI_GRPSER_MOVSER_SUBS1->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-42E7066F
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($CADCLI_GRPSER_MOVSER_SUBS);
unset($CADCLI_GRPSER_MOVSER_SUBS1);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
