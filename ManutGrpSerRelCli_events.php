<?php
//BindEvents Method @1-6602556E
function BindEvents()
{
    global $CADCLI_GRPSER_MOVSER_SUBS1;
    global $CCSEvents;
    $CADCLI_GRPSER_MOVSER_SUBS1->CGCCPF->CCSEvents["BeforeShow"] = "CADCLI_GRPSER_MOVSER_SUBS1_CGCCPF_BeforeShow";
    $CADCLI_GRPSER_MOVSER_SUBS1->DATINI->CCSEvents["BeforeShow"] = "CADCLI_GRPSER_MOVSER_SUBS1_DATINI_BeforeShow";
    $CADCLI_GRPSER_MOVSER_SUBS1->DATFIM->CCSEvents["BeforeShow"] = "CADCLI_GRPSER_MOVSER_SUBS1_DATFIM_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//CADCLI_GRPSER_MOVSER_SUBS1_CGCCPF_BeforeShow @59-1EDBBB75
function CADCLI_GRPSER_MOVSER_SUBS1_CGCCPF_BeforeShow(& $sender)
{
    $CADCLI_GRPSER_MOVSER_SUBS1_CGCCPF_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI_GRPSER_MOVSER_SUBS1; //Compatibility
//End CADCLI_GRPSER_MOVSER_SUBS1_CGCCPF_BeforeShow

//Custom Code @70-2A29BDB7
// -------------------------
    // Write your own code here.
	// Se o tamanho do CGC / CPF for de 14, trata-se de um CGC do contr�rio, trata-se de um CPF
	$mCGC_Parte1 = substr($CADCLI_GRPSER_MOVSER_SUBS1->CGCCPF->Value,00,3);
	$mCGC_Parte2 = substr($CADCLI_GRPSER_MOVSER_SUBS1->CGCCPF->Value,03,3);
	$mCGC_Parte3 = substr($CADCLI_GRPSER_MOVSER_SUBS1->CGCCPF->Value,06,3);
	if (strlen(trim($CADCLI_GRPSER_MOVSER_SUBS1->CGCCPF->Value)) == 14)
	{
	   //123.456.789.012-34 Formato de um CGC
	   $mCGC_Parte4 = substr($CADCLI_GRPSER_MOVSER_SUBS1->CGCCPF->Value,09,3);
	   $mCGC_Parte5 = substr($CADCLI_GRPSER_MOVSER_SUBS1->CGCCPF->Value,-02);  // Pega os dois �ltimos
       $CADCLI_GRPSER_MOVSER_SUBS1->CGCCPF->SetValue($mCGC_Parte1.'.'.$mCGC_Parte2.'.'.$mCGC_Parte3.'.'.$mCGC_Parte4.' - '.$mCGC_Parte5);
	}
	else
	{
	   //123.456.789-34 Formato de um CGC
	   $mCGC_Parte4 = substr($CADCLI_GRPSER_MOVSER_SUBS1->CGCCPF->Value,-02);  // Pega os dois �ltimos
       $CADCLI_GRPSER_MOVSER_SUBS1->CGCCPF->SetValue($mCGC_Parte1.'.'.$mCGC_Parte2.'.'.$mCGC_Parte3.' - '.$mCGC_Parte4);
	}
// -------------------------
//End Custom Code

//Close CADCLI_GRPSER_MOVSER_SUBS1_CGCCPF_BeforeShow @59-23E59DE1
    return $CADCLI_GRPSER_MOVSER_SUBS1_CGCCPF_BeforeShow;
}
//End Close CADCLI_GRPSER_MOVSER_SUBS1_CGCCPF_BeforeShow

//CADCLI_GRPSER_MOVSER_SUBS1_DATINI_BeforeShow @61-C1A49619
function CADCLI_GRPSER_MOVSER_SUBS1_DATINI_BeforeShow(& $sender)
{
    $CADCLI_GRPSER_MOVSER_SUBS1_DATINI_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI_GRPSER_MOVSER_SUBS1; //Compatibility
//End CADCLI_GRPSER_MOVSER_SUBS1_DATINI_BeforeShow

//Custom Code @71-2A29BDB7
// -------------------------
    // Write your own code here.
	// aaaa-mm-dd  - Formato em que a data est� sendo apresentada.
    $mAno = substr($CADCLI_GRPSER_MOVSER_SUBS1->DATINI->Value,00,04);
	$mMes = substr($CADCLI_GRPSER_MOVSER_SUBS1->DATINI->Value,05,02);
	$mDia = substr($CADCLI_GRPSER_MOVSER_SUBS1->DATINI->Value,-02);  // Pega os dois �ltimos bytes, ou seja, o dia.
    $CADCLI_GRPSER_MOVSER_SUBS1->DATINI->SetValue($mDia.'/'.$mMes.'/'.$mAno);
// -------------------------
//End Custom Code

//Close CADCLI_GRPSER_MOVSER_SUBS1_DATINI_BeforeShow @61-5B52C6F9
    return $CADCLI_GRPSER_MOVSER_SUBS1_DATINI_BeforeShow;
}
//End Close CADCLI_GRPSER_MOVSER_SUBS1_DATINI_BeforeShow

//CADCLI_GRPSER_MOVSER_SUBS1_DATFIM_BeforeShow @62-2A9F55D0
function CADCLI_GRPSER_MOVSER_SUBS1_DATFIM_BeforeShow(& $sender)
{
    $CADCLI_GRPSER_MOVSER_SUBS1_DATFIM_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI_GRPSER_MOVSER_SUBS1; //Compatibility
//End CADCLI_GRPSER_MOVSER_SUBS1_DATFIM_BeforeShow

//Custom Code @72-2A29BDB7
// -------------------------
    // Write your own code here.
	// aaaa-mm-dd  - Formato em que a data est� sendo apresentada.
    $mAno = substr($CADCLI_GRPSER_MOVSER_SUBS1->DATFIM->Value,00,04);
	$mMes = substr($CADCLI_GRPSER_MOVSER_SUBS1->DATFIM->Value,05,02);
	$mDia = substr($CADCLI_GRPSER_MOVSER_SUBS1->DATFIM->Value,-02);  // Pega os dois �ltimos bytes, ou seja, o dia.
    $CADCLI_GRPSER_MOVSER_SUBS1->DATFIM->SetValue($mDia.'/'.$mMes.'/'.$mAno);

// -------------------------
//End Custom Code

//Close CADCLI_GRPSER_MOVSER_SUBS1_DATFIM_BeforeShow @62-571521C2
    return $CADCLI_GRPSER_MOVSER_SUBS1_DATFIM_BeforeShow;
}
//End Close CADCLI_GRPSER_MOVSER_SUBS1_DATFIM_BeforeShow

//Page_BeforeShow @1-7E14A969
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutGrpSerRelCli; //Compatibility
//End Page_BeforeShow

//Custom Code @78-2A29BDB7
// -------------------------
/*
  O usuario pode ter permiss�o para somente cadastrar servi�os por isto esta autenticacao � ligeiramente diferente

*/

        include("controle_acesso.php");
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(27,28);
		$permissao_requerida_extra=array(29);
        controleacesso_extra($perfil,$permissao_requerida,"acessonegado.php",$permissao_requerida_extra,"ManutGrpSer.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
