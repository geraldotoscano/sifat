<?php
//BindEvents Method @1-B31121C2
function BindEvents()
{
    global $GRPSER;
    global $CCSEvents;
    $GRPSER->GRPSER->CCSEvents["BeforeShow"] = "GRPSER_GRPSER_BeforeShow";
    $GRPSER->CCSEvents["BeforeShow"] = "GRPSER_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//GRPSER_GRPSER_BeforeShow @11-F54DAFBB
function GRPSER_GRPSER_BeforeShow(& $sender)
{
    $GRPSER_GRPSER_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GRPSER; //Compatibility
//End GRPSER_GRPSER_BeforeShow

//Custom Code @13-2A29BDB7
// -------------------------
    // Write your own code here.
	// Se o bot�o de insert estiver habilitado significa que vai ser feita uma incus�o. Neste caso o c�digo do servi�o � o valor do
	// maior existente mais um.
	if ($GRPSER->Button_Insert->Visible) 
	{
       global $DBfaturar;
       $Page = CCGetParentPage($sender);
       $ccs_result = CCDLookUp("max(to_number(grpser))+1", "grpser", "", $Page->Connections["Faturar"]);
       $ccs_result = intval($ccs_result);
	   if (is_null($ccs_result) || $ccs_result == 0)
	   {
	   		$ccs_result = 1;//Para o caso da tabela estar vazia;
	   }
       $Component->SetValue($ccs_result);
	}
// -------------------------
//End Custom Code

//Close GRPSER_GRPSER_BeforeShow @11-C275AACA
    return $GRPSER_GRPSER_BeforeShow;
}
//End Close GRPSER_GRPSER_BeforeShow

//DEL  // -------------------------
//DEL     if ( !caracterInvado($Component->GetValue()) )
//DEL     {
//DEL        $GRPSER->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
//DEL     }
//DEL  // -------------------------

//GRPSER_BeforeShow @4-AD16F7C4
function GRPSER_BeforeShow(& $sender)
{
    $GRPSER_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $GRPSER; //Compatibility
//End GRPSER_BeforeShow

//Custom Code @14-2A29BDB7
// -------------------------
    // Write your own code here.
	if ($GRPSER->Button_Delete->Visible)
	{
       global $DBfaturar;
       $Page = CCGetParentPage($sender);
       $ccs_result = CCDLookUp("grpser", "subser", "grpser='".$GRPSER->GRPSER->Value."'", $Page->Connections["Faturar"]);
       //$Component->SetValue($ccs_result);
	   if ($ccs_result != "")
	   {
	      $GRPSER->Button_Delete->Visible = false;
	   }
    }

// -------------------------
//End Custom Code

//Close GRPSER_BeforeShow @4-C655CF76
    return $GRPSER_BeforeShow;
}
//End Close GRPSER_BeforeShow

//Page_BeforeShow @1-2C85D933
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutGrpSer; //Compatibility
//End Page_BeforeShow

//Custom Code @18-2A29BDB7
// -------------------------

        include("controle_acesso.php");
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(27,29);
		controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
