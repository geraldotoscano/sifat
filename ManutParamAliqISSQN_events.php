<?php
//BindEvents Method @1-EFB8FA10
function BindEvents()
{
    global $PARAM_ALIQUOTA;
    global $CCSEvents;
    $PARAM_ALIQUOTA->VIGENCIA_INSS->CCSEvents["BeforeShow"] = "PARAM_ALIQUOTA_VIGENCIA_INSS_BeforeShow";
    $PARAM_ALIQUOTA->Button_Insert->CCSEvents["OnClick"] = "PARAM_ALIQUOTA_Button_Insert_OnClick";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//PARAM_ALIQUOTA_VIGENCIA_INSS_BeforeShow @13-F79FE56C
function PARAM_ALIQUOTA_VIGENCIA_INSS_BeforeShow(& $sender)
{
    $PARAM_ALIQUOTA_VIGENCIA_INSS_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $PARAM_ALIQUOTA; //Compatibility
//End PARAM_ALIQUOTA_VIGENCIA_INSS_BeforeShow

//Custom Code @25-2A29BDB7
// -------------------------
	$PARAM_ALIQUOTA->VIGENCIA_INSS->SetValue(CCGetSession("DataSist"));
// -------------------------
//End Custom Code

//Close PARAM_ALIQUOTA_VIGENCIA_INSS_BeforeShow @13-940D7205
    return $PARAM_ALIQUOTA_VIGENCIA_INSS_BeforeShow;
}
//End Close PARAM_ALIQUOTA_VIGENCIA_INSS_BeforeShow

//PARAM_ALIQUOTA_Button_Insert_OnClick @5-3A8A01D0
function PARAM_ALIQUOTA_Button_Insert_OnClick(& $sender)
{
    $PARAM_ALIQUOTA_Button_Insert_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $PARAM_ALIQUOTA; //Compatibility
//End PARAM_ALIQUOTA_Button_Insert_OnClick

//Declare Variable @28-C5F80ACA
    global $mVigencia;
    $mVigencia = "";
//End Declare Variable
	$mVigencia = $PARAM_ALIQUOTA->VIGENCIA_INSS->GetValue();
//DLookup @27-A7B4143C
    global $DBFaturar;
    global $mISSQN;
    $Page = CCGetParentPage($sender);
    $ccs_result = CCDLookUp("TAXA_ISSQN", "PARAM_ALIQUOTA_ISSQN", "to_char(VIGENCIA_ISSQN,'dd/mm/yyyy')= '$mVigencia'", $Page->Connections["Faturar"]);
    $mISSQN = $ccs_result;
//End DLookup

//Custom Code @29-2A29BDB7
// -------------------------
    if ($mISSQN)
	{
		$PARAM_ALIQUOTA->Errors->addError('Al�quota de ISSQN j� cadastrado.');
		$PARAM_ALIQUOTA_Button_Insert_OnClick = false;
	}
// -------------------------
//End Custom Code

//Close PARAM_ALIQUOTA_Button_Insert_OnClick @5-181E9C4A
    return $PARAM_ALIQUOTA_Button_Insert_OnClick;
}
//End Close PARAM_ALIQUOTA_Button_Insert_OnClick

//Page_BeforeShow @1-2E6FF955
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutParamAliqISSQN; //Compatibility
//End Page_BeforeShow

//Custom Code @31-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(48);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow
?>
