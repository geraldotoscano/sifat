<?php
//BindEvents Method @1-EFB8FA10
function BindEvents()
{
    global $PARAM_ALIQUOTA;
    global $CCSEvents;
    $PARAM_ALIQUOTA->VIGENCIA_INSS->CCSEvents["BeforeShow"] = "PARAM_ALIQUOTA_VIGENCIA_INSS_BeforeShow";
    $PARAM_ALIQUOTA->Button_Insert->CCSEvents["OnClick"] = "PARAM_ALIQUOTA_Button_Insert_OnClick";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//PARAM_ALIQUOTA_VIGENCIA_INSS_BeforeShow @13-F79FE56C
function PARAM_ALIQUOTA_VIGENCIA_INSS_BeforeShow(& $sender)
{
    $PARAM_ALIQUOTA_VIGENCIA_INSS_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $PARAM_ALIQUOTA; //Compatibility
//End PARAM_ALIQUOTA_VIGENCIA_INSS_BeforeShow

//Custom Code @25-2A29BDB7
// -------------------------
	$PARAM_ALIQUOTA->VIGENCIA_INSS->SetValue(CCGetSession("DataSist"));
// -------------------------
//End Custom Code

//Close PARAM_ALIQUOTA_VIGENCIA_INSS_BeforeShow @13-940D7205
    return $PARAM_ALIQUOTA_VIGENCIA_INSS_BeforeShow;
}
//End Close PARAM_ALIQUOTA_VIGENCIA_INSS_BeforeShow

//PARAM_ALIQUOTA_Button_Insert_OnClick @5-3A8A01D0
function PARAM_ALIQUOTA_Button_Insert_OnClick(& $sender)
{
    $PARAM_ALIQUOTA_Button_Insert_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $PARAM_ALIQUOTA; //Compatibility
//End PARAM_ALIQUOTA_Button_Insert_OnClick

//Declare Variable @30-C5F80ACA
    global $mVigencia;
    $mVigencia = "";
//End Declare Variable
	$mVigencia = $PARAM_ALIQUOTA->VIGENCIA_INSS->GetValue();
//DLookup @29-97585A07
    global $DBFaturar;
    global $mINSS;
    $Page = CCGetParentPage($sender);
    $ccs_result = CCDLookUp("TAXA_INSS", "PARAM_ALIQUOTA_INSS", "to_char(VIGENCIA_INSS,'dd/mm/yyyy') = '$mVigencia'", $Page->Connections["Faturar"]);
    $mINSS = $ccs_result;
//End DLookup

//Custom Code @31-2A29BDB7
// -------------------------
    // Write your own code here.
	if (!empty($mINSS))
	{
		$PARAM_ALIQUOTA->Errors->addError('Aliquota de INSS j� cadastrado.');
		$PARAM_ALIQUOTA_Button_Insert_OnClick = false;
	}
	else
	{
		$mINSS = $PARAM_ALIQUOTA->TAXA_INSS->GetValue();
		$mINSS = str_replace(".", ",",$mINSS);
		$PARAM_ALIQUOTA->TAXA_INSS->SetValue($mINSS);
		
		$mINSSMIM = $PARAM_ALIQUOTA->INSS_MIM->GetValue();
		$mINSSMIM = str_replace(".", ",",$mINSSMIM);
		$PARAM_ALIQUOTA->INSS_MIM->SetValue($mINSSMIM);
	}
// -------------------------
//End Custom Code

//Close PARAM_ALIQUOTA_Button_Insert_OnClick @5-181E9C4A
    return $PARAM_ALIQUOTA_Button_Insert_OnClick;
}
//End Close PARAM_ALIQUOTA_Button_Insert_OnClick

//Page_BeforeShow @1-3B23BFD3
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutParamAliq; //Compatibility
//End Page_BeforeShow

//Custom Code @34-2A29BDB7
// -------------------------
    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(47);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");
// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow
?>
