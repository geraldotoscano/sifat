<?php
//BindEvents Method @1-90BA1C59
function BindEvents()
{
    global $PARAM_T_J;
    global $CCSEvents;
    $PARAM_T_J->VIGENCIA_JUROS->CCSEvents["BeforeShow"] = "PARAM_T_J_VIGENCIA_JUROS_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//PARAM_T_J_VIGENCIA_JUROS_BeforeShow @24-E43BAB18
function PARAM_T_J_VIGENCIA_JUROS_BeforeShow(& $sender)
{
    $PARAM_T_J_VIGENCIA_JUROS_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $PARAM_T_J; //Compatibility
//End PARAM_T_J_VIGENCIA_JUROS_BeforeShow

//Custom Code @30-2A29BDB7
// -------------------------
	$PARAM_T_J->VIGENCIA_JUROS->SetValue(CCGetSession("DataSist"));
// -------------------------
//End Custom Code

//Close PARAM_T_J_VIGENCIA_JUROS_BeforeShow @24-6BD10BD5
    return $PARAM_T_J_VIGENCIA_JUROS_BeforeShow;
}
//End Close PARAM_T_J_VIGENCIA_JUROS_BeforeShow

//Page_BeforeShow @1-C0B74FEA
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutParam; //Compatibility
//End Page_BeforeShow

//Custom Code @31-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(46);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
