<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="4" sourceType="Table" urlType="Relative" secured="False" allowInsert="True" allowUpdate="True" allowDelete="True" validateData="True" preserveParameters="GET" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" connection="Faturar" name="TABPERFIL" dataSource="TABPERFIL" errorSummator="Error" wizardCaption="Add/Edit TABPERFIL " wizardFormMethod="post">
			<Components>
				<TextBox id="9" visible="Yes" fieldSourceType="DBColumn" dataType="Float" name="IDPERFIL" fieldSource="IDPERFIL" required="True" caption="IDPERFIL" wizardCaption="IDPERFIL" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="10" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="NOMEGRUPO" fieldSource="NOMEGRUPO" required="True" caption="NOMEGRUPO" wizardCaption="NOMEGRUPO" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="11" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="ATIVO" fieldSource="ATIVO" required="True" caption="ATIVO" wizardCaption="ATIVO" wizardSize="1" wizardMaxLength="1" wizardIsPassword="False" wizardUseTemplateBlock="False" defaultValue="S">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<Button id="5" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Insert" operation="Insert" wizardCaption="Adicionar">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="6" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Update" operation="Update" wizardCaption="Mudar">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="7" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Delete" operation="Delete" wizardCaption="Apagar">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="8" conditionType="Parameter" useIsNull="False" field="IDPERFIL" parameterSource="IDPERFIL" dataType="Float" logicOperator="And" searchConditionType="Equal" parameterType="URL" orderNumber="1"/>
			</TableParameters>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Record id="23" sourceType="Table" urlType="Relative" secured="False" allowInsert="True" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="GET" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" connection="Faturar" name="TABPERFIL_FUNCAO" dataSource="TABPERFIL_FUNCAO" errorSummator="Error" wizardCaption="Add/Edit TABPERFIL FUNCAO " wizardFormMethod="post">
			<Components>
				<Hidden id="26" fieldSourceType="DBColumn" dataType="Integer" name="IDPERFIL" fieldSource="IDPERFIL" required="True" caption="IDPERFIL" wizardCaption="IDPERFIL" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<ListBox id="27" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Integer" returnValueType="Number" name="CODFUNCAO" fieldSource="CODFUNCAO" required="True" caption="CODFUNCAO" wizardCaption="CODFUNCAO" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="TABFUNCAO" textColumn="DESCFUNCAO" boundColumn="CODFUNCAO" orderBy="DESCFUNCAO">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="49" tableName="TABFUNCAO" posLeft="10" posTop="10" posWidth="95" posHeight="278"/>
					</JoinTables>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<Button id="24" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Insert" operation="Insert" wizardCaption="Adicionar">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="25" conditionType="Parameter" useIsNull="False" field="CODFUNCAO" parameterSource="CODFUNCAO" dataType="Float" logicOperator="And" searchConditionType="Equal" parameterType="URL" orderNumber="1"/>
			</TableParameters>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="28" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="30" connection="Faturar" dataSource="TABPERFIL_FUNCAO, TABFUNCAO" activeCollection="TableParameters" name="TABFUNCAO_TABPERFIL_FUNCA" orderBy="DESCFUNCAO" pageSizeLimit="100" wizardCaption="List of TABFUNCAO, TABPERFIL FUNCAO " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="Não encontrado.">
			<Components>
				<Sorter id="33" visible="True" name="Sorter_DESCFUNCAO" column="DESCFUNCAO" wizardCaption="DESCFUNCAO" wizardSortingType="SimpleDir" wizardControl="DESCFUNCAO" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="34" visible="True" name="Sorter_IDPERFIL" column="IDPERFIL" wizardCaption="IDPERFIL" wizardSortingType="SimpleDir" wizardControl="IDPERFIL" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="35" visible="True" name="Sorter_TABPERFIL_FUNCAO_CODFUNCAO" column="TABPERFIL_FUNCAO.CODFUNCAO" wizardCaption="CODFUNCAO" wizardSortingType="SimpleDir" wizardControl="TABPERFIL_FUNCAO_CODFUNCAO" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Link id="37" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESCFUNCAO" fieldSource="DESCFUNCAO" wizardCaption="DESCFUNCAO" wizardSize="50" wizardMaxLength="120" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="ManutTabPerfilFuncao.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="47" sourceType="DataField" name="IDPERFIL" source="IDPERFIL"/>
						<LinkParameter id="48" sourceType="DataField" name="TABPERFIL_FUNCAO_CODFUNCAO" source="TABPERFIL_FUNCAO_CODFUNCAO"/>
					</LinkParameters>
				</Link>
				<Link id="39" fieldSourceType="DBColumn" dataType="Float" html="False" name="IDPERFIL" fieldSource="IDPERFIL" wizardCaption="IDPERFIL" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="ManutTabPerfilFuncao.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="45" sourceType="DataField" name="IDPERFIL" source="IDPERFIL"/>
						<LinkParameter id="46" sourceType="DataField" name="TABPERFIL_FUNCAO_CODFUNCAO" source="TABPERFIL_FUNCAO_CODFUNCAO"/>
					</LinkParameters>
				</Link>
				<Link id="41" fieldSourceType="DBColumn" dataType="Float" html="False" name="TABPERFIL_FUNCAO_CODFUNCAO" fieldSource="TABPERFIL_FUNCAO_CODFUNCAO" wizardCaption="CODFUNCAO" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="ManutTabPerfilFuncao.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="43" sourceType="DataField" name="IDPERFIL" source="IDPERFIL"/>
						<LinkParameter id="44" sourceType="DataField" name="TABPERFIL_FUNCAO_CODFUNCAO" source="TABPERFIL_FUNCAO_CODFUNCAO"/>
					</LinkParameters>
				</Link>
				<Navigator id="42" size="10" type="Centered" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="of" wizardImagesScheme="Blueprint">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="30" conditionType="Parameter" useIsNull="False" field="TABPERFIL_FUNCAO.IDPERFIL" dataType="Integer" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="IDPERFIL"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="29" tableName="TABPERFIL_FUNCAO" schemaName="SL005V3" posLeft="10" posTop="10" posWidth="117" posHeight="88"/>
				<JoinTable id="31" tableName="TABFUNCAO" schemaName="SL005V3" posLeft="198" posTop="25" posWidth="95" posHeight="88"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="32" tableLeft="TABPERFIL_FUNCAO" tableRight="TABFUNCAO" fieldLeft="TABPERFIL_FUNCAO.CODFUNCAO" fieldRight="TABFUNCAO.CODFUNCAO" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="36" tableName="TABFUNCAO" fieldName="DESCFUNCAO"/>
				<Field id="38" tableName="TABPERFIL_FUNCAO" fieldName="IDPERFIL"/>
				<Field id="40" tableName="TABPERFIL_FUNCAO" fieldName="TABPERFIL_FUNCAO.CODFUNCAO" alias="TABPERFIL_FUNCAO_CODFUNCAO"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="ManutPerfil.php" forShow="True" url="ManutPerfil.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="ManutPerfil_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="50"/>
			</Actions>
		</Event>
	</Events>
</Page>
