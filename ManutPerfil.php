<?php
//Include Common Files @1-C416FECD
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "ManutPerfil.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordTABPERFIL { //TABPERFIL Class @4-FF89D9E3

//Variables @4-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @4-DFA0E05F
    function clsRecordTABPERFIL($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record TABPERFIL/Error";
        $this->DataSource = new clsTABPERFILDataSource($this);
        $this->ds = & $this->DataSource;
        $this->InsertAllowed = true;
        $this->UpdateAllowed = true;
        $this->DeleteAllowed = true;
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "TABPERFIL";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->IDPERFIL = new clsControl(ccsTextBox, "IDPERFIL", "IDPERFIL", ccsFloat, "", CCGetRequestParam("IDPERFIL", $Method, NULL), $this);
            $this->IDPERFIL->Required = true;
            $this->NOMEGRUPO = new clsControl(ccsTextBox, "NOMEGRUPO", "NOMEGRUPO", ccsText, "", CCGetRequestParam("NOMEGRUPO", $Method, NULL), $this);
            $this->NOMEGRUPO->Required = true;
            $this->ATIVO = new clsControl(ccsTextBox, "ATIVO", "ATIVO", ccsText, "", CCGetRequestParam("ATIVO", $Method, NULL), $this);
            $this->ATIVO->Required = true;
            $this->Button_Insert = new clsButton("Button_Insert", $Method, $this);
            $this->Button_Update = new clsButton("Button_Update", $Method, $this);
            $this->Button_Delete = new clsButton("Button_Delete", $Method, $this);
            if(!$this->FormSubmitted) {
                if(!is_array($this->ATIVO->Value) && !strlen($this->ATIVO->Value) && $this->ATIVO->Value !== false)
                    $this->ATIVO->SetText(S);
            }
        }
    }
//End Class_Initialize Event

//Initialize Method @4-5C7E2520
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->DataSource->Parameters["urlIDPERFIL"] = CCGetFromGet("IDPERFIL", NULL);
    }
//End Initialize Method

//Validate Method @4-7D49348D
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->IDPERFIL->Validate() && $Validation);
        $Validation = ($this->NOMEGRUPO->Validate() && $Validation);
        $Validation = ($this->ATIVO->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->IDPERFIL->Errors->Count() == 0);
        $Validation =  $Validation && ($this->NOMEGRUPO->Errors->Count() == 0);
        $Validation =  $Validation && ($this->ATIVO->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @4-B4C3C7ED
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->IDPERFIL->Errors->Count());
        $errors = ($errors || $this->NOMEGRUPO->Errors->Count());
        $errors = ($errors || $this->ATIVO->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @4-B908BA44
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->DataSource->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->DataSource->AllParametersSet;
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = $this->EditMode ? "Button_Update" : "Button_Insert";
            if($this->Button_Insert->Pressed) {
                $this->PressedButton = "Button_Insert";
            } else if($this->Button_Update->Pressed) {
                $this->PressedButton = "Button_Update";
            } else if($this->Button_Delete->Pressed) {
                $this->PressedButton = "Button_Delete";
            }
        }
        $Redirect = $FileName . "?" . CCGetQueryString("QueryString", array("ccsForm"));
        if($this->PressedButton == "Button_Delete") {
            if(!CCGetEvent($this->Button_Delete->CCSEvents, "OnClick", $this->Button_Delete) || !$this->DeleteRow()) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button_Insert") {
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick", $this->Button_Insert) || !$this->InsertRow()) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "Button_Update") {
                if(!CCGetEvent($this->Button_Update->CCSEvents, "OnClick", $this->Button_Update) || !$this->UpdateRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
        if ($Redirect)
            $this->DataSource->close();
    }
//End Operation Method

//InsertRow Method @4-5E25AFC3
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert", $this);
        if(!$this->InsertAllowed) return false;
        $this->DataSource->IDPERFIL->SetValue($this->IDPERFIL->GetValue(true));
        $this->DataSource->NOMEGRUPO->SetValue($this->NOMEGRUPO->GetValue(true));
        $this->DataSource->ATIVO->SetValue($this->ATIVO->GetValue(true));
        $this->DataSource->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert", $this);
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//UpdateRow Method @4-298B1EAC
    function UpdateRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeUpdate", $this);
        if(!$this->UpdateAllowed) return false;
        $this->DataSource->IDPERFIL->SetValue($this->IDPERFIL->GetValue(true));
        $this->DataSource->NOMEGRUPO->SetValue($this->NOMEGRUPO->GetValue(true));
        $this->DataSource->ATIVO->SetValue($this->ATIVO->GetValue(true));
        $this->DataSource->Update();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterUpdate", $this);
        return (!$this->CheckErrors());
    }
//End UpdateRow Method

//DeleteRow Method @4-299D98C3
    function DeleteRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDelete", $this);
        if(!$this->DeleteAllowed) return false;
        $this->DataSource->Delete();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDelete", $this);
        return (!$this->CheckErrors());
    }
//End DeleteRow Method

//Show Method @4-03C6F9A8
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->DataSource->Errors->Count()){
                $this->Errors->AddErrors($this->DataSource->Errors);
                $this->DataSource->Errors->clear();
            }
            $this->DataSource->Open();
            if($this->DataSource->Errors->Count() == 0 && $this->DataSource->next_record()) {
                $this->DataSource->SetValues();
                if(!$this->FormSubmitted){
                    $this->IDPERFIL->SetValue($this->DataSource->IDPERFIL->GetValue());
                    $this->NOMEGRUPO->SetValue($this->DataSource->NOMEGRUPO->GetValue());
                    $this->ATIVO->SetValue($this->DataSource->ATIVO->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->IDPERFIL->Errors->ToString());
            $Error = ComposeStrings($Error, $this->NOMEGRUPO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->ATIVO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataSource->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Insert->Visible = !$this->EditMode && $this->InsertAllowed;
        $this->Button_Update->Visible = $this->EditMode && $this->UpdateAllowed;
        $this->Button_Delete->Visible = $this->EditMode && $this->DeleteAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->IDPERFIL->Show();
        $this->NOMEGRUPO->Show();
        $this->ATIVO->Show();
        $this->Button_Insert->Show();
        $this->Button_Update->Show();
        $this->Button_Delete->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

} //End TABPERFIL Class @4-FCB6E20C

class clsTABPERFILDataSource extends clsDBFaturar {  //TABPERFILDataSource Class @4-4BB0E666

//DataSource Variables @4-724FD003
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $InsertParameters;
    public $UpdateParameters;
    public $DeleteParameters;
    public $wp;
    public $AllParametersSet;

    public $InsertFields = array();
    public $UpdateFields = array();

    // Datasource fields
    public $IDPERFIL;
    public $NOMEGRUPO;
    public $ATIVO;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-EE28F557
    function clsTABPERFILDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Record TABPERFIL/Error";
        $this->Initialize();
        $this->IDPERFIL = new clsField("IDPERFIL", ccsFloat, "");
        $this->NOMEGRUPO = new clsField("NOMEGRUPO", ccsText, "");
        $this->ATIVO = new clsField("ATIVO", ccsText, "");

        $this->InsertFields["IDPERFIL"] = array("Name" => "IDPERFIL", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->InsertFields["NOMEGRUPO"] = array("Name" => "NOMEGRUPO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["ATIVO"] = array("Name" => "ATIVO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["IDPERFIL"] = array("Name" => "IDPERFIL", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->UpdateFields["NOMEGRUPO"] = array("Name" => "NOMEGRUPO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["ATIVO"] = array("Name" => "ATIVO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
    }
//End DataSourceClass_Initialize Event

//Prepare Method @4-B37BEEE8
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlIDPERFIL", ccsFloat, "", "", $this->Parameters["urlIDPERFIL"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "IDPERFIL", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsFloat),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @4-AA1D5B40
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->SQL = "SELECT * \n\n" .
        "FROM TABPERFIL {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @4-CF4C1794
    function SetValues()
    {
        $this->IDPERFIL->SetDBValue(trim($this->f("IDPERFIL")));
        $this->NOMEGRUPO->SetDBValue($this->f("NOMEGRUPO"));
        $this->ATIVO->SetDBValue($this->f("ATIVO"));
    }
//End SetValues Method

//Insert Method @4-E240C089
    function Insert()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert", $this->Parent);
        $this->InsertFields["IDPERFIL"]["Value"] = $this->IDPERFIL->GetDBValue(true);
        $this->InsertFields["NOMEGRUPO"]["Value"] = $this->NOMEGRUPO->GetDBValue(true);
        $this->InsertFields["ATIVO"]["Value"] = $this->ATIVO->GetDBValue(true);
        $this->SQL = CCBuildInsert("TABPERFIL", $this->InsertFields, $this);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert", $this->Parent);
        }
    }
//End Insert Method

//Update Method @4-219A9A24
    function Update()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildUpdate", $this->Parent);
        $this->UpdateFields["IDPERFIL"]["Value"] = $this->IDPERFIL->GetDBValue(true);
        $this->UpdateFields["NOMEGRUPO"]["Value"] = $this->NOMEGRUPO->GetDBValue(true);
        $this->UpdateFields["ATIVO"]["Value"] = $this->ATIVO->GetDBValue(true);
        $this->SQL = CCBuildUpdate("TABPERFIL", $this->UpdateFields, $this);
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteUpdate", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteUpdate", $this->Parent);
        }
    }
//End Update Method

//Delete Method @4-D52478C4
    function Delete()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildDelete", $this->Parent);
        $this->SQL = "DELETE FROM TABPERFIL";
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteDelete", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteDelete", $this->Parent);
        }
    }
//End Delete Method

} //End TABPERFILDataSource Class @4-FCB6E20C

class clsRecordTABPERFIL_FUNCAO { //TABPERFIL_FUNCAO Class @23-514E42D8

//Variables @23-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @23-4FFCBFAC
    function clsRecordTABPERFIL_FUNCAO($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record TABPERFIL_FUNCAO/Error";
        $this->DataSource = new clsTABPERFIL_FUNCAODataSource($this);
        $this->ds = & $this->DataSource;
        $this->InsertAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "TABPERFIL_FUNCAO";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->IDPERFIL = new clsControl(ccsHidden, "IDPERFIL", "IDPERFIL", ccsInteger, "", CCGetRequestParam("IDPERFIL", $Method, NULL), $this);
            $this->IDPERFIL->Required = true;
            $this->CODFUNCAO = new clsControl(ccsListBox, "CODFUNCAO", "CODFUNCAO", ccsInteger, "", CCGetRequestParam("CODFUNCAO", $Method, NULL), $this);
            $this->CODFUNCAO->DSType = dsTable;
            $this->CODFUNCAO->DataSource = new clsDBFaturar();
            $this->CODFUNCAO->ds = & $this->CODFUNCAO->DataSource;
            $this->CODFUNCAO->DataSource->SQL = "SELECT * \n" .
"FROM TABFUNCAO {SQL_Where} {SQL_OrderBy}";
            $this->CODFUNCAO->DataSource->Order = "DESCFUNCAO";
            list($this->CODFUNCAO->BoundColumn, $this->CODFUNCAO->TextColumn, $this->CODFUNCAO->DBFormat) = array("CODFUNCAO", "DESCFUNCAO", "");
            $this->CODFUNCAO->DataSource->Order = "DESCFUNCAO";
            $this->CODFUNCAO->Required = true;
            $this->Button_Insert = new clsButton("Button_Insert", $Method, $this);
        }
    }
//End Class_Initialize Event

//Initialize Method @23-2F037F01
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->DataSource->Parameters["urlCODFUNCAO"] = CCGetFromGet("CODFUNCAO", NULL);
    }
//End Initialize Method

//Validate Method @23-D9D8BB28
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->IDPERFIL->Validate() && $Validation);
        $Validation = ($this->CODFUNCAO->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->IDPERFIL->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODFUNCAO->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @23-1FC4A1AA
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->IDPERFIL->Errors->Count());
        $errors = ($errors || $this->CODFUNCAO->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @23-EFC50250
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->DataSource->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->DataSource->AllParametersSet;
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_Insert";
            if($this->Button_Insert->Pressed) {
                $this->PressedButton = "Button_Insert";
            }
        }
        $Redirect = $FileName . "?" . CCGetQueryString("QueryString", array("ccsForm"));
        if($this->Validate()) {
            if($this->PressedButton == "Button_Insert") {
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick", $this->Button_Insert) || !$this->InsertRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
        if ($Redirect)
            $this->DataSource->close();
    }
//End Operation Method

//InsertRow Method @23-7B055961
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert", $this);
        if(!$this->InsertAllowed) return false;
        $this->DataSource->IDPERFIL->SetValue($this->IDPERFIL->GetValue(true));
        $this->DataSource->CODFUNCAO->SetValue($this->CODFUNCAO->GetValue(true));
        $this->DataSource->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert", $this);
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//Show Method @23-BA0CBB26
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->CODFUNCAO->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->DataSource->Errors->Count()){
                $this->Errors->AddErrors($this->DataSource->Errors);
                $this->DataSource->Errors->clear();
            }
            $this->DataSource->Open();
            if($this->DataSource->Errors->Count() == 0 && $this->DataSource->next_record()) {
                $this->DataSource->SetValues();
                if(!$this->FormSubmitted){
                    $this->IDPERFIL->SetValue($this->DataSource->IDPERFIL->GetValue());
                    $this->CODFUNCAO->SetValue($this->DataSource->CODFUNCAO->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->IDPERFIL->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODFUNCAO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataSource->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Insert->Visible = !$this->EditMode && $this->InsertAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->IDPERFIL->Show();
        $this->CODFUNCAO->Show();
        $this->Button_Insert->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

} //End TABPERFIL_FUNCAO Class @23-FCB6E20C

class clsTABPERFIL_FUNCAODataSource extends clsDBFaturar {  //TABPERFIL_FUNCAODataSource Class @23-0C180E9E

//DataSource Variables @23-8618AA3D
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $InsertParameters;
    public $wp;
    public $AllParametersSet;

    public $InsertFields = array();

    // Datasource fields
    public $IDPERFIL;
    public $CODFUNCAO;
//End DataSource Variables

//DataSourceClass_Initialize Event @23-651F43A0
    function clsTABPERFIL_FUNCAODataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Record TABPERFIL_FUNCAO/Error";
        $this->Initialize();
        $this->IDPERFIL = new clsField("IDPERFIL", ccsInteger, "");
        $this->CODFUNCAO = new clsField("CODFUNCAO", ccsInteger, "");

        $this->InsertFields["IDPERFIL"] = array("Name" => "IDPERFIL", "Value" => "", "DataType" => ccsInteger, "OmitIfEmpty" => 1);
        $this->InsertFields["CODFUNCAO"] = array("Name" => "CODFUNCAO", "Value" => "", "DataType" => ccsInteger, "OmitIfEmpty" => 1);
    }
//End DataSourceClass_Initialize Event

//Prepare Method @23-EAE5CA7E
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlCODFUNCAO", ccsFloat, "", "", $this->Parameters["urlCODFUNCAO"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "CODFUNCAO", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsFloat),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @23-8BBB9586
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->SQL = "SELECT * \n\n" .
        "FROM TABPERFIL_FUNCAO {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @23-3370B9C8
    function SetValues()
    {
        $this->IDPERFIL->SetDBValue(trim($this->f("IDPERFIL")));
        $this->CODFUNCAO->SetDBValue(trim($this->f("CODFUNCAO")));
    }
//End SetValues Method

//Insert Method @23-7C030240
    function Insert()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert", $this->Parent);
        $this->InsertFields["IDPERFIL"]["Value"] = $this->IDPERFIL->GetDBValue(true);
        $this->InsertFields["CODFUNCAO"]["Value"] = $this->CODFUNCAO->GetDBValue(true);
        $this->SQL = CCBuildInsert("TABPERFIL_FUNCAO", $this->InsertFields, $this);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert", $this->Parent);
        }
    }
//End Insert Method

} //End TABPERFIL_FUNCAODataSource Class @23-FCB6E20C

class clsGridTABFUNCAO_TABPERFIL_FUNCA { //TABFUNCAO_TABPERFIL_FUNCA class @28-4936DE06

//Variables @28-55A1A026

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_DESCFUNCAO;
    public $Sorter_IDPERFIL;
    public $Sorter_TABPERFIL_FUNCAO_CODFUNCAO;
//End Variables

//Class_Initialize Event @28-86E80A00
    function clsGridTABFUNCAO_TABPERFIL_FUNCA($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "TABFUNCAO_TABPERFIL_FUNCA";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid TABFUNCAO_TABPERFIL_FUNCA";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clsTABFUNCAO_TABPERFIL_FUNCADataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 30;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("TABFUNCAO_TABPERFIL_FUNCAOrder", "");
        $this->SorterDirection = CCGetParam("TABFUNCAO_TABPERFIL_FUNCADir", "");

        $this->DESCFUNCAO = new clsControl(ccsLink, "DESCFUNCAO", "DESCFUNCAO", ccsText, "", CCGetRequestParam("DESCFUNCAO", ccsGet, NULL), $this);
        $this->DESCFUNCAO->Page = "ManutTabPerfilFuncao.php";
        $this->IDPERFIL = new clsControl(ccsLink, "IDPERFIL", "IDPERFIL", ccsFloat, "", CCGetRequestParam("IDPERFIL", ccsGet, NULL), $this);
        $this->IDPERFIL->Page = "ManutTabPerfilFuncao.php";
        $this->TABPERFIL_FUNCAO_CODFUNCAO = new clsControl(ccsLink, "TABPERFIL_FUNCAO_CODFUNCAO", "TABPERFIL_FUNCAO_CODFUNCAO", ccsFloat, "", CCGetRequestParam("TABPERFIL_FUNCAO_CODFUNCAO", ccsGet, NULL), $this);
        $this->TABPERFIL_FUNCAO_CODFUNCAO->Page = "ManutTabPerfilFuncao.php";
        $this->Sorter_DESCFUNCAO = new clsSorter($this->ComponentName, "Sorter_DESCFUNCAO", $FileName, $this);
        $this->Sorter_IDPERFIL = new clsSorter($this->ComponentName, "Sorter_IDPERFIL", $FileName, $this);
        $this->Sorter_TABPERFIL_FUNCAO_CODFUNCAO = new clsSorter($this->ComponentName, "Sorter_TABPERFIL_FUNCAO_CODFUNCAO", $FileName, $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpCentered, $this);
    }
//End Class_Initialize Event

//Initialize Method @28-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @28-6401FB08
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urlIDPERFIL"] = CCGetFromGet("IDPERFIL", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["DESCFUNCAO"] = $this->DESCFUNCAO->Visible;
            $this->ControlsVisible["IDPERFIL"] = $this->IDPERFIL->Visible;
            $this->ControlsVisible["TABPERFIL_FUNCAO_CODFUNCAO"] = $this->TABPERFIL_FUNCAO_CODFUNCAO->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->DESCFUNCAO->SetValue($this->DataSource->DESCFUNCAO->GetValue());
                $this->DESCFUNCAO->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->DESCFUNCAO->Parameters = CCAddParam($this->DESCFUNCAO->Parameters, "IDPERFIL", $this->DataSource->f("IDPERFIL"));
                $this->DESCFUNCAO->Parameters = CCAddParam($this->DESCFUNCAO->Parameters, "TABPERFIL_FUNCAO_CODFUNCAO", $this->DataSource->f("TABPERFIL_FUNCAO_CODFUNCAO"));
                $this->IDPERFIL->SetValue($this->DataSource->IDPERFIL->GetValue());
                $this->IDPERFIL->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->IDPERFIL->Parameters = CCAddParam($this->IDPERFIL->Parameters, "IDPERFIL", $this->DataSource->f("IDPERFIL"));
                $this->IDPERFIL->Parameters = CCAddParam($this->IDPERFIL->Parameters, "TABPERFIL_FUNCAO_CODFUNCAO", $this->DataSource->f("TABPERFIL_FUNCAO_CODFUNCAO"));
                $this->TABPERFIL_FUNCAO_CODFUNCAO->SetValue($this->DataSource->TABPERFIL_FUNCAO_CODFUNCAO->GetValue());
                $this->TABPERFIL_FUNCAO_CODFUNCAO->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
                $this->TABPERFIL_FUNCAO_CODFUNCAO->Parameters = CCAddParam($this->TABPERFIL_FUNCAO_CODFUNCAO->Parameters, "IDPERFIL", $this->DataSource->f("IDPERFIL"));
                $this->TABPERFIL_FUNCAO_CODFUNCAO->Parameters = CCAddParam($this->TABPERFIL_FUNCAO_CODFUNCAO->Parameters, "TABPERFIL_FUNCAO_CODFUNCAO", $this->DataSource->f("TABPERFIL_FUNCAO_CODFUNCAO"));
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->DESCFUNCAO->Show();
                $this->IDPERFIL->Show();
                $this->TABPERFIL_FUNCAO_CODFUNCAO->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        $this->Sorter_DESCFUNCAO->Show();
        $this->Sorter_IDPERFIL->Show();
        $this->Sorter_TABPERFIL_FUNCAO_CODFUNCAO->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @28-4B3F171D
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->DESCFUNCAO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->IDPERFIL->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TABPERFIL_FUNCAO_CODFUNCAO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End TABFUNCAO_TABPERFIL_FUNCA Class @28-FCB6E20C

class clsTABFUNCAO_TABPERFIL_FUNCADataSource extends clsDBFaturar {  //TABFUNCAO_TABPERFIL_FUNCADataSource Class @28-4381D823

//DataSource Variables @28-A7B71D93
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $DESCFUNCAO;
    public $IDPERFIL;
    public $TABPERFIL_FUNCAO_CODFUNCAO;
//End DataSource Variables

//DataSourceClass_Initialize Event @28-BAD869D8
    function clsTABFUNCAO_TABPERFIL_FUNCADataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid TABFUNCAO_TABPERFIL_FUNCA";
        $this->Initialize();
        $this->DESCFUNCAO = new clsField("DESCFUNCAO", ccsText, "");
        $this->IDPERFIL = new clsField("IDPERFIL", ccsFloat, "");
        $this->TABPERFIL_FUNCAO_CODFUNCAO = new clsField("TABPERFIL_FUNCAO_CODFUNCAO", ccsFloat, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @28-2A81966D
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "DESCFUNCAO";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_DESCFUNCAO" => array("DESCFUNCAO", ""), 
            "Sorter_IDPERFIL" => array("IDPERFIL", ""), 
            "Sorter_TABPERFIL_FUNCAO_CODFUNCAO" => array("TABPERFIL_FUNCAO.CODFUNCAO", "")));
    }
//End SetOrder Method

//Prepare Method @28-BC5727BF
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlIDPERFIL", ccsInteger, "", "", $this->Parameters["urlIDPERFIL"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "TABPERFIL_FUNCAO.IDPERFIL", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsInteger),false);
        $this->Where = 
             $this->wp->Criterion[1];
        $this->Where = $this->wp->opAND(false, "( (TABPERFIL_FUNCAO.CODFUNCAO = TABFUNCAO.CODFUNCAO) )", $this->Where);
    }
//End Prepare Method

//Open Method @28-B6720832
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM TABPERFIL_FUNCAO,\n\n" .
        "TABFUNCAO";
        $this->SQL = "SELECT DESCFUNCAO, IDPERFIL, TABPERFIL_FUNCAO.CODFUNCAO AS TABPERFIL_FUNCAO_CODFUNCAO \n\n" .
        "FROM TABPERFIL_FUNCAO,\n\n" .
        "TABFUNCAO {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @28-E085026D
    function SetValues()
    {
        $this->DESCFUNCAO->SetDBValue($this->f("DESCFUNCAO"));
        $this->IDPERFIL->SetDBValue(trim($this->f("IDPERFIL")));
        $this->TABPERFIL_FUNCAO_CODFUNCAO->SetDBValue(trim($this->f("TABPERFIL_FUNCAO_CODFUNCAO")));
    }
//End SetValues Method

} //End TABFUNCAO_TABPERFIL_FUNCADataSource Class @28-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-4F158E0B
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "ManutPerfil.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Include events file @1-F57EFF2A
include("./ManutPerfil_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-2E211496
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$TABPERFIL = new clsRecordTABPERFIL("", $MainPage);
$TABPERFIL_FUNCAO = new clsRecordTABPERFIL_FUNCAO("", $MainPage);
$TABFUNCAO_TABPERFIL_FUNCA = new clsGridTABFUNCAO_TABPERFIL_FUNCA("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->TABPERFIL = & $TABPERFIL;
$MainPage->TABPERFIL_FUNCAO = & $TABPERFIL_FUNCAO;
$MainPage->TABFUNCAO_TABPERFIL_FUNCA = & $TABFUNCAO_TABPERFIL_FUNCA;
$MainPage->rodape = & $rodape;
$TABPERFIL->Initialize();
$TABPERFIL_FUNCAO->Initialize();
$TABFUNCAO_TABPERFIL_FUNCA->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-C8FA6F24
$cabec->Operations();
$TABPERFIL->Operation();
$TABPERFIL_FUNCAO->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-FD605EF7
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($TABPERFIL);
    unset($TABPERFIL_FUNCAO);
    unset($TABFUNCAO_TABPERFIL_FUNCA);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-20CD65F6
$cabec->Show();
$TABPERFIL->Show();
$TABPERFIL_FUNCAO->Show();
$TABFUNCAO_TABPERFIL_FUNCA->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-D6306A68
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($TABPERFIL);
unset($TABPERFIL_FUNCAO);
unset($TABFUNCAO_TABPERFIL_FUNCA);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
