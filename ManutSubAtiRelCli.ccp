<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="3" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="31" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="CADCLI_GRPATI_SUBATI_TABU" wizardCaption="Search CADCLI GRPATI SUBATI TABU " wizardOrientation="Vertical" wizardFormMethod="post" returnPage="ManutSubAtiRelCli.ccp" debugMode="False">
			<Components>
				<ListBox id="33" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="s_SUBATI" wizardCaption="SUBATI" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="SUBATI" boundColumn="SUBATI" textColumn="DESSUB" editable="True" hasErrorCollection="True">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<Button id="32" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="4" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="Faturar" dataSource="SUBATI, CADCLI, GRPATI, TABUNI" name="CADCLI_GRPATI_SUBATI_TABU1" pageSizeLimit="100" wizardCaption="List of CADCLI, GRPATI, SUBATI, TABUNI " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="Nenhuma Sub Atividade foi encontrada">
			<Components>
				<Sorter id="35" visible="True" name="Sorter_SUBATI" column="SUBATI.SUBATI" wizardCaption="SUBATI" wizardSortingType="SimpleDir" wizardControl="SUBATI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="36" visible="True" name="Sorter_DESSUB" column="DESSUB" wizardCaption="DESSUB" wizardSortingType="SimpleDir" wizardControl="DESSUB" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="47" visible="True" name="Sorter_DESATI" column="DESATI" wizardCaption="DESATI" wizardSortingType="SimpleDir" wizardControl="DESATI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="38" visible="True" name="Sorter_DESCLI" column="DESCLI" wizardCaption="DESCLI" wizardSortingType="SimpleDir" wizardControl="DESCLI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="39" visible="True" name="Sorter_CODSET" column="CODSET" wizardCaption="CODSET" wizardSortingType="SimpleDir" wizardControl="CODSET" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="40" visible="True" name="Sorter_LOGRAD" column="LOGRAD" wizardCaption="LOGRAD" wizardSortingType="SimpleDir" wizardControl="LOGRAD" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="41" visible="True" name="Sorter_BAIRRO" column="BAIRRO" wizardCaption="BAIRRO" wizardSortingType="SimpleDir" wizardControl="BAIRRO" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="42" visible="True" name="Sorter_CIDADE" column="CIDADE" wizardCaption="CIDADE" wizardSortingType="SimpleDir" wizardControl="CIDADE" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="43" visible="True" name="Sorter_ESTADO" column="ESTADO" wizardCaption="ESTADO" wizardSortingType="SimpleDir" wizardControl="ESTADO" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="44" visible="True" name="Sorter_TELEFO" column="TELEFO" wizardCaption="TELEFO" wizardSortingType="SimpleDir" wizardControl="TELEFO" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="45" visible="True" name="Sorter_TELFAX" column="TELFAX" wizardCaption="TELFAX" wizardSortingType="SimpleDir" wizardControl="TELFAX" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="46" visible="True" name="Sorter_CONTAT" column="CONTAT" wizardCaption="CONTAT" wizardSortingType="SimpleDir" wizardControl="CONTAT" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="48" visible="True" name="Sorter_DESUNI" column="DESUNI" wizardCaption="DESUNI" wizardSortingType="SimpleDir" wizardControl="DESUNI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="49" visible="True" name="Sorter_DESCRI" column="DESCRI" wizardCaption="DESCRI" wizardSortingType="SimpleDir" wizardControl="DESCRI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Label id="51" fieldSourceType="DBColumn" dataType="Text" html="False" name="SUBATI" fieldSource="SUBATI" wizardCaption="SUBATI" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="53" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESSUB" fieldSource="DESSUB" wizardCaption="DESSUB" wizardSize="35" wizardMaxLength="35" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="65" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESATI" fieldSource="DESATI" wizardCaption="DESATI" wizardSize="35" wizardMaxLength="35" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="56" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESCLI" fieldSource="DESCLI" wizardCaption="DESCLI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="57" fieldSourceType="DBColumn" dataType="Text" html="False" name="CODSET" fieldSource="CODSET" wizardCaption="CODSET" wizardSize="1" wizardMaxLength="1" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="58" fieldSourceType="DBColumn" dataType="Text" html="False" name="LOGRAD" fieldSource="LOGRAD" wizardCaption="LOGRAD" wizardSize="35" wizardMaxLength="35" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="59" fieldSourceType="DBColumn" dataType="Text" html="False" name="BAIRRO" fieldSource="BAIRRO" wizardCaption="BAIRRO" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="60" fieldSourceType="DBColumn" dataType="Text" html="False" name="CIDADE" fieldSource="CIDADE" wizardCaption="CIDADE" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="61" fieldSourceType="DBColumn" dataType="Text" html="False" name="ESTADO" fieldSource="ESTADO" wizardCaption="ESTADO" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="62" fieldSourceType="DBColumn" dataType="Text" html="False" name="TELEFO" fieldSource="TELEFO" wizardCaption="TELEFO" wizardSize="13" wizardMaxLength="13" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="63" fieldSourceType="DBColumn" dataType="Text" html="False" name="TELFAX" fieldSource="TELFAX" wizardCaption="TELFAX" wizardSize="13" wizardMaxLength="13" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="64" fieldSourceType="DBColumn" dataType="Text" html="False" name="CONTAT" fieldSource="CONTAT" wizardCaption="CONTAT" wizardSize="35" wizardMaxLength="35" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="66" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESUNI" fieldSource="DESUNI" wizardCaption="DESUNI" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="67" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESCRI" fieldSource="DESCRI" wizardCaption="DESCRI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="68" size="10" name="Navigator" wizardPagingType="Custom" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardImages="Images" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="True" wizardOfText="of" wizardImagesScheme="Gooseberry" type="Simple">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="34" conditionType="Parameter" useIsNull="False" field="SUBATI.SUBATI" parameterSource="s_SUBATI" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="1"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="5" tableName="SUBATI" posLeft="10" posTop="10" posWidth="95" posHeight="104"/>
				<JoinTable id="6" tableName="CADCLI" posLeft="126" posTop="105" posWidth="115" posHeight="180"/>
				<JoinTable id="7" tableName="GRPATI" posLeft="262" posTop="42" posWidth="95" posHeight="88"/>
				<JoinTable id="8" tableName="TABUNI" posLeft="304" posTop="153" posWidth="95" posHeight="104"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="19" tableLeft="CADCLI" tableRight="TABUNI" fieldLeft="CADCLI.CODUNI" fieldRight="TABUNI.CODUNI" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="20" tableLeft="CADCLI" tableRight="SUBATI" fieldLeft="CADCLI.SUBATI" fieldRight="SUBATI.SUBATI" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="21" tableLeft="GRPATI" tableRight="SUBATI" fieldLeft="GRPATI.GRPATI" fieldRight="SUBATI.GRPATI" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="15" tableName="GRPATI" fieldName="DESATI"/>
				<Field id="17" tableName="CADCLI" fieldName="DESCLI"/>
				<Field id="18" tableName="CADCLI" fieldName="CODSET"/>
				<Field id="22" tableName="CADCLI" fieldName="LOGRAD"/>
				<Field id="23" tableName="CADCLI" fieldName="BAIRRO"/>
				<Field id="24" tableName="CADCLI" fieldName="CIDADE"/>
				<Field id="25" tableName="CADCLI" fieldName="ESTADO"/>
				<Field id="26" tableName="CADCLI" fieldName="TELEFO"/>
				<Field id="27" tableName="CADCLI" fieldName="TELFAX"/>
				<Field id="28" tableName="CADCLI" fieldName="CONTAT"/>
				<Field id="29" tableName="TABUNI" fieldName="DESUNI"/>
				<Field id="30" tableName="TABUNI" fieldName="DESCRI"/>
				<Field id="70" tableName="SUBATI" fieldName="SUBATI.*"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="2" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="ManutSubAtiRelCli.php" forShow="True" url="ManutSubAtiRelCli.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="ManutSubAtiRelCli_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="71" groupID="1"/>
		<Group id="72" groupID="2"/>
		<Group id="73" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="74"/>
			</Actions>
		</Event>
	</Events>
</Page>
