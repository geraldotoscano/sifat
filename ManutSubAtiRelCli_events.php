<?php
//BindEvents Method @1-D40060DD
function BindEvents()
{
    global $CCSEvents;
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//Page_BeforeShow @1-4F403D22
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutSubAtiRelCli; //Compatibility
//End Page_BeforeShow

//Custom Code @74-2A29BDB7
// -------------------------

        include("controle_acesso.php");
        $Tabela = new clsDBfaturar();
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(9,8);
		controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
