<?php
//BindEvents Method @1-2ECA29C6
function BindEvents()
{
    global $SUBATI;
    global $CCSEvents;
    $SUBATI->SUBATI->CCSEvents["BeforeShow"] = "SUBATI_SUBATI_BeforeShow";
    $SUBATI->GRPATI->ds->CCSEvents["BeforeBuildSelect"] = "SUBATI_GRPATI_ds_BeforeBuildSelect";
    $SUBATI->CCSEvents["BeforeShow"] = "SUBATI_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//SUBATI_SUBATI_BeforeShow @11-1242DB53
function SUBATI_SUBATI_BeforeShow(& $sender)
{
    $SUBATI_SUBATI_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $SUBATI; //Compatibility
//End SUBATI_SUBATI_BeforeShow

//Custom Code @17-2A29BDB7
// -------------------------
	if ($SUBATI->Button_Insert->Visible)
	{
// -------------------------
//End Custom Code

//DLookup @17-601F8E6F
       global $DBfaturar;
       $Page = CCGetParentPage($sender);
       $ccs_result = CCDLookUp("max(to_number(subati))+1", "subati", "", $Page->Connections["Faturar"]);
       $ccs_result = intval($ccs_result);
	   if (is_null($ccs_result))
	   {
	   		$ccs_result = 1;//Para o caso da tabela estar vazia;
	   }
       $Component->SetValue($ccs_result);
	}
    // Write your own code here.
// -------------------------
//End Custom Code

//Close SUBATI_SUBATI_BeforeShow @11-007DA746
    return $SUBATI_SUBATI_BeforeShow;
}
//End Close SUBATI_SUBATI_BeforeShow

//DEL  // -------------------------
//DEL     if ( !caracterInvado($Component->GetValue()) )
//DEL     {
//DEL        $SUBATI->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
//DEL     }
//DEL  // -------------------------

//SUBATI_GRPATI_ds_BeforeBuildSelect @13-0C8F4465
function SUBATI_GRPATI_ds_BeforeBuildSelect(& $sender)
{
    $SUBATI_GRPATI_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $SUBATI; //Compatibility
//End SUBATI_GRPATI_ds_BeforeBuildSelect

//Custom Code @22-2A29BDB7
// -------------------------
    if ($SUBATI->GRPATI->DataSource->Order == "")
	{
		$SUBATI->GRPATI->DataSource->Order = "DESATI";
	}
// -------------------------
//End Custom Code

//Close SUBATI_GRPATI_ds_BeforeBuildSelect @13-AD451A42
    return $SUBATI_GRPATI_ds_BeforeBuildSelect;
}
//End Close SUBATI_GRPATI_ds_BeforeBuildSelect

//SUBATI_BeforeShow @4-2D08E5FE
function SUBATI_BeforeShow(& $sender)
{
    $SUBATI_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $SUBATI; //Compatibility
//End SUBATI_BeforeShow

//Custom Code @16-2A29BDB7
// -------------------------
    // Write your own code here.
    global $DBfaturar;
    $Page = CCGetParentPage($sender);
	if ($SUBATI->Button_Delete->Visible)
	{
       $ccs_result = CCDLookUp("coduni", "cadcli", "grpati='".$SUBATI->SUBATI->Value."'", $Page->Connections["Faturar"]);
       //$Component->SetValue($ccs_result);
	   if ($ccs_result != "")
	   {
	      $SUBATI->Button_Delete->Visible = false;
	   }
	}
// -------------------------
//End Custom Code

//Close SUBATI_BeforeShow @4-ABA8A29B
    return $SUBATI_BeforeShow;
}
//End Close SUBATI_BeforeShow

//Page_BeforeShow @1-9EE001E6
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutSubAti; //Compatibility
//End Page_BeforeShow

//Custom Code @23-2A29BDB7
// -------------------------

        include("controle_acesso.php");
        $Tabela = new clsDBfaturar();
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(8);
		controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
