<?php
//Include Common Files @1-121E2811
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "ManutSubServico.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @3-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordSUBSER { //SUBSER Class @4-BA140D54

//Variables @4-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @4-3263BC13
    function clsRecordSUBSER($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record SUBSER/Error";
        $this->DataSource = new clsSUBSERDataSource($this);
        $this->ds = & $this->DataSource;
        $this->InsertAllowed = true;
        $this->UpdateAllowed = true;
        $this->DeleteAllowed = true;
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "SUBSER";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->SUBSER = new clsControl(ccsTextBox, "SUBSER", "Sub Servi�o", ccsText, "", CCGetRequestParam("SUBSER", $Method, NULL), $this);
            $this->DESSUB = new clsControl(ccsTextBox, "DESSUB", "Descri��o", ccsText, "", CCGetRequestParam("DESSUB", $Method, NULL), $this);
            $this->DESSUB->Required = true;
            $this->SUBRED = new clsControl(ccsTextBox, "SUBRED", "Redizido", ccsText, "", CCGetRequestParam("SUBRED", $Method, NULL), $this);
            $this->SUBRED->Required = true;
            $this->GRPSER = new clsControl(ccsListBox, "GRPSER", "Servi�o", ccsText, "", CCGetRequestParam("GRPSER", $Method, NULL), $this);
            $this->GRPSER->DSType = dsTable;
            $this->GRPSER->DataSource = new clsDBFaturar();
            $this->GRPSER->ds = & $this->GRPSER->DataSource;
            $this->GRPSER->DataSource->SQL = "SELECT * \n" .
"FROM GRPSER {SQL_Where} {SQL_OrderBy}";
            list($this->GRPSER->BoundColumn, $this->GRPSER->TextColumn, $this->GRPSER->DBFormat) = array("GRPSER", "DESSER", "");
            $this->GRPSER->Required = true;
            $this->UNIDAD = new clsControl(ccsTextBox, "UNIDAD", "Unidade", ccsText, "", CCGetRequestParam("UNIDAD", $Method, NULL), $this);
            $this->UNIDAD->Required = true;
            $this->Button_Insert = new clsButton("Button_Insert", $Method, $this);
            $this->Button_Update = new clsButton("Button_Update", $Method, $this);
            $this->Button_Delete = new clsButton("Button_Delete", $Method, $this);
            $this->Button_Cancel = new clsButton("Button_Cancel", $Method, $this);
        }
    }
//End Class_Initialize Event

//Initialize Method @4-28B197A2
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->DataSource->Parameters["urlSUBSER"] = CCGetFromGet("SUBSER", NULL);
    }
//End Initialize Method

//Validate Method @4-3A781646
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->SUBSER->Validate() && $Validation);
        $Validation = ($this->DESSUB->Validate() && $Validation);
        $Validation = ($this->SUBRED->Validate() && $Validation);
        $Validation = ($this->GRPSER->Validate() && $Validation);
        $Validation = ($this->UNIDAD->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->SUBSER->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DESSUB->Errors->Count() == 0);
        $Validation =  $Validation && ($this->SUBRED->Errors->Count() == 0);
        $Validation =  $Validation && ($this->GRPSER->Errors->Count() == 0);
        $Validation =  $Validation && ($this->UNIDAD->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @4-035D452A
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->SUBSER->Errors->Count());
        $errors = ($errors || $this->DESSUB->Errors->Count());
        $errors = ($errors || $this->SUBRED->Errors->Count());
        $errors = ($errors || $this->GRPSER->Errors->Count());
        $errors = ($errors || $this->UNIDAD->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @4-CDC26E3B
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->DataSource->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->DataSource->AllParametersSet;
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = $this->EditMode ? "Button_Update" : "Button_Insert";
            if($this->Button_Insert->Pressed) {
                $this->PressedButton = "Button_Insert";
            } else if($this->Button_Update->Pressed) {
                $this->PressedButton = "Button_Update";
            } else if($this->Button_Delete->Pressed) {
                $this->PressedButton = "Button_Delete";
            } else if($this->Button_Cancel->Pressed) {
                $this->PressedButton = "Button_Cancel";
            }
        }
        $Redirect = "CadSubServico.php" . "?" . CCGetQueryString("QueryString", array("ccsForm", "SUBSER"));
        if($this->PressedButton == "Button_Delete") {
            if(!CCGetEvent($this->Button_Delete->CCSEvents, "OnClick", $this->Button_Delete) || !$this->DeleteRow()) {
                $Redirect = "";
            }
        } else if($this->PressedButton == "Button_Cancel") {
            if(!CCGetEvent($this->Button_Cancel->CCSEvents, "OnClick", $this->Button_Cancel)) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button_Insert") {
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick", $this->Button_Insert) || !$this->InsertRow()) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "Button_Update") {
                if(!CCGetEvent($this->Button_Update->CCSEvents, "OnClick", $this->Button_Update) || !$this->UpdateRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
        if ($Redirect)
            $this->DataSource->close();
    }
//End Operation Method

//InsertRow Method @4-7A6C058B
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert", $this);
        if(!$this->InsertAllowed) return false;
        $this->DataSource->SUBSER->SetValue($this->SUBSER->GetValue(true));
        $this->DataSource->DESSUB->SetValue($this->DESSUB->GetValue(true));
        $this->DataSource->SUBRED->SetValue($this->SUBRED->GetValue(true));
        $this->DataSource->GRPSER->SetValue($this->GRPSER->GetValue(true));
        $this->DataSource->UNIDAD->SetValue($this->UNIDAD->GetValue(true));
        $this->DataSource->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert", $this);
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//UpdateRow Method @4-6C2D6080
    function UpdateRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeUpdate", $this);
        if(!$this->UpdateAllowed) return false;
        $this->DataSource->SUBSER->SetValue($this->SUBSER->GetValue(true));
        $this->DataSource->DESSUB->SetValue($this->DESSUB->GetValue(true));
        $this->DataSource->SUBRED->SetValue($this->SUBRED->GetValue(true));
        $this->DataSource->GRPSER->SetValue($this->GRPSER->GetValue(true));
        $this->DataSource->UNIDAD->SetValue($this->UNIDAD->GetValue(true));
        $this->DataSource->Update();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterUpdate", $this);
        return (!$this->CheckErrors());
    }
//End UpdateRow Method

//DeleteRow Method @4-299D98C3
    function DeleteRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDelete", $this);
        if(!$this->DeleteAllowed) return false;
        $this->DataSource->Delete();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDelete", $this);
        return (!$this->CheckErrors());
    }
//End DeleteRow Method

//Show Method @4-DCC79290
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->GRPSER->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->DataSource->Errors->Count()){
                $this->Errors->AddErrors($this->DataSource->Errors);
                $this->DataSource->Errors->clear();
            }
            $this->DataSource->Open();
            if($this->DataSource->Errors->Count() == 0 && $this->DataSource->next_record()) {
                $this->DataSource->SetValues();
                if(!$this->FormSubmitted){
                    $this->SUBSER->SetValue($this->DataSource->SUBSER->GetValue());
                    $this->DESSUB->SetValue($this->DataSource->DESSUB->GetValue());
                    $this->SUBRED->SetValue($this->DataSource->SUBRED->GetValue());
                    $this->GRPSER->SetValue($this->DataSource->GRPSER->GetValue());
                    $this->UNIDAD->SetValue($this->DataSource->UNIDAD->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->SUBSER->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DESSUB->Errors->ToString());
            $Error = ComposeStrings($Error, $this->SUBRED->Errors->ToString());
            $Error = ComposeStrings($Error, $this->GRPSER->Errors->ToString());
            $Error = ComposeStrings($Error, $this->UNIDAD->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataSource->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Insert->Visible = !$this->EditMode && $this->InsertAllowed;
        $this->Button_Update->Visible = $this->EditMode && $this->UpdateAllowed;
        $this->Button_Delete->Visible = $this->EditMode && $this->DeleteAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->SUBSER->Show();
        $this->DESSUB->Show();
        $this->SUBRED->Show();
        $this->GRPSER->Show();
        $this->UNIDAD->Show();
        $this->Button_Insert->Show();
        $this->Button_Update->Show();
        $this->Button_Delete->Show();
        $this->Button_Cancel->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

} //End SUBSER Class @4-FCB6E20C

class clsSUBSERDataSource extends clsDBFaturar {  //SUBSERDataSource Class @4-79BDC49B

//DataSource Variables @4-47C481D1
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $InsertParameters;
    public $UpdateParameters;
    public $DeleteParameters;
    public $wp;
    public $AllParametersSet;

    public $InsertFields = array();
    public $UpdateFields = array();

    // Datasource fields
    public $SUBSER;
    public $DESSUB;
    public $SUBRED;
    public $GRPSER;
    public $UNIDAD;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-3E1EB54F
    function clsSUBSERDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Record SUBSER/Error";
        $this->Initialize();
        $this->SUBSER = new clsField("SUBSER", ccsText, "");
        $this->DESSUB = new clsField("DESSUB", ccsText, "");
        $this->SUBRED = new clsField("SUBRED", ccsText, "");
        $this->GRPSER = new clsField("GRPSER", ccsText, "");
        $this->UNIDAD = new clsField("UNIDAD", ccsText, "");

        $this->InsertFields["SUBSER"] = array("Name" => "SUBSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DESSUB"] = array("Name" => "DESSUB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["SUBRED"] = array("Name" => "SUBRED", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["GRPSER"] = array("Name" => "GRPSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["UNIDAD"] = array("Name" => "UNIDAD", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["SUBSER"] = array("Name" => "SUBSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["DESSUB"] = array("Name" => "DESSUB", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["SUBRED"] = array("Name" => "SUBRED", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["GRPSER"] = array("Name" => "GRPSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["UNIDAD"] = array("Name" => "UNIDAD", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
    }
//End DataSourceClass_Initialize Event

//Prepare Method @4-278A3E2A
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlSUBSER", ccsText, "", "", $this->Parameters["urlSUBSER"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "SUBSER", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @4-06C62D01
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->SQL = "SELECT * \n\n" .
        "FROM SUBSER {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @4-8565EE80
    function SetValues()
    {
        $this->SUBSER->SetDBValue($this->f("SUBSER"));
        $this->DESSUB->SetDBValue($this->f("DESSUB"));
        $this->SUBRED->SetDBValue($this->f("SUBRED"));
        $this->GRPSER->SetDBValue($this->f("GRPSER"));
        $this->UNIDAD->SetDBValue($this->f("UNIDAD"));
    }
//End SetValues Method

//Insert Method @4-A8A6091B
    function Insert()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert", $this->Parent);
        $this->InsertFields["SUBSER"]["Value"] = $this->SUBSER->GetDBValue(true);
        $this->InsertFields["DESSUB"]["Value"] = $this->DESSUB->GetDBValue(true);
        $this->InsertFields["SUBRED"]["Value"] = $this->SUBRED->GetDBValue(true);
        $this->InsertFields["GRPSER"]["Value"] = $this->GRPSER->GetDBValue(true);
        $this->InsertFields["UNIDAD"]["Value"] = $this->UNIDAD->GetDBValue(true);
        $this->SQL = CCBuildInsert("SUBSER", $this->InsertFields, $this);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert", $this->Parent);
        }
    }
//End Insert Method

//Update Method @4-402834B9
    function Update()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildUpdate", $this->Parent);
        $this->UpdateFields["SUBSER"]["Value"] = $this->SUBSER->GetDBValue(true);
        $this->UpdateFields["DESSUB"]["Value"] = $this->DESSUB->GetDBValue(true);
        $this->UpdateFields["SUBRED"]["Value"] = $this->SUBRED->GetDBValue(true);
        $this->UpdateFields["GRPSER"]["Value"] = $this->GRPSER->GetDBValue(true);
        $this->UpdateFields["UNIDAD"]["Value"] = $this->UNIDAD->GetDBValue(true);
        $this->SQL = CCBuildUpdate("SUBSER", $this->UpdateFields, $this);
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteUpdate", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteUpdate", $this->Parent);
        }
    }
//End Update Method

//Delete Method @4-AE35A52A
    function Delete()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildDelete", $this->Parent);
        $this->SQL = "DELETE FROM SUBSER";
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteDelete", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteDelete", $this->Parent);
        }
    }
//End Delete Method

} //End SUBSERDataSource Class @4-FCB6E20C

//Include Page implementation @2-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-A3D74B23
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "ManutSubServico.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-7FACF37D
CCSecurityRedirect("1;3", "");
//End Authenticate User

//Include events file @1-EB3E9367
include("./ManutSubServico_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-2875EFF0
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$SUBSER = new clsRecordSUBSER("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->SUBSER = & $SUBSER;
$MainPage->rodape = & $rodape;
$SUBSER->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-48B8C9F2
$cabec->Operations();
$SUBSER->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-69074A4D
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($SUBSER);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-DC9F33E9
$cabec->Show();
$SUBSER->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-A00029D8
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($SUBSER);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
