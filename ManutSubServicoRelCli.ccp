<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="43" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="CADCLI_MOVSER_SUBSER_TABU" wizardCaption="Search CADCLI MOVSER SUBSER TABU " wizardOrientation="Vertical" wizardFormMethod="post" returnPage="ManutSubServicoRelCli.ccp" debugMode="False">
			<Components>
				<ListBox id="45" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="s_SUBSER" wizardCaption="SUBSER" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="SUBSER" boundColumn="SUBSER" textColumn="DESSUB" editable="True" hasErrorCollection="True">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<ListBox id="46" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="s_DESUNI" wizardCaption="DESUNI" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="TABUNI" boundColumn="CODUNI" textColumn="DESUNI" editable="True" hasErrorCollection="True">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<Button id="44" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="4" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="Faturar" dataSource="SUBSER, CADCLI, MOVSER, TABUNI" name="CADCLI_MOVSER_SUBSER_TABU1" pageSizeLimit="100" wizardCaption="List of CADCLI, MOVSER, SUBSER, TABUNI " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="True" wizardNoRecords="Nenhum Sub Serviço foi encontrado">
			<Components>
				<Label id="47" fieldSourceType="DBColumn" dataType="Text" html="False" name="CADCLI_MOVSER_SUBSER_TABU1_TotalRecords" wizardUseTemplateBlock="False" editable="False">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Retrieve number of records" actionCategory="Database" id="48"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Panel id="51" visible="True" name="Header_SUBSER">
					<Components>
						<Sorter id="52" visible="True" name="Sorter_SUBSER" column="SUBSER.SUBSER" wizardCaption="SUBSER" wizardSortingType="SimpleDir" wizardControl="SUBSER" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="53" visible="True" name="Header_DESSUB">
					<Components>
						<Sorter id="54" visible="True" name="Sorter_DESSUB" column="DESSUB" wizardCaption="DESSUB" wizardSortingType="SimpleDir" wizardControl="DESSUB" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="55" visible="True" name="Header_DESUNI">
					<Components>
						<Sorter id="56" visible="True" name="Sorter_DESUNI" column="DESUNI" wizardCaption="DESUNI" wizardSortingType="SimpleDir" wizardControl="DESUNI" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="57" visible="True" name="Header_DESCRI">
					<Components>
						<Sorter id="58" visible="True" name="Sorter_DESCRI" column="DESCRI" wizardCaption="DESCRI" wizardSortingType="SimpleDir" wizardControl="DESCRI" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="59" visible="True" name="Header_SUBRED">
					<Components>
						<Sorter id="60" visible="True" name="Sorter_SUBRED" column="SUBRED" wizardCaption="SUBRED" wizardSortingType="SimpleDir" wizardControl="SUBRED" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="61" visible="True" name="Header_UNIDAD">
					<Components>
						<Sorter id="62" visible="True" name="Sorter_UNIDAD" column="UNIDAD" wizardCaption="UNIDAD" wizardSortingType="SimpleDir" wizardControl="UNIDAD" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="63" visible="True" name="Header_CADCLI_CODCLI">
					<Components>
						<Sorter id="64" visible="True" name="Sorter_CADCLI_CODCLI" column="CADCLI.CODCLI" wizardCaption="CODCLI" wizardSortingType="SimpleDir" wizardControl="CADCLI_CODCLI" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="65" visible="True" name="Header_DESCLI">
					<Components>
						<Sorter id="66" visible="True" name="Sorter_DESCLI" column="DESCLI" wizardCaption="DESCLI" wizardSortingType="SimpleDir" wizardControl="DESCLI" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="67" visible="True" name="Header_CODSET">
					<Components>
						<Sorter id="68" visible="True" name="Sorter_CODSET" column="CODSET" wizardCaption="CODSET" wizardSortingType="SimpleDir" wizardControl="CODSET" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="69" visible="True" name="Header_CGCCPF">
					<Components>
						<Sorter id="70" visible="True" name="Sorter_CGCCPF" column="CGCCPF" wizardCaption="CGCCPF" wizardSortingType="SimpleDir" wizardControl="CGCCPF" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="71" visible="True" name="Header_LOGRAD">
					<Components>
						<Sorter id="72" visible="True" name="Sorter_LOGRAD" column="LOGRAD" wizardCaption="LOGRAD" wizardSortingType="SimpleDir" wizardControl="LOGRAD" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="73" visible="True" name="Header_BAIRRO">
					<Components>
						<Sorter id="74" visible="True" name="Sorter_BAIRRO" column="BAIRRO" wizardCaption="BAIRRO" wizardSortingType="SimpleDir" wizardControl="BAIRRO" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="75" visible="True" name="Header_CIDADE">
					<Components>
						<Sorter id="76" visible="True" name="Sorter_CIDADE" column="CIDADE" wizardCaption="CIDADE" wizardSortingType="SimpleDir" wizardControl="CIDADE" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="77" visible="True" name="Header_ESTADO">
					<Components>
						<Sorter id="78" visible="True" name="Sorter_ESTADO" column="ESTADO" wizardCaption="ESTADO" wizardSortingType="SimpleDir" wizardControl="ESTADO" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="79" visible="True" name="Header_TELEFO">
					<Components>
						<Sorter id="80" visible="True" name="Sorter_TELEFO" column="TELEFO" wizardCaption="TELEFO" wizardSortingType="SimpleDir" wizardControl="TELEFO" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="81" visible="True" name="Header_TELFAX">
					<Components>
						<Sorter id="82" visible="True" name="Sorter_TELFAX" column="TELFAX" wizardCaption="TELFAX" wizardSortingType="SimpleDir" wizardControl="TELFAX" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="83" visible="True" name="Header_CONTAT">
					<Components>
						<Sorter id="84" visible="True" name="Sorter_CONTAT" column="CONTAT" wizardCaption="CONTAT" wizardSortingType="SimpleDir" wizardControl="CONTAT" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="85" visible="True" name="Header_QTDMED">
					<Components>
						<Sorter id="86" visible="True" name="Sorter_QTDMED" column="QTDMED" wizardCaption="QTDMED" wizardSortingType="SimpleDir" wizardControl="QTDMED" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="88" visible="True" name="Data_SUBSER">
					<Components>
						<Label id="89" fieldSourceType="DBColumn" dataType="Text" html="False" name="SUBSER" fieldSource="SUBSER" wizardCaption="SUBSER" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="91" visible="True" name="Data_DESSUB">
					<Components>
						<Label id="92" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESSUB" fieldSource="DESSUB" wizardCaption="DESSUB" wizardSize="50" wizardMaxLength="75" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="93" visible="True" name="Data_DESUNI">
					<Components>
						<Label id="94" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESUNI" fieldSource="DESUNI" wizardCaption="DESUNI" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="95" visible="True" name="Data_DESCRI">
					<Components>
						<Label id="96" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESCRI" fieldSource="DESCRI" wizardCaption="DESCRI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="98" visible="True" name="Data_SUBRED">
					<Components>
						<Label id="99" fieldSourceType="DBColumn" dataType="Text" html="False" name="SUBRED" fieldSource="SUBRED" wizardCaption="SUBRED" wizardSize="50" wizardMaxLength="60" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="101" visible="True" name="Data_UNIDAD">
					<Components>
						<Label id="102" fieldSourceType="DBColumn" dataType="Text" html="False" name="UNIDAD" fieldSource="UNIDAD" wizardCaption="UNIDAD" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="103" visible="True" name="Data_CADCLI_CODCLI">
					<Components>
						<Label id="104" fieldSourceType="DBColumn" dataType="Text" html="False" name="CADCLI_CODCLI" fieldSource="CADCLI_CODCLI" wizardCaption="CODCLI" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="105" visible="True" name="Data_DESCLI">
					<Components>
						<Label id="106" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESCLI" fieldSource="DESCLI" wizardCaption="DESCLI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="107" visible="True" name="Data_CODSET">
					<Components>
						<Label id="108" fieldSourceType="DBColumn" dataType="Text" html="False" name="CODSET" fieldSource="CODSET" wizardCaption="CODSET" wizardSize="1" wizardMaxLength="1" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="109" visible="True" name="Data_CGCCPF">
					<Components>
						<Label id="110" fieldSourceType="DBColumn" dataType="Text" html="False" name="CGCCPF" fieldSource="CGCCPF" wizardCaption="CGCCPF" wizardSize="14" wizardMaxLength="14" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="128"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="111" visible="True" name="Data_LOGRAD">
					<Components>
						<Label id="112" fieldSourceType="DBColumn" dataType="Text" html="False" name="LOGRAD" fieldSource="LOGRAD" wizardCaption="LOGRAD" wizardSize="35" wizardMaxLength="35" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="113" visible="True" name="Data_BAIRRO">
					<Components>
						<Label id="114" fieldSourceType="DBColumn" dataType="Text" html="False" name="BAIRRO" fieldSource="BAIRRO" wizardCaption="BAIRRO" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="115" visible="True" name="Data_CIDADE">
					<Components>
						<Label id="116" fieldSourceType="DBColumn" dataType="Text" html="False" name="CIDADE" fieldSource="CIDADE" wizardCaption="CIDADE" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="117" visible="True" name="Data_ESTADO">
					<Components>
						<Label id="118" fieldSourceType="DBColumn" dataType="Text" html="False" name="ESTADO" fieldSource="ESTADO" wizardCaption="ESTADO" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="119" visible="True" name="Data_TELEFO">
					<Components>
						<Label id="120" fieldSourceType="DBColumn" dataType="Text" html="False" name="TELEFO" fieldSource="TELEFO" wizardCaption="TELEFO" wizardSize="13" wizardMaxLength="13" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="121" visible="True" name="Data_TELFAX">
					<Components>
						<Label id="122" fieldSourceType="DBColumn" dataType="Text" html="False" name="TELFAX" fieldSource="TELFAX" wizardCaption="TELFAX" wizardSize="13" wizardMaxLength="13" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="123" visible="True" name="Data_CONTAT">
					<Components>
						<Label id="124" fieldSourceType="DBColumn" dataType="Text" html="False" name="CONTAT" fieldSource="CONTAT" wizardCaption="CONTAT" wizardSize="35" wizardMaxLength="35" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Panel id="125" visible="True" name="Data_QTDMED">
					<Components>
						<Label id="126" fieldSourceType="DBColumn" dataType="Float" html="False" name="QTDMED" fieldSource="QTDMED" wizardCaption="QTDMED" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Label>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Panel>
				<Navigator id="127" size="10" name="Navigator" wizardPagingType="Custom" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardImages="Images" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="True" wizardOfText="of" wizardImagesScheme="Apricot" type="Simple">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="49" conditionType="Parameter" useIsNull="False" field="SUBSER.SUBSER" parameterSource="s_SUBSER" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="1"/>
				<TableParameter id="50" conditionType="Parameter" useIsNull="False" field="TABUNI.DESUNI" parameterSource="s_DESUNI" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="2"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="5" tableName="SUBSER" posLeft="10" posTop="10" posWidth="115" posHeight="152"/>
				<JoinTable id="6" tableName="CADCLI" posLeft="282" posTop="135" posWidth="115" posHeight="180"/>
				<JoinTable id="7" tableName="MOVSER" posLeft="146" posTop="64" posWidth="115" posHeight="180"/>
				<JoinTable id="8" tableName="TABUNI" posLeft="418" posTop="10" posWidth="95" posHeight="104"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="39" tableLeft="CADCLI" tableRight="MOVSER" fieldLeft="CADCLI.CODCLI" fieldRight="MOVSER.CODCLI" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="40" tableLeft="CADCLI" tableRight="TABUNI" fieldLeft="CADCLI.CODUNI" fieldRight="TABUNI.CODUNI" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="41" tableLeft="MOVSER" tableRight="SUBSER" fieldLeft="MOVSER.SUBSER" fieldRight="SUBSER.SUBSER" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="22" tableName="SUBSER" fieldName="SUBSER.*"/>
				<Field id="23" tableName="CADCLI" fieldName="CADCLI.CODCLI" alias="CADCLI_CODCLI"/>
				<Field id="27" tableName="CADCLI" fieldName="DESCLI"/>
				<Field id="28" tableName="CADCLI" fieldName="CODSET"/>
				<Field id="29" tableName="CADCLI" fieldName="CGCCPF"/>
				<Field id="30" tableName="CADCLI" fieldName="LOGRAD"/>
				<Field id="31" tableName="CADCLI" fieldName="BAIRRO"/>
				<Field id="32" tableName="CADCLI" fieldName="CIDADE"/>
				<Field id="33" tableName="CADCLI" fieldName="ESTADO"/>
				<Field id="34" tableName="CADCLI" fieldName="TELEFO"/>
				<Field id="35" tableName="CADCLI" fieldName="TELFAX"/>
				<Field id="36" tableName="CADCLI" fieldName="CONTAT"/>
				<Field id="37" tableName="TABUNI" fieldName="DESUNI"/>
				<Field id="38" tableName="TABUNI" fieldName="DESCRI"/>
				<Field id="42" tableName="MOVSER" fieldName="QTDMED"/>
				<Field id="87" tableName="SUBSER" fieldName="SUBSER" alias="SUBSER"/>
				<Field id="90" tableName="SUBSER" fieldName="DESSUB" alias="DESSUB"/>
				<Field id="97" tableName="SUBSER" fieldName="SUBRED" alias="SUBRED"/>
				<Field id="100" tableName="SUBSER" fieldName="UNIDAD" alias="UNIDAD"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="PHPTemplates" name="ManutSubServicoRelCli_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Code" language="PHPTemplates" name="ManutSubServicoRelCli.php" forShow="True" url="ManutSubServicoRelCli.php" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="129" groupID="1"/>
		<Group id="130" groupID="2"/>
		<Group id="131" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="132"/>
			</Actions>
		</Event>
	</Events>
</Page>
