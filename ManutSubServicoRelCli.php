<?php
//Include Common Files @1-443C6E4A
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "ManutSubServicoRelCli.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordCADCLI_MOVSER_SUBSER_TABU { //CADCLI_MOVSER_SUBSER_TABU Class @43-F9869684

//Variables @43-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @43-38243E90
    function clsRecordCADCLI_MOVSER_SUBSER_TABU($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record CADCLI_MOVSER_SUBSER_TABU/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "CADCLI_MOVSER_SUBSER_TABU";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_SUBSER = new clsControl(ccsListBox, "s_SUBSER", "s_SUBSER", ccsText, "", CCGetRequestParam("s_SUBSER", $Method, NULL), $this);
            $this->s_SUBSER->DSType = dsTable;
            $this->s_SUBSER->DataSource = new clsDBFaturar();
            $this->s_SUBSER->ds = & $this->s_SUBSER->DataSource;
            $this->s_SUBSER->DataSource->SQL = "SELECT * \n" .
"FROM SUBSER {SQL_Where} {SQL_OrderBy}";
            list($this->s_SUBSER->BoundColumn, $this->s_SUBSER->TextColumn, $this->s_SUBSER->DBFormat) = array("SUBSER", "DESSUB", "");
            $this->s_DESUNI = new clsControl(ccsListBox, "s_DESUNI", "s_DESUNI", ccsText, "", CCGetRequestParam("s_DESUNI", $Method, NULL), $this);
            $this->s_DESUNI->DSType = dsTable;
            $this->s_DESUNI->DataSource = new clsDBFaturar();
            $this->s_DESUNI->ds = & $this->s_DESUNI->DataSource;
            $this->s_DESUNI->DataSource->SQL = "SELECT * \n" .
"FROM TABUNI {SQL_Where} {SQL_OrderBy}";
            list($this->s_DESUNI->BoundColumn, $this->s_DESUNI->TextColumn, $this->s_DESUNI->DBFormat) = array("CODUNI", "DESUNI", "");
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
        }
    }
//End Class_Initialize Event

//Validate Method @43-95643537
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_SUBSER->Validate() && $Validation);
        $Validation = ($this->s_DESUNI->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->s_SUBSER->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_DESUNI->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @43-8897A273
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_SUBSER->Errors->Count());
        $errors = ($errors || $this->s_DESUNI->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @43-4F0DB05B
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "ManutSubServicoRelCli.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                $Redirect = "ManutSubServicoRelCli.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @43-6F84E66C
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->s_SUBSER->Prepare();
        $this->s_DESUNI->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->s_SUBSER->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_DESUNI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_SUBSER->Show();
        $this->s_DESUNI->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End CADCLI_MOVSER_SUBSER_TABU Class @43-FCB6E20C

class clsGridCADCLI_MOVSER_SUBSER_TABU1 { //CADCLI_MOVSER_SUBSER_TABU1 class @4-788E99FD

//Variables @4-89FA139F

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_SUBSER;
    public $Sorter_DESSUB;
    public $Sorter_DESUNI;
    public $Sorter_DESCRI;
    public $Sorter_SUBRED;
    public $Sorter_UNIDAD;
    public $Sorter_CADCLI_CODCLI;
    public $Sorter_DESCLI;
    public $Sorter_CODSET;
    public $Sorter_CGCCPF;
    public $Sorter_LOGRAD;
    public $Sorter_BAIRRO;
    public $Sorter_CIDADE;
    public $Sorter_ESTADO;
    public $Sorter_TELEFO;
    public $Sorter_TELFAX;
    public $Sorter_CONTAT;
    public $Sorter_QTDMED;
//End Variables

//Class_Initialize Event @4-5536D8EA
    function clsGridCADCLI_MOVSER_SUBSER_TABU1($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "CADCLI_MOVSER_SUBSER_TABU1";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid CADCLI_MOVSER_SUBSER_TABU1";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clsCADCLI_MOVSER_SUBSER_TABU1DataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 10;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("CADCLI_MOVSER_SUBSER_TABU1Order", "");
        $this->SorterDirection = CCGetParam("CADCLI_MOVSER_SUBSER_TABU1Dir", "");

        $this->Data_SUBSER = new clsPanel("Data_SUBSER", $this);
        $this->SUBSER = new clsControl(ccsLabel, "SUBSER", "SUBSER", ccsText, "", CCGetRequestParam("SUBSER", ccsGet, NULL), $this);
        $this->Data_DESSUB = new clsPanel("Data_DESSUB", $this);
        $this->DESSUB = new clsControl(ccsLabel, "DESSUB", "DESSUB", ccsText, "", CCGetRequestParam("DESSUB", ccsGet, NULL), $this);
        $this->Data_DESUNI = new clsPanel("Data_DESUNI", $this);
        $this->DESUNI = new clsControl(ccsLabel, "DESUNI", "DESUNI", ccsText, "", CCGetRequestParam("DESUNI", ccsGet, NULL), $this);
        $this->Data_DESCRI = new clsPanel("Data_DESCRI", $this);
        $this->DESCRI = new clsControl(ccsLabel, "DESCRI", "DESCRI", ccsText, "", CCGetRequestParam("DESCRI", ccsGet, NULL), $this);
        $this->Data_SUBRED = new clsPanel("Data_SUBRED", $this);
        $this->SUBRED = new clsControl(ccsLabel, "SUBRED", "SUBRED", ccsText, "", CCGetRequestParam("SUBRED", ccsGet, NULL), $this);
        $this->Data_UNIDAD = new clsPanel("Data_UNIDAD", $this);
        $this->UNIDAD = new clsControl(ccsLabel, "UNIDAD", "UNIDAD", ccsText, "", CCGetRequestParam("UNIDAD", ccsGet, NULL), $this);
        $this->Data_CADCLI_CODCLI = new clsPanel("Data_CADCLI_CODCLI", $this);
        $this->CADCLI_CODCLI = new clsControl(ccsLabel, "CADCLI_CODCLI", "CADCLI_CODCLI", ccsText, "", CCGetRequestParam("CADCLI_CODCLI", ccsGet, NULL), $this);
        $this->Data_DESCLI = new clsPanel("Data_DESCLI", $this);
        $this->DESCLI = new clsControl(ccsLabel, "DESCLI", "DESCLI", ccsText, "", CCGetRequestParam("DESCLI", ccsGet, NULL), $this);
        $this->Data_CODSET = new clsPanel("Data_CODSET", $this);
        $this->CODSET = new clsControl(ccsLabel, "CODSET", "CODSET", ccsText, "", CCGetRequestParam("CODSET", ccsGet, NULL), $this);
        $this->Data_CGCCPF = new clsPanel("Data_CGCCPF", $this);
        $this->CGCCPF = new clsControl(ccsLabel, "CGCCPF", "CGCCPF", ccsText, "", CCGetRequestParam("CGCCPF", ccsGet, NULL), $this);
        $this->Data_LOGRAD = new clsPanel("Data_LOGRAD", $this);
        $this->LOGRAD = new clsControl(ccsLabel, "LOGRAD", "LOGRAD", ccsText, "", CCGetRequestParam("LOGRAD", ccsGet, NULL), $this);
        $this->Data_BAIRRO = new clsPanel("Data_BAIRRO", $this);
        $this->BAIRRO = new clsControl(ccsLabel, "BAIRRO", "BAIRRO", ccsText, "", CCGetRequestParam("BAIRRO", ccsGet, NULL), $this);
        $this->Data_CIDADE = new clsPanel("Data_CIDADE", $this);
        $this->CIDADE = new clsControl(ccsLabel, "CIDADE", "CIDADE", ccsText, "", CCGetRequestParam("CIDADE", ccsGet, NULL), $this);
        $this->Data_ESTADO = new clsPanel("Data_ESTADO", $this);
        $this->ESTADO = new clsControl(ccsLabel, "ESTADO", "ESTADO", ccsText, "", CCGetRequestParam("ESTADO", ccsGet, NULL), $this);
        $this->Data_TELEFO = new clsPanel("Data_TELEFO", $this);
        $this->TELEFO = new clsControl(ccsLabel, "TELEFO", "TELEFO", ccsText, "", CCGetRequestParam("TELEFO", ccsGet, NULL), $this);
        $this->Data_TELFAX = new clsPanel("Data_TELFAX", $this);
        $this->TELFAX = new clsControl(ccsLabel, "TELFAX", "TELFAX", ccsText, "", CCGetRequestParam("TELFAX", ccsGet, NULL), $this);
        $this->Data_CONTAT = new clsPanel("Data_CONTAT", $this);
        $this->CONTAT = new clsControl(ccsLabel, "CONTAT", "CONTAT", ccsText, "", CCGetRequestParam("CONTAT", ccsGet, NULL), $this);
        $this->Data_QTDMED = new clsPanel("Data_QTDMED", $this);
        $this->QTDMED = new clsControl(ccsLabel, "QTDMED", "QTDMED", ccsFloat, "", CCGetRequestParam("QTDMED", ccsGet, NULL), $this);
        $this->CADCLI_MOVSER_SUBSER_TABU1_TotalRecords = new clsControl(ccsLabel, "CADCLI_MOVSER_SUBSER_TABU1_TotalRecords", "CADCLI_MOVSER_SUBSER_TABU1_TotalRecords", ccsText, "", CCGetRequestParam("CADCLI_MOVSER_SUBSER_TABU1_TotalRecords", ccsGet, NULL), $this);
        $this->Header_SUBSER = new clsPanel("Header_SUBSER", $this);
        $this->Sorter_SUBSER = new clsSorter($this->ComponentName, "Sorter_SUBSER", $FileName, $this);
        $this->Header_DESSUB = new clsPanel("Header_DESSUB", $this);
        $this->Sorter_DESSUB = new clsSorter($this->ComponentName, "Sorter_DESSUB", $FileName, $this);
        $this->Header_DESUNI = new clsPanel("Header_DESUNI", $this);
        $this->Sorter_DESUNI = new clsSorter($this->ComponentName, "Sorter_DESUNI", $FileName, $this);
        $this->Header_DESCRI = new clsPanel("Header_DESCRI", $this);
        $this->Sorter_DESCRI = new clsSorter($this->ComponentName, "Sorter_DESCRI", $FileName, $this);
        $this->Header_SUBRED = new clsPanel("Header_SUBRED", $this);
        $this->Sorter_SUBRED = new clsSorter($this->ComponentName, "Sorter_SUBRED", $FileName, $this);
        $this->Header_UNIDAD = new clsPanel("Header_UNIDAD", $this);
        $this->Sorter_UNIDAD = new clsSorter($this->ComponentName, "Sorter_UNIDAD", $FileName, $this);
        $this->Header_CADCLI_CODCLI = new clsPanel("Header_CADCLI_CODCLI", $this);
        $this->Sorter_CADCLI_CODCLI = new clsSorter($this->ComponentName, "Sorter_CADCLI_CODCLI", $FileName, $this);
        $this->Header_DESCLI = new clsPanel("Header_DESCLI", $this);
        $this->Sorter_DESCLI = new clsSorter($this->ComponentName, "Sorter_DESCLI", $FileName, $this);
        $this->Header_CODSET = new clsPanel("Header_CODSET", $this);
        $this->Sorter_CODSET = new clsSorter($this->ComponentName, "Sorter_CODSET", $FileName, $this);
        $this->Header_CGCCPF = new clsPanel("Header_CGCCPF", $this);
        $this->Sorter_CGCCPF = new clsSorter($this->ComponentName, "Sorter_CGCCPF", $FileName, $this);
        $this->Header_LOGRAD = new clsPanel("Header_LOGRAD", $this);
        $this->Sorter_LOGRAD = new clsSorter($this->ComponentName, "Sorter_LOGRAD", $FileName, $this);
        $this->Header_BAIRRO = new clsPanel("Header_BAIRRO", $this);
        $this->Sorter_BAIRRO = new clsSorter($this->ComponentName, "Sorter_BAIRRO", $FileName, $this);
        $this->Header_CIDADE = new clsPanel("Header_CIDADE", $this);
        $this->Sorter_CIDADE = new clsSorter($this->ComponentName, "Sorter_CIDADE", $FileName, $this);
        $this->Header_ESTADO = new clsPanel("Header_ESTADO", $this);
        $this->Sorter_ESTADO = new clsSorter($this->ComponentName, "Sorter_ESTADO", $FileName, $this);
        $this->Header_TELEFO = new clsPanel("Header_TELEFO", $this);
        $this->Sorter_TELEFO = new clsSorter($this->ComponentName, "Sorter_TELEFO", $FileName, $this);
        $this->Header_TELFAX = new clsPanel("Header_TELFAX", $this);
        $this->Sorter_TELFAX = new clsSorter($this->ComponentName, "Sorter_TELFAX", $FileName, $this);
        $this->Header_CONTAT = new clsPanel("Header_CONTAT", $this);
        $this->Sorter_CONTAT = new clsSorter($this->ComponentName, "Sorter_CONTAT", $FileName, $this);
        $this->Header_QTDMED = new clsPanel("Header_QTDMED", $this);
        $this->Sorter_QTDMED = new clsSorter($this->ComponentName, "Sorter_QTDMED", $FileName, $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple, $this);
        $this->Header_SUBSER->AddComponent("Sorter_SUBSER", $this->Sorter_SUBSER);
        $this->Header_DESSUB->AddComponent("Sorter_DESSUB", $this->Sorter_DESSUB);
        $this->Header_DESUNI->AddComponent("Sorter_DESUNI", $this->Sorter_DESUNI);
        $this->Header_DESCRI->AddComponent("Sorter_DESCRI", $this->Sorter_DESCRI);
        $this->Header_SUBRED->AddComponent("Sorter_SUBRED", $this->Sorter_SUBRED);
        $this->Header_UNIDAD->AddComponent("Sorter_UNIDAD", $this->Sorter_UNIDAD);
        $this->Header_CADCLI_CODCLI->AddComponent("Sorter_CADCLI_CODCLI", $this->Sorter_CADCLI_CODCLI);
        $this->Header_DESCLI->AddComponent("Sorter_DESCLI", $this->Sorter_DESCLI);
        $this->Header_CODSET->AddComponent("Sorter_CODSET", $this->Sorter_CODSET);
        $this->Header_CGCCPF->AddComponent("Sorter_CGCCPF", $this->Sorter_CGCCPF);
        $this->Header_LOGRAD->AddComponent("Sorter_LOGRAD", $this->Sorter_LOGRAD);
        $this->Header_BAIRRO->AddComponent("Sorter_BAIRRO", $this->Sorter_BAIRRO);
        $this->Header_CIDADE->AddComponent("Sorter_CIDADE", $this->Sorter_CIDADE);
        $this->Header_ESTADO->AddComponent("Sorter_ESTADO", $this->Sorter_ESTADO);
        $this->Header_TELEFO->AddComponent("Sorter_TELEFO", $this->Sorter_TELEFO);
        $this->Header_TELFAX->AddComponent("Sorter_TELFAX", $this->Sorter_TELFAX);
        $this->Header_CONTAT->AddComponent("Sorter_CONTAT", $this->Sorter_CONTAT);
        $this->Header_QTDMED->AddComponent("Sorter_QTDMED", $this->Sorter_QTDMED);
        $this->Data_SUBSER->AddComponent("SUBSER", $this->SUBSER);
        $this->Data_DESSUB->AddComponent("DESSUB", $this->DESSUB);
        $this->Data_DESUNI->AddComponent("DESUNI", $this->DESUNI);
        $this->Data_DESCRI->AddComponent("DESCRI", $this->DESCRI);
        $this->Data_SUBRED->AddComponent("SUBRED", $this->SUBRED);
        $this->Data_UNIDAD->AddComponent("UNIDAD", $this->UNIDAD);
        $this->Data_CADCLI_CODCLI->AddComponent("CADCLI_CODCLI", $this->CADCLI_CODCLI);
        $this->Data_DESCLI->AddComponent("DESCLI", $this->DESCLI);
        $this->Data_CODSET->AddComponent("CODSET", $this->CODSET);
        $this->Data_CGCCPF->AddComponent("CGCCPF", $this->CGCCPF);
        $this->Data_LOGRAD->AddComponent("LOGRAD", $this->LOGRAD);
        $this->Data_BAIRRO->AddComponent("BAIRRO", $this->BAIRRO);
        $this->Data_CIDADE->AddComponent("CIDADE", $this->CIDADE);
        $this->Data_ESTADO->AddComponent("ESTADO", $this->ESTADO);
        $this->Data_TELEFO->AddComponent("TELEFO", $this->TELEFO);
        $this->Data_TELFAX->AddComponent("TELFAX", $this->TELFAX);
        $this->Data_CONTAT->AddComponent("CONTAT", $this->CONTAT);
        $this->Data_QTDMED->AddComponent("QTDMED", $this->QTDMED);
    }
//End Class_Initialize Event

//Initialize Method @4-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @4-69836A54
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urls_SUBSER"] = CCGetFromGet("s_SUBSER", NULL);
        $this->DataSource->Parameters["urls_DESUNI"] = CCGetFromGet("s_DESUNI", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["Data_SUBSER"] = $this->Data_SUBSER->Visible;
            $this->ControlsVisible["SUBSER"] = $this->SUBSER->Visible;
            $this->ControlsVisible["Data_DESSUB"] = $this->Data_DESSUB->Visible;
            $this->ControlsVisible["DESSUB"] = $this->DESSUB->Visible;
            $this->ControlsVisible["Data_DESUNI"] = $this->Data_DESUNI->Visible;
            $this->ControlsVisible["DESUNI"] = $this->DESUNI->Visible;
            $this->ControlsVisible["Data_DESCRI"] = $this->Data_DESCRI->Visible;
            $this->ControlsVisible["DESCRI"] = $this->DESCRI->Visible;
            $this->ControlsVisible["Data_SUBRED"] = $this->Data_SUBRED->Visible;
            $this->ControlsVisible["SUBRED"] = $this->SUBRED->Visible;
            $this->ControlsVisible["Data_UNIDAD"] = $this->Data_UNIDAD->Visible;
            $this->ControlsVisible["UNIDAD"] = $this->UNIDAD->Visible;
            $this->ControlsVisible["Data_CADCLI_CODCLI"] = $this->Data_CADCLI_CODCLI->Visible;
            $this->ControlsVisible["CADCLI_CODCLI"] = $this->CADCLI_CODCLI->Visible;
            $this->ControlsVisible["Data_DESCLI"] = $this->Data_DESCLI->Visible;
            $this->ControlsVisible["DESCLI"] = $this->DESCLI->Visible;
            $this->ControlsVisible["Data_CODSET"] = $this->Data_CODSET->Visible;
            $this->ControlsVisible["CODSET"] = $this->CODSET->Visible;
            $this->ControlsVisible["Data_CGCCPF"] = $this->Data_CGCCPF->Visible;
            $this->ControlsVisible["CGCCPF"] = $this->CGCCPF->Visible;
            $this->ControlsVisible["Data_LOGRAD"] = $this->Data_LOGRAD->Visible;
            $this->ControlsVisible["LOGRAD"] = $this->LOGRAD->Visible;
            $this->ControlsVisible["Data_BAIRRO"] = $this->Data_BAIRRO->Visible;
            $this->ControlsVisible["BAIRRO"] = $this->BAIRRO->Visible;
            $this->ControlsVisible["Data_CIDADE"] = $this->Data_CIDADE->Visible;
            $this->ControlsVisible["CIDADE"] = $this->CIDADE->Visible;
            $this->ControlsVisible["Data_ESTADO"] = $this->Data_ESTADO->Visible;
            $this->ControlsVisible["ESTADO"] = $this->ESTADO->Visible;
            $this->ControlsVisible["Data_TELEFO"] = $this->Data_TELEFO->Visible;
            $this->ControlsVisible["TELEFO"] = $this->TELEFO->Visible;
            $this->ControlsVisible["Data_TELFAX"] = $this->Data_TELFAX->Visible;
            $this->ControlsVisible["TELFAX"] = $this->TELFAX->Visible;
            $this->ControlsVisible["Data_CONTAT"] = $this->Data_CONTAT->Visible;
            $this->ControlsVisible["CONTAT"] = $this->CONTAT->Visible;
            $this->ControlsVisible["Data_QTDMED"] = $this->Data_QTDMED->Visible;
            $this->ControlsVisible["QTDMED"] = $this->QTDMED->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                // Parse Separator
                if($this->RowNumber) {
                    $this->Attributes->Show();
                    $Tpl->parseto("Separator", true, "Row");
                }
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->SUBSER->SetValue($this->DataSource->SUBSER->GetValue());
                $this->DESSUB->SetValue($this->DataSource->DESSUB->GetValue());
                $this->DESUNI->SetValue($this->DataSource->DESUNI->GetValue());
                $this->DESCRI->SetValue($this->DataSource->DESCRI->GetValue());
                $this->SUBRED->SetValue($this->DataSource->SUBRED->GetValue());
                $this->UNIDAD->SetValue($this->DataSource->UNIDAD->GetValue());
                $this->CADCLI_CODCLI->SetValue($this->DataSource->CADCLI_CODCLI->GetValue());
                $this->DESCLI->SetValue($this->DataSource->DESCLI->GetValue());
                $this->CODSET->SetValue($this->DataSource->CODSET->GetValue());
                $this->CGCCPF->SetValue($this->DataSource->CGCCPF->GetValue());
                $this->LOGRAD->SetValue($this->DataSource->LOGRAD->GetValue());
                $this->BAIRRO->SetValue($this->DataSource->BAIRRO->GetValue());
                $this->CIDADE->SetValue($this->DataSource->CIDADE->GetValue());
                $this->ESTADO->SetValue($this->DataSource->ESTADO->GetValue());
                $this->TELEFO->SetValue($this->DataSource->TELEFO->GetValue());
                $this->TELFAX->SetValue($this->DataSource->TELFAX->GetValue());
                $this->CONTAT->SetValue($this->DataSource->CONTAT->GetValue());
                $this->QTDMED->SetValue($this->DataSource->QTDMED->GetValue());
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->Data_SUBSER->Show();
                $this->Data_DESSUB->Show();
                $this->Data_DESUNI->Show();
                $this->Data_DESCRI->Show();
                $this->Data_SUBRED->Show();
                $this->Data_UNIDAD->Show();
                $this->Data_CADCLI_CODCLI->Show();
                $this->Data_DESCLI->Show();
                $this->Data_CODSET->Show();
                $this->Data_CGCCPF->Show();
                $this->Data_LOGRAD->Show();
                $this->Data_BAIRRO->Show();
                $this->Data_CIDADE->Show();
                $this->Data_ESTADO->Show();
                $this->Data_TELEFO->Show();
                $this->Data_TELFAX->Show();
                $this->Data_CONTAT->Show();
                $this->Data_QTDMED->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        $this->CADCLI_MOVSER_SUBSER_TABU1_TotalRecords->Show();
        $this->Header_SUBSER->Show();
        $this->Sorter_SUBSER->Show();
        $this->Header_DESSUB->Show();
        $this->Sorter_DESSUB->Show();
        $this->Header_DESUNI->Show();
        $this->Sorter_DESUNI->Show();
        $this->Header_DESCRI->Show();
        $this->Sorter_DESCRI->Show();
        $this->Header_SUBRED->Show();
        $this->Sorter_SUBRED->Show();
        $this->Header_UNIDAD->Show();
        $this->Sorter_UNIDAD->Show();
        $this->Header_CADCLI_CODCLI->Show();
        $this->Sorter_CADCLI_CODCLI->Show();
        $this->Header_DESCLI->Show();
        $this->Sorter_DESCLI->Show();
        $this->Header_CODSET->Show();
        $this->Sorter_CODSET->Show();
        $this->Header_CGCCPF->Show();
        $this->Sorter_CGCCPF->Show();
        $this->Header_LOGRAD->Show();
        $this->Sorter_LOGRAD->Show();
        $this->Header_BAIRRO->Show();
        $this->Sorter_BAIRRO->Show();
        $this->Header_CIDADE->Show();
        $this->Sorter_CIDADE->Show();
        $this->Header_ESTADO->Show();
        $this->Sorter_ESTADO->Show();
        $this->Header_TELEFO->Show();
        $this->Sorter_TELEFO->Show();
        $this->Header_TELFAX->Show();
        $this->Sorter_TELFAX->Show();
        $this->Header_CONTAT->Show();
        $this->Sorter_CONTAT->Show();
        $this->Header_QTDMED->Show();
        $this->Sorter_QTDMED->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @4-423B5D84
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->SUBSER->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESSUB->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESUNI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESCRI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->SUBRED->Errors->ToString());
        $errors = ComposeStrings($errors, $this->UNIDAD->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CADCLI_CODCLI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESCLI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CODSET->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CGCCPF->Errors->ToString());
        $errors = ComposeStrings($errors, $this->LOGRAD->Errors->ToString());
        $errors = ComposeStrings($errors, $this->BAIRRO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CIDADE->Errors->ToString());
        $errors = ComposeStrings($errors, $this->ESTADO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TELEFO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TELFAX->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CONTAT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->QTDMED->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End CADCLI_MOVSER_SUBSER_TABU1 Class @4-FCB6E20C

class clsCADCLI_MOVSER_SUBSER_TABU1DataSource extends clsDBFaturar {  //CADCLI_MOVSER_SUBSER_TABU1DataSource Class @4-D5356A15

//DataSource Variables @4-51AFABE5
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $SUBSER;
    public $DESSUB;
    public $DESUNI;
    public $DESCRI;
    public $SUBRED;
    public $UNIDAD;
    public $CADCLI_CODCLI;
    public $DESCLI;
    public $CODSET;
    public $CGCCPF;
    public $LOGRAD;
    public $BAIRRO;
    public $CIDADE;
    public $ESTADO;
    public $TELEFO;
    public $TELFAX;
    public $CONTAT;
    public $QTDMED;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-0501F4E8
    function clsCADCLI_MOVSER_SUBSER_TABU1DataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid CADCLI_MOVSER_SUBSER_TABU1";
        $this->Initialize();
        $this->SUBSER = new clsField("SUBSER", ccsText, "");
        $this->DESSUB = new clsField("DESSUB", ccsText, "");
        $this->DESUNI = new clsField("DESUNI", ccsText, "");
        $this->DESCRI = new clsField("DESCRI", ccsText, "");
        $this->SUBRED = new clsField("SUBRED", ccsText, "");
        $this->UNIDAD = new clsField("UNIDAD", ccsText, "");
        $this->CADCLI_CODCLI = new clsField("CADCLI_CODCLI", ccsText, "");
        $this->DESCLI = new clsField("DESCLI", ccsText, "");
        $this->CODSET = new clsField("CODSET", ccsText, "");
        $this->CGCCPF = new clsField("CGCCPF", ccsText, "");
        $this->LOGRAD = new clsField("LOGRAD", ccsText, "");
        $this->BAIRRO = new clsField("BAIRRO", ccsText, "");
        $this->CIDADE = new clsField("CIDADE", ccsText, "");
        $this->ESTADO = new clsField("ESTADO", ccsText, "");
        $this->TELEFO = new clsField("TELEFO", ccsText, "");
        $this->TELFAX = new clsField("TELFAX", ccsText, "");
        $this->CONTAT = new clsField("CONTAT", ccsText, "");
        $this->QTDMED = new clsField("QTDMED", ccsFloat, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @4-924574D3
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_SUBSER" => array("SUBSER.SUBSER", ""), 
            "Sorter_DESSUB" => array("DESSUB", ""), 
            "Sorter_DESUNI" => array("DESUNI", ""), 
            "Sorter_DESCRI" => array("DESCRI", ""), 
            "Sorter_SUBRED" => array("SUBRED", ""), 
            "Sorter_UNIDAD" => array("UNIDAD", ""), 
            "Sorter_CADCLI_CODCLI" => array("CADCLI.CODCLI", ""), 
            "Sorter_DESCLI" => array("DESCLI", ""), 
            "Sorter_CODSET" => array("CODSET", ""), 
            "Sorter_CGCCPF" => array("CGCCPF", ""), 
            "Sorter_LOGRAD" => array("LOGRAD", ""), 
            "Sorter_BAIRRO" => array("BAIRRO", ""), 
            "Sorter_CIDADE" => array("CIDADE", ""), 
            "Sorter_ESTADO" => array("ESTADO", ""), 
            "Sorter_TELEFO" => array("TELEFO", ""), 
            "Sorter_TELFAX" => array("TELFAX", ""), 
            "Sorter_CONTAT" => array("CONTAT", ""), 
            "Sorter_QTDMED" => array("QTDMED", "")));
    }
//End SetOrder Method

//Prepare Method @4-99C45F39
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_SUBSER", ccsText, "", "", $this->Parameters["urls_SUBSER"], "", false);
        $this->wp->AddParameter("2", "urls_DESUNI", ccsText, "", "", $this->Parameters["urls_DESUNI"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "SUBSER.SUBSER", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opContains, "TABUNI.DESUNI", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsText),false);
        $this->Where = $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]);
        $this->Where = $this->wp->opAND(false, "( (CADCLI.CODCLI = MOVSER.CODCLI) AND (CADCLI.CODUNI = TABUNI.CODUNI) AND (MOVSER.SUBSER = SUBSER.SUBSER) )", $this->Where);
    }
//End Prepare Method

//Open Method @4-5ED6DDBA
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM SUBSER,\n\n" .
        "CADCLI,\n\n" .
        "MOVSER,\n\n" .
        "TABUNI";
        $this->SQL = "SELECT SUBSER.*, CADCLI.CODCLI AS CADCLI_CODCLI, DESCLI, CODSET, CGCCPF, LOGRAD, BAIRRO, CIDADE, ESTADO, TELEFO, TELFAX, CONTAT, DESUNI,\n\n" .
        "DESCRI, QTDMED \n\n" .
        "FROM SUBSER,\n\n" .
        "CADCLI,\n\n" .
        "MOVSER,\n\n" .
        "TABUNI {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @4-B1D8C8A1
    function SetValues()
    {
        $this->SUBSER->SetDBValue($this->f("SUBSER"));
        $this->DESSUB->SetDBValue($this->f("DESSUB"));
        $this->DESUNI->SetDBValue($this->f("DESUNI"));
        $this->DESCRI->SetDBValue($this->f("DESCRI"));
        $this->SUBRED->SetDBValue($this->f("SUBRED"));
        $this->UNIDAD->SetDBValue($this->f("UNIDAD"));
        $this->CADCLI_CODCLI->SetDBValue($this->f("CADCLI_CODCLI"));
        $this->DESCLI->SetDBValue($this->f("DESCLI"));
        $this->CODSET->SetDBValue($this->f("CODSET"));
        $this->CGCCPF->SetDBValue($this->f("CGCCPF"));
        $this->LOGRAD->SetDBValue($this->f("LOGRAD"));
        $this->BAIRRO->SetDBValue($this->f("BAIRRO"));
        $this->CIDADE->SetDBValue($this->f("CIDADE"));
        $this->ESTADO->SetDBValue($this->f("ESTADO"));
        $this->TELEFO->SetDBValue($this->f("TELEFO"));
        $this->TELFAX->SetDBValue($this->f("TELFAX"));
        $this->CONTAT->SetDBValue($this->f("CONTAT"));
        $this->QTDMED->SetDBValue(trim($this->f("QTDMED")));
    }
//End SetValues Method

} //End CADCLI_MOVSER_SUBSER_TABU1DataSource Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-CA8619EB
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "ManutSubServicoRelCli.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-946ECC7A
CCSecurityRedirect("1;2;3", "");
//End Authenticate User

//Include events file @1-EC722EF0
include("./ManutSubServicoRelCli_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-8266CD91
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$CADCLI_MOVSER_SUBSER_TABU = new clsRecordCADCLI_MOVSER_SUBSER_TABU("", $MainPage);
$CADCLI_MOVSER_SUBSER_TABU1 = new clsGridCADCLI_MOVSER_SUBSER_TABU1("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->CADCLI_MOVSER_SUBSER_TABU = & $CADCLI_MOVSER_SUBSER_TABU;
$MainPage->CADCLI_MOVSER_SUBSER_TABU1 = & $CADCLI_MOVSER_SUBSER_TABU1;
$MainPage->rodape = & $rodape;
$CADCLI_MOVSER_SUBSER_TABU1->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-4ADC1B8E
$cabec->Operations();
$CADCLI_MOVSER_SUBSER_TABU->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-44063792
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($CADCLI_MOVSER_SUBSER_TABU);
    unset($CADCLI_MOVSER_SUBSER_TABU1);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-AC2A5A68
$cabec->Show();
$CADCLI_MOVSER_SUBSER_TABU->Show();
$CADCLI_MOVSER_SUBSER_TABU1->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-1A5E76F8
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($CADCLI_MOVSER_SUBSER_TABU);
unset($CADCLI_MOVSER_SUBSER_TABU1);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
