<?php
//BindEvents Method @1-3F8AA67F
function BindEvents()
{
    global $CADCLI_MOVSER_SUBSER_TABU1;
    global $CCSEvents;
    $CADCLI_MOVSER_SUBSER_TABU1->CADCLI_MOVSER_SUBSER_TABU1_TotalRecords->CCSEvents["BeforeShow"] = "CADCLI_MOVSER_SUBSER_TABU1_CADCLI_MOVSER_SUBSER_TABU1_TotalRecords_BeforeShow";
    $CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->CCSEvents["BeforeShow"] = "CADCLI_MOVSER_SUBSER_TABU1_CGCCPF_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//CADCLI_MOVSER_SUBSER_TABU1_CADCLI_MOVSER_SUBSER_TABU1_TotalRecords_BeforeShow @47-6CA0E986
function CADCLI_MOVSER_SUBSER_TABU1_CADCLI_MOVSER_SUBSER_TABU1_TotalRecords_BeforeShow(& $sender)
{
    $CADCLI_MOVSER_SUBSER_TABU1_CADCLI_MOVSER_SUBSER_TABU1_TotalRecords_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI_MOVSER_SUBSER_TABU1; //Compatibility
//End CADCLI_MOVSER_SUBSER_TABU1_CADCLI_MOVSER_SUBSER_TABU1_TotalRecords_BeforeShow

//Retrieve number of records @48-ABE656B4
    $Component->SetValue($Container->DataSource->RecordsCount);
//End Retrieve number of records

//Close CADCLI_MOVSER_SUBSER_TABU1_CADCLI_MOVSER_SUBSER_TABU1_TotalRecords_BeforeShow @47-26F19573
    return $CADCLI_MOVSER_SUBSER_TABU1_CADCLI_MOVSER_SUBSER_TABU1_TotalRecords_BeforeShow;
}
//End Close CADCLI_MOVSER_SUBSER_TABU1_CADCLI_MOVSER_SUBSER_TABU1_TotalRecords_BeforeShow

//CADCLI_MOVSER_SUBSER_TABU1_CGCCPF_BeforeShow @110-807751AD
function CADCLI_MOVSER_SUBSER_TABU1_CGCCPF_BeforeShow(& $sender)
{
    $CADCLI_MOVSER_SUBSER_TABU1_CGCCPF_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADCLI_MOVSER_SUBSER_TABU1; //Compatibility
//End CADCLI_MOVSER_SUBSER_TABU1_CGCCPF_BeforeShow

//Custom Code @128-2A29BDB7
// -------------------------
    // Write your own code here.
	// Se o tamanho do CGC / CPF for de 14, trata-se de um CGC do contr�rio, trata-se de um CPF
	$mCGC_Parte1 = substr($CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->Value,00,3);
	$mCGC_Parte2 = substr($CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->Value,03,3);
	$mCGC_Parte3 = substr($CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->Value,06,3);
	if (strlen(trim($CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->Value)) == 14)
	{
	   //123.456.789.012-34 Formato de um CGC
	   $mCGC_Parte4 = substr($CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->Value,09,3);
	   $mCGC_Parte5 = substr($CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->Value,-02);  // Pega os dois �ltimos
       $CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->SetValue($mCGC_Parte1.'.'.$mCGC_Parte2.'.'.$mCGC_Parte3.'.'.$mCGC_Parte4.' - '.$mCGC_Parte5);
	}
	else
	{
	   //123.456.789-34 Formato de um CGC
	   $mCGC_Parte4 = substr($CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->Value,-02);  // Pega os dois �ltimos
       $CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->SetValue($mCGC_Parte1.'.'.$mCGC_Parte2.'.'.$mCGC_Parte3.' - '.$mCGC_Parte4);
	}
// -------------------------
//End Custom Code

//Close CADCLI_MOVSER_SUBSER_TABU1_CGCCPF_BeforeShow @110-DF2A0A7A
    return $CADCLI_MOVSER_SUBSER_TABU1_CGCCPF_BeforeShow;
}
//End Close CADCLI_MOVSER_SUBSER_TABU1_CGCCPF_BeforeShow

//Page_BeforeShow @1-1E24DD23
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutSubServicoRelCli; //Compatibility
//End Page_BeforeShow

//Custom Code @132-2A29BDB7
// -------------------------

        include("controle_acesso.php");
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(13,12);
		controleacesso($perfil,$permissao_requerida,"acessonegado.php");

		
// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow
//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	// Se o tamanho do CGC / CPF for de 14, trata-se de um CGC do contr�rio, trata-se de um CPF
//DEL  	$mCGC_Parte1 = substr($CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->Value,00,3);
//DEL  	$mCGC_Parte2 = substr($CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->Value,03,3);
//DEL  	$mCGC_Parte3 = substr($CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->Value,06,3);
//DEL  	if (strlen(trim($CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->Value)) == 14)
//DEL  	{
//DEL  	   //123.456.789.012-34 Formato de um CGC
//DEL  	   $mCGC_Parte4 = substr($CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->Value,09,3);
//DEL  	   $mCGC_Parte5 = substr($CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->Value,-02);  // Pega os dois �ltimos
//DEL         $CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->SetValue($mCGC_Parte1.'.'.$mCGC_Parte2.'.'.$mCGC_Parte3.'.'.$mCGC_Parte4.' - '.$mCGC_Parte5);
//DEL  	}
//DEL  	else
//DEL  	{
//DEL  	   //123.456.789-34 Formato de um CGC
//DEL  	   $mCGC_Parte4 = substr($CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->Value,-02);  // Pega os dois �ltimos
//DEL         $CADCLI_MOVSER_SUBSER_TABU1->CGCCPF->SetValue($mCGC_Parte1.'.'.$mCGC_Parte2.'.'.$mCGC_Parte3.' - '.$mCGC_Parte4);
//DEL  	}
//DEL  // -------------------------


//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	$CADCLI_MOVSER_SUBSER_TABU1HTML->SetValue("<th>"."<!-- BEGIN Sorter Sorter_DESCLI -->".
//DEL  				            "<a href='{Sort_URL}'>".
//DEL  				            espacos(20).
//DEL  				            "Nome do Cliente".
//DEL  				            espacos(20).
//DEL  				            "</a> ".
//DEL                              "<!-- BEGIN Asc_On --><img src='Styles/Apricot/Images/Asc.gif' border='0'><!-- END Asc_On -->".
//DEL                              "<!-- BEGIN Desc_On --><img src='Styles/Apricot/Images/Desc.gif' border='0'><!-- END Desc_On --><!-- END Sorter Sorter_DESCLI --></th>".
//DEL                              "<!-- END Panel Header_DESCLI -->".
//DEL                              "<!-- BEGIN Panel Header_CODSET -->".
//DEL                              "<th>"
//DEL  						 );
//DEL  
//DEL  
//DEL  // -------------------------

?>
