<?php
//BindEvents Method @1-25E151EE
function BindEvents()
{
    global $SUBSER;
    global $CCSEvents;
    $SUBSER->SUBSER->CCSEvents["BeforeShow"] = "SUBSER_SUBSER_BeforeShow";
    $SUBSER->GRPSER->ds->CCSEvents["BeforeBuildSelect"] = "SUBSER_GRPSER_ds_BeforeBuildSelect";
    $SUBSER->CCSEvents["BeforeShow"] = "SUBSER_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//SUBSER_SUBSER_BeforeShow @11-8DEFA6D6
function SUBSER_SUBSER_BeforeShow(& $sender)
{
    $SUBSER_SUBSER_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $SUBSER; //Compatibility
//End SUBSER_SUBSER_BeforeShow

//Custom Code @18-2A29BDB7
// -------------------------
    // Write your own code here.
	if ($SUBSER->Button_Insert->Visible) 
	{
       global $DBfaturar;
       $Page = CCGetParentPage($sender);
       $ccs_result = CCDLookUp("max(to_number(subser))+1", "subser", "", $Page->Connections["Faturar"]);
       $ccs_result = intval($ccs_result);
	   if (is_null($ccs_result) || $ccs_result == 0)
	   {
	   		$ccs_result = 1;//Para o caso da tabela estar vazia;
	   }
       $Component->SetValue($ccs_result);
	}

// -------------------------
//End Custom Code

//Close SUBSER_SUBSER_BeforeShow @11-F2DA1DC0
    return $SUBSER_SUBSER_BeforeShow;
}
//End Close SUBSER_SUBSER_BeforeShow

//SUBSER_GRPSER_ds_BeforeBuildSelect @14-A8F1BCF6
function SUBSER_GRPSER_ds_BeforeBuildSelect(& $sender)
{
    $SUBSER_GRPSER_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $SUBSER; //Compatibility
//End SUBSER_GRPSER_ds_BeforeBuildSelect

//Custom Code @25-2A29BDB7
// -------------------------
    if ($SUBSER->GRPSER->DataSource->Order=="")
	{
		$SUBSER->GRPSER->DataSource->Order="DESSER";
	}
// -------------------------
//End Custom Code

//Close SUBSER_GRPSER_ds_BeforeBuildSelect @14-C87DEC62
    return $SUBSER_GRPSER_ds_BeforeBuildSelect;
}
//End Close SUBSER_GRPSER_ds_BeforeBuildSelect

//SUBSER_BeforeShow @4-52AF267C
function SUBSER_BeforeShow(& $sender)
{
    $SUBSER_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $SUBSER; //Compatibility
//End SUBSER_BeforeShow

//Custom Code @19-2A29BDB7
// -------------------------
    // Write your own code here.
	if ($SUBSER->Button_Delete->Visible)
	{
       global $DBfaturar;
       $Page = CCGetParentPage($sender);
       $ccs_result = CCDLookUp("SUBSER", "equser", "SUBSER='".$SUBSER->SUBSER->Value."'", $Page->Connections["Faturar"]);
       //$Component->SetValue($ccs_result);
	   if ($ccs_result != "")
	   {
	      $SUBSER->Button_Delete->Visible = false;
	   }
    }

// -------------------------
//End Custom Code

//Close SUBSER_BeforeShow @4-1E3CFA40
    return $SUBSER_BeforeShow;
}
//End Close SUBSER_BeforeShow

//Page_BeforeShow @1-54EF81FF
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutSubServico; //Compatibility
//End Page_BeforeShow

//Custom Code @26-2A29BDB7
// -------------------------
        
		include("controle_acesso.php");
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(12);
		controleacesso($perfil,$permissao_requerida,"acessonegado.php");
// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
