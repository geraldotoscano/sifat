<?php
//Include Common Files @1-6BA125AC
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "ManutTabCol.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @5-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordTABCOL { //TABCOL Class @6-6E3B9EC1

//Variables @6-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @6-10C28356
    function clsRecordTABCOL($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record TABCOL/Error";
        $this->DataSource = new clsTABCOLDataSource($this);
        $this->ds = & $this->DataSource;
        $this->InsertAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "TABCOL";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->CODUNI = new clsControl(ccsListBox, "CODUNI", "Unidade", ccsText, "", CCGetRequestParam("CODUNI", $Method, NULL), $this);
            $this->CODUNI->DSType = dsTable;
            $this->CODUNI->DataSource = new clsDBFaturar();
            $this->CODUNI->ds = & $this->CODUNI->DataSource;
            $this->CODUNI->DataSource->SQL = "SELECT * \n" .
"FROM TABUNI {SQL_Where} {SQL_OrderBy}";
            list($this->CODUNI->BoundColumn, $this->CODUNI->TextColumn, $this->CODUNI->DBFormat) = array("CODUNI", "DESUNI", "");
            $this->CODUNI->Required = true;
            $this->CODDIS = new clsControl(ccsListBox, "CODDIS", "Distrito", ccsText, "", CCGetRequestParam("CODDIS", $Method, NULL), $this);
            $this->CODDIS->DSType = dsTable;
            $this->CODDIS->DataSource = new clsDBFaturar();
            $this->CODDIS->ds = & $this->CODDIS->DataSource;
            $this->CODDIS->DataSource->SQL = "SELECT * \n" .
"FROM TABDIS {SQL_Where} {SQL_OrderBy}";
            list($this->CODDIS->BoundColumn, $this->CODDIS->TextColumn, $this->CODDIS->DBFormat) = array("CODDIS", "DESDIS", "");
            $this->CODDIS->Required = true;
            $this->CODSET = new clsControl(ccsListBox, "CODSET", "Setor", ccsText, "", CCGetRequestParam("CODSET", $Method, NULL), $this);
            $this->CODSET->Required = true;
            $this->FREQUE = new clsControl(ccsListBox, "FREQUE", "Frequência", ccsText, "", CCGetRequestParam("FREQUE", $Method, NULL), $this);
            $this->FREQUE->HTML = true;
            $this->FREQUE->Required = true;
            $this->Button_Insert = new clsButton("Button_Insert", $Method, $this);
            $this->Button_Cancel = new clsButton("Button_Cancel", $Method, $this);
        }
    }
//End Class_Initialize Event

//Initialize Method @6-C39381F7
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->DataSource->Parameters["urlCODUNI"] = CCGetFromGet("CODUNI", NULL);
    }
//End Initialize Method

//Validate Method @6-1DB2AC86
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->CODUNI->Validate() && $Validation);
        $Validation = ($this->CODDIS->Validate() && $Validation);
        $Validation = ($this->CODSET->Validate() && $Validation);
        $Validation = ($this->FREQUE->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->CODUNI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODDIS->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODSET->Errors->Count() == 0);
        $Validation =  $Validation && ($this->FREQUE->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @6-B3D1A483
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->CODUNI->Errors->Count());
        $errors = ($errors || $this->CODDIS->Errors->Count());
        $errors = ($errors || $this->CODSET->Errors->Count());
        $errors = ($errors || $this->FREQUE->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @6-46546F6C
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->DataSource->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->DataSource->AllParametersSet;
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_Insert";
            if($this->Button_Insert->Pressed) {
                $this->PressedButton = "Button_Insert";
            } else if($this->Button_Cancel->Pressed) {
                $this->PressedButton = "Button_Cancel";
            }
        }
        $Redirect = "CadTabCol.php" . "?" . CCGetQueryString("QueryString", array("ccsForm", "CODUNI", "CODDIS", "CODSET"));
        if($this->PressedButton == "Button_Cancel") {
            $Redirect = "CadTabCol.php" . "?" . CCGetQueryString("QueryString", array("ccsForm", "CODUNI", "CODDIS", "CODSET"));
            if(!CCGetEvent($this->Button_Cancel->CCSEvents, "OnClick", $this->Button_Cancel)) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button_Insert") {
                $Redirect = "CadTabCol.php" . "?" . CCGetQueryString("QueryString", array("ccsForm", "CODUNI", "CODDIS", "CODSET"));
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick", $this->Button_Insert) || !$this->InsertRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
        if ($Redirect)
            $this->DataSource->close();
    }
//End Operation Method

//InsertRow Method @6-4F0F046D
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert", $this);
        if(!$this->InsertAllowed) return false;
        $this->DataSource->CODUNI->SetValue($this->CODUNI->GetValue(true));
        $this->DataSource->CODDIS->SetValue($this->CODDIS->GetValue(true));
        $this->DataSource->CODSET->SetValue($this->CODSET->GetValue(true));
        $this->DataSource->FREQUE->SetValue($this->FREQUE->GetValue(true));
        $this->DataSource->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert", $this);
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//Show Method @6-AC5CBA4F
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->CODUNI->Prepare();
        $this->CODDIS->Prepare();
        $this->CODSET->Prepare();
        $this->FREQUE->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->DataSource->Errors->Count()){
                $this->Errors->AddErrors($this->DataSource->Errors);
                $this->DataSource->Errors->clear();
            }
            $this->DataSource->Open();
            if($this->DataSource->Errors->Count() == 0 && $this->DataSource->next_record()) {
                $this->DataSource->SetValues();
                if(!$this->FormSubmitted){
                    $this->CODUNI->SetValue($this->DataSource->CODUNI->GetValue());
                    $this->CODDIS->SetValue($this->DataSource->CODDIS->GetValue());
                    $this->CODSET->SetValue($this->DataSource->CODSET->GetValue());
                    $this->FREQUE->SetValue($this->DataSource->FREQUE->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->CODUNI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODDIS->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODSET->Errors->ToString());
            $Error = ComposeStrings($Error, $this->FREQUE->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataSource->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Insert->Visible = !$this->EditMode && $this->InsertAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->CODUNI->Show();
        $this->CODDIS->Show();
        $this->CODSET->Show();
        $this->FREQUE->Show();
        $this->Button_Insert->Show();
        $this->Button_Cancel->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

} //End TABCOL Class @6-FCB6E20C

class clsTABCOLDataSource extends clsDBFaturar {  //TABCOLDataSource Class @6-5F667396

//DataSource Variables @6-9B5F1746
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $InsertParameters;
    public $wp;
    public $AllParametersSet;

    public $InsertFields = array();

    // Datasource fields
    public $CODUNI;
    public $CODDIS;
    public $CODSET;
    public $FREQUE;
//End DataSource Variables

//DataSourceClass_Initialize Event @6-4134C4FD
    function clsTABCOLDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Record TABCOL/Error";
        $this->Initialize();
        $this->CODUNI = new clsField("CODUNI", ccsText, "");
        $this->CODDIS = new clsField("CODDIS", ccsText, "");
        $this->CODSET = new clsField("CODSET", ccsText, "");
        $this->FREQUE = new clsField("FREQUE", ccsText, "");

        $this->InsertFields["CODUNI"] = array("Name" => "CODUNI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODDIS"] = array("Name" => "CODDIS", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODSET"] = array("Name" => "CODSET", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["FREQUE"] = array("Name" => "FREQUE", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
    }
//End DataSourceClass_Initialize Event

//Prepare Method @6-1EC018D9
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlCODUNI", ccsText, "", "", $this->Parameters["urlCODUNI"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "CODUNI", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @6-C9625D34
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->SQL = "SELECT * \n\n" .
        "FROM TABCOL {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @6-6B86E1B0
    function SetValues()
    {
        $this->CODUNI->SetDBValue($this->f("CODUNI"));
        $this->CODDIS->SetDBValue($this->f("CODDIS"));
        $this->CODSET->SetDBValue($this->f("CODSET"));
        $this->FREQUE->SetDBValue($this->f("FREQUE"));
    }
//End SetValues Method

//Insert Method @6-91A21C47
    function Insert()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert", $this->Parent);
        $this->InsertFields["CODUNI"]["Value"] = $this->CODUNI->GetDBValue(true);
        $this->InsertFields["CODDIS"]["Value"] = $this->CODDIS->GetDBValue(true);
        $this->InsertFields["CODSET"]["Value"] = $this->CODSET->GetDBValue(true);
        $this->InsertFields["FREQUE"]["Value"] = $this->FREQUE->GetDBValue(true);
        $this->SQL = CCBuildInsert("TABCOL", $this->InsertFields, $this);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert", $this->Parent);
        }
    }
//End Insert Method

} //End TABCOLDataSource Class @6-FCB6E20C

//Include Page implementation @4-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-E39D915B
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "ManutTabCol.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-7FACF37D
CCSecurityRedirect("1;3", "");
//End Authenticate User

//Include events file @1-F5C4F21B
include("./ManutTabCol_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-28558C95
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$TABCOL = new clsRecordTABCOL("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->TABCOL = & $TABCOL;
$MainPage->rodape = & $rodape;
$TABCOL->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-9B540AE2
$cabec->Operations();
$TABCOL->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-B7CECA49
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($TABCOL);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-FAE38B3A
$cabec->Show();
$TABCOL->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-C2D10B12
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($TABCOL);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
