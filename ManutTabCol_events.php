<?php
//BindEvents Method @1-032065D5
function BindEvents()
{
    global $TABCOL;
    global $CCSEvents;
    $TABCOL->CODUNI->ds->CCSEvents["BeforeBuildSelect"] = "TABCOL_CODUNI_ds_BeforeBuildSelect";
    $TABCOL->CODDIS->ds->CCSEvents["BeforeBuildSelect"] = "TABCOL_CODDIS_ds_BeforeBuildSelect";
    $TABCOL->Button_Insert->CCSEvents["OnClick"] = "TABCOL_Button_Insert_OnClick";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//TABCOL_CODUNI_ds_BeforeBuildSelect @10-B8E79B72
function TABCOL_CODUNI_ds_BeforeBuildSelect(& $sender)
{
    $TABCOL_CODUNI_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABCOL; //Compatibility
//End TABCOL_CODUNI_ds_BeforeBuildSelect

//Custom Code @19-2A29BDB7
// -------------------------
    if ($TABCOL->CODUNI->DataSource->Order == "")
	{
		$TABCOL->CODUNI->DataSource->Order = "DESUNI";
	}
// -------------------------
//End Custom Code

//Close TABCOL_CODUNI_ds_BeforeBuildSelect @10-442FDB01
    return $TABCOL_CODUNI_ds_BeforeBuildSelect;
}
//End Close TABCOL_CODUNI_ds_BeforeBuildSelect

//TABCOL_CODDIS_ds_BeforeBuildSelect @11-12D6C3CB
function TABCOL_CODDIS_ds_BeforeBuildSelect(& $sender)
{
    $TABCOL_CODDIS_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABCOL; //Compatibility
//End TABCOL_CODDIS_ds_BeforeBuildSelect

//Custom Code @20-2A29BDB7
// -------------------------
    if ($TABCOL->CODDIS->DataSource->Order == "")
	{
		$TABCOL->CODDIS->DataSource->Order = "DESDIS";
	}
// -------------------------
//End Custom Code

//Close TABCOL_CODDIS_ds_BeforeBuildSelect @11-B36D55CA
    return $TABCOL_CODDIS_ds_BeforeBuildSelect;
}
//End Close TABCOL_CODDIS_ds_BeforeBuildSelect

//TABCOL_Button_Insert_OnClick @7-636E49DB
function TABCOL_Button_Insert_OnClick(& $sender)
{
    $TABCOL_Button_Insert_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABCOL; //Compatibility
	global $Redirect;
//End TABCOL_Button_Insert_OnClick

//DLookup @15-0AD82E98
    global $DBfaturar;
    $Page = CCGetParentPage($sender);
    $ccs_result = CCDLookUp("coduni", "tabcol", "CODUNI='".$TABCOL->CODUNI->Value."' and coddis = '".$TABCOL->CODDIS->Value."' and codset = '".$TABCOL->CODSET->Value."'", $Page->Connections["Faturar"]);
	if ($ccs_result != "" )
	{
        $Redirect = "ManutTabColErro.php?CODUNI=".$TABCOL->CODUNI->Value."&CODDIS=".$TABCOL->CODDIS->Value."&CODSET=".$TABCOL->CODSET->Value;
	}
//End DLookup

//Close TABCOL_Button_Insert_OnClick @7-5DB29D36
    return $TABCOL_Button_Insert_OnClick;
}
//End Close TABCOL_Button_Insert_OnClick

//Page_BeforeShow @1-F14AECFA
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutTabCol; //Compatibility
//End Page_BeforeShow

//Custom Code @21-2A29BDB7
// -------------------------

        include("controle_acesso.php");
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(18);
        controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
