<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="25" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="CADCLI_TABDIS_TABUNI" wizardCaption="Search CADCLI TABDIS TABUNI " wizardOrientation="Vertical" wizardFormMethod="post" returnPage="ManutTabDisRelCli.ccp" debugMode="False">
			<Components>
				<ListBox id="27" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="s_CODDIS" wizardCaption="CODDIS" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="TABDIS" boundColumn="CODDIS" textColumn="DESDIS" editable="True" hasErrorCollection="True">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<Button id="26" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="4" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="Faturar" dataSource="TABDIS, CADCLI, TABUNI" name="CADCLI_TABDIS_TABUNI1" pageSizeLimit="100" wizardCaption="List of CADCLI, TABDIS, TABUNI " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="Nenhum distrito foi encontrado">
			<Components>
				<Sorter id="29" visible="True" name="Sorter_CODDIS" column="TABDIS.CODDIS" wizardCaption="CODDIS" wizardSortingType="SimpleDir" wizardControl="CODDIS" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="30" visible="True" name="Sorter_DESDIS" column="DESDIS" wizardCaption="DESDIS" wizardSortingType="SimpleDir" wizardControl="DESDIS" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="31" visible="True" name="Sorter_DESCLI" column="DESCLI" wizardCaption="DESCLI" wizardSortingType="SimpleDir" wizardControl="DESCLI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="32" visible="True" name="Sorter_CODSET" column="CODSET" wizardCaption="CODSET" wizardSortingType="SimpleDir" wizardControl="CODSET" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="33" visible="True" name="Sorter_LOGRAD" column="LOGRAD" wizardCaption="LOGRAD" wizardSortingType="SimpleDir" wizardControl="LOGRAD" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="34" visible="True" name="Sorter_BAIRRO" column="BAIRRO" wizardCaption="BAIRRO" wizardSortingType="SimpleDir" wizardControl="BAIRRO" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="35" visible="True" name="Sorter_CIDADE" column="CIDADE" wizardCaption="CIDADE" wizardSortingType="SimpleDir" wizardControl="CIDADE" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="36" visible="True" name="Sorter_ESTADO" column="ESTADO" wizardCaption="ESTADO" wizardSortingType="SimpleDir" wizardControl="ESTADO" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="37" visible="True" name="Sorter_TELEFO" column="TELEFO" wizardCaption="TELEFO" wizardSortingType="SimpleDir" wizardControl="TELEFO" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="38" visible="True" name="Sorter_TELFAX" column="TELFAX" wizardCaption="TELFAX" wizardSortingType="SimpleDir" wizardControl="TELFAX" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="39" visible="True" name="Sorter_CONTAT" column="CONTAT" wizardCaption="CONTAT" wizardSortingType="SimpleDir" wizardControl="CONTAT" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="40" visible="True" name="Sorter_DESUNI" column="DESUNI" wizardCaption="DESUNI" wizardSortingType="SimpleDir" wizardControl="DESUNI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="41" visible="True" name="Sorter_DESCRI" column="DESCRI" wizardCaption="DESCRI" wizardSortingType="SimpleDir" wizardControl="DESCRI" wizardAddNbsp="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Label id="43" fieldSourceType="DBColumn" dataType="Text" html="False" name="CODDIS" fieldSource="CODDIS" wizardCaption="CODDIS" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="45" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESDIS" fieldSource="DESDIS" wizardCaption="DESDIS" wizardSize="15" wizardMaxLength="15" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="46" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESCLI" fieldSource="DESCLI" wizardCaption="DESCLI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="47" fieldSourceType="DBColumn" dataType="Text" html="False" name="CODSET" fieldSource="CODSET" wizardCaption="CODSET" wizardSize="1" wizardMaxLength="1" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="48" fieldSourceType="DBColumn" dataType="Text" html="False" name="LOGRAD" fieldSource="LOGRAD" wizardCaption="LOGRAD" wizardSize="35" wizardMaxLength="35" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="49" fieldSourceType="DBColumn" dataType="Text" html="False" name="BAIRRO" fieldSource="BAIRRO" wizardCaption="BAIRRO" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="50" fieldSourceType="DBColumn" dataType="Text" html="False" name="CIDADE" fieldSource="CIDADE" wizardCaption="CIDADE" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="51" fieldSourceType="DBColumn" dataType="Text" html="False" name="ESTADO" fieldSource="ESTADO" wizardCaption="ESTADO" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="52" fieldSourceType="DBColumn" dataType="Text" html="False" name="TELEFO" fieldSource="TELEFO" wizardCaption="TELEFO" wizardSize="13" wizardMaxLength="13" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="53" fieldSourceType="DBColumn" dataType="Text" html="False" name="TELFAX" fieldSource="TELFAX" wizardCaption="TELFAX" wizardSize="13" wizardMaxLength="13" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="54" fieldSourceType="DBColumn" dataType="Text" html="False" name="CONTAT" fieldSource="CONTAT" wizardCaption="CONTAT" wizardSize="35" wizardMaxLength="35" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="55" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESUNI" fieldSource="DESUNI" wizardCaption="DESUNI" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="56" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESCRI" fieldSource="DESCRI" wizardCaption="DESCRI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="57" size="10" name="Navigator" wizardPagingType="Custom" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="True" wizardOfText="of" wizardImages="Images" wizardUsePageScroller="True" type="Simple">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="28" conditionType="Parameter" useIsNull="False" field="TABDIS.CODDIS" parameterSource="s_CODDIS" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="1"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="5" tableName="TABDIS" posLeft="10" posTop="10" posWidth="95" posHeight="88"/>
				<JoinTable id="6" tableName="CADCLI" posLeft="126" posTop="10" posWidth="115" posHeight="180"/>
				<JoinTable id="7" tableName="TABUNI" posLeft="262" posTop="10" posWidth="95" posHeight="104"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="8" tableLeft="CADCLI" tableRight="TABUNI" fieldLeft="CADCLI.CODUNI" fieldRight="TABUNI.CODUNI" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="9" tableLeft="CADCLI" tableRight="TABDIS" fieldLeft="CADCLI.CODDIS" fieldRight="TABDIS.CODDIS" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="10" tableName="TABDIS" fieldName="TABDIS.*"/>
				<Field id="12" tableName="TABUNI" fieldName="DESUNI"/>
				<Field id="13" tableName="TABUNI" fieldName="DESCRI"/>
				<Field id="14" tableName="CADCLI" fieldName="DESCLI"/>
				<Field id="17" tableName="CADCLI" fieldName="CODSET"/>
				<Field id="18" tableName="CADCLI" fieldName="LOGRAD"/>
				<Field id="19" tableName="CADCLI" fieldName="BAIRRO"/>
				<Field id="20" tableName="CADCLI" fieldName="CIDADE"/>
				<Field id="21" tableName="CADCLI" fieldName="ESTADO"/>
				<Field id="22" tableName="CADCLI" fieldName="TELEFO"/>
				<Field id="23" tableName="CADCLI" fieldName="TELFAX"/>
				<Field id="24" tableName="CADCLI" fieldName="CONTAT"/>
				<Field id="42" tableName="TABDIS" fieldName="CODDIS" alias="CODDIS"/>
				<Field id="44" tableName="TABDIS" fieldName="DESDIS" alias="DESDIS"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="ManutTabDisRelCli.php" forShow="True" url="ManutTabDisRelCli.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="ManutTabDisRelCli_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="58" groupID="1"/>
		<Group id="59" groupID="2"/>
		<Group id="60" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="61"/>
			</Actions>
		</Event>
	</Events>
</Page>
