<?php
//Include Common Files @1-972DB091
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "ManutTabDisRelCli.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordCADCLI_TABDIS_TABUNI { //CADCLI_TABDIS_TABUNI Class @25-835C81DC

//Variables @25-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @25-3BA3EA43
    function clsRecordCADCLI_TABDIS_TABUNI($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record CADCLI_TABDIS_TABUNI/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "CADCLI_TABDIS_TABUNI";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_CODDIS = new clsControl(ccsListBox, "s_CODDIS", "s_CODDIS", ccsText, "", CCGetRequestParam("s_CODDIS", $Method, NULL), $this);
            $this->s_CODDIS->DSType = dsTable;
            $this->s_CODDIS->DataSource = new clsDBFaturar();
            $this->s_CODDIS->ds = & $this->s_CODDIS->DataSource;
            $this->s_CODDIS->DataSource->SQL = "SELECT * \n" .
"FROM TABDIS {SQL_Where} {SQL_OrderBy}";
            list($this->s_CODDIS->BoundColumn, $this->s_CODDIS->TextColumn, $this->s_CODDIS->DBFormat) = array("CODDIS", "DESDIS", "");
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
        }
    }
//End Class_Initialize Event

//Validate Method @25-5F7E9173
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_CODDIS->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->s_CODDIS->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @25-987CD400
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_CODDIS->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @25-6033A07C
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "ManutTabDisRelCli.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                $Redirect = "ManutTabDisRelCli.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @25-A615D8BC
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->s_CODDIS->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->s_CODDIS->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_CODDIS->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End CADCLI_TABDIS_TABUNI Class @25-FCB6E20C

class clsGridCADCLI_TABDIS_TABUNI1 { //CADCLI_TABDIS_TABUNI1 class @4-BD8D573E

//Variables @4-7A87DAF9

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
    public $Sorter_CODDIS;
    public $Sorter_DESDIS;
    public $Sorter_DESCLI;
    public $Sorter_CODSET;
    public $Sorter_LOGRAD;
    public $Sorter_BAIRRO;
    public $Sorter_CIDADE;
    public $Sorter_ESTADO;
    public $Sorter_TELEFO;
    public $Sorter_TELFAX;
    public $Sorter_CONTAT;
    public $Sorter_DESUNI;
    public $Sorter_DESCRI;
//End Variables

//Class_Initialize Event @4-9304869C
    function clsGridCADCLI_TABDIS_TABUNI1($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "CADCLI_TABDIS_TABUNI1";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid CADCLI_TABDIS_TABUNI1";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clsCADCLI_TABDIS_TABUNI1DataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 10;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;
        $this->SorterName = CCGetParam("CADCLI_TABDIS_TABUNI1Order", "");
        $this->SorterDirection = CCGetParam("CADCLI_TABDIS_TABUNI1Dir", "");

        $this->CODDIS = new clsControl(ccsLabel, "CODDIS", "CODDIS", ccsText, "", CCGetRequestParam("CODDIS", ccsGet, NULL), $this);
        $this->DESDIS = new clsControl(ccsLabel, "DESDIS", "DESDIS", ccsText, "", CCGetRequestParam("DESDIS", ccsGet, NULL), $this);
        $this->DESCLI = new clsControl(ccsLabel, "DESCLI", "DESCLI", ccsText, "", CCGetRequestParam("DESCLI", ccsGet, NULL), $this);
        $this->CODSET = new clsControl(ccsLabel, "CODSET", "CODSET", ccsText, "", CCGetRequestParam("CODSET", ccsGet, NULL), $this);
        $this->LOGRAD = new clsControl(ccsLabel, "LOGRAD", "LOGRAD", ccsText, "", CCGetRequestParam("LOGRAD", ccsGet, NULL), $this);
        $this->BAIRRO = new clsControl(ccsLabel, "BAIRRO", "BAIRRO", ccsText, "", CCGetRequestParam("BAIRRO", ccsGet, NULL), $this);
        $this->CIDADE = new clsControl(ccsLabel, "CIDADE", "CIDADE", ccsText, "", CCGetRequestParam("CIDADE", ccsGet, NULL), $this);
        $this->ESTADO = new clsControl(ccsLabel, "ESTADO", "ESTADO", ccsText, "", CCGetRequestParam("ESTADO", ccsGet, NULL), $this);
        $this->TELEFO = new clsControl(ccsLabel, "TELEFO", "TELEFO", ccsText, "", CCGetRequestParam("TELEFO", ccsGet, NULL), $this);
        $this->TELFAX = new clsControl(ccsLabel, "TELFAX", "TELFAX", ccsText, "", CCGetRequestParam("TELFAX", ccsGet, NULL), $this);
        $this->CONTAT = new clsControl(ccsLabel, "CONTAT", "CONTAT", ccsText, "", CCGetRequestParam("CONTAT", ccsGet, NULL), $this);
        $this->DESUNI = new clsControl(ccsLabel, "DESUNI", "DESUNI", ccsText, "", CCGetRequestParam("DESUNI", ccsGet, NULL), $this);
        $this->DESCRI = new clsControl(ccsLabel, "DESCRI", "DESCRI", ccsText, "", CCGetRequestParam("DESCRI", ccsGet, NULL), $this);
        $this->Sorter_CODDIS = new clsSorter($this->ComponentName, "Sorter_CODDIS", $FileName, $this);
        $this->Sorter_DESDIS = new clsSorter($this->ComponentName, "Sorter_DESDIS", $FileName, $this);
        $this->Sorter_DESCLI = new clsSorter($this->ComponentName, "Sorter_DESCLI", $FileName, $this);
        $this->Sorter_CODSET = new clsSorter($this->ComponentName, "Sorter_CODSET", $FileName, $this);
        $this->Sorter_LOGRAD = new clsSorter($this->ComponentName, "Sorter_LOGRAD", $FileName, $this);
        $this->Sorter_BAIRRO = new clsSorter($this->ComponentName, "Sorter_BAIRRO", $FileName, $this);
        $this->Sorter_CIDADE = new clsSorter($this->ComponentName, "Sorter_CIDADE", $FileName, $this);
        $this->Sorter_ESTADO = new clsSorter($this->ComponentName, "Sorter_ESTADO", $FileName, $this);
        $this->Sorter_TELEFO = new clsSorter($this->ComponentName, "Sorter_TELEFO", $FileName, $this);
        $this->Sorter_TELFAX = new clsSorter($this->ComponentName, "Sorter_TELFAX", $FileName, $this);
        $this->Sorter_CONTAT = new clsSorter($this->ComponentName, "Sorter_CONTAT", $FileName, $this);
        $this->Sorter_DESUNI = new clsSorter($this->ComponentName, "Sorter_DESUNI", $FileName, $this);
        $this->Sorter_DESCRI = new clsSorter($this->ComponentName, "Sorter_DESCRI", $FileName, $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple, $this);
    }
//End Class_Initialize Event

//Initialize Method @4-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @4-64289199
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urls_CODDIS"] = CCGetFromGet("s_CODDIS", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["CODDIS"] = $this->CODDIS->Visible;
            $this->ControlsVisible["DESDIS"] = $this->DESDIS->Visible;
            $this->ControlsVisible["DESCLI"] = $this->DESCLI->Visible;
            $this->ControlsVisible["CODSET"] = $this->CODSET->Visible;
            $this->ControlsVisible["LOGRAD"] = $this->LOGRAD->Visible;
            $this->ControlsVisible["BAIRRO"] = $this->BAIRRO->Visible;
            $this->ControlsVisible["CIDADE"] = $this->CIDADE->Visible;
            $this->ControlsVisible["ESTADO"] = $this->ESTADO->Visible;
            $this->ControlsVisible["TELEFO"] = $this->TELEFO->Visible;
            $this->ControlsVisible["TELFAX"] = $this->TELFAX->Visible;
            $this->ControlsVisible["CONTAT"] = $this->CONTAT->Visible;
            $this->ControlsVisible["DESUNI"] = $this->DESUNI->Visible;
            $this->ControlsVisible["DESCRI"] = $this->DESCRI->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->CODDIS->SetValue($this->DataSource->CODDIS->GetValue());
                $this->DESDIS->SetValue($this->DataSource->DESDIS->GetValue());
                $this->DESCLI->SetValue($this->DataSource->DESCLI->GetValue());
                $this->CODSET->SetValue($this->DataSource->CODSET->GetValue());
                $this->LOGRAD->SetValue($this->DataSource->LOGRAD->GetValue());
                $this->BAIRRO->SetValue($this->DataSource->BAIRRO->GetValue());
                $this->CIDADE->SetValue($this->DataSource->CIDADE->GetValue());
                $this->ESTADO->SetValue($this->DataSource->ESTADO->GetValue());
                $this->TELEFO->SetValue($this->DataSource->TELEFO->GetValue());
                $this->TELFAX->SetValue($this->DataSource->TELFAX->GetValue());
                $this->CONTAT->SetValue($this->DataSource->CONTAT->GetValue());
                $this->DESUNI->SetValue($this->DataSource->DESUNI->GetValue());
                $this->DESCRI->SetValue($this->DataSource->DESCRI->GetValue());
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->CODDIS->Show();
                $this->DESDIS->Show();
                $this->DESCLI->Show();
                $this->CODSET->Show();
                $this->LOGRAD->Show();
                $this->BAIRRO->Show();
                $this->CIDADE->Show();
                $this->ESTADO->Show();
                $this->TELEFO->Show();
                $this->TELFAX->Show();
                $this->CONTAT->Show();
                $this->DESUNI->Show();
                $this->DESCRI->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        $this->Sorter_CODDIS->Show();
        $this->Sorter_DESDIS->Show();
        $this->Sorter_DESCLI->Show();
        $this->Sorter_CODSET->Show();
        $this->Sorter_LOGRAD->Show();
        $this->Sorter_BAIRRO->Show();
        $this->Sorter_CIDADE->Show();
        $this->Sorter_ESTADO->Show();
        $this->Sorter_TELEFO->Show();
        $this->Sorter_TELFAX->Show();
        $this->Sorter_CONTAT->Show();
        $this->Sorter_DESUNI->Show();
        $this->Sorter_DESCRI->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @4-6A1B0EDC
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->CODDIS->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESDIS->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESCLI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CODSET->Errors->ToString());
        $errors = ComposeStrings($errors, $this->LOGRAD->Errors->ToString());
        $errors = ComposeStrings($errors, $this->BAIRRO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CIDADE->Errors->ToString());
        $errors = ComposeStrings($errors, $this->ESTADO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TELEFO->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TELFAX->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CONTAT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESUNI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DESCRI->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End CADCLI_TABDIS_TABUNI1 Class @4-FCB6E20C

class clsCADCLI_TABDIS_TABUNI1DataSource extends clsDBFaturar {  //CADCLI_TABDIS_TABUNI1DataSource Class @4-C3D6A2A9

//DataSource Variables @4-1512A780
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $CODDIS;
    public $DESDIS;
    public $DESCLI;
    public $CODSET;
    public $LOGRAD;
    public $BAIRRO;
    public $CIDADE;
    public $ESTADO;
    public $TELEFO;
    public $TELFAX;
    public $CONTAT;
    public $DESUNI;
    public $DESCRI;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-6A278D37
    function clsCADCLI_TABDIS_TABUNI1DataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid CADCLI_TABDIS_TABUNI1";
        $this->Initialize();
        $this->CODDIS = new clsField("CODDIS", ccsText, "");
        $this->DESDIS = new clsField("DESDIS", ccsText, "");
        $this->DESCLI = new clsField("DESCLI", ccsText, "");
        $this->CODSET = new clsField("CODSET", ccsText, "");
        $this->LOGRAD = new clsField("LOGRAD", ccsText, "");
        $this->BAIRRO = new clsField("BAIRRO", ccsText, "");
        $this->CIDADE = new clsField("CIDADE", ccsText, "");
        $this->ESTADO = new clsField("ESTADO", ccsText, "");
        $this->TELEFO = new clsField("TELEFO", ccsText, "");
        $this->TELFAX = new clsField("TELFAX", ccsText, "");
        $this->CONTAT = new clsField("CONTAT", ccsText, "");
        $this->DESUNI = new clsField("DESUNI", ccsText, "");
        $this->DESCRI = new clsField("DESCRI", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @4-7E7EBB78
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_CODDIS" => array("TABDIS.CODDIS", ""), 
            "Sorter_DESDIS" => array("DESDIS", ""), 
            "Sorter_DESCLI" => array("DESCLI", ""), 
            "Sorter_CODSET" => array("CODSET", ""), 
            "Sorter_LOGRAD" => array("LOGRAD", ""), 
            "Sorter_BAIRRO" => array("BAIRRO", ""), 
            "Sorter_CIDADE" => array("CIDADE", ""), 
            "Sorter_ESTADO" => array("ESTADO", ""), 
            "Sorter_TELEFO" => array("TELEFO", ""), 
            "Sorter_TELFAX" => array("TELFAX", ""), 
            "Sorter_CONTAT" => array("CONTAT", ""), 
            "Sorter_DESUNI" => array("DESUNI", ""), 
            "Sorter_DESCRI" => array("DESCRI", "")));
    }
//End SetOrder Method

//Prepare Method @4-76777E32
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_CODDIS", ccsText, "", "", $this->Parameters["urls_CODDIS"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "TABDIS.CODDIS", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->Where = 
             $this->wp->Criterion[1];
        $this->Where = $this->wp->opAND(false, "( (CADCLI.CODUNI = TABUNI.CODUNI) AND (CADCLI.CODDIS = TABDIS.CODDIS) )", $this->Where);
    }
//End Prepare Method

//Open Method @4-3A775661
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM TABDIS,\n\n" .
        "CADCLI,\n\n" .
        "TABUNI";
        $this->SQL = "SELECT TABDIS.*, DESUNI, DESCRI, DESCLI, CODSET, LOGRAD, BAIRRO, CIDADE, ESTADO, TELEFO, TELFAX, CONTAT \n\n" .
        "FROM TABDIS,\n\n" .
        "CADCLI,\n\n" .
        "TABUNI {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @4-1C7A0FBD
    function SetValues()
    {
        $this->CODDIS->SetDBValue($this->f("CODDIS"));
        $this->DESDIS->SetDBValue($this->f("DESDIS"));
        $this->DESCLI->SetDBValue($this->f("DESCLI"));
        $this->CODSET->SetDBValue($this->f("CODSET"));
        $this->LOGRAD->SetDBValue($this->f("LOGRAD"));
        $this->BAIRRO->SetDBValue($this->f("BAIRRO"));
        $this->CIDADE->SetDBValue($this->f("CIDADE"));
        $this->ESTADO->SetDBValue($this->f("ESTADO"));
        $this->TELEFO->SetDBValue($this->f("TELEFO"));
        $this->TELFAX->SetDBValue($this->f("TELFAX"));
        $this->CONTAT->SetDBValue($this->f("CONTAT"));
        $this->DESUNI->SetDBValue($this->f("DESUNI"));
        $this->DESCRI->SetDBValue($this->f("DESCRI"));
    }
//End SetValues Method

} //End CADCLI_TABDIS_TABUNI1DataSource Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-8612486B
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "ManutTabDisRelCli.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-946ECC7A
CCSecurityRedirect("1;2;3", "");
//End Authenticate User

//Include events file @1-C81A9AE2
include("./ManutTabDisRelCli_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-C4DC51F9
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$CADCLI_TABDIS_TABUNI = new clsRecordCADCLI_TABDIS_TABUNI("", $MainPage);
$CADCLI_TABDIS_TABUNI1 = new clsGridCADCLI_TABDIS_TABUNI1("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->CADCLI_TABDIS_TABUNI = & $CADCLI_TABDIS_TABUNI;
$MainPage->CADCLI_TABDIS_TABUNI1 = & $CADCLI_TABDIS_TABUNI1;
$MainPage->rodape = & $rodape;
$CADCLI_TABDIS_TABUNI1->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-9FAA5281
$cabec->Operations();
$CADCLI_TABDIS_TABUNI->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-89CBA9A0
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($CADCLI_TABDIS_TABUNI);
    unset($CADCLI_TABDIS_TABUNI1);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-3AD4FCC5
$cabec->Show();
$CADCLI_TABDIS_TABUNI->Show();
$CADCLI_TABDIS_TABUNI1->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-7B6E6033
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($CADCLI_TABDIS_TABUNI);
unset($CADCLI_TABDIS_TABUNI1);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
