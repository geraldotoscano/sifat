<?php
//Include Common Files @1-A2F06F98
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "ManutTabItauIncl.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

class clsRecordTABITAU { //TABITAU Class @4-37AFDCC1

//Variables @4-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @4-6F2EB1AD
    function clsRecordTABITAU($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record TABITAU/Error";
        $this->DataSource = new clsTABITAUDataSource($this);
        $this->ds = & $this->DataSource;
        $this->InsertAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "TABITAU";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->EMPRESA = new clsControl(ccsTextBox, "EMPRESA", "Empresa", ccsText, "", CCGetRequestParam("EMPRESA", $Method, NULL), $this);
            $this->EMPRESA->Required = true;
            $this->AGENCIA = new clsControl(ccsTextBox, "AGENCIA", "Ag�ncia", ccsText, "", CCGetRequestParam("AGENCIA", $Method, NULL), $this);
            $this->AGENCIA->Required = true;
            $this->DV_AGENCIA = new clsControl(ccsListBox, "DV_AGENCIA", "D�gito Verificador", ccsText, "", CCGetRequestParam("DV_AGENCIA", $Method, NULL), $this);
            $this->DV_AGENCIA->DSType = dsListOfValues;
            $this->DV_AGENCIA->Values = array(array("0", "0"), array("1", "1"), array("2", "2"), array("3", "3"), array("4", "4"), array("5", "5"), array("6", "6"), array("7", "7"), array("8", "8"), array("9", "9"));
            $this->DV_AGENCIA->Required = true;
            $this->CONTACO = new clsControl(ccsTextBox, "CONTACO", "Conta Corrente", ccsText, "", CCGetRequestParam("CONTACO", $Method, NULL), $this);
            $this->CONTACO->Required = true;
            $this->DAC = new clsControl(ccsListBox, "DAC", "DAC", ccsText, "", CCGetRequestParam("DAC", $Method, NULL), $this);
            $this->DAC->DSType = dsListOfValues;
            $this->DAC->Values = array(array("0", "0"), array("1", "1"), array("2", "2"), array("3", "3"), array("4", "4"), array("5", "5"), array("6", "6"), array("7", "7"), array("8", "8"), array("9", "9"));
            $this->DAC->Required = true;
            $this->NUMECGC = new clsControl(ccsTextBox, "NUMECGC", "CGC", ccsText, "", CCGetRequestParam("NUMECGC", $Method, NULL), $this);
            $this->NUMECGC->Required = true;
            $this->SIGLA = new clsControl(ccsTextBox, "SIGLA", "Sigla", ccsText, "", CCGetRequestParam("SIGLA", $Method, NULL), $this);
            $this->SIGLA->Required = true;
            $this->ENDERECO = new clsControl(ccsTextBox, "ENDERECO", "Endere�o", ccsText, "", CCGetRequestParam("ENDERECO", $Method, NULL), $this);
            $this->ENDERECO->Required = true;
            $this->CEP = new clsControl(ccsTextBox, "CEP", "CEP", ccsText, "", CCGetRequestParam("CEP", $Method, NULL), $this);
            $this->CEP->Required = true;
            $this->PRACA = new clsControl(ccsTextBox, "PRACA", "Pra�a", ccsText, "", CCGetRequestParam("PRACA", $Method, NULL), $this);
            $this->PRACA->Required = true;
            $this->SEQ_ARQ = new clsControl(ccsHidden, "SEQ_ARQ", "SEQ_ARQ", ccsText, "", CCGetRequestParam("SEQ_ARQ", $Method, NULL), $this);
            $this->Button_Update = new clsButton("Button_Update", $Method, $this);
            $this->Button_Cancel = new clsButton("Button_Cancel", $Method, $this);
            if(!$this->FormSubmitted) {
                if(!is_array($this->SEQ_ARQ->Value) && !strlen($this->SEQ_ARQ->Value) && $this->SEQ_ARQ->Value !== false)
                    $this->SEQ_ARQ->SetText(55);
            }
        }
    }
//End Class_Initialize Event

//Initialize Method @4-B99335D3
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->DataSource->Parameters["urlEMPRESA"] = CCGetFromGet("EMPRESA", NULL);
    }
//End Initialize Method

//Validate Method @4-2A9CE871
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->EMPRESA->Validate() && $Validation);
        $Validation = ($this->AGENCIA->Validate() && $Validation);
        $Validation = ($this->DV_AGENCIA->Validate() && $Validation);
        $Validation = ($this->CONTACO->Validate() && $Validation);
        $Validation = ($this->DAC->Validate() && $Validation);
        $Validation = ($this->NUMECGC->Validate() && $Validation);
        $Validation = ($this->SIGLA->Validate() && $Validation);
        $Validation = ($this->ENDERECO->Validate() && $Validation);
        $Validation = ($this->CEP->Validate() && $Validation);
        $Validation = ($this->PRACA->Validate() && $Validation);
        $Validation = ($this->SEQ_ARQ->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->EMPRESA->Errors->Count() == 0);
        $Validation =  $Validation && ($this->AGENCIA->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DV_AGENCIA->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CONTACO->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DAC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->NUMECGC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->SIGLA->Errors->Count() == 0);
        $Validation =  $Validation && ($this->ENDERECO->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CEP->Errors->Count() == 0);
        $Validation =  $Validation && ($this->PRACA->Errors->Count() == 0);
        $Validation =  $Validation && ($this->SEQ_ARQ->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @4-7928DEF7
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->EMPRESA->Errors->Count());
        $errors = ($errors || $this->AGENCIA->Errors->Count());
        $errors = ($errors || $this->DV_AGENCIA->Errors->Count());
        $errors = ($errors || $this->CONTACO->Errors->Count());
        $errors = ($errors || $this->DAC->Errors->Count());
        $errors = ($errors || $this->NUMECGC->Errors->Count());
        $errors = ($errors || $this->SIGLA->Errors->Count());
        $errors = ($errors || $this->ENDERECO->Errors->Count());
        $errors = ($errors || $this->CEP->Errors->Count());
        $errors = ($errors || $this->PRACA->Errors->Count());
        $errors = ($errors || $this->SEQ_ARQ->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @4-AECD9E7B
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->DataSource->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->DataSource->AllParametersSet;
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_Update";
            if($this->Button_Update->Pressed) {
                $this->PressedButton = "Button_Update";
            } else if($this->Button_Cancel->Pressed) {
                $this->PressedButton = "Button_Cancel";
            }
        }
        $Redirect = "CadTabItau.php" . "?" . CCGetQueryString("QueryString", array("ccsForm"));
        if($this->PressedButton == "Button_Cancel") {
            if(!CCGetEvent($this->Button_Cancel->CCSEvents, "OnClick", $this->Button_Cancel)) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button_Update") {
                if(!CCGetEvent($this->Button_Update->CCSEvents, "OnClick", $this->Button_Update) || !$this->InsertRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
        if ($Redirect)
            $this->DataSource->close();
    }
//End Operation Method

//InsertRow Method @4-8976C742
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert", $this);
        if(!$this->InsertAllowed) return false;
        $this->DataSource->EMPRESA->SetValue($this->EMPRESA->GetValue(true));
        $this->DataSource->AGENCIA->SetValue($this->AGENCIA->GetValue(true));
        $this->DataSource->DV_AGENCIA->SetValue($this->DV_AGENCIA->GetValue(true));
        $this->DataSource->CONTACO->SetValue($this->CONTACO->GetValue(true));
        $this->DataSource->DAC->SetValue($this->DAC->GetValue(true));
        $this->DataSource->NUMECGC->SetValue($this->NUMECGC->GetValue(true));
        $this->DataSource->SIGLA->SetValue($this->SIGLA->GetValue(true));
        $this->DataSource->ENDERECO->SetValue($this->ENDERECO->GetValue(true));
        $this->DataSource->CEP->SetValue($this->CEP->GetValue(true));
        $this->DataSource->PRACA->SetValue($this->PRACA->GetValue(true));
        $this->DataSource->SEQ_ARQ->SetValue($this->SEQ_ARQ->GetValue(true));
        $this->DataSource->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert", $this);
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//Show Method @4-D80DC2F7
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->DV_AGENCIA->Prepare();
        $this->DAC->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->DataSource->Errors->Count()){
                $this->Errors->AddErrors($this->DataSource->Errors);
                $this->DataSource->Errors->clear();
            }
            $this->DataSource->Open();
            if($this->DataSource->Errors->Count() == 0 && $this->DataSource->next_record()) {
                $this->DataSource->SetValues();
                if(!$this->FormSubmitted){
                    $this->EMPRESA->SetValue($this->DataSource->EMPRESA->GetValue());
                    $this->AGENCIA->SetValue($this->DataSource->AGENCIA->GetValue());
                    $this->DV_AGENCIA->SetValue($this->DataSource->DV_AGENCIA->GetValue());
                    $this->CONTACO->SetValue($this->DataSource->CONTACO->GetValue());
                    $this->DAC->SetValue($this->DataSource->DAC->GetValue());
                    $this->NUMECGC->SetValue($this->DataSource->NUMECGC->GetValue());
                    $this->SIGLA->SetValue($this->DataSource->SIGLA->GetValue());
                    $this->ENDERECO->SetValue($this->DataSource->ENDERECO->GetValue());
                    $this->CEP->SetValue($this->DataSource->CEP->GetValue());
                    $this->PRACA->SetValue($this->DataSource->PRACA->GetValue());
                    $this->SEQ_ARQ->SetValue($this->DataSource->SEQ_ARQ->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->EMPRESA->Errors->ToString());
            $Error = ComposeStrings($Error, $this->AGENCIA->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DV_AGENCIA->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CONTACO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DAC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->NUMECGC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->SIGLA->Errors->ToString());
            $Error = ComposeStrings($Error, $this->ENDERECO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CEP->Errors->ToString());
            $Error = ComposeStrings($Error, $this->PRACA->Errors->ToString());
            $Error = ComposeStrings($Error, $this->SEQ_ARQ->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataSource->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Update->Visible = !$this->EditMode && $this->InsertAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->EMPRESA->Show();
        $this->AGENCIA->Show();
        $this->DV_AGENCIA->Show();
        $this->CONTACO->Show();
        $this->DAC->Show();
        $this->NUMECGC->Show();
        $this->SIGLA->Show();
        $this->ENDERECO->Show();
        $this->CEP->Show();
        $this->PRACA->Show();
        $this->SEQ_ARQ->Show();
        $this->Button_Update->Show();
        $this->Button_Cancel->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

} //End TABITAU Class @4-FCB6E20C

class clsTABITAUDataSource extends clsDBFaturar {  //TABITAUDataSource Class @4-B8D0B262

//DataSource Variables @4-14661A9E
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $InsertParameters;
    public $wp;
    public $AllParametersSet;

    public $InsertFields = array();

    // Datasource fields
    public $EMPRESA;
    public $AGENCIA;
    public $DV_AGENCIA;
    public $CONTACO;
    public $DAC;
    public $NUMECGC;
    public $SIGLA;
    public $ENDERECO;
    public $CEP;
    public $PRACA;
    public $SEQ_ARQ;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-26FC9156
    function clsTABITAUDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Record TABITAU/Error";
        $this->Initialize();
        $this->EMPRESA = new clsField("EMPRESA", ccsText, "");
        $this->AGENCIA = new clsField("AGENCIA", ccsText, "");
        $this->DV_AGENCIA = new clsField("DV_AGENCIA", ccsText, "");
        $this->CONTACO = new clsField("CONTACO", ccsText, "");
        $this->DAC = new clsField("DAC", ccsText, "");
        $this->NUMECGC = new clsField("NUMECGC", ccsText, "");
        $this->SIGLA = new clsField("SIGLA", ccsText, "");
        $this->ENDERECO = new clsField("ENDERECO", ccsText, "");
        $this->CEP = new clsField("CEP", ccsText, "");
        $this->PRACA = new clsField("PRACA", ccsText, "");
        $this->SEQ_ARQ = new clsField("SEQ_ARQ", ccsText, "");

        $this->InsertFields["EMPRESA"] = array("Name" => "EMPRESA", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["AGENCIA"] = array("Name" => "AGENCIA", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DV_AGENCIA"] = array("Name" => "DV_AGENCIA", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CONTACO"] = array("Name" => "CONTACO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DAC"] = array("Name" => "DAC", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["NUMECGC"] = array("Name" => "NUMECGC", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["SIGLA"] = array("Name" => "SIGLA", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["ENDERECO"] = array("Name" => "ENDERECO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CEP"] = array("Name" => "CEP", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["PRACA"] = array("Name" => "PRACA", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["SEQ_ARQ"] = array("Name" => "SEQ_ARQ", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
    }
//End DataSourceClass_Initialize Event

//Prepare Method @4-AED1D73E
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlEMPRESA", ccsText, "", "", $this->Parameters["urlEMPRESA"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "EMPRESA", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @4-0385D33E
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->SQL = "SELECT * \n\n" .
        "FROM TABITAU {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @4-3DADF23E
    function SetValues()
    {
        $this->EMPRESA->SetDBValue($this->f("EMPRESA"));
        $this->AGENCIA->SetDBValue($this->f("AGENCIA"));
        $this->DV_AGENCIA->SetDBValue($this->f("DV_AGENCIA"));
        $this->CONTACO->SetDBValue($this->f("CONTACO"));
        $this->DAC->SetDBValue($this->f("DAC"));
        $this->NUMECGC->SetDBValue($this->f("NUMECGC"));
        $this->SIGLA->SetDBValue($this->f("SIGLA"));
        $this->ENDERECO->SetDBValue($this->f("ENDERECO"));
        $this->CEP->SetDBValue($this->f("CEP"));
        $this->PRACA->SetDBValue($this->f("PRACA"));
        $this->SEQ_ARQ->SetDBValue($this->f("SEQ_ARQ"));
    }
//End SetValues Method

//Insert Method @4-98C81137
    function Insert()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert", $this->Parent);
        $this->InsertFields["EMPRESA"]["Value"] = $this->EMPRESA->GetDBValue(true);
        $this->InsertFields["AGENCIA"]["Value"] = $this->AGENCIA->GetDBValue(true);
        $this->InsertFields["DV_AGENCIA"]["Value"] = $this->DV_AGENCIA->GetDBValue(true);
        $this->InsertFields["CONTACO"]["Value"] = $this->CONTACO->GetDBValue(true);
        $this->InsertFields["DAC"]["Value"] = $this->DAC->GetDBValue(true);
        $this->InsertFields["NUMECGC"]["Value"] = $this->NUMECGC->GetDBValue(true);
        $this->InsertFields["SIGLA"]["Value"] = $this->SIGLA->GetDBValue(true);
        $this->InsertFields["ENDERECO"]["Value"] = $this->ENDERECO->GetDBValue(true);
        $this->InsertFields["CEP"]["Value"] = $this->CEP->GetDBValue(true);
        $this->InsertFields["PRACA"]["Value"] = $this->PRACA->GetDBValue(true);
        $this->InsertFields["SEQ_ARQ"]["Value"] = $this->SEQ_ARQ->GetDBValue(true);
        $this->SQL = CCBuildInsert("TABITAU", $this->InsertFields, $this);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert", $this->Parent);
        }
    }
//End Insert Method

} //End TABITAUDataSource Class @4-FCB6E20C

//Include Page implementation @2-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-4B03C2DD
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "ManutTabItauIncl.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-7FACF37D
CCSecurityRedirect("1;3", "");
//End Authenticate User

//Include events file @1-30F56674
include("./ManutTabItauIncl_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-81331302
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$TABITAU = new clsRecordTABITAU("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->TABITAU = & $TABITAU;
$MainPage->rodape = & $rodape;
$TABITAU->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-0AED005F
$TABITAU->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-82BCACD1
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    unset($TABITAU);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-0215B474
$TABITAU->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-951E4845
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
unset($TABITAU);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
