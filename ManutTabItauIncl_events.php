<?php
//BindEvents Method @1-BE8AF0E8
function BindEvents()
{
    global $TABITAU;
    $TABITAU->SEQ_ARQ->CCSEvents["BeforeShow"] = "TABITAU_SEQ_ARQ_BeforeShow";
    $TABITAU->Button_Update->CCSEvents["OnClick"] = "TABITAU_Button_Update_OnClick";
}
//End BindEvents Method

//TABITAU_SEQ_ARQ_BeforeShow @30-5EEBFDD9
function TABITAU_SEQ_ARQ_BeforeShow(& $sender)
{
    $TABITAU_SEQ_ARQ_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABITAU; //Compatibility
//End TABITAU_SEQ_ARQ_BeforeShow

//Custom Code @31-2A29BDB7
// -------------------------
	/*
		A codifica��o abaixo se faz necess�rio devido ao fato do valor
		atribuido na propriedade Default Value n�o assumir o referido
		valor uma vez que vem do banco o valor null, ou seja, Default 
		Value considera o seu valor somente se o componente estiver 
		sem valor, o que n�o � o caso de null ou mesmo 0.
	*/
    $TABITAU->SEQ_ARQ->SetValue('55');
// -------------------------
//End Custom Code

//Close TABITAU_SEQ_ARQ_BeforeShow @30-390CC4AC
    return $TABITAU_SEQ_ARQ_BeforeShow;
}
//End Close TABITAU_SEQ_ARQ_BeforeShow

//TABITAU_Button_Update_OnClick @5-AEDA2B6D
function TABITAU_Button_Update_OnClick(& $sender)
{
    $TABITAU_Button_Update_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABITAU; //Compatibility
//End TABITAU_Button_Update_OnClick

//Custom Code @26-2A29BDB7
// -------------------------
    // Write your own code here.
	$mCEP = $TABITAU->CEP->GetValue();// 30.555-200
	$TABITAU->CEP->SetValue(substr($mCEP,0,2).substr($mCEP,3,3).substr($mCEP,7,3));
// -------------------------
//End Custom Code

//Close TABITAU_Button_Update_OnClick @5-20B05FC8
    return $TABITAU_Button_Update_OnClick;
}
//End Close TABITAU_Button_Update_OnClick


?>
