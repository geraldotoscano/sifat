<?php
//BindEvents Method @1-84FD3797
function BindEvents()
{
    global $TABITAU;
    global $CCSEvents;
    $TABITAU->AGENCIA->CCSEvents["OnValidate"] = "TABITAU_AGENCIA_OnValidate";
    $TABITAU->CONTACO->CCSEvents["OnValidate"] = "TABITAU_CONTACO_OnValidate";
    $TABITAU->SEQ_ARQ->CCSEvents["BeforeShow"] = "TABITAU_SEQ_ARQ_BeforeShow";
    $TABITAU->Button_Update->CCSEvents["OnClick"] = "TABITAU_Button_Update_OnClick";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//TABITAU_AGENCIA_OnValidate @9-2AF56EE2
function TABITAU_AGENCIA_OnValidate(& $sender)
{
    $TABITAU_AGENCIA_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABITAU; //Compatibility
//End TABITAU_AGENCIA_OnValidate

//Custom Code @32-2A29BDB7
// -------------------------
    if (strlen(trim($TABITAU->AGENCIA->GetValue())) != 4)
	{
	   $TABITAU->Errors->addError("Ag�ncia tem que ter obrigatoriamente 4 caracteres.");
	}
	else
	{
	   if (!is_numeric($TABITAU->AGENCIA->GetValue()))
	   {
	      $TABITAU->Errors->addError("A Ag�ncia � constituida apenas de n�meros.");
	   }
	   else
	   {
	      $A = strpos(strtoupper($TABITAU->AGENCIA->GetValue()),'A');
	      $B = strpos(strtoupper($TABITAU->AGENCIA->GetValue()),'B');
	      $C = strpos(strtoupper($TABITAU->AGENCIA->GetValue()),'C');
	      $D = strpos(strtoupper($TABITAU->AGENCIA->GetValue()),'D');
	      $E = strpos(strtoupper($TABITAU->AGENCIA->GetValue()),'E');
	      $F = strpos(strtoupper($TABITAU->AGENCIA->GetValue()),'F');
	      $Mais = strpos(strtoupper($TABITAU->AGENCIA->GetValue()),'+');
	      $Menos = strpos(strtoupper($TABITAU->AGENCIA->GetValue()),'-');
	      if (!is_bool($A) || !is_bool($B) || !is_bool($C) || !is_bool($D) || !is_bool($E) || !is_bool($F) || !is_bool($Mais) || !is_bool($Menos))
		  {
	         $TABITAU->Errors->addError("A Ag�ncia � constituida apenas de n�meros.");
		  }
	   }
    }
// -------------------------
//End Custom Code

//Close TABITAU_AGENCIA_OnValidate @9-963382E3
    return $TABITAU_AGENCIA_OnValidate;
}
//End Close TABITAU_AGENCIA_OnValidate

//TABITAU_CONTACO_OnValidate @11-59E3C2A6
function TABITAU_CONTACO_OnValidate(& $sender)
{
    $TABITAU_CONTACO_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABITAU; //Compatibility
//End TABITAU_CONTACO_OnValidate

//Custom Code @33-2A29BDB7
// -------------------------
    if (strlen(trim($TABITAU->CONTACO->GetValue())) != 5)
	{
	   $TABITAU->Errors->addError("A Conta Corrente tem que ter obrigatoriamente 5 digitos.");
	}
	else
	{
	   if (!is_numeric($TABITAU->CONTACO->GetValue()))
	   {
	      $TABITAU->Errors->addError("A Conta Corrente � constituida apenas de n�meros.");
	   }
	   else
	   {
	      $A = strpos(strtoupper($TABITAU->CONTACO->GetValue()),'A');
	      $B = strpos(strtoupper($TABITAU->CONTACO->GetValue()),'B');
	      $C = strpos(strtoupper($TABITAU->CONTACO->GetValue()),'C');
	      $D = strpos(strtoupper($TABITAU->CONTACO->GetValue()),'D');
	      $E = strpos(strtoupper($TABITAU->CONTACO->GetValue()),'E');
	      $F = strpos(strtoupper($TABITAU->CONTACO->GetValue()),'F');
	      $Mais = strpos(strtoupper($TABITAU->CONTACO->GetValue()),'+');
	      $Menos = strpos(strtoupper($TABITAU->CONTACO->GetValue()),'-');
	      if (!is_bool($A) || !is_bool($B) || !is_bool($C) || !is_bool($D) || !is_bool($E) || !is_bool($F) || !is_bool($Mais) || !is_bool($Menos))
		  {
	         $TABITAU->Errors->addError("A Conta Corrente � constituida apenas de n�meros.");
		  }
	   }
	}
// -------------------------
//End Custom Code

//Close TABITAU_CONTACO_OnValidate @11-A6C2D155
    return $TABITAU_CONTACO_OnValidate;
}
//End Close TABITAU_CONTACO_OnValidate

//TABITAU_SEQ_ARQ_BeforeShow @30-5EEBFDD9
function TABITAU_SEQ_ARQ_BeforeShow(& $sender)
{
    $TABITAU_SEQ_ARQ_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABITAU; //Compatibility
//End TABITAU_SEQ_ARQ_BeforeShow

//Custom Code @31-2A29BDB7
// -------------------------
	/*
		A codifica��o abaixo se faz necess�rio devido ao fato do valor
		atribuido na propriedade Default Value n�o assumir o referido
		valor uma vez que vem do banco o valor null, ou seja, Default 
		Value considera o seu valor somente se o componente estiver 
		sem valor, o que n�o � o caso de null ou mesmo 0.
	*/
    $TABITAU->SEQ_ARQ->SetValue('55');
// -------------------------
//End Custom Code

//Close TABITAU_SEQ_ARQ_BeforeShow @30-390CC4AC
    return $TABITAU_SEQ_ARQ_BeforeShow;
}
//End Close TABITAU_SEQ_ARQ_BeforeShow

//TABITAU_Button_Update_OnClick @5-AEDA2B6D
function TABITAU_Button_Update_OnClick(& $sender)
{
    $TABITAU_Button_Update_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABITAU; //Compatibility
//End TABITAU_Button_Update_OnClick

//Custom Code @26-2A29BDB7
// -------------------------
    // Write your own code here.
	$mCEP = $TABITAU->CEP->GetValue();// 30.555-200
	$TABITAU->CEP->SetValue(substr($mCEP,0,2).substr($mCEP,3,3).substr($mCEP,7,3));
// -------------------------
//End Custom Code

//Close TABITAU_Button_Update_OnClick @5-20B05FC8
    return $TABITAU_Button_Update_OnClick;
}
//End Close TABITAU_Button_Update_OnClick

//Page_BeforeShow @1-6CA3F02D
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutTabItau; //Compatibility
//End Page_BeforeShow

//Custom Code @34-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(22);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
