<?php
//Include Common Files @1-14C169C6
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "ManutTabOpe.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordTABOPE { //TABOPE Class @28-E2557E7D

//Variables @28-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @28-ED515DD9
    function clsRecordTABOPE($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record TABOPE/Error";
        $this->DataSource = new clsTABOPEDataSource($this);
        $this->ds = & $this->DataSource;
        $this->InsertAllowed = true;
        $this->UpdateAllowed = true;
        $this->DeleteAllowed = true;
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "TABOPE";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->ID = new clsControl(ccsHidden, "ID", "ID", ccsFloat, "", CCGetRequestParam("ID", $Method, NULL), $this);
            $this->GRP_ID = new clsControl(ccsHidden, "GRP_ID", "GRP ID", ccsFloat, "", CCGetRequestParam("GRP_ID", $Method, NULL), $this);
            $this->CODOPE = new clsControl(ccsTextBox, "CODOPE", "CODOPE", ccsText, "", CCGetRequestParam("CODOPE", $Method, NULL), $this);
            $this->CODOPE->Required = true;
            $this->DESOPE = new clsControl(ccsTextBox, "DESOPE", "DESOPE", ccsText, "", CCGetRequestParam("DESOPE", $Method, NULL), $this);
            $this->PASSWO = new clsControl(ccsTextBox, "PASSWO", "PASSWO", ccsText, "", CCGetRequestParam("PASSWO", $Method, NULL), $this);
            $this->PASSWO->Required = true;
            $this->CODUNI = new clsControl(ccsListBox, "CODUNI", "CODUNI", ccsText, "", CCGetRequestParam("CODUNI", $Method, NULL), $this);
            $this->CODUNI->DSType = dsTable;
            $this->CODUNI->DataSource = new clsDBFaturar();
            $this->CODUNI->ds = & $this->CODUNI->DataSource;
            $this->CODUNI->DataSource->SQL = "SELECT * \n" .
"FROM TABUNI {SQL_Where} {SQL_OrderBy}";
            list($this->CODUNI->BoundColumn, $this->CODUNI->TextColumn, $this->CODUNI->DBFormat) = array("CODUNI", "DESCRI", "");
            $this->CODUNI->Required = true;
            $this->IDPERFIL = new clsControl(ccsListBox, "IDPERFIL", "IDPERFIL", ccsFloat, "", CCGetRequestParam("IDPERFIL", $Method, NULL), $this);
            $this->IDPERFIL->DSType = dsTable;
            $this->IDPERFIL->DataSource = new clsDBFaturar();
            $this->IDPERFIL->ds = & $this->IDPERFIL->DataSource;
            $this->IDPERFIL->DataSource->SQL = "SELECT * \n" .
"FROM TABPERFIL {SQL_Where} {SQL_OrderBy}";
            list($this->IDPERFIL->BoundColumn, $this->IDPERFIL->TextColumn, $this->IDPERFIL->DBFormat) = array("IDPERFIL", "NOMEGRUPO", "");
            $this->IDPERFIL->Required = true;
            $this->ATIVO = new clsControl(ccsListBox, "ATIVO", "ATIVO", ccsText, "", CCGetRequestParam("ATIVO", $Method, NULL), $this);
            $this->ATIVO->DSType = dsListOfValues;
            $this->ATIVO->Values = array(array("S", "Sim"), array("N", "N�o"));
            $this->ATIVO->Required = true;
            $this->Button_Insert = new clsButton("Button_Insert", $Method, $this);
            $this->Button_Update = new clsButton("Button_Update", $Method, $this);
            $this->Button_Delete = new clsButton("Button_Delete", $Method, $this);
            if(!$this->FormSubmitted) {
                if(!is_array($this->ATIVO->Value) && !strlen($this->ATIVO->Value) && $this->ATIVO->Value !== false)
                    $this->ATIVO->SetText(S);
            }
        }
    }
//End Class_Initialize Event

//Initialize Method @28-D6CB1C94
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->DataSource->Parameters["urlID"] = CCGetFromGet("ID", NULL);
    }
//End Initialize Method

//Validate Method @28-59598B09
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->ID->Validate() && $Validation);
        $Validation = ($this->GRP_ID->Validate() && $Validation);
        $Validation = ($this->CODOPE->Validate() && $Validation);
        $Validation = ($this->DESOPE->Validate() && $Validation);
        $Validation = ($this->PASSWO->Validate() && $Validation);
        $Validation = ($this->CODUNI->Validate() && $Validation);
        $Validation = ($this->IDPERFIL->Validate() && $Validation);
        $Validation = ($this->ATIVO->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->ID->Errors->Count() == 0);
        $Validation =  $Validation && ($this->GRP_ID->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODOPE->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DESOPE->Errors->Count() == 0);
        $Validation =  $Validation && ($this->PASSWO->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODUNI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->IDPERFIL->Errors->Count() == 0);
        $Validation =  $Validation && ($this->ATIVO->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @28-88453576
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->ID->Errors->Count());
        $errors = ($errors || $this->GRP_ID->Errors->Count());
        $errors = ($errors || $this->CODOPE->Errors->Count());
        $errors = ($errors || $this->DESOPE->Errors->Count());
        $errors = ($errors || $this->PASSWO->Errors->Count());
        $errors = ($errors || $this->CODUNI->Errors->Count());
        $errors = ($errors || $this->IDPERFIL->Errors->Count());
        $errors = ($errors || $this->ATIVO->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @28-B908BA44
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->DataSource->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->DataSource->AllParametersSet;
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = $this->EditMode ? "Button_Update" : "Button_Insert";
            if($this->Button_Insert->Pressed) {
                $this->PressedButton = "Button_Insert";
            } else if($this->Button_Update->Pressed) {
                $this->PressedButton = "Button_Update";
            } else if($this->Button_Delete->Pressed) {
                $this->PressedButton = "Button_Delete";
            }
        }
        $Redirect = $FileName . "?" . CCGetQueryString("QueryString", array("ccsForm"));
        if($this->PressedButton == "Button_Delete") {
            if(!CCGetEvent($this->Button_Delete->CCSEvents, "OnClick", $this->Button_Delete) || !$this->DeleteRow()) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button_Insert") {
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick", $this->Button_Insert) || !$this->InsertRow()) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "Button_Update") {
                if(!CCGetEvent($this->Button_Update->CCSEvents, "OnClick", $this->Button_Update) || !$this->UpdateRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
        if ($Redirect)
            $this->DataSource->close();
    }
//End Operation Method

//InsertRow Method @28-63C492B3
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert", $this);
        if(!$this->InsertAllowed) return false;
        $this->DataSource->ID->SetValue($this->ID->GetValue(true));
        $this->DataSource->GRP_ID->SetValue($this->GRP_ID->GetValue(true));
        $this->DataSource->CODOPE->SetValue($this->CODOPE->GetValue(true));
        $this->DataSource->DESOPE->SetValue($this->DESOPE->GetValue(true));
        $this->DataSource->PASSWO->SetValue($this->PASSWO->GetValue(true));
        $this->DataSource->CODUNI->SetValue($this->CODUNI->GetValue(true));
        $this->DataSource->IDPERFIL->SetValue($this->IDPERFIL->GetValue(true));
        $this->DataSource->ATIVO->SetValue($this->ATIVO->GetValue(true));
        $this->DataSource->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert", $this);
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//UpdateRow Method @28-9240AB74
    function UpdateRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeUpdate", $this);
        if(!$this->UpdateAllowed) return false;
        $this->DataSource->ID->SetValue($this->ID->GetValue(true));
        $this->DataSource->GRP_ID->SetValue($this->GRP_ID->GetValue(true));
        $this->DataSource->CODOPE->SetValue($this->CODOPE->GetValue(true));
        $this->DataSource->DESOPE->SetValue($this->DESOPE->GetValue(true));
        $this->DataSource->PASSWO->SetValue($this->PASSWO->GetValue(true));
        $this->DataSource->CODUNI->SetValue($this->CODUNI->GetValue(true));
        $this->DataSource->IDPERFIL->SetValue($this->IDPERFIL->GetValue(true));
        $this->DataSource->ATIVO->SetValue($this->ATIVO->GetValue(true));
        $this->DataSource->Update();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterUpdate", $this);
        return (!$this->CheckErrors());
    }
//End UpdateRow Method

//DeleteRow Method @28-299D98C3
    function DeleteRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDelete", $this);
        if(!$this->DeleteAllowed) return false;
        $this->DataSource->Delete();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDelete", $this);
        return (!$this->CheckErrors());
    }
//End DeleteRow Method

//Show Method @28-21632506
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->CODUNI->Prepare();
        $this->IDPERFIL->Prepare();
        $this->ATIVO->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->DataSource->Errors->Count()){
                $this->Errors->AddErrors($this->DataSource->Errors);
                $this->DataSource->Errors->clear();
            }
            $this->DataSource->Open();
            if($this->DataSource->Errors->Count() == 0 && $this->DataSource->next_record()) {
                $this->DataSource->SetValues();
                if(!$this->FormSubmitted){
                    $this->ID->SetValue($this->DataSource->ID->GetValue());
                    $this->GRP_ID->SetValue($this->DataSource->GRP_ID->GetValue());
                    $this->CODOPE->SetValue($this->DataSource->CODOPE->GetValue());
                    $this->DESOPE->SetValue($this->DataSource->DESOPE->GetValue());
                    $this->PASSWO->SetValue($this->DataSource->PASSWO->GetValue());
                    $this->CODUNI->SetValue($this->DataSource->CODUNI->GetValue());
                    $this->IDPERFIL->SetValue($this->DataSource->IDPERFIL->GetValue());
                    $this->ATIVO->SetValue($this->DataSource->ATIVO->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->ID->Errors->ToString());
            $Error = ComposeStrings($Error, $this->GRP_ID->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODOPE->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DESOPE->Errors->ToString());
            $Error = ComposeStrings($Error, $this->PASSWO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODUNI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->IDPERFIL->Errors->ToString());
            $Error = ComposeStrings($Error, $this->ATIVO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataSource->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Insert->Visible = !$this->EditMode && $this->InsertAllowed;
        $this->Button_Update->Visible = $this->EditMode && $this->UpdateAllowed;
        $this->Button_Delete->Visible = $this->EditMode && $this->DeleteAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->ID->Show();
        $this->GRP_ID->Show();
        $this->CODOPE->Show();
        $this->DESOPE->Show();
        $this->PASSWO->Show();
        $this->CODUNI->Show();
        $this->IDPERFIL->Show();
        $this->ATIVO->Show();
        $this->Button_Insert->Show();
        $this->Button_Update->Show();
        $this->Button_Delete->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

} //End TABOPE Class @28-FCB6E20C

class clsTABOPEDataSource extends clsDBFaturar {  //TABOPEDataSource Class @28-D74957F2

//DataSource Variables @28-8A676F94
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $InsertParameters;
    public $UpdateParameters;
    public $DeleteParameters;
    public $wp;
    public $AllParametersSet;

    public $InsertFields = array();
    public $UpdateFields = array();

    // Datasource fields
    public $ID;
    public $GRP_ID;
    public $CODOPE;
    public $DESOPE;
    public $PASSWO;
    public $CODUNI;
    public $IDPERFIL;
    public $ATIVO;
//End DataSource Variables

//DataSourceClass_Initialize Event @28-E8864590
    function clsTABOPEDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Record TABOPE/Error";
        $this->Initialize();
        $this->ID = new clsField("ID", ccsFloat, "");
        $this->GRP_ID = new clsField("GRP_ID", ccsFloat, "");
        $this->CODOPE = new clsField("CODOPE", ccsText, "");
        $this->DESOPE = new clsField("DESOPE", ccsText, "");
        $this->PASSWO = new clsField("PASSWO", ccsText, "");
        $this->CODUNI = new clsField("CODUNI", ccsText, "");
        $this->IDPERFIL = new clsField("IDPERFIL", ccsFloat, "");
        $this->ATIVO = new clsField("ATIVO", ccsText, "");

        $this->InsertFields["ID"] = array("Name" => "ID", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->InsertFields["GRP_ID"] = array("Name" => "GRP_ID", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->InsertFields["CODOPE"] = array("Name" => "CODOPE", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DESOPE"] = array("Name" => "DESOPE", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["PASSWO"] = array("Name" => "PASSWO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODUNI"] = array("Name" => "CODUNI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["IDPERFIL"] = array("Name" => "IDPERFIL", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->InsertFields["ATIVO"] = array("Name" => "ATIVO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["ID"] = array("Name" => "ID", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->UpdateFields["GRP_ID"] = array("Name" => "GRP_ID", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODOPE"] = array("Name" => "CODOPE", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["DESOPE"] = array("Name" => "DESOPE", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["PASSWO"] = array("Name" => "PASSWO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODUNI"] = array("Name" => "CODUNI", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["IDPERFIL"] = array("Name" => "IDPERFIL", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->UpdateFields["ATIVO"] = array("Name" => "ATIVO", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
    }
//End DataSourceClass_Initialize Event

//Prepare Method @28-28574793
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlID", ccsFloat, "", "", $this->Parameters["urlID"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "ID", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsFloat),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @28-A4A4D7D5
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->SQL = "SELECT * \n\n" .
        "FROM TABOPE {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @28-4F5BA5D6
    function SetValues()
    {
        $this->ID->SetDBValue(trim($this->f("ID")));
        $this->GRP_ID->SetDBValue(trim($this->f("GRP_ID")));
        $this->CODOPE->SetDBValue($this->f("CODOPE"));
        $this->DESOPE->SetDBValue($this->f("DESOPE"));
        $this->PASSWO->SetDBValue($this->f("PASSWO"));
        $this->CODUNI->SetDBValue($this->f("CODUNI"));
        $this->IDPERFIL->SetDBValue(trim($this->f("IDPERFIL")));
        $this->ATIVO->SetDBValue($this->f("ATIVO"));
    }
//End SetValues Method

//Insert Method @28-C32549A4
    function Insert()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert", $this->Parent);
        $this->InsertFields["ID"]["Value"] = $this->ID->GetDBValue(true);
        $this->InsertFields["GRP_ID"]["Value"] = $this->GRP_ID->GetDBValue(true);
        $this->InsertFields["CODOPE"]["Value"] = $this->CODOPE->GetDBValue(true);
        $this->InsertFields["DESOPE"]["Value"] = $this->DESOPE->GetDBValue(true);
        $this->InsertFields["PASSWO"]["Value"] = $this->PASSWO->GetDBValue(true);
        $this->InsertFields["CODUNI"]["Value"] = $this->CODUNI->GetDBValue(true);
        $this->InsertFields["IDPERFIL"]["Value"] = $this->IDPERFIL->GetDBValue(true);
        $this->InsertFields["ATIVO"]["Value"] = $this->ATIVO->GetDBValue(true);
        $this->SQL = CCBuildInsert("TABOPE", $this->InsertFields, $this);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert", $this->Parent);
        }
    }
//End Insert Method

//Update Method @28-5B26F280
    function Update()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildUpdate", $this->Parent);
        $this->UpdateFields["ID"]["Value"] = $this->ID->GetDBValue(true);
        $this->UpdateFields["GRP_ID"]["Value"] = $this->GRP_ID->GetDBValue(true);
        $this->UpdateFields["CODOPE"]["Value"] = $this->CODOPE->GetDBValue(true);
        $this->UpdateFields["DESOPE"]["Value"] = $this->DESOPE->GetDBValue(true);
        $this->UpdateFields["PASSWO"]["Value"] = $this->PASSWO->GetDBValue(true);
        $this->UpdateFields["CODUNI"]["Value"] = $this->CODUNI->GetDBValue(true);
        $this->UpdateFields["IDPERFIL"]["Value"] = $this->IDPERFIL->GetDBValue(true);
        $this->UpdateFields["ATIVO"]["Value"] = $this->ATIVO->GetDBValue(true);
        $this->SQL = CCBuildUpdate("TABOPE", $this->UpdateFields, $this);
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteUpdate", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteUpdate", $this->Parent);
        }
    }
//End Update Method

//Delete Method @28-F7159644
    function Delete()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildDelete", $this->Parent);
        $this->SQL = "DELETE FROM TABOPE";
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteDelete", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteDelete", $this->Parent);
        }
    }
//End Delete Method

} //End TABOPEDataSource Class @28-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-2EED9B1D
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "ManutTabOpe.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Include events file @1-7E63D481
include("./ManutTabOpe_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-181E3298
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$TABOPE = new clsRecordTABOPE("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->TABOPE = & $TABOPE;
$MainPage->rodape = & $rodape;
$TABOPE->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-2B073B3A
$cabec->Operations();
$TABOPE->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-9AB27E01
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($TABOPE);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-1E1A85B5
$cabec->Show();
$TABOPE->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-FE37CA8C
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($TABOPE);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
