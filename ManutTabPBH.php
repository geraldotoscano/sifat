<?php
//Include Common Files @1-7C727B6A
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "ManutTabPBH.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordTABPBH { //TABPBH Class @4-402330E7

//Variables @4-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @4-0A1B8265
    function clsRecordTABPBH($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record TABPBH/Error";
        $this->DataSource = new clsTABPBHDataSource($this);
        $this->ds = & $this->DataSource;
        $this->InsertAllowed = true;
        $this->UpdateAllowed = true;
        $this->DeleteAllowed = true;
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "TABPBH";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->CODINC = new clsControl(ccsHidden, "CODINC", "CODINC", ccsText, "", CCGetRequestParam("CODINC", $Method, NULL), $this);
            $this->CODALT = new clsControl(ccsHidden, "CODALT", "CODALT", ccsText, "", CCGetRequestParam("CODALT", $Method, NULL), $this);
            $this->DATINC = new clsControl(ccsHidden, "DATINC", "DATINC", ccsText, "", CCGetRequestParam("DATINC", $Method, NULL), $this);
            $this->DATALT = new clsControl(ccsHidden, "DATALT", "DATALT", ccsText, "", CCGetRequestParam("DATALT", $Method, NULL), $this);
            $this->PROCES = new clsControl(ccsHidden, "PROCES", "PROCES", ccsText, "", CCGetRequestParam("PROCES", $Method, NULL), $this);
            $this->MESREF = new clsControl(ccsTextBox, "MESREF", "MESREF", ccsText, "", CCGetRequestParam("MESREF", $Method, NULL), $this);
            $this->VALPBH = new clsControl(ccsTextBox, "VALPBH", "Valor em R$", ccsFloat, "", CCGetRequestParam("VALPBH", $Method, NULL), $this);
            $this->VALPBH->Required = true;
            $this->Button_Insert = new clsButton("Button_Insert", $Method, $this);
            $this->Button_Update = new clsButton("Button_Update", $Method, $this);
            $this->Button_Delete = new clsButton("Button_Delete", $Method, $this);
            $this->Button_Cancel = new clsButton("Button_Cancel", $Method, $this);
            if(!$this->FormSubmitted) {
                if(!is_array($this->PROCES->Value) && !strlen($this->PROCES->Value) && $this->PROCES->Value !== false)
                    $this->PROCES->SetText("T");
            }
        }
    }
//End Class_Initialize Event

//Initialize Method @4-79F28AA1
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->DataSource->Parameters["urlMESREF"] = CCGetFromGet("MESREF", NULL);
    }
//End Initialize Method

//Validate Method @4-28015C57
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        if($this->EditMode && strlen($this->DataSource->Where))
            $Where = " AND NOT (" . $this->DataSource->Where . ")";
        $this->DataSource->MESREF->SetValue($this->MESREF->GetValue());
        if(CCDLookUp("COUNT(*)", "TABPBH", "MESREF=" . $this->DataSource->ToSQL($this->DataSource->MESREF->GetDBValue(), $this->DataSource->MESREF->DataType) . $Where, $this->DataSource) > 0)
            $this->MESREF->Errors->addError($CCSLocales->GetText("CCS_UniqueValue", "MESREF"));
        $Validation = ($this->CODINC->Validate() && $Validation);
        $Validation = ($this->CODALT->Validate() && $Validation);
        $Validation = ($this->DATINC->Validate() && $Validation);
        $Validation = ($this->DATALT->Validate() && $Validation);
        $Validation = ($this->PROCES->Validate() && $Validation);
        $Validation = ($this->MESREF->Validate() && $Validation);
        $Validation = ($this->VALPBH->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->CODINC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODALT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DATINC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DATALT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->PROCES->Errors->Count() == 0);
        $Validation =  $Validation && ($this->MESREF->Errors->Count() == 0);
        $Validation =  $Validation && ($this->VALPBH->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @4-D45B12C2
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->CODINC->Errors->Count());
        $errors = ($errors || $this->CODALT->Errors->Count());
        $errors = ($errors || $this->DATINC->Errors->Count());
        $errors = ($errors || $this->DATALT->Errors->Count());
        $errors = ($errors || $this->PROCES->Errors->Count());
        $errors = ($errors || $this->MESREF->Errors->Count());
        $errors = ($errors || $this->VALPBH->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @4-460EDC52
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->DataSource->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->DataSource->AllParametersSet;
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = $this->EditMode ? "Button_Update" : "Button_Insert";
            if($this->Button_Insert->Pressed) {
                $this->PressedButton = "Button_Insert";
            } else if($this->Button_Update->Pressed) {
                $this->PressedButton = "Button_Update";
            } else if($this->Button_Delete->Pressed) {
                $this->PressedButton = "Button_Delete";
            } else if($this->Button_Cancel->Pressed) {
                $this->PressedButton = "Button_Cancel";
            }
        }
        $Redirect = "index.php" . "?" . CCGetQueryString("QueryString", array("ccsForm", "MESREF"));
        if($this->PressedButton == "Button_Delete") {
            if(!CCGetEvent($this->Button_Delete->CCSEvents, "OnClick", $this->Button_Delete) || !$this->DeleteRow()) {
                $Redirect = "";
            }
        } else if($this->PressedButton == "Button_Cancel") {
            if(!CCGetEvent($this->Button_Cancel->CCSEvents, "OnClick", $this->Button_Cancel)) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button_Insert") {
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick", $this->Button_Insert) || !$this->InsertRow()) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "Button_Update") {
                if(!CCGetEvent($this->Button_Update->CCSEvents, "OnClick", $this->Button_Update) || !$this->UpdateRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
        if ($Redirect)
            $this->DataSource->close();
    }
//End Operation Method

//InsertRow Method @4-6C911280
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert", $this);
        if(!$this->InsertAllowed) return false;
        $this->DataSource->CODINC->SetValue($this->CODINC->GetValue(true));
        $this->DataSource->CODALT->SetValue($this->CODALT->GetValue(true));
        $this->DataSource->DATINC->SetValue($this->DATINC->GetValue(true));
        $this->DataSource->DATALT->SetValue($this->DATALT->GetValue(true));
        $this->DataSource->PROCES->SetValue($this->PROCES->GetValue(true));
        $this->DataSource->MESREF->SetValue($this->MESREF->GetValue(true));
        $this->DataSource->VALPBH->SetValue($this->VALPBH->GetValue(true));
        $this->DataSource->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert", $this);
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//UpdateRow Method @4-BB3D5F45
    function UpdateRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeUpdate", $this);
        if(!$this->UpdateAllowed) return false;
        $this->DataSource->CODINC->SetValue($this->CODINC->GetValue(true));
        $this->DataSource->CODALT->SetValue($this->CODALT->GetValue(true));
        $this->DataSource->DATINC->SetValue($this->DATINC->GetValue(true));
        $this->DataSource->DATALT->SetValue($this->DATALT->GetValue(true));
        $this->DataSource->PROCES->SetValue($this->PROCES->GetValue(true));
        $this->DataSource->MESREF->SetValue($this->MESREF->GetValue(true));
        $this->DataSource->VALPBH->SetValue($this->VALPBH->GetValue(true));
        $this->DataSource->Update();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterUpdate", $this);
        return (!$this->CheckErrors());
    }
//End UpdateRow Method

//DeleteRow Method @4-299D98C3
    function DeleteRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDelete", $this);
        if(!$this->DeleteAllowed) return false;
        $this->DataSource->Delete();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDelete", $this);
        return (!$this->CheckErrors());
    }
//End DeleteRow Method

//Show Method @4-4EC571C6
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->DataSource->Errors->Count()){
                $this->Errors->AddErrors($this->DataSource->Errors);
                $this->DataSource->Errors->clear();
            }
            $this->DataSource->Open();
            if($this->DataSource->Errors->Count() == 0 && $this->DataSource->next_record()) {
                $this->DataSource->SetValues();
                if(!$this->FormSubmitted){
                    $this->CODINC->SetValue($this->DataSource->CODINC->GetValue());
                    $this->CODALT->SetValue($this->DataSource->CODALT->GetValue());
                    $this->DATINC->SetValue($this->DataSource->DATINC->GetValue());
                    $this->DATALT->SetValue($this->DataSource->DATALT->GetValue());
                    $this->PROCES->SetValue($this->DataSource->PROCES->GetValue());
                    $this->MESREF->SetValue($this->DataSource->MESREF->GetValue());
                    $this->VALPBH->SetValue($this->DataSource->VALPBH->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->CODINC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODALT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DATINC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DATALT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->PROCES->Errors->ToString());
            $Error = ComposeStrings($Error, $this->MESREF->Errors->ToString());
            $Error = ComposeStrings($Error, $this->VALPBH->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataSource->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Insert->Visible = !$this->EditMode && $this->InsertAllowed;
        $this->Button_Update->Visible = $this->EditMode && $this->UpdateAllowed;
        $this->Button_Delete->Visible = $this->EditMode && $this->DeleteAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->CODINC->Show();
        $this->CODALT->Show();
        $this->DATINC->Show();
        $this->DATALT->Show();
        $this->PROCES->Show();
        $this->MESREF->Show();
        $this->VALPBH->Show();
        $this->Button_Insert->Show();
        $this->Button_Update->Show();
        $this->Button_Delete->Show();
        $this->Button_Cancel->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

} //End TABPBH Class @4-FCB6E20C

class clsTABPBHDataSource extends clsDBFaturar {  //TABPBHDataSource Class @4-856149A1

//DataSource Variables @4-7C98A6C8
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $InsertParameters;
    public $UpdateParameters;
    public $DeleteParameters;
    public $wp;
    public $AllParametersSet;

    public $InsertFields = array();
    public $UpdateFields = array();

    // Datasource fields
    public $CODINC;
    public $CODALT;
    public $DATINC;
    public $DATALT;
    public $PROCES;
    public $MESREF;
    public $VALPBH;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-9FCF3CAC
    function clsTABPBHDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Record TABPBH/Error";
        $this->Initialize();
        $this->CODINC = new clsField("CODINC", ccsText, "");
        $this->CODALT = new clsField("CODALT", ccsText, "");
        $this->DATINC = new clsField("DATINC", ccsText, "");
        $this->DATALT = new clsField("DATALT", ccsText, "");
        $this->PROCES = new clsField("PROCES", ccsText, "");
        $this->MESREF = new clsField("MESREF", ccsText, "");
        $this->VALPBH = new clsField("VALPBH", ccsFloat, "");

        $this->InsertFields["CODINC"] = array("Name" => "CODINC", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["CODALT"] = array("Name" => "CODALT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DATINC"] = array("Name" => "DATINC", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DATALT"] = array("Name" => "DATALT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["PROCES"] = array("Name" => "PROCES", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["MESREF"] = array("Name" => "MESREF", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["VALPBH"] = array("Name" => "VALPBH", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODINC"] = array("Name" => "CODINC", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODALT"] = array("Name" => "CODALT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["DATINC"] = array("Name" => "DATINC", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["DATALT"] = array("Name" => "DATALT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["PROCES"] = array("Name" => "PROCES", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["MESREF"] = array("Name" => "MESREF", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["VALPBH"] = array("Name" => "VALPBH", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
    }
//End DataSourceClass_Initialize Event

//Prepare Method @4-B49C2B1A
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlMESREF", ccsText, "", "", $this->Parameters["urlMESREF"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "MESREF", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @4-5764F82D
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->SQL = "SELECT * \n\n" .
        "FROM TABPBH {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @4-C2A7D6A2
    function SetValues()
    {
        $this->CODINC->SetDBValue($this->f("CODINC"));
        $this->CODALT->SetDBValue($this->f("CODALT"));
        $this->DATINC->SetDBValue($this->f("DATINC"));
        $this->DATALT->SetDBValue($this->f("DATALT"));
        $this->PROCES->SetDBValue($this->f("PROCES"));
        $this->MESREF->SetDBValue($this->f("MESREF"));
        $this->VALPBH->SetDBValue(trim($this->f("VALPBH")));
    }
//End SetValues Method

//Insert Method @4-E6DAF6A9
    function Insert()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert", $this->Parent);
        $this->InsertFields["CODINC"]["Value"] = $this->CODINC->GetDBValue(true);
        $this->InsertFields["CODALT"]["Value"] = $this->CODALT->GetDBValue(true);
        $this->InsertFields["DATINC"]["Value"] = $this->DATINC->GetDBValue(true);
        $this->InsertFields["DATALT"]["Value"] = $this->DATALT->GetDBValue(true);
        $this->InsertFields["PROCES"]["Value"] = $this->PROCES->GetDBValue(true);
        $this->InsertFields["MESREF"]["Value"] = $this->MESREF->GetDBValue(true);
        $this->InsertFields["VALPBH"]["Value"] = $this->VALPBH->GetDBValue(true);
        $this->SQL = CCBuildInsert("TABPBH", $this->InsertFields, $this);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert", $this->Parent);
        }
    }
//End Insert Method

//Update Method @4-313BF6CC
    function Update()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildUpdate", $this->Parent);
        $this->UpdateFields["CODINC"]["Value"] = $this->CODINC->GetDBValue(true);
        $this->UpdateFields["CODALT"]["Value"] = $this->CODALT->GetDBValue(true);
        $this->UpdateFields["DATINC"]["Value"] = $this->DATINC->GetDBValue(true);
        $this->UpdateFields["DATALT"]["Value"] = $this->DATALT->GetDBValue(true);
        $this->UpdateFields["PROCES"]["Value"] = $this->PROCES->GetDBValue(true);
        $this->UpdateFields["MESREF"]["Value"] = $this->MESREF->GetDBValue(true);
        $this->UpdateFields["VALPBH"]["Value"] = $this->VALPBH->GetDBValue(true);
        $this->SQL = CCBuildUpdate("TABPBH", $this->UpdateFields, $this);
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteUpdate", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteUpdate", $this->Parent);
        }
    }
//End Update Method

//Delete Method @4-80868485
    function Delete()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildDelete", $this->Parent);
        $this->SQL = "DELETE FROM TABPBH";
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteDelete", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteDelete", $this->Parent);
        }
    }
//End Delete Method

} //End TABPBHDataSource Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-76043594
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "ManutTabPBH.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-7FACF37D
CCSecurityRedirect("1;3", "");
//End Authenticate User

//Include events file @1-ADC3523B
include("./ManutTabPBH_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-74ACDFFA
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$TABPBH = new clsRecordTABPBH("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->TABPBH = & $TABPBH;
$MainPage->rodape = & $rodape;
$TABPBH->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-7473158F
$cabec->Operations();
$TABPBH->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-6B587D12
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($TABPBH);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-81DE271A
$cabec->Show();
$TABPBH->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-B8DA4D01
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($TABPBH);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
