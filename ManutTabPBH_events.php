<?php
//BindEvents Method @1-2C707280
function BindEvents()
{
    global $TABPBH;
    global $CCSEvents;
    $TABPBH->MESREF->CCSEvents["BeforeShow"] = "TABPBH_MESREF_BeforeShow";
    $TABPBH->CCSEvents["BeforeShow"] = "TABPBH_BeforeShow";
    $TABPBH->CCSEvents["BeforeInsert"] = "TABPBH_BeforeInsert";
    $TABPBH->CCSEvents["BeforeUpdate"] = "TABPBH_BeforeUpdate";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL      global $DBfaturar;
//DEL      $Page = CCGetParentPage($sender);
//DEL      $ccs_result = CCDLookUp("date()", "tabpbh", "", $Page->Connections["Faturar"]);
//DEL      //$ccs_result = CCParseDate($ccs_result, $Page->Connections["Faturar"]->DateFormat);
//DEL      //$Component->SetValue($ccs_result);
//DEL  	$TABPBH->DATINC->SetValue($ccs_result);
//DEL  // -------------------------

//TABPBH_MESREF_BeforeShow @11-68CF0234
function TABPBH_MESREF_BeforeShow(& $sender)
{
    $TABPBH_MESREF_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABPBH; //Compatibility
//End TABPBH_MESREF_BeforeShow

//Custom Code @21-2A29BDB7
// -------------------------
    // Write your own code here.
   if ($TABPBH->Button_Insert->Visible)
   {
      global $DBfaturar;
      $Page = CCGetParentPage($sender);
	  // Obtem do o maior ano do atributo mesref da tabela tabpbh. CCDLookUp retorna uma
	  // string portanto o $ccs_result abaixo � um string
      $ccs_result = CCDLookUp("max(to_number(substr(mesref,4,4)))", "tabpbh", "", $Page->Connections["Faturar"]);
      $mMaior_Ano = (int)$ccs_result;
	  
	  if (is_null($mMaior_Ano) || $mMaior_Ano==0)
	  {
	  	$mMaior_Ano = (int)substr(CCGetSession("DataSist")."",6,4);
	  }

      $Tabela = new clsDBfaturar();
      $Tabela->query("SELECT mesref FROM tabpbh where substr(mesref,4,4)='".$mMaior_Ano."' order by mesref desc");
	  // Select que retorna os mesref da tabela tabpbh do maior ano em ordem decrescente.
	  // o comando abaixo retorna o primeiro registro e consequentemente o menor mesref do
	  // maior ano.

	  
      $Tabela->next_record();
      
      $mMes = (int)substr($Tabela->f("mesref"),0,2);
	  if (is_null($mMes) || $mMes==0)
	   {
         $mMes = (int)substr(CCGetSession("DataSist")."",3,2) - 1;
	   }
      else
	   {
         $mMes = ((int)$mMes) + 1;
	   }
        
      

      if ($mMes == 13)
      {
         $mMes = "01";
         $mMaior_Ano += 1;
      }
	  else
	  {
	     if ($mMes>=1 and $mMes<=9)
		 {
		    $mMes = "0".$mMes;
		 }
	  }
      $TABPBH->MESREF->SetValue($mMes."/".$mMaior_Ano);
   }
// -------------------------
//End Custom Code

//DLookup @24-5A7DF5DA
//End DLookup

//Close TABPBH_MESREF_BeforeShow @11-F84017B5
    return $TABPBH_MESREF_BeforeShow;
}
//End Close TABPBH_MESREF_BeforeShow

//DEL      global $DBfaturar;
//DEL      $Page = CCGetParentPage($sender);
//DEL      $ccs_result = CCDLookUp("date()", "tabpbh", "1=0", $Page->Connections["Faturar"]);
//DEL      $ccs_result = CCParseDate($ccs_result, $Page->Connections["Faturar"]->DateFormat);
//DEL      //$Component->SetValue($ccs_result);
//DEL  	$TABPBH->DATINC->SetValue($ccs_result);
//DEL  	$TABPBH->CODINC->Value = CCGetUserLogin();

//DEL      global $DBfaturar;
//DEL      $Page = CCGetParentPage($sender);
//DEL      $ccs_result = CCDLookUp("date()", "tabpbh", "1=0", $Page->Connections["Faturar"]);
//DEL      $ccs_result = CCParseDate($ccs_result, $Page->Connections["Faturar"]->DateFormat);
//DEL      //$Component->SetValue($ccs_result);
//DEL      $TABPBH->DATALT->SetValue($ccs_result);
//DEL      $TABPBH->CODALT->Value = CCGetUserLogin();

//TABPBH_BeforeShow @4-77DAD227
function TABPBH_BeforeShow(& $sender)
{
    $TABPBH_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABPBH; //Compatibility
//End TABPBH_BeforeShow

//Custom Code @20-2A29BDB7
// -------------------------
    // Write your own code here.
    global $DBfaturar;
    $Page = CCGetParentPage($sender);
	if ($TABPBH->Button_Delete->Visible)
	{
       $ccs_result = CCDLookUp("mesref", "cadfat", "mesref='".$TABPBH->MESREF->Value."'", $Page->Connections["Faturar"]);
       //$Component->SetValue($ccs_result);
	   if ($ccs_result != "")
	   {
	      $TABPBH->Button_Delete->Visible = false;
	   }
	}

// -------------------------
//End Custom Code

//Close TABPBH_BeforeShow @4-7745A70A
    return $TABPBH_BeforeShow;
}
//End Close TABPBH_BeforeShow

//TABPBH_BeforeInsert @4-87539B1A
function TABPBH_BeforeInsert(& $sender)
{
    $TABPBH_BeforeInsert = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABPBH; //Compatibility
//End TABPBH_BeforeInsert

//Custom Code @26-2A29BDB7
// -------------------------
    // Write your own code here.
   $TABPBH->CODINC->Value = CCGetSession("IDUsuario");
   $TABPBH->DATINC->Value = CCGetSession("DataSist");
// -------------------------
//End Custom Code

//Close TABPBH_BeforeInsert @4-E8D06B21
    return $TABPBH_BeforeInsert;
}
//End Close TABPBH_BeforeInsert

//TABPBH_BeforeUpdate @4-77DFA116
function TABPBH_BeforeUpdate(& $sender)
{
    $TABPBH_BeforeUpdate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABPBH; //Compatibility
//End TABPBH_BeforeUpdate

//Custom Code @27-2A29BDB7
// -------------------------
    // Write your own code here.
   $TABPBH->DATALT->Value = CCGetSession("DataSist");
   $TABPBH->CODALT->Value = CCGetSession("IDUsuario");
// -------------------------
//End Custom Code

//Close TABPBH_BeforeUpdate @4-27F9AAAE
    return $TABPBH_BeforeUpdate;
}
//End Close TABPBH_BeforeUpdate

//Page_BeforeShow @1-B21833B7
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutTabPBH; //Compatibility
//End Page_BeforeShow

//Custom Code @34-2A29BDB7
// -------------------------

        include("controle_acesso.php");
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(14);
		controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
