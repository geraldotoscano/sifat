<?php
//Include Common Files @1-D389AA76
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "ManutTabPerfilFuncao.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordTABPERFIL_FUNCAO { //TABPERFIL_FUNCAO Class @4-514E42D8

//Variables @4-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @4-AC2C4B36
    function clsRecordTABPERFIL_FUNCAO($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record TABPERFIL_FUNCAO/Error";
        $this->DataSource = new clsTABPERFIL_FUNCAODataSource($this);
        $this->ds = & $this->DataSource;
        $this->DeleteAllowed = true;
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "TABPERFIL_FUNCAO";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->IDPERFIL = new clsControl(ccsTextBox, "IDPERFIL", "IDPERFIL", ccsFloat, "", CCGetRequestParam("IDPERFIL", $Method, NULL), $this);
            $this->IDPERFIL->Required = true;
            $this->IDPERFIL->Visible = false;
            $this->NomePerfil = new clsControl(ccsTextBox, "NomePerfil", "NomePerfil", ccsText, "", CCGetRequestParam("NomePerfil", $Method, NULL), $this);
            $this->CODFUNCAO = new clsControl(ccsTextBox, "CODFUNCAO", "CODFUNCAO", ccsFloat, "", CCGetRequestParam("CODFUNCAO", $Method, NULL), $this);
            $this->CODFUNCAO->Required = true;
            $this->CODFUNCAO->Visible = false;
            $this->NomeFuncao = new clsControl(ccsTextBox, "NomeFuncao", "NomeFuncao", ccsText, "", CCGetRequestParam("NomeFuncao", $Method, NULL), $this);
            $this->Button_Delete = new clsButton("Button_Delete", $Method, $this);
        }
    }
//End Class_Initialize Event

//Initialize Method @4-5C7E2520
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->DataSource->Parameters["urlIDPERFIL"] = CCGetFromGet("IDPERFIL", NULL);
    }
//End Initialize Method

//Validate Method @4-4BA43E05
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->IDPERFIL->Validate() && $Validation);
        $Validation = ($this->NomePerfil->Validate() && $Validation);
        $Validation = ($this->CODFUNCAO->Validate() && $Validation);
        $Validation = ($this->NomeFuncao->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->IDPERFIL->Errors->Count() == 0);
        $Validation =  $Validation && ($this->NomePerfil->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODFUNCAO->Errors->Count() == 0);
        $Validation =  $Validation && ($this->NomeFuncao->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @4-A0790329
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->IDPERFIL->Errors->Count());
        $errors = ($errors || $this->NomePerfil->Errors->Count());
        $errors = ($errors || $this->CODFUNCAO->Errors->Count());
        $errors = ($errors || $this->NomeFuncao->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @4-180D71FA
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->DataSource->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->DataSource->AllParametersSet;
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = $this->EditMode ? "Button_Delete" : "";
            if($this->Button_Delete->Pressed) {
                $this->PressedButton = "Button_Delete";
            }
        }
        $Redirect = $FileName . "?" . CCGetQueryString("QueryString", array("ccsForm"));
        if($this->PressedButton == "Button_Delete") {
            if(!CCGetEvent($this->Button_Delete->CCSEvents, "OnClick", $this->Button_Delete) || !$this->DeleteRow()) {
                $Redirect = "";
            }
        } else {
            $Redirect = "";
        }
        if ($Redirect)
            $this->DataSource->close();
    }
//End Operation Method

//DeleteRow Method @4-299D98C3
    function DeleteRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDelete", $this);
        if(!$this->DeleteAllowed) return false;
        $this->DataSource->Delete();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDelete", $this);
        return (!$this->CheckErrors());
    }
//End DeleteRow Method

//Show Method @4-F7890C92
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->DataSource->Errors->Count()){
                $this->Errors->AddErrors($this->DataSource->Errors);
                $this->DataSource->Errors->clear();
            }
            $this->DataSource->Open();
            if($this->DataSource->Errors->Count() == 0 && $this->DataSource->next_record()) {
                $this->DataSource->SetValues();
                if(!$this->FormSubmitted){
                    $this->IDPERFIL->SetValue($this->DataSource->IDPERFIL->GetValue());
                    $this->CODFUNCAO->SetValue($this->DataSource->CODFUNCAO->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->IDPERFIL->Errors->ToString());
            $Error = ComposeStrings($Error, $this->NomePerfil->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODFUNCAO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->NomeFuncao->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataSource->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Delete->Visible = $this->EditMode && $this->DeleteAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->IDPERFIL->Show();
        $this->NomePerfil->Show();
        $this->CODFUNCAO->Show();
        $this->NomeFuncao->Show();
        $this->Button_Delete->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

} //End TABPERFIL_FUNCAO Class @4-FCB6E20C

class clsTABPERFIL_FUNCAODataSource extends clsDBFaturar {  //TABPERFIL_FUNCAODataSource Class @4-0C180E9E

//DataSource Variables @4-F5F0CA12
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $DeleteParameters;
    public $wp;
    public $AllParametersSet;


    // Datasource fields
    public $IDPERFIL;
    public $NomePerfil;
    public $CODFUNCAO;
    public $NomeFuncao;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-0748D766
    function clsTABPERFIL_FUNCAODataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Record TABPERFIL_FUNCAO/Error";
        $this->Initialize();
        $this->IDPERFIL = new clsField("IDPERFIL", ccsFloat, "");
        $this->NomePerfil = new clsField("NomePerfil", ccsText, "");
        $this->CODFUNCAO = new clsField("CODFUNCAO", ccsFloat, "");
        $this->NomeFuncao = new clsField("NomeFuncao", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//Prepare Method @4-B37BEEE8
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlIDPERFIL", ccsFloat, "", "", $this->Parameters["urlIDPERFIL"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "IDPERFIL", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsFloat),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @4-8BBB9586
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->SQL = "SELECT * \n\n" .
        "FROM TABPERFIL_FUNCAO {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @4-3370B9C8
    function SetValues()
    {
        $this->IDPERFIL->SetDBValue(trim($this->f("IDPERFIL")));
        $this->CODFUNCAO->SetDBValue(trim($this->f("CODFUNCAO")));
    }
//End SetValues Method

//Delete Method @4-4E966818
    function Delete()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildDelete", $this->Parent);
        $this->SQL = "DELETE FROM TABPERFIL_FUNCAO";
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteDelete", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteDelete", $this->Parent);
        }
    }
//End Delete Method

} //End TABPERFIL_FUNCAODataSource Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-7E59CA64
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "ManutTabPerfilFuncao.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Include events file @1-5D806971
include("./ManutTabPerfilFuncao_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-0E4D891F
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$TABPERFIL_FUNCAO = new clsRecordTABPERFIL_FUNCAO("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->TABPERFIL_FUNCAO = & $TABPERFIL_FUNCAO;
$MainPage->rodape = & $rodape;
$TABPERFIL_FUNCAO->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-6A4CE94D
$cabec->Operations();
$TABPERFIL_FUNCAO->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-7D2D55D0
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($TABPERFIL_FUNCAO);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-3B83F6A7
$cabec->Show();
$TABPERFIL_FUNCAO->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-61A0935C
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($TABPERFIL_FUNCAO);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
