<?php
//BindEvents Method @1-D9BBA4C1
function BindEvents()
{
    global $TABPERFIL_FUNCAO;
    global $CCSEvents;
    $TABPERFIL_FUNCAO->NomePerfil->CCSEvents["BeforeShow"] = "TABPERFIL_FUNCAO_NomePerfil_BeforeShow";
    $TABPERFIL_FUNCAO->NomeFuncao->CCSEvents["BeforeShow"] = "TABPERFIL_FUNCAO_NomeFuncao_BeforeShow";
    $TABPERFIL_FUNCAO->Button_Delete->CCSEvents["OnClick"] = "TABPERFIL_FUNCAO_Button_Delete_OnClick";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//TABPERFIL_FUNCAO_NomePerfil_BeforeShow @9-C0DB0861
function TABPERFIL_FUNCAO_NomePerfil_BeforeShow(& $sender)
{
    $TABPERFIL_FUNCAO_NomePerfil_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABPERFIL_FUNCAO; //Compatibility
//End TABPERFIL_FUNCAO_NomePerfil_BeforeShow

//Custom Code @10-2A29BDB7
// -------------------------
   $ds=$TABPERFIL_FUNCAO->ds;
   $perfil=CCGetParam('IDPERFIL','');
   $sender->Value=(CCDLookUp('nomegrupo','tabperfil','idperfil='.$perfil,$ds));
// -------------------------
//End Custom Code

//Close TABPERFIL_FUNCAO_NomePerfil_BeforeShow @9-D8509913
    return $TABPERFIL_FUNCAO_NomePerfil_BeforeShow;
}
//End Close TABPERFIL_FUNCAO_NomePerfil_BeforeShow

//TABPERFIL_FUNCAO_NomeFuncao_BeforeShow @11-006AED47
function TABPERFIL_FUNCAO_NomeFuncao_BeforeShow(& $sender)
{
    $TABPERFIL_FUNCAO_NomeFuncao_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABPERFIL_FUNCAO; //Compatibility
//End TABPERFIL_FUNCAO_NomeFuncao_BeforeShow

//Custom Code @12-2A29BDB7
// -------------------------
   $ds=$TABPERFIL_FUNCAO->ds;
   $funcao=CCGetParam('TABPERFIL_FUNCAO_CODFUNCAO','');
   $sender->Value=(CCDLookUp('descfuncao','tabfuncao','codfuncao='.$funcao,$ds));

// -------------------------
//End Custom Code

//Close TABPERFIL_FUNCAO_NomeFuncao_BeforeShow @11-33953074
    return $TABPERFIL_FUNCAO_NomeFuncao_BeforeShow;
}
//End Close TABPERFIL_FUNCAO_NomeFuncao_BeforeShow

//TABPERFIL_FUNCAO_Button_Delete_OnClick @5-6EF219A3
function TABPERFIL_FUNCAO_Button_Delete_OnClick(& $sender)
{
    $TABPERFIL_FUNCAO_Button_Delete_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABPERFIL_FUNCAO; //Compatibility
//End TABPERFIL_FUNCAO_Button_Delete_OnClick

//Custom Code @13-2A29BDB7
// -------------------------
   $perfil=CCGetParam('IDPERFIL','');
   $funcao=CCGetParam('TABPERFIL_FUNCAO_CODFUNCAO','');
   $Tab_CADFAT = new clsDBfaturar();
   $Tab_CADFAT->query('delete from tabperfil_funcao where idperfil='.$perfil.' and codfuncao='.$funcao);

   $Tab_CADFAT->close();
   header('Location: ManutPerfil.php?IDPERFIL='.$perfil=CCGetParam('IDPERFIL',''));
   exit();
// -------------------------
//End Custom Code

//Close TABPERFIL_FUNCAO_Button_Delete_OnClick @5-303F7AB6
    return $TABPERFIL_FUNCAO_Button_Delete_OnClick;
}
//End Close TABPERFIL_FUNCAO_Button_Delete_OnClick

//Page_BeforeShow @1-ABF53AAC
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutTabPerfilFuncao; //Compatibility
//End Page_BeforeShow

//Custom Code @14-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(1);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
