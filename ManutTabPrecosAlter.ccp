<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="4" sourceType="Table" urlType="Relative" secured="False" allowInsert="True" allowUpdate="True" allowDelete="True" validateData="True" preserveParameters="GET" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" connection="Faturar" name="EQUSER" dataSource="EQUSER" errorSummator="Error" wizardCaption="Add/Edit EQUSER " wizardFormMethod="post" activeCollection="TableParameters" pasteAsReplace="pasteAsReplace" debugMode="False" returnPage="CadTabPreco.ccp" removeParameters="SUBSER;MESREF">
			<Components>
				<Hidden id="15" fieldSourceType="DBColumn" dataType="Text" name="CODINC" fieldSource="CODALT" required="False" caption="CODINC" wizardCaption="CODINC" wizardSize="3" wizardMaxLength="3" wizardIsPassword="False" wizardUseTemplateBlock="False" html="False" editable="True" hasErrorCollection="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<Hidden id="17" fieldSourceType="DBColumn" dataType="Text" name="DATINC" fieldSource="DATALT" required="False" caption="DATINC" wizardCaption="DATINC" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" html="False" editable="True" hasErrorCollection="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<ListBox id="11" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="SUBSER" fieldSource="SUBSER" required="False" caption="Sub Serviço" wizardCaption="SUBSER" wizardSize="2" wizardMaxLength="2" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="SUBSER" boundColumn="SUBSER" textColumn="DESSUB" editable="True" hasErrorCollection="True">
					<Components/>
					<Events>
					</Events>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<TextBox id="12" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="MESREF" fieldSource="MESREF" required="False" caption="Mês / Ano de Referência" wizardCaption="MESREF" wizardSize="7" wizardMaxLength="7" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="TABPBH" boundColumn="MESREF" textColumn="MESREF" html="False" editable="True" hasErrorCollection="True">
					<Components/>
					<Events>
					</Events>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</TextBox>
				<Label id="31" fieldSourceType="DBColumn" dataType="Text" name="Hidden1" html="False" editable="False">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="32" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Hidden id="33" fieldSourceType="DBColumn" dataType="Integer" name="Hidden2" html="False" editable="True" hasErrorCollection="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<TextBox id="13" visible="Yes" fieldSourceType="DBColumn" dataType="Float" name="EQUPBH" fieldSource="EQUPBH" required="True" caption="Equivalência" wizardCaption="EQUPBH" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" html="False" editable="True" hasErrorCollection="True">
					<Components/>
					<Events>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="30"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="14" visible="Yes" fieldSourceType="DBColumn" dataType="Float" name="VALEQU" fieldSource="VALEQU" required="True" caption="Valor em R$" wizardCaption="VALEQU" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" html="False" editable="True" hasErrorCollection="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<Button id="5" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Insert" wizardCaption="Adicionar" operation="Insert">
					<Components/>
					<Events>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="6" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Update" operation="Update" wizardCaption="Mudar" returnPage="CadTabPreco.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="7" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Delete" operation="Delete" wizardCaption="Apagar" returnPage="CadTabPreco.ccp">
					<Components/>
					<Events>
						<Event name="OnClick" type="Client">
							<Actions>
								<Action actionName="Confirmation Message" actionCategory="General" id="8" message="Delete record?"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="9" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Cancel" operation="Cancel" wizardCaption="Cancelar" returnPage="CadTabPreco.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="21"/>
					</Actions>
				</Event>
				<Event name="BeforeUpdate" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="34"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters>
				<TableParameter id="23" conditionType="Parameter" useIsNull="False" field="MESREF" dataType="Text" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="MESREF"/>
				<TableParameter id="26" conditionType="Parameter" useIsNull="False" field="SUBSER" dataType="Text" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="SUBSER"/>
			</TableParameters>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables>
				<JoinTable id="22" tableName="EQUSER" posLeft="10" posTop="10" posWidth="118" posHeight="212"/>
			</JoinTables>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="PHPTemplates" name="ManutTabPrecosAlter_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Code" language="PHPTemplates" name="ManutTabPrecosAlter.php" forShow="True" url="ManutTabPrecosAlter.php" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="36" groupID="1"/>
		<Group id="37" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="38"/>
			</Actions>
		</Event>
	</Events>
</Page>
