<?php
//Include Common Files @1-A50310C1
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "ManutTabPrecosAlter.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordEQUSER { //EQUSER Class @4-F74BAD24

//Variables @4-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @4-C92FB0D6
    function clsRecordEQUSER($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record EQUSER/Error";
        $this->DataSource = new clsEQUSERDataSource($this);
        $this->ds = & $this->DataSource;
        $this->InsertAllowed = true;
        $this->UpdateAllowed = true;
        $this->DeleteAllowed = true;
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "EQUSER";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->CODINC = new clsControl(ccsHidden, "CODINC", "CODINC", ccsText, "", CCGetRequestParam("CODINC", $Method, NULL), $this);
            $this->DATINC = new clsControl(ccsHidden, "DATINC", "DATINC", ccsText, "", CCGetRequestParam("DATINC", $Method, NULL), $this);
            $this->SUBSER = new clsControl(ccsListBox, "SUBSER", "Sub Servi�o", ccsText, "", CCGetRequestParam("SUBSER", $Method, NULL), $this);
            $this->SUBSER->DSType = dsTable;
            $this->SUBSER->DataSource = new clsDBFaturar();
            $this->SUBSER->ds = & $this->SUBSER->DataSource;
            $this->SUBSER->DataSource->SQL = "SELECT * \n" .
"FROM SUBSER {SQL_Where} {SQL_OrderBy}";
            list($this->SUBSER->BoundColumn, $this->SUBSER->TextColumn, $this->SUBSER->DBFormat) = array("SUBSER", "DESSUB", "");
            $this->MESREF = new clsControl(ccsTextBox, "MESREF", "M�s / Ano de Refer�ncia", ccsText, "", CCGetRequestParam("MESREF", $Method, NULL), $this);
            $this->Hidden1 = new clsControl(ccsLabel, "Hidden1", "Hidden1", ccsText, "", CCGetRequestParam("Hidden1", $Method, NULL), $this);
            $this->Hidden2 = new clsControl(ccsHidden, "Hidden2", "Hidden2", ccsInteger, "", CCGetRequestParam("Hidden2", $Method, NULL), $this);
            $this->EQUPBH = new clsControl(ccsTextBox, "EQUPBH", "Equival�ncia", ccsFloat, "", CCGetRequestParam("EQUPBH", $Method, NULL), $this);
            $this->EQUPBH->Required = true;
            $this->VALEQU = new clsControl(ccsTextBox, "VALEQU", "Valor em R$", ccsFloat, "", CCGetRequestParam("VALEQU", $Method, NULL), $this);
            $this->VALEQU->Required = true;
            $this->Button_Insert = new clsButton("Button_Insert", $Method, $this);
            $this->Button_Update = new clsButton("Button_Update", $Method, $this);
            $this->Button_Delete = new clsButton("Button_Delete", $Method, $this);
            $this->Button_Cancel = new clsButton("Button_Cancel", $Method, $this);
        }
    }
//End Class_Initialize Event

//Initialize Method @4-45E5D5EF
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->DataSource->Parameters["urlMESREF"] = CCGetFromGet("MESREF", NULL);
        $this->DataSource->Parameters["urlSUBSER"] = CCGetFromGet("SUBSER", NULL);
    }
//End Initialize Method

//Validate Method @4-04148D44
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->CODINC->Validate() && $Validation);
        $Validation = ($this->DATINC->Validate() && $Validation);
        $Validation = ($this->SUBSER->Validate() && $Validation);
        $Validation = ($this->MESREF->Validate() && $Validation);
        $Validation = ($this->Hidden2->Validate() && $Validation);
        $Validation = ($this->EQUPBH->Validate() && $Validation);
        $Validation = ($this->VALEQU->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->CODINC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DATINC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->SUBSER->Errors->Count() == 0);
        $Validation =  $Validation && ($this->MESREF->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Hidden2->Errors->Count() == 0);
        $Validation =  $Validation && ($this->EQUPBH->Errors->Count() == 0);
        $Validation =  $Validation && ($this->VALEQU->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @4-8379CCE4
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->CODINC->Errors->Count());
        $errors = ($errors || $this->DATINC->Errors->Count());
        $errors = ($errors || $this->SUBSER->Errors->Count());
        $errors = ($errors || $this->MESREF->Errors->Count());
        $errors = ($errors || $this->Hidden1->Errors->Count());
        $errors = ($errors || $this->Hidden2->Errors->Count());
        $errors = ($errors || $this->EQUPBH->Errors->Count());
        $errors = ($errors || $this->VALEQU->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @4-1CEB966F
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->DataSource->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->DataSource->AllParametersSet;
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = $this->EditMode ? "Button_Update" : "Button_Insert";
            if($this->Button_Insert->Pressed) {
                $this->PressedButton = "Button_Insert";
            } else if($this->Button_Update->Pressed) {
                $this->PressedButton = "Button_Update";
            } else if($this->Button_Delete->Pressed) {
                $this->PressedButton = "Button_Delete";
            } else if($this->Button_Cancel->Pressed) {
                $this->PressedButton = "Button_Cancel";
            }
        }
        $Redirect = "CadTabPreco.php" . "?" . CCGetQueryString("QueryString", array("ccsForm", "SUBSER", "MESREF"));
        if($this->PressedButton == "Button_Delete") {
            $Redirect = "CadTabPreco.php" . "?" . CCGetQueryString("QueryString", array("ccsForm", "SUBSER", "MESREF"));
            if(!CCGetEvent($this->Button_Delete->CCSEvents, "OnClick", $this->Button_Delete) || !$this->DeleteRow()) {
                $Redirect = "";
            }
        } else if($this->PressedButton == "Button_Cancel") {
            $Redirect = "CadTabPreco.php" . "?" . CCGetQueryString("QueryString", array("ccsForm", "SUBSER", "MESREF"));
            if(!CCGetEvent($this->Button_Cancel->CCSEvents, "OnClick", $this->Button_Cancel)) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button_Insert") {
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick", $this->Button_Insert) || !$this->InsertRow()) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "Button_Update") {
                $Redirect = "CadTabPreco.php" . "?" . CCGetQueryString("QueryString", array("ccsForm", "SUBSER", "MESREF"));
                if(!CCGetEvent($this->Button_Update->CCSEvents, "OnClick", $this->Button_Update) || !$this->UpdateRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
        if ($Redirect)
            $this->DataSource->close();
    }
//End Operation Method

//InsertRow Method @4-A79E749E
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert", $this);
        if(!$this->InsertAllowed) return false;
        $this->DataSource->CODINC->SetValue($this->CODINC->GetValue(true));
        $this->DataSource->DATINC->SetValue($this->DATINC->GetValue(true));
        $this->DataSource->SUBSER->SetValue($this->SUBSER->GetValue(true));
        $this->DataSource->MESREF->SetValue($this->MESREF->GetValue(true));
        $this->DataSource->Hidden1->SetValue($this->Hidden1->GetValue(true));
        $this->DataSource->Hidden2->SetValue($this->Hidden2->GetValue(true));
        $this->DataSource->EQUPBH->SetValue($this->EQUPBH->GetValue(true));
        $this->DataSource->VALEQU->SetValue($this->VALEQU->GetValue(true));
        $this->DataSource->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert", $this);
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//UpdateRow Method @4-54D9F4AB
    function UpdateRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeUpdate", $this);
        if(!$this->UpdateAllowed) return false;
        $this->DataSource->CODINC->SetValue($this->CODINC->GetValue(true));
        $this->DataSource->DATINC->SetValue($this->DATINC->GetValue(true));
        $this->DataSource->SUBSER->SetValue($this->SUBSER->GetValue(true));
        $this->DataSource->MESREF->SetValue($this->MESREF->GetValue(true));
        $this->DataSource->Hidden1->SetValue($this->Hidden1->GetValue(true));
        $this->DataSource->Hidden2->SetValue($this->Hidden2->GetValue(true));
        $this->DataSource->EQUPBH->SetValue($this->EQUPBH->GetValue(true));
        $this->DataSource->VALEQU->SetValue($this->VALEQU->GetValue(true));
        $this->DataSource->Update();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterUpdate", $this);
        return (!$this->CheckErrors());
    }
//End UpdateRow Method

//DeleteRow Method @4-299D98C3
    function DeleteRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDelete", $this);
        if(!$this->DeleteAllowed) return false;
        $this->DataSource->Delete();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDelete", $this);
        return (!$this->CheckErrors());
    }
//End DeleteRow Method

//Show Method @4-38230BB5
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->SUBSER->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->DataSource->Errors->Count()){
                $this->Errors->AddErrors($this->DataSource->Errors);
                $this->DataSource->Errors->clear();
            }
            $this->DataSource->Open();
            if($this->DataSource->Errors->Count() == 0 && $this->DataSource->next_record()) {
                $this->DataSource->SetValues();
                if(!$this->FormSubmitted){
                    $this->CODINC->SetValue($this->DataSource->CODINC->GetValue());
                    $this->DATINC->SetValue($this->DataSource->DATINC->GetValue());
                    $this->SUBSER->SetValue($this->DataSource->SUBSER->GetValue());
                    $this->MESREF->SetValue($this->DataSource->MESREF->GetValue());
                    $this->EQUPBH->SetValue($this->DataSource->EQUPBH->GetValue());
                    $this->VALEQU->SetValue($this->DataSource->VALEQU->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->CODINC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DATINC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->SUBSER->Errors->ToString());
            $Error = ComposeStrings($Error, $this->MESREF->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Hidden1->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Hidden2->Errors->ToString());
            $Error = ComposeStrings($Error, $this->EQUPBH->Errors->ToString());
            $Error = ComposeStrings($Error, $this->VALEQU->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataSource->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Insert->Visible = !$this->EditMode && $this->InsertAllowed;
        $this->Button_Update->Visible = $this->EditMode && $this->UpdateAllowed;
        $this->Button_Delete->Visible = $this->EditMode && $this->DeleteAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->CODINC->Show();
        $this->DATINC->Show();
        $this->SUBSER->Show();
        $this->MESREF->Show();
        $this->Hidden1->Show();
        $this->Hidden2->Show();
        $this->EQUPBH->Show();
        $this->VALEQU->Show();
        $this->Button_Insert->Show();
        $this->Button_Update->Show();
        $this->Button_Delete->Show();
        $this->Button_Cancel->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

} //End EQUSER Class @4-FCB6E20C

class clsEQUSERDataSource extends clsDBFaturar {  //EQUSERDataSource Class @4-039FACB8

//DataSource Variables @4-2F2D2723
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $InsertParameters;
    public $UpdateParameters;
    public $DeleteParameters;
    public $wp;
    public $AllParametersSet;

    public $InsertFields = array();
    public $UpdateFields = array();

    // Datasource fields
    public $CODINC;
    public $DATINC;
    public $SUBSER;
    public $MESREF;
    public $Hidden1;
    public $Hidden2;
    public $EQUPBH;
    public $VALEQU;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-7E8AA29C
    function clsEQUSERDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Record EQUSER/Error";
        $this->Initialize();
        $this->CODINC = new clsField("CODINC", ccsText, "");
        $this->DATINC = new clsField("DATINC", ccsText, "");
        $this->SUBSER = new clsField("SUBSER", ccsText, "");
        $this->MESREF = new clsField("MESREF", ccsText, "");
        $this->Hidden1 = new clsField("Hidden1", ccsText, "");
        $this->Hidden2 = new clsField("Hidden2", ccsInteger, "");
        $this->EQUPBH = new clsField("EQUPBH", ccsFloat, "");
        $this->VALEQU = new clsField("VALEQU", ccsFloat, "");

        $this->InsertFields["CODALT"] = array("Name" => "CODALT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["DATALT"] = array("Name" => "DATALT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["SUBSER"] = array("Name" => "SUBSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["MESREF"] = array("Name" => "MESREF", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->InsertFields["EQUPBH"] = array("Name" => "EQUPBH", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->InsertFields["VALEQU"] = array("Name" => "VALEQU", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->UpdateFields["CODALT"] = array("Name" => "CODALT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["DATALT"] = array("Name" => "DATALT", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["SUBSER"] = array("Name" => "SUBSER", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["MESREF"] = array("Name" => "MESREF", "Value" => "", "DataType" => ccsText, "OmitIfEmpty" => 1);
        $this->UpdateFields["EQUPBH"] = array("Name" => "EQUPBH", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->UpdateFields["VALEQU"] = array("Name" => "VALEQU", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
    }
//End DataSourceClass_Initialize Event

//Prepare Method @4-BFB89D66
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlMESREF", ccsText, "", "", $this->Parameters["urlMESREF"], "", false);
        $this->wp->AddParameter("2", "urlSUBSER", ccsText, "", "", $this->Parameters["urlSUBSER"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "MESREF", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opEqual, "SUBSER", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsText),false);
        $this->Where = $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]);
    }
//End Prepare Method

//Open Method @4-6F4597FC
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->SQL = "SELECT * \n\n" .
        "FROM EQUSER {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @4-14399B57
    function SetValues()
    {
        $this->CODINC->SetDBValue($this->f("CODALT"));
        $this->DATINC->SetDBValue($this->f("DATALT"));
        $this->SUBSER->SetDBValue($this->f("SUBSER"));
        $this->MESREF->SetDBValue($this->f("MESREF"));
        $this->EQUPBH->SetDBValue(trim($this->f("EQUPBH")));
        $this->VALEQU->SetDBValue(trim($this->f("VALEQU")));
    }
//End SetValues Method

//Insert Method @4-332FCABC
    function Insert()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert", $this->Parent);
        $this->InsertFields["CODALT"]["Value"] = $this->CODINC->GetDBValue(true);
        $this->InsertFields["DATALT"]["Value"] = $this->DATINC->GetDBValue(true);
        $this->InsertFields["SUBSER"]["Value"] = $this->SUBSER->GetDBValue(true);
        $this->InsertFields["MESREF"]["Value"] = $this->MESREF->GetDBValue(true);
        $this->InsertFields["EQUPBH"]["Value"] = $this->EQUPBH->GetDBValue(true);
        $this->InsertFields["VALEQU"]["Value"] = $this->VALEQU->GetDBValue(true);
        $this->SQL = CCBuildInsert("EQUSER", $this->InsertFields, $this);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert", $this->Parent);
        }
    }
//End Insert Method

//Update Method @4-6E482DB6
    function Update()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildUpdate", $this->Parent);
        $this->UpdateFields["CODALT"]["Value"] = $this->CODINC->GetDBValue(true);
        $this->UpdateFields["DATALT"]["Value"] = $this->DATINC->GetDBValue(true);
        $this->UpdateFields["SUBSER"]["Value"] = $this->SUBSER->GetDBValue(true);
        $this->UpdateFields["MESREF"]["Value"] = $this->MESREF->GetDBValue(true);
        $this->UpdateFields["EQUPBH"]["Value"] = $this->EQUPBH->GetDBValue(true);
        $this->UpdateFields["VALEQU"]["Value"] = $this->VALEQU->GetDBValue(true);
        $this->SQL = CCBuildUpdate("EQUSER", $this->UpdateFields, $this);
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteUpdate", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteUpdate", $this->Parent);
        }
    }
//End Update Method

//Delete Method @4-28B0A0EC
    function Delete()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildDelete", $this->Parent);
        $this->SQL = "DELETE FROM EQUSER";
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteDelete", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteDelete", $this->Parent);
        }
    }
//End Delete Method

} //End EQUSERDataSource Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-BBBB100E
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "ManutTabPrecosAlter.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-7FACF37D
CCSecurityRedirect("1;3", "");
//End Authenticate User

//Include events file @1-9F4711E1
include("./ManutTabPrecosAlter_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-D59C4471
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$EQUSER = new clsRecordEQUSER("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->EQUSER = & $EQUSER;
$MainPage->rodape = & $rodape;
$EQUSER->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-339F08D5
$cabec->Operations();
$EQUSER->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-32529B6C
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($EQUSER);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-3CF8B5E9
$cabec->Show();
$EQUSER->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-3615035D
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($EQUSER);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
