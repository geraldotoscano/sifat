<?php
//BindEvents Method @1-4C3BDCC2
function BindEvents()
{
    global $EQUSER;
    global $CCSEvents;
    $EQUSER->SUBSER->ds->CCSEvents["BeforeBuildSelect"] = "EQUSER_SUBSER_ds_BeforeBuildSelect";
    $EQUSER->MESREF->CCSEvents["BeforeShow"] = "EQUSER_MESREF_BeforeShow";
    $EQUSER->Hidden1->CCSEvents["BeforeShow"] = "EQUSER_Hidden1_BeforeShow";
    $EQUSER->Button_Insert->CCSEvents["OnClick"] = "EQUSER_Button_Insert_OnClick";
    $EQUSER->CCSEvents["BeforeInsert"] = "EQUSER_BeforeInsert";
    $EQUSER->CCSEvents["BeforeShow"] = "EQUSER_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//EQUSER_SUBSER_ds_BeforeBuildSelect @11-92111190
function EQUSER_SUBSER_ds_BeforeBuildSelect(& $sender)
{
    $EQUSER_SUBSER_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $EQUSER; //Compatibility
//End EQUSER_SUBSER_ds_BeforeBuildSelect

//Custom Code @28-2A29BDB7
// -------------------------
    // Write your own code here.
    //$EQUSER->SUBSER->DataSource->SQL = "SELECT dessub,subser FROM subser MINUS SELECT s.dessub,e.subser FROM subser s,equser e WHERE s.subser=e.subser AND e.mesref ='".$EQUSER->MESREF->Value."'";
	//$EQUSER->SUBSER->DataSource->SQL = "SELECT * FROM subser s,equser e WHERE s.subser=e.subser AND e.subser is null";
	//$EQUSER->SUBSER->DataSource->SQL = "SELECT * FROM subser s,equser e WHERE s.subser=e.subser";
	//$EQUSER->SUBSER->DataSource->SQL = "SELECT * FROM subser s,equser e WHERE s.subser=e.subser AND (e.subser not in (select e.subser from equser e where e.mesref = '08/1996'))";
	$EQUSER->SUBSER->DataSource->Order = "dessub ASC";
// -------------------------
//End Custom Code

//Close EQUSER_SUBSER_ds_BeforeBuildSelect @11-E49C0511
    return $EQUSER_SUBSER_ds_BeforeBuildSelect;
}
//End Close EQUSER_SUBSER_ds_BeforeBuildSelect

//EQUSER_MESREF_BeforeShow @12-C4B874EA
function EQUSER_MESREF_BeforeShow(& $sender)
{
    $EQUSER_MESREF_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $EQUSER; //Compatibility
//End EQUSER_MESREF_BeforeShow

//Custom Code @27-2A29BDB7
// -------------------------
    // Write your own code here.
   if ($EQUSER->Button_Insert->Visible)
   {
      global $DBfaturar;
      $Page = CCGetParentPage($sender);
      $ccs_result = CCDLookUp("max(to_number(substr(mesref,4,4)))", "tabpbh", "", $Page->Connections["Faturar"]);
      $ccs_result = substr($ccs_result."",0,4);
	  //echo $ccs_result;

      $Tabela = new clsDBfaturar();
      $Tabela->query("SELECT mesref FROM tabpbh where substr(mesref,4,4)='".$ccs_result."' order by mesref desc");
      $Tabela->next_record();
      {
         $EQUSER->MESREF->SetValue($Tabela->f("mesref"));
      }

   }
// -------------------------
//End Custom Code

//Close EQUSER_MESREF_BeforeShow @12-3A8598E4
    return $EQUSER_MESREF_BeforeShow;
}
//End Close EQUSER_MESREF_BeforeShow

//EQUSER_Hidden1_BeforeShow @31-D88CE210
function EQUSER_Hidden1_BeforeShow(& $sender)
{
    $EQUSER_Hidden1_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $EQUSER; //Compatibility
//End EQUSER_Hidden1_BeforeShow

//Custom Code @32-2A29BDB7
// -------------------------
    // Write your own code here.
// -------------------------
//End Custom Code

//C-2A29BDB7
// -------------------------
    // Write your own code here.
      global $DBfaturar;
      $Page = CCGetParentPage($sender);
      $ccs_result = CCDLookUp("max(to_number(substr(mesref,4,4)))", "tabpbh", "", $Page->Connections["Faturar"]);
      $ccs_result = substr($ccs_result."",0,4);
	  //echo $ccs_result;

      $Tabela = new clsDBfaturar();
      $Tabela->query("SELECT valpbh,mesref FROM tabpbh where substr(mesref,4,4)='".$ccs_result."' order by mesref desc");
      $Tabela->next_record();
      {
         $ccs_result = $Tabela->f("valpbh");
         $mmesref = $Tabela->f("mesref");
      }
      $EQUSER->Hidden1->SetValue("R$ ".$ccs_result." em ".$mmesref);
      $EQUSER->Hidden2->SetValue($ccs_result);
// -------------------------
//End Custom Code

//Close EQUSER_Hidden1_BeforeShow @31-21B02ADA
    return $EQUSER_Hidden1_BeforeShow;
}
//End Close EQUSER_Hidden1_BeforeShow

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	echo $EQUSER->SUBSER->DataSource->SQL;
//DEL  // -------------------------

//DEL  // -------------------------
//DEL      // Write your own code here.$Tasks->DataSource->SQL = "SELECT * FROM Tasks";
//DEL  // -------------------------

//EQUSER_Button_Insert_OnClick @5-54DA9B56
function EQUSER_Button_Insert_OnClick(& $sender)
{
    $EQUSER_Button_Insert_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $EQUSER; //Compatibility
	global $Redirect;
//End EQUSER_Button_Insert_OnClick

//DLookup @29-6A65CEC1
    global $DBfaturar;
    $Page = CCGetParentPage($sender);
	$mValPHB = $EQUSER->Hidden2->GetValue();
	if (empty($mValPHB))
	{
	   	$EQUSER->Errors->addError("Valor da UFIR do mes de refer�ncia n�o cadastrado.");
	   	$EQUSER_Button_Insert_OnClick = false;
	}
	else
	{
    	$ccs_result = CCDLookUp("subser", "equser", "subser ='" .$EQUSER->SUBSER->Value."' and mesref ='".$EQUSER->MESREF->Value."'", $Page->Connections["Faturar"]);
		if ($ccs_result != "")
		{
	   		//$Redirect = "ManutTabPrecosErro.php";
	   		$EQUSER->Errors->addError("�tem de tabela de pre�os j� cadastrado.");
	   		$EQUSER_Button_Insert_OnClick = false;
		}
		else
		{
	   		$EQUSER->InsertRow();
		}
	}
//End DLookup

//Close EQUSER_Button_Insert_OnClick @5-8970F843
    return $EQUSER_Button_Insert_OnClick;
}
//End Close EQUSER_Button_Insert_OnClick

//EQUSER_BeforeInsert @4-1B7E2CDF
function EQUSER_BeforeInsert(& $sender)
{
    $EQUSER_BeforeInsert = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $EQUSER; //Compatibility
//End EQUSER_BeforeInsert

//Custom Code @19-2A29BDB7
// -------------------------
    // Write your own code here.
   $EQUSER->CODINC->Value = CCGetSession("IDUsuario");
   $EQUSER->DATINC->Value = CCGetSession("DataSist");
// -------------------------
//End Custom Code

//Close EQUSER_BeforeInsert @4-5DD4C4AC
    return $EQUSER_BeforeInsert;
}
//End Close EQUSER_BeforeInsert

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL      global $DBfaturar;
//DEL      $Page = CCGetParentPage($sender);
//DEL      $mDataAtu  = CCDLookUp("date()", "", "", $Page->Connections["Faturar"]);
//DEL     $EQUSER->DATALT->Value = substr($mDataAtu,-2).'/'.substr($mDataAtu,5,2).'/'.substr($mDataAtu,0,4);
//DEL     $EQUSER->CODALT->Value = CCGetUserLogin();
//DEL  // -------------------------

//EQUSER_BeforeShow @4-48869E02
function EQUSER_BeforeShow(& $sender)
{
    $EQUSER_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $EQUSER; //Compatibility
//End EQUSER_BeforeShow

//Custom Code @21-2A29BDB7
// -------------------------
    // Write your own code here.
    global $DBfaturar;
    $Page = CCGetParentPage($sender);
	if ($EQUSER->Button_Delete->Visible)
	{
       //$EQUSER->SUBSER->e
       $mCodFat = CCDLookUp("codfat", "movfat", "subser='".$EQUSER->SUBSER->Value."'", $Page->Connections["Faturar"]);
       $ccs_result = CCDLookUp("codfat", "cadfat", "codfat='".$mCodFat."'", $Page->Connections["Faturar"]);
       //$Component->SetValue($ccs_result);
	   if ($ccs_result != "")
	   {
	      $EQUSER->Button_Delete->Visible = false;
	   }
	}
// -------------------------
//End Custom Code

//Close EQUSER_BeforeShow @4-ACC9F869
    return $EQUSER_BeforeShow;
}
//End Close EQUSER_BeforeShow

//Page_BeforeShow @1-3D93533A
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutTabPrecos; //Compatibility
//End Page_BeforeShow

//Custom Code @40-2A29BDB7
// -------------------------

        include("controle_acesso.php");
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(16);
        controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
