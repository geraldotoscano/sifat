<?php
//BindEvents Method @1-7CFFCFB6
function BindEvents()
{
    global $TABUNI;
    global $CCSEvents;
    $TABUNI->CODUNI->CCSEvents["BeforeShow"] = "TABUNI_CODUNI_BeforeShow";
    $TABUNI->Button_Insert->CCSEvents["BeforeShow"] = "TABUNI_Button_Insert_BeforeShow";
    $TABUNI->CCSEvents["BeforeShow"] = "TABUNI_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//TABUNI_CODUNI_BeforeShow @11-3A7B6FA8
function TABUNI_CODUNI_BeforeShow(& $sender)
{
    $TABUNI_CODUNI_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABUNI; //Compatibility
//End TABUNI_CODUNI_BeforeShow
    //global $mCod_Uni;

//Custom Code @15-2A29BDB7
// -------------------------
    // Write your own code here.
	if ($TABUNI->Button_Insert->Visible)
	{
	   TABUNI_Button_Insert_BeforeShow(& $sender);
	}
// -------------------------
//End Custom Code

//Close TABUNI_CODUNI_BeforeShow @11-DDF5B63E
    return $TABUNI_CODUNI_BeforeShow;
}
//End Close TABUNI_CODUNI_BeforeShow

//DEL  // -------------------------
//DEL     if ( !caracterInvado($Component->GetValue()) )
//DEL     {
//DEL        $TABUNI->Errors->addError("Caracter inv�lido no campo ".$Component->Caption.".");
//DEL     }
//DEL  // -------------------------

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	global $ccs_result;
//DEL  	echo $ccs_result;
//DEL  	if ($ccs_result!='')
//DEL  	{
//DEL         $TABUNI->CODUNI->SetValue($ccs_result);
//DEL  	}
//DEL  // -------------------------

//TABUNI_Button_Insert_BeforeShow @5-607CFB1A
function TABUNI_Button_Insert_BeforeShow(& $sender)
{
    $TABUNI_Button_Insert_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABUNI; //Compatibility
//End TABUNI_Button_Insert_BeforeShow

//DLookup @14-09012EFE
    global $DBfaturar;
    //$Page = CCGetParentPage($sender);
    //$mCod_Uni = CCDLookUp("max(val(coduni))+1", "tabuni", "", $DBfaturar);
    //$mCod_Uni = intval($mCod_Uni);
    //$TABUNI->CODUNI->Value = $mCod_Uni;
	//$TABUNI->CODUNI->SetValue($mCod_Uni);
	//echo $mCod_Uni;
//End DLookup

//Close TABUNI_Button_Insert_BeforeShow @5-E2A899FA
    return $TABUNI_Button_Insert_BeforeShow;
}
//End Close TABUNI_Button_Insert_BeforeShow

//TABUNI_BeforeShow @4-646DD1C4
function TABUNI_BeforeShow(& $sender)
{
    $TABUNI_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $TABUNI; //Compatibility
//End TABUNI_BeforeShow

//DLookup @18-8D96F9F5
    global $DBfaturar;
    $Page = CCGetParentPage($sender);
	if ($TABUNI->Button_Delete->Visible)
	{
       $ccs_result = CCDLookUp("coduni", "cadcli", "coduni='".$TABUNI->CODUNI->Value."'", $Page->Connections["Faturar"]);
       //$Component->SetValue($ccs_result);
	   if ($ccs_result != "")
	   {
	      $TABUNI->Button_Delete->Visible = false;
	   }
	   else
	   {
          $ccs_result = CCDLookUp("coduni", "tabcol", "coduni='".$TABUNI->CODUNI->Value."'", $Page->Connections["Faturar"]);
	      if ($ccs_result != "")
	      {
	         $TABUNI->Button_Delete->Visible = false;
	      }
	   }
	}
	if ($TABUNI->Button_Insert->Visible)
	{
		$ccs_result = CCDLookUp("(max(to_number(coduni)))+1", "tabuni", "", $Page->Connections["Faturar"]);
		if (is_null($ccs_result))
		{
			$ccs_result = 1;
		}
		$TABUNI->CODUNI->Value = $ccs_result;
	}
//End DLookup

//Close TABUNI_BeforeShow @4-D5E6328E
    return $TABUNI_BeforeShow;
}
//End Close TABUNI_BeforeShow

//Page_BeforeShow @1-6428868E
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $ManutTabUni; //Compatibility
//End Page_BeforeShow

//Custom Code @24-2A29BDB7
// -------------------------
        include("controle_acesso.php");
        $Tabela = new clsDBfaturar();
        $perfil=CCGetSession("IDPerfil");
		$permissao_requerida=array(2);
        controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
