<?php
class clsConsistencia { //consistencia class @1-1B67F9CF

//Variables @1-9721D5A2
	var $cCampo;
	var $cTipo;
//End Variables

/*
		Na classe principal, lembrar que:
		  * Todo campo tem :
		    * Tipo (Se num�rico, String(Se somente letras,somente n�meros ou ambos), L�gico, Data)
			* Tamanho m�ximo (Quantidade m�xima de bytes informados pelo usu�rio)
			* Tamanho m�nimo (Quantidade m�nima de bytes informados pelo usu�rio)
			* Decimais (Quantidade m�xima de casas decimais)
			* M�scara (Formata��o de como os dados ser�o digitados. Ex. 30.555.200 - CEP)
			* Valor m�ximo (Para campos num�ricos, o maior valor permitido)
			* Valor m�nimo (Para campos num�ricos, o menor valor permitido)
*/
    /*
	   Construtor da classe clsConsistencia.
	   Nome       : clsConsistencia
	   Assinatura : clsconsistencia($iCampo)
	   Par�metro 1: $iCampo
	   Descri��o  : $iCampo � o conte�do de um dado digitado pelo usu�rio.
	   Par�metro 2: $iTipo 
	   Descri��o  : Tipo do campo que pode ser um dos seguintes:
	                a) N = Num�rico
					b) S = String - Somente letras
					c) s = String - Quaisque caracteres
					d) n = String - Somente n�meros
					e) L = L�gico
					f) D = Data
					g) H = Hora
					i) d = Data e Hora
	*/
    function clsConsistencia($iCampo,$iTipo)
    {
	    $this->cCampo = $iCampo;
		$this->cTipo = $iTipo;
    }
//  Fun��o para testar o tipo do campo
   function situacao()
   {
    if ($this->testarVazio())
	{
	   print("Cliente tem que ter um c�digo");
	   return false;
	   //$CADCLI->Errors->  //addError("Cliente tem que ter um c�digo");
	}
	else
	{
	   if ($this->testarNumerico())
	   {
	      print("C�digo do cliente deve ser composto somente por n�meros.");
		  return false;
	   }
	   else
	   {
	      print("Cliente cadastrado");
		  return true;
	      //$CADCLI->InsertRow();
       }
	}
      
   }
// Fim da fun��o testarTipo
// Inicio da fun��o testarVazio
   function testarVazio()
   {
      //print ('*'.$this->cCampo);
      if (empty($this->cCampo))
	  {
	     return true;
	  }
	  else
	  {
	     return false;
	  }
   }
// Fim da fun��o testarVazio
//Inicio da fun��o testarNumerico
   function testarNumerico()
   {
      print ('*'.$this->cCampo."*");
	  if (is_numeric($this->cCampo) && is_int($this->cCampo))
	  {
	     return true;
	  }
	  else
	  {
	     return false;
	  }
   }
//Fim da fun��o testarNumerico
} //End consistencia Class @1-FCB6E20C


?>
