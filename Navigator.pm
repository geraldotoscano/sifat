#Navigator Class @0-E5C5459B

package Navigator;
use Common;

sub new {
  my ($class, $ComponentName, $NavigatorName, $FileName, $NumberPages, $NavigatorType) = @_;
  my $self                = {};
  $self->{ds}             = "";
  $self->{CCSEventResult} = undef;
  $self->{CCSEvents}      = {};
  $self->{TargetName}     = $ComponentName;
  $self->{NavigatorName}  = $NavigatorName;
  $self->{FileName}       = $FileName;
  $self->{Visible}        = 1;
  $self->{NumberPages}    = $NumberPages;
  $self->{NavigatorType}  = $NavigatorType;
  bless $self, $class;
  return $self;
}

sub Show {
  my $self = shift;
  
  $self->{EventResult} = CCGetEvent($self->{CCSEvents}, "BeforeShow" );
  my $NavigatorBlock = "Navigator " . $self->{NavigatorName};
  return if(!$self->{Visible});

  my $FirstOnPath    = $NavigatorBlock . "/First_On";
  my $FirstOffPath   = $NavigatorBlock . "/First_Off";
  my $PrevOnPath     = $NavigatorBlock . "/Prev_On";
  my $PrevOffPath    = $NavigatorBlock . "/Prev_Off";
  my $NextOnPath     = $NavigatorBlock . "/Next_On";
  my $NextOffPath    = $NavigatorBlock . "/Next_Off";
  my $LastOnPath     = $NavigatorBlock . "/Last_On";
  my $LastOffPath    = $NavigatorBlock . "/Last_Off";
  my $PageOnPath     = $NavigatorBlock . "/Pages/Page_On";
  my $PageOffPath    = $NavigatorBlock . "/Pages/Page_Off";
  my $PagesPath      = $NavigatorBlock . "/Pages";

  $self->{PageNumber} = 1 if ($self->{PageNumber} < 1);
  my $LastPage  = $self->{TotalPages};
  my $BeginPage = "";
  my $EndPage   = "";

   if ( $LastPage == 0 ) { $LastPage = 1 };

  my $QueryString = CCGetQueryString("QueryString", ($self->{TargetName} . "Page", "ccsForm") );

  # Parse First and Prev blocks
  if ( $self->{PageNumber} <= 1 ) {
    if ( $Tpl->blockexists($FirstOffPath) ) { $Tpl->parse($FirstOffPath, "false") }
    if ( $Tpl->blockexists($PrevOffPath) ) { $Tpl->parse($PrevOffPath, "false") }
  } else {
    if ( $Tpl->blockexists($FirstOnPath) ) {
      $Tpl->setvar("First_URL", $self->{FileName} . "?" . CCAddParam($QueryString, $self->{TargetName} . "Page", "1"));
      $Tpl->parse($FirstOnPath, "false");
    }
    if ( $Tpl->blockexists( $PrevOnPath ) ) {
        $Tpl->setvar("Prev_URL", $self->{FileName} . "?" . CCAddParam($QueryString, $self->{TargetName} . "Page", ($self->{PageNumber} - 1)));
        $Tpl->parse($PrevOnPath, "false");
    }
  }

  my $PageOnExist  = $Tpl->blockexists($PageOnPath);
  my $PageOffExist = $Tpl->blockexists($PageOffPath);
    
  if ( $self->{NavigatorType} == $tpCentered && ($PageOnExist || $PageOffExist) ) {
      $BeginPage = $self->{PageNumber} - int(($self->{NumberPages} - 1) / 2);
      if ( $BeginPage < 1 ) { $BeginPage = 1 };
      $EndPage = $BeginPage + $self->{NumberPages} - 1; ###
      if ( $EndPage > $LastPage ) {
        $BeginPage = $BeginPage - $EndPage + $LastPage;
        if ( $BeginPage < 1 ) { $BeginPage = 1 };
        $EndPage = $LastPage;
      }
      for ( my $j = $BeginPage; $j <= $EndPage; $j++ ) {
        if ( ( $j == $self->{PageNumber} ) && $PageOffExist ) {
          $Tpl->setvar("Page_Number", $j);
          $Tpl->parseto($PageOffPath, "true", $PagesPath);
        } elsif ( $PageOnExist ) {
          $Tpl->setvar("Page_Number", $j);
          $Tpl->setvar("Page_URL", $self->{FileName} . "?" . CCAddParam($QueryString, $self->{TargetName} . "Page", $j));
          $Tpl->parseto($PageOnPath, "true", $PagesPath);
        }
      }
    } elsif ( ( $self->{NavigatorType} == $tpMoving ) && ( $PageOnExist || $PageOffExist ) ) { ###
      
      my $initial = $self->{PageNumber} / $self->{NumberPages};
      my $rounded = sprintf("%.0f", $self->{PageNumber} / $self->{NumberPages});
      my $GroupNumber = $rounded < $initial ? $rounded + 1 : $rounded;
     
      $BeginPage = 1 + $self->{NumberPages} * ($GroupNumber - 1);
      $EndPage = $self->{NumberPages} * $GroupNumber;
      if ( $BeginPage < 1 ) { $BeginPage = 1 };
      if ( $EndPage > $LastPage ) { $EndPage = $LastPage };
      if ( $BeginPage > 1 ) {
        $Tpl->setvar("Page_Number", "&lt;" . ($BeginPage - 1));
        $Tpl->setvar("Page_URL", $self->{FileName} . "?" . CCAddParam($QueryString, $self->{TargetName} . "Page", ($BeginPage - 1)));
        $Tpl->parseto($PageOnPath, "true", $PagesPath);
      }
      for (my $j = $BeginPage; $j <= $EndPage; $j++ ) {
        if ( ( $j  == $self->{PageNumber} ) && $PageOffExist ) {
          $Tpl->setvar("Page_Number", $j);
          $Tpl->parseto($PageOffPath, "true", $PagesPath);
        } elsif ($PageOnExist) {
          $Tpl->setvar("Page_Number", $j);
          $Tpl->setvar("Page_URL", $self->{FileName} . "?" . CCAddParam($QueryString, $self->{TargetName} . "Page", $j));
          $Tpl->parseto($PageOnPath, "true", $PagesPath);
        }
      }
      if ( $EndPage < $LastPage ) {
        $Tpl->setvar("Page_Number", ($EndPage + 1) . "&gt;");
        $Tpl->setvar("Page_URL", $self->{FileName} . "?" . CCAddParam($QueryString, $self->{TargetName} . "Page", $EndPage + 1));
        $Tpl->parseto($PageOnPath, "true", $PagesPath);
      }
    } elsif ( $self->{NavigatorType} == $tpSimple ) {
      # Set Page Number
      $Tpl->setvar("Page_Number", $self->{PageNumber});
      if ( $PageOffExist ) {
        $Tpl->parse($PageOffPath, "false");
        if ( $Tpl->blockexists($PagesPath) ) { $Tpl->parse($PagesPath, "false") };
      }
    }

    # Set Total Pages
    $Tpl->setvar("Total_Pages", $LastPage);

    # Parse Last and Next blocks
    if( $self->{PageNumber} >= $LastPage ) {
      if ( $Tpl->blockexists( $NextOffPath ) ) { $Tpl->parse($NextOffPath, "false") };
      if ( $Tpl->blockexists( $LastOffPath ) ) { $Tpl->parse($LastOffPath, "false") };
    } else {
      if ( $Tpl->blockexists($NextOnPath) ) {
        $Tpl->setvar("Next_URL", $self->{FileName} . "?" . CCAddParam($QueryString, $self->{TargetName} . "Page", ($self->{PageNumber} + 1)));
        $Tpl->parse($NextOnPath, "false");
      }
      if ( $Tpl->blockexists($LastOnPath) ) {
        $Tpl->setvar("Last_URL", $self->{FileName} . "?" . CCAddParam($QueryString, $self->{TargetName} . "Page", $LastPage));
        $Tpl->parse($LastOnPath, "false");
      }
    }
    $Tpl->parse($NavigatorBlock, "false");
}

1;
#End Navigator Class

