<?php
//BindEvents Method @1-0504C3A6
function BindEvents()
{
    global $NewRecord1;
    $NewRecord1->TextBox1->CCSEvents["BeforeShow"] = "NewRecord1_TextBox1_BeforeShow";
    $NewRecord1->FileUpload1->CCSEvents["AfterProcessFile"] = "NewRecord1_FileUpload1_AfterProcessFile";
}
//End BindEvents Method

//NewRecord1_TextBox1_BeforeShow @5-54CF35C1
function NewRecord1_TextBox1_BeforeShow(& $sender)
{
    $NewRecord1_TextBox1_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NewRecord1; //Compatibility
//End NewRecord1_TextBox1_BeforeShow

//Custom Code @10-2A29BDB7
// -------------------------
    $NewRecord1->TextBox1->SetValue($NewRecord1->FileUpload1->Move());
	echo $NewRecord1->FileUpload1->Move();
// -------------------------
//End Custom Code

//Close NewRecord1_TextBox1_BeforeShow @5-74544E0D
    return $NewRecord1_TextBox1_BeforeShow;
}
//End Close NewRecord1_TextBox1_BeforeShow

//NewRecord1_FileUpload1_AfterProcessFile @8-F6CA13EC
function NewRecord1_FileUpload1_AfterProcessFile(& $sender)
{
    $NewRecord1_FileUpload1_AfterProcessFile = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NewRecord1; //Compatibility
//End NewRecord1_FileUpload1_AfterProcessFile

//Custom Code @9-2A29BDB7
// -------------------------
    $NewRecord1->TextBox1->SetValue($NewRecord1->FileUpload1->Move());
	//echo $NewRecord1->FileUpload1->Move();
	echo $NewRecord1->FileUpload1-> GetFileName();
// -------------------------
//End Custom Code

//Close NewRecord1_FileUpload1_AfterProcessFile @8-8EF845D1
    return $NewRecord1_FileUpload1_AfterProcessFile;
}
//End Close NewRecord1_FileUpload1_AfterProcessFile


?>
