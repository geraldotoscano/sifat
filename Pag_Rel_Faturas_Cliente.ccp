<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0" isService="False">
	<Components>
		<IncludePage id="123" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Label id="80" fieldSourceType="DBColumn" dataType="Text" html="False" name="Label1" editable="False">
			<Components/>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="130"/>
					</Actions>
				</Event>
			</Events>
			<Attributes/>
			<Features/>
		</Label>
		<Link id="3" visible="Dynamic" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Report_Print" hrefSource="Pag_Rel_Faturas_Cliente.ccp" wizardTheme="Compact" wizardThemeType="File" wizardDefaultValue="Printable version" wizardUseTemplateBlock="True" wizardBeforeHTML="&lt;p align=&quot;right&quot;&gt;" wizardAfterHTML="&lt;/p&gt;" wizardLinkTarget="_blank" PathID="Report_Print" editable="False">
			<Components/>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Hide-Show Component" actionCategory="General" id="5" action="Hide" conditionType="Parameter" dataType="Text" condition="Equal" parameter1="Print" name1="ViewMode" sourceType1="URL" name2="&quot;Print&quot;" sourceType2="Expression"/>
					</Actions>
				</Event>
			</Events>
			<LinkParameters>
				<LinkParameter id="4" sourceType="Expression" format="yyyy-mm-dd" name="ViewMode" source="&quot;Print&quot;"/>
			</LinkParameters>
			<Attributes/>
			<Features/>
		</Link>
		<Report id="2" secured="False" enablePrint="False" showMode="Web" sourceType="Table" returnValueType="Number" linesPerWebPage="66" connection="Faturar" dataSource="CADFAT" name="CADFAT" pageSizeLimit="100" wizardCaption=" CADFAT " wizardLayoutType="Tabular" activeCollection="TableParameters" linesPerPhysicalPage="66" PathID="CADFAT">
			<Components>
				<Section id="6" visible="True" lines="0" name="Report_Header" wizardSectionType="ReportHeader" PathID="CADFATReport_Header">
					<Components>
						<ReportLabel id="12" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="Report_TotalRecords" function="Count" wizardUseTemplateBlock="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="7" visible="True" lines="1" name="Page_Header" wizardSectionType="PageHeader" wizardAllowSorting="True" PathID="CADFATPage_Header">
					<Components>
						<Sorter id="26" visible="True" name="Sorter_CODFAT" column="CODFAT" wizardCaption="CODFAT" wizardSortingType="SimpleDir" wizardControl="CODFAT" PathID="CADFATPage_HeaderSorter_CODFAT">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="35" visible="True" name="Sorter_VALFAT" column="VALFAT" wizardCaption="VALFAT" wizardSortingType="SimpleDir" wizardControl="VALFAT" PathID="CADFATPage_HeaderSorter_VALFAT">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="29" visible="True" name="Sorter_MESREF" column="MESREF" wizardCaption="MESREF" wizardSortingType="SimpleDir" wizardControl="MESREF" PathID="CADFATPage_HeaderSorter_MESREF">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="32" visible="True" name="Sorter_DATVNC" column="DATVNC" wizardCaption="DATVNC" wizardSortingType="SimpleDir" wizardControl="DATVNC" PathID="CADFATPage_HeaderSorter_DATVNC">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Hidden id="82" fieldSourceType="DBColumn" dataType="Text" name="HINSS_MIM" visible="Yes" PathID="CADFATPage_HeaderHINSS_MIM">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Declare Variable" actionCategory="General" id="128" name="mMim" type="Text" initialValue="CCGetSession(&quot;mINSS_MIM&quot;)"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</Hidden>
						<Sorter id="62" visible="True" name="Sorter1" wizardSortingType="SimpleDir" wizardCaption="ISSQN" column="ISSQN" PathID="CADFATPage_HeaderSorter1">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="37" visible="True" name="Sorter_RET_INSS" column="RET_INSS" wizardCaption="RET INSS" wizardSortingType="SimpleDir" wizardControl="RET_INSS" PathID="CADFATPage_HeaderSorter_RET_INSS">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="39" visible="True" name="Sorter_DATPGT" column="DATPGT" wizardCaption="DATPGT" wizardSortingType="SimpleDir" wizardControl="DATPGT" PathID="CADFATPage_HeaderSorter_DATPGT">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="42" visible="True" name="Sorter_VALPGT" column="VALPGT" wizardCaption="VALPGT" wizardSortingType="SimpleDir" wizardControl="VALPGT" PathID="CADFATPage_HeaderSorter_VALPGT">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="8" visible="True" lines="1" name="Detail" PathID="CADFATDetail">
					<Components>
						<ReportLabel id="28" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="CODFAT" fieldSource="CODFAT" wizardCaption="CODFAT" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="36" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="VALFAT" fieldSource="VALFAT" wizardCaption="VALFAT" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" format="#,##0.00">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="31" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="MESREF" fieldSource="MESREF" wizardCaption="MESREF" wizardSize="7" wizardMaxLength="7" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="50" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="PGTOELET" wizardCaption="PGTOELET" wizardSize="1" wizardMaxLength="1" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="51"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="34" fieldSourceType="DBColumn" dataType="Date" html="False" hideDuplicates="False" resetAt="Report" name="DATVNC" fieldSource="DATVNC" wizardCaption="DATVNC" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" format="dd/mm/yyyy">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="63"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Hidden id="94" fieldSourceType="DBColumn" dataType="Integer" name="Hidden1" visible="Yes" html="False" fieldSource="Expr1" PathID="CADFATDetailHidden1">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</Hidden>
						<ReportLabel id="45" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="VALJUR" wizardCaption="VALJUR" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" format="#,##0.00">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="89"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="61" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="ISSQN" format="#,##0.00" fieldSource="ISSQN">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="81"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="38" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="RET_INSS" wizardCaption="RET_INSS" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" format="#,##0.00" fieldSource="RET_INSS">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="85"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="41" fieldSourceType="DBColumn" dataType="Date" html="False" hideDuplicates="False" resetAt="Report" name="DATPGT" fieldSource="DATPGT" wizardCaption="DATPGT" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" format="dd/mm/yyyy">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="64"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="43" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="VALPGT" fieldSource="VALPGT" wizardCaption="VALPGT" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" format="#,##0.00">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="67"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="47" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="VALMUL" wizardCaption="VALMUL" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" format="#,##0.00">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Custom Code" actionCategory="General" id="121"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="9" visible="True" lines="1" name="Report_Footer" wizardSectionType="ReportFooter" PathID="CADFATReport_Footer">
					<Components>
						<Panel id="10" visible="True" name="NoRecords" wizardNoRecords="No records" PathID="CADFATReport_FooterNoRecords">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Panel>
						<ReportLabel id="17" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_VALFAT" fieldSource="VALFAT" summarised="True" wizardCaption="VALFAT" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="Sum: " wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" format="#,##0.00" function="Sum">
							<Components/>
							<Events>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Hidden id="87" fieldSourceType="DBColumn" dataType="Text" name="HINSS_MIM1" visible="Yes" PathID="CADFATReport_FooterHINSS_MIM1">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Declare Variable" actionCategory="General" id="129" name="mMim1" type="Text" initialValue="CCGetSession(&quot;mINSS_MIM&quot;)"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</Hidden>
						<ReportLabel id="23" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_VALJUR" summarised="True" wizardCaption="VALJUR" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="Sum: " wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" format="#,##0.00">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="DLookup" actionCategory="Database" id="120" typeOfTarget="Control" expression="&quot;round ((1 * (VALFAT - $ninss)) / 100 / 30 * (data()-datvnc), 2)&quot;" domain="&quot;CADFAT&quot;"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="66" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_ISSQN" format="#,##0.00">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="DLookup" actionCategory="Database" id="83" typeOfTarget="Control" expression="&quot;sum(issqn)&quot;" domain="&quot;CADFAT&quot;" connection="Faturar" dataType="Float" criteria="&quot;CODCLI = '&quot;.CCGetParam(&quot;CODCLI&quot;,&quot;&quot;).&quot;'&quot;"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="19" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_RET_INSS" summarised="True" wizardCaption="RET INSS" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="Sum: " wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" format="#,##0.00">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="DLookup" actionCategory="Database" id="86" typeOfTarget="Control" expression="&quot;round((VALFAT)*11/100,2)&quot;" domain="&quot;CADFAT&quot;" criteria="&quot;right(MESREF,4)+left(MESREF,2) &gt; '199901' AND CODCLI = '&quot;.CCGetParam(&quot;CODCLI&quot;,&quot;&quot;).&quot;' AND round((VALFAT)*11/100,2)&gt;&quot; $CADFAT-&gt;HINSS_MIM-&gt;Value" connection="Faturar" dataType="Float"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="21" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_VALPGT" fieldSource="VALPGT" summarised="True" function="Sum" wizardCaption="VALPGT" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="Sum: " wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" format="#,##0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="25" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="TotalSum_VALMUL" summarised="True" wizardCaption="VALMUL" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardPrefix="Sum: " wizardAddNbsp="False" wizardAlign="right" wizardVAlign="baseline" format="#,##0.00">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="DLookup" actionCategory="Database" id="122" typeOfTarget="Control" expression="&quot;sum(VALFAT + iif((date() - datvnc)&lt;=0 or VALPGT &gt; 0,0,round((1 * (VALFAT - iif(round(VALFAT*11/100,2)&lt;=&quot;.$mHINSS_MIM.&quot; or right(MESREF,4)+left(MESREF,2) &lt; '199901',0,round(VALFAT*11/100,2)))) / 100 / 30 * (date() - datvnc), 2)) -(iif(round(VALFAT*11/100,2)&lt;=&quot;.$mHINSS_MIM.&quot; or right(MESREF,4)+left(MESREF,2) &lt; '199901',0,round(VALFAT*11/100,2))+ iif(right(MESREF,4)+left(MESREF,2) &lt; '199901',0,round(VALFAT*2/100,2))))&quot;" domain="&quot;CADFAT&quot;" connection="Faturar" criteria="&quot;CODCLI = '&quot;.CCGetParam(&quot;CODCLI&quot;,&quot;&quot;).&quot;'&quot;"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="11" visible="True" lines="2" name="Page_Footer" wizardSectionType="PageFooter" pageBreakAfter="True" PathID="CADFATPage_Footer">
					<Components>
						<ReportLabel id="14" fieldSourceType="SpecialValue" dataType="Date" html="False" hideDuplicates="False" resetAt="Report" name="Report_CurrentDateTime" fieldSource="CurrentDateTime" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardInsertToDateTD="True">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Navigator id="15" size="10" type="Simple" name="Navigator" wizardPagingType="Simple" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardImages="Images" wizardPageNumbers="Simple" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="of" wizardImagesScheme="Compact" pageSizes="1;5;10;25;50" PathID="CADFATPage_FooterNavigator">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Navigator>
						<Panel id="13" visible="True" name="PageBreak" PathID="CADFATPage_FooterPageBreak">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Panel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="72" conditionType="Parameter" useIsNull="False" field="CODCLI" dataType="Text" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="CODCLI"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="95" tableName="CADFAT" posLeft="10" posTop="10" posWidth="115" posHeight="180"/>
			</JoinTables>
			<JoinLinks/>
			<Fields>
				<Field id="117" fieldName="*"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<ReportGroups/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Report>
		<IncludePage id="124" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="PHPTemplates" name="Pag_Rel_Faturas_Cliente_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Code" language="PHPTemplates" name="Pag_Rel_Faturas_Cliente.php" forShow="True" url="Pag_Rel_Faturas_Cliente.php" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="125" groupID="1"/>
		<Group id="126" groupID="2"/>
		<Group id="127" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
