<?php
//Include Common Files @1-86C57E6A
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "Pag_Rel_Faturas_Cliente.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @123-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation



//CADFAT ReportGroup class @2-A366097E
class clsReportGroupCADFAT {
    public $GroupType;
    public $mode; //1 - open, 2 - close
    public $Report_TotalRecords, $_Report_TotalRecordsAttributes;
    public $HINSS_MIM, $_HINSS_MIMAttributes;
    public $CODFAT, $_CODFATAttributes;
    public $VALFAT, $_VALFATAttributes;
    public $MESREF, $_MESREFAttributes;
    public $PGTOELET, $_PGTOELETAttributes;
    public $DATVNC, $_DATVNCAttributes;
    public $Hidden1, $_Hidden1Attributes;
    public $VALJUR, $_VALJURAttributes;
    public $ISSQN, $_ISSQNAttributes;
    public $RET_INSS, $_RET_INSSAttributes;
    public $DATPGT, $_DATPGTAttributes;
    public $VALPGT, $_VALPGTAttributes;
    public $VALMUL, $_VALMULAttributes;
    public $TotalSum_VALFAT, $_TotalSum_VALFATAttributes;
    public $HINSS_MIM1, $_HINSS_MIM1Attributes;
    public $TotalSum_VALJUR, $_TotalSum_VALJURAttributes;
    public $TotalSum_ISSQN, $_TotalSum_ISSQNAttributes;
    public $TotalSum_RET_INSS, $_TotalSum_RET_INSSAttributes;
    public $TotalSum_VALPGT, $_TotalSum_VALPGTAttributes;
    public $TotalSum_VALMUL, $_TotalSum_VALMULAttributes;
    public $Report_CurrentDateTime, $_Report_CurrentDateTimeAttributes;
    public $Attributes;
    public $ReportTotalIndex = 0;
    public $PageTotalIndex;
    public $PageNumber;
    public $RowNumber;
    public $Parent;

    function clsReportGroupCADFAT(& $parent) {
        $this->Parent = & $parent;
        $this->Attributes = $this->Parent->Attributes->GetAsArray();
    }
    function SetControls($PrevGroup = "") {
        $this->HINSS_MIM = $this->Parent->HINSS_MIM->Value;
        $this->CODFAT = $this->Parent->CODFAT->Value;
        $this->VALFAT = $this->Parent->VALFAT->Value;
        $this->MESREF = $this->Parent->MESREF->Value;
        $this->PGTOELET = $this->Parent->PGTOELET->Value;
        $this->DATVNC = $this->Parent->DATVNC->Value;
        $this->Hidden1 = $this->Parent->Hidden1->Value;
        $this->VALJUR = $this->Parent->VALJUR->Value;
        $this->ISSQN = $this->Parent->ISSQN->Value;
        $this->RET_INSS = $this->Parent->RET_INSS->Value;
        $this->DATPGT = $this->Parent->DATPGT->Value;
        $this->VALPGT = $this->Parent->VALPGT->Value;
        $this->VALMUL = $this->Parent->VALMUL->Value;
        $this->HINSS_MIM1 = $this->Parent->HINSS_MIM1->Value;
        $this->TotalSum_VALJUR = $this->Parent->TotalSum_VALJUR->Value;
        $this->TotalSum_ISSQN = $this->Parent->TotalSum_ISSQN->Value;
        $this->TotalSum_RET_INSS = $this->Parent->TotalSum_RET_INSS->Value;
        $this->TotalSum_VALMUL = $this->Parent->TotalSum_VALMUL->Value;
    }

    function SetTotalControls($mode = "", $PrevGroup = "") {
        $this->Report_TotalRecords = $this->Parent->Report_TotalRecords->GetTotalValue($mode);
        $this->TotalSum_VALFAT = $this->Parent->TotalSum_VALFAT->GetTotalValue($mode);
        $this->TotalSum_VALPGT = $this->Parent->TotalSum_VALPGT->GetTotalValue($mode);
        $this->_Report_TotalRecordsAttributes = $this->Parent->Report_TotalRecords->Attributes->GetAsArray();
        $this->_Sorter_CODFATAttributes = $this->Parent->Sorter_CODFAT->Attributes->GetAsArray();
        $this->_Sorter_VALFATAttributes = $this->Parent->Sorter_VALFAT->Attributes->GetAsArray();
        $this->_Sorter_MESREFAttributes = $this->Parent->Sorter_MESREF->Attributes->GetAsArray();
        $this->_Sorter_DATVNCAttributes = $this->Parent->Sorter_DATVNC->Attributes->GetAsArray();
        $this->_HINSS_MIMAttributes = $this->Parent->HINSS_MIM->Attributes->GetAsArray();
        $this->_Sorter1Attributes = $this->Parent->Sorter1->Attributes->GetAsArray();
        $this->_Sorter_RET_INSSAttributes = $this->Parent->Sorter_RET_INSS->Attributes->GetAsArray();
        $this->_Sorter_DATPGTAttributes = $this->Parent->Sorter_DATPGT->Attributes->GetAsArray();
        $this->_Sorter_VALPGTAttributes = $this->Parent->Sorter_VALPGT->Attributes->GetAsArray();
        $this->_CODFATAttributes = $this->Parent->CODFAT->Attributes->GetAsArray();
        $this->_VALFATAttributes = $this->Parent->VALFAT->Attributes->GetAsArray();
        $this->_MESREFAttributes = $this->Parent->MESREF->Attributes->GetAsArray();
        $this->_PGTOELETAttributes = $this->Parent->PGTOELET->Attributes->GetAsArray();
        $this->_DATVNCAttributes = $this->Parent->DATVNC->Attributes->GetAsArray();
        $this->_Hidden1Attributes = $this->Parent->Hidden1->Attributes->GetAsArray();
        $this->_VALJURAttributes = $this->Parent->VALJUR->Attributes->GetAsArray();
        $this->_ISSQNAttributes = $this->Parent->ISSQN->Attributes->GetAsArray();
        $this->_RET_INSSAttributes = $this->Parent->RET_INSS->Attributes->GetAsArray();
        $this->_DATPGTAttributes = $this->Parent->DATPGT->Attributes->GetAsArray();
        $this->_VALPGTAttributes = $this->Parent->VALPGT->Attributes->GetAsArray();
        $this->_VALMULAttributes = $this->Parent->VALMUL->Attributes->GetAsArray();
        $this->_TotalSum_VALFATAttributes = $this->Parent->TotalSum_VALFAT->Attributes->GetAsArray();
        $this->_HINSS_MIM1Attributes = $this->Parent->HINSS_MIM1->Attributes->GetAsArray();
        $this->_TotalSum_VALJURAttributes = $this->Parent->TotalSum_VALJUR->Attributes->GetAsArray();
        $this->_TotalSum_ISSQNAttributes = $this->Parent->TotalSum_ISSQN->Attributes->GetAsArray();
        $this->_TotalSum_RET_INSSAttributes = $this->Parent->TotalSum_RET_INSS->Attributes->GetAsArray();
        $this->_TotalSum_VALPGTAttributes = $this->Parent->TotalSum_VALPGT->Attributes->GetAsArray();
        $this->_TotalSum_VALMULAttributes = $this->Parent->TotalSum_VALMUL->Attributes->GetAsArray();
        $this->_Report_CurrentDateTimeAttributes = $this->Parent->Report_CurrentDateTime->Attributes->GetAsArray();
        $this->_NavigatorAttributes = $this->Parent->Navigator->Attributes->GetAsArray();
    }
    function SyncWithHeader(& $Header) {
        $Header->Report_TotalRecords = $this->Report_TotalRecords;
        $Header->_Report_TotalRecordsAttributes = $this->_Report_TotalRecordsAttributes;
        $Header->TotalSum_VALFAT = $this->TotalSum_VALFAT;
        $Header->_TotalSum_VALFATAttributes = $this->_TotalSum_VALFATAttributes;
        $Header->TotalSum_VALPGT = $this->TotalSum_VALPGT;
        $Header->_TotalSum_VALPGTAttributes = $this->_TotalSum_VALPGTAttributes;
        $this->HINSS_MIM = $Header->HINSS_MIM;
        $Header->_HINSS_MIMAttributes = $this->_HINSS_MIMAttributes;
        $this->Parent->HINSS_MIM->Value = $Header->HINSS_MIM;
        $this->Parent->HINSS_MIM->Attributes->RestoreFromArray($Header->_HINSS_MIMAttributes);
        $this->CODFAT = $Header->CODFAT;
        $Header->_CODFATAttributes = $this->_CODFATAttributes;
        $this->Parent->CODFAT->Value = $Header->CODFAT;
        $this->Parent->CODFAT->Attributes->RestoreFromArray($Header->_CODFATAttributes);
        $this->VALFAT = $Header->VALFAT;
        $Header->_VALFATAttributes = $this->_VALFATAttributes;
        $this->Parent->VALFAT->Value = $Header->VALFAT;
        $this->Parent->VALFAT->Attributes->RestoreFromArray($Header->_VALFATAttributes);
        $this->MESREF = $Header->MESREF;
        $Header->_MESREFAttributes = $this->_MESREFAttributes;
        $this->Parent->MESREF->Value = $Header->MESREF;
        $this->Parent->MESREF->Attributes->RestoreFromArray($Header->_MESREFAttributes);
        $this->PGTOELET = $Header->PGTOELET;
        $Header->_PGTOELETAttributes = $this->_PGTOELETAttributes;
        $this->Parent->PGTOELET->Value = $Header->PGTOELET;
        $this->Parent->PGTOELET->Attributes->RestoreFromArray($Header->_PGTOELETAttributes);
        $this->DATVNC = $Header->DATVNC;
        $Header->_DATVNCAttributes = $this->_DATVNCAttributes;
        $this->Parent->DATVNC->Value = $Header->DATVNC;
        $this->Parent->DATVNC->Attributes->RestoreFromArray($Header->_DATVNCAttributes);
        $this->Hidden1 = $Header->Hidden1;
        $Header->_Hidden1Attributes = $this->_Hidden1Attributes;
        $this->Parent->Hidden1->Value = $Header->Hidden1;
        $this->Parent->Hidden1->Attributes->RestoreFromArray($Header->_Hidden1Attributes);
        $this->VALJUR = $Header->VALJUR;
        $Header->_VALJURAttributes = $this->_VALJURAttributes;
        $this->Parent->VALJUR->Value = $Header->VALJUR;
        $this->Parent->VALJUR->Attributes->RestoreFromArray($Header->_VALJURAttributes);
        $this->ISSQN = $Header->ISSQN;
        $Header->_ISSQNAttributes = $this->_ISSQNAttributes;
        $this->Parent->ISSQN->Value = $Header->ISSQN;
        $this->Parent->ISSQN->Attributes->RestoreFromArray($Header->_ISSQNAttributes);
        $this->RET_INSS = $Header->RET_INSS;
        $Header->_RET_INSSAttributes = $this->_RET_INSSAttributes;
        $this->Parent->RET_INSS->Value = $Header->RET_INSS;
        $this->Parent->RET_INSS->Attributes->RestoreFromArray($Header->_RET_INSSAttributes);
        $this->DATPGT = $Header->DATPGT;
        $Header->_DATPGTAttributes = $this->_DATPGTAttributes;
        $this->Parent->DATPGT->Value = $Header->DATPGT;
        $this->Parent->DATPGT->Attributes->RestoreFromArray($Header->_DATPGTAttributes);
        $this->VALPGT = $Header->VALPGT;
        $Header->_VALPGTAttributes = $this->_VALPGTAttributes;
        $this->Parent->VALPGT->Value = $Header->VALPGT;
        $this->Parent->VALPGT->Attributes->RestoreFromArray($Header->_VALPGTAttributes);
        $this->VALMUL = $Header->VALMUL;
        $Header->_VALMULAttributes = $this->_VALMULAttributes;
        $this->Parent->VALMUL->Value = $Header->VALMUL;
        $this->Parent->VALMUL->Attributes->RestoreFromArray($Header->_VALMULAttributes);
        $this->HINSS_MIM1 = $Header->HINSS_MIM1;
        $Header->_HINSS_MIM1Attributes = $this->_HINSS_MIM1Attributes;
        $this->Parent->HINSS_MIM1->Value = $Header->HINSS_MIM1;
        $this->Parent->HINSS_MIM1->Attributes->RestoreFromArray($Header->_HINSS_MIM1Attributes);
        $this->TotalSum_VALJUR = $Header->TotalSum_VALJUR;
        $Header->_TotalSum_VALJURAttributes = $this->_TotalSum_VALJURAttributes;
        $this->Parent->TotalSum_VALJUR->Value = $Header->TotalSum_VALJUR;
        $this->Parent->TotalSum_VALJUR->Attributes->RestoreFromArray($Header->_TotalSum_VALJURAttributes);
        $this->TotalSum_ISSQN = $Header->TotalSum_ISSQN;
        $Header->_TotalSum_ISSQNAttributes = $this->_TotalSum_ISSQNAttributes;
        $this->Parent->TotalSum_ISSQN->Value = $Header->TotalSum_ISSQN;
        $this->Parent->TotalSum_ISSQN->Attributes->RestoreFromArray($Header->_TotalSum_ISSQNAttributes);
        $this->TotalSum_RET_INSS = $Header->TotalSum_RET_INSS;
        $Header->_TotalSum_RET_INSSAttributes = $this->_TotalSum_RET_INSSAttributes;
        $this->Parent->TotalSum_RET_INSS->Value = $Header->TotalSum_RET_INSS;
        $this->Parent->TotalSum_RET_INSS->Attributes->RestoreFromArray($Header->_TotalSum_RET_INSSAttributes);
        $this->TotalSum_VALMUL = $Header->TotalSum_VALMUL;
        $Header->_TotalSum_VALMULAttributes = $this->_TotalSum_VALMULAttributes;
        $this->Parent->TotalSum_VALMUL->Value = $Header->TotalSum_VALMUL;
        $this->Parent->TotalSum_VALMUL->Attributes->RestoreFromArray($Header->_TotalSum_VALMULAttributes);
    }
    function ChangeTotalControls() {
        $this->Report_TotalRecords = $this->Parent->Report_TotalRecords->GetValue();
        $this->TotalSum_VALFAT = $this->Parent->TotalSum_VALFAT->GetValue();
        $this->TotalSum_VALPGT = $this->Parent->TotalSum_VALPGT->GetValue();
    }
}
//End CADFAT ReportGroup class

//CADFAT GroupsCollection class @2-595373CD
class clsGroupsCollectionCADFAT {
    public $Groups;
    public $mPageCurrentHeaderIndex;
    public $PageSize;
    public $TotalPages = 0;
    public $TotalRows = 0;
    public $CurrentPageSize = 0;
    public $Pages;
    public $Parent;
    public $LastDetailIndex;

    function clsGroupsCollectionCADFAT(& $parent) {
        $this->Parent = & $parent;
        $this->Groups = array();
        $this->Pages  = array();
        $this->mReportTotalIndex = 0;
        $this->mPageTotalIndex = 1;
    }

    function & InitGroup() {
        $group = new clsReportGroupCADFAT($this->Parent);
        $group->RowNumber = $this->TotalRows + 1;
        $group->PageNumber = $this->TotalPages;
        $group->PageTotalIndex = $this->mPageCurrentHeaderIndex;
        return $group;
    }

    function RestoreValues() {
        $this->Parent->Report_TotalRecords->Value = $this->Parent->Report_TotalRecords->initialValue;
        $this->Parent->HINSS_MIM->Value = $this->Parent->HINSS_MIM->initialValue;
        $this->Parent->CODFAT->Value = $this->Parent->CODFAT->initialValue;
        $this->Parent->VALFAT->Value = $this->Parent->VALFAT->initialValue;
        $this->Parent->MESREF->Value = $this->Parent->MESREF->initialValue;
        $this->Parent->PGTOELET->Value = $this->Parent->PGTOELET->initialValue;
        $this->Parent->DATVNC->Value = $this->Parent->DATVNC->initialValue;
        $this->Parent->Hidden1->Value = $this->Parent->Hidden1->initialValue;
        $this->Parent->VALJUR->Value = $this->Parent->VALJUR->initialValue;
        $this->Parent->ISSQN->Value = $this->Parent->ISSQN->initialValue;
        $this->Parent->RET_INSS->Value = $this->Parent->RET_INSS->initialValue;
        $this->Parent->DATPGT->Value = $this->Parent->DATPGT->initialValue;
        $this->Parent->VALPGT->Value = $this->Parent->VALPGT->initialValue;
        $this->Parent->VALMUL->Value = $this->Parent->VALMUL->initialValue;
        $this->Parent->TotalSum_VALFAT->Value = $this->Parent->TotalSum_VALFAT->initialValue;
        $this->Parent->HINSS_MIM1->Value = $this->Parent->HINSS_MIM1->initialValue;
        $this->Parent->TotalSum_VALJUR->Value = $this->Parent->TotalSum_VALJUR->initialValue;
        $this->Parent->TotalSum_ISSQN->Value = $this->Parent->TotalSum_ISSQN->initialValue;
        $this->Parent->TotalSum_RET_INSS->Value = $this->Parent->TotalSum_RET_INSS->initialValue;
        $this->Parent->TotalSum_VALPGT->Value = $this->Parent->TotalSum_VALPGT->initialValue;
        $this->Parent->TotalSum_VALMUL->Value = $this->Parent->TotalSum_VALMUL->initialValue;
    }

    function OpenPage() {
        $this->TotalPages++;
        $Group = & $this->InitGroup();
        $this->Parent->Page_Header->CCSEventResult = CCGetEvent($this->Parent->Page_Header->CCSEvents, "OnInitialize", $this->Parent->Page_Header);
        if ($this->Parent->Page_Header->Visible)
            $this->CurrentPageSize = $this->CurrentPageSize + $this->Parent->Page_Header->Height;
        $Group->SetTotalControls("GetNextValue");
        $this->Parent->Page_Header->CCSEventResult = CCGetEvent($this->Parent->Page_Header->CCSEvents, "OnCalculate", $this->Parent->Page_Header);
        $Group->SetControls();
        $Group->Mode = 1;
        $Group->GroupType = "Page";
        $Group->PageTotalIndex = count($this->Groups);
        $this->mPageCurrentHeaderIndex = count($this->Groups);
        $this->Groups[] =  & $Group;
        $this->Pages[] =  count($this->Groups) == 2 ? 0 : count($this->Groups) - 1;
    }

    function OpenGroup($groupName) {
        $Group = "";
        $OpenFlag = false;
        if ($groupName == "Report") {
            $Group = & $this->InitGroup(true);
            $this->Parent->Report_Header->CCSEventResult = CCGetEvent($this->Parent->Report_Header->CCSEvents, "OnInitialize", $this->Parent->Report_Header);
            if ($this->Parent->Report_Header->Visible) 
                $this->CurrentPageSize = $this->CurrentPageSize + $this->Parent->Report_Header->Height;
                $Group->SetTotalControls("GetNextValue");
            $this->Parent->Report_Header->CCSEventResult = CCGetEvent($this->Parent->Report_Header->CCSEvents, "OnCalculate", $this->Parent->Report_Header);
            $Group->SetControls();
            $Group->Mode = 1;
            $Group->GroupType = "Report";
            $this->Groups[] = & $Group;
            $this->OpenPage();
        }
    }

    function ClosePage() {
        $Group = & $this->InitGroup();
        $this->Parent->Page_Footer->CCSEventResult = CCGetEvent($this->Parent->Page_Footer->CCSEvents, "OnInitialize", $this->Parent->Page_Footer);
        $Group->SetTotalControls("GetPrevValue");
        $Group->SyncWithHeader($this->Groups[$this->mPageCurrentHeaderIndex]);
        $this->Parent->Page_Footer->CCSEventResult = CCGetEvent($this->Parent->Page_Footer->CCSEvents, "OnCalculate", $this->Parent->Page_Footer);
        $Group->SetControls();
        $this->RestoreValues();
        $this->CurrentPageSize = 0;
        $Group->Mode = 2;
        $Group->GroupType = "Page";
        $this->Groups[] = & $Group;
    }

    function CloseGroup($groupName)
    {
        $Group = "";
        if ($groupName == "Report") {
            $Group = & $this->InitGroup(true);
            $this->Parent->Report_Footer->CCSEventResult = CCGetEvent($this->Parent->Report_Footer->CCSEvents, "OnInitialize", $this->Parent->Report_Footer);
            if ($this->Parent->Page_Footer->Visible) 
                $OverSize = $this->Parent->Report_Footer->Height + $this->Parent->Page_Footer->Height;
            else
                $OverSize = $this->Parent->Report_Footer->Height;
            if (($this->PageSize > 0) and $this->Parent->Report_Footer->Visible and ($this->CurrentPageSize + $OverSize > $this->PageSize)) {
                $this->ClosePage();
                $this->OpenPage();
            }
            $Group->SetTotalControls("GetPrevValue");
            $Group->SyncWithHeader($this->Groups[0]);
            if ($this->Parent->Report_Footer->Visible)
                $this->CurrentPageSize = $this->CurrentPageSize + $this->Parent->Report_Footer->Height;
            $this->Parent->Report_Footer->CCSEventResult = CCGetEvent($this->Parent->Report_Footer->CCSEvents, "OnCalculate", $this->Parent->Report_Footer);
            $Group->SetControls();
            $this->RestoreValues();
            $Group->Mode = 2;
            $Group->GroupType = "Report";
            $this->Groups[] = & $Group;
            $this->ClosePage();
            return;
        }
    }

    function AddItem()
    {
        $Group = & $this->InitGroup(true);
        $this->Parent->Detail->CCSEventResult = CCGetEvent($this->Parent->Detail->CCSEvents, "OnInitialize", $this->Parent->Detail);
        if ($this->Parent->Page_Footer->Visible) 
            $OverSize = $this->Parent->Detail->Height + $this->Parent->Page_Footer->Height;
        else
            $OverSize = $this->Parent->Detail->Height;
        if (($this->PageSize > 0) and $this->Parent->Detail->Visible and ($this->CurrentPageSize + $OverSize > $this->PageSize)) {
            $this->ClosePage();
            $this->OpenPage();
        }
        $this->TotalRows++;
        if ($this->LastDetailIndex)
            $PrevGroup = & $this->Groups[$this->LastDetailIndex];
        else
            $PrevGroup = "";
        $Group->SetTotalControls("", $PrevGroup);
        if ($this->Parent->Detail->Visible)
            $this->CurrentPageSize = $this->CurrentPageSize + $this->Parent->Detail->Height;
        $this->Parent->Detail->CCSEventResult = CCGetEvent($this->Parent->Detail->CCSEvents, "OnCalculate", $this->Parent->Detail);
        $Group->SetControls($PrevGroup);
        $this->LastDetailIndex = count($this->Groups);
        $this->Groups[] = & $Group;
    }
}
//End CADFAT GroupsCollection class

class clsReportCADFAT { //CADFAT Class @2-B7F61FA1

//CADFAT Variables @2-D9D86DF7

    public $ComponentType = "Report";
    public $PageSize;
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $CCSEvents = array();
    public $CCSEventResult;
    public $RelativePath = "";
    public $ViewMode = "Web";
    public $TemplateBlock;
    public $PageNumber;
    public $RowNumber;
    public $TotalRows;
    public $TotalPages;
    public $ControlsVisible = array();
    public $IsEmpty;
    public $Attributes;
    public $DetailBlock, $Detail;
    public $Report_FooterBlock, $Report_Footer;
    public $Report_HeaderBlock, $Report_Header;
    public $Page_FooterBlock, $Page_Footer;
    public $Page_HeaderBlock, $Page_Header;
    public $SorterName, $SorterDirection;

    public $ds;
    public $DataSource;
    public $UseClientPaging = false;

    //Report Controls
    public $StaticControls, $RowControls, $Report_FooterControls, $Report_HeaderControls;
    public $Page_FooterControls, $Page_HeaderControls;
    public $Sorter_CODFAT;
    public $Sorter_VALFAT;
    public $Sorter_MESREF;
    public $Sorter_DATVNC;
    public $Sorter1;
    public $Sorter_RET_INSS;
    public $Sorter_DATPGT;
    public $Sorter_VALPGT;
//End CADFAT Variables

//Class_Initialize Event @2-34F8E353
    function clsReportCADFAT($RelativePath = "", & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "CADFAT";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->Detail = new clsSection($this);
        $MinPageSize = 0;
        $MaxSectionSize = 0;
        $this->Detail->Height = 1;
        $MaxSectionSize = max($MaxSectionSize, $this->Detail->Height);
        $this->Report_Footer = new clsSection($this);
        $this->Report_Footer->Height = 1;
        $MaxSectionSize = max($MaxSectionSize, $this->Report_Footer->Height);
        $this->Report_Header = new clsSection($this);
        $this->Page_Footer = new clsSection($this);
        $this->Page_Footer->Height = 2;
        $MinPageSize += $this->Page_Footer->Height;
        $this->Page_Header = new clsSection($this);
        $this->Page_Header->Height = 1;
        $MinPageSize += $this->Page_Header->Height;
        $this->Errors = new clsErrors();
        $this->DataSource = new clsCADFATDataSource($this);
        $this->ds = & $this->DataSource;
        $PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(is_numeric($PageSize) && $PageSize > 0) {
            $this->PageSize = $PageSize;
        } else {
            if (!is_numeric($PageSize) || $PageSize < 0)
                $this->PageSize = 66;
             else if ($PageSize == "0")
                $this->PageSize = 100;
             else 
                $this->PageSize = min(100, $PageSize);
        }
        $MinPageSize += $MaxSectionSize;
        if ($this->PageSize && $MinPageSize && $this->PageSize < $MinPageSize)
            $this->PageSize = $MinPageSize;
        $this->PageNumber = $this->ViewMode == "Print" ? 1 : intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0 ) {
            $this->PageNumber = 1;
        }
        $this->SorterName = CCGetParam("CADFATOrder", "");
        $this->SorterDirection = CCGetParam("CADFATDir", "");

        $this->Report_TotalRecords = new clsControl(ccsReportLabel, "Report_TotalRecords", "Report_TotalRecords", ccsText, "", 0, $this);
        $this->Report_TotalRecords->TotalFunction = "Count";
        $this->Report_TotalRecords->IsEmptySource = true;
        $this->Sorter_CODFAT = new clsSorter($this->ComponentName, "Sorter_CODFAT", $FileName, $this);
        $this->Sorter_VALFAT = new clsSorter($this->ComponentName, "Sorter_VALFAT", $FileName, $this);
        $this->Sorter_MESREF = new clsSorter($this->ComponentName, "Sorter_MESREF", $FileName, $this);
        $this->Sorter_DATVNC = new clsSorter($this->ComponentName, "Sorter_DATVNC", $FileName, $this);
        $this->HINSS_MIM = new clsControl(ccsHidden, "HINSS_MIM", "HINSS_MIM", ccsText, "", CCGetRequestParam("HINSS_MIM", ccsGet, NULL), $this);
        $this->Sorter1 = new clsSorter($this->ComponentName, "Sorter1", $FileName, $this);
        $this->Sorter_RET_INSS = new clsSorter($this->ComponentName, "Sorter_RET_INSS", $FileName, $this);
        $this->Sorter_DATPGT = new clsSorter($this->ComponentName, "Sorter_DATPGT", $FileName, $this);
        $this->Sorter_VALPGT = new clsSorter($this->ComponentName, "Sorter_VALPGT", $FileName, $this);
        $this->CODFAT = new clsControl(ccsReportLabel, "CODFAT", "CODFAT", ccsText, "", "", $this);
        $this->VALFAT = new clsControl(ccsReportLabel, "VALFAT", "VALFAT", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), "", $this);
        $this->MESREF = new clsControl(ccsReportLabel, "MESREF", "MESREF", ccsText, "", "", $this);
        $this->PGTOELET = new clsControl(ccsReportLabel, "PGTOELET", "PGTOELET", ccsText, "", "", $this);
        $this->PGTOELET->IsEmptySource = true;
        $this->DATVNC = new clsControl(ccsReportLabel, "DATVNC", "DATVNC", ccsDate, array("dd", "/", "mm", "/", "yyyy"), "", $this);
        $this->Hidden1 = new clsControl(ccsHidden, "Hidden1", "Hidden1", ccsInteger, "", CCGetRequestParam("Hidden1", ccsGet, NULL), $this);
        $this->VALJUR = new clsControl(ccsReportLabel, "VALJUR", "VALJUR", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), "", $this);
        $this->VALJUR->IsEmptySource = true;
        $this->ISSQN = new clsControl(ccsReportLabel, "ISSQN", "ISSQN", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), "", $this);
        $this->RET_INSS = new clsControl(ccsReportLabel, "RET_INSS", "RET_INSS", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), "", $this);
        $this->DATPGT = new clsControl(ccsReportLabel, "DATPGT", "DATPGT", ccsDate, array("dd", "/", "mm", "/", "yyyy"), "", $this);
        $this->VALPGT = new clsControl(ccsReportLabel, "VALPGT", "VALPGT", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), "", $this);
        $this->VALMUL = new clsControl(ccsReportLabel, "VALMUL", "VALMUL", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), "", $this);
        $this->VALMUL->IsEmptySource = true;
        $this->NoRecords = new clsPanel("NoRecords", $this);
        $this->TotalSum_VALFAT = new clsControl(ccsReportLabel, "TotalSum_VALFAT", "TotalSum_VALFAT", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), "", $this);
        $this->TotalSum_VALFAT->TotalFunction = "Sum";
        $this->HINSS_MIM1 = new clsControl(ccsHidden, "HINSS_MIM1", "HINSS_MIM1", ccsText, "", CCGetRequestParam("HINSS_MIM1", ccsGet, NULL), $this);
        $this->TotalSum_VALJUR = new clsControl(ccsReportLabel, "TotalSum_VALJUR", "TotalSum_VALJUR", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), "", $this);
        $this->TotalSum_VALJUR->IsEmptySource = true;
        $this->TotalSum_ISSQN = new clsControl(ccsReportLabel, "TotalSum_ISSQN", "TotalSum_ISSQN", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), "", $this);
        $this->TotalSum_ISSQN->IsEmptySource = true;
        $this->TotalSum_RET_INSS = new clsControl(ccsReportLabel, "TotalSum_RET_INSS", "TotalSum_RET_INSS", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), "", $this);
        $this->TotalSum_RET_INSS->IsEmptySource = true;
        $this->TotalSum_VALPGT = new clsControl(ccsReportLabel, "TotalSum_VALPGT", "TotalSum_VALPGT", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), "", $this);
        $this->TotalSum_VALPGT->TotalFunction = "Sum";
        $this->TotalSum_VALMUL = new clsControl(ccsReportLabel, "TotalSum_VALMUL", "TotalSum_VALMUL", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), "", $this);
        $this->TotalSum_VALMUL->IsEmptySource = true;
        $this->Report_CurrentDateTime = new clsControl(ccsReportLabel, "Report_CurrentDateTime", "Report_CurrentDateTime", ccsText, array('ShortDate', ' ', 'ShortTime'), "", $this);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple, $this);
        $this->PageBreak = new clsPanel("PageBreak", $this);
    }
//End Class_Initialize Event

//Initialize Method @2-6C59EE65
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = $this->PageSize;
        $this->DataSource->AbsolutePage = $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//CheckErrors Method @2-417E2B71
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->Report_TotalRecords->Errors->Count());
        $errors = ($errors || $this->HINSS_MIM->Errors->Count());
        $errors = ($errors || $this->CODFAT->Errors->Count());
        $errors = ($errors || $this->VALFAT->Errors->Count());
        $errors = ($errors || $this->MESREF->Errors->Count());
        $errors = ($errors || $this->PGTOELET->Errors->Count());
        $errors = ($errors || $this->DATVNC->Errors->Count());
        $errors = ($errors || $this->Hidden1->Errors->Count());
        $errors = ($errors || $this->VALJUR->Errors->Count());
        $errors = ($errors || $this->ISSQN->Errors->Count());
        $errors = ($errors || $this->RET_INSS->Errors->Count());
        $errors = ($errors || $this->DATPGT->Errors->Count());
        $errors = ($errors || $this->VALPGT->Errors->Count());
        $errors = ($errors || $this->VALMUL->Errors->Count());
        $errors = ($errors || $this->TotalSum_VALFAT->Errors->Count());
        $errors = ($errors || $this->HINSS_MIM1->Errors->Count());
        $errors = ($errors || $this->TotalSum_VALJUR->Errors->Count());
        $errors = ($errors || $this->TotalSum_ISSQN->Errors->Count());
        $errors = ($errors || $this->TotalSum_RET_INSS->Errors->Count());
        $errors = ($errors || $this->TotalSum_VALPGT->Errors->Count());
        $errors = ($errors || $this->TotalSum_VALMUL->Errors->Count());
        $errors = ($errors || $this->Report_CurrentDateTime->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//GetErrors Method @2-43B7C8C9
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->Report_TotalRecords->Errors->ToString());
        $errors = ComposeStrings($errors, $this->HINSS_MIM->Errors->ToString());
        $errors = ComposeStrings($errors, $this->CODFAT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->VALFAT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->MESREF->Errors->ToString());
        $errors = ComposeStrings($errors, $this->PGTOELET->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DATVNC->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Hidden1->Errors->ToString());
        $errors = ComposeStrings($errors, $this->VALJUR->Errors->ToString());
        $errors = ComposeStrings($errors, $this->ISSQN->Errors->ToString());
        $errors = ComposeStrings($errors, $this->RET_INSS->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DATPGT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->VALPGT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->VALMUL->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TotalSum_VALFAT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->HINSS_MIM1->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TotalSum_VALJUR->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TotalSum_ISSQN->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TotalSum_RET_INSS->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TotalSum_VALPGT->Errors->ToString());
        $errors = ComposeStrings($errors, $this->TotalSum_VALMUL->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Report_CurrentDateTime->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

//Show Method @2-57D0DA44
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $ShownRecords = 0;

        $this->DataSource->Parameters["urlCODCLI"] = CCGetFromGet("CODCLI", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();

        $Groups = new clsGroupsCollectionCADFAT($this);
        $Groups->PageSize = $this->PageSize > 0 ? $this->PageSize : 0;

        $is_next_record = $this->DataSource->next_record();
        $this->IsEmpty = ! $is_next_record;
        while($is_next_record) {
            $this->DataSource->SetValues();
            $this->CODFAT->SetValue($this->DataSource->CODFAT->GetValue());
            $this->VALFAT->SetValue($this->DataSource->VALFAT->GetValue());
            $this->MESREF->SetValue($this->DataSource->MESREF->GetValue());
            $this->DATVNC->SetValue($this->DataSource->DATVNC->GetValue());
            $this->Hidden1->SetValue($this->DataSource->Hidden1->GetValue());
            $this->ISSQN->SetValue($this->DataSource->ISSQN->GetValue());
            $this->RET_INSS->SetValue($this->DataSource->RET_INSS->GetValue());
            $this->DATPGT->SetValue($this->DataSource->DATPGT->GetValue());
            $this->VALPGT->SetValue($this->DataSource->VALPGT->GetValue());
            $this->TotalSum_VALFAT->SetValue($this->DataSource->TotalSum_VALFAT->GetValue());
            $this->TotalSum_VALPGT->SetValue($this->DataSource->TotalSum_VALPGT->GetValue());
            $this->Report_TotalRecords->SetValue(1);
            $this->PGTOELET->SetValue("");
            $this->VALJUR->SetValue("");
            $this->VALMUL->SetValue("");
            $this->TotalSum_VALJUR->SetValue("");
            $this->TotalSum_ISSQN->SetValue("");
            $this->TotalSum_RET_INSS->SetValue("");
            $this->TotalSum_VALMUL->SetValue("");
            if (count($Groups->Groups) == 0) $Groups->OpenGroup("Report");
            $Groups->AddItem();
            $is_next_record = $this->DataSource->next_record();
        }
        if (!count($Groups->Groups)) {
            $Groups->OpenGroup("Report");
            $this->NoRecords->Visible = true;
        } else {
            $this->NoRecords->Visible = false;
        }
        $Groups->CloseGroup("Report");
        $this->TotalPages = $Groups->TotalPages;
        $this->TotalRows = $Groups->TotalRows;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $this->Attributes->Show();
        $ReportBlock = "Report " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $ReportBlock;

        if($this->CheckErrors()) {
            $Tpl->replaceblock("", $this->GetErrors());
            $Tpl->block_path = $ParentPath;
            return;
        } else {
            $items = & $Groups->Groups;
            $i = $Groups->Pages[min($this->PageNumber, $Groups->TotalPages) - 1];
            $this->ControlsVisible["CODFAT"] = $this->CODFAT->Visible;
            $this->ControlsVisible["VALFAT"] = $this->VALFAT->Visible;
            $this->ControlsVisible["MESREF"] = $this->MESREF->Visible;
            $this->ControlsVisible["PGTOELET"] = $this->PGTOELET->Visible;
            $this->ControlsVisible["DATVNC"] = $this->DATVNC->Visible;
            $this->ControlsVisible["Hidden1"] = $this->Hidden1->Visible;
            $this->ControlsVisible["VALJUR"] = $this->VALJUR->Visible;
            $this->ControlsVisible["ISSQN"] = $this->ISSQN->Visible;
            $this->ControlsVisible["RET_INSS"] = $this->RET_INSS->Visible;
            $this->ControlsVisible["DATPGT"] = $this->DATPGT->Visible;
            $this->ControlsVisible["VALPGT"] = $this->VALPGT->Visible;
            $this->ControlsVisible["VALMUL"] = $this->VALMUL->Visible;
            do {
                $this->Attributes->RestoreFromArray($items[$i]->Attributes);
                $this->RowNumber = $items[$i]->RowNumber;
                switch ($items[$i]->GroupType) {
                    Case "":
                        $Tpl->block_path = $ParentPath . "/" . $ReportBlock . "/Section Detail";
                        $this->CODFAT->SetValue($items[$i]->CODFAT);
                        $this->CODFAT->Attributes->RestoreFromArray($items[$i]->_CODFATAttributes);
                        $this->VALFAT->SetValue($items[$i]->VALFAT);
                        $this->VALFAT->Attributes->RestoreFromArray($items[$i]->_VALFATAttributes);
                        $this->MESREF->SetValue($items[$i]->MESREF);
                        $this->MESREF->Attributes->RestoreFromArray($items[$i]->_MESREFAttributes);
                        $this->PGTOELET->SetValue($items[$i]->PGTOELET);
                        $this->PGTOELET->Attributes->RestoreFromArray($items[$i]->_PGTOELETAttributes);
                        $this->DATVNC->SetValue($items[$i]->DATVNC);
                        $this->DATVNC->Attributes->RestoreFromArray($items[$i]->_DATVNCAttributes);
                        $this->Hidden1->SetValue($items[$i]->Hidden1);
                        $this->Hidden1->Attributes->RestoreFromArray($items[$i]->_Hidden1Attributes);
                        $this->VALJUR->SetValue($items[$i]->VALJUR);
                        $this->VALJUR->Attributes->RestoreFromArray($items[$i]->_VALJURAttributes);
                        $this->ISSQN->SetValue($items[$i]->ISSQN);
                        $this->ISSQN->Attributes->RestoreFromArray($items[$i]->_ISSQNAttributes);
                        $this->RET_INSS->SetValue($items[$i]->RET_INSS);
                        $this->RET_INSS->Attributes->RestoreFromArray($items[$i]->_RET_INSSAttributes);
                        $this->DATPGT->SetValue($items[$i]->DATPGT);
                        $this->DATPGT->Attributes->RestoreFromArray($items[$i]->_DATPGTAttributes);
                        $this->VALPGT->SetValue($items[$i]->VALPGT);
                        $this->VALPGT->Attributes->RestoreFromArray($items[$i]->_VALPGTAttributes);
                        $this->VALMUL->SetValue($items[$i]->VALMUL);
                        $this->VALMUL->Attributes->RestoreFromArray($items[$i]->_VALMULAttributes);
                        $this->Detail->CCSEventResult = CCGetEvent($this->Detail->CCSEvents, "BeforeShow", $this->Detail);
                        $this->Attributes->Show();
                        $this->CODFAT->Show();
                        $this->VALFAT->Show();
                        $this->MESREF->Show();
                        $this->PGTOELET->Show();
                        $this->DATVNC->Show();
                        $this->Hidden1->Show();
                        $this->VALJUR->Show();
                        $this->ISSQN->Show();
                        $this->RET_INSS->Show();
                        $this->DATPGT->Show();
                        $this->VALPGT->Show();
                        $this->VALMUL->Show();
                        $Tpl->block_path = $ParentPath . "/" . $ReportBlock;
                        if ($this->Detail->Visible)
                            $Tpl->parseto("Section Detail", true, "Section Detail");
                        break;
                    case "Report":
                        if ($items[$i]->Mode == 1) {
                            $this->Report_TotalRecords->SetValue($items[$i]->Report_TotalRecords);
                            $this->Report_TotalRecords->Attributes->RestoreFromArray($items[$i]->_Report_TotalRecordsAttributes);
                            $this->Report_Header->CCSEventResult = CCGetEvent($this->Report_Header->CCSEvents, "BeforeShow", $this->Report_Header);
                            if ($this->Report_Header->Visible) {
                                $Tpl->block_path = $ParentPath . "/" . $ReportBlock . "/Section Report_Header";
                                $this->Attributes->Show();
                                $this->Report_TotalRecords->Show();
                                $Tpl->block_path = $ParentPath . "/" . $ReportBlock;
                                $Tpl->parseto("Section Report_Header", true, "Section Detail");
                            }
                        }
                        if ($items[$i]->Mode == 2) {
                            $this->TotalSum_VALFAT->SetText(CCFormatNumber($items[$i]->TotalSum_VALFAT, array(False, 2, Null, Null, False, "", "", 1, True, "")), ccsFloat);
                            $this->TotalSum_VALFAT->Attributes->RestoreFromArray($items[$i]->_TotalSum_VALFATAttributes);
                            $this->HINSS_MIM1->SetValue($items[$i]->HINSS_MIM1);
                            $this->HINSS_MIM1->Attributes->RestoreFromArray($items[$i]->_HINSS_MIM1Attributes);
                            $this->TotalSum_VALJUR->SetValue($items[$i]->TotalSum_VALJUR);
                            $this->TotalSum_VALJUR->Attributes->RestoreFromArray($items[$i]->_TotalSum_VALJURAttributes);
                            $this->TotalSum_ISSQN->SetValue($items[$i]->TotalSum_ISSQN);
                            $this->TotalSum_ISSQN->Attributes->RestoreFromArray($items[$i]->_TotalSum_ISSQNAttributes);
                            $this->TotalSum_RET_INSS->SetValue($items[$i]->TotalSum_RET_INSS);
                            $this->TotalSum_RET_INSS->Attributes->RestoreFromArray($items[$i]->_TotalSum_RET_INSSAttributes);
                            $this->TotalSum_VALPGT->SetText(CCFormatNumber($items[$i]->TotalSum_VALPGT, array(False, 2, Null, Null, False, "", "", 1, True, "")), ccsFloat);
                            $this->TotalSum_VALPGT->Attributes->RestoreFromArray($items[$i]->_TotalSum_VALPGTAttributes);
                            $this->TotalSum_VALMUL->SetValue($items[$i]->TotalSum_VALMUL);
                            $this->TotalSum_VALMUL->Attributes->RestoreFromArray($items[$i]->_TotalSum_VALMULAttributes);
                            $this->Report_Footer->CCSEventResult = CCGetEvent($this->Report_Footer->CCSEvents, "BeforeShow", $this->Report_Footer);
                            if ($this->Report_Footer->Visible) {
                                $Tpl->block_path = $ParentPath . "/" . $ReportBlock . "/Section Report_Footer";
                                $this->NoRecords->Show();
                                $this->TotalSum_VALFAT->Show();
                                $this->HINSS_MIM1->Show();
                                $this->TotalSum_VALJUR->Show();
                                $this->TotalSum_ISSQN->Show();
                                $this->TotalSum_RET_INSS->Show();
                                $this->TotalSum_VALPGT->Show();
                                $this->TotalSum_VALMUL->Show();
                                $this->Attributes->Show();
                                $Tpl->block_path = $ParentPath . "/" . $ReportBlock;
                                $Tpl->parseto("Section Report_Footer", true, "Section Detail");
                            }
                        }
                        break;
                    case "Page":
                        if ($items[$i]->Mode == 1) {
                            $this->HINSS_MIM->SetValue($items[$i]->HINSS_MIM);
                            $this->HINSS_MIM->Attributes->RestoreFromArray($items[$i]->_HINSS_MIMAttributes);
                            $this->Page_Header->CCSEventResult = CCGetEvent($this->Page_Header->CCSEvents, "BeforeShow", $this->Page_Header);
                            if ($this->Page_Header->Visible) {
                                $Tpl->block_path = $ParentPath . "/" . $ReportBlock . "/Section Page_Header";
                                $this->Attributes->Show();
                                $this->Sorter_CODFAT->Show();
                                $this->Sorter_VALFAT->Show();
                                $this->Sorter_MESREF->Show();
                                $this->Sorter_DATVNC->Show();
                                $this->HINSS_MIM->Show();
                                $this->Sorter1->Show();
                                $this->Sorter_RET_INSS->Show();
                                $this->Sorter_DATPGT->Show();
                                $this->Sorter_VALPGT->Show();
                                $Tpl->block_path = $ParentPath . "/" . $ReportBlock;
                                $Tpl->parseto("Section Page_Header", true, "Section Detail");
                            }
                        }
                        if ($items[$i]->Mode == 2 && !$this->UseClientPaging || $items[$i]->Mode == 1 && $this->UseClientPaging) {
                            $this->PageBreak->Visible = (($i < count($items) - 1) && ($this->ViewMode == "Print"));
                            $this->Report_CurrentDateTime->SetValue(CCFormatDate(CCGetDateArray(), $this->Report_CurrentDateTime->Format));
                            $this->Report_CurrentDateTime->Attributes->RestoreFromArray($items[$i]->_Report_CurrentDateTimeAttributes);
                            $this->Navigator->PageNumber = $items[$i]->PageNumber;
                            $this->Navigator->TotalPages = $Groups->TotalPages;
                            $this->Navigator->Visible = ("Print" != $this->ViewMode);
                            $this->Page_Footer->CCSEventResult = CCGetEvent($this->Page_Footer->CCSEvents, "BeforeShow", $this->Page_Footer);
                            if ($this->Page_Footer->Visible) {
                                $Tpl->block_path = $ParentPath . "/" . $ReportBlock . "/Section Page_Footer";
                                $this->Report_CurrentDateTime->Show();
                                $this->Navigator->Show();
                                $this->PageBreak->Show();
                                $this->Attributes->Show();
                                $Tpl->block_path = $ParentPath . "/" . $ReportBlock;
                                $Tpl->parseto("Section Page_Footer", true, "Section Detail");
                            }
                        }
                        break;
                }
                $i++;
            } while ($i < count($items) && ($this->ViewMode == "Print" ||  !($i > 1 && $items[$i]->GroupType == 'Page' && $items[$i]->Mode == 1)));
            $Tpl->block_path = $ParentPath;
            $Tpl->parse($ReportBlock);
            $this->DataSource->close();
        }

    }
//End Show Method

} //End CADFAT Class @2-FCB6E20C

class clsCADFATDataSource extends clsDBFaturar {  //CADFATDataSource Class @2-E7B8EFDB

//DataSource Variables @2-1951D3BC
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $wp;


    // Datasource fields
    public $CODFAT;
    public $VALFAT;
    public $MESREF;
    public $DATVNC;
    public $Hidden1;
    public $ISSQN;
    public $RET_INSS;
    public $DATPGT;
    public $VALPGT;
    public $TotalSum_VALFAT;
    public $TotalSum_VALPGT;
//End DataSource Variables

//DataSourceClass_Initialize Event @2-4ECC3837
    function clsCADFATDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Report CADFAT";
        $this->Initialize();
        $this->CODFAT = new clsField("CODFAT", ccsText, "");
        $this->VALFAT = new clsField("VALFAT", ccsFloat, "");
        $this->MESREF = new clsField("MESREF", ccsText, "");
        $this->DATVNC = new clsField("DATVNC", ccsDate, $this->DateFormat);
        $this->Hidden1 = new clsField("Hidden1", ccsInteger, "");
        $this->ISSQN = new clsField("ISSQN", ccsFloat, "");
        $this->RET_INSS = new clsField("RET_INSS", ccsFloat, "");
        $this->DATPGT = new clsField("DATPGT", ccsDate, $this->DateFormat);
        $this->VALPGT = new clsField("VALPGT", ccsFloat, "");
        $this->TotalSum_VALFAT = new clsField("TotalSum_VALFAT", ccsFloat, "");
        $this->TotalSum_VALPGT = new clsField("TotalSum_VALPGT", ccsFloat, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @2-DE01F2C0
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_CODFAT" => array("CODFAT", ""), 
            "Sorter_VALFAT" => array("VALFAT", ""), 
            "Sorter_MESREF" => array("MESREF", ""), 
            "Sorter_DATVNC" => array("DATVNC", ""), 
            "Sorter1" => array("ISSQN", ""), 
            "Sorter_RET_INSS" => array("RET_INSS", ""), 
            "Sorter_DATPGT" => array("DATPGT", ""), 
            "Sorter_VALPGT" => array("VALPGT", "")));
    }
//End SetOrder Method

//Prepare Method @2-63C23736
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlCODCLI", ccsText, "", "", $this->Parameters["urlCODCLI"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "CODCLI", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @2-07747F35
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->SQL = "SELECT * \n\n" .
        "FROM CADFAT {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @2-922B424B
    function SetValues()
    {
        $this->CODFAT->SetDBValue($this->f("CODFAT"));
        $this->VALFAT->SetDBValue(trim($this->f("VALFAT")));
        $this->MESREF->SetDBValue($this->f("MESREF"));
        $this->DATVNC->SetDBValue(trim($this->f("DATVNC")));
        $this->Hidden1->SetDBValue(trim($this->f("Expr1")));
        $this->ISSQN->SetDBValue(trim($this->f("ISSQN")));
        $this->RET_INSS->SetDBValue(trim($this->f("RET_INSS")));
        $this->DATPGT->SetDBValue(trim($this->f("DATPGT")));
        $this->VALPGT->SetDBValue(trim($this->f("VALPGT")));
        $this->TotalSum_VALFAT->SetDBValue(trim($this->f("VALFAT")));
        $this->TotalSum_VALPGT->SetDBValue(trim($this->f("VALPGT")));
    }
//End SetValues Method

} //End CADFATDataSource Class @2-FCB6E20C



//Include Page implementation @124-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-5D5305C4
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "Pag_Rel_Faturas_Cliente.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-946ECC7A
CCSecurityRedirect("1;2;3", "");
//End Authenticate User

//Include events file @1-8415C489
include("./Pag_Rel_Faturas_Cliente_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-0DBA8DD4
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$Label1 = new clsControl(ccsLabel, "Label1", "Label1", ccsText, "", CCGetRequestParam("Label1", ccsGet, NULL), $MainPage);
$Report_Print = new clsControl(ccsLink, "Report_Print", "Report_Print", ccsText, "", CCGetRequestParam("Report_Print", ccsGet, NULL), $MainPage);
$Report_Print->Page = "Pag_Rel_Faturas_Cliente.php";
$CADFAT = new clsReportCADFAT("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->Label1 = & $Label1;
$MainPage->Report_Print = & $Report_Print;
$MainPage->CADFAT = & $CADFAT;
$MainPage->rodape = & $rodape;
$Report_Print->Parameters = CCGetQueryString("QueryString", array("ccsForm"));
$Report_Print->Parameters = CCAddParam($Report_Print->Parameters, "ViewMode", "Print");
$CADFAT->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-30AB84C1
$cabec->Operations();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-E60D18CB
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($CADFAT);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-56A69888
$cabec->Show();
$CADFAT->Show();
$rodape->Show();
$Label1->Show();
$Report_Print->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-C2A45BCB
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($CADFAT);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
