<?php
//BindEvents Method @1-36A56816
function BindEvents()
{
    global $Label1;
    global $Report_Print;
    global $CADFAT;
    $Label1->CCSEvents["BeforeShow"] = "Label1_BeforeShow";
    $Report_Print->CCSEvents["BeforeShow"] = "Report_Print_BeforeShow";
    $CADFAT->HINSS_MIM->CCSEvents["BeforeShow"] = "CADFAT_HINSS_MIM_BeforeShow";
    $CADFAT->PGTOELET->CCSEvents["BeforeShow"] = "CADFAT_PGTOELET_BeforeShow";
    $CADFAT->DATVNC->CCSEvents["BeforeShow"] = "CADFAT_DATVNC_BeforeShow";
    $CADFAT->VALJUR->CCSEvents["BeforeShow"] = "CADFAT_VALJUR_BeforeShow";
    $CADFAT->ISSQN->CCSEvents["BeforeShow"] = "CADFAT_ISSQN_BeforeShow";
    $CADFAT->RET_INSS->CCSEvents["BeforeShow"] = "CADFAT_RET_INSS_BeforeShow";
    $CADFAT->DATPGT->CCSEvents["BeforeShow"] = "CADFAT_DATPGT_BeforeShow";
    $CADFAT->VALPGT->CCSEvents["BeforeShow"] = "CADFAT_VALPGT_BeforeShow";
    $CADFAT->VALMUL->CCSEvents["BeforeShow"] = "CADFAT_VALMUL_BeforeShow";
    $CADFAT->HINSS_MIM1->CCSEvents["BeforeShow"] = "CADFAT_HINSS_MIM1_BeforeShow";
    $CADFAT->TotalSum_VALJUR->CCSEvents["BeforeShow"] = "CADFAT_TotalSum_VALJUR_BeforeShow";
    $CADFAT->TotalSum_ISSQN->CCSEvents["BeforeShow"] = "CADFAT_TotalSum_ISSQN_BeforeShow";
    $CADFAT->TotalSum_RET_INSS->CCSEvents["BeforeShow"] = "CADFAT_TotalSum_RET_INSS_BeforeShow";
    $CADFAT->TotalSum_VALMUL->CCSEvents["BeforeShow"] = "CADFAT_TotalSum_VALMUL_BeforeShow";
}
//End BindEvents Method

//Label1_BeforeShow @80-62EBFD0A
function Label1_BeforeShow(& $sender)
{
    $Label1_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Label1; //Compatibility
//End Label1_BeforeShow

//Custom Code @130-2A29BDB7
// -------------------------
    $mCgcCpf = CCGetParam("CGCCPF","");
	if (strlen(trim($mCgcCpf))==14)
	{
       $mCgcCpf = substr($mCgcCpf,0,2).".".substr($mCgcCpf,2,3).".".substr($mCgcCpf,5,3).".".substr($mCgcCpf,8,3).".".substr($mCgcCpf,11,2);
    }
	else
	{
       $mCgcCpf = substr($mCgcCpf,2,3).".".substr($mCgcCpf,5,3).".".substr($mCgcCpf,8,3).".".substr($mCgcCpf,11,2);
	}
    $Label1->SetValue("C L I E N T E : ".$mCgcCpf." ".CCGetParam("DESCLI",""));
// -------------------------
//End Custom Code

//Close Label1_BeforeShow @80-B48DF954
    return $Label1_BeforeShow;
}
//End Close Label1_BeforeShow

//Report_Print_BeforeShow @3-6CD7E3F9
function Report_Print_BeforeShow(& $sender)
{
    $Report_Print_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Report_Print; //Compatibility
//End Report_Print_BeforeShow

//Hide-Show Component @5-286F3E6C
    $Parameter1 = CCGetFromGet("ViewMode", "");
    $Parameter2 = "Print";
    if (0 == CCCompareValues($Parameter1, $Parameter2, ccsText))
        $Component->Visible = false;
//End Hide-Show Component

//Close Report_Print_BeforeShow @3-0DD1CC60
    return $Report_Print_BeforeShow;
}
//End Close Report_Print_BeforeShow

//DEL  // -------------------------
//DEL      $CADFAT->VALFAT1->SetValue("C L I E N T E : ".CCGetParam("CGCCPF","")." ".CCGetParam("DESCLI",""));
//DEL  // -------------------------

//CADFAT_HINSS_MIM_BeforeShow @82-9532F86B
function CADFAT_HINSS_MIM_BeforeShow(& $sender)
{
    $CADFAT_HINSS_MIM_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT; //Compatibility
//End CADFAT_HINSS_MIM_BeforeShow

//Declare Variable @128-6047B267
    global $mMim;
    $mMim = CCGetSession("mINSS_MIM");
//End Declare Variable
	$CADFAT->HINSS_MIM->SetValue($mMim);
//Close CADFAT_HINSS_MIM_BeforeShow @82-B85C0291
    return $CADFAT_HINSS_MIM_BeforeShow;
}
//End Close CADFAT_HINSS_MIM_BeforeShow

//CADFAT_PGTOELET_BeforeShow @50-1EC3A734
function CADFAT_PGTOELET_BeforeShow(& $sender)
{
    $CADFAT_PGTOELET_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT; //Compatibility
//End CADFAT_PGTOELET_BeforeShow

//Custom Code @51-2A29BDB7
// -------------------------
    $mValpgt = $CADFAT->VALPGT->GetValue();
    if ($mValpgt != 0)
	{
       $CADFAT->PGTOELET->SetValue("Sim");
	}
	else
	{
       $CADFAT->PGTOELET->SetValue("N�o");
	}
// -------------------------
//End Custom Code

//Close CADFAT_PGTOELET_BeforeShow @50-073E329B
    return $CADFAT_PGTOELET_BeforeShow;
}
//End Close CADFAT_PGTOELET_BeforeShow

//CADFAT_DATVNC_BeforeShow @34-60A96F2E
function CADFAT_DATVNC_BeforeShow(& $sender)
{
    $CADFAT_DATVNC_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT; //Compatibility
//End CADFAT_DATVNC_BeforeShow

//Custom Code @63-2A29BDB7
// -------------------------
/*
    if (!$CADFAT->DATVNC->IsNull)
	{
       $mDia = substr($CADFAT->DATVNC->GetValue(),-2);
	   $mMes = substr($CADFAT->DATVNC->GetValue(),5,2);
	   $mAno = substr($CADFAT->DATVNC->GetValue(),0,4);
       $CADFAT->DATVNC->SetValue($mDia."/".$mMes."/".$mAno);
	}
*/
// -------------------------
//End Custom Code

//Close CADFAT_DATVNC_BeforeShow @34-E8DC9168
    return $CADFAT_DATVNC_BeforeShow;
}
//End Close CADFAT_DATVNC_BeforeShow

//CADFAT_VALJUR_BeforeShow @45-5602D836
function CADFAT_VALJUR_BeforeShow(& $sender)
{
    $CADFAT_VALJUR_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT; //Compatibility
//End CADFAT_VALJUR_BeforeShow

//Custom Code @89-2A29BDB7
// -------------------------

   if ($CADFAT->VALPGT->GetValue() > 0.00) 
   {
      $mvr_multa  = 0.00;
   }
   else 
   {           
      if ($CADFAT->Hidden1->GetValue() <= 0) //date() - datvnc
	  {
         $mvr_multa = 0.00;
      }
      else 
	  {
         $mMesRef = $CADFAT->MESREF->GetValue();
         $mVr_Fat = $CADFAT->VALFAT->GetValue();
         $mINSS_MIM = $CADFAT->HINSS_MIM->GetValue();
         if (substr($mMesRef,3,4).substr($mMesRef,0,2)>'199901')
         {
            $nInss  = $CADFAT->RET_INSS->GetValue();
            if ($nInss <= $mINSS_MIM)
            {
               $nInss = 0;
            }
         }
         else
         {
            $nInss = 0;
         }
         $mdias = (int)$CADFAT->Hidden1->GetValue();
		 //echo $mDataHoje.'-'.$mDataVnc.'-';
         $mvr_multa = round ((1 * ($CADFAT->VALFAT->GetValue() - $nInss)) / 100 / 30 * $mdias, 2);
      }
   }
   $CADFAT->VALJUR->SetValue($mvr_multa);

   //$CADFAT->TotalSum_VALJUR->Value+=$mvr_multa;
   // -------------------------
//End Custom Code

//Close CADFAT_VALJUR_BeforeShow @45-5E45738D
    return $CADFAT_VALJUR_BeforeShow;
}
//End Close CADFAT_VALJUR_BeforeShow

//CADFAT_ISSQN_BeforeShow @61-67A4904F
function CADFAT_ISSQN_BeforeShow(& $sender)
{
    $CADFAT_ISSQN_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT; //Compatibility
//End CADFAT_ISSQN_BeforeShow

//Custom Code @81-2A29BDB7
// -------------------------
   //if ((substr($CADFAT->MESREF->Value,3,4).substr($CADFAT->MESREF->Value,0,2))>'199901')
   //{
   //   $CADFAT->ISSQN->SetValue(round(($CADFAT->VALFAT->Value)*2/100,2));
   //}
   //else
   //{
   //   $CADFAT->ISSQN->SetValue(0.00);
   //}
// -------------------------
//End Custom Code

//Close CADFAT_ISSQN_BeforeShow @61-64ACBB5D
    return $CADFAT_ISSQN_BeforeShow;
}
//End Close CADFAT_ISSQN_BeforeShow

//CADFAT_RET_INSS_BeforeShow @38-9C5EC165
function CADFAT_RET_INSS_BeforeShow(& $sender)
{
    $CADFAT_RET_INSS_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT; //Compatibility
//End CADFAT_RET_INSS_BeforeShow

//Custom Code @85-2A29BDB7
// -------------------------
   //$mMesRef = $CADFAT->MESREF->Value;
   //$mVr_Fat = $CADFAT->VALFAT->Value;
   //$mINSS_MIM = $CADFAT->HINSS_MIM->Value;
   //if (substr($mMesRef,3,4).substr($mMesRef,0,2)>'199901')
   //{
   //   $nInss  = round(($mVr_Fat)*11/100,2);
   //   if ($nInss <= $mINSS_MIM)
   //   {
   //      $nInss = 0;
   //   }
   //}
   //else
   //{
   //   $nInss = 0;
   //}
   //$CADFAT->RET_INSS->SetValue($nInss);
   //$CADFAT->RET_INSS->Value = $nInss;
// -------------------------
//End Custom Code

//Close CADFAT_RET_INSS_BeforeShow @38-167AB53D
    return $CADFAT_RET_INSS_BeforeShow;
}
//End Close CADFAT_RET_INSS_BeforeShow

//CADFAT_DATPGT_BeforeShow @41-F1ECF2E5
function CADFAT_DATPGT_BeforeShow(& $sender)
{
    $CADFAT_DATPGT_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT; //Compatibility
//End CADFAT_DATPGT_BeforeShow

//Custom Code @64-2A29BDB7
// -------------------------
/*
    if (!$CADFAT->DATPGT->IsNull)
	{
       $mDia = substr($CADFAT->DATPGT->GetValue(),-2);
	   $mMes = substr($CADFAT->DATPGT->GetValue(),5,2);
	   $mAno = substr($CADFAT->DATPGT->GetValue(),0,4);
       $CADFAT->DATPGT->SetValue($mDia."/".$mMes."/".$mAno);
	}
*/
// -------------------------
//End Custom Code

//Close CADFAT_DATPGT_BeforeShow @41-CE40794F
    return $CADFAT_DATPGT_BeforeShow;
}
//End Close CADFAT_DATPGT_BeforeShow

//CADFAT_VALPGT_BeforeShow @43-E584FF30
function CADFAT_VALPGT_BeforeShow(& $sender)
{
    $CADFAT_VALPGT_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT; //Compatibility
//End CADFAT_VALPGT_BeforeShow

//Custom Code @67-2A29BDB7
// -------------------------
    if ($CADFAT->VALPGT->IsNull)
	{
	   $CADFAT->VALPGT->SetValue(0);
       $CADFAT->VALPGT->SetValue(0.00);
	}
// -------------------------
//End Custom Code

//Close CADFAT_VALPGT_BeforeShow @43-58F79257
    return $CADFAT_VALPGT_BeforeShow;
}
//End Close CADFAT_VALPGT_BeforeShow

//CADFAT_VALMUL_BeforeShow @47-E8D94805
function CADFAT_VALMUL_BeforeShow(& $sender)
{
    $CADFAT_VALMUL_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT; //Compatibility
//End CADFAT_VALMUL_BeforeShow

//Custom Code @121-2A29BDB7
// -------------------------
$mMesRef = $CADFAT->MESREF->GetValue();
$mVr_Fat = $CADFAT->VALFAT->GetValue();
$mINSS_MIM = $CADFAT->HINSS_MIM->GetValue();
if (substr($mMesRef,3,4).substr($mMesRef,0,2)>'199901')
{
   $mIssqn = round(($mVr_Fat)*2/100,2);
   $nInss  = round(($mVr_Fat)*11/100,2);
   if ($nInss <= $mINSS_MIM)
   {
      $nInss = 0;
   }
}
else
{
   $mIssqn = 0;
   $nInss = 0;
}
if ($CADFAT->VALPGT->GetValue() > 0.00)
{
   $mdeb_atual = 0.00;
}
else 
{
   if ($CADFAT->Hidden1->GetValue() <= 0) //date() - datvnc
   {
      $mvr_multa = 0.00;
   }
   else 
   {
      $mdias = (int)$CADFAT->Hidden1->GetValue();
      $mvr_multa = round ((1 * ($CADFAT->VALFAT->GetValue() - $nInss)) / 100 / 30 * $mdias, 2);
   }
   $mdeb_atual = $CADFAT->VALFAT->GetValue() + $mvr_multa - ($nInss + $mIssqn);
   // inss = iif(round(VALFAT*11/100,2)<=".$mHINSS_MIM." or right(MESREF,4)+left(MESREF,2) < '199901',0,round(VALFAT*11/100,2))
   // select sum(VALFAT + iif((date() - datvnc)<=0 or VALPGT > 0,0,round((1 * (VALFAT - iif(round(VALFAT*11/100,2)<=".$mHINSS_MIM." or right(MESREF,4)+left(MESREF,2) < '199901',0,round(VALFAT*11/100,2)))) / 100 / 30 * (date() - datvnc), 2)) -(iif(round(VALFAT*11/100,2)<=".$mHINSS_MIM." or right(MESREF,4)+left(MESREF,2) < '199901',0,round(VALFAT*11/100,2))+ iif(right(MESREF,4)+left(MESREF,2) < '199901',0,round(VALFAT*2/100,2)))) from cadfat where
}
$CADFAT->VALMUL->SetValue($mdeb_atual);
// -------------------------
//End Custom Code

//Close CADFAT_VALMUL_BeforeShow @47-0E95E214
    return $CADFAT_VALMUL_BeforeShow;
}
//End Close CADFAT_VALMUL_BeforeShow

//DEL  
//DEL      global $DBfaturar;
//DEL      $Page = CCGetParentPage($sender);
//DEL  	$mCgcCpf = "valpgt <=0 AND CODCLI = '".CCGetParam("CGCCPF","")."'";
//DEL      $ccs_result = CCDLookUp("sum(VALFAT)", "cadfat", "(valpgt <=0 or valpgt is null) AND CODCLI = '".CCGetParam("CODCLI","")."'", $Page->Connections["Faturar"]);
//DEL      $ccs_result = doubleval($ccs_result);
//DEL      $Component->SetValue($ccs_result);
//DEL  

//CADFAT_HINSS_MIM1_BeforeShow @87-CF60B483
function CADFAT_HINSS_MIM1_BeforeShow(& $sender)
{
    $CADFAT_HINSS_MIM1_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT; //Compatibility
//End CADFAT_HINSS_MIM1_BeforeShow

//Declare Variable @129-552186F6
    global $mMim1;
    $mMim1 = CCGetSession("mINSS_MIM");
//End Declare Variable
	$CADFAT->HINSS_MIM1->SetValue($mMim1);
//Close CADFAT_HINSS_MIM1_BeforeShow @87-B0E4BB38
    return $CADFAT_HINSS_MIM1_BeforeShow;
}
//End Close CADFAT_HINSS_MIM1_BeforeShow

//CADFAT_TotalSum_VALJUR_BeforeShow @23-48B3169F
function CADFAT_TotalSum_VALJUR_BeforeShow(& $sender)
{
    $CADFAT_TotalSum_VALJUR_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT; //Compatibility
//End CADFAT_TotalSum_VALJUR_BeforeShow

//DLookup @120-9F7C1C25
    global $DB;
    $mHINSS_MIM = (string)CCGetSession("mINSS_MIM");//$CADFAT->HINSS_MIM1->GetValue();
	$mHINSS_MIM = (str_replace(",", ".",$mHINSS_MIM));
    $Page = CCGetParentPage($sender);
	//                                                            +-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
	//                                                            |                                                                                                                                                                                                                   |
	//                                                            | INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS |
	//                                                            |                                                                                                                                                                                                                   |
	//                                                            +-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
    //$ccs_result = CCDLookUp("SUM(round ((1 * (VALFAT - ".(float)(CCDLookUp("round(VALFAT*11/100,2)", "CADFAT", "right(MESREF,4)+left(MESREF,2) > '199901' AND CODCLI = '".CCGetParam("CODCLI","")."' AND round(VALFAT*11/100,2) > ".$mHINSS_MIM, $Page->Connections["Faturar"]))." )) / 100 / 30 * (date()-datvnc), 2))", "CADFAT", "VALPGT <=0 AND (date() - datvnc) > 0 AND CODCLI = '".CCGetParam("CODCLI","")."'", $Page->Connections["Faturar"]);  iif(round(VALFAT*11/100,2)<=".$mHINSS_MIM." or right(MESREF,4)+left(MESREF,2) < '199901',0,
    $ccs_result = CCDLookUp("SUM(round((VALFAT-RET_INSS)/100/30*(SYSDATE-datvnc),2))", "CADFAT", "(VALPGT <=0 or valpgt is null) AND (SYSDATE - datvnc) > 0 AND CODCLI = '".CCGetParam("CODCLI","")."' and (ret_inss<=".$mHINSS_MIM." or substr(MESREF,4,4)||substr(MESREF,1,2) < '199901')", $Page->Connections["Faturar"]);
    $ccs_result = doubleval($ccs_result);
    $Component->SetValue($ccs_result);
//End DLookup

//Close CADFAT_TotalSum_VALJUR_BeforeShow @23-499826BF
    return $CADFAT_TotalSum_VALJUR_BeforeShow;
}
//End Close CADFAT_TotalSum_VALJUR_BeforeShow

//CADFAT_TotalSum_ISSQN_BeforeShow @66-3ED6F745
function CADFAT_TotalSum_ISSQN_BeforeShow(& $sender)
{
    $CADFAT_TotalSum_ISSQN_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT; //Compatibility
//End CADFAT_TotalSum_ISSQN_BeforeShow

//DLookup @83-1C3EF3F4
    global $DBfaturar;
    $Page = CCGetParentPage($sender);
	/*
	                                       A T E N � � O
       Substituir as fun��es "left" e "right" por "substr" quando da substitui��o do banco de dados de
	   .DBF (DBase/Clipper Summer 87) para Oracle. Esta substitui��o de banco dever� ser feita ao t�r-
	   mino do upgrade total do sistema.
	*/
    $ccs_result = CCDLookUp("sum(round((VALFAT)*2/100,2))", "CADFAT", "(valpgt<=0 or valpgt is null) and substr(MESREF,4,4)||substr(MESREF,1,2) > '199901' AND CODCLI = '".CCGetParam("CODCLI","")."'", $Page->Connections["Faturar"]);
    $ccs_result = doubleval($ccs_result);
    $Component->SetValue($ccs_result);
//End DLookup

//Close CADFAT_TotalSum_ISSQN_BeforeShow @66-0E47CAC3
    return $CADFAT_TotalSum_ISSQN_BeforeShow;
}
//End Close CADFAT_TotalSum_ISSQN_BeforeShow

//CADFAT_TotalSum_RET_INSS_BeforeShow @19-7BDA57A9
function CADFAT_TotalSum_RET_INSS_BeforeShow(& $sender)
{
    $CADFAT_TotalSum_RET_INSS_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT; //Compatibility
//End CADFAT_TotalSum_RET_INSS_BeforeShow

//DLookup @86-AD201468
    global $DBfaturar;
    $Page = CCGetParentPage($sender);
    $mHINSS_MIM = (string)CCGetSession("mINSS_MIM");//$CADFAT->HINSS_MIM1->GetValue();
	$mHINSS_MIM = (str_replace(",", ".",$mHINSS_MIM));
    $ccs_result = CCDLookUp("sum(ret_inss)", "CADFAT", "(valpgt<=0 or valpgt is null) and substr(MESREF,4,4)+substr(MESREF,1,2) > '199901' AND CODCLI = '".CCGetParam("CODCLI","")."' AND ret_inss > ".$mHINSS_MIM, $Page->Connections["Faturar"]);
    $ccs_result = doubleval($ccs_result);
    $Component->SetValue($ccs_result);
//End DLookup

//Close CADFAT_TotalSum_RET_INSS_BeforeShow @19-E00B53EA
    return $CADFAT_TotalSum_RET_INSS_BeforeShow;
}
//End Close CADFAT_TotalSum_RET_INSS_BeforeShow

//CADFAT_TotalSum_VALMUL_BeforeShow @25-15F1F5AA
function CADFAT_TotalSum_VALMUL_BeforeShow(& $sender)
{
    $CADFAT_TotalSum_VALMUL_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT; //Compatibility
//End CADFAT_TotalSum_VALMUL_BeforeShow

//DLookup @122-D7A63968
    global $DBfaturar;
    $mINSS_MIM = CCGetSession("mINSS_MIM");//$CADFAT->HINSS_MIM->GetValue();
	$mINSS_MIM = (str_replace(",", ".",$mINSS_MIM));
    $Page = CCGetParentPage($sender);// iif((date() - datvnc)<=0 or VALPGT > 0,0,  iif(round(VALFAT*11/100,2)<=".$mINSS_MIM." or right(MESREF,4)+left(MESREF,2) < '199901',0,  iif(round(VALFAT*11/100,2)<=".$mINSS_MIM." or right(MESREF,4)+left(MESREF,2) < '199901',0,  iif(round(VALFAT*11/100,2)<=".$mINSS_MIM." or right(MESREF,4)+left(MESREF,2) < '199901',0,
    $ccs_result = CCDLookUp("sum(VALFAT + round((VALFAT - RET_INSS) / 100 / 30 * (sysdate - datvnc), 2) - (RET_INSS + ISSQN))", "CADFAT", "(VALPGT <=0 or valpgt is null) AND CODCLI = '".CCGetParam("CODCLI","")."' and ((sysdate - datvnc)<=0 or VALPGT > 0) and (ret_inss<=".$mINSS_MIM." or substr(MESREF,4,4)||substr(MESREF,1,2) < '199901')", $Page->Connections["Faturar"]);
    $ccs_result = doubleval($ccs_result);
    $Component->SetValue($ccs_result);
//End DLookup

//Close CADFAT_TotalSum_VALMUL_BeforeShow @25-1948B726
    return $CADFAT_TotalSum_VALMUL_BeforeShow;
}
//End Close CADFAT_TotalSum_VALMUL_BeforeShow
//DEL  // -------------------------
//DEL      $CADFAT->nomeCgcCpfCliente->SetValue("C L I E N T E : ".CCGetParam("CGCCPF","")." ".CCGetParam("DESCLI",""));
//DEL  // -------------------------

//DEL  // -------------------------
//DEL      $mValpgt = $CADFAT->VALPGT->Value;
//DEL      if ($mValpgt != 0)
//DEL  	{
//DEL         $CADFAT->PGTOELET->SetValue("Sim");
//DEL  	}
//DEL  	else
//DEL  	{
//DEL         $CADFAT->PGTOELET->SetValue("N�o");
//DEL  	}
//DEL  // -------------------------


//DEL  // -------------------------
//DEL      if (!$CADFAT->DATVNC->IsNull)
//DEL  	{
//DEL         $mDia = substr($CADFAT->DATVNC->Value,-2);
//DEL  	   $mMes = substr($CADFAT->DATVNC->Value,5,2);
//DEL  	   $mAno = substr($CADFAT->DATVNC->Value,0,4);
//DEL         $CADFAT->DATVNC->SetValue($mDia."/".$mMes."/".$mAno);
//DEL  	}
//DEL  // -------------------------


//DEL      global $DB;
//DEL      $Page = CCGetParentPage($sender);
//DEL      $ccs_result = CCDLookUp("DATVNC", "CADFAT", "CODFAT = '".$CADFAT->CODFAT->Value."'", $Page->Connections["Faturar"]);
//DEL      $Component->SetValue($ccs_result);

//DEL      global $DBfaturar;
//DEL      $Page = CCGetParentPage($sender);
//DEL      $ccs_result = CCDLookUp("DATVNC", "CADFAT", "CODFAT = '".$CADFAT->CODFAT->Value."'", $Page->Connections["Faturar"]);
//DEL      //$ccs_result = CCParseDate($ccs_result, array("dd", "/", "mm", "/", "yyyy", " ", "HH", ":", "nn", ":", "ss"));
//DEL      $Component->SetValue($ccs_result);

//DEL  // -------------------------
//DEL     if ($CADFAT->VALPGT->Value > 0.00) 
//DEL     {
//DEL        $mvr_multa  = 0.00;
//DEL     }
//DEL     else 
//DEL     {           
//DEL        if ($CADFAT->Hidden1->Value <= 0) //date() - datvnc
//DEL  	  {
//DEL           $mvr_multa = 0.00;
//DEL        }
//DEL        else 
//DEL  	  {
//DEL           $mMesRef = $CADFAT->MESREF->Value;
//DEL           $mVr_Fat = $CADFAT->VALFAT->Value;
//DEL           $mINSS_MIM = $CADFAT->HINSS_MIM->Value;
//DEL           if (substr($mMesRef,3,4).substr($mMesRef,0,2)>'199901')
//DEL           {
//DEL              $nInss  = round(($mVr_Fat)*11/100,2);
//DEL              if ($nInss <= $mINSS_MIM)
//DEL              {
//DEL                 $nInss = 0;
//DEL              }
//DEL           }
//DEL           else
//DEL           {
//DEL              $nInss = 0;
//DEL           }
//DEL           $mdias = (int)$CADFAT->Hidden1->Value;
//DEL  		 //echo $mDataHoje.'-'.$mDataVnc.'-';
//DEL           $mvr_multa = round ((1 * ($CADFAT->VALFAT->Value - $nInss)) / 100 / 30 * $mdias, 2);
//DEL        }
//DEL     }
//DEL     $CADFAT->VALJUR->SetValue($mvr_multa);
//DEL     // -------------------------


//DEL  // -------------------------
//DEL     if ((substr($CADFAT->MESREF->Value,3,4).substr($CADFAT->MESREF->Value,0,2))>'199901')
//DEL     {
//DEL        $CADFAT->ISSQN->SetValue(round(($CADFAT->VALFAT->Value)*2/100,2));
//DEL     }
//DEL     else
//DEL     {
//DEL        $CADFAT->ISSQN->SetValue(0.00);
//DEL     }
//DEL  // -------------------------


//DEL  // -------------------------
//DEL     $mMesRef = $CADFAT->MESREF->Value;
//DEL     $mVr_Fat = $CADFAT->VALFAT->Value;
//DEL     $mINSS_MIM = $CADFAT->HINSS_MIM->Value;
//DEL     if (substr($mMesRef,3,4).substr($mMesRef,0,2)>'199901')
//DEL     {
//DEL        $nInss  = round(($mVr_Fat)*11/100,2);
//DEL        if ($nInss <= $mINSS_MIM)
//DEL        {
//DEL           $nInss = 0;
//DEL        }
//DEL     }
//DEL     else
//DEL     {
//DEL        $nInss = 0;
//DEL     }
//DEL     $CADFAT->RET_INSS->SetValue($nInss);
//DEL     $CADFAT->RET_INSS->Value = $nInss;
//DEL  // -------------------------


//DEL  function CADFAT_VALFAT_BeforeShow(& $sender)
//DEL  {
//DEL      $CADFAT_VALFAT_BeforeShow = true;
//DEL      $Component = & $sender;
//DEL      $Container = & CCGetParentContainer($sender);
//DEL      global $CADFAT; //Compatibility
//DEL  	global $mTotalFatPagas;


//DEL  // -------------------------
//DEL      if ($CADFAT->VALPGT->Value <= 0) 
//DEL  	{
//DEL         $mTotalFatPagas += $CADFAT->VALFAT->Value;
//DEL  	}
//DEL  // -------------------------

//DEL  // -------------------------
//DEL      $mJuros  = 0.00;
//DEL      if ($CADFAT->VALPGT->Value <= 0)
//DEL  	{
//DEL         $numDias = 2 ;//idate("d.m.y") - idate("d.m.y",$CADFAT->HDDVencimento->Value);
//DEL         if ($numDias > 0)
//DEL  	   {
//DEL  	     $valFat  = $CADFAT->VALFAT->Value;
//DEL  	     $nInss   = $CADFAT->RET_INSS->Value;
//DEL  	     $mJuros  = round((1*($valFat-$nInss)/100)/30*$numDias,2);
//DEL         }
//DEL  	}
//DEL  	$CADFAT->VALJUR->SetValue($mJuros);
//DEL  // -------------------------

//DEL  // -------------------------
//DEL      if (!$CADFAT->DATPGT->IsNull)
//DEL  	{
//DEL         $mDia = substr($CADFAT->DATPGT->Value,-2);
//DEL  	   $mMes = substr($CADFAT->DATPGT->Value,5,2);
//DEL  	   $mAno = substr($CADFAT->DATPGT->Value,0,4);
//DEL         $CADFAT->DATPGT->SetValue($mDia."/".$mMes."/".$mAno);
//DEL  	}
//DEL  // -------------------------


//DEL  // -------------------------
//DEL      if ($CADFAT->VALPGT->IsNull)
//DEL  	{
//DEL  	   $CADFAT->VALPGT->Value = 0;
//DEL         $CADFAT->VALPGT->SetValue(0.00);
//DEL  	}
//DEL  // -------------------------


//DEL  // -------------------------
//DEL  $mMesRef = $CADFAT->MESREF->Value;
//DEL  $mVr_Fat = $CADFAT->VALFAT->Value;
//DEL  $mINSS_MIM = $CADFAT->HINSS_MIM->Value;
//DEL  if (substr($mMesRef,3,4).substr($mMesRef,0,2)>'199901')
//DEL  {
//DEL     $mIssqn = round(($mVr_Fat)*2/100,2);
//DEL     $nInss  = round(($mVr_Fat)*11/100,2);
//DEL     if ($nInss <= $mINSS_MIM)
//DEL     {
//DEL        $nInss = 0;
//DEL     }
//DEL  }
//DEL  else
//DEL  {
//DEL     $mIssqn = 0;
//DEL     $nInss = 0;
//DEL  }
//DEL  if ($CADFAT->VALPGT->Value > 0.00)
//DEL  {
//DEL     $mdeb_atual = 0.00;
//DEL  }
//DEL  else 
//DEL  {
//DEL     if ($CADFAT->Hidden1->Value <= 0) //date() - datvnc
//DEL     {
//DEL        $mvr_multa = 0.00;
//DEL     }
//DEL     else 
//DEL     {
//DEL        $mdias = (int)$CADFAT->Hidden1->Value;
//DEL        $mvr_multa = round ((1 * ($CADFAT->VALFAT->Value - $nInss)) / 100 / 30 * $mdias, 2);
//DEL     }
//DEL     $mdeb_atual = $CADFAT->VALFAT->Value + $mvr_multa - ($nInss + $mIssqn);
//DEL     // inss = iif(round(VALFAT*11/100,2)<=".$mHINSS_MIM." or right(MESREF,4)+left(MESREF,2) < '199901',0,round(VALFAT*11/100,2))
//DEL     // select sum(VALFAT + iif((date() - datvnc)<=0 or VALPGT > 0,0,round((1 * (VALFAT - iif(round(VALFAT*11/100,2)<=".$mHINSS_MIM." or right(MESREF,4)+left(MESREF,2) < '199901',0,round(VALFAT*11/100,2)))) / 100 / 30 * (date() - datvnc), 2)) -(iif(round(VALFAT*11/100,2)<=".$mHINSS_MIM." or right(MESREF,4)+left(MESREF,2) < '199901',0,round(VALFAT*11/100,2))+ iif(right(MESREF,4)+left(MESREF,2) < '199901',0,round(VALFAT*2/100,2)))) from cadfat where
//DEL  }
//DEL  $CADFAT->VALMUL->SetValue($mdeb_atual);
//DEL  // -------------------------


//DEL  function CADFAT_TotalSum_VALFAT_BeforeShow(& $sender)
//DEL  {
//DEL      $CADFAT_TotalSum_VALFAT_BeforeShow = true;
//DEL      $Component = & $sender;
//DEL      $Container = & CCGetParentContainer($sender);
//DEL      global $CADFAT; //Compatibility
//DEL  	global $mTotalFatPagas;


//DEL  // -------------------------
//DEL      $CADFAT->TotalSum_VALFAT->SetValue($mTotalFatPagas);
//DEL  // -------------------------


//DEL      global $DBfaturar;
//DEL      $Page = CCGetParentPage($sender);
//DEL  	$mCgcCpf = "valpgt <=0 AND CODCLI = '".CCGetParam("CGCCPF","")."'";
//DEL      $ccs_result = CCDLookUp("sum(VALFAT)", "cadfat", "(valpgt <=0 or valpgt is null) AND CODCLI = '".CCGetParam("CODCLI","")."'", $Page->Connections["Faturar"]);
//DEL      $ccs_result = doubleval($ccs_result);
//DEL      $Component->SetValue($ccs_result);


//DEL      global $DB;
//DEL      $mHINSS_MIM = (string)$CADFAT->HINSS_MIM1->Value;
//DEL      $Page = CCGetParentPage($sender);
//DEL  	//                                                            +-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
//DEL  	//                                                            |                                                                                                                                                                                                                   |
//DEL  	//                                                            | INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS INSS |
//DEL  	//                                                            |                                                                                                                                                                                                                   |
//DEL  	//                                                            +-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
//DEL      //$ccs_result = CCDLookUp("SUM(round ((1 * (VALFAT - ".(float)(CCDLookUp("round(VALFAT*11/100,2)", "CADFAT", "right(MESREF,4)+left(MESREF,2) > '199901' AND CODCLI = '".CCGetParam("CODCLI","")."' AND round(VALFAT*11/100,2) > ".$mHINSS_MIM, $Page->Connections["Faturar"]))." )) / 100 / 30 * (date()-datvnc), 2))", "CADFAT", "VALPGT <=0 AND (date() - datvnc) > 0 AND CODCLI = '".CCGetParam("CODCLI","")."'", $Page->Connections["Faturar"]);
//DEL      $ccs_result = CCDLookUp("SUM(round ((1 * (VALFAT - iif(round(VALFAT*11/100,2)<=".$mHINSS_MIM." or right(MESREF,4)+left(MESREF,2) < '199901',0,round(VALFAT*11/100,2)) )) / 100 / 30 * (date()-datvnc), 2))", "CADFAT", "(VALPGT <=0 or valpgt is null) AND (date() - datvnc) > 0 AND CODCLI = '".CCGetParam("CODCLI","")."'", $Page->Connections["Faturar"]);
//DEL      $ccs_result = doubleval($ccs_result);
//DEL      $Component->SetValue($ccs_result);


//DEL      global $DBfaturar;
//DEL      $Page = CCGetParentPage($sender);
//DEL  	/*
//DEL  	                                       A T E N � � O
//DEL         Substituir as fun��es "left" e "right" por "substr" quando da substitui��o do banco de dados de
//DEL  	   .DBF (DBase/Clipper Summer 87) para Oracle. Esta substitui��o de banco dever� ser feita ao t�r-
//DEL  	   mino do upgrade total do sistema.
//DEL  	*/
//DEL      $ccs_result = CCDLookUp("sum(round((VALFAT)*2/100,2))", "CADFAT", "(valpgt<=0 or valpgt is null) and right(MESREF,4)+left(MESREF,2) > '199901' AND CODCLI = '".CCGetParam("CODCLI","")."'", $Page->Connections["Faturar"]);
//DEL      $ccs_result = doubleval($ccs_result);
//DEL      $Component->SetValue($ccs_result);


//DEL  // -------------------------
//DEL      $CADFAT->TotalSum_VALJUR->SetValue($CADFAT->HddTotValJur->Value);
//DEL  // -------------------------

//DEL      global $DBfaturar;
//DEL      $Page = CCGetParentPage($sender);
//DEL      $mHINSS_MIM = (string)$CADFAT->HINSS_MIM1->Value;
//DEL      $ccs_result = CCDLookUp("sum(round(VALFAT*11/100,2))", "CADFAT", "(valpgt<=0 or valpgt is null) and right(MESREF,4)+left(MESREF,2) > '199901' AND CODCLI = '".CCGetParam("CODCLI","")."' AND round(VALFAT*11/100,2) > ".$mHINSS_MIM, $Page->Connections["Faturar"]);
//DEL      $ccs_result = doubleval($ccs_result);
//DEL      $Component->SetValue($ccs_result);


//DEL      global $DBfaturar;
//DEL      $mINSS_MIM = $CADFAT->HINSS_MIM->Value;
//DEL      $Page = CCGetParentPage($sender);
//DEL      $ccs_result = CCDLookUp("sum(VALFAT + iif((date() - datvnc)<=0 or VALPGT > 0,0,round((1 * (VALFAT - iif(round(VALFAT*11/100,2)<=".$mINSS_MIM." or right(MESREF,4)+left(MESREF,2) < '199901',0,round(VALFAT*11/100,2)))) / 100 / 30 * (date() - datvnc), 2)) -(iif(round(VALFAT*11/100,2)<=".$mINSS_MIM." or right(MESREF,4)+left(MESREF,2) < '199901',0,round(VALFAT*11/100,2))+ iif(right(MESREF,4)+left(MESREF,2) < '199901',0,round(VALFAT*2/100,2))))", "CADFAT", "(VALPGT <=0 or valpgt is null) AND CODCLI = '".CCGetParam("CODCLI","")."'", $Page->Connections["Faturar"]);
//DEL      $ccs_result = doubleval($ccs_result);
//DEL      $Component->SetValue($ccs_result);


//DEL  // -------------------------
//DEL      $CADFAT->RET_INSS->SetValue($CADFAT->RET_INSS->Value);
//DEL  // -------------------------

//DEL      global $DBfaturar;
//DEL      $Page = CCGetParentPage($sender);
//DEL  	/*
//DEL  	                                       A T E N � � O
//DEL         Substituir as fun��es "left" e "right" por "substr" quando da substitui��o do banco de dados de
//DEL  	   .DBF (DBase/Clipper Summer 87) para Oracle. Esta substitui��o de banco dever� ser feita ao t�r-
//DEL  	   mino do upgrade total do sistema.
//DEL  	*/
//DEL      $ccs_result = CCDLookUp("sum(round((VALFAT)*2/100,2))", "CADFAT", " right(MESREF,4)+left(MESREF,2) > '199901' AND CODCLI = '".CCGetParam("CODCLI","")."'", $Page->Connections["Faturar"]);
//DEL      $ccs_result = doubleval($ccs_result);
//DEL      $Component->SetValue($ccs_result);


//DEL  // -------------------------
//DEL      $mImpostos = $CADFAT->RET_INSS->Value + 
//DEL  	             $CADFAT->ISSQN->Value;
//DEL      if ($CADFAT->VALPGT->Value <= 0 )
//DEL  	{
//DEL      $mDebTot = (
//DEL  	              $CADFAT->VALFAT->Value    +
//DEL  	              $CADFAT->VALJUR->Value
//DEL  				) - $mImpostos;
//DEL      }
//DEL  	else
//DEL  	{
//DEL  	   $mDebTot =  0;
//DEL  	}
//DEL      $CADFAT->VALMUL->SetValue($mDebTot);
//DEL  	$CADFAT->VALMUL->Value = $mDebTot;
//DEL  // -------------------------


//DEL  function CADFAT_TotalSum_VALJUR_BeforeShow(& $sender)
//DEL  {
//DEL      $CADFAT_TotalSum_VALJUR_BeforeShow = true;
//DEL      $Component = & $sender;
//DEL      $Container = & CCGetParentContainer($sender);
//DEL  	global $mTotJur;
//DEL      global $CADFAT; //Compatibility


//DEL  // -------------------------
//DEL      $CADFAT->TotalSum_VALJUR->SetValue($mTotJur);
//DEL  // -------------------------

//DEL  function CADFAT_TotalSum_VALMUL_BeforeShow(& $sender)
//DEL  {
//DEL      $CADFAT_TotalSum_VALMUL_BeforeShow = true;
//DEL      $Component = & $sender;
//DEL      $Container = & CCGetParentContainer($sender);
//DEL  	global $mTotDeb;
//DEL      global $CADFAT; //Compatibility


//DEL  // -------------------------
//DEL      $CADFAT->TotalSum_VALMUL->SetValue($mTotDeb);
//DEL  // -------------------------



?>
