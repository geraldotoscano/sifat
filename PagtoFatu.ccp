<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="4" sourceType="Table" urlType="Relative" secured="False" allowInsert="True" allowUpdate="True" allowDelete="False" validateData="True" preserveParameters="GET" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="Pagamento" actionPage="PagtoFatu" errorSummator="Error" wizardFormMethod="post" returnPage="index.ccp">
			<Components>
				<Label id="11" fieldSourceType="DBColumn" dataType="Text" html="False" name="lblTitulo">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="12"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<TextBox id="5" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="tbxFatura" required="True" caption="Fatura">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Validate Entry" actionCategory="Validation" id="10" required="True" minimumLength="6" maximumLength="6" inputMask="000000" errorMessage="Somente números"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<Button id="6" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Insert" operation="Search" wizardCaption="Adicionar">
					<Components/>
					<Events>
						<Event name="OnClick" type="Server">
							<Actions>
								<Action actionName="DLookup" actionCategory="Database" id="9" typeOfTarget="Control" expression="&quot;codfat&quot;" domain="&quot;cadfat&quot;" criteria="&quot;codfat = $cod_fat&quot;" connection="Faturar"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="8" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Cancel" operation="Cancel" wizardCaption="Cancelar">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="PagtoFatu.php" forShow="True" url="PagtoFatu.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="PagtoFatu_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="13" groupID="1"/>
		<Group id="14" groupID="2"/>
		<Group id="15" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="16"/>
			</Actions>
		</Event>
	</Events>
</Page>
