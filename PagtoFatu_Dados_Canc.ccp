<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="4" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="True" allowDelete="False" validateData="True" preserveParameters="GET" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="Pagto" actionPage="PagtoFatu_Dados" errorSummator="Error" wizardFormMethod="post" connection="Faturar" dataSource="CADFAT, CADCLI, TABPBH" activeCollection="TableParameters" returnPage="default.ccp" customUpdate="CADFAT" customUpdateType="Table" pasteAsReplace="pasteAsReplace" removeParameters="opcao">
			<Components>
				<Label id="15" fieldSourceType="DBColumn" dataType="Text" html="False" name="Label1">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="16" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="17" fieldSourceType="DBColumn" dataType="Text" html="False" name="Label2">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="18" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<TextBox id="5" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="CODFAT" fieldSource="CODFAT">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<Hidden id="95" fieldSourceType="DBColumn" dataType="Text" name="ESFERA" fieldSource="ESFERA">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<TextBox id="103" fieldSourceType="DBColumn" dataType="Float" name="RET_INSS" fieldSource="RET_INSS" visible="Yes">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="133" eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<Hidden id="120" fieldSourceType="DBColumn" dataType="Text" name="EXPORT" fieldSource="EXPORT">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<TextBox id="6" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="DESCLI" fieldSource="DESCLI">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="11" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="DATEMI" fieldSource="DATEMI" format="dd/mm/yyyy">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<Hidden id="143" fieldSourceType="DBColumn" dataType="Float" name="Hidd_RET_INSS" fieldSource="RET_INSS">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<TextBox id="7" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="DATVNC" fieldSource="DATVNC" format="dd/mm/yyyy">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<Hidden id="113" fieldSourceType="DBColumn" dataType="Float" name="VALCOB" fieldSource="VALCOB" format="#,##0.00">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="115" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Hidden>
				<TextBox id="12" visible="Yes" fieldSourceType="DBColumn" dataType="Float" name="VALPBH" fieldSource="VALPBH" format="#,##0.00">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="134" eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<Label id="19" fieldSourceType="DBColumn" dataType="Text" html="False" name="lblJa_Exportada" defaultValue="'Já Exportada?'"><Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="46" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<TextBox id="107" fieldSourceType="DBColumn" dataType="Float" name="VALFAT" fieldSource="VALFAT" visible="Yes">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="132" eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<Hidden id="114" fieldSourceType="DBColumn" dataType="Float" name="VALMUL" fieldSource="VALMUL" format="#,##0.00" defaultValue="0">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<TextBox id="13" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="MESREF" fieldSource="CADFAT_MESREF">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="8" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="DATPGT" fieldSource="DATPGT" required="False" caption="Pagamento" format="dd/mm/yyyy">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="109" eventType="Server"/>
							</Actions>
						</Event>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="110" eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="124" name="DatePicker_DATPGT1" control="DATPGT" wizardDatePickerType="Image" wizardPicture="Styles/Blueprint/Images/DatePicker.gif" style="Styles/Blueprint/Style.css">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
				<Hidden id="144" fieldSourceType="DBColumn" dataType="Float" name="Hidd_VALFAT" fieldSource="VALFAT">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<TextBox id="146" fieldSourceType="DBColumn" dataType="Text" html="False" name="lbl_Dias_Atraso" visible="Yes">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<Label id="140" fieldSourceType="DBColumn" dataType="Text" html="False" name="Label3">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<TextBox id="14" visible="Yes" fieldSourceType="DBColumn" dataType="Float" name="VALPGT" fieldSource="VALPGT">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="108" eventType="Server"/>
							</Actions>
						</Event>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="135" eventType="Client"/>
							</Actions>
						</Event>
						<Event name="OnChange" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="137" eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="91" fieldSourceType="DBColumn" dataType="Float" name="VALJUR" fieldSource="VALJUR" visible="Yes">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="121" eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<Hidden id="141" fieldSourceType="DBColumn" dataType="Float" name="taxa_juros">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="142" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Hidden>
				<TextBox id="99" fieldSourceType="DBColumn" dataType="Float" name="ISSQN" fieldSource="ISSQN" visible="Yes" format="#,##0.00">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="111" eventType="Server"/>
							</Actions>
						</Event>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="136" eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<ListBox id="148" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="CODTIPCANC" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="TIPCANC" boundColumn="CODTIPCANC" textColumn="DESCTIPCANC" required="True">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="151" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="149" tableName="TIPCANC" schemaName="SL005V3" posLeft="10" posTop="10" posWidth="95" posHeight="88"/>
					</JoinTables>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<TextArea id="150" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="DESCCANC" caption="Descrição cancelamento" required="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextArea>
				<Button id="9" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Update" wizardCaption="Mudar">
					<Components/>
					<Events>
						<Event name="OnClick" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="145"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="10" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Cancel" operation="Cancel" wizardCaption="Cancelar">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="126" eventType="Server"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters>
				<TableParameter id="44" conditionType="Parameter" useIsNull="False" field="CADFAT.CODFAT" dataType="Text" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="CODFAT"/>
				<TableParameter id="153" conditionType="Expression" useIsNull="False" searchConditionType="Equal" parameterType="URL" logicOperator="And" expression="CADFAT.VALPGT = 0"/>
			</TableParameters>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables>
				<JoinTable id="20" tableName="CADFAT" schemaName="SL005V3" posLeft="10" posTop="10" posWidth="115" posHeight="425"/>
				<JoinTable id="25" tableName="CADCLI" schemaName="SL005V3" posLeft="297" posTop="26" posWidth="115" posHeight="205"/>
				<JoinTable id="32" tableName="TABPBH" schemaName="SL005V3" posLeft="146" posTop="111" posWidth="115" posHeight="168"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="40" tableLeft="CADCLI" tableRight="CADFAT" fieldLeft="CADCLI.CODCLI" fieldRight="CADFAT.CODCLI" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="41" tableLeft="TABPBH" tableRight="CADFAT" fieldLeft="TABPBH.MESREF" fieldRight="CADFAT.MESREF" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="21" tableName="CADFAT" fieldName="CODFAT"/>
				<Field id="22" tableName="CADFAT" fieldName="CADFAT.CODCLI" alias="CADFAT_CODCLI"/>
				<Field id="23" tableName="CADFAT" fieldName="DATVNC"/>
				<Field id="24" tableName="CADFAT" fieldName="VALPGT"/>
				<Field id="29" tableName="CADCLI" fieldName="DESCLI"/>
				<Field id="31" tableName="CADFAT" fieldName="DATEMI"/>
				<Field id="39" tableName="TABPBH" fieldName="VALPBH"/>
				<Field id="42" tableName="CADFAT" fieldName="EXPORT"/>
				<Field id="43" tableName="CADFAT" fieldName="CADFAT.MESREF" alias="CADFAT_MESREF"/>
				<Field id="45" tableName="CADFAT" fieldName="DATPGT"/>
				<Field id="57" tableName="CADFAT" fieldName="VALCOB"/>
				<Field id="58" tableName="CADFAT" fieldName="VALJUR"/>
				<Field id="59" tableName="CADFAT" fieldName="VALMUL"/>
				<Field id="94" tableName="CADCLI" fieldName="ESFERA"/>
				<Field id="98" tableName="CADFAT" fieldName="ISSQN"/>
				<Field id="102" tableName="CADFAT" fieldName="RET_INSS"/>
				<Field id="106" tableName="CADFAT" fieldName="VALFAT"/>
			</Fields>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions>
				<TableParameter id="47" conditionType="Parameter" useIsNull="False" field="CADFAT.CODFAT" dataType="Text" parameterType="URL" parameterSource="CODFAT" searchConditionType="Equal" logicOperator="And"/>
			</UConditions>
			<UFormElements>
				<CustomParameter id="54" field="DATPGT" dataType="Date" parameterType="Control" parameterSource="DATPGT" omitIfEmpty="True"/>
				<CustomParameter id="55" field="VALPGT" dataType="Float" parameterType="Control" parameterSource="VALPGT" omitIfEmpty="True"/>
				<CustomParameter id="116" field="VALCOB" dataType="Float" parameterType="Control" omitIfEmpty="True" parameterSource="VALCOB"/>
				<CustomParameter id="117" field="VALJUR" dataType="Float" parameterType="Control" omitIfEmpty="True" parameterSource="VALJUR"/>
				<CustomParameter id="118" field="VALMUL" dataType="Float" parameterType="Control" omitIfEmpty="True" parameterSource="VALMUL"/>
			</UFormElements>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="60" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="20" name="Servicos" wizardGridType="Tabular" wizardAllowSorting="False" wizardSortingType="Simple" wizardUsePageScroller="False" wizardAllowInsert="False" wizardAltRecord="False" wizardRecordSeparator="False" connection="Faturar" dataSource="MOVFAT, TABPBH, SUBSER" activeCollection="TableParameters" pasteAsReplace="pasteAsReplace">
			<Components>
				<Label id="61" fieldSourceType="DBColumn" dataType="Text" html="False" name="SUBRED" fieldSource="SUBRED">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="62" fieldSourceType="DBColumn" dataType="Float" html="False" name="QTDMED" fieldSource="QTDMED">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="138"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Hidden id="139" fieldSourceType="DBColumn" dataType="Float" name="hdd_QTDMED" fieldSource="QTDMED">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<Label id="63" fieldSourceType="DBColumn" dataType="Text" html="False" name="EQUPBH_X_VALPBH">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="83" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Hidden id="84" fieldSourceType="DBColumn" dataType="Float" name="EQUPBH" fieldSource="EQUPBH">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<Label id="64" fieldSourceType="DBColumn" dataType="Text" html="False" name="EQUPBH_X_VALPBH_X_QTMED">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="86"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Hidden id="85" fieldSourceType="DBColumn" dataType="Float" name="VALPBH" fieldSource="TABPBH_VALPBH">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<Label id="104" fieldSourceType="DBColumn" dataType="Text" html="False" name="TOT_FAT">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="105"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="129" fieldSourceType="DBColumn" dataType="Text" html="False" name="lbl_Inss">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="130"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="131" fieldSourceType="DBColumn" dataType="Text" html="False" name="lbl_Issqn">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="67" conditionType="Parameter" useIsNull="False" field="MOVFAT.CODFAT" dataType="Text" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="CODFAT"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="66" tableName="MOVFAT" schemaName="SL005V3" posLeft="10" posTop="10" posWidth="115" posHeight="252"/>
				<JoinTable id="68" tableName="TABPBH" schemaName="SL005V3" posLeft="146" posTop="220" posWidth="183" posHeight="168"/>
				<JoinTable id="70" tableName="SUBSER" schemaName="SL005V3" posLeft="212" posTop="58" posWidth="115" posHeight="152"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="71" tableLeft="TABPBH" tableRight="MOVFAT" fieldLeft="TABPBH.MESREF" fieldRight="MOVFAT.MESREF" joinType="inner" conditionType="Equal"/>
				<JoinTable2 id="72" tableLeft="SUBSER" tableRight="MOVFAT" fieldLeft="SUBSER.SUBSER" fieldRight="MOVFAT.SUBSER" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="75" tableName="SUBSER" fieldName="SUBRED"/>
				<Field id="77" tableName="TABPBH" fieldName="TABPBH.VALPBH" alias="TABPBH_VALPBH"/>
				<Field id="80" tableName="MOVFAT" fieldName="QTDMED"/>
				<Field id="81" tableName="MOVFAT" fieldName="EQUPBH"/>
				<Field id="82" tableName="MOVFAT" fieldName="VALSER"/>
				<Field id="87" tableName="MOVFAT" fieldName="MOVFAT.MESREF" alias="MOVFAT_MESREF"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes>
				<Attribute id="65" name="RowNumber" sourceType="Expression"/>
			</Attributes>
			<Features/>
		</Grid>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="PHPTemplates" name="PagtoFatu_Dados_Canc_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Code" language="PHPTemplates" name="PagtoFatu_Dados_Canc.php" forShow="True" url="PagtoFatu_Dados_Canc.php" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="122" groupID="1"/>
		<Group id="123" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="147"/>
			</Actions>
		</Event>
	</Events>
</Page>
