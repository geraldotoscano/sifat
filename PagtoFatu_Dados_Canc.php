<?php
//Include Common Files @1-ADFB858B
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "PagtoFatu_Dados_Canc.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordPagto { //Pagto Class @4-9E53166C

//Variables @4-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @4-6E073CF1
    function clsRecordPagto($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record Pagto/Error";
        $this->DataSource = new clsPagtoDataSource($this);
        $this->ds = & $this->DataSource;
        $this->UpdateAllowed = true;
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "Pagto";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->Label1 = new clsControl(ccsLabel, "Label1", "Label1", ccsText, "", CCGetRequestParam("Label1", $Method, NULL), $this);
            $this->Label2 = new clsControl(ccsLabel, "Label2", "Label2", ccsText, "", CCGetRequestParam("Label2", $Method, NULL), $this);
            $this->CODFAT = new clsControl(ccsTextBox, "CODFAT", "CODFAT", ccsText, "", CCGetRequestParam("CODFAT", $Method, NULL), $this);
            $this->ESFERA = new clsControl(ccsHidden, "ESFERA", "ESFERA", ccsText, "", CCGetRequestParam("ESFERA", $Method, NULL), $this);
            $this->RET_INSS = new clsControl(ccsTextBox, "RET_INSS", "RET_INSS", ccsFloat, "", CCGetRequestParam("RET_INSS", $Method, NULL), $this);
            $this->EXPORT = new clsControl(ccsHidden, "EXPORT", "EXPORT", ccsText, "", CCGetRequestParam("EXPORT", $Method, NULL), $this);
            $this->DESCLI = new clsControl(ccsTextBox, "DESCLI", "DESCLI", ccsText, "", CCGetRequestParam("DESCLI", $Method, NULL), $this);
            $this->DATEMI = new clsControl(ccsTextBox, "DATEMI", "DATEMI", ccsDate, array("dd", "/", "mm", "/", "yyyy"), CCGetRequestParam("DATEMI", $Method, NULL), $this);
            $this->Hidd_RET_INSS = new clsControl(ccsHidden, "Hidd_RET_INSS", "Hidd_RET_INSS", ccsFloat, "", CCGetRequestParam("Hidd_RET_INSS", $Method, NULL), $this);
            $this->DATVNC = new clsControl(ccsTextBox, "DATVNC", "DATVNC", ccsDate, array("dd", "/", "mm", "/", "yyyy"), CCGetRequestParam("DATVNC", $Method, NULL), $this);
            $this->VALCOB = new clsControl(ccsHidden, "VALCOB", "VALCOB", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), CCGetRequestParam("VALCOB", $Method, NULL), $this);
            $this->VALPBH = new clsControl(ccsTextBox, "VALPBH", "VALPBH", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), CCGetRequestParam("VALPBH", $Method, NULL), $this);
            $this->lblJa_Exportada = new clsControl(ccsLabel, "lblJa_Exportada", "lblJa_Exportada", ccsText, "", CCGetRequestParam("lblJa_Exportada", $Method, NULL), $this);
            $this->VALFAT = new clsControl(ccsTextBox, "VALFAT", "VALFAT", ccsFloat, "", CCGetRequestParam("VALFAT", $Method, NULL), $this);
            $this->VALMUL = new clsControl(ccsHidden, "VALMUL", "VALMUL", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), CCGetRequestParam("VALMUL", $Method, NULL), $this);
            $this->MESREF = new clsControl(ccsTextBox, "MESREF", "MESREF", ccsText, "", CCGetRequestParam("MESREF", $Method, NULL), $this);
            $this->DATPGT = new clsControl(ccsTextBox, "DATPGT", "Pagamento", ccsDate, array("dd", "/", "mm", "/", "yyyy"), CCGetRequestParam("DATPGT", $Method, NULL), $this);
            $this->DatePicker_DATPGT1 = new clsDatePicker("DatePicker_DATPGT1", "Pagto", "DATPGT", $this);
            $this->Hidd_VALFAT = new clsControl(ccsHidden, "Hidd_VALFAT", "Hidd_VALFAT", ccsFloat, "", CCGetRequestParam("Hidd_VALFAT", $Method, NULL), $this);
            $this->lbl_Dias_Atraso = new clsControl(ccsTextBox, "lbl_Dias_Atraso", "lbl_Dias_Atraso", ccsText, "", CCGetRequestParam("lbl_Dias_Atraso", $Method, NULL), $this);
            $this->Label3 = new clsControl(ccsLabel, "Label3", "Label3", ccsText, "", CCGetRequestParam("Label3", $Method, NULL), $this);
            $this->VALPGT = new clsControl(ccsTextBox, "VALPGT", "VALPGT", ccsFloat, "", CCGetRequestParam("VALPGT", $Method, NULL), $this);
            $this->VALJUR = new clsControl(ccsTextBox, "VALJUR", "VALJUR", ccsFloat, "", CCGetRequestParam("VALJUR", $Method, NULL), $this);
            $this->taxa_juros = new clsControl(ccsHidden, "taxa_juros", "taxa_juros", ccsFloat, "", CCGetRequestParam("taxa_juros", $Method, NULL), $this);
            $this->ISSQN = new clsControl(ccsTextBox, "ISSQN", "ISSQN", ccsFloat, array(False, 2, Null, Null, False, "", "", 1, True, ""), CCGetRequestParam("ISSQN", $Method, NULL), $this);
            $this->CODTIPCANC = new clsControl(ccsListBox, "CODTIPCANC", "CODTIPCANC", ccsText, "", CCGetRequestParam("CODTIPCANC", $Method, NULL), $this);
            $this->CODTIPCANC->DSType = dsTable;
            $this->CODTIPCANC->DataSource = new clsDBFaturar();
            $this->CODTIPCANC->ds = & $this->CODTIPCANC->DataSource;
            $this->CODTIPCANC->DataSource->SQL = "SELECT * \n" .
"FROM TIPCANC {SQL_Where} {SQL_OrderBy}";
            list($this->CODTIPCANC->BoundColumn, $this->CODTIPCANC->TextColumn, $this->CODTIPCANC->DBFormat) = array("CODTIPCANC", "DESCTIPCANC", "");
            $this->CODTIPCANC->Required = true;
            $this->DESCCANC = new clsControl(ccsTextArea, "DESCCANC", "Descri��o cancelamento", ccsText, "", CCGetRequestParam("DESCCANC", $Method, NULL), $this);
            $this->DESCCANC->Required = true;
            $this->Button_Update = new clsButton("Button_Update", $Method, $this);
            $this->Button_Cancel = new clsButton("Button_Cancel", $Method, $this);
            if(!$this->FormSubmitted) {
                if(!is_array($this->VALMUL->Value) && !strlen($this->VALMUL->Value) && $this->VALMUL->Value !== false)
                    $this->VALMUL->SetText(0);
            }
            if(!is_array($this->lblJa_Exportada->Value) && !strlen($this->lblJa_Exportada->Value) && $this->lblJa_Exportada->Value !== false)
                $this->lblJa_Exportada->SetText('J� Exportada?');
        }
    }
//End Class_Initialize Event

//Initialize Method @4-656E5310
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->DataSource->Parameters["urlCODFAT"] = CCGetFromGet("CODFAT", NULL);
    }
//End Initialize Method

//Validate Method @4-789D0F35
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->CODFAT->Validate() && $Validation);
        $Validation = ($this->ESFERA->Validate() && $Validation);
        $Validation = ($this->RET_INSS->Validate() && $Validation);
        $Validation = ($this->EXPORT->Validate() && $Validation);
        $Validation = ($this->DESCLI->Validate() && $Validation);
        $Validation = ($this->DATEMI->Validate() && $Validation);
        $Validation = ($this->Hidd_RET_INSS->Validate() && $Validation);
        $Validation = ($this->DATVNC->Validate() && $Validation);
        $Validation = ($this->VALCOB->Validate() && $Validation);
        $Validation = ($this->VALPBH->Validate() && $Validation);
        $Validation = ($this->VALFAT->Validate() && $Validation);
        $Validation = ($this->VALMUL->Validate() && $Validation);
        $Validation = ($this->MESREF->Validate() && $Validation);
        $Validation = ($this->DATPGT->Validate() && $Validation);
        $Validation = ($this->Hidd_VALFAT->Validate() && $Validation);
        $Validation = ($this->lbl_Dias_Atraso->Validate() && $Validation);
        $Validation = ($this->VALPGT->Validate() && $Validation);
        $Validation = ($this->VALJUR->Validate() && $Validation);
        $Validation = ($this->taxa_juros->Validate() && $Validation);
        $Validation = ($this->ISSQN->Validate() && $Validation);
        $Validation = ($this->CODTIPCANC->Validate() && $Validation);
        $Validation = ($this->DESCCANC->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->CODFAT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->ESFERA->Errors->Count() == 0);
        $Validation =  $Validation && ($this->RET_INSS->Errors->Count() == 0);
        $Validation =  $Validation && ($this->EXPORT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DESCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DATEMI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Hidd_RET_INSS->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DATVNC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->VALCOB->Errors->Count() == 0);
        $Validation =  $Validation && ($this->VALPBH->Errors->Count() == 0);
        $Validation =  $Validation && ($this->VALFAT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->VALMUL->Errors->Count() == 0);
        $Validation =  $Validation && ($this->MESREF->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DATPGT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Hidd_VALFAT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->lbl_Dias_Atraso->Errors->Count() == 0);
        $Validation =  $Validation && ($this->VALPGT->Errors->Count() == 0);
        $Validation =  $Validation && ($this->VALJUR->Errors->Count() == 0);
        $Validation =  $Validation && ($this->taxa_juros->Errors->Count() == 0);
        $Validation =  $Validation && ($this->ISSQN->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CODTIPCANC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->DESCCANC->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @4-81954698
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->Label1->Errors->Count());
        $errors = ($errors || $this->Label2->Errors->Count());
        $errors = ($errors || $this->CODFAT->Errors->Count());
        $errors = ($errors || $this->ESFERA->Errors->Count());
        $errors = ($errors || $this->RET_INSS->Errors->Count());
        $errors = ($errors || $this->EXPORT->Errors->Count());
        $errors = ($errors || $this->DESCLI->Errors->Count());
        $errors = ($errors || $this->DATEMI->Errors->Count());
        $errors = ($errors || $this->Hidd_RET_INSS->Errors->Count());
        $errors = ($errors || $this->DATVNC->Errors->Count());
        $errors = ($errors || $this->VALCOB->Errors->Count());
        $errors = ($errors || $this->VALPBH->Errors->Count());
        $errors = ($errors || $this->lblJa_Exportada->Errors->Count());
        $errors = ($errors || $this->VALFAT->Errors->Count());
        $errors = ($errors || $this->VALMUL->Errors->Count());
        $errors = ($errors || $this->MESREF->Errors->Count());
        $errors = ($errors || $this->DATPGT->Errors->Count());
        $errors = ($errors || $this->DatePicker_DATPGT1->Errors->Count());
        $errors = ($errors || $this->Hidd_VALFAT->Errors->Count());
        $errors = ($errors || $this->lbl_Dias_Atraso->Errors->Count());
        $errors = ($errors || $this->Label3->Errors->Count());
        $errors = ($errors || $this->VALPGT->Errors->Count());
        $errors = ($errors || $this->VALJUR->Errors->Count());
        $errors = ($errors || $this->taxa_juros->Errors->Count());
        $errors = ($errors || $this->ISSQN->Errors->Count());
        $errors = ($errors || $this->CODTIPCANC->Errors->Count());
        $errors = ($errors || $this->DESCCANC->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @4-B01DFF1A
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->DataSource->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->DataSource->AllParametersSet;
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = $this->EditMode ? "Button_Update" : "Button_Update";
            if($this->Button_Update->Pressed) {
                $this->PressedButton = "Button_Update";
            } else if($this->Button_Cancel->Pressed) {
                $this->PressedButton = "Button_Cancel";
            }
        }
        $Redirect = "default.php" . "?" . CCGetQueryString("QueryString", array("ccsForm", "opcao"));
        if($this->PressedButton == "Button_Cancel") {
            if(!CCGetEvent($this->Button_Cancel->CCSEvents, "OnClick", $this->Button_Cancel)) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button_Update") {
                if(!CCGetEvent($this->Button_Update->CCSEvents, "OnClick", $this->Button_Update)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
        if ($Redirect)
            $this->DataSource->close();
    }
//End Operation Method

//UpdateRow Method @4-01C294B1
    function UpdateRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeUpdate", $this);
        if(!$this->UpdateAllowed) return false;
        $this->DataSource->DATPGT->SetValue($this->DATPGT->GetValue(true));
        $this->DataSource->VALPGT->SetValue($this->VALPGT->GetValue(true));
        $this->DataSource->VALCOB->SetValue($this->VALCOB->GetValue(true));
        $this->DataSource->VALJUR->SetValue($this->VALJUR->GetValue(true));
        $this->DataSource->VALMUL->SetValue($this->VALMUL->GetValue(true));
        $this->DataSource->Update();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterUpdate", $this);
        return (!$this->CheckErrors());
    }
//End UpdateRow Method

//Show Method @4-12D5A208
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->CODTIPCANC->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->DataSource->Errors->Count()){
                $this->Errors->AddErrors($this->DataSource->Errors);
                $this->DataSource->Errors->clear();
            }
            $this->DataSource->Open();
            if($this->DataSource->Errors->Count() == 0 && $this->DataSource->next_record()) {
                $this->DataSource->SetValues();
                if(!$this->FormSubmitted){
                    $this->CODFAT->SetValue($this->DataSource->CODFAT->GetValue());
                    $this->ESFERA->SetValue($this->DataSource->ESFERA->GetValue());
                    $this->RET_INSS->SetValue($this->DataSource->RET_INSS->GetValue());
                    $this->EXPORT->SetValue($this->DataSource->EXPORT->GetValue());
                    $this->DESCLI->SetValue($this->DataSource->DESCLI->GetValue());
                    $this->DATEMI->SetValue($this->DataSource->DATEMI->GetValue());
                    $this->Hidd_RET_INSS->SetValue($this->DataSource->Hidd_RET_INSS->GetValue());
                    $this->DATVNC->SetValue($this->DataSource->DATVNC->GetValue());
                    $this->VALCOB->SetValue($this->DataSource->VALCOB->GetValue());
                    $this->VALPBH->SetValue($this->DataSource->VALPBH->GetValue());
                    $this->VALFAT->SetValue($this->DataSource->VALFAT->GetValue());
                    $this->VALMUL->SetValue($this->DataSource->VALMUL->GetValue());
                    $this->MESREF->SetValue($this->DataSource->MESREF->GetValue());
                    $this->DATPGT->SetValue($this->DataSource->DATPGT->GetValue());
                    $this->Hidd_VALFAT->SetValue($this->DataSource->Hidd_VALFAT->GetValue());
                    $this->VALPGT->SetValue($this->DataSource->VALPGT->GetValue());
                    $this->VALJUR->SetValue($this->DataSource->VALJUR->GetValue());
                    $this->ISSQN->SetValue($this->DataSource->ISSQN->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->Label1->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Label2->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODFAT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->ESFERA->Errors->ToString());
            $Error = ComposeStrings($Error, $this->RET_INSS->Errors->ToString());
            $Error = ComposeStrings($Error, $this->EXPORT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DESCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DATEMI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Hidd_RET_INSS->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DATVNC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->VALCOB->Errors->ToString());
            $Error = ComposeStrings($Error, $this->VALPBH->Errors->ToString());
            $Error = ComposeStrings($Error, $this->lblJa_Exportada->Errors->ToString());
            $Error = ComposeStrings($Error, $this->VALFAT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->VALMUL->Errors->ToString());
            $Error = ComposeStrings($Error, $this->MESREF->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DATPGT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DatePicker_DATPGT1->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Hidd_VALFAT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->lbl_Dias_Atraso->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Label3->Errors->ToString());
            $Error = ComposeStrings($Error, $this->VALPGT->Errors->ToString());
            $Error = ComposeStrings($Error, $this->VALJUR->Errors->ToString());
            $Error = ComposeStrings($Error, $this->taxa_juros->Errors->ToString());
            $Error = ComposeStrings($Error, $this->ISSQN->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CODTIPCANC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DESCCANC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataSource->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->Label1->Show();
        $this->Label2->Show();
        $this->CODFAT->Show();
        $this->ESFERA->Show();
        $this->RET_INSS->Show();
        $this->EXPORT->Show();
        $this->DESCLI->Show();
        $this->DATEMI->Show();
        $this->Hidd_RET_INSS->Show();
        $this->DATVNC->Show();
        $this->VALCOB->Show();
        $this->VALPBH->Show();
        $this->lblJa_Exportada->Show();
        $this->VALFAT->Show();
        $this->VALMUL->Show();
        $this->MESREF->Show();
        $this->DATPGT->Show();
        $this->DatePicker_DATPGT1->Show();
        $this->Hidd_VALFAT->Show();
        $this->lbl_Dias_Atraso->Show();
        $this->Label3->Show();
        $this->VALPGT->Show();
        $this->VALJUR->Show();
        $this->taxa_juros->Show();
        $this->ISSQN->Show();
        $this->CODTIPCANC->Show();
        $this->DESCCANC->Show();
        $this->Button_Update->Show();
        $this->Button_Cancel->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

} //End Pagto Class @4-FCB6E20C

class clsPagtoDataSource extends clsDBFaturar {  //PagtoDataSource Class @4-8FBEB182

//DataSource Variables @4-02C737C0
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $UpdateParameters;
    public $wp;
    public $AllParametersSet;

    public $UpdateFields = array();

    // Datasource fields
    public $Label1;
    public $Label2;
    public $CODFAT;
    public $ESFERA;
    public $RET_INSS;
    public $EXPORT;
    public $DESCLI;
    public $DATEMI;
    public $Hidd_RET_INSS;
    public $DATVNC;
    public $VALCOB;
    public $VALPBH;
    public $lblJa_Exportada;
    public $VALFAT;
    public $VALMUL;
    public $MESREF;
    public $DATPGT;
    public $Hidd_VALFAT;
    public $lbl_Dias_Atraso;
    public $Label3;
    public $VALPGT;
    public $VALJUR;
    public $taxa_juros;
    public $ISSQN;
    public $CODTIPCANC;
    public $DESCCANC;
//End DataSource Variables

//DataSourceClass_Initialize Event @4-4A04B2BA
    function clsPagtoDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Record Pagto/Error";
        $this->Initialize();
        $this->Label1 = new clsField("Label1", ccsText, "");
        $this->Label2 = new clsField("Label2", ccsText, "");
        $this->CODFAT = new clsField("CODFAT", ccsText, "");
        $this->ESFERA = new clsField("ESFERA", ccsText, "");
        $this->RET_INSS = new clsField("RET_INSS", ccsFloat, "");
        $this->EXPORT = new clsField("EXPORT", ccsText, "");
        $this->DESCLI = new clsField("DESCLI", ccsText, "");
        $this->DATEMI = new clsField("DATEMI", ccsDate, $this->DateFormat);
        $this->Hidd_RET_INSS = new clsField("Hidd_RET_INSS", ccsFloat, "");
        $this->DATVNC = new clsField("DATVNC", ccsDate, $this->DateFormat);
        $this->VALCOB = new clsField("VALCOB", ccsFloat, "");
        $this->VALPBH = new clsField("VALPBH", ccsFloat, "");
        $this->lblJa_Exportada = new clsField("lblJa_Exportada", ccsText, "");
        $this->VALFAT = new clsField("VALFAT", ccsFloat, "");
        $this->VALMUL = new clsField("VALMUL", ccsFloat, "");
        $this->MESREF = new clsField("MESREF", ccsText, "");
        $this->DATPGT = new clsField("DATPGT", ccsDate, $this->DateFormat);
        $this->Hidd_VALFAT = new clsField("Hidd_VALFAT", ccsFloat, "");
        $this->lbl_Dias_Atraso = new clsField("lbl_Dias_Atraso", ccsText, "");
        $this->Label3 = new clsField("Label3", ccsText, "");
        $this->VALPGT = new clsField("VALPGT", ccsFloat, "");
        $this->VALJUR = new clsField("VALJUR", ccsFloat, "");
        $this->taxa_juros = new clsField("taxa_juros", ccsFloat, "");
        $this->ISSQN = new clsField("ISSQN", ccsFloat, "");
        $this->CODTIPCANC = new clsField("CODTIPCANC", ccsText, "");
        $this->DESCCANC = new clsField("DESCCANC", ccsText, "");

        $this->UpdateFields["DATPGT"] = array("Name" => "DATPGT", "Value" => "", "DataType" => ccsDate, "OmitIfEmpty" => 1);
        $this->UpdateFields["VALPGT"] = array("Name" => "VALPGT", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->UpdateFields["VALCOB"] = array("Name" => "VALCOB", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->UpdateFields["VALJUR"] = array("Name" => "VALJUR", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
        $this->UpdateFields["VALMUL"] = array("Name" => "VALMUL", "Value" => "", "DataType" => ccsFloat, "OmitIfEmpty" => 1);
    }
//End DataSourceClass_Initialize Event

//Prepare Method @4-6300D955
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlCODFAT", ccsText, "", "", $this->Parameters["urlCODFAT"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "CADFAT.CODFAT", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = "( CADFAT.VALPGT = 0 )";
        $this->Where = $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]);
        $this->Where = $this->wp->opAND(false, "( (CADCLI.CODCLI = CADFAT.CODCLI) AND (TABPBH.MESREF = CADFAT.MESREF) )", $this->Where);
    }
//End Prepare Method

//Open Method @4-182C00A7
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->SQL = "SELECT CODFAT, CADFAT.CODCLI AS CADFAT_CODCLI, DATVNC, VALPGT, DESCLI, DATEMI, VALPBH, EXPORT, CADFAT.MESREF AS CADFAT_MESREF, DATPGT,\n\n" .
        "VALCOB, VALJUR, VALMUL, ESFERA, ISSQN, RET_INSS, VALFAT \n\n" .
        "FROM CADFAT,\n\n" .
        "CADCLI,\n\n" .
        "TABPBH {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
    }
//End Open Method

//SetValues Method @4-C255CA05
    function SetValues()
    {
        $this->CODFAT->SetDBValue($this->f("CODFAT"));
        $this->ESFERA->SetDBValue($this->f("ESFERA"));
        $this->RET_INSS->SetDBValue(trim($this->f("RET_INSS")));
        $this->EXPORT->SetDBValue($this->f("EXPORT"));
        $this->DESCLI->SetDBValue($this->f("DESCLI"));
        $this->DATEMI->SetDBValue(trim($this->f("DATEMI")));
        $this->Hidd_RET_INSS->SetDBValue(trim($this->f("RET_INSS")));
        $this->DATVNC->SetDBValue(trim($this->f("DATVNC")));
        $this->VALCOB->SetDBValue(trim($this->f("VALCOB")));
        $this->VALPBH->SetDBValue(trim($this->f("VALPBH")));
        $this->VALFAT->SetDBValue(trim($this->f("VALFAT")));
        $this->VALMUL->SetDBValue(trim($this->f("VALMUL")));
        $this->MESREF->SetDBValue($this->f("CADFAT_MESREF"));
        $this->DATPGT->SetDBValue(trim($this->f("DATPGT")));
        $this->Hidd_VALFAT->SetDBValue(trim($this->f("VALFAT")));
        $this->VALPGT->SetDBValue(trim($this->f("VALPGT")));
        $this->VALJUR->SetDBValue(trim($this->f("VALJUR")));
        $this->ISSQN->SetDBValue(trim($this->f("ISSQN")));
    }
//End SetValues Method

//Update Method @4-8F34E84C
    function Update()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->cp["DATPGT"] = new clsSQLParameter("ctrlDATPGT", ccsDate, $DefaultDateFormat, $this->DateFormat, $this->DATPGT->GetValue(true), "", false, $this->ErrorBlock);
        $this->cp["VALPGT"] = new clsSQLParameter("ctrlVALPGT", ccsFloat, "", "", $this->VALPGT->GetValue(true), "", false, $this->ErrorBlock);
        $this->cp["VALCOB"] = new clsSQLParameter("ctrlVALCOB", ccsFloat, "", "", $this->VALCOB->GetValue(true), "", false, $this->ErrorBlock);
        $this->cp["VALJUR"] = new clsSQLParameter("ctrlVALJUR", ccsFloat, "", "", $this->VALJUR->GetValue(true), "", false, $this->ErrorBlock);
        $this->cp["VALMUL"] = new clsSQLParameter("ctrlVALMUL", ccsFloat, "", "", $this->VALMUL->GetValue(true), "", false, $this->ErrorBlock);
        $wp = new clsSQLParameters($this->ErrorBlock);
        $wp->AddParameter("1", "urlCODFAT", ccsText, "", "", CCGetFromGet("CODFAT", NULL), "", false);
        if(!$wp->AllParamsSet()) {
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        }
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildUpdate", $this->Parent);
        if (!is_null($this->cp["DATPGT"]->GetValue()) and !strlen($this->cp["DATPGT"]->GetText()) and !is_bool($this->cp["DATPGT"]->GetValue())) 
            $this->cp["DATPGT"]->SetValue($this->DATPGT->GetValue(true));
        if (!is_null($this->cp["VALPGT"]->GetValue()) and !strlen($this->cp["VALPGT"]->GetText()) and !is_bool($this->cp["VALPGT"]->GetValue())) 
            $this->cp["VALPGT"]->SetValue($this->VALPGT->GetValue(true));
        if (!is_null($this->cp["VALCOB"]->GetValue()) and !strlen($this->cp["VALCOB"]->GetText()) and !is_bool($this->cp["VALCOB"]->GetValue())) 
            $this->cp["VALCOB"]->SetValue($this->VALCOB->GetValue(true));
        if (!is_null($this->cp["VALJUR"]->GetValue()) and !strlen($this->cp["VALJUR"]->GetText()) and !is_bool($this->cp["VALJUR"]->GetValue())) 
            $this->cp["VALJUR"]->SetValue($this->VALJUR->GetValue(true));
        if (!is_null($this->cp["VALMUL"]->GetValue()) and !strlen($this->cp["VALMUL"]->GetText()) and !is_bool($this->cp["VALMUL"]->GetValue())) 
            $this->cp["VALMUL"]->SetValue($this->VALMUL->GetValue(true));
        $wp->Criterion[1] = $wp->Operation(opEqual, "CADFAT.CODFAT", $wp->GetDBValue("1"), $this->ToSQL($wp->GetDBValue("1"), ccsText),false);
        $Where = 
             $wp->Criterion[1];
        $this->UpdateFields["DATPGT"]["Value"] = $this->cp["DATPGT"]->GetDBValue(true);
        $this->UpdateFields["VALPGT"]["Value"] = $this->cp["VALPGT"]->GetDBValue(true);
        $this->UpdateFields["VALCOB"]["Value"] = $this->cp["VALCOB"]->GetDBValue(true);
        $this->UpdateFields["VALJUR"]["Value"] = $this->cp["VALJUR"]->GetDBValue(true);
        $this->UpdateFields["VALMUL"]["Value"] = $this->cp["VALMUL"]->GetDBValue(true);
        $this->SQL = CCBuildUpdate("CADFAT", $this->UpdateFields, $this);
        $this->SQL = CCBuildSQL($this->SQL, $Where, "");
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteUpdate", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteUpdate", $this->Parent);
        }
    }
//End Update Method

} //End PagtoDataSource Class @4-FCB6E20C

class clsGridServicos { //Servicos class @60-3F5E5C32

//Variables @60-6E51DF5A

    // Public variables
    public $ComponentType = "Grid";
    public $ComponentName;
    public $Visible;
    public $Errors;
    public $ErrorBlock;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $ForceIteration = false;
    public $HasRecord = false;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $RowNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";
    public $Attributes;

    // Grid Controls
    public $StaticControls;
    public $RowControls;
//End Variables

//Class_Initialize Event @60-A3E3C2BD
    function clsGridServicos($RelativePath, & $Parent)
    {
        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->ComponentName = "Servicos";
        $this->Visible = True;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid Servicos";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->DataSource = new clsServicosDataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = 20;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;

        $this->SUBRED = new clsControl(ccsLabel, "SUBRED", "SUBRED", ccsText, "", CCGetRequestParam("SUBRED", ccsGet, NULL), $this);
        $this->QTDMED = new clsControl(ccsLabel, "QTDMED", "QTDMED", ccsFloat, "", CCGetRequestParam("QTDMED", ccsGet, NULL), $this);
        $this->hdd_QTDMED = new clsControl(ccsHidden, "hdd_QTDMED", "hdd_QTDMED", ccsFloat, "", CCGetRequestParam("hdd_QTDMED", ccsGet, NULL), $this);
        $this->EQUPBH_X_VALPBH = new clsControl(ccsLabel, "EQUPBH_X_VALPBH", "EQUPBH_X_VALPBH", ccsText, "", CCGetRequestParam("EQUPBH_X_VALPBH", ccsGet, NULL), $this);
        $this->EQUPBH = new clsControl(ccsHidden, "EQUPBH", "EQUPBH", ccsFloat, "", CCGetRequestParam("EQUPBH", ccsGet, NULL), $this);
        $this->EQUPBH_X_VALPBH_X_QTMED = new clsControl(ccsLabel, "EQUPBH_X_VALPBH_X_QTMED", "EQUPBH_X_VALPBH_X_QTMED", ccsText, "", CCGetRequestParam("EQUPBH_X_VALPBH_X_QTMED", ccsGet, NULL), $this);
        $this->VALPBH = new clsControl(ccsHidden, "VALPBH", "VALPBH", ccsFloat, "", CCGetRequestParam("VALPBH", ccsGet, NULL), $this);
        $this->TOT_FAT = new clsControl(ccsLabel, "TOT_FAT", "TOT_FAT", ccsText, "", CCGetRequestParam("TOT_FAT", ccsGet, NULL), $this);
        $this->lbl_Inss = new clsControl(ccsLabel, "lbl_Inss", "lbl_Inss", ccsText, "", CCGetRequestParam("lbl_Inss", ccsGet, NULL), $this);
        $this->lbl_Issqn = new clsControl(ccsLabel, "lbl_Issqn", "lbl_Issqn", ccsText, "", CCGetRequestParam("lbl_Issqn", ccsGet, NULL), $this);
    }
//End Class_Initialize Event

//Initialize Method @60-90E704C5
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @60-F4E32D7D
    function Show()
    {
        global $Tpl;
        global $CCSLocales;
        if(!$this->Visible) return;

        $this->RowNumber = 0;

        $this->DataSource->Parameters["urlCODFAT"] = CCGetFromGet("CODFAT", NULL);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $this->DataSource->Prepare();
        $this->DataSource->Open();
        $this->HasRecord = $this->DataSource->has_next_record();
        $this->IsEmpty = ! $this->HasRecord;
        $this->Attributes->SetValue("RowNumber", "");
        $this->Attributes->Show();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        if (!$this->IsEmpty) {
            $this->ControlsVisible["SUBRED"] = $this->SUBRED->Visible;
            $this->ControlsVisible["QTDMED"] = $this->QTDMED->Visible;
            $this->ControlsVisible["hdd_QTDMED"] = $this->hdd_QTDMED->Visible;
            $this->ControlsVisible["EQUPBH_X_VALPBH"] = $this->EQUPBH_X_VALPBH->Visible;
            $this->ControlsVisible["EQUPBH"] = $this->EQUPBH->Visible;
            $this->ControlsVisible["EQUPBH_X_VALPBH_X_QTMED"] = $this->EQUPBH_X_VALPBH_X_QTMED->Visible;
            $this->ControlsVisible["VALPBH"] = $this->VALPBH->Visible;
            while ($this->ForceIteration || (($this->RowNumber < $this->PageSize) &&  ($this->HasRecord = $this->DataSource->has_next_record()))) {
                $this->RowNumber++;
                if ($this->HasRecord) {
                    $this->DataSource->next_record();
                    $this->DataSource->SetValues();
                }
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->SUBRED->SetValue($this->DataSource->SUBRED->GetValue());
                $this->QTDMED->SetValue($this->DataSource->QTDMED->GetValue());
                $this->hdd_QTDMED->SetValue($this->DataSource->hdd_QTDMED->GetValue());
                $this->EQUPBH->SetValue($this->DataSource->EQUPBH->GetValue());
                $this->VALPBH->SetValue($this->DataSource->VALPBH->GetValue());
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->SUBRED->Show();
                $this->QTDMED->Show();
                $this->hdd_QTDMED->Show();
                $this->EQUPBH_X_VALPBH->Show();
                $this->EQUPBH->Show();
                $this->EQUPBH_X_VALPBH_X_QTMED->Show();
                $this->VALPBH->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
            }
        }
        else { // Show NoRecords block if no records are found
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->TOT_FAT->Show();
        $this->lbl_Inss->Show();
        $this->lbl_Issqn->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

//GetErrors Method @60-42C12DD7
    function GetErrors()
    {
        $errors = "";
        $errors = ComposeStrings($errors, $this->SUBRED->Errors->ToString());
        $errors = ComposeStrings($errors, $this->QTDMED->Errors->ToString());
        $errors = ComposeStrings($errors, $this->hdd_QTDMED->Errors->ToString());
        $errors = ComposeStrings($errors, $this->EQUPBH_X_VALPBH->Errors->ToString());
        $errors = ComposeStrings($errors, $this->EQUPBH->Errors->ToString());
        $errors = ComposeStrings($errors, $this->EQUPBH_X_VALPBH_X_QTMED->Errors->ToString());
        $errors = ComposeStrings($errors, $this->VALPBH->Errors->ToString());
        $errors = ComposeStrings($errors, $this->Errors->ToString());
        $errors = ComposeStrings($errors, $this->DataSource->Errors->ToString());
        return $errors;
    }
//End GetErrors Method

} //End Servicos Class @60-FCB6E20C

class clsServicosDataSource extends clsDBFaturar {  //ServicosDataSource Class @60-9BE8C17A

//DataSource Variables @60-2090039C
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $CountSQL;
    public $wp;


    // Datasource fields
    public $SUBRED;
    public $QTDMED;
    public $hdd_QTDMED;
    public $EQUPBH;
    public $VALPBH;
//End DataSource Variables

//DataSourceClass_Initialize Event @60-FEF96600
    function clsServicosDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "Grid Servicos";
        $this->Initialize();
        $this->SUBRED = new clsField("SUBRED", ccsText, "");
        $this->QTDMED = new clsField("QTDMED", ccsFloat, "");
        $this->hdd_QTDMED = new clsField("hdd_QTDMED", ccsFloat, "");
        $this->EQUPBH = new clsField("EQUPBH", ccsFloat, "");
        $this->VALPBH = new clsField("VALPBH", ccsFloat, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @60-9E1383D1
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            "");
    }
//End SetOrder Method

//Prepare Method @60-A0EBBB75
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlCODFAT", ccsText, "", "", $this->Parameters["urlCODFAT"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "MOVFAT.CODFAT", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->Where = 
             $this->wp->Criterion[1];
        $this->Where = $this->wp->opAND(false, "( (TABPBH.MESREF = MOVFAT.MESREF) AND (SUBSER.SUBSER = MOVFAT.SUBSER) )", $this->Where);
    }
//End Prepare Method

//Open Method @60-6A0C8F70
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM MOVFAT,\n\n" .
        "TABPBH,\n\n" .
        "SUBSER";
        $this->SQL = "SELECT SUBRED, TABPBH.VALPBH AS TABPBH_VALPBH, QTDMED, EQUPBH, VALSER, MOVFAT.MESREF AS MOVFAT_MESREF \n\n" .
        "FROM MOVFAT,\n\n" .
        "TABPBH,\n\n" .
        "SUBSER {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @60-E4EC7A8D
    function SetValues()
    {
        $this->SUBRED->SetDBValue($this->f("SUBRED"));
        $this->QTDMED->SetDBValue(trim($this->f("QTDMED")));
        $this->hdd_QTDMED->SetDBValue(trim($this->f("QTDMED")));
        $this->EQUPBH->SetDBValue(trim($this->f("EQUPBH")));
        $this->VALPBH->SetDBValue(trim($this->f("TABPBH_VALPBH")));
    }
//End SetValues Method

} //End ServicosDataSource Class @60-FCB6E20C







//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-14A5666B
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "PagtoFatu_Dados_Canc.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-7FACF37D
CCSecurityRedirect("1;3", "");
//End Authenticate User

//Include events file @1-602CD68F
include("./PagtoFatu_Dados_Canc_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-76699CA3
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$Pagto = new clsRecordPagto("", $MainPage);
$Servicos = new clsGridServicos("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->Pagto = & $Pagto;
$MainPage->Servicos = & $Servicos;
$MainPage->rodape = & $rodape;
$Pagto->Initialize();
$Servicos->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-9C774196
$cabec->Operations();
$Pagto->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-1A5970F1
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($Pagto);
    unset($Servicos);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-69F0A57D
$cabec->Show();
$Pagto->Show();
$Servicos->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-4014617A
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($Pagto);
unset($Servicos);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
