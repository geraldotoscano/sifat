<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="12" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="cadcli_CADFAT" wizardCaption=" Cadcli CADFAT " wizardOrientation="Vertical" wizardFormMethod="post" returnPage="PagtoFatu_Dados_Canc_Lote.ccp" pasteAsReplace="pasteAsReplace">
			<Components>
				<ListBox id="52" visible="Yes" fieldSourceType="CodeExpression" sourceType="Table" dataType="Text" returnValueType="Number" name="s_CODCLI" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="CADCLI" boundColumn="CODCLI" textColumn="descricao" required="False" orderBy="DESCLI">
					<Components/>
					<Events>
					</Events>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="54" tableName="CADCLI" posLeft="47" posTop="3" posWidth="115" posHeight="619"/>
					</JoinTables>
					<JoinLinks/>
					<Fields>
						<Field id="55" fieldName="cadcli.descli||' ('||cadcli.codcli||') '||cadcli.desfan" isExpression="True" alias="descricao"/>
						<Field id="56" tableName="CADCLI" fieldName="CODCLI"/>
						<Field id="57" tableName="CADCLI" fieldName="DESCLI"/>
						<Field id="58" tableName="CADCLI" fieldName="DESFAN"/>
						<Field id="59" tableName="CADCLI" fieldName="CODUNI"/>
						<Field id="60" tableName="CADCLI" fieldName="CODDIS"/>
						<Field id="61" tableName="CADCLI" fieldName="CODSET"/>
						<Field id="62" tableName="CADCLI" fieldName="GRPATI"/>
						<Field id="63" tableName="CADCLI" fieldName="SUBATI"/>
						<Field id="64" tableName="CADCLI" fieldName="CODTIP"/>
						<Field id="65" tableName="CADCLI" fieldName="CGCCPF"/>
						<Field id="66" tableName="CADCLI" fieldName="LOGRAD"/>
						<Field id="67" tableName="CADCLI" fieldName="BAIRRO"/>
						<Field id="68" tableName="CADCLI" fieldName="CIDADE"/>
						<Field id="69" tableName="CADCLI" fieldName="ESTADO"/>
						<Field id="70" tableName="CADCLI" fieldName="CODPOS"/>
						<Field id="71" tableName="CADCLI" fieldName="TELEFO"/>
						<Field id="72" tableName="CADCLI" fieldName="TELFAX"/>
						<Field id="73" tableName="CADCLI" fieldName="CONTAT"/>
						<Field id="74" tableName="CADCLI" fieldName="LOGCOB"/>
						<Field id="75" tableName="CADCLI" fieldName="BAICOB"/>
						<Field id="76" tableName="CADCLI" fieldName="CIDCOB"/>
						<Field id="77" tableName="CADCLI" fieldName="ESTCOB"/>
						<Field id="78" tableName="CADCLI" fieldName="POSCOB"/>
						<Field id="79" tableName="CADCLI" fieldName="TELCOB"/>
						<Field id="80" tableName="CADCLI" fieldName="CODSIT"/>
						<Field id="81" tableName="CADCLI" fieldName="CODINC"/>
						<Field id="82" tableName="CADCLI" fieldName="CODALT"/>
						<Field id="83" tableName="CADCLI" fieldName="DATINC"/>
						<Field id="84" tableName="CADCLI" fieldName="DATALT"/>
						<Field id="85" tableName="CADCLI" fieldName="PGTOELET"/>
						<Field id="86" tableName="CADCLI" fieldName="ESFERA"/>
					</Fields>
					<Attributes/>
					<Features/>
				</ListBox>
				<TextBox id="42" visible="Yes" fieldSourceType="CodeExpression" dataType="Text" name="s_DATEMI">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="16" name="DatePicker_s_DATEMI" control="s_DATEMI" wizardSatellite="True" wizardControl="s_DATEMI" wizardDatePickerType="Image" wizardPicture="Styles/Blueprint/Images/DatePicker.gif" style="Styles/Blueprint/Style.css">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
				<TextBox id="15" visible="Yes" fieldSourceType="CodeExpression" dataType="Date" name="s_DATEMI_FIM" wizardCaption="DATEMI" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="40" name="DatePicker_s_DATEMI_FIM1" control="s_DATEMI_FIM" wizardDatePickerType="Image" wizardPicture="Styles/Blueprint/Images/DatePicker.gif" style="Styles/Blueprint/Style.css">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
				<TextBox id="17" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="s_DATVNC" wizardCaption="DATVNC" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="18" name="DatePicker_s_DATVNC" control="s_DATVNC" wizardSatellite="True" wizardControl="s_DATVNC" wizardDatePickerType="Image" wizardPicture="Styles/Blueprint/Images/DatePicker.gif" style="Styles/Blueprint/Style.css">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
				<TextBox id="44" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_DATVNC_FIM">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="45" name="DatePicker_s_DATVNC_FIM1" control="s_DATVNC_FIM" wizardDatePickerType="Image" wizardPicture="Styles/Blueprint/Images/DatePicker.gif" style="Styles/Blueprint/Style.css">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
				<ListBox id="19" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" returnValueType="Number" name="CADFAT_CADCLIOrder" dataSource=";Select Field;Sorter_DESCLI;DESCLI;Sorter_CODFAT;CODFAT;Sorter_DATEMI;DATEMI;Sorter_DATVNC;DATVNC;Sorter_VALFAT;VALFAT" wizardCaption="Sort by">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<ListBox id="20" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" returnValueType="Number" name="CADFAT_CADCLIDir" dataSource=";Select Order;ASC;Ascending;DESC;Descending" wizardCaption=" ">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<Button id="13" urlType="Relative" enableValidation="True" isDefault="True" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<EditableGrid id="101" urlType="Relative" secured="False" emptyRows="0" allowInsert="False" allowUpdate="False" allowDelete="True" validateData="True" preserveParameters="GET" sourceType="Table" defaultPageSize="300" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" connection="Faturar" dataSource="CADFAT" activeCollection="TableParameters" name="CADFAT_CADCLI" pageSizeLimit="100" wizardCaption="List of CADFAT, CADCLI " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAltRecord="False" wizardRecordSeparator="False" wizardNoRecords="Não encontrado." deleteControl="CheckBox_Delete" pasteAsReplace="pasteAsReplace">
			<Components>
				<Sorter id="115" visible="True" name="Sorter_DESCLI" column="DESCLI" wizardCaption="DESCLI" wizardSortingType="SimpleDir" wizardControl="DESCLI">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="116" visible="True" name="Sorter_CODFAT" column="CODFAT" wizardCaption="CODFAT" wizardSortingType="SimpleDir" wizardControl="CODFAT">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="117" visible="True" name="Sorter_DATEMI" column="DATEMI" wizardCaption="DATEMI" wizardSortingType="SimpleDir" wizardControl="DATEMI">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="118" visible="True" name="Sorter_DATVNC" column="DATVNC" wizardCaption="DATVNC" wizardSortingType="SimpleDir" wizardControl="DATVNC">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="119" visible="True" name="Sorter_VALFAT" column="VALFAT" wizardCaption="VALFAT" wizardSortingType="SimpleDir" wizardControl="VALFAT">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<CheckBox id="136" visible="Yes" fieldSourceType="DBColumn" dataType="Boolean" name="CheckBox_All" defaultValue="Checked">
					<Components/>
					<Events>
						<Event name="OnClick" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="137"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</CheckBox>
				<Label id="120" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESCLI" fieldSource="DESCLI" required="False" caption="DESCLI" wizardCaption="DESCLI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="130"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="121" fieldSourceType="DBColumn" dataType="Text" html="False" name="CODFAT" fieldSource="CODFAT" required="False" caption="CODFAT" wizardCaption="CODFAT" wizardSize="6" wizardMaxLength="6" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="122" fieldSourceType="DBColumn" dataType="Date" html="False" name="DATEMI" fieldSource="DATEMI" required="False" caption="DATEMI" wizardCaption="DATEMI" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="123" fieldSourceType="DBColumn" dataType="Date" html="False" name="DATVNC" fieldSource="DATVNC" required="False" caption="DATVNC" wizardCaption="DATVNC" wizardSize="10" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="124" fieldSourceType="DBColumn" dataType="Float" html="False" name="VALFAT" fieldSource="VALFAT" required="False" caption="VALFAT" wizardCaption="VALFAT" wizardSize="12" wizardMaxLength="12" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<CheckBox id="125" visible="Dynamic" fieldSourceType="CodeExpression" dataType="Boolean" name="CheckBox_Delete" checkedValue="true" uncheckedValue="false" wizardCaption="Apagar" wizardAddNbsp="True" defaultValue="Checked">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</CheckBox>
				<Navigator id="126" size="10" type="Centered" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardImagesScheme="Blueprint">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
				<ListBox id="135" visible="Yes" fieldSourceType="CodeExpression" sourceType="Table" dataType="Text" returnValueType="Number" name="TIPO_CANC" wizardEmptyCaption="Select Value" caption="Motivo do cancelamento" dataSource="TIPCANC" boundColumn="CODTIPCANC" textColumn="DESCTIPCANC" required="True" connection="Faturar">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<TextArea id="134" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="DESC_CANC" caption="Descrição (justificativa)">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextArea>
				<Button id="127" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Submit" operation="Submit" wizardCaption="Mudar">
					<Components/>
					<Events>
						<Event name="OnClick" type="Client">
							<Actions>
								<Action actionName="Confirmation Message" actionCategory="General" id="128" message="Confirma o cancelamento das faturas selecionadas?"/>
							</Actions>
						</Event>
						<Event name="OnClick" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="129"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events>
			</Events>
			<TableParameters>
				<TableParameter id="108" conditionType="Expression" useIsNull="False" field="VALPGT" dataType="Float" searchConditionType="NotEqual" parameterType="URL" logicOperator="And" expression="CADFAT.VALPGT=0" leftBrackets="0" rightBrackets="0" parameterSource="0"/>
				<TableParameter id="109" conditionType="Parameter" useIsNull="False" field="DATEMI" dataType="Date" searchConditionType="GreaterThanOrEqual" parameterType="URL" logicOperator="And" parameterSource="s_DATEMI" leftBrackets="0" rightBrackets="0"/>
				<TableParameter id="110" conditionType="Parameter" useIsNull="False" field="DATEMI" dataType="Date" searchConditionType="LessThanOrEqual" parameterType="URL" logicOperator="And" parameterSource="s_DATEMI_FIM" leftBrackets="0" rightBrackets="0"/>
				<TableParameter id="111" conditionType="Parameter" useIsNull="False" field="DATVNC" dataType="Date" searchConditionType="GreaterThanOrEqual" parameterType="URL" logicOperator="And" parameterSource="s_DATVNC" leftBrackets="0" rightBrackets="0"/>
				<TableParameter id="112" conditionType="Parameter" useIsNull="False" field="DATVNC" dataType="Date" searchConditionType="LessThanOrEqual" parameterType="URL" logicOperator="And" parameterSource="s_DATVNC_FIM" leftBrackets="0" rightBrackets="0"/>
				<TableParameter id="113" conditionType="Parameter" useIsNull="False" field="CODCLI" dataType="Text" searchConditionType="EndsWith" parameterType="URL" logicOperator="And" parameterSource="s_CODCLI" leftBrackets="0" rightBrackets="0"/>
			</TableParameters>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables>
				<JoinTable id="103" tableName="CADFAT" schemaName="SL005V3" posLeft="54" posTop="30" posWidth="115" posHeight="518"/>
			</JoinTables>
			<JoinLinks/>
			<Fields/>
			<PKFields>
				<PKField id="114" tableName="CADFAT" fieldName="CODFAT" dataType="Text"/>
			</PKFields>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</EditableGrid>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="PagtoFatu_Dados_Canc_Lote.php" forShow="True" url="PagtoFatu_Dados_Canc_Lote.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="PagtoFatu_Dados_Canc_Lote_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="89"/>
			</Actions>
		</Event>
	</Events>
</Page>
