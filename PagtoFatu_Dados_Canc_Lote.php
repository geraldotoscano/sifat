<?php
//Include Common Files @1-0BEDDABC
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "PagtoFatu_Dados_Canc_Lote.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

$Tabela=0;

//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordcadcli_CADFAT { //cadcli_CADFAT Class @12-0A14EA5E

//Variables @12-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @12-09D219CD
    function clsRecordcadcli_CADFAT($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record cadcli_CADFAT/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "cadcli_CADFAT";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_CODCLI = new clsControl(ccsListBox, "s_CODCLI", "s_CODCLI", ccsText, "", CCGetRequestParam("s_CODCLI", $Method, NULL), $this);
            $this->s_CODCLI->DSType = dsTable;
            $this->s_CODCLI->DataSource = new clsDBFaturar();
            $this->s_CODCLI->ds = & $this->s_CODCLI->DataSource;
            $this->s_CODCLI->DataSource->SQL = "SELECT cadcli.descli||' ('||cadcli.codcli||') '||cadcli.desfan AS descricao, CODCLI, DESCLI, DESFAN, CODUNI, CODDIS, CODSET, GRPATI,\n" .
"SUBATI, CODTIP, CGCCPF, LOGRAD, BAIRRO, CIDADE, ESTADO, CODPOS, TELEFO, TELFAX, CONTAT, LOGCOB, BAICOB, CIDCOB, ESTCOB,\n" .
"POSCOB, TELCOB, CODSIT, CODINC, CODALT, DATINC, DATALT, PGTOELET, ESFERA \n" .
"FROM CADCLI {SQL_Where} {SQL_OrderBy}";
            $this->s_CODCLI->DataSource->Order = "DESCLI";
            list($this->s_CODCLI->BoundColumn, $this->s_CODCLI->TextColumn, $this->s_CODCLI->DBFormat) = array("CODCLI", "descricao", "");
            $this->s_CODCLI->DataSource->Order = "DESCLI";
            $this->s_DATEMI = new clsControl(ccsTextBox, "s_DATEMI", "s_DATEMI", ccsText, "", CCGetRequestParam("s_DATEMI", $Method, NULL), $this);
            $this->DatePicker_s_DATEMI = new clsDatePicker("DatePicker_s_DATEMI", "cadcli_CADFAT", "s_DATEMI", $this);
            $this->s_DATEMI_FIM = new clsControl(ccsTextBox, "s_DATEMI_FIM", "s_DATEMI_FIM", ccsDate, $DefaultDateFormat, CCGetRequestParam("s_DATEMI_FIM", $Method, NULL), $this);
            $this->DatePicker_s_DATEMI_FIM1 = new clsDatePicker("DatePicker_s_DATEMI_FIM1", "cadcli_CADFAT", "s_DATEMI_FIM", $this);
            $this->s_DATVNC = new clsControl(ccsTextBox, "s_DATVNC", "s_DATVNC", ccsDate, $DefaultDateFormat, CCGetRequestParam("s_DATVNC", $Method, NULL), $this);
            $this->DatePicker_s_DATVNC = new clsDatePicker("DatePicker_s_DATVNC", "cadcli_CADFAT", "s_DATVNC", $this);
            $this->s_DATVNC_FIM = new clsControl(ccsTextBox, "s_DATVNC_FIM", "s_DATVNC_FIM", ccsText, "", CCGetRequestParam("s_DATVNC_FIM", $Method, NULL), $this);
            $this->DatePicker_s_DATVNC_FIM1 = new clsDatePicker("DatePicker_s_DATVNC_FIM1", "cadcli_CADFAT", "s_DATVNC_FIM", $this);
            $this->CADFAT_CADCLIOrder = new clsControl(ccsListBox, "CADFAT_CADCLIOrder", "CADFAT_CADCLIOrder", ccsText, "", CCGetRequestParam("CADFAT_CADCLIOrder", $Method, NULL), $this);
            $this->CADFAT_CADCLIOrder->DSType = dsListOfValues;
            $this->CADFAT_CADCLIOrder->Values = array(array("", "Select Field"), array("Sorter_DESCLI", "DESCLI"), array("Sorter_CODFAT", "CODFAT"), array("Sorter_DATEMI", "DATEMI"), array("Sorter_DATVNC", "DATVNC"), array("Sorter_VALFAT", "VALFAT"));
            $this->CADFAT_CADCLIDir = new clsControl(ccsListBox, "CADFAT_CADCLIDir", "CADFAT_CADCLIDir", ccsText, "", CCGetRequestParam("CADFAT_CADCLIDir", $Method, NULL), $this);
            $this->CADFAT_CADCLIDir->DSType = dsListOfValues;
            $this->CADFAT_CADCLIDir->Values = array(array("", "Select Order"), array("ASC", "Ascending"), array("DESC", "Descending"));
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
        }
    }
//End Class_Initialize Event

//Validate Method @12-A569E111
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_CODCLI->Validate() && $Validation);
        $Validation = ($this->s_DATEMI->Validate() && $Validation);
        $Validation = ($this->s_DATEMI_FIM->Validate() && $Validation);
        $Validation = ($this->s_DATVNC->Validate() && $Validation);
        $Validation = ($this->s_DATVNC_FIM->Validate() && $Validation);
        $Validation = ($this->CADFAT_CADCLIOrder->Validate() && $Validation);
        $Validation = ($this->CADFAT_CADCLIDir->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->s_CODCLI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_DATEMI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_DATEMI_FIM->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_DATVNC->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_DATVNC_FIM->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CADFAT_CADCLIOrder->Errors->Count() == 0);
        $Validation =  $Validation && ($this->CADFAT_CADCLIDir->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @12-BC60FDF9
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_CODCLI->Errors->Count());
        $errors = ($errors || $this->s_DATEMI->Errors->Count());
        $errors = ($errors || $this->DatePicker_s_DATEMI->Errors->Count());
        $errors = ($errors || $this->s_DATEMI_FIM->Errors->Count());
        $errors = ($errors || $this->DatePicker_s_DATEMI_FIM1->Errors->Count());
        $errors = ($errors || $this->s_DATVNC->Errors->Count());
        $errors = ($errors || $this->DatePicker_s_DATVNC->Errors->Count());
        $errors = ($errors || $this->s_DATVNC_FIM->Errors->Count());
        $errors = ($errors || $this->DatePicker_s_DATVNC_FIM1->Errors->Count());
        $errors = ($errors || $this->CADFAT_CADCLIOrder->Errors->Count());
        $errors = ($errors || $this->CADFAT_CADCLIDir->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @12-4AD2B84D
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "PagtoFatu_Dados_Canc_Lote.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                $Redirect = "PagtoFatu_Dados_Canc_Lote.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @12-2EABB1EE
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->s_CODCLI->Prepare();
        $this->CADFAT_CADCLIOrder->Prepare();
        $this->CADFAT_CADCLIDir->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->s_CODCLI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_DATEMI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DatePicker_s_DATEMI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_DATEMI_FIM->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DatePicker_s_DATEMI_FIM1->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_DATVNC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DatePicker_s_DATVNC->Errors->ToString());
            $Error = ComposeStrings($Error, $this->s_DATVNC_FIM->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DatePicker_s_DATVNC_FIM1->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CADFAT_CADCLIOrder->Errors->ToString());
            $Error = ComposeStrings($Error, $this->CADFAT_CADCLIDir->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_CODCLI->Show();
        $this->s_DATEMI->Show();
        $this->DatePicker_s_DATEMI->Show();
        $this->s_DATEMI_FIM->Show();
        $this->DatePicker_s_DATEMI_FIM1->Show();
        $this->s_DATVNC->Show();
        $this->DatePicker_s_DATVNC->Show();
        $this->s_DATVNC_FIM->Show();
        $this->DatePicker_s_DATVNC_FIM1->Show();
        $this->CADFAT_CADCLIOrder->Show();
        $this->CADFAT_CADCLIDir->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End cadcli_CADFAT Class @12-FCB6E20C

class clsEditableGridCADFAT_CADCLI { //CADFAT_CADCLI Class @101-9F6A2CB0

//Variables @101-6A3E47CC

    // Public variables
    public $ComponentType = "EditableGrid";
    public $ComponentName;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormParameters;
    public $FormState;
    public $FormEnctype;
    public $CachedColumns;
    public $TotalRows;
    public $UpdatedRows;
    public $EmptyRows;
    public $Visible;
    public $RowsErrors;
    public $ds;
    public $DataSource;
    public $PageSize;
    public $IsEmpty;
    public $SorterName = "";
    public $SorterDirection = "";
    public $PageNumber;
    public $ControlsVisible = array();

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode;
    public $ValidatingControls;
    public $Controls;
    public $ControlsErrors;
    public $RowNumber;
    public $Attributes;

    // Class variables
    public $Sorter_DESCLI;
    public $Sorter_CODFAT;
    public $Sorter_DATEMI;
    public $Sorter_DATVNC;
    public $Sorter_VALFAT;
//End Variables

//Class_Initialize Event @101-C5FA4BA8
    function clsEditableGridCADFAT_CADCLI($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "EditableGrid CADFAT_CADCLI/Error";
        $this->ControlsErrors = array();
        $this->ComponentName = "CADFAT_CADCLI";
        $this->Attributes = new clsAttributes($this->ComponentName . ":");
        $this->CachedColumns["CODFAT"][0] = "CODFAT";
        $this->DataSource = new clsCADFAT_CADCLIDataSource($this);
        $this->ds = & $this->DataSource;
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 300;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: EditableGrid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        if ($this->PageNumber <= 0) $this->PageNumber = 1;

        $this->EmptyRows = 0;
        $this->DeleteAllowed = true;
        $this->ReadAllowed = true;
        if(!$this->Visible) return;

        $CCSForm = CCGetFromGet("ccsForm", "");
        $this->FormEnctype = "application/x-www-form-urlencoded";
        $this->FormSubmitted = ($CCSForm == $this->ComponentName);
        if($this->FormSubmitted) {
            $this->FormState = CCGetFromPost("FormState", "");
            $this->SetFormState($this->FormState);
        } else {
            $this->FormState = "";
        }
        $Method = $this->FormSubmitted ? ccsPost : ccsGet;

        $this->SorterName = CCGetParam("CADFAT_CADCLIOrder", "");
        $this->SorterDirection = CCGetParam("CADFAT_CADCLIDir", "");

        $this->Sorter_DESCLI = new clsSorter($this->ComponentName, "Sorter_DESCLI", $FileName, $this);
        $this->Sorter_CODFAT = new clsSorter($this->ComponentName, "Sorter_CODFAT", $FileName, $this);
        $this->Sorter_DATEMI = new clsSorter($this->ComponentName, "Sorter_DATEMI", $FileName, $this);
        $this->Sorter_DATVNC = new clsSorter($this->ComponentName, "Sorter_DATVNC", $FileName, $this);
        $this->Sorter_VALFAT = new clsSorter($this->ComponentName, "Sorter_VALFAT", $FileName, $this);
        $this->CheckBox_All = new clsControl(ccsCheckBox, "CheckBox_All", "CheckBox_All", ccsBoolean, $CCSLocales->GetFormatInfo("BooleanFormat"), NULL, $this);
        $this->CheckBox_All->CheckedValue = true;
        $this->CheckBox_All->UncheckedValue = false;
        $this->DESCLI = new clsControl(ccsLabel, "DESCLI", "DESCLI", ccsText, "", NULL, $this);
        $this->CODFAT = new clsControl(ccsLabel, "CODFAT", "CODFAT", ccsText, "", NULL, $this);
        $this->DATEMI = new clsControl(ccsLabel, "DATEMI", "DATEMI", ccsDate, $DefaultDateFormat, NULL, $this);
        $this->DATVNC = new clsControl(ccsLabel, "DATVNC", "DATVNC", ccsDate, $DefaultDateFormat, NULL, $this);
        $this->VALFAT = new clsControl(ccsLabel, "VALFAT", "VALFAT", ccsFloat, "", NULL, $this);
        $this->CheckBox_Delete = new clsControl(ccsCheckBox, "CheckBox_Delete", "CheckBox_Delete", ccsBoolean, $CCSLocales->GetFormatInfo("BooleanFormat"), NULL, $this);
        $this->CheckBox_Delete->CheckedValue = true;
        $this->CheckBox_Delete->UncheckedValue = false;
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpCentered, $this);
        $this->TIPO_CANC = new clsControl(ccsListBox, "TIPO_CANC", "Motivo do cancelamento", ccsText, "", NULL, $this);
        $this->TIPO_CANC->DSType = dsTable;
        $this->TIPO_CANC->DataSource = new clsDBFaturar();
        $this->TIPO_CANC->ds = & $this->TIPO_CANC->DataSource;
        $this->TIPO_CANC->DataSource->SQL = "SELECT * \n" .
"FROM TIPCANC {SQL_Where} {SQL_OrderBy}";
        list($this->TIPO_CANC->BoundColumn, $this->TIPO_CANC->TextColumn, $this->TIPO_CANC->DBFormat) = array("CODTIPCANC", "DESCTIPCANC", "");
        $this->TIPO_CANC->Required = true;
        $this->DESC_CANC = new clsControl(ccsTextArea, "DESC_CANC", "Descri��o (justificativa)", ccsText, "", NULL, $this);
        $this->Button_Submit = new clsButton("Button_Submit", $Method, $this);
    }
//End Class_Initialize Event

//Initialize Method @101-C80B06F4
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->DataSource->PageSize = & $this->PageSize;
        $this->DataSource->AbsolutePage = & $this->PageNumber;
        $this->DataSource->SetOrder($this->SorterName, $this->SorterDirection);

        $this->DataSource->Parameters["urls_DATEMI"] = CCGetFromGet("s_DATEMI", NULL);
        $this->DataSource->Parameters["urls_DATEMI_FIM"] = CCGetFromGet("s_DATEMI_FIM", NULL);
        $this->DataSource->Parameters["urls_DATVNC"] = CCGetFromGet("s_DATVNC", NULL);
        $this->DataSource->Parameters["urls_DATVNC_FIM"] = CCGetFromGet("s_DATVNC_FIM", NULL);
        $this->DataSource->Parameters["urls_CODCLI"] = CCGetFromGet("s_CODCLI", NULL);
    }
//End Initialize Method

//GetFormParameters Method @101-5BC48FB4
    function GetFormParameters()
    {
        for($RowNumber = 1; $RowNumber <= $this->TotalRows; $RowNumber++)
        {
            $this->FormParameters["CheckBox_Delete"][$RowNumber] = CCGetFromPost("CheckBox_Delete_" . $RowNumber, NULL);
        }
    }
//End GetFormParameters Method

//Validate Method @101-D6A9CF11
    function Validate()
    {
        $Validation = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);

        for($this->RowNumber = 1; $this->RowNumber <= $this->TotalRows; $this->RowNumber++)
        {
            $this->DataSource->CachedColumns["CODFAT"] = $this->CachedColumns["CODFAT"][$this->RowNumber];
            $this->DataSource->CurrentRow = $this->RowNumber;
            $this->CheckBox_Delete->SetText($this->FormParameters["CheckBox_Delete"][$this->RowNumber], $this->RowNumber);
            if ($this->UpdatedRows >= $this->RowNumber) {
                if(!$this->CheckBox_Delete->Value)
                    $Validation = ($this->ValidateRow() && $Validation);
            }
            else if($this->CheckInsert())
            {
                $Validation = ($this->ValidateRow() && $Validation);
            }
        }
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//ValidateRow Method @101-213646E1
    function ValidateRow()
    {
        global $CCSLocales;
        $this->CheckBox_Delete->Validate();
        $this->RowErrors = new clsErrors();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidateRow", $this);
        $errors = "";
        $errors = ComposeStrings($errors, $this->CheckBox_Delete->Errors->ToString());
        $this->CheckBox_Delete->Errors->Clear();
        $errors = ComposeStrings($errors, $this->RowErrors->ToString());
        $this->RowsErrors[$this->RowNumber] = $errors;
        return $errors != "" ? 0 : 1;
    }
//End ValidateRow Method

//CheckInsert Method @101-FC0A7F41
    function CheckInsert()
    {
        $filed = false;
        return $filed;
    }
//End CheckInsert Method

//CheckErrors Method @101-F5A3B433
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->DataSource->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @101-909F269B
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->DataSource->Prepare();
        if(!$this->FormSubmitted)
            return;

        $this->GetFormParameters();
        $this->PressedButton = "Button_Submit";
        if($this->Button_Submit->Pressed) {
            $this->PressedButton = "Button_Submit";
        }

        $Redirect = $FileName . "?" . CCGetQueryString("QueryString", array("ccsForm"));
        if($this->PressedButton == "Button_Submit") {
            if(!CCGetEvent($this->Button_Submit->CCSEvents, "OnClick", $this->Button_Submit) || !$this->UpdateGrid()) {
                $Redirect = "";
            }
        } else {
            $Redirect = "";
        }
        if ($Redirect)
            $this->DataSource->close();
    }
//End Operation Method

//UpdateGrid Method @101-57860A98
    function UpdateGrid()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSubmit", $this);
        if(!$this->Validate()) return;
        $Validation = true;
        for($this->RowNumber = 1; $this->RowNumber <= $this->TotalRows; $this->RowNumber++)
        {
            $this->DataSource->CachedColumns["CODFAT"] = $this->CachedColumns["CODFAT"][$this->RowNumber];
            $this->DataSource->CurrentRow = $this->RowNumber;
            $this->CheckBox_Delete->SetText($this->FormParameters["CheckBox_Delete"][$this->RowNumber], $this->RowNumber);
            if ($this->UpdatedRows >= $this->RowNumber) {
                if($this->CheckBox_Delete->Value) {
                    if($this->DeleteAllowed) { $Validation = ($this->DeleteRow() && $Validation); }
                } else if($this->UpdateAllowed) {
                    $Validation = ($this->UpdateRow() && $Validation);
                }
            }
            else if($this->CheckInsert() && $this->InsertAllowed)
            {
                $Validation = ($Validation && $this->InsertRow());
            }
        }
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterSubmit", $this);
        if ($this->Errors->Count() == 0 && $Validation){
            $this->DataSource->close();
            return true;
        }
        return false;
    }
//End UpdateGrid Method

//DeleteRow Method @101-A4A656F6
    function DeleteRow()
    {
        if(!$this->DeleteAllowed) return false;
        $this->DataSource->Delete();
        $errors = "";
        if($this->DataSource->Errors->Count() > 0) {
            $errors = $this->DataSource->Errors->ToString();
            $this->RowsErrors[$this->RowNumber] = $errors;
            $this->DataSource->Errors->Clear();
        }
        return (($this->Errors->Count() == 0) && !strlen($errors));
    }
//End DeleteRow Method

//FormScript Method @101-FD0DD43D
    function FormScript($TotalRows)
    {
        $script = "";
        $script .= "\n<script language=\"JavaScript\" type=\"text/javascript\">\n<!--\n";
        $script .= "var CADFAT_CADCLIElements;\n";
        $script .= "var CADFAT_CADCLIEmptyRows = 0;\n";
        $script .= "var " . $this->ComponentName . "DeleteControl = 0;\n";
        $script .= "\nfunction initCADFAT_CADCLIElements() {\n";
        $script .= "\tvar ED = document.forms[\"CADFAT_CADCLI\"];\n";
        $script .= "\tCADFAT_CADCLIElements = new Array (\n";
        for($i = 1; $i <= $TotalRows; $i++) {
            $script .= "\t\tnew Array(" . "ED.CheckBox_Delete_" . $i . ")";
            if($i != $TotalRows) $script .= ",\n";
        }
        $script .= ");\n";
        $script .= "}\n";
        $script .= "\n//-->\n</script>";
        return $script;
    }
//End FormScript Method

//SetFormState Method @101-26CC69FD
    function SetFormState($FormState)
    {
        if(strlen($FormState)) {
            $FormState = str_replace("\\\\", "\\" . ord("\\"), $FormState);
            $FormState = str_replace("\\;", "\\" . ord(";"), $FormState);
            $pieces = explode(";", $FormState);
            $this->UpdatedRows = $pieces[0];
            $this->EmptyRows   = $pieces[1];
            $this->TotalRows = $this->UpdatedRows + $this->EmptyRows;
            $RowNumber = 0;
            for($i = 2; $i < sizeof($pieces); $i = $i + 1)  {
                $piece = $pieces[$i + 0];
                $piece = str_replace("\\" . ord("\\"), "\\", $piece);
                $piece = str_replace("\\" . ord(";"), ";", $piece);
                $this->CachedColumns["CODFAT"][$RowNumber] = $piece;
                $RowNumber++;
            }

            if(!$RowNumber) { $RowNumber = 1; }
            for($i = 1; $i <= $this->EmptyRows; $i++) {
                $this->CachedColumns["CODFAT"][$RowNumber] = "";
                $RowNumber++;
            }
        }
    }
//End SetFormState Method

//GetFormState Method @101-5C6230C7
    function GetFormState($NonEmptyRows)
    {
        if(!$this->FormSubmitted) {
            $this->FormState  = $NonEmptyRows . ";";
            $this->FormState .= $this->InsertAllowed ? $this->EmptyRows : "0";
            if($NonEmptyRows) {
                for($i = 0; $i <= $NonEmptyRows; $i++) {
                    $this->FormState .= ";" . str_replace(";", "\\;", str_replace("\\", "\\\\", $this->CachedColumns["CODFAT"][$i]));
                }
            }
        }
        return $this->FormState;
    }
//End GetFormState Method

//Show Method @101-7C56A5F7
    function Show()
    {
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        global $CCSUseAmp;
        $Error = "";

        if(!$this->Visible) { return; }

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->TIPO_CANC->Prepare();

        $this->DataSource->open();
        $is_next_record = ($this->ReadAllowed && $this->DataSource->next_record());
        $this->IsEmpty = ! $is_next_record;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        if(!$this->Visible) { return; }

        $this->Attributes->Show();
        $this->Button_Submit->Visible = $this->Button_Submit->Visible && ($this->InsertAllowed || $this->UpdateAllowed || $this->DeleteAllowed);
        $ParentPath = $Tpl->block_path;
        $EditableGridPath = $ParentPath . "/EditableGrid " . $this->ComponentName;
        $EditableGridRowPath = $ParentPath . "/EditableGrid " . $this->ComponentName . "/Row";
        $Tpl->block_path = $EditableGridRowPath;
        $this->RowNumber = 0;
        $NonEmptyRows = 0;
        $EmptyRowsLeft = $this->EmptyRows;
        $this->ControlsVisible["DESCLI"] = $this->DESCLI->Visible;
        $this->ControlsVisible["CODFAT"] = $this->CODFAT->Visible;
        $this->ControlsVisible["DATEMI"] = $this->DATEMI->Visible;
        $this->ControlsVisible["DATVNC"] = $this->DATVNC->Visible;
        $this->ControlsVisible["VALFAT"] = $this->VALFAT->Visible;
        $this->ControlsVisible["CheckBox_Delete"] = $this->CheckBox_Delete->Visible;
        if ($is_next_record || ($EmptyRowsLeft && $this->InsertAllowed)) {
            do {
                $this->RowNumber++;
                if($is_next_record) {
                    $NonEmptyRows++;
                    $this->DataSource->SetValues();
                }
                if (!($is_next_record) || !($this->DeleteAllowed)) {
                    $this->CheckBox_Delete->Visible = false;
                }
                if (!($this->FormSubmitted) && $is_next_record) {
                    $this->CachedColumns["CODFAT"][$this->RowNumber] = $this->DataSource->CachedColumns["CODFAT"];
                    $this->CheckBox_Delete->SetValue(true);
                    $this->DESCLI->SetValue($this->DataSource->DESCLI->GetValue());
                    $this->CODFAT->SetValue($this->DataSource->CODFAT->GetValue());
                    $this->DATEMI->SetValue($this->DataSource->DATEMI->GetValue());
                    $this->DATVNC->SetValue($this->DataSource->DATVNC->GetValue());
                    $this->VALFAT->SetValue($this->DataSource->VALFAT->GetValue());
                } elseif ($this->FormSubmitted && $is_next_record) {
                    $this->DESCLI->SetText("");
                    $this->CODFAT->SetText("");
                    $this->DATEMI->SetText("");
                    $this->DATVNC->SetText("");
                    $this->VALFAT->SetText("");
                    $this->DESCLI->SetValue($this->DataSource->DESCLI->GetValue());
                    $this->CODFAT->SetValue($this->DataSource->CODFAT->GetValue());
                    $this->DATEMI->SetValue($this->DataSource->DATEMI->GetValue());
                    $this->DATVNC->SetValue($this->DataSource->DATVNC->GetValue());
                    $this->VALFAT->SetValue($this->DataSource->VALFAT->GetValue());
                    $this->CheckBox_Delete->SetText($this->FormParameters["CheckBox_Delete"][$this->RowNumber], $this->RowNumber);
                } elseif (!$this->FormSubmitted) {
                    $this->CachedColumns["CODFAT"][$this->RowNumber] = "";
                    $this->DESCLI->SetText("");
                    $this->CODFAT->SetText("");
                    $this->DATEMI->SetText("");
                    $this->DATVNC->SetText("");
                    $this->VALFAT->SetText("");
                } else {
                    $this->DESCLI->SetText("");
                    $this->CODFAT->SetText("");
                    $this->DATEMI->SetText("");
                    $this->DATVNC->SetText("");
                    $this->VALFAT->SetText("");
                    $this->CheckBox_Delete->SetText($this->FormParameters["CheckBox_Delete"][$this->RowNumber], $this->RowNumber);
                }
                $this->Attributes->SetValue("rowNumber", $this->RowNumber);
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow", $this);
                $this->Attributes->Show();
                $this->DESCLI->Show($this->RowNumber);
                $this->CODFAT->Show($this->RowNumber);
                $this->DATEMI->Show($this->RowNumber);
                $this->DATVNC->Show($this->RowNumber);
                $this->VALFAT->Show($this->RowNumber);
                $this->CheckBox_Delete->Show($this->RowNumber);
                if (isset($this->RowsErrors[$this->RowNumber]) && ($this->RowsErrors[$this->RowNumber] != "")) {
                    $Tpl->setblockvar("RowError", "");
                    $Tpl->setvar("Error", $this->RowsErrors[$this->RowNumber]);
                    $this->Attributes->Show();
                    $Tpl->parse("RowError", false);
                } else {
                    $Tpl->setblockvar("RowError", "");
                }
                $Tpl->setvar("FormScript", $this->FormScript($this->RowNumber));
                $Tpl->parse();
                if ($is_next_record) {
                    if ($this->FormSubmitted) {
                        $is_next_record = $this->RowNumber < $this->UpdatedRows;
                        if (($this->DataSource->CachedColumns["CODFAT"] == $this->CachedColumns["CODFAT"][$this->RowNumber])) {
                            if ($this->ReadAllowed) $this->DataSource->next_record();
                        }
                    }else{
                        $is_next_record = ($this->RowNumber < $this->PageSize) &&  $this->ReadAllowed && $this->DataSource->next_record();
                    }
                } else { 
                    $EmptyRowsLeft--;
                }
            } while($is_next_record || ($EmptyRowsLeft && $this->InsertAllowed));
        } else {
            $Tpl->block_path = $EditableGridPath;
            $this->Attributes->Show();
            $Tpl->parse("NoRecords", false);
        }

        $Tpl->block_path = $EditableGridPath;
        if(!is_array($this->CheckBox_All->Value) && !strlen($this->CheckBox_All->Value) && $this->CheckBox_All->Value !== false)
            $this->CheckBox_All->SetValue(true);
        $this->Navigator->PageNumber = $this->DataSource->AbsolutePage;
        if ($this->DataSource->RecordsCount == "CCS not counted")
            $this->Navigator->TotalPages = $this->DataSource->AbsolutePage + ($this->DataSource->next_record() ? 1 : 0);
        else
            $this->Navigator->TotalPages = $this->DataSource->PageCount();
        $this->Sorter_DESCLI->Show();
        $this->Sorter_CODFAT->Show();
        $this->Sorter_DATEMI->Show();
        $this->Sorter_DATVNC->Show();
        $this->Sorter_VALFAT->Show();
        $this->CheckBox_All->Show();
        $this->Navigator->Show();
        $this->TIPO_CANC->Show();
        $this->DESC_CANC->Show();
        $this->Button_Submit->Show();

        if($this->CheckErrors()) {
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Error = ComposeStrings($Error, $this->DataSource->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        if (!$CCSUseAmp) {
            $Tpl->SetVar("HTMLFormProperties", "method=\"POST\" action=\"" . $this->HTMLFormAction . "\" name=\"" . $this->ComponentName . "\"");
        } else {
            $Tpl->SetVar("HTMLFormProperties", "method=\"post\" action=\"" . str_replace("&", "&amp;", $this->HTMLFormAction) . "\" id=\"" . $this->ComponentName . "\"");
        }
        $Tpl->SetVar("FormState", CCToHTML($this->GetFormState($NonEmptyRows)));
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->DataSource->close();
    }
//End Show Method

} //End CADFAT_CADCLI Class @101-FCB6E20C

class clsCADFAT_CADCLIDataSource extends clsDBFaturar {  //CADFAT_CADCLIDataSource Class @101-C44B730E

//DataSource Variables @101-F4C6DE76
    public $Parent = "";
    public $CCSEvents = "";
    public $CCSEventResult;
    public $ErrorBlock;
    public $CmdExecution;

    public $DeleteParameters;
    public $CountSQL;
    public $wp;
    public $AllParametersSet;

    public $CachedColumns;
    public $CurrentRow;

    // Datasource fields
    public $DESCLI;
    public $CODFAT;
    public $DATEMI;
    public $DATVNC;
    public $VALFAT;
    public $CheckBox_Delete;
//End DataSource Variables

//DataSourceClass_Initialize Event @101-EC787F67
    function clsCADFAT_CADCLIDataSource(& $Parent)
    {
        $this->Parent = & $Parent;
        $this->ErrorBlock = "EditableGrid CADFAT_CADCLI/Error";
        $this->Initialize();
        $this->DESCLI = new clsField("DESCLI", ccsText, "");
        $this->CODFAT = new clsField("CODFAT", ccsText, "");
        $this->DATEMI = new clsField("DATEMI", ccsDate, $this->DateFormat);
        $this->DATVNC = new clsField("DATVNC", ccsDate, $this->DateFormat);
        $this->VALFAT = new clsField("VALFAT", ccsFloat, "");
        $this->CheckBox_Delete = new clsField("CheckBox_Delete", ccsBoolean, $this->BooleanFormat);

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @101-6BD2F2FA
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_DESCLI" => array("DESCLI", ""), 
            "Sorter_CODFAT" => array("CODFAT", ""), 
            "Sorter_DATEMI" => array("DATEMI", ""), 
            "Sorter_DATVNC" => array("DATVNC", ""), 
            "Sorter_VALFAT" => array("VALFAT", "")));
    }
//End SetOrder Method

//Prepare Method @101-E6B0B639
    function Prepare()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("2", "urls_DATEMI", ccsDate, $DefaultDateFormat, $this->DateFormat, $this->Parameters["urls_DATEMI"], "", false);
        $this->wp->AddParameter("3", "urls_DATEMI_FIM", ccsDate, $DefaultDateFormat, $this->DateFormat, $this->Parameters["urls_DATEMI_FIM"], "", false);
        $this->wp->AddParameter("4", "urls_DATVNC", ccsDate, $DefaultDateFormat, $this->DateFormat, $this->Parameters["urls_DATVNC"], "", false);
        $this->wp->AddParameter("5", "urls_DATVNC_FIM", ccsDate, $DefaultDateFormat, $this->DateFormat, $this->Parameters["urls_DATVNC_FIM"], "", false);
        $this->wp->AddParameter("6", "urls_CODCLI", ccsText, "", "", $this->Parameters["urls_CODCLI"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = "( CADFAT.VALPGT=0 )";
        $this->wp->Criterion[2] = $this->wp->Operation(opGreaterThanOrEqual, "DATEMI", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsDate),false);
        $this->wp->Criterion[3] = $this->wp->Operation(opLessThanOrEqual, "DATEMI", $this->wp->GetDBValue("3"), $this->ToSQL($this->wp->GetDBValue("3"), ccsDate),false);
        $this->wp->Criterion[4] = $this->wp->Operation(opGreaterThanOrEqual, "DATVNC", $this->wp->GetDBValue("4"), $this->ToSQL($this->wp->GetDBValue("4"), ccsDate),false);
        $this->wp->Criterion[5] = $this->wp->Operation(opLessThanOrEqual, "DATVNC", $this->wp->GetDBValue("5"), $this->ToSQL($this->wp->GetDBValue("5"), ccsDate),false);
        $this->wp->Criterion[6] = $this->wp->Operation(opEndsWith, "CODCLI", $this->wp->GetDBValue("6"), $this->ToSQL($this->wp->GetDBValue("6"), ccsText),false);
        $this->Where = $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]), 
             $this->wp->Criterion[3]), 
             $this->wp->Criterion[4]), 
             $this->wp->Criterion[5]), 
             $this->wp->Criterion[6]);
    }
//End Prepare Method

//Open Method @101-D2147365
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect", $this->Parent);
        $this->CountSQL = "SELECT COUNT(*)\n\n" .
        "FROM CADFAT";
        $this->SQL = "SELECT * \n\n" .
        "FROM CADFAT {SQL_Where} {SQL_OrderBy}";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect", $this->Parent);
        if ($this->CountSQL) 
            $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        else
            $this->RecordsCount = "CCS not counted";
        $this->query(CCBuildSQL($this->SQL, $this->Where, $this->Order));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect", $this->Parent);
        $this->MoveToPage($this->AbsolutePage);
    }
//End Open Method

//SetValues Method @101-7E712FFB
    function SetValues()
    {
        $this->CachedColumns["CODFAT"] = $this->f("CODFAT");
        $this->DESCLI->SetDBValue($this->f("DESCLI"));
        $this->CODFAT->SetDBValue($this->f("CODFAT"));
        $this->DATEMI->SetDBValue(trim($this->f("DATEMI")));
        $this->DATVNC->SetDBValue(trim($this->f("DATVNC")));
        $this->VALFAT->SetDBValue(trim($this->f("VALFAT")));
    }
//End SetValues Method

//Delete Method @101-FE528073
    function Delete()
    {
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildDelete", $this->Parent);
        $SelectWhere = $this->Where;
        $this->Where = "CODFAT=" . $this->ToSQL($this->CachedColumns["CODFAT"], ccsText);
        $this->SQL = "DELETE FROM CADFAT";
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        if (!strlen($this->Where) && $this->Errors->Count() == 0) 
            $this->Errors->addError($CCSLocales->GetText("CCS_CustomOperationError_MissingParameters"));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteDelete", $this->Parent);
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteDelete", $this->Parent);
        }
        $this->Where = $SelectWhere;
    }
//End Delete Method

} //End CADFAT_CADCLIDataSource Class @101-FCB6E20C





//Initialize Page @1-F1BF99CB
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "PagtoFatu_Dados_Canc_Lote.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Include events file @1-F1CF469E
include("./PagtoFatu_Dados_Canc_Lote_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-499BBC0C
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$cadcli_CADFAT = new clsRecordcadcli_CADFAT("", $MainPage);
$CADFAT_CADCLI = new clsEditableGridCADFAT_CADCLI("", $MainPage);
$MainPage->cabec = & $cabec;
$MainPage->cadcli_CADFAT = & $cadcli_CADFAT;
$MainPage->CADFAT_CADCLI = & $CADFAT_CADCLI;
$CADFAT_CADCLI->Initialize();

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-00D55CD7
$cabec->Operations();
$cadcli_CADFAT->Operation();
$CADFAT_CADCLI->Operation();
//End Execute Components

//Go to destination page @1-20A619EF
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($cadcli_CADFAT);
    unset($CADFAT_CADCLI);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-97F9A6EC
$cabec->Show();
$cadcli_CADFAT->Show();
$CADFAT_CADCLI->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-194E5B4C
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($cadcli_CADFAT);
unset($CADFAT_CADCLI);
unset($Tpl);
//End Unload Page


?>
