<?php
//BindEvents Method @1-A7DFF1E5
function BindEvents()
{
    global $CADFAT_CADCLI;
    global $CCSEvents;
    $CADFAT_CADCLI->DESCLI->CCSEvents["BeforeShow"] = "CADFAT_CADCLI_DESCLI_BeforeShow";
    $CADFAT_CADCLI->Button_Submit->CCSEvents["OnClick"] = "CADFAT_CADCLI_Button_Submit_OnClick";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//CADFAT_CADCLI_DESCLI_BeforeShow @120-568B4465
function CADFAT_CADCLI_DESCLI_BeforeShow(& $sender)
{
    $CADFAT_CADCLI_DESCLI_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_DESCLI_BeforeShow

//Custom Code @130-2A29BDB7
// -------------------------

	global $Tabela;

	if( $Tabela===0)
		$Tabela = new clsDBfaturar();

	$CADFAT_CADCLI->DESCLI->SetValue( CCDLookUp("descli||' ('||codcli||')'", "CADCLI", "codcli='".$CADFAT_CADCLI->DataSource->f("CODCLI")."'", $Tabela));


// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_DESCLI_BeforeShow @120-6B06838A
    return $CADFAT_CADCLI_DESCLI_BeforeShow;
}
//End Close CADFAT_CADCLI_DESCLI_BeforeShow


//CADFAT_CADCLI_Button_Submit_OnClick @127-0DE7EC57
function CADFAT_CADCLI_Button_Submit_OnClick(& $sender)
{
    $CADFAT_CADCLI_Button_Submit_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT_CADCLI; //Compatibility
//End CADFAT_CADCLI_Button_Submit_OnClick

//Custom Code @129-2A29BDB7
// -------------------------

	global $Tabela;

	$desc_canc = CCGetParam('DESC_CANC');
	$tipo_canc = CCGetParam('TIPO_CANC');
	$faturas_excluidas = array();

    if(!$desc_canc) {
		$CADFAT_CADCLI->Errors->addError("� necess�rio informar uma {$CADFAT_CADCLI->DESC_CANC->Caption} para excluir faturas");
	} elseif(!is_numeric($tipo_canc)) {
		$CADFAT_CADCLI->Errors->addError("� necess�rio selecionar um {$CADFAT_CADCLI->TIPO_CANC->Caption} para excluir faturas");
	} else {
		if($Tabela===0)
			$Tabela = new clsDBfaturar();
		$desc_canc = str_replace("'",'`',$desc_canc);

		$num_faturas = count($CADFAT_CADCLI->FormParameters['CheckBox_Delete']);
		for($i=1; $i<=$num_faturas; $i++) {
			//para cada fatura marcada para exclus�o
			if($CADFAT_CADCLI->FormParameters['CheckBox_Delete'][$i]=='1') {
				$mCodFat=$CADFAT_CADCLI->CachedColumns['CODFAT'][$i];

				//salva dados da fatura na tabela CADFAT_CANC
				$Tabela->query("
					INSERT INTO cadfat_canc (CODFAT,CODCLI,MESREF,DATEMI,DATVNC,VALFAT,VALMOV,RET_INSS,DATPGT,VALPGT,VALCOB,VALJUR,VALMUL,EXPORT,PGTOELET,CONSIST,CONSIST_M,ISSQN,LOGSER,BAISER,CIDSER,ESTSER,DATCAN,IDCAN,CODTIPCANC,DESCCANC) 
						SELECT CODFAT,CODCLI,MESREF,DATEMI,DATVNC,VALFAT,VALMOV,RET_INSS,DATPGT,VALPGT,VALCOB,VALJUR,VALMUL,cadfat.EXPORT,PGTOELET,CONSIST,CONSIST_M,ISSQN,LOGSER,BAISER,CIDSER,ESTSER,SYSDATE as DATCAN, ".CCGetSession("IDUsuario")." as IDCAN,{$tipo_canc} as CODTIPCANC,'{$desc_canc}' as DESCCANC
						FROM cadfat
						WHERE codfat = '$mCodFat'
				");

				//se ocorreu erro na inclus�o da fatura em CADFAT_CANC
				if(is_array($Tabela->Error)) { 
					//cancela a exclus�o dos registros da tabela CADFAT (a��o padr�o do bot�o "Excluir faturas")
					$CADFAT_CADCLI_Button_Submit_OnClick = false;
					//apaga os registros que possam ter sido salvos em CADFAT_CANC
					$Tabela->query("DELETE FROM cadfat_canc WHERE codfat IN ('".implode("','",$faturas_excluidas)."')");
					$CADFAT_CADCLI->Errors->addError("Ocorreu um erro inesperado ao excluir a fatura $mCodFat, por isso a opera��o foi cancelada.");
					break;
				}
				$faturas_excluidas[] = $mCodFat;
			}
		}
	}
// -------------------------
//End Custom Code

//Close CADFAT_CADCLI_Button_Submit_OnClick @127-2C5964AA
    return $CADFAT_CADCLI_Button_Submit_OnClick;
}
//End Close CADFAT_CADCLI_Button_Submit_OnClick

//Page_BeforeShow @1-C97ED775
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $PagtoFatu_Dados_Canc_Lote; //Compatibility
//End Page_BeforeShow

//Custom Code @89-2A29BDB7
// -------------------------
    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(34);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");
// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
