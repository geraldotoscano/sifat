<?php
//BindEvents Method @1-9C42454F
function BindEvents()
{
    global $Pagto;
    global $Servicos;
    global $CCSEvents;
    $Pagto->Label1->CCSEvents["BeforeShow"] = "Pagto_Label1_BeforeShow";
    $Pagto->Label2->CCSEvents["BeforeShow"] = "Pagto_Label2_BeforeShow";
    $Pagto->VALCOB->CCSEvents["BeforeShow"] = "Pagto_VALCOB_BeforeShow";
    $Pagto->lblJa_Exportada->CCSEvents["BeforeShow"] = "Pagto_lblJa_Exportada_BeforeShow";
    $Pagto->VALMUL->CCSEvents["BeforeShow"] = "Pagto_VALMUL_BeforeShow";
    $Pagto->DATPGT->CCSEvents["BeforeShow"] = "Pagto_DATPGT_BeforeShow";
    $Pagto->VALPGT->CCSEvents["BeforeShow"] = "Pagto_VALPGT_BeforeShow";
    $Pagto->VALJUR->CCSEvents["BeforeShow"] = "Pagto_VALJUR_BeforeShow";
    $Pagto->taxa_juros->CCSEvents["BeforeShow"] = "Pagto_taxa_juros_BeforeShow";
    $Pagto->ISSQN->CCSEvents["BeforeShow"] = "Pagto_ISSQN_BeforeShow";
    $Pagto->Button_Update->CCSEvents["OnClick"] = "Pagto_Button_Update_OnClick";
    $Pagto->CCSEvents["BeforeShow"] = "Pagto_BeforeShow";
    $Servicos->QTDMED->CCSEvents["BeforeShow"] = "Servicos_QTDMED_BeforeShow";
    $Servicos->EQUPBH_X_VALPBH->CCSEvents["BeforeShow"] = "Servicos_EQUPBH_X_VALPBH_BeforeShow";
    $Servicos->EQUPBH_X_VALPBH_X_QTMED->CCSEvents["BeforeShow"] = "Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow";
    $Servicos->TOT_FAT->CCSEvents["BeforeShow"] = "Servicos_TOT_FAT_BeforeShow";
    $Servicos->lbl_Inss->CCSEvents["BeforeShow"] = "Servicos_lbl_Inss_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//Pagto_Label1_BeforeShow @15-F99F9008
function Pagto_Label1_BeforeShow(& $sender)
{
    $Pagto_Label1_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_Label1_BeforeShow

//Custom Code @16-2A29BDB7
// -------------------------
    // Write your own code here.
	$Pagto->Label1->SetValue(CCGetSession("DataSist"));
// -------------------------
//End Custom Code

//Close Pagto_Label1_BeforeShow @15-9041719F
    return $Pagto_Label1_BeforeShow;
}
//End Close Pagto_Label1_BeforeShow

//DEL  // -------------------------
//DEL  	$mOpcao = CCGetParam("opcao","");
//DEL  	if ($mOpcao=="'1'")
//DEL  	{
//DEL  		$mTexto = 'Visualiza��o ';
//DEL  	}
//DEL  	else if ($mOpcao=="'2'")
//DEL  	{
//DEL  		$mTexto = 'Pagamento - Baixa ';
//DEL  	}
//DEL  	else
//DEL  	{
//DEL  		$mTexto = 'Cancelamento de pagamento ';
//DEL  	}
//DEL      $Pagto->lblTitulo->SetValue($mTexto);
//DEL  // -------------------------

//Pagto_Label2_BeforeShow @17-DA7464D1
function Pagto_Label2_BeforeShow(& $sender)
{
    $Pagto_Label2_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_Label2_BeforeShow

//Custom Code @18-2A29BDB7
// -------------------------
    // Write your own code here.
	$Pagto->Label2->SetValue(CCGetSession("HoraSist"));
// -------------------------
//End Custom Code

//Close Pagto_Label2_BeforeShow @17-EC205444
    return $Pagto_Label2_BeforeShow;
}
//End Close Pagto_Label2_BeforeShow

//Pagto_VALCOB_BeforeShow @113-AC739AC8
function Pagto_VALCOB_BeforeShow(& $sender)
{
    $Pagto_VALCOB_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_VALCOB_BeforeShow

//Custom Code @115-2A29BDB7
// -------------------------
    // Write your own code here.
	$Pagto->VALCOB->SetValue("0.00");
// -------------------------
//End Custom Code

//Close Pagto_VALCOB_BeforeShow @113-87CFF35D
    return $Pagto_VALCOB_BeforeShow;
}
//End Close Pagto_VALCOB_BeforeShow

//Pagto_lblJa_Exportada_BeforeShow @19-8B8F461A
function Pagto_lblJa_Exportada_BeforeShow(& $sender)
{
    $Pagto_lblJa_Exportada_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_lblJa_Exportada_BeforeShow

//Custom Code @46-2A29BDB7
// -------------------------
    // Write your own code here.
	$Pagto->lblJa_Exportada->SetValue($Pagto->EXPORT->GetValue()=="T" ? "J� Exportada":"N�o Exportada");
// -------------------------
//End Custom Code

//Close Pagto_lblJa_Exportada_BeforeShow @19-A9E33F80
    return $Pagto_lblJa_Exportada_BeforeShow;
}
//End Close Pagto_lblJa_Exportada_BeforeShow

//Pagto_VALMUL_BeforeShow @114-3B511793
function Pagto_VALMUL_BeforeShow(& $sender)
{
    $Pagto_VALMUL_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_VALMUL_BeforeShow

//Custom Code @146-2A29BDB7
// -------------------------
    $Pagto->VALMUL->SetValue("0.00");
// -------------------------
//End Custom Code

//Close Pagto_VALMUL_BeforeShow @114-4FBBB22F
    return $Pagto_VALMUL_BeforeShow;
}
//End Close Pagto_VALMUL_BeforeShow

//Pagto_DATPGT_BeforeShow @8-ED308691
function Pagto_DATPGT_BeforeShow(& $sender)
{
    $Pagto_DATPGT_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_DATPGT_BeforeShow

//Custom Code @109-2A29BDB7
// -------------------------
    // Write your own code here.
	$Pagto->DATPGT->SetValue(null);
// -------------------------
//End Custom Code

//Close Pagto_DATPGT_BeforeShow @8-8F6E2974
    return $Pagto_DATPGT_BeforeShow;
}
//End Close Pagto_DATPGT_BeforeShow

//Pagto_VALPGT_BeforeShow @14-DD3F53A8
function Pagto_VALPGT_BeforeShow(& $sender)
{
    $Pagto_VALPGT_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_VALPGT_BeforeShow

//Custom Code @108-2A29BDB7
// -------------------------
    // Write your own code here.
	$Pagto->VALPGT->SetValue("0.00");
	/*if (CCGetParam("opcao","")=="'2'")
	{
	   $nINSS=$Pagto->RET_INSS->GetValue();
	   $mJuros = $Pagto->VALJUR->GetValue();
	   $Pagto->VALPGT->SetValue((($Pagto->VALFAT->GetValue()+$mJuros)-$nINSS));
	   //$Pagto->VALPGT->SetValue(0.00);
	}*/
// -------------------------
//End Custom Code

//Close Pagto_VALPGT_BeforeShow @14-19D9C26C
    return $Pagto_VALPGT_BeforeShow;
}
//End Close Pagto_VALPGT_BeforeShow

//Pagto_VALJUR_BeforeShow @91-58BB54CE
function Pagto_VALJUR_BeforeShow(& $sender)
{
    $Pagto_VALJUR_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_VALJUR_BeforeShow

//Custom Code @145-2A29BDB7
// -------------------------
    $Pagto->VALJUR->SetValue("0.00");
// -------------------------
//End Custom Code

//Close Pagto_VALJUR_BeforeShow @91-1F6B23B6
    return $Pagto_VALJUR_BeforeShow;
}
//End Close Pagto_VALJUR_BeforeShow

//Pagto_taxa_juros_BeforeShow @141-AA88C452
function Pagto_taxa_juros_BeforeShow(& $sender)
{
    $Pagto_taxa_juros_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_taxa_juros_BeforeShow

//Custom Code @142-2A29BDB7
// -------------------------
    $Pagto->taxa_juros->SetValue(CCGetSession("mTAXA_JUROS"));
// -------------------------
//End Custom Code

//Close Pagto_taxa_juros_BeforeShow @141-5A86524C
    return $Pagto_taxa_juros_BeforeShow;
}
//End Close Pagto_taxa_juros_BeforeShow

//Pagto_ISSQN_BeforeShow @99-EA348A81
function Pagto_ISSQN_BeforeShow(& $sender)
{
    $Pagto_ISSQN_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_ISSQN_BeforeShow

//Custom Code @111-2A29BDB7
// -------------------------
    // Write your own code here.
	/*$mMesRef = $Pagto->MESREF->GetValue();
	$nAux1 = 0+(substr($mMesRef,3,4).substr($mMesRef,0,2));
	if (($nAux1 > 200812) || ($Pagto->ESFERA->GetValue()=='T'))
	{
			$Pagto->ISSQN->SetValue(0);
	}*/
// -------------------------
//End Custom Code

//Close Pagto_ISSQN_BeforeShow @99-4E86E0F6
    return $Pagto_ISSQN_BeforeShow;
}
//End Close Pagto_ISSQN_BeforeShow

//Pagto_Button_Update_OnClick @9-1F4E4FAD
function Pagto_Button_Update_OnClick(& $sender)
{
    $Pagto_Button_Update_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_Button_Update_OnClick

//Custom Code @148-2A29BDB7
// -------------------------
    $Pagto->DATPGT->SetValue(NULL);
	$Pagto->VALPGT->SetValue(0);
// -------------------------
//End Custom Code

//Close Pagto_Button_Update_OnClick @9-663B3315
    return $Pagto_Button_Update_OnClick;
}
//End Close Pagto_Button_Update_OnClick

//DEL  // -------------------------
//DEL     global $DBFaturar;
//DEL     $mCodFat = $Pagto->CODFAT->GetValue();
//DEL     $DBFaturar->query("insert into cadfat_canc select * from cadfat where codfat = '$mCodFat'");
//DEL     $DBFaturar->query("delete from cadfat where codfat = '$mCodFat'"); 
//DEL  // -------------------------

//Pagto_BeforeShow @4-665D9AC6
function Pagto_BeforeShow(& $sender)
{
    $Pagto_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_BeforeShow

//Custom Code @126-2A29BDB7
// -------------------------
	$mOpcao = CCGetParam("opcao","");
	if ($mOpcao=="'1'")
	{
		$mTexto = 'Visualiza��o ';
	    $Pagto->Button_Update->Visible=false;
	}
	else if ($mOpcao=="'2'")
	{
		$mTexto = 'Pagamento - Baixa ';
	    $Pagto->Button_Update->Visible=true;
	}
	else
	{
		$mTexto = 'Cancelamento ';
	    $Pagto->Button_Update->Visible=true;
	}
	if ($Pagto->VALPGT->GetValue() > 0)
	{
		$Pagto->Label3->SetValue('pago');
	}
	else
	{
		$Pagto->Label3->SetValue('a pagar');
	}

// -------------------------
//End Custom Code

//Close Pagto_BeforeShow @4-C3C3E26F
    return $Pagto_BeforeShow;
}
//End Close Pagto_BeforeShow

//Servicos_QTDMED_BeforeShow @62-0AE1459B
function Servicos_QTDMED_BeforeShow(& $sender)
{
    $Servicos_QTDMED_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Servicos; //Compatibility
//End Servicos_QTDMED_BeforeShow

//Custom Code @138-2A29BDB7
// -------------------------
	$Servicos->QTDMED->SetValue(number_format($Servicos->QTDMED->GetValue(), 2,',','.'));
// -------------------------
//End Custom Code

//Close Servicos_QTDMED_BeforeShow @62-787D978C
    return $Servicos_QTDMED_BeforeShow;
}
//End Close Servicos_QTDMED_BeforeShow

//Servicos_EQUPBH_X_VALPBH_BeforeShow @63-C7ABC05D
function Servicos_EQUPBH_X_VALPBH_BeforeShow(& $sender)
{
    $Servicos_EQUPBH_X_VALPBH_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Servicos; //Compatibility
//End Servicos_EQUPBH_X_VALPBH_BeforeShow

//Custom Code @83-2A29BDB7
// -------------------------
    // Write your own code here.
	$mEquPBH = $Servicos->EQUPBH->GetValue();
	$mValPBH = $Servicos->VALPBH->GetValue();
	$Servicos->EQUPBH_X_VALPBH->SetValue(number_format(round($mEquPBH*$mValPBH,2), 2,',','.'));
// -------------------------
//End Custom Code

//Close Servicos_EQUPBH_X_VALPBH_BeforeShow @63-31F58000
    return $Servicos_EQUPBH_X_VALPBH_BeforeShow;
}
//End Close Servicos_EQUPBH_X_VALPBH_BeforeShow

//Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow @64-5AE11F3B
function Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow(& $sender)
{
    $Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Servicos; //Compatibility
//End Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow

//Custom Code @86-2A29BDB7
// -------------------------
    // Write your own code here.
	global $mTotalFat;
	$mEquPBH = $Servicos->EQUPBH->GetValue();
	$mValPBH = $Servicos->VALPBH->GetValue();
	$mQtdMed = $Servicos->hdd_QTDMED->GetValue();
	$Servicos->EQUPBH_X_VALPBH_X_QTMED->SetValue(number_format(round($mEquPBH*$mValPBH*$mQtdMed,2), 2,',','.'));
	$mTotalFat+=round($mEquPBH*$mValPBH*$mQtdMed,2);
// -------------------------
//End Custom Code

//Close Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow @64-D15D496B
    return $Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow;
}
//End Close Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow

//DEL  // -------------------------
//DEL  	global $mTotalFat;
//DEL      $Servicos->Label1->SetValue("Parcial -> $mTotalFat");
//DEL  // -------------------------

//Servicos_TOT_FAT_BeforeShow @104-B2F8693F
function Servicos_TOT_FAT_BeforeShow(& $sender)
{
    $Servicos_TOT_FAT_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Servicos; //Compatibility
//End Servicos_TOT_FAT_BeforeShow

//Custom Code @105-2A29BDB7
// -------------------------
	global $mTotalFat;
    // Write your own code here.
	// $mTotalFat � a soma dos servi�os individuais acumulados no 
	// evento Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow() que det�m o valor
	// individual de cada servi�o
	$Servicos->TOT_FAT->SetValue(number_format(round($mTotalFat,2), 2,',','.'));

// -------------------------
//End Custom Code

//Close Servicos_TOT_FAT_BeforeShow @104-BEE238D2
    return $Servicos_TOT_FAT_BeforeShow;
}
//End Close Servicos_TOT_FAT_BeforeShow

//Servicos_lbl_Inss_BeforeShow @129-9509FE40
function Servicos_lbl_Inss_BeforeShow(& $sender)
{
    $Servicos_lbl_Inss_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Servicos; //Compatibility
//End Servicos_lbl_Inss_BeforeShow

//Custom Code @130-2A29BDB7 
// -------------------------
	$mINSS = (float)(str_replace(",", ".",CCGetSession("mINSS")));
	$mINSS = number_format($mINSS, 2,',','.');
	$Servicos->lbl_Inss->SetValue($mINSS);

	$mISSQN = (float)(str_replace(",", ".",CCGetSession("mISSQN")));
	$mISSQN = number_format($mISSQN, 2,',','.')	;
	$Servicos->lbl_Issqn->SetValue($mISSQN);
// -------------------------
//End Custom Code

//Close Servicos_lbl_Inss_BeforeShow @129-DA67001D
    return $Servicos_lbl_Inss_BeforeShow;
}
//End Close Servicos_lbl_Inss_BeforeShow

//Page_BeforeShow @1-B0AF10E3
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $PagtoFatu_Dados_Est; //Compatibility
//End Page_BeforeShow

//Custom Code @147-2A29BDB7
// -------------------------
    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(33);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");


// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	// � o m�s / ano de refer�ncia.
//DEL  	$mMesRef = $Pagto->MESREF->GetValue();
//DEL  	// Pega o ano de refer�ncia e concatena o m�s e soma 0 tornando $nAux1
//DEL  	// um n�mero.Lembramos que no php a tipagem � din�mica.
//DEL  	$nAux1 = 0+(substr($mMesRef,3,4).substr($mMesRef,0,2));
//DEL  
//DEL  	if ($nAux1 > 199901)
//DEL  	{
//DEL  		// S� faz sentido calcular o INSS para faturas do M�s/Ano de refer�ncia
//DEL  		// acima de fevereiro de 1999.
//DEL  		// $mTotalFat � a soma dos servi�os individuais acumulados no 
//DEL  		// evento Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow() que det�m o valor
//DEL  		// individual de cada servi�o.
//DEL  		// C�lculo do INSS que inside em 11% sobre o valor total da fatura .
//DEL  		$nINSS=round(($Pagto->VALFAT->GetValue())*11/100,2);
//DEL  		$Pagto->RET_INSS->SetValue($nINSS);
//DEL  	}
//DEL  	else
//DEL  	{
//DEL  		$Pagto->RET_INSS->SetValue(0);
//DEL  	}
//DEL  
//DEL  // -------------------------
	//$Pagto->Button_Update->Visible = (CCGetParam("opcao","1")=="2");


//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	global $DBfaturar;
//DEL  	$mValPGT = $Pagto->VALPGT->GetValue();
//DEL  	$mDatPGT = $Pagto->DATPGT->GetValue();
//DEL  	$mCodFat = $Pagto->CODFAT->GetValue();
//DEL  	//$DBfaturar->query("update movfat set valpgt = $mValPGT,datpgt = to_date('$mDatPGT') where codfat = $mCodFat");
//DEL  
//DEL  // -------------------------

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	$Pagto->TextBox7->SetValue($Pagto->VALFAT->GetValue());
//DEL  // -------------------------
$mTotalFat = 0;

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	$Servicos->JUROS->SetValue($Pagto->VALJUR->GetValue());
//DEL  // -------------------------

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	$Servicos->Ret_ISSQN->SetValue($Pagto->ESFERA->GetValue()=='T' ? "Esfera Municipal, Estadual ou Federal":"Retencao de ISSQN");
//DEL  // -------------------------

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	$mMesRef = $Pagto->TextBox7->GetValue();
//DEL  	$nAux1 = 0+(substr($mMesRef,3,4).substr($mMesRef,0,2));
//DEL  	if (($nAux1 > 200812) || ($Pagto->ESFERA->GetValue()=='T'))
//DEL  	{
//DEL  			$Servicos->ISSQN->SetValue(0);
//DEL  	}
//DEL  	else
//DEL  	{
//DEL  			$Servicos->ISSQN->SetValue($Pagto->ISSQN->GetValue());
//DEL  	}
//DEL  // -------------------------


//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	// � o m�s / ano de refer�ncia.
//DEL  	$mMesRef = $Pagto->TextBox7->GetValue();
//DEL  	// Pega o ano de refer�ncia e concatena o m�s e soma 0 tornando $nAux1
//DEL  	// um n�mero.Lembramos que no php a tipagem � din�mica.
//DEL  	$nAux1 = 0+(substr($mMesRef,3,4).substr($mMesRef,0,2));
//DEL  
//DEL  	if ($nAux1 > 199901)
//DEL  	{
//DEL  		// S� faz sentido calcular o INSS para faturas do M�s/Ano de refer�ncia
//DEL  		// acima de fevereiro de 1999.
//DEL  		// $mTotalFat � a soma dos servi�os individuais acumulados no 
//DEL  		// evento Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow() que det�m o valor
//DEL  		// individual de cada servi�o.
//DEL  		// C�lculo do INSS que inside em 11% sobre o valor total da fatura .
//DEL  		$nINSS=round(($mTotalFat)*11/100,2);
//DEL  		$Servicos->INSS->SetValue($nINSS);
//DEL  	}
//DEL  	else
//DEL  	{
//DEL  		$Servicos->INSS->SetValue(0);
//DEL  	}
//DEL  
//DEL  // -------------------------



//DEL  function Servicos_Label2_BeforeShow(& $sender)
//DEL  {
//DEL      $Servicos_Label2_BeforeShow = true;
//DEL      $Component = & $sender;
//DEL      $Container = & CCGetParentContainer($sender);
//DEL  	global $Pagto;
//DEL      global $Servicos; //Compatibility

?>
