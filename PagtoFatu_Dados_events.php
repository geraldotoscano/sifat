<?php
include('util.php');
//BindEvents Method @1-A0B493A9
function BindEvents()
{
    global $Pagto;
    global $Servicos;
    global $CCSEvents;
    $Pagto->Label1->CCSEvents["BeforeShow"] = "Pagto_Label1_BeforeShow";
    $Pagto->lblTitulo->CCSEvents["BeforeShow"] = "Pagto_lblTitulo_BeforeShow";
    $Pagto->Label2->CCSEvents["BeforeShow"] = "Pagto_Label2_BeforeShow";
    $Pagto->RET_INSS->CCSEvents["BeforeShow"] = "Pagto_RET_INSS_BeforeShow";
    $Pagto->RET_INSS->CCSEvents["OnValidate"] = "Pagto_RET_INSS_OnValidate";
    $Pagto->VALCOB->CCSEvents["BeforeShow"] = "Pagto_VALCOB_BeforeShow";
    $Pagto->VALPBH->CCSEvents["BeforeShow"] = "Pagto_VALPBH_BeforeShow";
    $Pagto->VALPBH->CCSEvents["OnValidate"] = "Pagto_VALPBH_OnValidate";
    $Pagto->lblJa_Exportada->CCSEvents["BeforeShow"] = "Pagto_lblJa_Exportada_BeforeShow";
    $Pagto->VALFAT->CCSEvents["BeforeShow"] = "Pagto_VALFAT_BeforeShow";
    $Pagto->VALFAT->CCSEvents["OnValidate"] = "Pagto_VALFAT_OnValidate";
    $Pagto->DATPGT->CCSEvents["BeforeShow"] = "Pagto_DATPGT_BeforeShow";
    $Pagto->VALPGT->CCSEvents["BeforeShow"] = "Pagto_VALPGT_BeforeShow";
    $Pagto->VALPGT->CCSEvents["OnValidate"] = "Pagto_VALPGT_OnValidate";
    $Pagto->VALJUR->CCSEvents["OnValidate"] = "Pagto_VALJUR_OnValidate";
    $Pagto->taxa_juros->CCSEvents["BeforeShow"] = "Pagto_taxa_juros_BeforeShow";
    $Pagto->ISSQN->CCSEvents["BeforeShow"] = "Pagto_ISSQN_BeforeShow";
    $Pagto->Label5->CCSEvents["BeforeShow"] = "Pagto_Label5_BeforeShow";
    $Pagto->senha->CCSEvents["BeforeShow"] = "Pagto_senha_BeforeShow";
    $Pagto->Label4->CCSEvents["BeforeShow"] = "Pagto_Label4_BeforeShow";
    $Pagto->CBAutorizacao->CCSEvents["BeforeShow"] = "Pagto_CBAutorizacao_BeforeShow";
    $Pagto->CCSEvents["BeforeShow"] = "Pagto_BeforeShow";
    $Pagto->CCSEvents["BeforeUpdate"] = "Pagto_BeforeUpdate";
    $Servicos->QTDMED->CCSEvents["BeforeShow"] = "Servicos_QTDMED_BeforeShow";
    $Servicos->EQUPBH_X_VALPBH->CCSEvents["BeforeShow"] = "Servicos_EQUPBH_X_VALPBH_BeforeShow";
    $Servicos->EQUPBH_X_VALPBH_X_QTMED->CCSEvents["BeforeShow"] = "Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow";
    $Servicos->TOT_FAT->CCSEvents["BeforeShow"] = "Servicos_TOT_FAT_BeforeShow";
    $Servicos->lbl_Inss->CCSEvents["BeforeShow"] = "Servicos_lbl_Inss_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//Pagto_Label1_BeforeShow @15-F99F9008
function Pagto_Label1_BeforeShow(& $sender)
{
    $Pagto_Label1_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_Label1_BeforeShow

//Custom Code @16-2A29BDB7
// -------------------------
    // Write your own code here.
	$Pagto->Label1->SetValue(CCGetSession("DataSist"));
// -------------------------
//End Custom Code

//Close Pagto_Label1_BeforeShow @15-9041719F
    return $Pagto_Label1_BeforeShow;
}
//End Close Pagto_Label1_BeforeShow

//Pagto_lblTitulo_BeforeShow @127-FC02DD0E
function Pagto_lblTitulo_BeforeShow(& $sender)
{
    $Pagto_lblTitulo_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_lblTitulo_BeforeShow

//Custom Code @128-2A29BDB7
// -------------------------
	$mOpcao = CCGetParam("opcao","");
	if ($mOpcao=="'1'")
	{
		$mTexto = 'Visualiza��o ';
	}
	else if ($mOpcao=="'2'")
	{
		$mTexto = 'Pagamento - Baixa ';
	}
	else
	{
		$mTexto = 'Cancelamento de pagamento ';
	}
    $Pagto->lblTitulo->SetValue($mTexto);
// -------------------------
//End Custom Code

//Close Pagto_lblTitulo_BeforeShow @127-9E13BBF3
    return $Pagto_lblTitulo_BeforeShow;
}
//End Close Pagto_lblTitulo_BeforeShow

//Pagto_Label2_BeforeShow @17-DA7464D1
function Pagto_Label2_BeforeShow(& $sender)
{
    $Pagto_Label2_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_Label2_BeforeShow

//Custom Code @18-2A29BDB7
// -------------------------
    // Write your own code here.
	$Pagto->Label2->SetValue(CCGetSession("HoraSist"));
// -------------------------
//End Custom Code

//Close Pagto_Label2_BeforeShow @17-EC205444
    return $Pagto_Label2_BeforeShow;
}
//End Close Pagto_Label2_BeforeShow

//Pagto_RET_INSS_BeforeShow @103-AD7B34D1
function Pagto_RET_INSS_BeforeShow(& $sender)
{
    $Pagto_RET_INSS_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_RET_INSS_BeforeShow

//Custom Code @147-2A29BDB7
// -------------------------
  Pagto_VALPGT_BeforeShow($sender);
// -------------------------
//End Custom Code

//Close Pagto_RET_INSS_BeforeShow @103-41A34ADF
    return $Pagto_RET_INSS_BeforeShow;
}
//End Close Pagto_RET_INSS_BeforeShow

//Pagto_RET_INSS_OnValidate @103-C33C9823
function Pagto_RET_INSS_OnValidate(& $sender)
{
    $Pagto_RET_INSS_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_RET_INSS_OnValidate

//Custom Code @152-2A29BDB7
// -------------------------
    Pagto_VALPGT_OnValidate($sender);
// -------------------------
//End Custom Code

//Close Pagto_RET_INSS_OnValidate @103-7E582E56
    return $Pagto_RET_INSS_OnValidate;
}
//End Close Pagto_RET_INSS_OnValidate

//Pagto_VALCOB_BeforeShow @113-AC739AC8
function Pagto_VALCOB_BeforeShow(& $sender)
{
    $Pagto_VALCOB_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_VALCOB_BeforeShow

//Custom Code @115-2A29BDB7
// -------------------------
    // Write your own code here.
	$Pagto->VALCOB->SetValue($Pagto->VALFAT->GetValue());
// -------------------------
//End Custom Code

//Close Pagto_VALCOB_BeforeShow @113-87CFF35D
    return $Pagto_VALCOB_BeforeShow;
}
//End Close Pagto_VALCOB_BeforeShow

//Pagto_VALPBH_BeforeShow @12-0F5F3B0F
function Pagto_VALPBH_BeforeShow(& $sender)
{
    $Pagto_VALPBH_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_VALPBH_BeforeShow

//Custom Code @146-2A29BDB7
// -------------------------
//   $Pagto->VALPBH->SetValue(number_format($Pagto->VALPBH->GetValue(), 2,',','.'));
   Pagto_VALPGT_BeforeShow($sender);
// -------------------------
//End Custom Code

//Close Pagto_VALPBH_BeforeShow @12-6619D1A6
    return $Pagto_VALPBH_BeforeShow;
}
//End Close Pagto_VALPBH_BeforeShow

//Pagto_VALPBH_OnValidate @12-18E4B74C
function Pagto_VALPBH_OnValidate(& $sender)
{
    $Pagto_VALPBH_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_VALPBH_OnValidate

//Custom Code @151-2A29BDB7
// -------------------------
    Pagto_VALPGT_OnValidate($sender);
// -------------------------
//End Custom Code

//Close Pagto_VALPBH_OnValidate @12-59E2B52F
    return $Pagto_VALPBH_OnValidate;
}
//End Close Pagto_VALPBH_OnValidate

//Pagto_lblJa_Exportada_BeforeShow @19-8B8F461A
function Pagto_lblJa_Exportada_BeforeShow(& $sender)
{
    $Pagto_lblJa_Exportada_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_lblJa_Exportada_BeforeShow

//Custom Code @46-2A29BDB7
// -------------------------
    // Write your own code here.
	$Pagto->lblJa_Exportada->SetValue($Pagto->EXPORT->GetValue()=="T" ? "J� Exportada":"N�o Exportada");
// -------------------------
//End Custom Code

//Close Pagto_lblJa_Exportada_BeforeShow @19-A9E33F80
    return $Pagto_lblJa_Exportada_BeforeShow;
}
//End Close Pagto_lblJa_Exportada_BeforeShow

//Pagto_VALFAT_BeforeShow @107-DFD700A8
function Pagto_VALFAT_BeforeShow(& $sender)
{
    $Pagto_VALFAT_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_VALFAT_BeforeShow

//Custom Code @148-2A29BDB7
// -------------------------
   Pagto_VALPGT_BeforeShow($sender);
// -------------------------
//End Custom Code

//Close Pagto_VALFAT_BeforeShow @107-8E9541E4
    return $Pagto_VALFAT_BeforeShow;
}
//End Close Pagto_VALFAT_BeforeShow

//Pagto_VALFAT_OnValidate @107-C86C8CEB
function Pagto_VALFAT_OnValidate(& $sender)
{
    $Pagto_VALFAT_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_VALFAT_OnValidate

//Custom Code @154-2A29BDB7
// -------------------------
    Pagto_VALPGT_OnValidate($sender);
// -------------------------
//End Custom Code

//Close Pagto_VALFAT_OnValidate @107-B16E256D
    return $Pagto_VALFAT_OnValidate;
}
//End Close Pagto_VALFAT_OnValidate

//Pagto_DATPGT_BeforeShow @8-ED308691
function Pagto_DATPGT_BeforeShow(& $sender)
{
    $Pagto_DATPGT_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_DATPGT_BeforeShow

//Custom Code @109-2A29BDB7
// -------------------------
    // Write your own code here.
    if ($Pagto->DATPGT->IsNull)
    {
        $Pagto->DATPGT->SetValue(CCGetSession("DataSist"));
    }
// -------------------------
//End Custom Code

//Close Pagto_DATPGT_BeforeShow @8-8F6E2974
    return $Pagto_DATPGT_BeforeShow;
}
//End Close Pagto_DATPGT_BeforeShow

//Pagto_VALPGT_BeforeShow @14-DD3F53A8
function Pagto_VALPGT_BeforeShow(& $sender)
{
    $Pagto_VALPGT_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_VALPGT_BeforeShow

//Custom Code @108-2A29BDB7
// -------------------------
   
   if(substr($Component->GetValue(), -3, 1)!="," && $Component->GetValue()!="")
     $Component->SetValue(number_format($Component->GetValue(), 2,',','.'));
    // Write your own code here.
	// C�lculo do INSS que inside em 11% sobre o valor total da fatura .
	//$nINSS=$Pagto->RET_INSS->GetValue();
	// Valor total da fatura menos a insid�ncia do INSS.
	//$Pagto->VALPGT->SetValue(($Pagto->VALFAT->GetValue()-$nINSS));
	/*if (CCGetParam("opcao","")=='3')
	{
		$Pagto->VALPGT->SetValue(0.00);
	}
	else
	{
		$Pagto->VALPGT->SetValue(1.11);
	}*/
	
// -------------------------
//End Custom Code

//Close Pagto_VALPGT_BeforeShow @14-19D9C26C
    return $Pagto_VALPGT_BeforeShow;
}
//End Close Pagto_VALPGT_BeforeShow

//Pagto_VALPGT_OnValidate @14-CA84DFEB
function Pagto_VALPGT_OnValidate(& $sender)
{
    $Pagto_VALPGT_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_VALPGT_OnValidate

//Custom Code @150-2A29BDB7
// -------------------------
   
   if(substr($Component->GetValue(), -3, 1)=="," &&  $Component->GetValue()!="")
     $Component->SetValue(number_format($Component->GetValue(), 2,'.'));
    
// -------------------------
//End Custom Code

//Close Pagto_VALPGT_OnValidate @14-2622A6E5
    return $Pagto_VALPGT_OnValidate;
}
//End Close Pagto_VALPGT_OnValidate

//Pagto_VALJUR_OnValidate @91-4F00D88D
function Pagto_VALJUR_OnValidate(& $sender)
{
    $Pagto_VALJUR_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_VALJUR_OnValidate

//Custom Code @153-2A29BDB7
// -------------------------
    Pagto_VALPGT_OnValidate($sender);
// -------------------------
//End Custom Code

//Close Pagto_VALJUR_OnValidate @91-2090473F
    return $Pagto_VALJUR_OnValidate;
}
//End Close Pagto_VALJUR_OnValidate

//Pagto_taxa_juros_BeforeShow @141-AA88C452
function Pagto_taxa_juros_BeforeShow(& $sender)
{
    $Pagto_taxa_juros_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_taxa_juros_BeforeShow

//Custom Code @142-2A29BDB7
// -------------------------
    $Pagto->taxa_juros->SetValue(CCGetSession("mTAXA_JUROS"));
// -------------------------
//End Custom Code

//Close Pagto_taxa_juros_BeforeShow @141-5A86524C
    return $Pagto_taxa_juros_BeforeShow;
}
//End Close Pagto_taxa_juros_BeforeShow

//Pagto_ISSQN_BeforeShow @99-EA348A81
function Pagto_ISSQN_BeforeShow(& $sender)
{
    $Pagto_ISSQN_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_ISSQN_BeforeShow

//Custom Code @111-2A29BDB7
// -------------------------
    // Write your own code here.
	/*$mMesRef = $Pagto->MESREF->GetValue();
	$nAux1 = 0+(substr($mMesRef,3,4).substr($mMesRef,0,2));
	if (($nAux1 > 200812) || ($Pagto->ESFERA->GetValue()=='T'))
	{
			$Pagto->ISSQN->SetValue(0);
	}*/
	
//	$Pagto->VALPBH->SetValue(number_format($sender->GetValue(), 2,',','.'));
  Pagto_VALPGT_BeforeShow($sender);
// -------------------------
//End Custom Code

//Close Pagto_ISSQN_BeforeShow @99-4E86E0F6
    return $Pagto_ISSQN_BeforeShow;
}
//End Close Pagto_ISSQN_BeforeShow

//Pagto_Label5_BeforeShow @161-35D5DCEB
function Pagto_Label5_BeforeShow(& $sender)
{
    $Pagto_Label5_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_Label5_BeforeShow

//Custom Code @165-2A29BDB7
// -------------------------
	    //S� exibe se houver permissao para dar baixa com valor diferente do calculado
		$permissao_requerida=array(32);
		$mOpcao = CCGetParam("opcao","");
		$perfil=CCGetSession("IDPerfil");
		
        if(verificaacesso($perfil,$permissao_requerida)==false || $mOpcao!="'2'" )
		  {
           $Component->Visible=false;
		  }

    
// -------------------------
//End Custom Code

//Close Pagto_Label5_BeforeShow @161-89EDFE84
    return $Pagto_Label5_BeforeShow;
}
//End Close Pagto_Label5_BeforeShow

//Pagto_senha_BeforeShow @159-4E60245C
function Pagto_senha_BeforeShow(& $sender)
{
    $Pagto_senha_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_senha_BeforeShow

//Custom Code @166-2A29BDB7
// -------------------------
	    //S� exibe se houver permissao para dar baixa com valor diferente do calculado
		$permissao_requerida=array(32);
		$mOpcao = CCGetParam("opcao","");
		$perfil=CCGetSession("IDPerfil");
		
        if(verificaacesso($perfil,$permissao_requerida)==false || $mOpcao!="'2'" )
		  {
           $Component->Visible=false;
		  }
// -------------------------
//End Custom Code

//Close Pagto_senha_BeforeShow @159-6857A0F4
    return $Pagto_senha_BeforeShow;
}
//End Close Pagto_senha_BeforeShow

//Pagto_Label4_BeforeShow @162-9DA38D63
function Pagto_Label4_BeforeShow(& $sender)
{
    $Pagto_Label4_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_Label4_BeforeShow

//Custom Code @167-2A29BDB7
// -------------------------
	    //S� exibe se houver permissao para dar baixa com valor diferente do calculado
		$permissao_requerida=array(32);
		$mOpcao = CCGetParam("opcao","");
		$perfil=CCGetSession("IDPerfil");
		
        if(verificaacesso($perfil,$permissao_requerida)==false || $mOpcao!="'2'" )
		  {
           $Component->Visible=false;
		  }
// -------------------------
//End Custom Code

//Close Pagto_Label4_BeforeShow @162-14E21FF2
    return $Pagto_Label4_BeforeShow;
}
//End Close Pagto_Label4_BeforeShow

//Pagto_CBAutorizacao_BeforeShow @163-5BED79EC
function Pagto_CBAutorizacao_BeforeShow(& $sender)
{
    $Pagto_CBAutorizacao_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_CBAutorizacao_BeforeShow

//Custom Code @168-2A29BDB7
// -------------------------
	    //S� exibe se houver permissao para dar baixa com valor diferente do calculado
		$permissao_requerida=array(32);
		$mOpcao = CCGetParam("opcao","");
		$perfil=CCGetSession("IDPerfil");
		
        if(verificaacesso($perfil,$permissao_requerida)==false || $mOpcao!="'2'" )
		  {
           $Component->Visible=false;
		  }
// -------------------------
//End Custom Code

//Close Pagto_CBAutorizacao_BeforeShow @163-D9FC70FD
    return $Pagto_CBAutorizacao_BeforeShow;
}
//End Close Pagto_CBAutorizacao_BeforeShow

//Pagto_BeforeShow @4-665D9AC6
function Pagto_BeforeShow(& $sender)
{
    $Pagto_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_BeforeShow

//Custom Code @126-2A29BDB7
// -------------------------
	$mOpcao = CCGetParam("opcao","");
	if ($mOpcao=="'1'")
	{
		$mTexto = 'Visualiza��o ';
	    $Pagto->Button_Update->Visible=false;
	}
	else if ($mOpcao=="'2'")
	{
		$mTexto = 'Pagamento - Baixa ';
	    $Pagto->Button_Update->Visible=true;
	}
	else
	{
		$mTexto = 'Cancelamento ';
	    $Pagto->Button_Update->Visible=true;
	}
	
	if ($Pagto->VALPGT->GetValue() > 0)
	{
		$Pagto->Label3->SetValue('pago');
	}
	else
	{
		$Pagto->Label3->SetValue('a pagar');
	}

// -------------------------
//End Custom Code

//Close Pagto_BeforeShow @4-C3C3E26F
    return $Pagto_BeforeShow;
}
//End Close Pagto_BeforeShow

//Pagto_BeforeUpdate @4-2F0388C7
function Pagto_BeforeUpdate(& $sender)
{
    $Pagto_BeforeUpdate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagto; //Compatibility
//End Pagto_BeforeUpdate

//Custom Code @171-2A29BDB7
// -------------------------

   
   

   $permissao_requerida=array(32);
   $mOpcao = CCGetParam("opcao","");
   $perfil=CCGetSession("IDPerfil");
		

   if(floatval($Pagto->VALPGT->Value)<>0)
     {


	  if( $Pagto->DATPGT->Value!="")
	    {

		  //Faz o calculo do valor da fatura
		  $Page = CCGetParentPage($sender);
	      $mDias = verificaData($Pagto->DATVNC->Value,$Pagto->DATPGT->Value);
		  if($mDias<0)
		   $mDias=0;

		  $mTaxa_Juros = floatval(str_replace(',','.',CCGetSession("mTAXA_JUROS")));
	  
	      $vfat  = floatval( str_replace(',','.',CCDLookUp('valfat','cadfat',"codfat='".$Pagto->CODFAT->Value."'",$Pagto->ds)) );
	      $mInss = floatval(str_replace(',','.',CCDLookUp('RET_INSS','cadfat',"codfat='".$Pagto->CODFAT->Value."'",$Pagto->ds)) );
	      $mVr_Multa = floatval(  round((($vfat-$mInss)*($mTaxa_Juros/100))/30*$mDias ,2  ));
	      $l_valpgt = $vfat + $mVr_Multa - $mInss;
  
	  
		  $mvalpgt=round(($vfat + $mVr_Multa - $mInss),2);

		  
	      
		  if($Pagto->VALPGT->Value<>$mvalpgt)
		    {
			 if(verificaacesso($perfil,$permissao_requerida)==true  )
			  {
           
		         if($Pagto->CBAutorizacao->Value==true)
				   {
                               
					$senha_usuario=CCDLookUp("PASSWO","TABOPE","ID=".CCGetSession("IDUsuario"),$Pagto->ds);
			 
		            if(    CCEncryptString($Pagto->senha->Value, "5")<> $senha_usuario  )
					  {
			            $Pagto->Errors->addError("Senha do usu�rio inv�lida");
						$Pagto->VALPGT->SetValue(CCDLookUp('valpgt','cadfat',"codfat='".$Pagto->CODFAT->Value."'",$Pagto->ds));
						$Pagto_BeforeUpdate=false;
					  }
                     else
					  {
                         $Pagto->DATABXADIF->SetValue(CCDLookUp("to_char(sysdate,'dd/mm/yyyy')","DUAL","",$Pagto->ds));
                         $Pagto->IDBXADIF->SetValue(CCGetSession("IDUsuario"));
						 $Pagto->VALBXADIF->SetValue($Pagto->VALPGT->GetValue());
					  }
				   }
		          else
				   {
		            $Pagto->Errors->addError("� preciso autoriza��o que a senha de autoriza��o do usu�rio e o campo confirma��o estejam marcados para dar baixa de fatura com valor difer�nte do calculado pelo sistema.");
					$Pagto->VALPGT->SetValue(CCDLookUp('valpgt','cadfat',"codfat='".$Pagto->CODFAT->Value."'",$Pagto->ds));
					$Pagto_BeforeUpdate=false;
				   }

			  }
	         else
			  {
		        $Pagto->Errors->addError("Voc� n�o tem autoriza��o para dar baixa de fatura com valor difer�nte do calculado pelo sistema.");
				$Pagto->VALPGT->SetValue(CCDLookUp('valpgt','cadfat',"codfat='".$Pagto->CODFAT->Value."'",$Pagto->ds));
				$Pagto_BeforeUpdate=false;
			  }

			}

		 }
		else
		 {
		  
		  $Pagto->Errors->addError("A data de pagamento � obrigat�ria quando o valor da fatura � preenchido.");
		  $Pagto->VALPGT->SetValue(CCDLookUp('valpgt','cadfat',"codfat='".$Pagto->CODFAT->Value."'",$Pagto->ds));
		  $Pagto_BeforeUpdate=false;
		  
		 }


	 }

  if($Pagto_BeforeUpdate==true)
	 {
	  
//      $Pagto->DATAMOD->Value=(CCDLookUp("to_char(sysdate,'dd/mm/yyyy')","DUAL","",$Pagto->ds));
      $Pagto->DATAMOD->SetValue(data_todbdata(CCDLookUp("to_char(sysdate,'dd/mm/yyyy')","DUAL","",$Pagto->ds)));
      $Pagto->IDMOD->SetValue(CCGetSession("IDUsuario"));
	  $Pagto->IDMOD->Validate();
      
	 }




// -------------------------
//End Custom Code

//Close Pagto_BeforeUpdate @4-39C534D5
    return $Pagto_BeforeUpdate;
}
//End Close Pagto_BeforeUpdate

//Servicos_QTDMED_BeforeShow @62-0AE1459B
function Servicos_QTDMED_BeforeShow(& $sender)
{
    $Servicos_QTDMED_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Servicos; //Compatibility
//End Servicos_QTDMED_BeforeShow

//DEL  // -------------------------
//DEL  	$Servicos->QTDMED->SetValue(number_format($Servicos->QTDMED->GetValue(), 2,',','.'));
//DEL  // -------------------------

//Custom Code @191-2A29BDB7
// -------------------------
    // Write your own code here.
// -------------------------
//End Custom Code

//Close Servicos_QTDMED_BeforeShow @62-787D978C
    return $Servicos_QTDMED_BeforeShow;
}
//End Close Servicos_QTDMED_BeforeShow

//Servicos_EQUPBH_X_VALPBH_BeforeShow @63-C7ABC05D
function Servicos_EQUPBH_X_VALPBH_BeforeShow(& $sender)
{
    $Servicos_EQUPBH_X_VALPBH_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Servicos; //Compatibility
//End Servicos_EQUPBH_X_VALPBH_BeforeShow

  // -------------------------
      // Write your own code here.
  	$mEquPBH = $Servicos->EQUPBH->GetValue();
  	$mValPBH = $Servicos->VALPBH->GetValue();
  	$Servicos->EQUPBH_X_VALPBH->SetValue(number_format(round($mEquPBH*$mValPBH,2), 2,',','.'));
  // -------------------------

//Custom Code @192-2A29BDB7
// -------------------------
    // Write your own code here.
// -------------------------
//End Custom Code

//Close Servicos_EQUPBH_X_VALPBH_BeforeShow @63-31F58000
    return $Servicos_EQUPBH_X_VALPBH_BeforeShow;
}
//End Close Servicos_EQUPBH_X_VALPBH_BeforeShow

//Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow @64-5AE11F3B
function Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow(& $sender)
{
    $Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Servicos; //Compatibility
//End Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow

  // -------------------------
      // Write your own code here.
/*
  	global $mTotalFat;

  	$mEquPBH = $Servicos->EQUPBH->GetValue();
  	$mValPBH = $Servicos->VALPBH->GetValue();
  	$mQtdMed = $Servicos->hdd_QTDMED->GetValue();
  	$Servicos->EQUPBH_X_VALPBH_X_QTMED->SetValue(number_format(round($mEquPBH*$mValPBH*$mQtdMed,2), 2,',','.'));

    $Servicos->EQUPBH_X_VALPBH_X_QTMED->SetValue($mValPBH*$mEquPBH*$mQtdMed);

  	$mTotalFat+=round($mEquPBH*$mValPBH*$mQtdMed,2);
*/
  // -------------------------

//Custom Code @193-2A29BDB7
// -------------------------
    // Write your own code here.
// -------------------------
//End Custom Code

//Close Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow @64-D15D496B
    return $Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow;
}
//End Close Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow

//Servicos_TOT_FAT_BeforeShow @104-B2F8693F
function Servicos_TOT_FAT_BeforeShow(& $sender)
{
    $Servicos_TOT_FAT_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Servicos; //Compatibility
//End Servicos_TOT_FAT_BeforeShow

//DEL  // -------------------------
//DEL  	global $mTotalFat;
//DEL      // Write your own code here.
//DEL  	// $mTotalFat � a soma dos servi�os individuais acumulados no 
//DEL  	// evento Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow() que det�m o valor
//DEL  	// individual de cada servi�o
//DEL  	$Servicos->TOT_FAT->SetValue(number_format(round($mTotalFat,2), 2,',','.'));
//DEL  
//DEL  // -------------------------

//Custom Code @194-2A29BDB7
// -------------------------
    // Write your own code here.
// -------------------------
//End Custom Code

//Close Servicos_TOT_FAT_BeforeShow @104-BEE238D2
    return $Servicos_TOT_FAT_BeforeShow;
}
//End Close Servicos_TOT_FAT_BeforeShow

//Servicos_lbl_Inss_BeforeShow @129-9509FE40
function Servicos_lbl_Inss_BeforeShow(& $sender)
{
    $Servicos_lbl_Inss_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Servicos; //Compatibility
//End Servicos_lbl_Inss_BeforeShow

//DEL  // -------------------------
//DEL  	$mINSS = (float)(str_replace(",", ".",CCGetSession("mINSS")));
//DEL  	$mINSS = number_format($mINSS, 2,',','.');
//DEL  	$Servicos->lbl_Inss->SetValue($mINSS);
//DEL  
//DEL  	$mISSQN = (float)(str_replace(",", ".",CCGetSession("mISSQN")));
//DEL  	$mISSQN = number_format($mISSQN, 2,',','.')	;
//DEL  	$Servicos->lbl_Issqn->SetValue($mISSQN);
//DEL  // -------------------------

//Custom Code @195-2A29BDB7
// -------------------------
    // Write your own code here.
// -------------------------
//End Custom Code

//Close Servicos_lbl_Inss_BeforeShow @129-DA67001D
    return $Servicos_lbl_Inss_BeforeShow;
}
//End Close Servicos_lbl_Inss_BeforeShow

//DEL  // -------------------------
//DEL  
//DEL  	
//DEL  
//DEL  // -------------------------

//DEL  // -------------------------
//DEL  
//DEL  
//DEL      
//DEL  // -------------------------

//DEL  // -------------------------
//DEL     global $DBFaturar;
//DEL     $mCodFat = $Pagto->CODFAT->GetValue();
//DEL     $DBFaturar->query("insert into cadfat_canc select * from cadfat where codfat = '$mCodFat'");
//DEL     $DBFaturar->query("delete from cadfat where codfat = '$mCodFat'"); 
//DEL  // -------------------------

//DEL  // -------------------------
//DEL      
//DEL  // -------------------------

//DEL  // -------------------------
//DEL  	global $mTotalFat;
//DEL      $Servicos->Label1->SetValue("Parcial -> $mTotalFat");
//DEL  // -------------------------

//Page_BeforeShow @1-CBA1F501
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $PagtoFatu_Dados; //Compatibility
//End Page_BeforeShow

//Custom Code @158-2A29BDB7
// -------------------------
	$mOpcao = CCGetParam("opcao","");
    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");

	if ($mOpcao=="'1'")
	{
	    //Visualiza��o
		$permissao_requerida=array(30);
        controleacesso($perfil,$permissao_requerida,"acessonegado.php");

		
	}
	else if ($mOpcao=="'2'")
	{
	    //Pagamento - baixa
		$permissao_requerida=array(31,32);
        controleacesso($perfil,$permissao_requerida,"acessonegado.php");
	}
	else
	{
	    //Cancelamento de pagamento
		$permissao_requerida=array(33);
        controleacesso($perfil,$permissao_requerida,"acessonegado.php");
	}
// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	// � o m�s / ano de refer�ncia.
//DEL  	$mMesRef = $Pagto->MESREF->GetValue();
//DEL  	// Pega o ano de refer�ncia e concatena o m�s e soma 0 tornando $nAux1
//DEL  	// um n�mero.Lembramos que no php a tipagem � din�mica.
//DEL  	$nAux1 = 0+(substr($mMesRef,3,4).substr($mMesRef,0,2));
//DEL  
//DEL  	if ($nAux1 > 199901)
//DEL  	{
//DEL  		// S� faz sentido calcular o INSS para faturas do M�s/Ano de refer�ncia
//DEL  		// acima de fevereiro de 1999.
//DEL  		// $mTotalFat � a soma dos servi�os individuais acumulados no 
//DEL  		// evento Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow() que det�m o valor
//DEL  		// individual de cada servi�o.
//DEL  		// C�lculo do INSS que inside em 11% sobre o valor total da fatura .
//DEL  		$nINSS=round(($Pagto->VALFAT->GetValue())*11/100,2);
//DEL  		$Pagto->RET_INSS->SetValue($nINSS);
//DEL  	}
//DEL  	else
//DEL  	{
//DEL  		$Pagto->RET_INSS->SetValue(0);
//DEL  	}
//DEL  
//DEL  // -------------------------
	//$Pagto->Button_Update->Visible = (CCGetParam("opcao","1")=="2");


//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	global $DBfaturar;
//DEL  	$mValPGT = $Pagto->VALPGT->GetValue();
//DEL  	$mDatPGT = $Pagto->DATPGT->GetValue();
//DEL  	$mCodFat = $Pagto->CODFAT->GetValue();
//DEL  	//$DBfaturar->query("update movfat set valpgt = $mValPGT,datpgt = to_date('$mDatPGT') where codfat = $mCodFat");
//DEL  
//DEL  // -------------------------

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	$Pagto->TextBox7->SetValue($Pagto->VALFAT->GetValue());
//DEL  // -------------------------
$mTotalFat = 0;

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	$Servicos->JUROS->SetValue($Pagto->VALJUR->GetValue());
//DEL  // -------------------------

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	$Servicos->Ret_ISSQN->SetValue($Pagto->ESFERA->GetValue()=='T' ? "Esfera Municipal, Estadual ou Federal":"Retencao de ISSQN");
//DEL  // -------------------------

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	$mMesRef = $Pagto->TextBox7->GetValue();
//DEL  	$nAux1 = 0+(substr($mMesRef,3,4).substr($mMesRef,0,2));
//DEL  	if (($nAux1 > 200812) || ($Pagto->ESFERA->GetValue()=='T'))
//DEL  	{
//DEL  			$Servicos->ISSQN->SetValue(0);
//DEL  	}
//DEL  	else
//DEL  	{
//DEL  			$Servicos->ISSQN->SetValue($Pagto->ISSQN->GetValue());
//DEL  	}
//DEL  // -------------------------


//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	// � o m�s / ano de refer�ncia.
//DEL  	$mMesRef = $Pagto->TextBox7->GetValue();
//DEL  	// Pega o ano de refer�ncia e concatena o m�s e soma 0 tornando $nAux1
//DEL  	// um n�mero.Lembramos que no php a tipagem � din�mica.
//DEL  	$nAux1 = 0+(substr($mMesRef,3,4).substr($mMesRef,0,2));
//DEL  
//DEL  	if ($nAux1 > 199901)
//DEL  	{
//DEL  		// S� faz sentido calcular o INSS para faturas do M�s/Ano de refer�ncia
//DEL  		// acima de fevereiro de 1999.
//DEL  		// $mTotalFat � a soma dos servi�os individuais acumulados no 
//DEL  		// evento Servicos_EQUPBH_X_VALPBH_X_QTMED_BeforeShow() que det�m o valor
//DEL  		// individual de cada servi�o.
//DEL  		// C�lculo do INSS que inside em 11% sobre o valor total da fatura .
//DEL  		$nINSS=round(($mTotalFat)*11/100,2);
//DEL  		$Servicos->INSS->SetValue($nINSS);
//DEL  	}
//DEL  	else
//DEL  	{
//DEL  		$Servicos->INSS->SetValue(0);
//DEL  	}
//DEL  
//DEL  // -------------------------



//DEL  function Servicos_Label2_BeforeShow(& $sender)
//DEL  {
//DEL      $Servicos_Label2_BeforeShow = true;
//DEL      $Component = & $sender;
//DEL      $Container = & CCGetParentContainer($sender);
//DEL  	global $Pagto;
//DEL      global $Servicos; //Compatibility

?>
