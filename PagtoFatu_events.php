<?php
//BindEvents Method @1-1B9CF967
function BindEvents()
{
    global $Pagamento;
    global $CCSEvents;
    $Pagamento->lblTitulo->CCSEvents["BeforeShow"] = "Pagamento_lblTitulo_BeforeShow";
    $Pagamento->Button_Insert->CCSEvents["OnClick"] = "Pagamento_Button_Insert_OnClick";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//Pagamento_lblTitulo_BeforeShow @11-1EE98F2D
function Pagamento_lblTitulo_BeforeShow(& $sender)
{
    $Pagamento_lblTitulo_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagamento; //Compatibility
//End Pagamento_lblTitulo_BeforeShow

//Custom Code @12-2A29BDB7
// -------------------------
	$mOpcao = CCGetParam("opcao","");
	if ($mOpcao=="'1'")
	{
		$mTexto = 'Visualiza��o ';
	}
	else if ($mOpcao=="'2'")
	{
		$mTexto = 'Pagamento - Baixa ';
	}
	else if ($mOpcao=="'3'")
	{
		$mTexto = 'Estorno ';
	}
	else if ($mOpcao=="'4'")
	{
		$mTexto = 'Cancelamento ';
	}
	else
	{
		$mTexto = 'Recupera��o ';
	}
    $Pagamento->lblTitulo->SetValue($mTexto);
// -------------------------
//End Custom Code

//Close Pagamento_lblTitulo_BeforeShow @11-5F5E0BDF
    return $Pagamento_lblTitulo_BeforeShow;
}
//End Close Pagamento_lblTitulo_BeforeShow

//Pagamento_Button_Insert_OnClick @6-41641F37
function Pagamento_Button_Insert_OnClick(& $sender)
{
    $Pagamento_Button_Insert_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Pagamento; //Compatibility
//End Pagamento_Button_Insert_OnClick
	global $Redirect;
//DLookup @9-5471BFF0
	//$Pagamento->tbxFatura->SetValue($Pagamento->tbxFatura->GetValue().'*');
    global $DBfaturar;
    $Page = CCGetParentPage($sender);
	$cod_fat = $Pagamento->tbxFatura->GetValue();
	$opcao = CCGetParam("opcao","2");
	if ($opcao!="'5'")
	{
    	$ccs_result = CCDLookUp("c.valpgt", "cadfat c,cadcli a", "c.codfat = '$cod_fat' and c.codcli = a.codcli", $Page->Connections["Faturar"]);
    	if ($ccs_result=="")
		{

            $result_consulta = CCDLookUp("c.codcli", "cadfat_canc c", "c.codfat = '$cod_fat' ", $Page->Connections["Faturar"]);


			if($result_consulta!="")
			  {
               $result_consulta = CCDLookUp("c.codcli", "cadfat_canc c", "c.codfat = '$cod_fat' ", $Page->Connections["Faturar"]);
			   if($result_consulta!="")
			     {
                  $Redirect = "PagtoFatu_Dados_Ret.php?CODFAT=$cod_fat&opcao=$opcao";  
			     }
			  }
             else
			  {
				$Pagamento->Errors->addError("Fatura ainda n�o criada - redigite.");
				$Pagamento_Button_Insert_OnClick = false;
			  } 

		}
	}
	else
	{
    	$ccs_result = CCDLookUp("c.valpgt", "cadfat_canc c,cadcli a", "c.codfat = '$cod_fat' and c.codcli = a.codcli", $Page->Connections["Faturar"]);
    	if ($ccs_result=="")
		{
			$Pagamento->Errors->addError("Fatura ainda n�o cancelada - redigite.");
			$Pagamento_Button_Insert_OnClick = false;
		}
	}
    
	if ($ccs_result!="")
	{

		if ($ccs_result+0 > 0 && $opcao=="'2'")
		{
			$opcao = "'1'";
		}
		if ($opcao=="'1'" || $opcao=="'2'")
		{
			$Redirect = "PagtoFatu_Dados.php?CODFAT=$cod_fat&opcao=$opcao";
			CCSetSession("opcao",$opcao);
			CCSetSession("CODFAT",$opcao);


		}
		else if ($opcao=="'3'")
		{
	   		if ($ccs_result+0 > 0)
	   		{
			  	$Redirect = "PagtoFatu_Dados_Est.php?CODFAT=$cod_fat&opcao=$opcao";
       		}
	   		else
	   		{
		      	$Pagamento->Errors->addError("Fatura n�o foi paga ainda.");
	      		$Pagamento_Button_Insert_OnClick = false;   
	   		}
		}
		else if ($opcao=="'4'")
		{

	   		if ($ccs_result+0 > 0)
	   		  {
		      	$Pagamento->Errors->addError("Fatura j� paga");
	      	  	$Pagamento_Button_Insert_OnClick = false;   
       		  }
	   		else
	   		 {
				$Redirect = "PagtoFatu_Dados_Canc.php?CODFAT=$cod_fat&opcao=$opcao";
 	   		 }
		}
		else if ($opcao=="'5'")
		{
	   		//if ($ccs_result+0 > 0)
	   		//{
				$Redirect = "PagtoFatu_Dados_Ret.php?CODFAT=$cod_fat&opcao=$opcao";
       		//}
	   		//else
	   		//{
		    //  	$Pagamento->Errors->addError("Fatura n�o foi paga ainda.");
	      	//	$Pagamento_Button_Insert_OnClick = false;   
	   		//}
		}
	}
//End DLookup

//Close Pagamento_Button_Insert_OnClick @6-FFB53AFB
    return $Pagamento_Button_Insert_OnClick;
}
//End Close Pagamento_Button_Insert_OnClick

//Page_BeforeShow @1-CDE753B0
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $PagtoFatu; //Compatibility
//End Page_BeforeShow

//Custom Code @16-2A29BDB7
// -------------------------
  include("controle_acesso.php");
  $opcao = CCGetParam("opcao","");

  switch($opcao)
  {
   case "'1'":
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(30);
	controleacesso($perfil,$permissao_requerida,"acessonegado.php");
   break;
   case "'2'":
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(31);
	controleacesso($perfil,$permissao_requerida,"acessonegado.php");
   break;
   case "'3'":
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(33);
	controleacesso($perfil,$permissao_requerida,"acessonegado.php");
   break;
   case "'4'":
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(34);
	controleacesso($perfil,$permissao_requerida,"acessonegado.php");
   break;
   case "'5'":
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(35);
	controleacesso($perfil,$permissao_requerida,"acessonegado.php");
   break;



  }



// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
