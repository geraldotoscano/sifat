<?php
	error_reporting (E_ALL);
	define("RelativePath", "../..");
	define('FPDF_FONTPATH','fpdf/font/');
	require('fpdf/fpdi.php');
	include(RelativePath . "/Common.php");
	include(RelativePath . "/Template.php");
	include(RelativePath . "/Sorter.php");
	include(RelativePath . "/Navigator.php");
	$pdf= new fpdi();

	$pagecount = $pdf->setSourceFile("fpdf/templates/blank.pdf");
	$tplidx = $pdf->ImportPage(1);

	//$pdf->SetFont('Times','',10);
	$pdf->SetTextColor(0, 0, 0);
	$pdf->SetAutoPageBreak(0);
	$pdf->addPage('Landscape'); 
    $pdf->useTemplate($tplidx,0,0,210);

	$pdf->SetFont('Times','',10);
	// dimens�es da folha A4 - Largura = 210.6 mm e Comprimento 296.93 mm em Portrait. Em Landscape � s� inverter.
	$pdf->SetXY(10,10);
	//'SECRETARIA MUNICIPAL DE EDUCA��O DE BELO HORIZONTE'
	$pdf->Cell(279,190,'',1,1,'C');
	// Meio = (286/2) - (50/2) = 148,3 - 25 = 123,3
	$pdf->Text(98,14,'SECRETARIA MUNICIPAL DE EDUCA��O DE BELO HORIZONTE');
	$pdf->Text(128,19,'GER�NCIA DE TRANSPORTE');
	$pdf->Text(96,24,'PLANILHA DE APURA��O DE VIAGENS - TRANSPORTE ESCOLAR');
	
	$pdf->SetXY(12,29);
	$pdf->Cell(20,05.5,'EMPRESA:',1,1,'L');//135
	
	$pdf->SetXY(32,29);
	$pdf->Cell(115,05.5,'',1,1,'C');
	
	$pdf->SetXY(152,29);
	$pdf->Cell(20,05.5,'N� �NIBUS:',1,1,'L');//135
	
	$pdf->SetXY(172,29);
	$pdf->Cell(115,05.5,'',1,1,'C');
	
	$pdf->SetXY(12,34.5);
	$pdf->Cell(20,05.5,'LOTE:',1,1,'L');//135
	
	$pdf->SetXY(32,34.5);
	$pdf->Cell(115,05.5,'',1,1,'C');
	
	$pdf->SetXY(152,34.5);
	$pdf->Cell(20,05.5,'PLACA:',1,1,'L');//135
	
	$pdf->SetXY(172,34.5);
	$pdf->Cell(115,05.5,'',1,1,'C');

	$pdf->SetXY(12,40);
	$pdf->Cell(20,05.5,'ESCOLA:',1,1,'L');//135
	
	$pdf->SetXY(32,40);
	$pdf->Cell(115,05.5,'',1,1,'C');
	
	$pdf->SetXY(152,40);
	$pdf->Cell(20,05.5,'TURNO(S):',1,1,'L');//135
	
	$pdf->SetXY(172,40);
	$pdf->Cell(115,05.5,'',1,1,'C');

	$pdf->Text(28,52,'SR.(A) DIRETOR(A): FAVOR RUBRICAR NO CALEND�RIO ABAIXO TODOS OS DIAS TRABALHADOS. PREENCHER UMA PLANILHA PARA CADA �NIBUS.');

	$pdf->SetXY(12,57);
	$pdf->Cell(135,05.5,'MANH�',1,1,'C');

	$pdf->SetXY(152,57);
	$pdf->Cell(135,05.5,'TARDE',1,1,'C');
	
	$anoAtual  = CCGetParam("AnoAtual");
	$mesAtual  = CCGetParam("mesAtual");
	$diaSemana = CCGetParam("diaSemana"); // 0 = domingo

	$pdf->SetFont('Times','',12);
	$pdf->SetXY(12,62.5);
	$pdf->Cell(135,05.5,$mesAtual.' '.$anoAtual,1,1,'C');

	$pdf->SetXY(152,62.5);
	$pdf->Cell(135,05.5,$mesAtual.' '.$anoAtual,1,1,'C');
	
	/*
	    Para montar este calend�rio � necess�rio:
			a) Ter um dado tipo data come�ando do dia 1�.
				Obs : A impress�o deve iniciar no dia da semana referente e este primeiro dia do m�s.
			b) Saber o dia da semana do primeiro dia da data.
			c) Saber o �ltimo dia da data (30,31,28 ou 29)
			   select to_number(substr(to_char(last_day(to_date('01/04/2009','dd/mm/yyyy')),'dd/mm/yyyy'),1,2)) from dual;

	*/
	$mes = array("Janeiro"=>"01","Fevereiro"=>"02","Mar�o"=>"03","Abril"=>"04","Maio"=>"05","Junho"=>"06","Julho"=>"07","Agosto"=>"08","Setembro"=>"09","Outubro"=>"10","Novembro"=>"11","Dezembro"=>"12");
	$dataUltimoDia = '01/'.$mes[$mesAtual].'/'.$anoAtual;
	$conex1 = new clsDBOracle1;
	$conex1->query("select to_number(substr(to_char(last_day(to_date('".$dataUltimoDia."','dd/mm/yyyy')),'dd/mm/yyyy'),1,2)) as ultimoDia from dual");
	if ($conex1->next_record())
	{
		$maiorDiaMes = $conex1->f("ultimoDia");
	}
	//SetFillColor(int r [, int g, int b])
	$pdf->SetFillColor(192, 192, 192);
	$pdf->SetXY(12,68);
	$pdf->Cell(19.2,05.5,'Domingo',1,1,'C',1);

	$pdf->SetXY(31.2,68);
	$pdf->Cell(19.2,05.5,'Segunda',1,1,'C');

	$pdf->SetXY(50.4,68);
	$pdf->Cell(19.2,05.5,'Ter�a',1,1,'C');

	$pdf->SetXY(69.6,68);
	$pdf->Cell(19.2,05.5,'Quarta',1,1,'C');

	$pdf->SetXY(88.8,68);
	$pdf->Cell(19.2,05.5,'Quinta',1,1,'C');

	$pdf->SetXY(108,68);
	$pdf->Cell(19.2,05.5,'Sexta',1,1,'C');

	$pdf->SetXY(127.2,68);
	$pdf->Cell(19.8,05.5,'S�bado',1,1,'C');

	//$pdf->SetXY(152,62.5);
	//$pdf->Cell(135,05.5,$mesAtual.' '.$anoAtual,1,1,'C');
	//Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, int fill [, mixed link]]]]]]])
	
	//$pdf->SetTextColor(128, 0, 128);
	$pdf->SetXY(152,68);
	$pdf->Cell(19.2,05.5,'Domingo',1,1,'C',1);

	$pdf->SetXY(171.2,68);
	$pdf->Cell(19.2,05.5,'Segunda',1,1,'C');

	$pdf->SetXY(190.4,68);
	$pdf->Cell(19.2,05.5,'Ter�a',1,1,'C');

	$pdf->SetXY(209.6,68);
	$pdf->Cell(19.2,05.5,'Quarta',1,1,'C');

	$pdf->SetXY(228.8,68);
	$pdf->Cell(19.2,05.5,'Quinta',1,1,'C');

	$pdf->SetXY(248,68);
	$pdf->Cell(19.2,05.5,'Sexta',1,1,'C');

	$pdf->SetXY(267.2,68);
	$pdf->Cell(19.8,05.5,'S�bado',1,1,'C');
	
	$linha = 68;
	//for($diasMes=1;$diasMes<=28;$diasMes+=7)
	$diaMes  = '';
	$diaMes1 = '';
	/*
	    O n�mero de linhas do calend�rio depende da quantidade de dias no m�s e em que dia da semana o pri-
	  meiro dia do m�s vai aparecer.
	     As poss�veis quantidades de dias em um m�s s�o de 28,29,30 ou 31.
		 Os poss�veis dias da semana em que o primeiro dia do m�s pode aparecer s�o Domingo,Segunda,Ter�a,Quarta
		 Quinta,Sexta ou S�bado.
		 Se a quantidade de dias no m�s for 28 e:
			o dia da semana do primeiro dia do mes for domingo, teremos um calend�rio de 4 linhas.
			o dia da semana do primeiro dia do mes estiver entre Segunda e S�bado , teremos um calend�rio de 5 
			linhas.
		 Se a quantidade de dias no m�s for 29 e:
			o dia da semana do primeiro dia do mes estiver entre domingo e S�bado, teremos um calend�rio de 5 
			linhas. OBS: Neste caso n�o precisa fazer este teste. Verificar se a quantidade de dias no m�s � 29
			j� � suficiente.
		 Se a quantidade de dias no m�s for 30 e:
			o dia da semana do primeiro dia do mes for estiver entre domingo e Sexta, teremos um calend�rio de 5 
			linhas.
			o dia da semana do primeiro dia do mes for S�bado, teremos um calend�rio de 6 linhas.
		 Se a quantidade de dias no m�s for 31 e:
			o dia da semana do primeiro dia do mes estiver entre domingo e Quinta, teremos um calend�rio de 5 
			linhas.
			o dia da semana do primeiro dia do mes for Sexta ou S�bado, teremos um calend�rio de 6 linhas.
			
	*/
	// Detec��o do n�mero de linhas do calend�rio.
	if ($maiorDiaMes == 28 && $diaSemana == 0) // domingo
	{
		$numLinhCale = 4;
	}
	elseif ($maiorDiaMes == 28 && ($diaSemana > 0 && $diaSemana < 7 )) // Entre Segunda e S�bado
	{
		$numLinhCale = 5;
	}
	elseif ($maiorDiaMes == 29) // Entre Segunda e S�bado
	{
		$numLinhCale = 5;
	}
	elseif ($maiorDiaMes == 30 && ($diaSemana >= 0 && $diaSemana < 6 )) // Entre Domingo e Sexta
	{
		$numLinhCale = 5;
	}
	elseif ($maiorDiaMes == 30 && $diaSemana == 6 ) // S�bado
	{
		$numLinhCale = 6;
	}
	elseif ($maiorDiaMes == 31 && ($diaSemana >= 0 && $diaSemana < 5 )) // Entre Domingo e Quinta
	{
		$numLinhCale = 5;
	}
	else
	{
		$numLinhCale = 6;
	}

	// Exibi��o(impress�o) do calend�rio com o n�mero de linhas detectatdas totalmente em branco.
	
	for($x=1;$x<=$numLinhCale;$x++)
	{
		$linha += 05.5;
			// Monta a linha numerada do primeiro quadro.
		
			//if ($diaSemana==0){$diaMes++;if($diaMes>$maiorDiaMes){$diaMes='';}}else{if($diaMes<$maiorDiaMes && $diaMes!=0){$diaMes++;}else{$diaMes='';}}
			$pdf->SetXY(12,$linha);
			$pdf->Cell(19.2,05.5,$diaMes,1,1,'C',1); // Domingo

			//if ($diaSemana==1){$diaMes++;if($diaMes>$maiorDiaMes){$diaMes='';}}
			$pdf->SetXY(31.2,$linha);
			$pdf->Cell(19.2,05.5,$diaMes,1,1,'C');// Segunda

			//if ($diaSemana==2){$diaMes++;if($diaMes>$maiorDiaMes){$diaMes='';}}
			$pdf->SetXY(50.4,$linha);
			$pdf->Cell(19.2,05.5,$diaMes,1,1,'C');// ter�a

			//if ($diaSemana==3){$diaMes++;if($diaMes>$maiorDiaMes){$diaMes='';}}
			$pdf->SetXY(69.6,$linha);
			$pdf->Cell(19.2,05.5,$diaMes,1,1,'C');// Quarta

			//if ($diaSemana==4){$diaMes++;if($diaMes>$maiorDiaMes){$diaMes='';}}
			$pdf->SetXY(88.8,$linha);
			$pdf->Cell(19.2,05.5,$diaMes,1,1,'C');// Quinta

			//if ($diaSemana==5){$diaMes++;if($diaMes>$maiorDiaMes){$diaMes='';}}
			$pdf->SetXY(108,$linha);
			$pdf->Cell(19.2,05.5,$diaMes,1,1,'C');// Sexta

			//if ($diaSemana==6){$diaMes++;if($diaMes>$maiorDiaMes){$diaMes='';}}
			$pdf->SetXY(127.2,$linha);
			$pdf->Cell(19.8,05.5,$diaMes,1,1,'C');// S�bado
	
			// Monta a linha numerada do segundo quadro.
		
			//if ($diaSemana==0){$diaMes1++;if($diaMes1>$maiorDiaMes){$diaMes1='';}}
			$pdf->SetXY(152,$linha);
			$pdf->Cell(19.2,05.5,$diaMes1,1,1,'C',1);// Domingo

			//if ($diaSemana==1){$diaMes1++;if($diaMes1>$maiorDiaMes){$diaMes1='';}}
			$pdf->SetXY(171.2,$linha);
			$pdf->Cell(19.2,05.5,$diaMes1,1,1,'C');// Segunda

			//if ($diaSemana==2){$diaMes1++;if($diaMes1>$maiorDiaMes){$diaMes1='';}}
			$pdf->SetXY(190.4,$linha);
			$pdf->Cell(19.2,05.5,$diaMes1,1,1,'C');// Ter�a

			//if ($diaSemana==3){$diaMes1++;if($diaMes1>$maiorDiaMes){$diaMes1='';}}
			$pdf->SetXY(209.6,$linha);
			$pdf->Cell(19.2,05.5,$diaMes1,1,1,'C');// Quarta

			//if ($diaSemana==4){$diaMes1++;if($diaMes1>$maiorDiaMes){$diaMes1='';}}
			$pdf->SetXY(228.8,$linha);
			$pdf->Cell(19.2,05.5,$diaMes1,1,1,'C');// Quinta

			//if ($diaSemana==5){$diaMes1++;if($diaMes1>$maiorDiaMes){$diaMes1='';}}
			$pdf->SetXY(248,$linha);
			$pdf->Cell(19.2,05.5,$diaMes1,1,1,'C');// Sexta

			//if ($diaSemana==6){$diaMes1++;if($diaMes1>$maiorDiaMes){$diaMes1='';}}
			$pdf->SetXY(267.2,$linha);
			$pdf->Cell(19.8,05.5,$diaMes1,1,1,'C');// S�bado
	}
	/*
		Para mostrar os n�mreros no calend�rio � necess�rio saber:
			a) em qual dia da semana se iniciar� a amostragem dos n�meros dos dias do m�s. 
	*/
	if ($diaSemana==0)
	{
		$colIni1 = 12;
		$colIni2 = 152;
	}
	else
	{
		if ($diaSemana==1)
		{
			$colIni1 = 31.2;
			$colIni2 = 171.2;
		}
		else
		{
			if ($diaSemana==2)
			{
				$colIni1 = 50.4;
				$colIni2 = 190.4;
			}
			else
			{
				if ($diaSemana==3)
				{
					$colIni1 = 69.6;
					$colIni2 = 209.6;
				}	
				else
				{
					if ($diaSemana==4)
					{
						$colIni1 = 88.8;
						$colIni2 = 228.8;
					}
					else
					{
						if ($diaSemana==5)
						{
							$colIni1 = 108;
							$colIni2 = 248;
						}
						else
						{
							$colIni1 = 127.2;
							$colIni2 = 267.2;
						}
					}
				}
			}
		}
	}
	/* Exibi��o do calend�rio com os n�meros dos dias do mes.
	   Para a exibi��o dos n�meros dos dias do m�s no calendpario em branco pr�-impresso � necess�rio:
	      1� - Saber em que dia da semana inicia-se a impress�o do primeiro dia do m�s.
		  2� - Saber onde termina a impress�o, neste caso, a �ltima c�lula de impress�o (mais especificamente a
		       a c�lula 127.2).
		  3� - Ap�s a impress�o da �ltima c�lula, reiniciar a impress�o sempre a partir da primeira c�lula.
		       Colunas 12 e 152.
		  4� - Implementar um contador de dias do mes iniciando em 1 e terminando no �ltimo dia do m�s em quest�o.
		       �ltimo dia do m�s est� em $maiorDiaMes.
		  5� - Completar a impress�o das c�lulas restantes com brancos ou simplesmente n�o imprimir nada pois o
		       calend�rio � pr�-impresso.
	*/
	$diaMes = 1;
	$linha  = 68;
	for($x=1;$x<=$numLinhCale;$x++)
	{
		$linha += 05.5;
		for ($col=$colIni1;$col<=127.2;$col+=19.2)
		{
			if ($diaMes <= $maiorDiaMes)
			{
				// Monta a linha numerada do primeiro quadro.
				$pdf->SetXY($col,$linha);
				$pdf->Cell(19.2,05.5,$diaMes,0,1,'C'); // Domingo
				// Monta a linha numerada do primeiro quadro.
				$pdf->SetXY($colIni2,$linha);
				$pdf->Cell(19.2,05.5,$diaMes,0,1,'C');// Domingo
				$colIni2+=19.2;
				$diaMes++;
			}
			else
			{
				$col = 127.2;
				$x = $numLinhCale + 1;
				// Monta a linha numerada do primeiro quadro.
				//$pdf->SetXY($col,$linha);
				//$pdf->Cell(19.2,05.5,'',1,1,'C'); // Imprime em branco
				// Monta a linha numerada do primeiro quadro.
				//$pdf->SetXY($colIni2,$linha);
				//$pdf->Cell(19.2,05.5,'',1,1,'C');// Imprime em branco
				//$colIni2+=19.2;
			}
		}
		$colIni1 = 12;
		$colIni2 = 152;
	}
	
	// Exibi��o das linhas abaixo do calend�rio.
	
	$linha += (3 * 05.5);
	$pdf->SetFont('Times','',10);
	$pdf->Text(12,$linha,'N� DIAS LETIVOS (CALEND�RIO ESCOLAR MANH�):');
	$pdf->SetXY(100,($linha-4.6));
	$pdf->Cell(20,07.5,'',1,1,'C');
	
	$pdf->Text(152,$linha,'N� DIAS LETIVOS (CALEND�RIO ESCOLAR TARDE):');
	$pdf->SetXY(239,($linha-4.6));
	$pdf->Cell(20,07.5,'',1,1,'C');
		
	$linha += (2 * 05.5);
	$pdf->Text(12,$linha,'N� DIAS TRABALHADOS MANH�:');
	$pdf->SetXY(69,($linha-4.6));
	$pdf->Cell(20,07.5,'',1,1,'C');
	
	$pdf->Text(152,$linha,'N� DIAS TRABALHADOS TARDE:');
	$pdf->SetXY(208,($linha-4.6));
	$pdf->Cell(20,07.5,'',1,1,'C');
		
	$linha += (2 * 05.5);
	$pdf->Text(12,$linha,'KM ROTA MANH�(PREENCHIMENTO GETRAN):');
	$pdf->SetXY(92,($linha-4.6));
	$pdf->Cell(20,07.5,'',1,1,'C');
	
	$pdf->Text(152,$linha,'KM ROTA TARDE(PREENCHIMENTO GETRAN):');
	$pdf->SetXY(231,($linha-4.6));
	$pdf->Cell(20,07.5,'',1,1,'C');
	

	// Impress�o do campo observa��es
	$linha += (2 * 05.5);
	$pdf->SetXY(12,($linha-4.6));
	$pdf->Cell(275,05.5,'OBSERVA��ES:',1,1,'L');
	$linha += 05.5;
	$pdf->SetXY(12,($linha-4.6));
	$pdf->Cell(275,05.5,'',1,1,'');
	$linha += 05.5;
	$pdf->SetXY(12,($linha-4.6));
	$pdf->Cell(275,05.5,'',1,1,'');
	$linha += 05.5;
	$pdf->SetXY(12,($linha-4.6));
	$pdf->Cell(275,05.5,'',1,1,'');
	$linha += 05.5;
	$pdf->SetXY(12,($linha-4.6));
	$pdf->Cell(275,05.5,'',1,1,'');
	
	//           F  I  N  A  L  I  Z  A  �  �  O
	
	$linha += (5.5 * 05.5);
	$pdf->Text(20,$linha,'CARIMBO E ASSINATURA ESCOLA');
	$pdf->Text(120,$linha,'CARIMBO E ASSINATURA EMPRESA');
	$pdf->Text(215,$linha,'CARIMBO E ASSINATURA GETRAN');
		
	$pdf->Output();
	$pdf->closeParsers();
?>