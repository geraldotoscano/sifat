<?php
	error_reporting (E_ALL);
	//define("RelativePath", ".");
	//define('FPDF_FONTPATH','/pdf/font/');
	require('pdf/fpdf.php');
	//include('pdf/fpdi.php');
	//require_once("pdf/fpdi_pdf_parser.php");
	//require_once("pdf/fpdf_tpl.php");
	//include(RelativePath . "/Common.php");
	//include(RelativePath . "/Template.php");
	//include(RelativePath . "/Sorter.php");
	//include(RelativePath . "/Navigator.php");
	//include_once(RelativePath . "/cabec.php");
class relArrEmisPDF extends fpdf {	
	var $titulo;
	var $mesRef;
	var $posInscricao;
	var $posCliente;
	var $posFatura;
	var $posEmissao;
	var $posValFat;
	var $posValFatcanc;
	var $posVenc;
	var $totalGeral;
	var $StringTam;
	var $relat;
	var $mudouDistrito;
	var $distrito;
	var $posMesAno;
	var $posINSS;
	var $posINSScanc;
	var $posPGT;
	var $posAcres;
	var $posRec;
	var $posISSQN;
	var $posJuros;
	var $posValRec;
	//$pdf= new fpdi();
	function relatorio($titu,$mesAno,$rel,$csv=false)
	{
		$this->mesRef = $mesAno;
		
		$this->titulo = $titu;
		
		$Tabela   = new clsDBfaturar();

		$Tabela->query("
		SELECT
		  F.CODCLI,
		  to_char(F.DATEMI,'dd/mm/yyyy') AS DATEMI,
		  to_char(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
		  to_char(F.DATPGT,'dd/mm/yyyy') AS DATPGT,
		  F.CODFAT,
		  F.VALFAT,
		  F.ISSQN,
		  F.RET_INSS,
		  F.JUROS,
		  F.VALPGT,
		  F.CANCELADA,
		  C.DESCLI,
		  C.CGCCPF,
		  C.ESFERA,
		  T.TOTGER,
		  T.TOTGER_CANC,
		  T.TOTISSQN,
		  T.TOTRET_INSS,
		  T.TOTRET_INSS_CANC,
		  T.TOTJUROS,
		  T.TOTVALPGT
		FROM
		  (
		    /*consulta faturas ativas e canceladas do per�odo (precisa de UNION pq ficam em tabelas distintas)*/
		    SELECT
		      CODFAT,
		      CODCLI,
		      MESREF,
		      DATEMI,
		      DATVNC,
		      DATPGT,
		      VALFAT,
		      ISSQN,
		      RET_INSS,
		      DECODE((DATPGT-DATVNC),ABS((DATPGT-DATVNC)),ROUND(VALMUL*(DATPGT-DATVNC),2),0) AS JUROS,
		      VALPGT,
		      0 AS CANCELADA
		    FROM
		      CADFAT
		    WHERE
		      MESREF='{$mesAno}'
  
		    UNION

		    SELECT
		      CODFAT,
		      CODCLI,
		      MESREF,
		      DATEMI,
		      DATVNC,
		      DATPGT,
		      VALFAT,
		      ISSQN,
		      RET_INSS,
		      DECODE((DATPGT-DATVNC),ABS((DATPGT-DATVNC)),ROUND(VALMUL*(DATPGT-DATVNC),2),0) AS JUROS,
		      VALPGT,
		      1 AS CANCELADA
		    FROM
		      CADFAT_CANC
		    WHERE
		      MESREF='{$mesAno}'
		  ) F,
		  CADCLI C,
		  (
		    /*consulta TOTAL das faturas ativas e canceladas do per�odo (precisa de UNION pq ficam em tabelas distintas)*/
		    SELECT 
		      SUM(VALFAT) AS TOTGER,
		      SUM(VALFAT_CANC) AS TOTGER_CANC,
		      SUM(RET_INSS) AS TOTRET_INSS,
		      SUM(RET_INSS_CANC) AS TOTRET_INSS_CANC,
		      SUM(JUROS) AS TOTJUROS,
		      SUM(VALPGT) AS TOTVALPGT,
		      (
		        /*a soma de ISSQN considerava somente faturas antes de 2008/12, o comportamento foi mantido*/
		        SELECT
		          SUM(ISSQN)
		        FROM
		          CADFAT
		        WHERE 
		          MESREF='{$mesAno}' AND
		          SUBSTR(MESREF,4,4)||SUBSTR(MESREF,1,2) < '200812'
		      ) AS TOTISSQN
		    FROM 
		      (
		      SELECT
		        CODFAT,
		        MESREF,
		        VALFAT,
		        0 AS VALFAT_CANC,
		        ISSQN,
		        RET_INSS,
		        0 AS RET_INSS_CANC,
		        DECODE((DATPGT-DATVNC),ABS((DATPGT-DATVNC)),ROUND(VALMUL*(DATPGT-DATVNC),2),0) AS JUROS,
		        VALPGT
		      FROM
		        CADFAT
		      WHERE
		        MESREF='{$mesAno}'
    
		      UNION
  
		      SELECT
		        CODFAT,
		        MESREF,
		        VALFAT,
		        VALFAT AS VALFAT_CANC,
		        ISSQN,
		        RET_INSS,
		        RET_INSS AS RET_INSS_CANC,
		        DECODE((DATPGT-DATVNC),ABS((DATPGT-DATVNC)),ROUND(VALMUL*(DATPGT-DATVNC),2),0) AS JUROS,
		        VALPGT
		      FROM
		        CADFAT_CANC
		      WHERE
		        MESREF='{$mesAno}'
		      )
		    GROUP BY
		      MESREF
		  ) T
		WHERE
		  F.CODCLI=C.CODCLI
		ORDER BY 
		  MESREF,CODCLI
  		");

       if($csv==false)
	     {
          $this->Gerapdf($titu,$mesAno,$rel,$Tabela);
		 }
        else
		 {
          $this->Geracsv($titu,$mesAno,$rel,$Tabela);
		 }


	}

    function Gerapdf($titu,$mesAno,$rel,$Tabela)
	{


                $this->AliasNbPages();
                $this->SetTitle('SUPERINTEND�NCIA DE LIMPEZA URBANA');
				$Linha = 50;
				$this->SetY($Linha);
				$this->addPage('L');
				$this->SetFillColor(230,230,235);
				
				$i = 0;
				while ($Tabela->next_record())
				{

					if ($this->totalGeral==0)
					{
						$this->totalGeral = (float)((str_replace(",", ".", $Tabela->f("TOTGER"))));
						$this->totalGeral = number_format($this->totalGeral, 2,',','.');
						
						$TOTISSQN    = str_replace(",", ".",$Tabela->f("TOTISSQN"));
						$TOTISSQN    = number_format($TOTISSQN, 2,',','.');
						
						$TOTRET_INSS = str_replace(",", ".",$Tabela->f("TOTRET_INSS"));
						$TOTRET_INSS = number_format($TOTRET_INSS, 2,',','.');
						
						$TOTJUROS    = str_replace(",", ".",$Tabela->f("TOTJUROS"));
						$TOTJUROS    = number_format($TOTJUROS, 2,',','.');
						
						$TOTVALPGT   = str_replace(",", ".",$Tabela->f("TOTVALPGT"));
						$TOTVALPGT   = number_format($TOTVALPGT, 2,',','.');

						$TOTGER_CANC = str_replace(",", ".",$Tabela->f("TOTGER_CANC"));
						$TOTGER_CANC = number_format($TOTGER_CANC, 2,',','.');

						$TOTRET_INSS_CANC = str_replace(",", ".",$Tabela->f("TOTRET_INSS_CANC"));
						$TOTRET_INSS_CANC = number_format($TOTRET_INSS_CANC, 2,',','.');
					}
						//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
					if ($this->GetY() >= ($this->fw-12))
					{
						$Linha = 50;
						$this->addPage('L'); 
					}
					$this->SetY($Linha);

					//destacar linhas impares
					if(($i++)%2)
						$this->Rect(0, $Linha-3, $this->w, 4, 'F');//Rect(x, y, width, height, style['F'|'D'|'FD'])


					$Inscricao=$Tabela->f("CODCLI");
					$this->Text($this->posInscricao,$Linha,$Inscricao);

					$Cliente = $Tabela->f("DESCLI");
					$this->Text($this->posCliente,$Linha,$Cliente);
		
//					$Inscricao = $Tabela->f("CGCCPF");
//					$this->Text($this->posCPF,$Linha,$Inscricao);
		
					$Emissao = $Tabela->f("DATEMI");
					$this->Text($this->posEmissao,$Linha,$Emissao);
		
					$Fatura = $Tabela->f("CODFAT");
					$this->Text($this->posFatura,$Linha,$Fatura);
				
					$ValFat = $Tabela->f("VALFAT");
					$ValFat = (float)((str_replace(",", ".", $ValFat)));
					$ValFat = number_format($ValFat, 2,',','.');
					$this->StringTam = $this->GetStringWidth('VALOR FATURA');
					$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($ValFat)),$Linha,$ValFat);
					
					$Venci = $Tabela->f("DATVNC");
					$this->Text($this->posVenc,$Linha,$Venci);
					
					$PAGTO = $Tabela->f("DATPGT");
					$this->Text($this->posPGT,$Linha,$PAGTO);

					$ISSQN = $Tabela->f("ISSQN");
					$ISSQN = (float)((str_replace(",", ".", $ISSQN)));
					$ISSQN = number_format($ISSQN, 2,',','.');
					if ($Tabela->f("VALPGT") == 0)
					{
						$ISSQN.=" N";
					}
					else
					{
						if (round($Tabela->f("VALPGT"),2) == round(($Tabela->f("VALFAT") - $Tabela->f("INSS") - ($Tabela->f("INSS")=='N')?($Tabela->f("INSSQN")):(0)),2))
						{
							$ISSQN.=" R";
						}
						else
						{
							if (round($Tabela->f("VALPGT"),2) == round(($Tabela->f("VALFAT") - $Tabela->f("INSS")),2))
							{
								$ISSQN.=" N";
							}
							else
							{
								if (round($Tabela->f("VALPGT"),2) == round(($Tabela->f("VALFAT") - ($Tabela->f("INSS")=='N')?($Tabela->f("INSSQN")):(0)),2))
								{
									$ISSQN.=" R";
								}
								else
								{
									if (round($Tabela->f("VALPGT"),2) == round($Tabela->f("VALFAT"),2))
									{
										$ISSQN.=" N";
									}
									else
									{
										if (round($Tabela->f("VALPGT"),2) > round($Tabela->f("VALFAT"),2))
										{
											$ISSQN.=" +";
										}
										else
										{
											$ISSQN.=" -";
										}
									}
								}
							}
						}
					}
					//$this->Text($this->posISSQN,$Linha,$ISSQN);
					$this->StringTam = $this->GetStringWidth('  ISSQN');
					$this->Text((($this->posISSQN + $this->StringTam) - $this->GetStringWidth($ISSQN)),$Linha,$ISSQN);
					
					
					$INSS = $Tabela->f("RET_INSS");
					$INSS = (float)((str_replace(",", ".", $INSS)));
					$INSS = number_format($INSS, 2,',','.');
					//$this->Text($this->posINSS,$Linha,$INSS);
					$this->StringTam = $this->GetStringWidth('   INSS');
					$this->Text((($this->posINSS + $this->StringTam) - $this->GetStringWidth($INSS)),$Linha,$INSS);

					if($Tabela->f("CANCELADA")) {
						//quando a fatura tiver sido encontrada na tabela CADFAT_CANC, repete o valor da fatura e de INSS nas colunas referentes a cancelamento
						$this->StringTam = $this->GetStringWidth('VALOR CANCELADO');
						$this->Text((($this->posValFatcanc + $this->StringTam) - $this->GetStringWidth($ValFat)),$Linha,$ValFat);

						$this->StringTam = $this->GetStringWidth('INSS CANCELADO');
						$this->Text((($this->posINSScanc + $this->StringTam) - $this->GetStringWidth($INSS)),$Linha,$INSS);
					}
					
					$JUROS = $Tabela->f("JUROS");
					$JUROS = (float)((str_replace(",", ".", $JUROS)));
					$JUROS = number_format($JUROS, 2,',','.');
					//$this->Text($this->posJuros,$Linha,$JUROS);
					$this->StringTam = $this->GetStringWidth('   JUROS');
					$this->Text((($this->posJuros + $this->StringTam) - $this->GetStringWidth($JUROS)),$Linha,$JUROS);
					
					$VALPGT = $Tabela->f("VALPGT");
					$VALPGT = (float)((str_replace(",", ".", $VALPGT)));
					$VALPGT = number_format($VALPGT, 2,',','.');
					//$this->Text($this->posValRec,$Linha,$VALPGT);
					$this->StringTam = $this->GetStringWidth('   RECEBIDO');
					$this->Text((($this->posValRec + $this->StringTam) - $this->GetStringWidth($VALPGT)),$Linha,$VALPGT);
					
					$Linha+=4;
				}
				$this->Text(3,$Linha,"T  o  t  a  l     G  e  r  a  l");
				//           (       f i n a l                   ) -       t  a  m  n  h  o    
				$this->StringTam = $this->GetStringWidth('VALOR FATURA');
				$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$Linha,$this->totalGeral);
				
				$this->StringTam = $this->GetStringWidth('  ISSQN');
				$this->Text((($this->posISSQN + $this->StringTam) - $this->GetStringWidth($TOTISSQN))-1,$Linha,$TOTISSQN);
						
				$this->StringTam = $this->GetStringWidth('   INSS');
				$this->Text((($this->posINSS + $this->StringTam) - $this->GetStringWidth($TOTRET_INSS)),$Linha,$TOTRET_INSS);

				$this->StringTam = $this->GetStringWidth('VALOR CANCELADO');
				$this->Text((($this->posValFatcanc + $this->StringTam) - $this->GetStringWidth($TOTGER_CANC)),$Linha,$TOTGER_CANC);
						
				$this->StringTam = $this->GetStringWidth('INSS CANCELADO');
				$this->Text((($this->posINSScanc + $this->StringTam) - $this->GetStringWidth($TOTRET_INSS_CANC)),$Linha,$TOTRET_INSS_CANC);

				$this->StringTam = $this->GetStringWidth('   JUROS');
				$this->Text((($this->posJuros + $this->StringTam) - $this->GetStringWidth($TOTJUROS)),$Linha,$TOTJUROS);
						
				$this->StringTam = $this->GetStringWidth('   RECEBIDO');
				$this->Text((($this->posValRec + $this->StringTam) - $this->GetStringWidth($TOTVALPGT)),$Linha,$TOTVALPGT);
				
				//$this->SetMargins(5,5,5);
				$this->SetFont('Arial','U',10);
				$this->SetTextColor(0, 0, 0);
				$this->SetAutoPageBreak(1);
				$this->Output();
	}



    function Geracsv($titu,$mesAno,$rel,$Tabela)
	{

				$TOTISSQN    = 0;
				$TOTRET_INSS = 0;
				$TOTJUROS    = 0;
				$TOTVALPGT   = 0;
				$TOTGER_CANC = 0;
				$TOTRET_INSS_CANC = 0;

				$tabsaida[] = array(
					'INSCRI��O',
					'NOME DO CLIENTE',
					'EMISS�O',
					'FATURA',
					'VALOR DA FATURA',
					'VENCIMENTO',
					'PAGAMENTO',
					'ISSQN',
					'INSS',
					'VALOR CANCELADO',
					'INSS CANCELADO',
					'JUROS',
					'RECEBIDO'
				);

				$this->totalGeral=0;

				while ($Tabela->next_record())
				{
					if ($this->totalGeral==0)
					{
						$this->totalGeral = (float)(str_replace(",", ".", $Tabela->f("TOTGER")));
						$this->totalGeral = number_format($this->totalGeral, 2,',','.');
						
						$TOTISSQN    = str_replace(",", ".",$Tabela->f("TOTISSQN"));
						$TOTISSQN    = number_format($TOTISSQN, 2,',','.');
						
						$TOTRET_INSS = str_replace(",", ".",$Tabela->f("TOTRET_INSS"));
						$TOTRET_INSS = number_format($TOTRET_INSS, 2,',','.');
						
						$TOTJUROS    = str_replace(",", ".",$Tabela->f("TOTJUROS"));
						$TOTJUROS    = number_format($TOTJUROS, 2,',','.');
						
						$TOTVALPGT   = str_replace(",", ".",$Tabela->f("TOTVALPGT"));
						$TOTVALPGT   = number_format($TOTVALPGT, 2,',','.');


						$TOTGER_CANC = str_replace(",", ".",$Tabela->f("TOTGER_CANC"));
						$TOTGER_CANC = number_format($TOTGER_CANC, 2,',','.');

						$TOTRET_INSS_CANC = str_replace(",", ".",$Tabela->f("TOTRET_INSS_CANC"));
						$TOTRET_INSS_CANC = number_format($TOTRET_INSS_CANC, 2,',','.');
					}

					$Inscricao=$Tabela->f("CODCLI");

					$Cliente = $Tabela->f("DESCLI");

					$Emissao = $Tabela->f("DATEMI");

					$Fatura = $Tabela->f("CODFAT");

					$ValFat = $Tabela->f("VALFAT");
					$ValFat = (float)((str_replace(",", ".", $ValFat)));
					$ValFat = number_format($ValFat, 2,',','.');
										
					$Venci = $Tabela->f("DATVNC");

					$PAGTO = $Tabela->f("DATPGT");

					$ISSQN = $Tabela->f("ISSQN");
					$ISSQN = (float)((str_replace(",", ".", $ISSQN)));
					$ISSQN = number_format($ISSQN, 2,',','.');
					if ($Tabela->f("VALPGT") == 0)
					{
						$ISSQN.=" N";
					}
					else
					{
						if (round($Tabela->f("VALPGT"),2) == round(($Tabela->f("VALFAT") - $Tabela->f("INSS") - ($Tabela->f("INSS")=='N')?($Tabela->f("INSSQN")):(0)),2))
						{
							$ISSQN.=" R";
						}
						else
						{
							if (round($Tabela->f("VALPGT"),2) == round(($Tabela->f("VALFAT") - $Tabela->f("INSS")),2))
							{
								$ISSQN.=" N";
							}
							else
							{
								if (round($Tabela->f("VALPGT"),2) == round(($Tabela->f("VALFAT") - ($Tabela->f("INSS")=='N')?($Tabela->f("INSSQN")):(0)),2))
								{
									$ISSQN.=" R";
								}
								else
								{
									if (round($Tabela->f("VALPGT"),2) == round($Tabela->f("VALFAT"),2))
									{
										$ISSQN.=" N";
									}
									else
									{
										if (round($Tabela->f("VALPGT"),2) > round($Tabela->f("VALFAT"),2))
										{
											$ISSQN.=" +";
										}
										else
										{
											$ISSQN.=" -";
										}
									}
								}
							}
						}
					}

					$INSS = $Tabela->f("RET_INSS");
					$INSS = (float)((str_replace(",", ".", $INSS)));
					$INSS = number_format($INSS, 2,',','.');

					$JUROS = $Tabela->f("JUROS");
					$JUROS = (float)((str_replace(",", ".", $JUROS)));
					$JUROS = number_format($JUROS, 2,',','.');

					$VALPGT = $Tabela->f("VALPGT");
					$VALPGT = (float)((str_replace(",", ".", $VALPGT)));
					$VALPGT = number_format($VALPGT, 2,',','.');

					if($Tabela->f("CANCELADA")==1) {
						$ValFat_canc = $ValFat;
						$INSS_canc = $INSS;
					} else {
						$ValFat_canc = 0;
						$INSS_canc = 0;
					}

					//linha com dados da fatura
					$tabsaida[] = array(
	                   $Inscricao,
					   $Cliente,
					   $Emissao,
					   $Fatura,
					   $ValFat,
					   $Venci,
					   $PAGTO,
					   $ISSQN,
					   $INSS,
					   $ValFat_canc,
					   $INSS_canc,
					   $JUROS,
					   $VALPGT
					);
				}

				//linha com os totais gerais
				$tabsaida[]= array(
					0 => 'T  o  t  a  l     G  e  r  a  l',
					4 => $this->totalGeral,
					7 => $TOTISSQN,
					8 => $TOTRET_INSS,
					9 => $TOTGER_CANC,
					10 => $TOTRET_INSS_CANC,
					11 => $TOTJUROS,
					12 => $TOTVALPGT
				);

				header('Content-type: text/csv');
				header('Content-Disposition: attachment; filename="Relat�riodeEmiss�o'.$mesAno.'.csv"');

				print $this->vetorcsv($tabsaida);
				
				exit();
	}

    function vetorcsv($vetor)
	{

	 $saida="";
	 
	 $ultima_linha=array_keys($vetor);
	 $ultima_linha=$ultima_linha[count($ultima_linha)-1]+1;

     for($lin=0;$lin<$ultima_linha;$lin++)
	   {

	     $ultima_coluna=array_keys($vetor[$lin]);
	     $ultima_coluna=$ultima_coluna[count($ultima_coluna)-1]+1;
	    
	     for($col=0;$col<$ultima_coluna;$col++)
		   {
	        $saida=$saida.str_replace(';',' ',$vetor[$lin][$col]).';';
			

		   }

         $saida=$saida."\n";
	   }
      return($saida);


	}

	function Header()
	{

		$this->SetFont('Arial','B',20);
		// dimens�es da folha A4 - Largura = 210.6 mm e Comprimento 296.93 mm em Portrait. Em Landscape � s� inverter.
		$this->SetXY(0,0);
		//'SECRETARIA MUNICIPAL DE EDUCA��O DE BELO HORIZONTE'
		// Meio = (286/2) - (50/2) = 148,3 - 25 = 123,3
		$tanString = $this->GetStringWidth($this->title);
		$tamPonto = $this->fwPt;
		$tan = $this->fh;
		//$this->Text(($tamPonto/2) - ($tanString/2),6,$this->title);
		//$Unidade = CCGetSession("mDERCRI_UNI");
		$this->Text(($tan/2) - ($tanString/2),8,$this->title);
		$pad = 5;

		//$this->Text(18,19,'DEPARTAMENTO DE ADMINISTRA��O FINANCEIRA');
		//$this->SetFont('Arial','B',15);
		//$tanString = $this->GetStringWidth($Unidade);
		//$this->Text(($tan/2) - ($tanString/2),16,$Unidade);
		$this->SetFont('Arial','B',10);
		$tanString = $this->GetStringWidth($this->titulo);
		$this->Text(($tan/2) - ($tanString/2),24,$this->titulo);
		$this->Image('pdf/PBH_-_nova_-_Vertical_-_COR.jpg',5,10,33);
		//$mesRef = CCGetParam("mesRef");
		$this->Text(5,40,'M�s / Ano de Refer�ncia : '.$this->mesRef);
		$dataSist = "Data da Emiss�o : ".CCGetSession("DataSist");
		$tanString = $this->GetStringWidth($dataSist);
		$this->Text($tan-($tanString+5),40,$dataSist);
		$this->Line(0,41,296.93,41);
		//               T   I   T   U   L   O       D   A   S       C   O  L  U   N   A   S 
		
		// Relat�rio de Arrecada��o
		{
			$this->SetFont('Arial','B',06.5);

			$this->SetXY(1,45);


			$this->posInscricao=1;
  		    $this->Text($this->posInscricao,$this->GetY(),'INSCRI��O');
			$tanString = $this->GetStringWidth('INSCRI��O');
	        
     	    
            $this->posCliente=$this->Getx()+$tanString+$pad;
			$this->Setx($this->posCliente);
			$this->Text($this->posCliente,$this->GetY(),'NOME DO CLIENTE');
			$tanString = $this->GetStringWidth('NOME DO CLIENTE');
			
/*			$this->posCPF = $this->GetX()+$tanString+55;
			$this->Setx($this->posCPF);
			$this->Text($this->posCPF,$this->GetY(),'CNPJ/CPF');
			$tanString = $this->GetStringWidth('CNPJ/CPF');
*/
			$this->posEmissao = $this->GetX()+$tanString+$pad+50;
			$this->Setx($this->posEmissao);
			$this->Text($this->posEmissao,$this->GetY(),'EMISS�O');
			$tanString = $this->GetStringWidth('EMISS�O');

			
			$this->posFatura = $this->GetX()+$tanString+$pad;
			$this->Setx($this->posFatura);
			$this->Text($this->posFatura,$this->GetY(),'FATURA');
			$tanString = $this->GetStringWidth('FATURA');
			

			$this->posValFat = $this->GetX()+$tanString+$pad;
			$this->Setx($this->posValFat);
			$this->Text($this->posValFat,$this->GetY(),'VALOR FATURA');
			$tanString = $this->GetStringWidth('VALOR FATURA');
			
			
			$this->posVenc = $this->GetX()+$tanString+$pad;
			$this->Setx($this->posVenc);
			$this->Text($this->posVenc,$this->GetY(),'VENCIMENTO');
			$tanString = $this->GetStringWidth('VENCIMENTO');
			
			
			$this->posPGT = $this->GetX()+$tanString+$pad;
			$this->Setx($this->posPGT);
			$this->Text($this->posPGT,$this->GetY(),'PAGAMENTO');
			$tanString = $this->GetStringWidth('PAGAMENTO');
			

			$this->posISSQN = $this->GetX()+$tanString+$pad;
			$this->Setx($this->posISSQN);
			$this->Text($this->posISSQN,$this->GetY(),'  ISSQN');
			$tanString = $this->GetStringWidth('  ISSQN');
			
			$this->posINSS = $this->GetX()+$tanString+$pad;
			$this->Setx($this->posINSS);
			$this->Text($this->posINSS,$this->GetY(),'   INSS');
			$tanString = $this->GetStringWidth('   INSS');

			$this->posValFatcanc = $this->GetX()+$tanString+$pad;
			$this->Setx($this->posValFatcanc);
			$this->Text($this->posValFatcanc,$this->GetY(),'VALOR CANCELADO');
			$tanString = $this->GetStringWidth('VALOR CANCELADO');

			$this->posINSScanc = $this->GetX()+$tanString+$pad-2;
			$this->Setx($this->posINSScanc);
			$this->Text($this->posINSScanc,$this->GetY(),'INSS CANCELADO');
			$tanString = $this->GetStringWidth('INSS CANCELADO');
			
			$this->posJuros = $this->GetX()+$tanString+$pad;
			$this->Setx($this->posJuros);
			$this->Text($this->posJuros,$this->GetY(),'   JUROS');
			$tanString = $this->GetStringWidth('   JUROS');
			
			$this->posValRec = $this->GetX()+$tanString+$pad;
			$this->Setx($this->posValRec);
			$this->Text($this->posValRec,$this->GetY(),'   RECEBIDO');
			$tanString = $this->GetStringWidth('   RECEBIDO');
			
			$this->StringTam = $tanString;
			$this->Line(0,$this->GetY()+1,296.93,$this->GetY()+1);
		}
	}
	function footer()
	{
    //Position at 1.5 cm from bottom
    //$this->SetY(-7);
    //Arial italic 8
	//$this->SetFont('Arial','B',06);
	//$this->Text(3,$this->GetY(),"T  o  t  a  l     G  e  r  a  l");
	//           (       f i n a l                   ) -       t  a  m  n  h  o    
	//$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$this->GetY(),$this->totalGeral);
    //Position at 1.5 cm from bottom
    $this->SetY(-8);
    //Arial italic 8
    $this->SetFont('Arial','I',8);
	$this->Cell(0,10,'P�gina '.$this->PageNo().' / {nb}',0,0,'C');
    //Page number
	}
}
//unset($Tabela);
//unset($Grupo);
	/*$Imprime = new relatoriosPDF();
	$title = 'SUPERINTEND�NCIA DE LIMPESA URBANA';
	$Imprime->addPage('Portrait'); 
	$Imprime->SetFont('Arial','',10);
	$Imprime->SetTextColor(0, 0, 0);
	$Imprime->SetAutoPageBreak(0);
	$Imprime->Output();*/
?>