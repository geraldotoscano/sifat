<?php
error_reporting(0);
//BindEvents Method @1-EC792D1D
function BindEvents()
{
    global $NRRelEmisInc;
    global $CCSEvents;
    $NRRelEmisInc->Button_Insert->CCSEvents["OnClick"] = "NRRelEmisInc_Button_Insert_OnClick";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//NRRelEmisInc_Button_Insert_OnClick @7-7DDD32A7
function NRRelEmisInc_Button_Insert_OnClick(& $sender)
{
    $NRRelEmisInc_Button_Insert_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRRelEmisInc; //Compatibility
//End NRRelEmisInc_Button_Insert_OnClick

//Custom Code @11-2A29BDB7
// -------------------------
	
    $Imprime = new relArrEmisPDF();

	$Imprime->relatorio('Relat�rio de Emiss�o/Arrecada��o por Inscri��o',$NRRelEmisInc->TextBox1->GetValue(),CCGetParam('opcao'), ($NRRelEmisInc->RBFormato->GetValue()=='CSV'));
// -------------------------
//End Custom Code

//Close NRRelEmisInc_Button_Insert_OnClick @7-944EB3E4
    return $NRRelEmisInc_Button_Insert_OnClick;
}
//End Close NRRelEmisInc_Button_Insert_OnClick

//Page_BeforeShow @1-70F93C99
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $RelArrecInsc; //Compatibility
//End Page_BeforeShow

//Custom Code @21-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(39);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow

?>
