<?php
//BindEvents Method @1-B2B58918
function BindEvents()
{
    global $CADFAT;
    $CADFAT->Button_DoSearch->CCSEvents["OnClick"] = "CADFAT_Button_DoSearch_OnClick";
}
//End BindEvents Method

//CADFAT_Button_DoSearch_OnClick @5-A4F534BC
function CADFAT_Button_DoSearch_OnClick(& $sender)
{
    $CADFAT_Button_DoSearch_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CADFAT; //Compatibility
//End CADFAT_Button_DoSearch_OnClick

//Custom Code @9-2A29BDB7
// -------------------------
$relatorio=new relatorioPDFBase();

//Definir o arquivo html que servir� de modelo
//O modelo deve conter parametros cercados por % ex: %parametro% %idade%
//Os parametros do modelo deve ter o mesmo nome dos campos retornados na consulta
//ou o mesmo nome dos parametros passados no modelo por exemplo
//neste relat�rio os parametros validos sao:
//Do SQL: %MESREF% %VALFAT% %RET_INSS% %JUROS% e %VALPGT%
//Do parametro passado: %MES_REF_INI% e %MES_REF_FIM%

$relatorio->set_modelo("modelos/ArrecMesAno.html");


//Fazendo a consulta com parametros os parametros devem estar cercados por chaves ex: {PARAMETRO}
$sql="select
  MESREF,
  sum(VALFAT) AS VALFAT,
  sum(RET_INSS) AS RET_INSS,
  sum(VALFAT_CANC) AS VALFAT_CANC,
  sum(RET_INSS_CANC) AS RET_INSS_CANC,
  sum(JUROS) AS JUROS,
  sum(VALPGT) AS VALPGT
FROM
(
  SELECT
    CODFAT,
    MESREF,
    VALFAT,
    RET_INSS, 
    DECODE((DATPGT-DATVNC),ABS((DATPGT-DATVNC)),ROUND(VALMUL*(DATPGT-DATVNC),2),0) AS JUROS,
    VALPGT, 
    0 AS VALFAT_CANC,
    0 AS RET_INSS_CANC
  FROM CADFAT
  WHERE substr(mesref,4,4)*100+substr(mesref,0,2)>='{MES_REF_INI}' and substr(mesref,4,4)*100+substr(mesref,0,2)<='{MES_REF_FIM}'

  UNION
  
  SELECT
    CODFAT,
    MESREF,
    VALFAT,
    RET_INSS, 
    DECODE((DATPGT-DATVNC),ABS((DATPGT-DATVNC)),ROUND(VALMUL*(DATPGT-DATVNC),2),0) AS JUROS,
    VALPGT,
    VALFAT AS VALFAT_CANC,
    RET_INSS AS RET_INSS_CANC
  FROM CADFAT_CANC
  WHERE substr(mesref,4,4)*100+substr(mesref,0,2)>='{MES_REF_INI}' and substr(mesref,4,4)*100+substr(mesref,0,2)<='{MES_REF_FIM}'
) F
GROUP BY mesref
ORDER BY substr(mesref,4,4)*100+substr(mesref,0,2)
";

$relatorio->set_sql_relatorio($sql);

//Fazendo a consulta final com os totais do relatorio
$sql_fim="SELECT
  sum(VALFAT) AS TOTAL_VALFAT,
  sum(RET_INSS) AS TOTAL_RET_INSS,
  sum(VALFAT_CANC) AS TOTAL_VALFAT_CANC,
  sum(RET_INSS_CANC) AS TOTAL_RET_INSS_CANC,
  sum(JUROS) AS TOTAL_JUROS,
  sum(VALPGT) AS TOTAL_VALPGT
FROM
(
  SELECT
    CODFAT,
    MESREF,
    VALFAT,
    RET_INSS, 
    DECODE((DATPGT-DATVNC),ABS((DATPGT-DATVNC)),ROUND(VALMUL*(DATPGT-DATVNC),2),0) AS JUROS,
    VALPGT, 
    0 AS VALFAT_CANC,
    0 AS RET_INSS_CANC
  FROM CADFAT
  WHERE substr(mesref,4,4)*100+substr(mesref,0,2)>='{MES_REF_INI}' and substr(mesref,4,4)*100+substr(mesref,0,2)<='{MES_REF_FIM}'

  UNION
  
  SELECT
    CODFAT,
    MESREF,
    VALFAT,
    RET_INSS, 
    DECODE((DATPGT-DATVNC),ABS((DATPGT-DATVNC)),ROUND(VALMUL*(DATPGT-DATVNC),2),0) AS JUROS,
    VALPGT,
    VALFAT AS VALFAT_CANC,
    RET_INSS AS RET_INSS_CANC
  FROM CADFAT_CANC
  WHERE substr(mesref,4,4)*100+substr(mesref,0,2)>='{MES_REF_INI}' and substr(mesref,4,4)*100+substr(mesref,0,2)<='{MES_REF_FIM}'
) F";

$relatorio->set_sql_fim_relatorio($sql_fim);

//Definindo os parametros a ser passados a classe
//Utilizando o um vetor contendo como indicie o nome do parameto

$parametros=array(
                  'MES_REF_INI'=>substr($CADFAT->MES_REF_INI->GetValue(),3,4).substr($CADFAT->MES_REF_INI->GetValue(),0,2),
                  'MES_REF_FIM'=>substr($CADFAT->MES_REF_FIM->GetValue(),3,4).substr($CADFAT->MES_REF_FIM->GetValue(),0,2),
				  'TITULO'=>'Total de arrecada��o mensal'
				  );
$relatorio->parametro_em_real(array('JUROS','VALPGT','RET_INSS','VALFAT','RET_INSS_CANC','VALFAT_CANC','TOTAL_VALPGT','TOTAL_JUROS','TOTAL_RET_INSS','TOTAL_VALFAT','TOTAL_VALFAT_CANC','TOTAL_RET_INSS_CANC'));
header('Content-type: application/pdf');
$relatorio->set_linhas(23);
print $relatorio->relatorio($parametros,true);

exit();
// -------------------------
//End Custom Code

//Close CADFAT_Button_DoSearch_OnClick @5-6A96FF51
    return $CADFAT_Button_DoSearch_OnClick;
}
//End Close CADFAT_Button_DoSearch_OnClick


?>
