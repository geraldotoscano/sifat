<?php
	error_reporting (E_ALL);
	//define("RelativePath", "../..");
	//define('FPDF_FONTPATH','/pdf/font/');
	require('pdf/fpdf.php');
	//include('pdf/fpdi.php');
	//require_once("pdf/fpdi_pdf_parser.php");
	//require_once("pdf/fpdf_tpl.php");
	//include(RelativePath . "/Common.php");
	//include(RelativePath . "/Template.php");
	//include(RelativePath . "/Sorter.php");
	//include(RelativePath . "/Navigator.php");
class relCliSerPDF extends fpdf {	
	var $titulo;
	var $mesRef;
	var $posCliente;
	var $posFatura;
	var $posEmissao;
	var $posValFat;
	var $posVenc;
	var $totalGeral;
	var $StringTam;
	var $relat;
	var $mudouDistrito;
	var $distrito;
	var $posINSS;
	var $posPGT;
	var $posAcres;
	var $posRec;
	var $posISSQN;
	var $posJuros;
	var $posValRec;
	var $posSit;
	var $posTipCli;
	//$pdf= new fpdi();
	function relatorio($titu,$rel,$CodDis='')
	{
		$this->SetTitle('SUPERINTEND�NCIA DE LIMPEZA URBANA');
		$this->titulo = $titu;
		$this->AliasNbPages();
		$Tabela = new clsDBfaturar();
		$Grupo  = new clsDBfaturar();
		//              L   I   N   H   A       D   E       D   E   T   A   L   H   E
		//                       Relat�rio de Arrecada��o por Divis�o
		if ($CodDis!="Todos")
		{
			$Tabela->query("SELECT 
								DISTINCT M.GRPSER AS GRPSER,
								C.CODCLI,
								C.DESCLI,
								C.CGCCPF,
								C.CONTAT,
								C.LOGRAD,
								C.BAIRRO,
								C.CIDADE,
								C.ESTADO,
								C.TELEFO,
								C.CODPOS,
								C.TELFAX,
								DECODE(C.CODTIP,'J','P. JUR�DICA','P. F�SICA') AS CODTIP,
								DECODE(C.CODSIT,'A','ATIVO','INATIVO') AS CODSIT,
								C.ESFERA,
								T.DESSER
							FROM 
								CADCLI C,
								GRPSER T,
								MOVSER M
							WHERE 
								T.GRPSER = '$CodDis'  AND
								T.GRPSER = M.GRPSER   AND
								M.CODCLI = C.CODCLI
							ORDER BY 
								M.GRPSER,
								C.DESCLI"
							);
		}
		else
		{
			$Tabela->query("SELECT 
								DISTINCT M.GRPSER AS GRPSER,
								C.CODCLI,
								C.DESCLI,
								C.CGCCPF,
								C.CONTAT,
								C.LOGRAD,
								C.BAIRRO,
								C.CIDADE,
								C.ESTADO,
								C.TELEFO,
								C.CODPOS,
								C.TELFAX,
								DECODE(C.CODTIP,'J','P. JUR�DICA','P. F�SICA') AS CODTIP,
								DECODE(C.CODSIT,'A','ATIVO','INATIVO') AS CODSIT,
								C.ESFERA,
								T.DESSER
							FROM 
								CADCLI C,
								GRPSER T,
								MOVSER M
							WHERE 
								T.GRPSER = M.GRPSER   AND
								M.CODCLI = C.CODCLI
							ORDER BY 
								M.GRPSER,
								C.DESCLI"
							);
		}
		if ($CodDis=="Todos")
		{
			$Grupo->query(	"	SELECT
									DISTINCT M.GRPSER,
									COUNT(M.GRPSER) AS TotalDistr
								FROM
									CADCLI C,
									GRPSER T,
									MOVSER M
								WHERE 
									T.GRPSER = M.GRPSER AND
									M.CODCLI = C.CODCLI
								GROUP BY 
									M.GRPSER
								ORDER BY
									M.GRPSER
							");
		}
		else
		{
			$Grupo->query(	"	SELECT
									DISTINCT M.GRPSER,
									COUNT(M.GRPSER) AS TotalDistr
								FROM
									CADCLI C,
									GRPSER T,
									MOVSER M
								WHERE 
									T.GRPSER = '$CodDis' AND
									T.GRPSER = M.GRPSER AND
									M.CODCLI = C.CODCLI
								GROUP BY 
									M.GRPSER
								ORDER BY
									M.GRPSER
							");
		}
		$Linha = 54;
		$this->SetY($Linha);
		$Grupo->next_record();
		$Tabela->next_record();
		$this->distrito = $Tabela->f("DESSER");
		$this->mudouDistrito = true;
		$this->addPage('L');
		do 
		{
			$this->mudouDistrito = false;

			//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
			if ($this->GetY() >= ($this->fw-12))
			{
				$Linha = 50;
				$this->addPage('L'); 
			}
			if ($Tabela->f("GRPSER") != $Grupo->f("GRPSER"))
			{
				$totalDistrito = (float)((str_replace(",", ".", $Grupo->f("TotalDistr"))));
				$totalDistrito = number_format($totalDistrito, 2,',','.');
				$this->Text(3,$Linha,"Total de clientes no Servi�o --> ".$Grupo->f("TotalDistr"));
				$Grupo->next_record();
				$Linha = 54;
				$this->mudouDistrito = true;
				$this->distrito = $Tabela->f("DESSER");
				$this->addPage('L'); 
			}
			$this->SetY($Linha);

			$Emissao = $Tabela->f("CODCLI");
			$this->Text(1,$Linha,$Emissao);

			$Cliente = $Tabela->f("DESCLI");
			$this->Text($this->posCPF,$Linha,$Cliente);
		
			$Inscricao = $Tabela->f("CGCCPF");
			if (strlen($Inscricao) == 14)
			{
				// cgc = 11.111.111/1111-11  Ex: 81.243.735/0002-29
				$Inscricao = 	substr($Inscricao,0,2).".".
								substr($Inscricao,2,3).".".
								substr($Inscricao,5,3)."/".
								substr($Inscricao,8,4)."-".
								substr($Inscricao,-2);
			}
			else
			{
				// cpf = 11.111.111/1111-11  Ex: 683.179.306-10
				$Inscricao = 	substr($Inscricao,0,3).".".
								substr($Inscricao,3,3).".".
								substr($Inscricao,6,3)."-".
								substr($Inscricao,-2);
			}
			$this->Text($this->posValRec,$Linha,$Inscricao);
		
			$Emissao = $Tabela->f("CONTAT");
			$this->Text($this->posEmissao,$Linha,$Emissao);
		
			$Fatura = $Tabela->f("LOGRAD");
			$this->Text($this->posFatura,$Linha,$Fatura);
				
			$ValFat = $Tabela->f("BAIRRO");
			$this->Text($this->posValFat,$Linha,$ValFat);
					
			$Venci = $Tabela->f("CIDADE");
			$this->Text($this->posVenc,$Linha,$Venci);
					
			$PAGTO = $Tabela->f("ESTADO");
			$this->Text($this->posPGT,$Linha,$PAGTO);

			$ISSQN = $Tabela->f("TELEFO");
			$this->Text($this->posISSQN,$Linha,$ISSQN);
					
			$INSS = $Tabela->f("CODPOS");
			$this->Text($this->posINSS,$Linha,$INSS);
			
			$JUROS = $Tabela->f("TELFAX");
			$this->Text($this->posJuros,$Linha,$JUROS);
					
			$VALPGT = $Tabela->f("CODTIP");
			$this->Text($this->posTipCli,$Linha,$VALPGT);
					
			$VALPGT = $Tabela->f("CODSIT");
			$this->Text($this->posSit,$Linha,$VALPGT);

			$Linha+=2;
		}while ($Tabela->next_record());
		//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
		if ($this->GetY()+24 >= ($this->fw-12))
		{
			$Linha = 50;
			$this->addPage('L'); 
		}
		if (!$this->mudouDistrito)
		{
			$totalDistrito = (float)((str_replace(",", ".", $Grupo->f("TotalDistr"))));
			$totalDistrito = number_format($totalDistrito, 2,',','.');
			$this->Text(3,$Linha,"Total de clientes no Servi�o --> ".$Grupo->f("TotalDistr"));
			$Linha+=2;
		}
		
				
		//$this->SetMargins(5,5,5);
		$this->SetFont('Arial','U',10);
		$this->SetTextColor(0, 0, 0);
		$this->SetAutoPageBreak(1);
		$this->Output();
	}
	function Header()
	{

		$this->SetFont('Arial','B',20);
		// dimens�es da folha A4 - Largura = 210.6 mm e Comprimento 296.93 mm em Portrait. Em Landscape � s� inverter.
		$this->SetXY(0,0);
		//'SECRETARIA MUNICIPAL DE EDUCA��O DE BELO HORIZONTE'
		// Meio = (286/2) - (50/2) = 148,3 - 25 = 123,3
		$tanString = $this->GetStringWidth($this->title);
		$tamPonto = $this->fwPt;
		$tan = $this->fh;
		//$this->Text(($tamPonto/2) - ($tanString/2),6,$this->title);
		//$Unidade = CCGetSession("mDERCRI_UNI");
		$this->Text(($tan/2) - ($tanString/2),8,$this->title);

		//$this->Text(18,19,'DEPARTAMENTO DE ADMINISTRA��O FINANCEIRA');
		$this->SetFont('Arial','B',15);
		//$tanString = $this->GetStringWidth($Unidade);
		//$this->Text(($tan/2) - ($tanString/2),16,$Unidade);
		$this->SetFont('Arial','B',10);
		$tanString = $this->GetStringWidth($this->titulo);
		$this->Text(($tan/2) - ($tanString/2),24,$this->titulo);
		$this->Image('pdf/PBH_-_nova_-_Vertical_-_COR.jpg',5,10,33);
		$dataSist = "Data da Emiss�o : ".CCGetSession("DataSist");
		$tanString = $this->GetStringWidth($dataSist);
		$this->Text($tan-($tanString+5),40,$dataSist);
		$this->Line(0,41,296.93,41);
		//            I  M  P  R  I  M  I     A     Q  U  E  B  R  A     S  E     E  X  I  S  T  I  R
		if ($this->mudouDistrito)
		{
			$this->SetFont('Arial','B',10);
			$this->Text(05,45,"Servi�o : $this->distrito");
			$this->Line(0,47,296.93,47);
			$this->SetXY(1,50);
		}
		else
		{
			$this->SetXY(1,45);
		}
		//               T   I   T   U   L   O       D   A   S       C   O  L  U   N   A   S 
		
		// Relat�rio de Arrecada��o
		{
			$this->SetFont('Arial','B',04.5);


			$this->Text(1,$this->GetY(),' C�DIGO ');
			$tanString = $this->GetStringWidth('C�DIGO');
			
			$this->Text($this->GetX()+$tanString+03,$this->GetY(),'DENOMINA��O');
			$this->posCPF = ($this->GetX()+$tanString+03);
			$this->SetX($this->GetX()+$tanString+03);
			$tanString = $this->GetStringWidth('DENOMINA��O');
			
			$this->Text($this->GetX()+$tanString+50,$this->GetY(),'CGC/CPF');
			$this->posValRec = ($this->GetX()+$tanString+50);
			$this->SetX($this->GetX()+$tanString+50);
			$tanString = $this->GetStringWidth('CGC/CPF');
			
			$this->Text($this->GetX()+$tanString+17,$this->GetY(),'CONTATO');
			$this->posEmissao = ($this->GetX()+$tanString+17);
			$this->SetX($this->GetX()+$tanString+17); 
			$tanString = $this->GetStringWidth('CONTATO');

			$this->Text($this->GetX()+$tanString+31,$this->GetY(),'LOGRADOURO');
			$this->posFatura = ($this->GetX()+$tanString+31);
			$this->SetX($this->GetX()+$tanString+31);
			$tanString = $this->GetStringWidth('LOGRADOURO');
			
			$this->Text($this->GetX()+$tanString+28,$this->GetY(),'BAIRRO');
			$this->posValFat = ($this->GetX()+$tanString+28);
			$this->SetX($this->GetX()+$tanString+28);
			$tanString = $this->GetStringWidth('BAIRRO');
			
			$this->Text($this->GetX()+$tanString+17,$this->GetY(),'CIDADE');
			$this->posVenc = ($this->GetX()+$tanString+17);
			$this->SetX($this->GetX()+$tanString+17);
			$tanString = $this->GetStringWidth('CIDADE');
			
			$this->Text($this->GetX()+$tanString+17,$this->GetY(),'UF');
			$this->posPGT = ($this->GetX()+$tanString+17);
			$this->SetX($this->GetX()+$tanString+17);
			$tanString = $this->GetStringWidth('UF');
			
			$this->Text($this->GetX()+$tanString+03,$this->GetY(),'TELEFONE');
			$this->posISSQN = ($this->GetX()+$tanString+03);
			$this->SetX($this->GetX()+$tanString+03);
			$tanString = $this->GetStringWidth('TELEFONE');
			
			$this->Text($this->GetX()+$tanString+11,$this->GetY(),'CEP');
			$this->posINSS = ($this->GetX()+$tanString+11);
			$this->SetX($this->GetX()+$tanString+11);
			$tanString = $this->GetStringWidth('CEP');
			
			$this->Text($this->GetX()+$tanString+12,$this->GetY(),'FAX');
			$this->posJuros = ($this->GetX()+$tanString+12);
			$this->SetX($this->GetX()+$tanString+12);
			$tanString = $this->GetStringWidth('FAX');
			
			$this->Text($this->GetX()+$tanString+12,$this->GetY(),'TIPO');
			$this->posTipCli = ($this->GetX()+$tanString+12);
			$this->SetX($this->GetX()+$tanString+12);
			$tanString = $this->GetStringWidth('TIPO');
			
			$this->Text($this->GetX()+$tanString+8,$this->GetY(),'SITUA��O');
			$this->posSit = ($this->GetX()+$tanString+8);
			$this->SetX($this->GetX()+$tanString+8);
			$tanString = $this->GetStringWidth('SITUA��O');
			
			$this->StringTam = $tanString;
			$this->Line(0,$this->GetY()+1,296.93,$this->GetY()+1);
		}
	}
	function footer()
	{
    //Position at 1.5 cm from bottom
    //$this->SetY(-7);
    //Arial italic 8
	//$this->SetFont('Arial','B',06);
	//$this->Text(3,$this->GetY(),"T  o  t  a  l     G  e  r  a  l");
	//           (       f i n a l                   ) -       t  a  m  n  h  o    
	//$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$this->GetY(),$this->totalGeral);
    //Position at 1.5 cm from bottom
    $this->SetY(-8);
    //Arial italic 8
    $this->SetFont('Arial','I',8);
	$this->Cell(0,10,'P�gina '.$this->PageNo().' / {nb}',0,0,'C');
    //Page number
	}
}
unset($Tabela);
unset($Grupo);
	/*$Imprime = new relatoriosPDF();
	$title = 'SUPERINTEND�NCIA DE LIMPESA URBANA';
	$Imprime->addPage('Portrait'); 
	$Imprime->SetFont('Arial','',10);
	$Imprime->SetTextColor(0, 0, 0);
	$Imprime->SetAutoPageBreak(0);
	$Imprime->Output();*/
?>