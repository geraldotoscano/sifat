<?php
//BindEvents Method @1-E73D7E4D
function BindEvents()
{
    global $NRRelDebGera;
    global $CCSEvents;
    $NRRelDebGera->Button_DoSearch->CCSEvents["OnClick"] = "NRRelDebGera_Button_DoSearch_OnClick";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//NRRelDebGera_Button_DoSearch_OnClick @7-7C2CDA47
function NRRelDebGera_Button_DoSearch_OnClick(& $sender)
{
    $NRRelDebGera_Button_DoSearch_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRRelDebGera; //Compatibility
//End NRRelDebGera_Button_DoSearch_OnClick

//Custom Code @9-2A29BDB7
// -------------------------
	$bd = new clsDBfaturar();
	//obtem a situa��o dos usu�rios que deve constar no relat�rio
	$opcao = $NRRelDebGera->RBAtivo->GetValue();
	$filtros = array();
	if($opcao=='I') {
		$opcao = "WHERE CODSIT = 'I'";
		$filtros[] = "Somente clientes INATIVOS";
	} elseif($opcao=='A') {
		$opcao = "WHERE CODSIT = 'A'";
		$filtros[] = "Somente clientes ATIVOS";
	} else
		$opcao = '';

	$tipo = $NRRelDebGera->RBFormatoRelatorio->GetValue();

	//instru��o para consultar as faturas da atividade para cliente no per�odo
    $sql = "
		SELECT 
			C.CODCLI,
			TRIM(C.DESCLI) AS DESCLI,
	        CASE 
	            WHEN (LENGTH(C.CGCCPF)=14) THEN SUBSTR(C.CGCCPF,1,2)||'.'||SUBSTR(C.CGCCPF,3,3)||'.'||SUBSTR(C.CGCCPF,6,3)||'/'||SUBSTR(C.CGCCPF,9,4)||'-'||SUBSTR(C.CGCCPF,-2)
	            WHEN (LENGTH(C.CGCCPF)=11) THEN SUBSTR(C.CGCCPF,1,3)||'.'||SUBSTR(C.CGCCPF,4,3)||'.'||SUBSTR(C.CGCCPF,7,3)||'-'||SUBSTR(C.CGCCPF,-2)
	            ELSE C.CGCCPF END AS CGCCPF,
			C.CONTAT,
			C.LOGRAD,
			C.BAIRRO,
			C.CIDADE,
			C.ESTADO,
			C.TELEFO,
			C.CODPOS,
			C.TELFAX,
			DECODE(C.CODTIP,'J','PJ','PF') AS CODTIP,
			DECODE(C.CODSIT,'A','ATIVO','I','INATIVO','INDEFINIDO') AS SITUACAO,
			C.CODSIT
		FROM 
			CADCLI C
		$opcao
		ORDER BY
			C.CODSIT,
			TRIM(C.DESCLI)
    ";

    //propriedade do relat�rio
	$cfg = array(
        'SOURCE' => $sql,
        'TITULO' => 'Relat�rio de Clientes por Situa��o',
        'COLUNAS' => array(
            'CODCLI' => array('C�DIGO'),
			'DESCLI' => array('DENOMINA��O', 'L', 62, 2),
			'CGCCPF' => array('CNPJ/CPF', 'L', 17),
			'CONTAT' => array('CONTATO', 'L', 33, 2),
			'LOGRAD' => array('LOGRADOURO', 'L', 45, 2),
			'BAIRRO' => array('BAIRRO', 'L', 26, 2),
			'CIDADE' => array('CIDADE', 'L', 18, 2),
			'ESTADO' => array('UF'),
			'TELEFO' => array('TELEFONE', 'L', 13),
			'CODPOS' => array('CEP', 'L', 10),
			'TELFAX' => array('FAX', 'L', 13),
			'CODTIP' => array('TIPO'),
			'SITUACAO' => array('SITUA��O', 'L', 11),
        ),
        'AGRUPAMENTO' => array(
            'ID' => 'CODSIT',
            'VALUE' => 'SITUACAO',
            'NAME' => 'Situa��o',
			'DESCRICAO-TOTAL' => 'Total de clientes na {NAME} "{VALUE}" => {QUANTIDADE}'
        ),
		'TOTAL' => array(
			'DESCRICAO' => 'CODCLI',
			'COLUNAS' => array()//n�o soma nenhuma coluna, mas usa a linha de totais para exibir a quantidade de registro
		),
		'FILTROS' => $filtros,
		'FONT-SIZE' => 5.5,
		'LEFT-MARGIN' => 2,
		'LINE-HEIGHT' => 3
    );

	$relatorio = new RelatorioFPDF($cfg);
    $relatorio->emitir($tipo);

// -------------------------
//End Custom Code

//Close NRRelDebGera_Button_DoSearch_OnClick @7-BA67D9B3
    return $NRRelDebGera_Button_DoSearch_OnClick;
}
//End Close NRRelDebGera_Button_DoSearch_OnClick

//Page_BeforeShow @1-4707BA8A
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $RelClienteSitu; //Compatibility
//End Page_BeforeShow

//Custom Code @13-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(42);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
