<?php
	error_reporting (E_ALL);
	//define("RelativePath", "../..");
	//define('FPDF_FONTPATH','/pdf/font/');
	require('pdf/fpdf.php');
	//include('pdf/fpdi.php');
	//require_once("pdf/fpdi_pdf_parser.php");
	//require_once("pdf/fpdf_tpl.php");
	//include(RelativePath . "/Common.php");
	//include(RelativePath . "/Template.php");
	//include(RelativePath . "/Sorter.php");
	//include(RelativePath . "/Navigator.php");
class relDebDisPDF extends fpdf {	
	var $titulo;
	var $mesRef;
	var $posCliente;
	var $posFatura;
	var $posValFat;
	var $posVenc;
	var $totalGeral;
	var $StringTam;
	var $relat;
	var $mudouDistrito;
	var $distrito;
	var $posMesAno;
	var $posINSS;
	var $posAcres;
	var $posRec;
	var $posJuros;
	var $posValRec;
	//$pdf= new fpdi();
	function relatorio($titu,$mesAno,$rel,$CodDis='')
	{
		$this->mesRef = $mesAno;
		$this->SetTitle('SUPERINTEND�NCIA DE LIMPEZA URBANA');
		$this->titulo = $titu;
		$this->AliasNbPages();
		$Tabela = new clsDBfaturar();
		$Grupo  = new clsDBfaturar();
		$GrupoISSQN = new clsDBfaturar();
		//              L   I   N   H   A       D   E       D   E   T   A   L   H   E
		//                       Relat�rio de Arrecada��o por Distrito
		if ($CodDis!="Todos")
		{
			$Tabela->query("SELECT 
								F.CODCLI,
								to_char(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
								(SYSDATE - F.DATVNC) AS NDIAS,
								ROUND(F.VALMUL*(SYSDATE-F.DATVNC),2) AS JUROS,
								F.CODFAT,
								F.VALFAT,
								F.RET_INSS,
								C.CODDIS,
								C.DESCLI,
								C.CGCCPF,
								C.ESFERA,
								T.DESDIS,
								(
									SELECT
										SUM(VALFAT)
									FROM
										CADFAT F,
										CADCLI C,
										TABDIS T
									WHERE 
										SYSDATE > F.DATVNC AND
										T.CODDIS=$CodDis   AND
										F.CODCLI=C.CODCLI  AND 
										C.CODDIS=T.CODDIS  AND
										F.MESREF='$this->mesRef' AND
										(
											F.VALPGT IS NULL OR
											F.VALPGT = 0
										)
								) AS TOTGER,
								(
									SELECT
										SUM(RET_INSS)
									FROM
										CADFAT F,
										CADCLI C,
										TABDIS T
									WHERE 
										SYSDATE > F.DATVNC AND
										T.CODDIS=$CodDis   AND
										F.CODCLI=C.CODCLI  AND 
										C.CODDIS=T.CODDIS  AND
										F.MESREF='$this->mesRef' AND
										(
											F.VALPGT IS NULL OR
											F.VALPGT = 0
										)
								) AS TOTRET_INSS,
								(
									SELECT
										SUM(round( F.VALMUL*(SYSDATE-F.DATVNC),2))
									FROM
										CADFAT F,
										CADCLI C,
										TABDIS T
									WHERE 
										SYSDATE > F.DATVNC       AND
										T.CODDIS=$CodDis         AND
										F.CODCLI=C.CODCLI        AND 
										C.CODDIS=T.CODDIS        AND
										F.MESREF='$this->mesRef' AND
										(
											F.VALPGT IS NULL OR
											F.VALPGT = 0
										)
								) AS TOTJUROS
							FROM 
								CADFAT F,
								CADCLI C,
								TABDIS T
							WHERE 
								SYSDATE > F.DATVNC AND
								T.CODDIS=$CodDis   AND
								F.CODCLI=C.CODCLI  AND 
								C.CODDIS=T.CODDIS  AND
								F.MESREF='$this->mesRef'AND
								(
									F.VALPGT IS NULL OR
									F.VALPGT = 0
								)
							ORDER BY 
								C.CODDIS,
								DESCLI"
							);
		}
		else
		{
			$Tabela->query("SELECT 
								F.CODCLI,
								to_char(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
								(SYSDATE - F.DATVNC) AS NDIAS,
								ROUND(F.VALMUL*(SYSDATE-F.DATVNC),2) AS JUROS,
								F.CODFAT,
								F.VALFAT,
								F.RET_INSS,
								C.CODDIS,
								C.DESCLI,
								C.CGCCPF,
								C.ESFERA,
								T.DESDIS,
								(
									SELECT
										SUM(F.VALFAT)
									FROM
										CADFAT F,
										CADCLI C,
										TABDIS T
									WHERE 
										SYSDATE > F.DATVNC AND
										F.CODCLI=C.CODCLI  AND 
										C.CODDIS=T.CODDIS  AND
										F.MESREF='$this->mesRef'AND
										(
											F.VALPGT IS NULL OR
											F.VALPGT = 0
										)
								) AS TOTGER,
								(
									SELECT
										SUM(F.RET_INSS)
									FROM
										CADFAT F,
										CADCLI C,
										TABDIS T
									WHERE 
										SYSDATE > F.DATVNC AND
										F.CODCLI=C.CODCLI  AND 
										C.CODDIS=T.CODDIS  AND
										F.MESREF='$this->mesRef'AND
										(
											F.VALPGT IS NULL OR
											F.VALPGT = 0
										)
								) AS TOTRET_INSS,
								(
									SELECT
										SUM(round( F.VALMUL*(SYSDATE-F.DATVNC),2))
									FROM
										CADFAT F,
										CADCLI C,
										TABDIS T
									WHERE 
										SYSDATE > F.DATVNC       AND
										F.CODCLI=C.CODCLI        AND 
										C.CODDIS=T.CODDIS        AND
										F.MESREF='$this->mesRef' AND
										(
											F.VALPGT IS NULL OR
											F.VALPGT = 0
										)
								) AS TOTJUROS
							FROM 
								CADFAT F,
								CADCLI C,
								TABDIS T
							WHERE 
								SYSDATE > F.DATVNC AND
								F.CODCLI=C.CODCLI  AND 
								C.CODDIS=T.CODDIS  AND
								F.MESREF='$this->mesRef' AND
								(
									F.VALPGT IS NULL OR
									F.VALPGT = 0
								)
							ORDER BY 
								C.CODDIS,
								DESCLI"
							);
		}
		if ($CodDis=="Todos")
		{
			$Grupo->query(	"	SELECT
									C.CODDIS,
									SUM(F.VALFAT) AS TotalDistr,
									SUM(F.RET_INSS) AS TotalInssUni,
									SUM(round( F.VALMUL*(SYSDATE-F.DATVNC),2)) AS TotalJurosUni
								FROM
									CADFAT F,
									CADCLI C,
									TABDIS T
								WHERE 
									SYSDATE > F.DATVNC AND
									F.CODCLI=C.CODCLI  AND 
									C.CODDIS=T.CODDIS  AND
									F.MESREF='$this->mesRef' AND
									(
										F.VALPGT IS NULL OR
										F.VALPGT = 0
									)
								GROUP BY 
									C.CODDIS
								ORDER BY
									C.CODDIS
							");
		}
		else
		{
			$Grupo->query(	"	SELECT
									C.CODDIS,
									SUM(F.VALFAT) AS TotalDistr,
									SUM(F.RET_INSS) AS TotalInssUni,
									SUM(round( F.VALMUL*(SYSDATE-F.DATVNC),2)) AS TotalJurosUni
								FROM
									CADFAT F,
									CADCLI C,
									TABDIS T
								WHERE 
									SYSDATE > F.DATVNC AND
									T.CODDIS='$CodDis' AND
									F.CODCLI=C.CODCLI  AND 
									C.CODDIS=T.CODDIS  AND
									MESREF='$this->mesRef' AND
									(
										F.VALPGT IS NULL OR
										F.VALPGT = 0
									)
								GROUP BY 
									C.CODDIS
								ORDER BY
									C.CODDIS
							");
		}
				$Linha = 54;
				$this->SetY($Linha);
				$Grupo->next_record();
				$Tabela->next_record();
				$this->distrito = $Tabela->f("DESDIS");
				$this->mudouDistrito = true;
				$this->addPage('L');
				$l_tottit = 0; // Total de faturas emitidas
				$TotalPGTOUni = 0;
				$TOTVALPGT    = 0;
				do 
				{
					$this->mudouDistrito = false;
					$l_tottit++;
					if ($this->totalGeral==0)
					{
					
						$this->totalGeral = (float)((str_replace(",", ".", $Tabela->f("TOTGER"))));
						$this->totalGeral = number_format($this->totalGeral, 2,',','.');
						
						$TOTRET_INSS = (str_replace(",", ".",$Tabela->f("TOTRET_INSS")));
						$TOTRET_INSS = number_format($TOTRET_INSS, 2,',','.');
						
						$TOTJUROS    = (str_replace(",", ".",$Tabela->f("TOTJUROS")));
						$TOTJUROS    = number_format($TOTJUROS, 2,',','.');
					}

					//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
					if ($this->GetY() >= ($this->fw-12))
					{
						$Linha = 50;
						$this->addPage('L'); 
					}
					//else
					//{
						if ($Tabela->f("CODDIS") != $Grupo->f("CODDIS"))
						{
							$totalDistrito = (float)((str_replace(",", ".", $Grupo->f("TotalDistr"))));
							$totalDistrito = number_format($totalDistrito, 2,',','.');
							$this->Text(3,$Linha,"T  o  t  a  i  s     n  o     D  i  s  t  r  i  t  o");
							//           (       f i n a l                   ) -       t  a  m  n  h  o    
							$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
							$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($totalDistrito)),$Linha,$totalDistrito);
							//
							$TotalInssUni  = (float)((str_replace(",", ".", $Grupo->f("TotalInssUni"))));
							$TotalInssUni = number_format($TotalInssUni, 2,',','.');
							
							$TotalJurosUni = (float)((str_replace(",", ".", $Grupo->f("TotalJurosUni"))));
							$TotalJurosUni = number_format($TotalJurosUni, 2,',','.');
							
							$this->StringTam = $this->GetStringWidth('   INSS   ');
							$this->Text((($this->posINSS + $this->StringTam) - $this->GetStringWidth($TotalInssUni)),$Linha,$TotalInssUni);
							
							$this->StringTam = $this->GetStringWidth('   JUROS   ');
							$this->Text((($this->posJuros + $this->StringTam) - $this->GetStringWidth($TotalJurosUni)),$Linha,$TotalJurosUni);
							
							$this->StringTam = $this->GetStringWidth(' D�BITO ATUAL ');
							$TotalPGTOUni = number_format($TotalPGTOUni, 2,',','.');
							$this->Text((($this->posValRec + $this->StringTam) - $this->GetStringWidth($TotalPGTOUni)),$Linha,$TotalPGTOUni);
							$TotalPGTOUni = 0;
						
							$Grupo->next_record();
							$Linha = 54;
							$this->mudouDistrito = true;
							$this->distrito = $Tabela->f("DESDIS");
							$this->addPage('L'); 
							//$this->SetFont('Arial','B',10);
							//$this->Text(05,50,"Distrito : $distrito");
							//$this->Line(0,51,210.6,51);
							$this->SetFont('Arial','B',06);
						}
					//}
					$this->SetY($Linha);
					$Cliente = $Tabela->f("DESCLI");
					$this->Text(1,$Linha,$Cliente);
		
					$Inscricao = $Tabela->f("CGCCPF");
					if (strlen($Inscricao) == 14)
					{
						// cgc = 11.111.111/1111-11  Ex: 81.243.735/0002-29
						$Inscricao = 	substr($Inscricao,0,2).".".
										substr($Inscricao,2,3).".".
										substr($Inscricao,5,3)."/".
										substr($Inscricao,8,4)."-".
										substr($Inscricao,-2);
					}
					else
					{
						// cpf = 11.111.111/1111-11  Ex: 683.179.306-10
						$Inscricao = 	substr($Inscricao,0,3).".".
										substr($Inscricao,3,3).".".
										substr($Inscricao,6,3)."-".
										substr($Inscricao,-2);
					}
					$this->Text($this->posCPF,$Linha,$Inscricao);
		
					$Fatura = $Tabela->f("CODFAT");
					$this->Text($this->posFatura,$Linha,$Fatura);
				
					$ValFat = $Tabela->f("VALFAT");
					$ValFat = (float)((str_replace(",", ".", $ValFat)));
					$VALPGT = $ValFat;
					$ValFat = number_format($ValFat, 2,',','.');
					//           (       f i n a l                   ) -       t  a  m  n  h  o    
					$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
					$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($ValFat)),$Linha,$ValFat);
					
					$Venci = $Tabela->f("DATVNC");
					$this->Text($this->posVenc,$Linha,$Venci);
					
					$INSS = $Tabela->f("RET_INSS");
					$INSS = (float)((str_replace(",", ".", $INSS)));
					$VALPGT -= $INSS;
					$INSS = number_format($INSS, 2,',','.');

					$this->StringTam = $this->GetStringWidth('   INSS   ');
					$this->Text((($this->posINSS + $this->StringTam) - $this->GetStringWidth($INSS)),$Linha,$INSS);
					
					if ($Tabela->f("NDIAS") > 0)
					{
						$JUROS = $Tabela->f("JUROS");
					}
					else
					{
						$JUROS = 0;
					}
					$JUROS = (float)((str_replace(",", ".", $JUROS)));
					$VALPGT += $JUROS;
					$TotalPGTOUni += $VALPGT;
					$TOTVALPGT    += $VALPGT;
					$JUROS = number_format($JUROS, 2,',','.');
					
					$this->StringTam = $this->GetStringWidth('   JUROS   ');
					$this->Text((($this->posJuros + $this->StringTam) - $this->GetStringWidth($JUROS)),$Linha,$JUROS);
					
					$VALPGT = number_format($VALPGT, 2,',','.');
					
					$this->StringTam = $this->GetStringWidth(' D�BITO ATUAL ');
					$this->Text((($this->posValRec + $this->StringTam) - $this->GetStringWidth($VALPGT)),$Linha,$VALPGT);
					
					$Linha+=4;
				}while ($Tabela->next_record());
				//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
				if ($this->GetY()+24 >= ($this->fw-12))
				{
					$Linha = 50;
					$this->addPage('L'); 
				}
				if (!$this->mudouDistrito)
				{
					$totalDistrito = (float)((str_replace(",", ".", $Grupo->f("TotalDistr"))));
					$totalDistrito = number_format($totalDistrito, 2,',','.');
					$this->Text(3,$Linha,"T  o  t  a  i  s     n  o     D  i  s  t  r  i  t  o");
					//           (       f i n a l                   ) -       t  a  m  n  h  o    
					$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
					$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($totalDistrito)),$Linha,$totalDistrito);

					$TotalInssUni  = (float)((str_replace(",", ".", $Grupo->f("TotalInssUni"))));
					$TotalInssUni = number_format($TotalInssUni, 2,',','.');

					$TotalJurosUni = (float)((str_replace(",", ".", $Grupo->f("TotalJurosUni"))));
					$TotalJurosUni = number_format($TotalJurosUni, 2,',','.');

					$this->StringTam = $this->GetStringWidth('   INSS   ');
					$this->Text((($this->posINSS + $this->StringTam) - $this->GetStringWidth($TotalInssUni)),$Linha,$TotalInssUni);
					
					$this->StringTam = $this->GetStringWidth('   JUROS   ');
					$this->Text((($this->posJuros + $this->StringTam) - $this->GetStringWidth($TotalJurosUni)),$Linha,$TotalJurosUni);
					
					$this->StringTam = $this->GetStringWidth(' D�BITO ATUAL ');
					$TotalPGTOUni = number_format($TotalPGTOUni, 2,',','.');
					$this->Text((($this->posValRec + $this->StringTam) - $this->GetStringWidth($TotalPGTOUni)),$Linha,$TotalPGTOUni);
					$Linha+=4;
				}
				$this->Text(3,$Linha,"T  o  t  a  l     G  e  r  a  l");
				//           (       f i n a l                   ) -       t  a  m  n  h  o    
				$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
				$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$Linha,$this->totalGeral);
				
				$this->StringTam = $this->GetStringWidth('   INSS   ');
				$this->Text((($this->posINSS + $this->StringTam) - $this->GetStringWidth($TOTRET_INSS)),$Linha,$TOTRET_INSS);
				//$this->Text($this->posINSS,$Linha,$TOTRET_INSS);
						
						//$TOTJUROS    = (str_replace(",", ".",$Tabela->f("TOTJUROS")));
						//$TOTJUROS    = number_format($TOTJUROS, 2,',','.');
				$this->StringTam = $this->GetStringWidth('   JUROS   ');
				$this->Text((($this->posJuros + $this->StringTam) - $this->GetStringWidth($TOTJUROS)),$Linha,$TOTJUROS);
				//$this->Text($this->posJuros,$Linha,$TOTJUROS);
						
				$this->StringTam = $this->GetStringWidth(' D�BITO ATUAL ');
				$TOTVALPGT = number_format($TOTVALPGT, 2,',','.');
				$this->Text((($this->posValRec + $this->StringTam) - $this->GetStringWidth($TOTVALPGT)),$Linha,$TOTVALPGT);
				//$this->Text($this->posValRec,$Linha,$TOTVALPGT);
				
				$Linha+=8;
				
				$this->Text(3,$Linha,"TOTAL DE FATURAS EMITIDAS   =>   $l_tottit");

				//$this->SetMargins(5,5,5);
				$this->SetFont('Arial','U',10);
				$this->SetTextColor(0, 0, 0);
				$this->SetAutoPageBreak(1);
				$this->Output();
	}
	function Header()
	{

		$this->SetFont('Arial','B',20);
		// dimens�es da folha A4 - Largura = 210.6 mm e Comprimento 296.93 mm em Portrait. Em Landscape � s� inverter.
		$this->SetXY(0,0);
		//'SECRETARIA MUNICIPAL DE EDUCA��O DE BELO HORIZONTE'
		// Meio = (286/2) - (50/2) = 148,3 - 25 = 123,3
		$tanString = $this->GetStringWidth($this->title);
		$tamPonto = $this->fwPt;
		$tan = $this->fh;
		//$this->Text(($tamPonto/2) - ($tanString/2),6,$this->title);
		//$Unidade = CCGetSession("mDERCRI_UNI");
		$this->Text(($tan/2) - ($tanString/2),8,$this->title);

		//$this->Text(18,19,'DEPARTAMENTO DE ADMINISTRA��O FINANCEIRA');
		$this->SetFont('Arial','B',15);
		//$tanString = $this->GetStringWidth($Unidade);
		//$this->Text(($tan/2) - ($tanString/2),16,$Unidade);
		$this->SetFont('Arial','B',10);
		$tanString = $this->GetStringWidth($this->titulo);
		$this->Text(($tan/2) - ($tanString/2),24,$this->titulo);
		$this->Image('pdf/PBH_-_nova_-_Vertical_-_COR.jpg',5,10,33);
		//$mesRef = CCGetParam("mesRef");
		$this->Text(5,40,'M�s / Ano de Refer�ncia : '.$this->mesRef);
		$dataSist = "Data da Emiss�o : ".CCGetSession("DataSist");
		$tanString = $this->GetStringWidth($dataSist);
		$this->Text($tan-($tanString+5),40,$dataSist);
		$this->Line(0,41,296.93,41);
		//            I  M  P  R  I  M  I     A     Q  U  E  B  R  A     S  E     E  X  I  S  T  I  R
		if ($this->mudouDistrito)
		{
			$this->SetFont('Arial','B',10);
			$this->Text(05,45,"Distrito : $this->distrito");
			$this->Line(0,47,296.93,47);
			$this->SetXY(1,50);
		}
		else
		{
			$this->SetXY(1,45);
		}
		//               T   I   T   U   L   O       D   A   S       C   O  L  U   N   A   S 
		
		// Relat�rio de Arrecada��o
		{
			$this->SetFont('Arial','B',06.5);


			$this->Text(1,$this->GetY(),'NOME DO CLIENTE');
			$tanString = $this->GetStringWidth('NOME DO CLIENTE');
			
			$this->Text($this->GetX()+$tanString+65,$this->GetY(),'CNPJ/CPF');
			$this->posCPF = ($this->GetX()+$tanString+65);
			$this->SetX($this->GetX()+$tanString+65);
			$tanString = $this->GetStringWidth('CNPJ/CPF');
			
			$this->Text($this->GetX()+$tanString+20,$this->GetY(),'FATURA');
			$this->posFatura = ($this->GetX()+$tanString+20);
			$this->SetX($this->GetX()+$tanString+20);
			$tanString = $this->GetStringWidth('FATURA');
			
			$this->Text($this->GetX()+$tanString+15,$this->GetY(),'VALOR DA FATURA');
			$this->posValFat = ($this->GetX()+$tanString+15);
			$this->SetX($this->GetX()+$tanString+15);
			$tanString = $this->GetStringWidth('VALOR DA FATURA');
			
			$this->Text($this->GetX()+$tanString+15,$this->GetY(),'VENCIMENTO');
			$this->posVenc = ($this->GetX()+$tanString+15);
			$this->SetX($this->GetX()+$tanString+15);
			$tanString = $this->GetStringWidth('VENCIMENTO');
			
			$this->Text($this->GetX()+$tanString+15,$this->GetY(),'   INSS   ');
			$this->posINSS = ($this->GetX()+$tanString+15);
			$this->SetX($this->GetX()+$tanString+15);
			$tanString = $this->GetStringWidth('   INSS   ');
			
			$this->Text($this->GetX()+$tanString+15,$this->GetY(),'   JUROS   ');
			$this->posJuros = ($this->GetX()+$tanString+15);
			$this->SetX($this->GetX()+$tanString+15);
			$tanString = $this->GetStringWidth('   JUROS   ');
			
			$this->Text($this->GetX()+$tanString+15,$this->GetY(),' D�BITO ATUAL ');
			$this->posValRec = ($this->GetX()+$tanString+15);
			$this->SetX($this->GetX()+$tanString+15);
			$tanString = $this->GetStringWidth(' D�BITO ATUAL ');
			
			$this->StringTam = $tanString;
			$this->Line(0,$this->GetY()+1,296.93,$this->GetY()+1);
		}
	}
	function footer()
	{
    //Position at 1.5 cm from bottom
    //$this->SetY(-7);
    //Arial italic 8
	//$this->SetFont('Arial','B',06);
	//$this->Text(3,$this->GetY(),"T  o  t  a  l     G  e  r  a  l");
	//           (       f i n a l                   ) -       t  a  m  n  h  o    
	//$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$this->GetY(),$this->totalGeral);
    //Position at 1.5 cm from bottom
    $this->SetY(-8);
    //Arial italic 8
    $this->SetFont('Arial','I',8);
	$this->Cell(0,10,'P�gina '.$this->PageNo().' / {nb}',0,0,'C');
    //Page number
	}
}
//unset($Tabela);
//unset($Grupo);
	/*$Imprime = new relatoriosPDF();
	$title = 'SUPERINTEND�NCIA DE LIMPESA URBANA';
	$Imprime->addPage('Portrait'); 
	$Imprime->SetFont('Arial','',10);
	$Imprime->SetTextColor(0, 0, 0);
	$Imprime->SetAutoPageBreak(0);
	$Imprime->Output();*/
?>