<?php
	//error_reporting (E_ALL);
	///define("RelativePath", ".");
	///define("PathToCurrentPage", "/");
	///define("FileName", "RelDebGeraPDF.php");
	//define('FPDF_FONTPATH','/pdf/font/');
	require('pdf/fpdf.php');
	//include('pdf/fpdi.php');
	//require_once("pdf/fpdi_pdf_parser.php");
	//require_once("pdf/fpdf_tpl.php");
	///include(RelativePath . "/Common.php");
	//include(RelativePath . "/Template.php");
	//include(RelativePath . "/Sorter.php");
	//include(RelativePath . "/Navigator.php");
class relDebGeraPDF extends fpdf {
	var $titulo;
	var $mesRef;
	var $posInscricao;
	var $posCliente;
	var $posFatura;
	var $posEmissao;
	var $posValFat;
	var $posVenc;
	var $totalGeral;
	var $StringTam;
	var $relat;
	var $mudouDistrito;
	var $distrito;
	var $posMesAno;
	var $posINSS;
	var $posPGT;
	var $posAcres;
	var $posRec;
	var $posISSQN;
	var $posJuros;
	var $posValRec;
	//$pdf= new fpdi();
	function relatorio($titu,$Opcao)
	{
		$this->SetTitle('SUPERINTEND�NCIA DE LIMPEZA URBANA');
		$this->titulo = $titu;
		$this->AliasNbPages();
		$Tabela   = new clsDBfaturar();
		//              L   I   N   H   A       D   E       D   E   T   A   L   H   E
		//                       Relat�rio de D�bitos por Inscri��o
		if ($Opcao == "('A')")
		{
			$this->mesRef = "Somente Clientes Ativos";
		}
		else if ($Opcao == "('I')")
		{
			$this->mesRef = "Somente Clientes Inativos";
		}
		else
		{
			$this->mesRef = "Todos os Clientes";
		}
		$Tabela->query("SELECT 
							F.CODCLI,
							to_char(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
							F.CODFAT,
							F.VALFAT,
							F.RET_INSS,
							F.ISSQN,
							round( ((F.VALFAT-F.RET_INSS)*(1/100/30))*floor(SYSDATE-F.DATVNC),2) AS JUROS,
							(F.VALFAT-F.RET_INSS)+round( ((F.VALFAT-F.RET_INSS)*(1/100/30))*floor(SYSDATE-F.DATVNC),2) as VALPGT,
							C.DESCLI,
							C.CGCCPF,
							C.ESFERA,
							(
								SELECT
									SUM(F.VALFAT)
								FROM
									CADFAT F,
									CADCLI C
								WHERE 
									SYSDATE > F.DATVNC       AND
									F.CODCLI=C.CODCLI  AND
									C.CODSIT in $Opcao AND
									(
										F.VALPGT IS NULL   OR
										F.VALPGT = 0
									)
							) AS TOTGER,
							(
								SELECT
									SUM(F.ISSQN)
								FROM
									CADFAT F,
									CADCLI C
								WHERE 
									SYSDATE > F.DATVNC       AND
									F.CODCLI=C.CODCLI  AND 
									C.CODSIT in $Opcao AND
									(
										F.VALPGT IS NULL or
										F.VALPGT = 0
									)
							) AS TOTISSQN,
							(
								SELECT
									SUM(F.RET_INSS)
								FROM
									CADFAT F,
									CADCLI C
								WHERE 
									SYSDATE > F.DATVNC       AND
									F.CODCLI=C.CODCLI  AND 
									C.CODSIT in $Opcao AND
									(
										F.VALPGT IS NULL   OR
										F.VALPGT = 0
									)
							) AS TOTINSS
						FROM 
							CADFAT F,
							CADCLI C 
						WHERE 
							SYSDATE > F.DATVNC       AND
							F.CODCLI=C.CODCLI  AND 
							C.CODSIT in $Opcao AND
							(
								F.VALPGT IS NULL   OR
								F.VALPGT = 0
							)
						ORDER BY 
							DESCLI,
							CODFAT"
						);
				//$articles = array(); 	ISSQN -> SUBSTR(F.MESREF,4,4)||SUBSTR(F.MESREF,1,2) < '200812' AND

				$Linha = 50;
				$TotalJuros  = 0;
				$TotalDebito = 0;
				$this->SetY($Linha);
				$this->addPage('L');
				while ($Tabela->next_record())
				{
					if ($this->totalGeral==0)
					{
						$this->totalGeral = (float)((str_replace(",", ".", $Tabela->f("TOTGER"))));
						$this->totalGeral = number_format($this->totalGeral, 2,',','.');
					}
						//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
					if ($this->GetY() >= ($this->fw-12))
					{
						$Linha = 50;
						$this->addPage('L'); 
					}
					$this->SetY($Linha);
					$codcli = $Tabela->f("CODCLI");
					$this->Text($this->posInscricao,$Linha,$codcli);


					$Cliente = $Tabela->f("DESCLI");
					$this->Text($this->posCliente,$Linha,substr($Cliente,0,35));
		
					$Inscricao = $Tabela->f("CGCCPF");
					if (strlen(trim($Inscricao))==14)
					{
						$Inscricao = substr($Inscricao,0,2).".".substr($Inscricao,2,3).".".substr($Inscricao,5,3).".".substr($Inscricao,8,3).".".substr($Inscricao,11,3);
					}
					else
					{
						$Inscricao = substr($Inscricao,2,3).".".substr($Inscricao,5,3).".".substr($Inscricao,8,3).".".substr($Inscricao,11,2);
					}
					$this->Text($this->posCPF,$Linha,$Inscricao);
					
					$Fatura = $Tabela->f("CODFAT");
					//$Fatura = substr($Fatura,0,3).'.'.substr($Fatura,-3);
					$this->Text($this->posFatura,$Linha,$Fatura);
				
					$ValFat = $Tabela->f("VALFAT");
					$ValFat = (float)((str_replace(",", ".", $ValFat)));
					$VALPGT = $ValFat;
					$ValFat = number_format($ValFat, 2,',','.');
					//           (       f i n a l                   ) -       t  a  m  n  h  o   
					$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
				    $this->Text($this->posValFat+$this->StringTam-$this->GetStringWidth($ValFat),$Linha,$ValFat);
					
					
					$Venci = $Tabela->f("DATVNC");
					$this->Text($this->posVenc,$Linha,$Venci);
					
					$INSS = $Tabela->f("RET_INSS");
					$INSS = (float)((str_replace(",", ".", $INSS)));
					$VALPGT -= $INSS;
					$INSS = number_format($INSS, 2,',','.');
					$this->StringTam = $this->GetStringWidth('   INSS   ');
					$this->Text((($this->posINSS + $this->StringTam) - $this->GetStringWidth($INSS)),$Linha,$INSS);
					//$this->Text($this->posINSS,$Linha,$INSS);
					
					$ISSQN = $Tabela->f("ISSQN");
					$ISSQN = (float)((str_replace(",", ".", $ISSQN)));
					$ISSQN = number_format($ISSQN, 2,',','.');
					$this->StringTam = $this->GetStringWidth('  ISSQN  ');
					$this->Text((($this->posISSQN + $this->StringTam) - $this->GetStringWidth($ISSQN)),$Linha,$ISSQN);
					
					$JUROS = $Tabela->f("JUROS");
					$JUROS = (float)((str_replace(",", ".", $JUROS)));
					$VALPGT += $JUROS;
					$TotalJuros  += $JUROS;
					$TotalDebito += $VALPGT;
					$JUROS = number_format($JUROS, 2,',','.');
					$this->StringTam = $this->GetStringWidth('   JUROS   ');
					$this->Text((($this->posJuros + $this->StringTam) - $this->GetStringWidth($JUROS)),$Linha,$JUROS);

					//$this->Text($this->posJuros,$Linha,$JUROS);
					
					//$VALPGT = $Tabela->f("VALFAT") + $Tabela->f("JUROS") - $Tabela->f("RET_INSS");
					//$VALPGT = (float)((str_replace(",", ".", $VALPGT)));
					$VALPGT = number_format($VALPGT, 2,',','.');

					$this->StringTam = $this->GetStringWidth(' D�BITO ATUAL ');
					
					$this->Text((($this->posValRec + $this->StringTam) - $this->GetStringWidth($VALPGT)),$Linha,$VALPGT);
			        
			        

					
					//$this->Text($this->posValRec,$Linha,$VALPGT);
					
					$Linha+=4;
				}
				$this->Text(3,$Linha,"T  o  t  a  l     G  e  r  a  l");
				$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
				//           (       f i n a l                   ) -       t  a  m  n  h  o    
				$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$Linha,$this->totalGeral);
				
				$TOTINSS = $Tabela->f("TOTINSS");
				$TOTINSS = (float)((str_replace(",", ".", $TOTINSS)));
				$TOTINSS = number_format($TOTINSS, 2,',','.');
				$this->StringTam = $this->GetStringWidth('   INSS   ');
				$this->Text((($this->posINSS + $this->StringTam) - $this->GetStringWidth($TOTINSS)),$Linha,$TOTINSS);

				$TOTISSQN = $Tabela->f("TOTISSQN");
				$TOTISSQN = (float)((str_replace(",", ".", $TOTISSQN)));
				$TOTISSQN = number_format($TOTISSQN, 2,',','.');
				$this->StringTam = $this->GetStringWidth('  ISSQN  ');
				$this->Text((($this->posISSQN + $this->StringTam) - $this->GetStringWidth($TOTISSQN)),$Linha,$TOTISSQN);
				
				$this->StringTam = $this->GetStringWidth('   JUROS   ');
				$TotalJuros = number_format($TotalJuros, 2,',','.');
				$this->Text((($this->posJuros + $this->StringTam) - $this->GetStringWidth($TotalJuros)),$Linha,$TotalJuros);

				$this->StringTam = $this->GetStringWidth(' D�BITO ATUAL ');
				$TotalDebito = number_format($TotalDebito, 2,',','.');
				$this->Text((($this->posValRec + $this->StringTam) - $this->GetStringWidth($TotalDebito)),$Linha,$TotalDebito);

				//$this->SetMargins(5,5,5);
				$this->SetFont('Arial','U',10);
				$this->SetTextColor(0, 0, 0);
				$this->SetAutoPageBreak(1);
				$this->Output();
	}
	function Header()
	{

		$this->SetFont('Arial','B',20);
		// dimens�es da folha A4 - Largura = 210.6 mm e Comprimento 296.93 mm em Portrait. Em Landscape � s� inverter.
		$this->SetXY(0,0);
		//'SECRETARIA MUNICIPAL DE EDUCA��O DE BELO HORIZONTE'
		// Meio = (286/2) - (50/2) = 148,3 - 25 = 123,3
		$tanString = $this->GetStringWidth($this->title);
		$tamPonto = $this->fwPt;
		$tan = $this->fh;
		//$this->Text(($tamPonto/2) - ($tanString/2),6,$this->title);
		$Unidade = CCGetSession("mDERCRI_UNI");
		$this->Text(($tan/2) - ($tanString/2),8,$this->title);

		//$this->Text(18,19,'DEPARTAMENTO DE ADMINISTRA��O FINANCEIRA');
		$this->SetFont('Arial','B',15);
		$tanString = $this->GetStringWidth($Unidade);
		//$this->Text(($tan/2) - ($tanString/2),16,$Unidade);
		$this->SetFont('Arial','B',10);
		$tanString = $this->GetStringWidth($this->titulo);
		$this->Text(($tan/2) - ($tanString/2),24,$this->titulo);
		$this->Image('pdf/PBH_-_nova_-_Vertical_-_COR.jpg',5,10,33);
		//$mesRef = CCGetParam("mesRef");
		$this->Text(5,40,$this->mesRef);
		$dataSist = "Data da Emiss�o : ".CCGetSession("DataSist");
		$tanString = $this->GetStringWidth($dataSist);
		$this->Text($tan-($tanString+5),40,$dataSist);
		$this->Line(0,41,296.93,41);
		//               T   I   T   U   L   O       D   A   S       C   O  L  U   N   A   S 
		
		// Relat�rio de Arrecada��o
		{
			$this->SetFont('Arial','B',6.5);

			$this->SetXY(1,45);
			
			$Coluna = 12;

             $this->SetX(5);

            $this->posInscricao=$this->GetX();
			$this->Text($this->posInscricao,$this->GetY(),'INSCRI��O');
			$tanString = $this->GetStringWidth('INSCRI��O');
			$this->SetX($this->GetX()+$tanString+10);


            $this->posCliente=$this->GetX();
			$this->Text($this->posCliente,$this->GetY(),'NOME DO CLIENTE');
			$tanString = $this->GetStringWidth('NOME DO CLIENTE');
			$this->SetX($this->GetX()+$tanString+45);
			
			$this->posCPF = ($this->GetX());
			$this->Text($this->posCPF,$this->GetY(),'CNPJ/CPF');
			$tanString = $this->GetStringWidth('CNPJ/CPF');
			$this->SetX($this->GetX()+$tanString+12);
			
			$this->posFatura = ($this->GetX());
			$this->Text($this->posFatura,$this->GetY(),'FATURA');
			$tanString = $this->GetStringWidth('FATURA');
			$this->SetX($this->GetX()+$tanString+$Coluna);
			
			$this->posVenc = ($this->GetX());
			$this->Text($this->posVenc,$this->GetY(),'VENCIMENTO');
			$tanString = $this->GetStringWidth('VENCIMENTO');
			$this->SetX($this->GetX()+$tanString+$Coluna);
			
			$this->posINSS = ($this->GetX());
			$this->Text($this->posINSS,$this->GetY(),'   INSS   ');
			$tanString = $this->GetStringWidth('   INSS   ');
			$this->SetX($this->GetX()+$tanString+$Coluna);

			$this->posISSQN = ($this->GetX());
			$this->Text($this->posISSQN,$this->GetY(),'  ISSQN  ');
			$tanString = $this->GetStringWidth('  ISSQN  ');
			$this->SetX($this->GetX()+$tanString+$Coluna);
			
			$this->posJuros = ($this->GetX());
			$this->Text($this->posJuros,$this->GetY(),'   JUROS   ');
			$tanString = $this->GetStringWidth('   JUROS   ');
			$this->SetX($this->GetX()+$tanString+$Coluna);
			
			$this->posValRec = ($this->GetX());
			$this->Text($this->posValRec,$this->GetY(),' D�BITO ATUAL ');
			$tanString = $this->GetStringWidth(' D�BITO ATUAL ');
			$this->SetX($this->GetX()+$tanString+$Coluna);

			$this->posValFat = $this->GetX();
			$this->Text($this->posValFat,$this->GetY(),'VALOR DA FATURA');
			$tanString = $this->GetStringWidth('TOTAL FATURADO');
			$this->SetX($this->GetX()+$tanString+$Coluna);
			$this->StringTam = $tanString;

			
			$this->StringTam = $tanString;
			$this->Line(0,$this->GetY()+1,296.93,$this->GetY()+1);
		}
	}
	function footer()
	{
    //Position at 1.5 cm from bottom
    //$this->SetY(-7);
    //Arial italic 8
	//$this->SetFont('Arial','B',06);
	//$this->Text(3,$this->GetY(),"T  o  t  a  l     G  e  r  a  l");
	//           (       f i n a l                   ) -       t  a  m  n  h  o    
	//$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$this->GetY(),$this->totalGeral);
    //Position at 1.5 cm from bottom
    $this->SetY(-8);
    //Arial italic 8
    $this->SetFont('Arial','I',12);
	$this->Cell(0,10,'P�gina '.$this->PageNo().' / {nb}',0,0,'C');
    //Page number
	}
}
//unset($Tabela);
//unset($Grupo);
	/*$Imprime = new relatoriosPDF();
	$title = 'SUPERINTEND�NCIA DE LIMPESA URBANA';
	$Imprime->addPage('Portrait'); 
	$Imprime->SetFont('Arial','',10);
	$Imprime->SetTextColor(0, 0, 0);
	$Imprime->SetAutoPageBreak(0);
	$Imprime->Output();*/
?>