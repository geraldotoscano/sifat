<?php
	
error_reporting (E_ALL & ~E_DEPRECATED);
//define("RelativePath", "../..");
//define('FPDF_FONTPATH','/pdf/font/');
require('pdf/fpdf.php');
//include('pdf/fpdi.php');
//require_once("pdf/fpdi_pdf_parser.php");
//require_once("pdf/fpdf_tpl.php");
//include(RelativePath . "/Common.php");
//include(RelativePath . "/Template.php");
//include(RelativePath . "/Sorter.php");
//include(RelativePath . "/Navigator.php");

class relDebSerPDF extends fpdf {	
	var $titulo;
	var $mesRef;
	var $posCliente;
	var $posFatura;
	var $posValFat;
	var $posVenc;
	var $StringTam;
	var $relat;
	var $mudouServico;
	var $servico;
	var $posMesAno;
	var $posINSS;
	var $posAcres;
	var $posRec;
	var $posJuros;
	var $posValRec;

	function relatorio($titu,$mesAno,$rel,$CodDis='')
	{
		$this->mesRef = $mesAno;
		$this->SetTitle('SUPERINTEND�NCIA DE LIMPEZA URBANA');
		$this->titulo = $titu;
		$this->AliasNbPages();
		$Tabela = new clsDBfaturar();

		//Relat�rio de D�BITOS por SERVI�O em um M�S espec�fico
		$Tabela->query("
		 SELECT 
			T.DESSER,
			C.DESCLI,
			F.CODFAT,
			VALFAT,
			to_char(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
			FLOOR(SYSDATE-F.DATVNC) AS NDIAS,
			round((F.VALFAT-RET_INSS) * (0.01/30) * FLOOR(SYSDATE-F.DATVNC),2) AS JUROS,
			F.RET_INSS,
			C.CGCCPF,
			T.GRPSER
		FROM 
			CADFAT F,
			CADCLI C,
			GRPSER T,
			MOVFAT M
		WHERE 
			SYSDATE > F.DATVNC AND
			".(is_numeric($CodDis) ? 'T.grpser=$CodDis AND' : '')."
			M.CODFAT=F.CODFAT  AND
			T.GRPSER=M.GRPSER  AND
			F.CODCLI=C.CODCLI  AND 
			F.MESREF='$this->mesRef' AND
			(
			  F.VALPGT IS NULL OR
			  F.VALPGT = 0
			)
		GROUP BY
			T.DESSER,
			C.DESCLI,
			F.CODFAT,
			F.DATVNC,
			F.VALFAT,
			F.RET_INSS,
			C.CGCCPF,
			T.GRPSER
		ORDER BY
			T.GRPSER,
			C.DESCLI"
		);
		
		$Linha = 54;
		$this->SetY($Linha);
		$Tabela->next_record();
		$cod_servico = $Tabela->f("GRPSER");
		$this->servico = $Tabela->f("DESSER");
		$this->mudouServico = true;
		$this->addPage('L');
		$l_tottit = 0; // Total de faturas emitidas
		$TOTRET_INSS = 0;
		$TOTJUROS = 0;
		$TOTFAT = 0;

		do 
		{
			$this->mudouServico = false;
			$l_tottit++;

			//QUEBRA DE P�GINA 
			if ($this->GetY() >= ($this->fw-12))
			{
				$Linha = 50;
				$this->addPage('L'); 
			}

			//Encontrou um servi�o diferente
			if ($Tabela->f("GRPSER") != $cod_servico)
			{	
				//exibe os totais do servi�o anterior
				$this->Text(3,$Linha,"T  o  t  a  i  s     n  o     S  e  r  v  i  �  o");
				
				$DEBT_servico = number_format($VALFAT_servico - $RET_INSS_servico + $JUROS_servico, 2,',','.');
				$VALFAT_servico = number_format($VALFAT_servico, 2,',','.');
				$JUROS_servico = number_format($JUROS_servico, 2,',','.');
				$RET_INSS_servico = number_format($RET_INSS_servico, 2,',','.');
				
				$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
				$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($VALFAT_servico)),$Linha,$VALFAT_servico);

				$this->StringTam = $this->GetStringWidth('   INSS   ');
				$this->Text((($this->posINSS + $this->StringTam) - $this->GetStringWidth($RET_INSS_servico)),$Linha,$RET_INSS_servico);
				
				$this->StringTam = $this->GetStringWidth('   JUROS   ');
				$this->Text((($this->posJuros + $this->StringTam) - $this->GetStringWidth($JUROS_servico)),$Linha,$JUROS_servico);
				
				$this->StringTam = $this->GetStringWidth(' D�BITO ATUAL ');
				$this->Text((($this->posValRec + $this->StringTam) - $this->GetStringWidth($DEBT_servico)),$Linha,$DEBT_servico);

				$VALFAT_servico = 0;
				$JUROS_servico = 0;
				$RET_INSS_servico = 0;
			
				//$Grupo->next_record();
				$Linha = 54;
				$this->mudouServico = true;
				$cod_servico = $Tabela->f("GRPSER");
				$this->servico = $Tabela->f("DESSER");
				$this->addPage('L'); 

				$this->SetFont('Arial','B',06);
			}

			$this->SetY($Linha);
			$Cliente = $Tabela->f("DESCLI");
			$this->Text(1,$Linha,$Cliente);

			$CGC = $Tabela->f("CGCCPF");
			// cgc = 11.111.111/1111-11  Ex: 81.243.735/0002-29
			if (strlen($CGC) == 14)
				$CGC = substr($CGC,0,2).".".substr($CGC,2,3).".".substr($CGC,5,3)."/".substr($CGC,8,4)."-".substr($CGC,-2);
			// cpf = 11.111.111/1111-11  Ex: 683.179.306-10
			else
				$CGC = substr($CGC,0,3).".".substr($CGC,3,3).".".substr($CGC,6,3)."-".substr($CGC,-2);
			$this->Text($this->posCPF,$Linha,$CGC);

			$Fatura = $Tabela->f("CODFAT");
			$this->Text($this->posFatura,$Linha,$Fatura);
		
			$ValFat = $Tabela->f("VALFAT");
			$ValFat = (float)((str_replace(",", ".", $ValFat)));
			$TOTFAT += $ValFat;
			$VALFAT_servico +=  $ValFat;
			
			$INSS = $Tabela->f("RET_INSS");
			$INSS = (float)((str_replace(",", ".", $INSS)));
			$TOTRET_INSS += $INSS;
			$RET_INSS_servico += $INSS;
			
			$JUROS = ($Tabela->f("NDIAS") > 0) ? $Tabela->f("JUROS") : 0;
			$JUROS = (float)((str_replace(",", ".", $JUROS)));
			$TOTJUROS += $JUROS;
			$JUROS_servico += $JUROS;
			
			$VALPGT = $ValFat - $INSS + $JUROS;
			
			$ValFat = number_format($ValFat, 2,',','.');
			$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
			$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($ValFat)),$Linha,$ValFat);
			
			$Venci = $Tabela->f("DATVNC");
			$this->Text($this->posVenc,$Linha,$Venci);

			$INSS = number_format($INSS, 2,',','.');
			$this->StringTam = $this->GetStringWidth('   INSS   ');
			$this->Text((($this->posINSS + $this->StringTam) - $this->GetStringWidth($INSS)),$Linha,$INSS);
			
			$JUROS = number_format($JUROS, 2,',','.');
			$this->StringTam = $this->GetStringWidth('   JUROS   ');
			$this->Text((($this->posJuros + $this->StringTam) - $this->GetStringWidth($JUROS)),$Linha,$JUROS);
			
			$VALPGT = number_format($VALPGT, 2,',','.');
			$this->StringTam = $this->GetStringWidth(' D�BITO ATUAL ');
			$this->Text((($this->posValRec + $this->StringTam) - $this->GetStringWidth($VALPGT)),$Linha,$VALPGT);
			
			$Linha+=4;
		} 
		while ($Tabela->next_record());

		//QUEBRA DE P�GINA
		if ($this->GetY()+24 >= ($this->fw-12))
		{
			$Linha = 50;
			$this->addPage('L'); 
		}
		if (!$this->mudouServico)
		{
				//exibe os totais do �ltimo servi�o
				$this->Text(3,$Linha,"T  o  t  a  i  s     n  o     S  e  r  v  i  �  o");
				
				$DEBT_servico = number_format($VALFAT_servico - $RET_INSS_servico + $JUROS_servico, 2,',','.');
				$VALFAT_servico = number_format($VALFAT_servico, 2,',','.');
				$JUROS_servico = number_format($JUROS_servico, 2,',','.');
				$RET_INSS_servico = number_format($RET_INSS_servico, 2,',','.');
				
				$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
				$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($VALFAT_servico)),$Linha,$VALFAT_servico);

				$this->StringTam = $this->GetStringWidth('   INSS   ');
				$this->Text((($this->posINSS + $this->StringTam) - $this->GetStringWidth($RET_INSS_servico)),$Linha,$RET_INSS_servico);
				
				$this->StringTam = $this->GetStringWidth('   JUROS   ');
				$this->Text((($this->posJuros + $this->StringTam) - $this->GetStringWidth($JUROS_servico)),$Linha,$JUROS_servico);
				
				$this->StringTam = $this->GetStringWidth(' D�BITO ATUAL ');
				$this->Text((($this->posValRec + $this->StringTam) - $this->GetStringWidth($DEBT_servico)),$Linha,$DEBT_servico);
				
				$Linha+=4;
		}
		
		//Exibe totais gerais
		$this->Text(3,$Linha,"T  o  t  a  l     G  e  r  a  l");
		$TOTDEBITO = number_format($TOTFAT - $TOTRET_INSS + $TOTJUROS, 2,',','.');

		$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
		$TOTFAT =  number_format($TOTFAT, 2,',','.');
		$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($TOTFAT)),$Linha,$TOTFAT);
		
		$this->StringTam = $this->GetStringWidth('   INSS   ');
		$TOTRET_INSS =  number_format($TOTRET_INSS, 2,',','.');
		$this->Text((($this->posINSS + $this->StringTam) - $this->GetStringWidth($TOTRET_INSS)),$Linha,$TOTRET_INSS);

		$this->StringTam = $this->GetStringWidth('   JUROS   ');
		$TOTJUROS =  number_format($TOTJUROS, 2,',','.');
		$this->Text((($this->posJuros + $this->StringTam) - $this->GetStringWidth($TOTJUROS)),$Linha,$TOTJUROS);
				
		$this->StringTam = $this->GetStringWidth(' D�BITO ATUAL ');
		$this->Text((($this->posValRec + $this->StringTam) - $this->GetStringWidth($TOTDEBITO)),$Linha,$TOTDEBITO);
		
		$Linha+=8;
		
		$this->Text(3,$Linha,"TOTAL DE FATURAS EMITIDAS   =>   $l_tottit");
		
		//$this->SetMargins(5,5,5);
		$this->SetFont('Arial','U',10);
		$this->SetTextColor(0, 0, 0);
		$this->SetAutoPageBreak(1);
		//$this->Output('C:\Documents and Settings\pb003280\Desktop\teste1.pdf','F');
		$this->Output();
	}
	function Header()
	{

		$this->SetFont('Arial','B',20);
		// dimens�es da folha A4 - Largura = 210.6 mm e Comprimento 296.93 mm em Portrait. Em Landscape � s� inverter.
		$this->SetXY(0,0);
		//'SECRETARIA MUNICIPAL DE EDUCA��O DE BELO HORIZONTE'
		// Meio = (286/2) - (50/2) = 148,3 - 25 = 123,3
		$tanString = $this->GetStringWidth($this->title);
		$tamPonto = $this->fwPt;
		$tan = $this->fh;
		//$this->Text(($tamPonto/2) - ($tanString/2),6,$this->title);
		//$Unidade = CCGetSession("mDERCRI_UNI");
		$this->Text(($tan/2) - ($tanString/2),8,$this->title);

		//$this->Text(18,19,'DEPARTAMENTO DE ADMINISTRA��O FINANCEIRA');
		$this->SetFont('Arial','B',15);
		//$tanString = $this->GetStringWidth($Unidade);
		//$this->Text(($tan/2) - ($tanString/2),16,$Unidade);
		$this->SetFont('Arial','B',10);
		$tanString = $this->GetStringWidth($this->titulo);
		$this->Text(($tan/2) - ($tanString/2),24,$this->titulo);
		$this->Image('pdf/PBH_-_nova_-_Vertical_-_COR.jpg',5,10,33);
		//$mesRef = CCGetParam("mesRef");
		$this->Text(5,40,'M�s / Ano de Refer�ncia : '.$this->mesRef);
		$dataSist = "Data da Emiss�o : ".CCGetSession("DataSist");
		$tanString = $this->GetStringWidth($dataSist);
		$this->Text($tan-($tanString+5),40,$dataSist);
		$this->Line(0,41,296.93,41);
		
		//IMPRIME A QUEBRA DE P�GINA SE HOUVER
		if ($this->mudouServico)
		{
			$this->SetFont('Arial','B',10);
			$this->Text(05,45,"Servi�o : $this->servico");
			$this->Line(0,47,296.93,47);
			$this->SetXY(1,50);
		}
		else
		{
			$this->SetXY(1,45);
		}
		
		//T�TULOS DAS COLUNAS 
		$this->SetFont('Arial','B',06.5);

		$this->Text(1,$this->GetY(),'NOME DO CLIENTE');
		$tanString = $this->GetStringWidth('NOME DO CLIENTE');
		
		$this->Text($this->GetX()+$tanString+65,$this->GetY(),'CNPJ/CPF');
		$this->posCPF = ($this->GetX()+$tanString+65);
		$this->SetX($this->GetX()+$tanString+65);
		$tanString = $this->GetStringWidth('CNPJ/CPF');
		
		$this->Text($this->GetX()+$tanString+20,$this->GetY(),'FATURA');
		$this->posFatura = ($this->GetX()+$tanString+20);
		$this->SetX($this->GetX()+$tanString+20);
		$tanString = $this->GetStringWidth('FATURA');
		
		$this->Text($this->GetX()+$tanString+15,$this->GetY(),'VALOR DA FATURA');
		$this->posValFat = ($this->GetX()+$tanString+15);
		$this->SetX($this->GetX()+$tanString+15);
		$tanString = $this->GetStringWidth('VALOR DA FATURA');
		
		$this->Text($this->GetX()+$tanString+15,$this->GetY(),'VENCIMENTO');
		$this->posVenc = ($this->GetX()+$tanString+15);
		$this->SetX($this->GetX()+$tanString+15);
		$tanString = $this->GetStringWidth('VENCIMENTO');
		
		$this->Text($this->GetX()+$tanString+15,$this->GetY(),'   INSS   ');
		$this->posINSS = ($this->GetX()+$tanString+15);
		$this->SetX($this->GetX()+$tanString+15);
		$tanString = $this->GetStringWidth('   INSS   ');
		
		$this->Text($this->GetX()+$tanString+15,$this->GetY(),'   JUROS   ');
		$this->posJuros = ($this->GetX()+$tanString+15);
		$this->SetX($this->GetX()+$tanString+15);
		$tanString = $this->GetStringWidth('   JUROS   ');
		
		$this->Text($this->GetX()+$tanString+15,$this->GetY(),' D�BITO ATUAL ');
		$this->posValRec = ($this->GetX()+$tanString+15);
		$this->SetX($this->GetX()+$tanString+15);
		$tanString = $this->GetStringWidth(' D�BITO ATUAL ');
		
		$this->StringTam = $tanString;
		$this->Line(0,$this->GetY()+1,296.93,$this->GetY()+1);
	}
	
	function footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-8);
		//Arial italic 8
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,'P�gina '.$this->PageNo().' / {nb}',0,0,'C');
	}
}

?>