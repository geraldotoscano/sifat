<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="14" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="4" sourceType="Table" urlType="Relative" secured="False" allowInsert="True" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="GET" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="FormFiltro" actionPage="RelEmisInsc" errorSummator="Error" wizardFormMethod="post" wizardCaption="{res:NRRelEmisInc}" returnPage="index.ccp" pasteAsReplace="pasteAsReplace">
			<Components>
				<TextBox id="5" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="MES_REF_INI" wizardCaption="{res:TextBox1}" sourceType="Table" format="mm/yyyy" caption="Mês inicial" required="True">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Validate Entry" actionCategory="Validation" id="13" required="True" minimumLength="7" maximumLength="7" inputMask="00/0000" regularExpression="^\d{2}\/\d{4}$" errorMessage="Data de início inválida. Deve ser digitado mês e ano. Ex.: 01/2017." eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
				</TextBox>
				<TextBox id="23" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="MES_REF_FIM" required="True" format="mm/yyyy" caption="Mês final">
					<Components/>
					<Events>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Validate Entry" actionCategory="Validation" id="27" required="True" minimumLength="7" maximumLength="7" inputMask="00/0000" regularExpression="^\d{2}\/\d{4}$" errorMessage="Data final inválida. Deve ser digitado mês e ano. Ex.: 01/2017." eventType="Client"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<ListBox id="18" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="GRPATI" wizardEmptyCaption="{res:CCS_SelectValue}" connection="Faturar" dataSource="GRPATI" boundColumn="GRPATI" textColumn="DESATI" orderBy="TRIM(DESATI)">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="28" tableName="GRPATI" posLeft="10" posTop="10" posWidth="160" posHeight="40"/>
					</JoinTables>
					<JoinLinks/>
					<Fields>
						<Field id="29" fieldName="GRPATI" isExpression="True" alias="GRPATI"/>
						<Field id="30" fieldName="TRIM(DESATI)" isExpression="True" alias="DESATI"/>
					</Fields>
					<Attributes/>
					<Features/>
				</ListBox>
				<RadioButton id="31" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" html="True" returnValueType="Number" name="RBFormatoRelatorio" caption="Formato do relatório" dataSource="PDF;PDF;CSV;CSV" required="True" defaultValue="&quot;PDF&quot;">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</RadioButton>
				<Button id="7" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Insert" wizardCaption="{res:CCS_Insert}">
					<Components/>
					<Events>
						<Event name="OnClick" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="11" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="8" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Cancel" operation="Cancel" wizardCaption="{res:CCS_Cancel}">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events>
				<Event name="OnValidate" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="26" eventType="Server"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<IncludePage id="15" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="PHPTemplates" name="RelDebiAtiv_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Code" language="PHPTemplates" name="RelDebiAtiv.php" forShow="True" url="RelDebiAtiv.php" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="19" groupID="1"/>
		<Group id="20" groupID="2"/>
		<Group id="21" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="25"/>
			</Actions>
		</Event>
	</Events>
</Page>
