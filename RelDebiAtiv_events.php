<?php
//BindEvents Method @1-3F8AC505
function BindEvents()
{
    global $FormFiltro;
    global $CCSEvents;
    $FormFiltro->Button_Insert->CCSEvents["OnClick"] = "FormFiltro_Button_Insert_OnClick";
    $FormFiltro->CCSEvents["OnValidate"] = "FormFiltro_OnValidate";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//FormFiltro_Button_Insert_OnClick @7-E638611D
function FormFiltro_Button_Insert_OnClick(& $sender)
{
    $FormFiltro_Button_Insert_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $FormFiltro; //Compatibility
//End FormFiltro_Button_Insert_OnClick

//Custom Code @11-2A29BDB7
// -------------------------
	$bd = new clsDBfaturar();
	$descricao_filtros = array();
	//determina a atividade
	$atividade = (int)$FormFiltro->GRPATI->GetValue();
	if($atividade) {
		$descricao_filtros[] = 'Atividade: '.CCDLookUp('DESATI','GRPATI',"GRPATI = $atividade", $bd);
		$filtro_atividade = "AND T.GRPATI = $atividade";
	} else {
		$descricao_filtros[] = 'Todas as Atividades';
		$filtro_atividade = '';
	}

    //instru��o para consultar as faturas da ativade para cliente no per�odo
    $sql_faturas = "
	SELECT 
		F.CODCLI,
		TO_CHAR(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
		ROUND((F.VALFAT-F.RET_INSS)*(0.01/30)*FLOOR(SYSDATE-F.DATVNC), 2) AS JUROS,
		ROUND((F.VALFAT-F.RET_INSS)*(1+(0.01/30)*FLOOR(SYSDATE-F.DATVNC)), 2) AS VALPGT,
		F.CODFAT,
		F.MESREF,
		F.VALFAT,
		F.RET_INSS,
		C.GRPATI,
		C.DESCLI,
		CASE 
			WHEN (LENGTH(C.CGCCPF)=14) THEN SUBSTR(C.CGCCPF,1,2)||'.'||SUBSTR(C.CGCCPF,3,3)||'.'||SUBSTR(C.CGCCPF,6,3)||'/'||SUBSTR(C.CGCCPF,9,4)||'-'||SUBSTR(C.CGCCPF,-2)
			WHEN (LENGTH(C.CGCCPF)=11) THEN SUBSTR(C.CGCCPF,1,3)||'.'||SUBSTR(C.CGCCPF,4,3)||'.'||SUBSTR(C.CGCCPF,7,3)||'-'||SUBSTR(C.CGCCPF,-2)
			ELSE C.CGCCPF END AS CGCCPF,
		C.ESFERA,
		T.DESATI
	FROM 
		CADFAT F,
		CADCLI C,
		GRPATI T
	WHERE 
		SYSDATE > F.DATVNC
		$filtro_atividade
		AND F.CODCLI=C.CODCLI
		AND C.GRPATI=T.GRPATI
		AND F.MESREF {PERIODO}
		AND NVL(F.VALPGT, 0) = 0
	ORDER BY 
		TRIM(DESATI),
		TRIM(DESCLI)
    ";

    //propriedade do relat�rio
	$cfg = array(
        'SOURCE' => $sql_faturas,
        'TITULO' => 'Relat�rio de D�bito por Atividade',
		'PERIODO' => array($FormFiltro->MES_REF_INI->GetValue(), $FormFiltro->MES_REF_FIM->GetValue()),
		'FILTROS' => $descricao_filtros,
        'COLUNAS' => array(
            'DESCLI' => array('NOME DO CLIENTE', 'L', 80),
            'CGCCPF' => array('CPF/CNPJ', 'L', 25),
            'CODFAT' => array('FATURA', 'R'),
			'VALFAT' => array('VALOR DA FATURA', 'R', null, 1),
            'MESREF' => array('M�S REF.', 'R'),
            'DATVNC' => array('VENCIMENTO', 'R'),
            'RET_INSS' => array('INSS', 'R', null, 1),
            'JUROS' => array('JUROS', 'R', null, 1),
            'VALPGT' => array('D�BITO ATUAL', 'R', null, 1)
        ),
        'AGRUPAMENTO' => array(
            'ID' => 'GRPATI',
            'VALUE' => 'DESATI',
            'NAME' => 'Atividade',
			'QUEBRA-PAGINA' => false
        ),
		'LINE-HEIGHT' => 4.5
    );

	$relatorio = new RelatorioFPDF($cfg);
    $relatorio->emitir($FormFiltro->RBFormatoRelatorio->GetValue());

// -------------------------
//End Custom Code

//Close FormFiltro_Button_Insert_OnClick @7-23FE9D24
    return $FormFiltro_Button_Insert_OnClick;
}
//End Close FormFiltro_Button_Insert_OnClick

//FormFiltro_OnValidate @4-875A8BB2
function FormFiltro_OnValidate(& $sender)
{
    $FormFiltro_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $FormFiltro; //Compatibility
//End FormFiltro_OnValidate

//Custom Code @26-2A29BDB7
// -------------------------
	try {
		$meses = RelatorioFPDF::mesesPeriodo($FormFiltro->MES_REF_INI->GetValue(), $FormFiltro->MES_REF_FIM->GetValue());
	} catch(Exception $e) {
		$FormFiltro->Errors->addError($e->getMessage());
	}
// -------------------------
//End Custom Code

//Close FormFiltro_OnValidate @4-B13236C7
    return $FormFiltro_OnValidate;
}
//End Close FormFiltro_OnValidate

//Page_BeforeShow @1-08C54599
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $RelDebiAtiv; //Compatibility
//End Page_BeforeShow

//Custom Code @25-2A29BDB7
// -------------------------
	include("controle_acesso.php");
	$perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(41);
	controleacesso($perfil,$permissao_requerida,"acessonegado.php");
// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow

?>
