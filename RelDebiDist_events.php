<?php
//BindEvents Method @1-C0876C5A
function BindEvents()
{
    global $NRRelEmisInc;
    global $CCSEvents;
    $NRRelEmisInc->TextBox1->CCSEvents["BeforeShow"] = "NRRelEmisInc_TextBox1_BeforeShow";
    $NRRelEmisInc->Button_Insert->CCSEvents["OnClick"] = "NRRelEmisInc_Button_Insert_OnClick";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//NRRelEmisInc_TextBox1_BeforeShow @5-90E29981
function NRRelEmisInc_TextBox1_BeforeShow(& $sender)
{
    $NRRelEmisInc_TextBox1_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRRelEmisInc; //Compatibility
//End NRRelEmisInc_TextBox1_BeforeShow

//Custom Code @12-2A29BDB7
// -------------------------
// Preenchimento come�a em 10/1995 e vai at� o mes/ano anterior ao me/ano atual.
	//$AnoAtual = (float)(substr(CCGetSession("DataSist"),-4));
	//$MesAtual = 12;
	//$MesAtual--;
	//$Inicio = 10;
	//$Valores = "";
	//for ($Ano=1995;$Ano<=$AnoAtual;$Ano++)
	//{
	//	if ($Ano == $AnoAtual)
	//	{
	//		$MesAtual = (float)(substr(CCGetSession("DataSist"),0,2));
	//	}
	//	for($Mes=$Inicio;$Mes<=$MesAtual;$Mes++)
	//	{
	//		if ($Mes < 10)
	//		{
	//			$MesAno = '0'.$Mes.'/'.$Ano;
	//		}
	//		else
	//		{
	//			$MesAno = $Mes.'/'.$Ano;
	//		}
	//		// Chegou ao final, ou seja, mes atual do ano atual
	//		if (($Ano == $AnoAtual) && ($Mes == $MesAtual))
	//		{
	// 			$Valores .= "array('$MesAno','$MesAno')";
	//		}
	//		else
	//		{
	// 			$Valores .= "array('$MesAno','$MesAno'),";
	//		}
	//	}
	//	$Inicio = 1;
	//}
	//$NRRelEmisInc->TextBox1->Values = array($Valores);
	 //$NRRelEmisInc->Status->Values = array(array($MesAno, $MesAno),array("2", "Normal"), array("3", "Low"));
	//$NRRelEmisInc->Status->Values = array(array("1", "High"),array("2", "Normal"), array("3", "Low"));

// -------------------------
//End Custom Code

//Close NRRelEmisInc_TextBox1_BeforeShow @5-B7BAD91C
    return $NRRelEmisInc_TextBox1_BeforeShow;
}
//End Close NRRelEmisInc_TextBox1_BeforeShow

//NRRelEmisInc_Button_Insert_OnClick @7-7DDD32A7
function NRRelEmisInc_Button_Insert_OnClick(& $sender)
{
    $NRRelEmisInc_Button_Insert_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRRelEmisInc; //Compatibility
//End NRRelEmisInc_Button_Insert_OnClick

//Custom Code @11-2A29BDB7
// -------------------------
    $Imprime = new relDebDisPDF();
	$distrito = $NRRelEmisInc->ListBox1->GetValue();
	if (empty($distrito))
	{
		$distrito = "Todos";
	}
	$Imprime->relatorio('Relat�rio de D�bitos por Distrito',$NRRelEmisInc->TextBox1->GetValue(),CCGetParam('opcao'),$distrito);//
// -------------------------
//End Custom Code

//Close NRRelEmisInc_Button_Insert_OnClick @7-944EB3E4
    return $NRRelEmisInc_Button_Insert_OnClick;
}
//End Close NRRelEmisInc_Button_Insert_OnClick

//Page_BeforeShow @1-EF3C37E3
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $RelDebiDist; //Compatibility
//End Page_BeforeShow

//Custom Code @20-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(41);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL  	$opcao = CCGetParam('opcao'); 
//DEL  	$NRRelEmisInc->ListBox1->Visible = false;
//DEL  	$NRRelEmisInc->GRPATI->Visible = false;
//DEL  	$NRRelEmisInc->SUBATI->Visible = false;
//DEL  	$NRRelEmisInc->CODUNI->Visible = false;
//DEL  	$NRRelEmisInc->GRPSER->Visible = false;
//DEL  	$NRRelEmisInc->SUBSER->Visible = false;
//DEL  	switch ($opcao) 
//DEL  	{
//DEL  		case 412; //                       Relat�rio de Emissao por Divisao
//DEL  			$NRRelEmisInc->CODUNI->Visible = true;
//DEL  		break;
//DEL  		case 413; //                       Relat�rio de Emissao por Inscri��o
//DEL  			$NRRelEmisInc->ListBox1->Visible = true;
//DEL  		break;
//DEL  		case 414; //                       Relat�rio de Emissao por Atividade
//DEL  			$NRRelEmisInc->GRPATI->Visible = true;
//DEL  		break;
//DEL  		case 415; //                       Relat�rio de Emissao por Sub-Atividade
//DEL  			$NRRelEmisInc->SUBATI->Visible = true;
//DEL  		break;
//DEL  		case 416; //                       Relat�rio de Emissao por Servi�o
//DEL  			$NRRelEmisInc->GRPSER->Visible = true;
//DEL  		break;
//DEL  		case 417; //                       Relat�rio de Emissao por Sub Servi�o
//DEL  			$NRRelEmisInc->SUBSER->Visible = true;
//DEL  		break;
//DEL  		case 422; //                       Relat�rio de Receitas por Divisao
//DEL  			$NRRelEmisInc->CODUNI->Visible = true;
//DEL  		break;
//DEL  		case 423; //                       Relat�rio de Receitas por Inscri��o
//DEL  			$NRRelEmisInc->ListBox1->Visible = true;
//DEL  		break;
//DEL  		case 424; //                       Relat�rio de Receitas por Atividade
//DEL  			$NRRelEmisInc->GRPATI->Visible = true;
//DEL  		break;
//DEL  		case 425; //                       Relat�rio de Receitas por Sub-Atividade
//DEL  			$NRRelEmisInc->SUBATI->Visible = true;
//DEL  		break;
//DEL  		case 426; //                       Relat�rio de Receitas por Servi�o
//DEL  			$NRRelEmisInc->GRPSER->Visible = true;
//DEL  		break;
//DEL  		case 427; //                       Relat�rio de Receitas por Sub Servi�o
//DEL  			$NRRelEmisInc->SUBSER->Visible = true;
//DEL  		break;
//DEL  	}
//DEL  // -------------------------



?>
