<?php
//BindEvents Method @1-739D063B
function BindEvents()
{
    global $FormFiltro;
    global $CCSEvents;
    $FormFiltro->Button_DoSearch->CCSEvents["OnClick"] = "FormFiltro_Button_DoSearch_OnClick";
    $FormFiltro->CCSEvents["OnValidate"] = "FormFiltro_OnValidate";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//FormFiltro_Button_DoSearch_OnClick @7-C1BC3466
function FormFiltro_Button_DoSearch_OnClick(& $sender)
{
    $FormFiltro_Button_DoSearch_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $FormFiltro; //Compatibility
//End FormFiltro_Button_DoSearch_OnClick

//Custom Code @9-2A29BDB7
// -------------------------
	$bd = new clsDBfaturar();
	$descricao_filtros = array();
	//obtem os m�ses do per�odo especificado
	$meses = mesesPeriodo($FormFiltro->MES_REF_INI->GetValue(), $FormFiltro->MES_REF_FIM->GetValue());
	$meses_ref = '\''.implode('\',\'', $meses).'\'';
	//determina o cliente (� obrigat�rio e j� foi validado no evento OnValidate)
	$cliente = (int)$FormFiltro->CLIENTE->GetValue();
	$descricao_filtros[] = 'Cliente: '.CCDLookUp('DESCLI','CADCLI',"CODCLI = $cliente", $bd);
	
    //instru��o para consultar as faturas da ativade para cliente no per�odo
    $sql_faturas = "
    SELECT 
        F.CODCLI,
        to_char(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
        CASE
            WHEN (SYSDATE-F.DATVNC>0) THEN ((F.VALFAT-F.RET_INSS)*(0.01/30)*FLOOR(SYSDATE-F.DATVNC)) 
            ELSE 0 END AS JUROS,
        F.CODFAT,
        F.VALFAT,
        F.RET_INSS,
        F.MESREF,
        ((F.VALFAT-F.RET_INSS)*(1+(0.01/30)*FLOOR(SYSDATE-F.DATVNC))) AS VALPGT,
        C.GRPATI,
        C.DESCLI,
        CASE 
            WHEN (LENGTH(C.CGCCPF)=14) THEN SUBSTR(C.CGCCPF,1,2)||'.'||SUBSTR(C.CGCCPF,3,3)||'.'||SUBSTR(C.CGCCPF,6,3)||'/'||SUBSTR(C.CGCCPF,9,4)||'-'||SUBSTR(C.CGCCPF,-2)
            WHEN (LENGTH(C.CGCCPF)=11) THEN SUBSTR(C.CGCCPF,1,3)||'.'||SUBSTR(C.CGCCPF,4,3)||'.'||SUBSTR(C.CGCCPF,7,3)||'-'||SUBSTR(C.CGCCPF,-2)
            ELSE C.CGCCPF END AS CGCCPF,
        C.ESFERA,
        A.DESATI
    FROM 
        CADFAT F,
        CADCLI C,
        GRPATI A
    WHERE 
        SYSDATE > F.DATVNC AND
        F.CODCLI = C.CODCLI  AND 
        C.GRPATI = A.GRPATI AND
		C.CODCLI = $cliente AND
        F.MESREF IN ($meses_ref) AND
		NVL(F.VALPGT, 0) = 0
    ORDER BY 
        DESATI,
        DESCLI,
        CODFAT
    ";

    //propriedade do relat�rio
	$cfg = array(
        'SOURCE' => $sql_faturas,
        'TITULO' => 'Relat�rio de D�bito por Cliente por Atividade',
        'PERIODO' => 'M�s/Ano de refer�ncia: '.((count($meses)>1)?$meses[0].' a '.$meses[count($meses)-1]:$meses[0]),
		'FILTROS' => $descricao_filtros,
        'COLUNAS' => array(
            'DESCLI' => array('NOME DO CLIENTE', 'L', 80),
            'CGCCPF' => array('CPF/CNPJ', 'L', 25),
            'CODFAT' => array('FATURA', 'R'),
            'MESREF' => array('M�S REF.', 'R'),
            'DATVNC' => array('VENCIMENTO', 'R'),
            'VALFAT' => array('VALOR DA FATURA', 'R', null, 1),
            'RET_INSS' => array('INSS', 'R', null, 1),
            'JUROS' => array('JUROS', 'R', null, 1),
            'VALPGT' => array('D�BITO ATUAL', 'R', null, 1)
        ),
        'AGRUPAMENTO' => array(
            'ID' => 'GRPATI',
            'VALUE' => 'DESATI',
            'NAME' => 'Atividade'
        )
    );

	$relatorio = new RelatorioFPDF($cfg);
    $relatorio->emitir('PDF');
	
// -------------------------
//End Custom Code

//Close FormFiltro_Button_DoSearch_OnClick @7-9E60E70E
    return $FormFiltro_Button_DoSearch_OnClick;
}
//End Close FormFiltro_Button_DoSearch_OnClick

//FormFiltro_OnValidate @4-875A8BB2
function FormFiltro_OnValidate(& $sender)
{
    $FormFiltro_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $FormFiltro; //Compatibility
//End FormFiltro_OnValidate

//Custom Code @26-2A29BDB7
// -------------------------
	try {
		$meses = mesesPeriodo($FormFiltro->MES_REF_INI->GetValue(), $FormFiltro->MES_REF_FIM->GetValue());
	} catch(Exception $e) {
		$FormFiltro->Errors->addError($e->getMessage());
	}

	if(!$FormFiltro->CLIENTE->GetValue())
		$FormFiltro->Errors->addError('Selecione um cliente');
// -------------------------
//End Custom Code

//Close FormFiltro_OnValidate @4-B13236C7
    return $FormFiltro_OnValidate;
}
//End Close FormFiltro_OnValidate

//Page_BeforeShow @1-47A04304
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $RelDebiGeraClienteAtiv; //Compatibility
//End Page_BeforeShow

//Custom Code @20-2A29BDB7
// -------------------------
    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(41);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");
// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow

?>