<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="4" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="NRRelDebGera" actionPage="RelDebiGera" errorSummator="Error" wizardFormMethod="post" wizardOrientation="Vertical">
			<Components>
				<TextBox id="12" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="Inicio" caption="Mês inicial" required="True" format="mm/yyyy">
					<Components/>
					<Events>
<Event name="OnKeyPress" type="Client">
<Actions>
<Action actionName="Validate Entry" actionCategory="Validation" id="21" required="True" minimumLength="7" maximumLength="7" inputMask="00/0000" regularExpression="^\d{2}\/\d{4}$" errorMessage="Data de início inválida. Deve ser digitado mês e ano. Ex.: 01/2015."/>
</Actions>
</Event>
</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="14" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="Fim" caption="Mês final" required="True" format="mm/yyyy">
					<Components/>
					<Events>
<Event name="OnKeyPress" type="Client">
<Actions>
<Action actionName="Validate Entry" actionCategory="Validation" id="22" required="True" minimumLength="7" maximumLength="7" inputMask="00/0000" regularExpression="^\d{2}\/\d{4}$" errorMessage="Data final inválida. Deve ser digitado mês e ano. Ex.: 01/2015."/>
</Actions>
</Event>
</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<Button id="7" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events>
						<Event name="OnClick" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="9"/>
							</Actions>
						</Event>
						<Event name="OnClick" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="16"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="PHPTemplates" name="RelDebiGeraClientePeri_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Code" language="PHPTemplates" name="RelDebiGeraClientePeri.php" forShow="True" url="RelDebiGeraClientePeri.php" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="17" groupID="1"/>
		<Group id="18" groupID="2"/>
		<Group id="19" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="20"/>
			</Actions>
		</Event>
	</Events>
</Page>
