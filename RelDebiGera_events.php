<?php
//BindEvents Method @1-E73D7E4D
function BindEvents()
{
    global $NRRelDebGera;
    global $CCSEvents;
    $NRRelDebGera->Button_DoSearch->CCSEvents["OnClick"] = "NRRelDebGera_Button_DoSearch_OnClick";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//NRRelDebGera_Button_DoSearch_OnClick @7-7C2CDA47
function NRRelDebGera_Button_DoSearch_OnClick(& $sender)
{
    $NRRelDebGera_Button_DoSearch_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRRelDebGera; //Compatibility
//End NRRelDebGera_Button_DoSearch_OnClick

//Custom Code @9-2A29BDB7
// -------------------------
    // Write your own code here.
    $Imprime = new relDebGeraPDF();
	$Opcao = $NRRelDebGera->RBAtivo->GetValue();
	$Imprime->relatorio('Relat�rio Geral de D�bitos',$Opcao);//
// -------------------------
//End Custom Code

//Close NRRelDebGera_Button_DoSearch_OnClick @7-BA67D9B3
    return $NRRelDebGera_Button_DoSearch_OnClick;
}
//End Close NRRelDebGera_Button_DoSearch_OnClick

//Page_BeforeShow @1-ACF1A95E
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $RelDebiGera; //Compatibility
//End Page_BeforeShow

//Custom Code @13-2A29BDB7
// -------------------------
    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(41);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");
// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
