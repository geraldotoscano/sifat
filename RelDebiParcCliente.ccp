<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="4" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="NRRelDebGera" actionPage="RelDebiGera" errorSummator="Error" wizardFormMethod="post" wizardOrientation="Vertical" pasteAsReplace="pasteAsReplace">
			<Components>
				<ListBox id="10" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="CODCLI" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="CADCLI" boundColumn="CODCLI" textColumn="descricao" caption="Cliente" required="True">
					<Components/>
					<Events>
						<Event name="BeforeBuildSelect" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="11" eventType="Server"/>
							</Actions>
						</Event>
					</Events>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="16" tableName="CADCLI" posLeft="47" posTop="3" posWidth="115" posHeight="619"/>
					</JoinTables>
					<JoinLinks/>
					<Fields>
						<Field id="17" fieldName="cadcli.descli||' ('||cadcli.codcli||')'" isExpression="True" alias="descricao"/>
						<Field id="18" tableName="CADCLI" fieldName="CODCLI"/>
						<Field id="19" tableName="CADCLI" fieldName="DESCLI"/>
						<Field id="20" tableName="CADCLI" fieldName="DESFAN"/>
						<Field id="21" tableName="CADCLI" fieldName="CODUNI"/>
						<Field id="22" tableName="CADCLI" fieldName="CODDIS"/>
						<Field id="23" tableName="CADCLI" fieldName="CODSET"/>
						<Field id="24" tableName="CADCLI" fieldName="GRPATI"/>
						<Field id="25" tableName="CADCLI" fieldName="SUBATI"/>
						<Field id="26" tableName="CADCLI" fieldName="CODTIP"/>
						<Field id="27" tableName="CADCLI" fieldName="CGCCPF"/>
						<Field id="28" tableName="CADCLI" fieldName="LOGRAD"/>
						<Field id="29" tableName="CADCLI" fieldName="BAIRRO"/>
						<Field id="30" tableName="CADCLI" fieldName="CIDADE"/>
						<Field id="31" tableName="CADCLI" fieldName="ESTADO"/>
						<Field id="32" tableName="CADCLI" fieldName="CODPOS"/>
						<Field id="33" tableName="CADCLI" fieldName="TELEFO"/>
						<Field id="34" tableName="CADCLI" fieldName="TELFAX"/>
						<Field id="35" tableName="CADCLI" fieldName="CONTAT"/>
						<Field id="36" tableName="CADCLI" fieldName="LOGCOB"/>
						<Field id="37" tableName="CADCLI" fieldName="BAICOB"/>
						<Field id="38" tableName="CADCLI" fieldName="CIDCOB"/>
						<Field id="39" tableName="CADCLI" fieldName="ESTCOB"/>
						<Field id="40" tableName="CADCLI" fieldName="POSCOB"/>
						<Field id="41" tableName="CADCLI" fieldName="TELCOB"/>
						<Field id="42" tableName="CADCLI" fieldName="CODSIT"/>
						<Field id="43" tableName="CADCLI" fieldName="CODINC"/>
						<Field id="44" tableName="CADCLI" fieldName="CODALT"/>
						<Field id="45" tableName="CADCLI" fieldName="DATINC"/>
						<Field id="46" tableName="CADCLI" fieldName="DATALT"/>
						<Field id="47" tableName="CADCLI" fieldName="PGTOELET"/>
						<Field id="48" tableName="CADCLI" fieldName="ESFERA"/>
					</Fields>
					<Attributes/>
					<Features/>
				</ListBox>
				<Button id="7" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events>
						<Event name="OnClick" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="9"/>
							</Actions>
						</Event>
						<Event name="OnLoad" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="49"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="PHPTemplates" name="RelDebiParcCliente_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Code" language="PHPTemplates" name="RelDebiParcCliente.php" forShow="True" url="RelDebiParcCliente.php" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="12" groupID="1"/>
		<Group id="13" groupID="2"/>
		<Group id="14" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="15"/>
			</Actions>
		</Event>
	</Events>
</Page>
