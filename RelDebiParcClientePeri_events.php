<?php
//BindEvents Method @1-E658E7CF
function BindEvents()
{
    global $NRRelDebGera;
    global $CCSEvents;
    $NRRelDebGera->CODCLI->ds->CCSEvents["BeforeBuildSelect"] = "NRRelDebGera_CODCLI_ds_BeforeBuildSelect";
    $NRRelDebGera->Button_DoSearch->CCSEvents["OnClick"] = "NRRelDebGera_Button_DoSearch_OnClick";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//NRRelDebGera_CODCLI_ds_BeforeBuildSelect @10-9751DCB1
function NRRelDebGera_CODCLI_ds_BeforeBuildSelect(& $sender)
{
    $NRRelDebGera_CODCLI_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRRelDebGera; //Compatibility
//End NRRelDebGera_CODCLI_ds_BeforeBuildSelect

//Custom Code @11-2A29BDB7
// -------------------------
    // Write your own code here.
  If ($NRRelDebGera->CODCLI->DataSource->Order == "") { 
     $NRRelDebGera->CODCLI->DataSource->Order = "DESCLI";
  }
// -------------------------
//End Custom Code

//Close NRRelDebGera_CODCLI_ds_BeforeBuildSelect @10-32D95611
    return $NRRelDebGera_CODCLI_ds_BeforeBuildSelect;
}
//End Close NRRelDebGera_CODCLI_ds_BeforeBuildSelect

//DEL  // -------------------------
//DEL      // Write your own code here.
//DEL    if ($NRRelDebGera-> DataSource->Where <> "") {
//DEL      $Tasks->DataSource->Where .= " AND ";
//DEL    }
//DEL  
//DEL    $Tasks->DataSource->Where .= "task_name like 'Improve%'";
//DEL  // -------------------------

//NRRelDebGera_Button_DoSearch_OnClick @7-7C2CDA47
function NRRelDebGera_Button_DoSearch_OnClick(& $sender)
{
    $NRRelDebGera_Button_DoSearch_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRRelDebGera; //Compatibility
//End NRRelDebGera_Button_DoSearch_OnClick

//Custom Code @9-2A29BDB7
// -------------------------
    // Write your own code here.
    $Imprime = new relDebParcClientePeriPDF();
	$Opcao   = $NRRelDebGera->CODCLI->GetValue();
	$Inicio  = $NRRelDebGera->Inicio->GetValue();
	$Fim     = $NRRelDebGera->Fim->GetValue();
	$Imprime->relatorio('Totaliza��o de D�bitos do Cliente ('.$Opcao.') no Per�odo',$Opcao,$Inicio,$Fim);//
// -------------------------
//End Custom Code

//Close NRRelDebGera_Button_DoSearch_OnClick @7-BA67D9B3
    return $NRRelDebGera_Button_DoSearch_OnClick;
}
//End Close NRRelDebGera_Button_DoSearch_OnClick

//Page_BeforeShow @1-30278CD6
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $RelDebiParcClientePeri; //Compatibility
//End Page_BeforeShow

//Custom Code @20-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(41);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
