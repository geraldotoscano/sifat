<?php
//BindEvents Method @1-739D063B
function BindEvents()
{
    global $FormFiltro;
    global $CCSEvents;
    $FormFiltro->Button_DoSearch->CCSEvents["OnClick"] = "FormFiltro_Button_DoSearch_OnClick";
    $FormFiltro->CCSEvents["OnValidate"] = "FormFiltro_OnValidate";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//FormFiltro_Button_DoSearch_OnClick @7-C1BC3466
function FormFiltro_Button_DoSearch_OnClick(& $sender)
{
    $FormFiltro_Button_DoSearch_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $FormFiltro; //Compatibility
//End FormFiltro_Button_DoSearch_OnClick

//Custom Code @9-2A29BDB7
// -------------------------
  	$bd = new clsDBfaturar();
  	$descricao_filtros = array();
  	//obtem os m�ses do per�odo especificado
  	$meses = mesesPeriodo($FormFiltro->MES_REF_INI->GetValue(), $FormFiltro->MES_REF_FIM->GetValue());
  	$meses_ref = implode('\',\'', $meses);
  	//determina o primeiro e o �ltimo dia do per�odo
  	//$inicio_periodo = '01/'.$meses[0];
  	//$fim_periodo = DateTime::createFromFormat('d/m/Y', '01/'.$meses[count($meses)-1])->modify('+1 month -1 day')->format('d/m/Y');
  
  	$subservico = (int)$FormFiltro->SUBSERVICO->GetValue();
  	//verifica os filtro aplicados
  	if($subservico) {
  		$descricao_filtros[] = "Subservi�o: $subservico - ".CCDLookUp('DESSUB','SUBSER',"SUBSER = $subservico", $bd);
  		$subservico = "AND S.SUBSER = $subservico";
  	} else {
  		$descricao_filtros[] = "Todos os Subservi�os";
  		$subservico = '';
  	}
  	
	//Relat�rio anal�tico
	if($FormFiltro->RBConteudo->GetValue() == 'ANALITICO') {
		//instru��o para consultar todas as faturas do(s) servi�o(s) emitidas no per�odo, mesmo que a configura��o do servi�o tenha mudado
		$sql_faturas = "
			SELECT
				MF.CODCLI,
				TRIM(C.DESCLI) AS DESCLI,
				DECODE(C.CODSIT, 'A', 'ATIVO', 'INATIVO') AS CODSIT,
				MF.VALSER,
				MF.IDSERCLI,
				S.SUBSER AS SUBSER,
				S.SUBSER||' - '||S.DESSUB AS DESSUB,
				MF.CODFAT,
				MF.MESREF
			FROM
				MOVFAT MF
				INNER JOIN SUBSER S ON (MF.SUBSER = S.SUBSER)
				INNER JOIN CADCLI C ON (MF.CODCLI = C.CODCLI)
				/*INNER JOIN TABPBH T ON (MF.MESREF = T.MESREF)*/
			WHERE
				MF.MESREF IN ('$meses_ref')
				$subservico
			ORDER BY
				S.DESSUB, TRIM(C.DESCLI), MF.MESREF
			";

		//propriedades do relat�rio
		$cfg = array( 
			'SOURCE' => $sql_faturas,
			'TITULO' => 'Relat�rio de Emiss�o por Subservi�o',
			'PERIODO' => 'M�s/Ano de refer�ncia: '.(count($meses)==1 ? $meses[0] : $meses[0].' a '.$meses[count($meses)-1]),
			'FILTROS' => $descricao_filtros,
			'COLUNAS' => array(
				'CODCLI' => array('CLIENTE'),
				'DESCLI' => array('DESCRI��O', 'L', 80, 2),
				'CODSIT' => array('SITUA.'),	

				'IDSERCLI' => array('C�D SERV.', 'R'),
				'VALSER' => array('VALOR SERV.', 'R', null, 1),
				'CODFAT' => array('FATURA', 'R'),
				'MESREF' => array('M�S. REF', 'R')
			),
			'AGRUPAMENTO' => array(
				'ID' => 'SUBSER',
				'VALUE' => 'DESSUB',
				'NAME' => 'Subservi�o'
			),
			'LINE-HEIGHT' => 4.5
		);
	}
	//Relat�rio sint�tico (somente valores totais faturados para cada servi�o)
	else {
		//instru��o para consultar valores totais faturados
		$sql_faturas = "
			SELECT
				S.SUBSER,
				S.DESSUB,
				SUM(MF.VALSER) AS VALSER
			FROM
				MOVFAT MF
				INNER JOIN SUBSER S ON (MF.SUBSER = S.SUBSER)
			WHERE
				MF.MESREF IN ('$meses_ref')
				$subservico
			GROUP BY
				S.SUBSER,
				S.DESSUB
			ORDER BY
				S.DESSUB
			";

		//propriedades do relat�rio
		$cfg = array( 
			'SOURCE' => $sql_faturas,
			'TITULO' => 'Relat�rio de Emiss�o por Subservi�o (Sint�tico)',
			'PERIODO' => 'M�s/Ano de refer�ncia: '.(count($meses)==1 ? $meses[0] : $meses[0].' a '.$meses[count($meses)-1]),
			'FILTROS' => $descricao_filtros,
			'COLUNAS' => array(
				'SUBSER' => array('C�DIGO'),
				'DESSUB' => array('SUBSERVI�O', 'L', 90, 2),
				'VALSER' => array('VALOR TOTAL FATURADO', 'R', null, 1)
			)
		);
	}

	$relatorio = new RelatorioFPDF($cfg);
	$relatorio->emitir($FormFiltro->RBFormatoRelatorio->GetValue());
	
// -------------------------
//End Custom Code

//Close FormFiltro_Button_DoSearch_OnClick @7-9E60E70E
    return $FormFiltro_Button_DoSearch_OnClick;
}
//End Close FormFiltro_Button_DoSearch_OnClick

//FormFiltro_OnValidate @4-875A8BB2
function FormFiltro_OnValidate(& $sender)
{
    $FormFiltro_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $FormFiltro; //Compatibility
//End FormFiltro_OnValidate

//Custom Code @26-2A29BDB7
// -------------------------
	try {
		$meses = mesesPeriodo($FormFiltro->MES_REF_INI->GetValue(), $FormFiltro->MES_REF_FIM->GetValue());
	} catch(Exception $e) {
		$FormFiltro->Errors->addError($e->getMessage());
	}
// -------------------------
//End Custom Code

//Close FormFiltro_OnValidate @4-B13236C7
    return $FormFiltro_OnValidate;
}
//End Close FormFiltro_OnValidate

//Page_BeforeShow @1-598002D6
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $RelEmisSubS; //Compatibility
//End Page_BeforeShow

//Custom Code @20-2A29BDB7
// -------------------------
    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(37);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");
// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow

?>