<?php
	//error_reporting (E_ALL);
	//define("RelativePath", ".");
	//define('FPDF_FONTPATH','/pdf/font/');
	require('pdf/fpdf.php');
	//include('pdf/fpdi.php');
	//require_once("pdf/fpdi_pdf_parser.php");
	//require_once("pdf/fpdf_tpl.php");
	//include(RelativePath . "/Common.php");e
	//include(RelativePath . "/Template.php");
	//include(RelativePath . "/Sorter.php");
	//include(RelativePath . "/Navigator.php");
	//include_once(RelativePath . "/cabec.php");
class relFatCancPDF extends fpdf {	
	var $titulo;
	var $mesRef;
	var $posCliente;
	var $posFatura;
	var $posEmissao;
	var $posValFat;
	var $posVenc;
	var $posMotivo;
	var $posDatcan;
	var $porOperador;
	var $totalGeral;
	var $StringTam;
	var $relat;
	var $mudouDistrito;
	var $distrito;
	var $posMesAno;
	var $posINSS;
	var $posPGT;
	var $posAcres;
	var $posRec;
	var $posISSQN;
	var $posJuros;
	var $posValRec;
	var $incial;
	var $final;
	var $hoje;
	//$pdf= new fpdi();
	function relatorio($titu,$mesAno,$Inicio,$Fim,$PorDataCanc,$codtipcanc)
	{
		if (is_null($mesAno))
		{
			$mesAno='F.CODCLI';
		}
		if (is_null($Inicio))
		{
			$Inicio="'01/09/1994'";
		}
		$Tabela   = new clsDBfaturar();
		if (is_null($Fim))
		{
			$Tabela->query("SELECT TO_CHAR(SYSDATE+1,'DD/MM/YYYY') AS THE_END FROM DUAL");
			$Tabela->next_record();
			$Fim = $Tabela->f("THE_END");
			$Fim = "'$Fim'";
			//$Fim='SYSDATE+1';
		}

		$Tabela->query("SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') AS THE_END FROM DUAL");
		$Tabela->next_record();
		$hoje = $Tabela->f("THE_END");
		$hoje = "'$hoje'";

		$this->incial = $Inicio;
		$this->final = $Fim;
		$this->mesRef = $mesAno;
		$this->SetTitle('SUPERINTEND�NCIA DE LIMPEZA URBANA');
		$this->titulo = $titu;
		$this->AliasNbPages();
		
		$TOTAIS   = new clsDBfaturar();


		if($PorDataCanc!='1')
		  {
           $condicao="				F.CODCLI=$this->mesRef                              AND
								F.DATEMI BETWEEN TO_DATE($Inicio,'DD/MM/YYYY') AND TO_DATE($Fim,'DD/MM/YYYY') AND
								F.CODCLI=C.CODCLI";
          }
         else
		  {
		   $condicao="				F.CODCLI=$this->mesRef                              AND
								F.DATCAN BETWEEN TO_DATE($Inicio,'DD/MM/YYYY') AND TO_DATE($Fim,'DD/MM/YYYY') AND
								F.CODCLI=C.CODCLI";
		  }

        if($codtipcanc!="")
		  {
           $condicao=$condicao." AND CODTIPCANC=".$codtipcanc;

		  }

		if($PorDataCanc!='1')
		  {
			$TOTAIS->query("SELECT
								SUM(NVL(VALFAT,0)) AS TOTGER
							FROM
								CADFAT_CANC F,
								CADCLI C
							WHERE  ".$condicao);
			$TOTAIS->next_record();
			$this->totalGeral = (float)((str_replace(",", ".", $TOTAIS->f("TOTGER"))));
			$this->totalGeral = number_format($this->totalGeral, 2,',','.');

			$TOTAIS->query("SELECT
								SUM(NVL(ISSQN,0)) AS TOTISSQN
							FROM
								CADFAT_CANC F,
								CADCLI C
							WHERE ".$condicao);

			$TOTAIS->next_record();
			$TOTISSQN    = (str_replace(",", ".",$TOTAIS->f("TOTISSQN")));
			$TOTISSQN    = number_format($TOTISSQN, 2,',','.');

			$TOTAIS->query("SELECT
								SUM(NVL(RET_INSS,0)) AS TOTRET_INSS
							FROM
								CADFAT_CANC F,
								CADCLI C
							WHERE ".$condicao);
			$TOTAIS->next_record();
			$TOTRET_INSS = (str_replace(",", ".",$TOTAIS->f("TOTRET_INSS")));
			$TOTRET_INSS = number_format($TOTRET_INSS, 2,',','.');
			
			$TOTAIS->query("SELECT
								SUM(DECODE((TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC),ABS((TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC)),ROUND(NVL(F.VALMUL,0)*(TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC),2),0)) AS TOTJUROS
							FROM
								CADFAT_CANC F,
								CADCLI C
							WHERE ".$condicao);
			$TOTAIS->next_record();
			$TOTJUROS    = (str_replace(",", ".",$TOTAIS->f("TOTJUROS")));
			$TOTJUROS    = number_format($TOTJUROS, 2,',','.');

			$TOTAIS->query("SELECT
								SUM(NVL(F.VALFAT,0) + (DECODE((TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC),ABS((TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC)),ROUND(NVL(F.VALMUL,0)*(TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC),2),0))-NVL(F.RET_INSS,0)-NVL(F.ISSQN,0)) AS TOTVALPGT
							FROM
								CADFAT_CANC F,
								CADCLI C
							WHERE ".$condicao);
			$TOTAIS->next_record();
			$TOTVALPGT   = (str_replace(",", ".",$TOTAIS->f("TOTVALPGT")));
			$TOTVALPGT   = number_format($TOTVALPGT, 2,',','.');
			$Tabela->query("SELECT 
								F.CODCLI,
								F.MESREF AS DATEMI,
								F.DESCCANC,
								F.DATCAN,
								to_char(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
								to_char(F.DATPGT,'dd/mm/yyyy') AS DATPGT,
								F.CODFAT,
								NVL(F.VALFAT,0) AS VALFAT,
								NVL(F.ISSQN,0) AS ISSQN,
								NVL(F.RET_INSS,0) AS RET_INSS,
								DECODE((TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC),ABS((TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC)),ROUND(NVL(F.VALMUL,0)*(TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC),2),0) AS JUROS,
								(NVL(F.VALFAT,0) + (DECODE((TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC),ABS((TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC)),ROUND(NVL(F.VALMUL,0)*(TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC),2),0)) - NVL(F.ISSQN,0) - NVL(F.RET_INSS,0)) AS DEBI_ATU,
								NVL(F.VALPGT,0) AS VALPGT,
								U.CODOPE AS CODOPE,
								C.DESCLI,
								C.CGCCPF,
								C.ESFERA
							FROM 
								CADFAT_CANC F LEFT JOIN TABOPE U ON F.IDCAN = U.ID,
								CADCLI C 
							WHERE ".$condicao."
							ORDER BY 
								MESREF,CODCLI"
							);
                
          }
         else
		  {
			$TOTAIS->query("SELECT
								SUM(NVL(VALFAT,0)) AS TOTGER
							FROM
								CADFAT_CANC F,
								CADCLI C
							WHERE ".$condicao);

			$TOTAIS->next_record();
			$this->totalGeral = (float)((str_replace(",", ".", $TOTAIS->f("TOTGER"))));
			$this->totalGeral = number_format($this->totalGeral, 2,',','.');

			$TOTAIS->query("SELECT
								SUM(NVL(ISSQN,0)) AS TOTISSQN
							FROM
								CADFAT_CANC F,
								CADCLI C
							WHERE ".$condicao);

			$TOTAIS->next_record();
			$TOTISSQN    = (str_replace(",", ".",$TOTAIS->f("TOTISSQN")));
			$TOTISSQN    = number_format($TOTISSQN, 2,',','.');

			$TOTAIS->query("SELECT
								SUM(NVL(RET_INSS,0)) AS TOTRET_INSS
							FROM
								CADFAT_CANC F,
								CADCLI C
							WHERE ".$condicao);

			$TOTAIS->next_record();
			$TOTRET_INSS = (str_replace(",", ".",$TOTAIS->f("TOTRET_INSS")));
			$TOTRET_INSS = number_format($TOTRET_INSS, 2,',','.');
			
			$TOTAIS->query("SELECT
								SUM(DECODE((TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC),ABS((TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC)),ROUND(NVL(F.VALMUL,0)*(TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC),2),0)) AS TOTJUROS
							FROM
								CADFAT_CANC F,
								CADCLI C
							WHERE ".$condicao);

			$TOTAIS->next_record();
			$TOTJUROS    = (str_replace(",", ".",$TOTAIS->f("TOTJUROS")));
			$TOTJUROS    = number_format($TOTJUROS, 2,',','.');

			$TOTAIS->query("SELECT
								SUM(NVL(F.VALFAT,0) + (DECODE((TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC),ABS((TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC)),ROUND(NVL(F.VALMUL,0)*(TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC),2),0))-NVL(F.RET_INSS,0)-NVL(F.ISSQN,0)) AS TOTVALPGT
							FROM
								CADFAT_CANC F,
								CADCLI C
							WHERE ".$condicao);

			$TOTAIS->next_record();
			$TOTVALPGT   = (str_replace(",", ".",$TOTAIS->f("TOTVALPGT")));
			$TOTVALPGT   = number_format($TOTVALPGT, 2,',','.');
			$Tabela->query("SELECT 
								F.CODCLI,
								F.MESREF AS DATEMI,
								F.DESCCANC,
								F.DATCAN,
								to_char(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
								to_char(F.DATPGT,'dd/mm/yyyy') AS DATPGT,
								F.CODFAT,
								NVL(F.VALFAT,0) AS VALFAT,
								NVL(F.ISSQN,0) AS ISSQN,
								NVL(F.RET_INSS,0) AS RET_INSS,
								DECODE((TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC),ABS((TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC)),ROUND(NVL(F.VALMUL,0)*(TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC),2),0) AS JUROS,
								(NVL(F.VALFAT,0) + (DECODE((TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC),ABS((TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC)),ROUND(NVL(F.VALMUL,0)*(TO_DATE($hoje,'DD/MM/YYYY')-F.DATVNC),2),0)) - NVL(F.ISSQN,0) - NVL(F.RET_INSS,0)) AS DEBI_ATU,
								NVL(F.VALPGT,0) AS VALPGT,
								U.CODOPE AS CODOPE,
								C.DESCLI,
								C.CGCCPF,
								C.ESFERA
							FROM 
								CADFAT_CANC F LEFT JOIN TABOPE U ON F.IDCAN = U.ID,
								CADCLI C 
							WHERE ".$condicao."
							ORDER BY 
								MESREF,CODCLI"
							);
                



		  }
							//$articles = array();
				$Linha = 50;
				$this->SetY($Linha);
				$this->addPage('L');
				$this->totalGeral=0;
				$Cliente = "";
				while ($Tabela->next_record())
				{
						//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
					if ($this->GetY() >= ($this->fw-12))
					{
						$Linha = 50;
						$this->addPage('L'); 
					}
					$this->SetY($Linha);
					if ($Cliente != $Tabela->f("DESCLI"))
					{
					   $Cliente ='('.$Tabela->f("CODCLI").')'.$Tabela->f("DESCLI");
					   $tanString = $this->GetStringWidth('NOME DO CLIENTE')+55;

					   if( $this->GetStringWidth($Cliente)>$tanString )
					     {
                          $this->Text(3,$Linha,substr($Cliente,0,47));
						 }
                        else
						 {
						  $this->Text(3,$Linha,$Cliente);
                         }

					   
					   
		
					   $Inscricao = $Tabela->f("CGCCPF");
					   $this->Text($this->posCPF,$Linha,$Inscricao);
					}
		
					$Emissao = $Tabela->f("DATEMI");
					$this->Text($this->posEmissao,$Linha,$Emissao);
		
					$Fatura = $Tabela->f("CODFAT");
					$this->Text($this->posFatura,$Linha,$Fatura);
				
					$ValFat = $Tabela->f("VALFAT");
					$ValFat = (float)((str_replace(",", ".", $ValFat)));
					$this->totalGeral=$ValFat+$this->totalGeral;
					$ValFat = number_format($ValFat, 2,',','.');
					//           (       f i n a l                   ) -       t  a  m  n  h  o    
					$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
					$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($ValFat)),$Linha,$ValFat);
					
					
					$Venci = $Tabela->f("DATVNC");
					$this->Text($this->posVenc,$Linha,$Venci);
					
//					$PAGTO = $Tabela->f("DATPGT");
//					$this->Text($this->posPGT,$Linha,$PAGTO);

					$ISSQN = $Tabela->f("ISSQN");
					$ISSQN = (float)((str_replace(",", ".", $ISSQN)));
					$ISSQN = number_format($ISSQN, 2,',','.');
					if ($Tabela->f("VALPGT") == 0)
					{
						$ISSQN.=" N";
					}
					else
					{
						if (round($Tabela->f("VALPGT"),2) == round(($Tabela->f("VALFAT") - $Tabela->f("INSS") - ($Tabela->f("INSS")=='N')?($Tabela->f("INSSQN")):(0)),2))
						{
							$ISSQN.=" R";
						}
						else
						{
							if (round($Tabela->f("VALPGT"),2) == round(($Tabela->f("VALFAT") - $Tabela->f("INSS")),2))
							{
								$ISSQN.=" N";
							}
							else
							{
								if (round($Tabela->f("VALPGT"),2) == round(($Tabela->f("VALFAT") - ($Tabela->f("INSS")=='N')?($Tabela->f("INSSQN")):(0)),2))
								{
									$ISSQN.=" R";
								}
								else
								{
									if (round($Tabela->f("VALPGT"),2) == round($Tabela->f("VALFAT"),2))
									{
										$ISSQN.=" N";
									}
									else
									{
										if (round($Tabela->f("VALPGT"),2) > round($Tabela->f("VALFAT"),2))
										{
											$ISSQN.=" +";
										}
										else
										{
											$ISSQN.=" -";
										}
									}
								}
							}
						}
					}
					//$this->Text($this->posISSQN,$Linha,$ISSQN);
					//$this->StringTam = $this->GetStringWidth('  ISSQN');
					//$this->Text((($this->posISSQN + $this->StringTam) - $this->GetStringWidth($ISSQN)),$Linha,$ISSQN);
					
					
					$INSS = $Tabela->f("RET_INSS");
					$INSS = (float)((str_replace(",", ".", $INSS)));
					$INSS = number_format($INSS, 2,',','.');
					//$this->Text($this->posINSS,$Linha,$INSS);
					$this->StringTam = $this->GetStringWidth('   INSS');
					$this->Text((($this->posINSS + $this->StringTam) - $this->GetStringWidth($INSS)),$Linha,$INSS);
					
					//$JUROS = $Tabela->f("JUROS");
					//$JUROS = (float)((str_replace(",", ".", $JUROS)));
					//$JUROS = number_format($JUROS, 2,',','.');
					//$this->Text($this->posJuros,$Linha,$JUROS);
					//$this->StringTam = $this->GetStringWidth('   JUROS');
					//$this->Text((($this->posJuros + $this->StringTam) - $this->GetStringWidth($JUROS)),$Linha,$JUROS);
					
					//$VALPGT = $Tabela->f("DEBI_ATU");
					//$VALPGT = (float)((str_replace(",", ".", $VALPGT)));
					//$VALPGT = number_format($VALPGT, 2,',','.');
					//$this->Text($this->posValRec,$Linha,$VALPGT);
					//$this->StringTam = $this->GetStringWidth(' D�BITO ATUAL ');
					//$this->Text((($this->posValRec + $this->StringTam) - $this->GetStringWidth($VALPGT)),$Linha,$VALPGT);

					
					$DESCCANC =  trim(substr($Tabela->f("DESCCANC"),0,40));
					$this->Text($this->posMotivo,$Linha,$DESCCANC);
					$this->StringTam = $this->GetStringWidth('   MOTIVO');
                    $this->Text($this->posMotivo,$Linha,$DESCCANC);

					$DATCAN = substr($Tabela->f("DATCAN"),0,10);
					$this->StringTam = $this->GetStringWidth('DATA CANCELAMENTO');
					$this->Text((($this->posDatcan + $this->StringTam) - $this->GetStringWidth($DATCAN)),$Linha,$DATCAN);

					$CODOPE = $Tabela->f("CODOPE");
					$this->StringTam = $this->GetStringWidth('OPERADOR');
					$this->Text($this->posOperador,$Linha,$CODOPE);
					
					$Linha+=4;
				}
				if ($Linha != 50)
				{
					$this->Text(3,$Linha,"T  o  t  a  l     G  e  r  a  l");
					//           (       f i n a l                   ) -       t  a  m  n  h  o    
					$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
					$this->totalGeral = (float)((str_replace(",", ".", $this->totalGeral)));
					$this->totalGeral = number_format($this->totalGeral, 2,',','.');


					$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$Linha,$this->totalGeral);
				
					$this->StringTam = $this->GetStringWidth('  ISSQN');
				//	$this->Text((($this->posISSQN + $this->StringTam) - $this->GetStringWidth($TOTISSQN))-1,$Linha,$TOTISSQN);
					//$this->Text($this->posISSQN,$Linha,$TOTISSQN);
						
					$this->StringTam = $this->GetStringWidth('   INSS');
					$this->Text((($this->posINSS + $this->StringTam) - $this->GetStringWidth($TOTRET_INSS)),$Linha,$TOTRET_INSS);
					//$this->Text($this->posINSS,$Linha,$TOTRET_INSS);
						
						//$TOTJUROS    = (str_replace(",", ".",$Tabela->f("TOTJUROS")));
						//$TOTJUROS    = number_format($TOTJUROS, 2,',','.');
					$this->StringTam = $this->GetStringWidth('   JUROS');
				//	$this->Text((($this->posJuros + $this->StringTam) - $this->GetStringWidth($TOTJUROS)),$Linha,$TOTJUROS);
					//$this->Text($this->posJuros,$Linha,$TOTJUROS);
						
					$this->StringTam = $this->GetStringWidth(' D�BITO ATUAL ');
				//	$this->Text((($this->posValRec + $this->StringTam) - $this->GetStringWidth($TOTVALPGT)),$Linha,$TOTVALPGT);
					//$this->Text($this->posValRec,$Linha,$TOTVALPGT);
				}
				
				//$this->SetMargins(5,5,5);
				$this->SetFont('Arial','U',10);
				$this->SetTextColor(0, 0, 0);
				$this->SetAutoPageBreak(1);
				$this->Output();
				//unset($Tabela);
	}
	function Header()
	{

		$this->SetFont('Arial','B',20);
		// dimens�es da folha A4 - Largura = 210.6 mm e Comprimento 296.93 mm em Portrait. Em Landscape � s� inverter.
		$this->SetXY(0,0);
		//'SECRETARIA MUNICIPAL DE EDUCA��O DE BELO HORIZONTE'
		// Meio = (286/2) - (50/2) = 148,3 - 25 = 123,3
		$tanString = $this->GetStringWidth($this->title);
		$tamPonto = $this->fwPt;
		$tan = $this->fh;
		//$this->Text(($tamPonto/2) - ($tanString/2),6,$this->title);
		//$Unidade = CCGetSession("mDERCRI_UNI");
		$this->Text(($tan/2) - ($tanString/2),8,$this->title);

		//$this->Text(18,19,'DEPARTAMENTO DE ADMINISTRA��O FINANCEIRA');
		$this->SetFont('Arial','B',15);
		//$tanString = $this->GetStringWidth($Unidade);
		//$this->Text(($tan/2) - ($tanString/2),16,$Unidade);
		$this->SetFont('Arial','B',10);
		$tanString = $this->GetStringWidth($this->titulo);
		$this->Text(($tan/2) - ($tanString/2),24,$this->titulo);
		$this->Image('pdf/PBH_-_nova_-_Vertical_-_COR.jpg',5,10,33);
		//$mesRef = CCGetParam("mesRef");
		$this->Text(5,40,'Per�odo: '.$this->incial.' a '.$this->final);
		$dataSist = "Data da Emiss�o : ".CCGetSession("DataSist");
		$tanString = $this->GetStringWidth($dataSist);
		$this->Text($tan-($tanString+5),40,$dataSist);
		$this->Line(0,41,296.93,41);
		//               T   I   T   U   L   O       D   A   S       C   O  L  U   N   A   S 
		
		// Relat�rio de Arrecada��o
		{
			$this->SetFont('Arial','B',06.5);

			$this->SetXY(1,45);

			$this->Text(1,$this->GetY(),'NOME DO CLIENTE');
			$tanString = $this->GetStringWidth('NOME DO CLIENTE');
			
			$this->Text($this->GetX()+$tanString+55,$this->GetY(),'CNPJ/CPF');
			$this->posCPF = ($this->GetX()+$tanString+55);
			$this->SetX($this->GetX()+$tanString+55);
			$tanString = $this->GetStringWidth('CNPJ/CPF');
			
			$this->Text($this->GetX()+$tanString+10,$this->GetY(),'MES/ANO');
			$this->posEmissao = ($this->GetX()+$tanString+10);
			$this->SetX($this->GetX()+$tanString+10); 
			$tanString = $this->GetStringWidth('MES/ANO');

			$this->Text($this->GetX()+$tanString+7,$this->GetY(),'FATURA');
			$this->posFatura = ($this->GetX()+$tanString+7);
			$this->SetX($this->GetX()+$tanString+7);
			$tanString = $this->GetStringWidth('FATURA');
			
			$this->Text($this->GetX()+$tanString+7,$this->GetY(),'VALOR DA FATURA');
			$this->posValFat = ($this->GetX()+$tanString+7);
			$this->SetX($this->GetX()+$tanString+7);
			$tanString = $this->GetStringWidth('VALOR DA FATURA');
			
			$this->Text($this->GetX()+$tanString+7,$this->GetY(),'VENCIMENTO');
			$this->posVenc = ($this->GetX()+$tanString+7);
			$this->SetX($this->GetX()+$tanString+7);
			$tanString = $this->GetStringWidth('VENCIMENTO');
			/*
			$this->Text($this->GetX()+$tanString+10,$this->GetY(),'PAGAMENTO');
			$this->posPGT = ($this->GetX()+$tanString+10);
			$this->SetX($this->GetX()+$tanString+10);
			$tanString = $this->GetStringWidth('PAGAMENTO');
			
			$this->Text($this->GetX()+$tanString+10,$this->GetY(),'  ISSQN');
			$this->posISSQN = ($this->GetX()+$tanString+10);
			$this->SetX($this->GetX()+$tanString+10);
			$tanString = $this->GetStringWidth('  ISSQN');
			*/
			
			$this->Text($this->GetX()+$tanString+7,$this->GetY(),'   INSS');
			$this->posINSS = ($this->GetX()+$tanString+7);
			$this->SetX($this->GetX()+$tanString+7);
			$tanString = $this->GetStringWidth('   INSS');

			/*
			$this->Text($this->GetX()+$tanString+10,$this->GetY(),'   JUROS');
			$this->posJuros = ($this->GetX()+$tanString+10);
			$this->SetX($this->GetX()+$tanString+10);
			$tanString = $this->GetStringWidth('   JUROS');
			
			$this->Text($this->GetX()+$tanString+10,$this->GetY(),' D�BITO ATUAL ');
			$this->posValRec = ($this->GetX()+$tanString+10);
			$this->SetX($this->GetX()+$tanString+10);
			$tanString = $this->GetStringWidth(' D�BITO ATUAL ');
			*/

			$this->Text($this->GetX()+$tanString+7,$this->GetY(),'MOTIVO                                                     ');
			$this->posMotivo = ($this->GetX()+$tanString+7);
			$this->SetX($this->GetX()+$tanString+7);
			$tanString = $this->GetStringWidth('MOTIVO                                                     ');
			

			$this->Text($this->GetX()+$tanString+7,$this->GetY(),'DATA CANCELAMENTO');
			$this->posDatcan = ($this->GetX()+$tanString+7);
			$this->SetX($this->GetX()+$tanString+7);
			$tanString = $this->GetStringWidth('DATA CANCELAMENTO');
			
			$this->Text($this->GetX()+$tanString+7,$this->GetY(),'OPERADOR');
			$this->posOperador = ($this->GetX()+$tanString+7);
			$this->SetX($this->GetX()+$tanString+7);
			$tanString = $this->GetStringWidth('OPERADOR');
			
			$this->StringTam = $tanString;
			$this->Line(0,$this->GetY()+1,296.93,$this->GetY()+1);
		}
	}
	function footer()
	{
    //Position at 1.5 cm from bottom
    //$this->SetY(-7);
    //Arial italic 8
	//$this->SetFont('Arial','B',06);
	//$this->Text(3,$this->GetY(),"T  o  t  a  l     G  e  r  a  l");
	//           (       f i n a l                   ) -       t  a  m  n  h  o    
	//$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$this->GetY(),$this->totalGeral);
    //Position at 1.5 cm from bottom
    $this->SetY(-8);
    //Arial italic 8
    $this->SetFont('Arial','I',8);
	$this->Cell(0,10,'P�gina '.$this->PageNo().' / {nb}',0,0,'C');
    //Page number
	}
}
//unset($Tabela);
//unset($Grupo);
	/*$Imprime = new relatoriosPDF();
	$title = 'SUPERINTEND�NCIA DE LIMPESA URBANA';
	$Imprime->addPage('Portrait'); 
	$Imprime->SetFont('Arial','',10);
	$Imprime->SetTextColor(0, 0, 0);
	$Imprime->SetAutoPageBreak(0);
	$Imprime->Output();*/
?>