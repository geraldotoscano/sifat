<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0">
	<Components>
		<IncludePage id="2" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="4" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="GET" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="FormFiltro" actionPage="RelDebiGera" errorSummator="Error" wizardFormMethod="post" wizardOrientation="Vertical">
			<Components>
				<TextBox id="12" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="MES_REF_INI" caption="Mês inicial" required="True" format="mm/yyyy">
					<Components/>
					<Events>
						<Event name="OnKeyPress" type="Client">
							<Actions>
								<Action actionName="Validate Entry" actionCategory="Validation" id="21" required="True" minimumLength="7" maximumLength="7" inputMask="00/0000" regularExpression="^\d{2}\/\d{4}$" errorMessage="Data de início inválida. Deve ser digitado mês e ano. Ex.: 01/2015."/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="14" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="MES_REF_FIM" caption="Mês final" required="True" format="mm/yyyy">
					<Components/>
					<Events>
						<Event name="OnKeyPress" type="Client">
							<Actions>
								<Action actionName="Validate Entry" actionCategory="Validation" id="22" required="True" minimumLength="7" maximumLength="7" inputMask="00/0000" regularExpression="^\d{2}\/\d{4}$" errorMessage="Data final inválida. Deve ser digitado mês e ano. Ex.: 01/2015."/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<RadioButton id="30" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" html="True" returnValueType="Number" name="RBConsultar" caption="Quais serviços consultar" required="True" _valueOfList="vencimento" _nameOfList="Serviços que vencem no período" dataSource="vencimento;Serviços que vencem no período;incidencia;Serviços que incidem no período" defaultValue="vencimento">
<Components/>
<Events/>
<TableParameters/>
<SPParameters/>
<SQLParameters/>
<JoinTables/>
<JoinLinks/>
<Fields/>
<Attributes/>
<Features/>
</RadioButton>
<Button id="24" urlType="Relative" enableValidation="True" isDefault="False" name="btBuscarServico" operation="Search">
					<Components/>
					<Events>
						<Event name="OnClick" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="25"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
				<ListBox id="23" visible="Yes" fieldSourceType="DBColumn" sourceType="SQL" dataType="Text" returnValueType="Number" name="SUBSERVICO" wizardEmptyCaption="Select Value" dataSource="SELECT
  SUBSER,
  SUBSER||' - '||TRIM(DESSUB) AS DESCRICAO
FROM
  SUBSER
ORDER BY
  TRIM(DESSUB)" boundColumn="SUBSER" textColumn="DESCRICAO" required="False" connection="Faturar">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<RadioButton id="28" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" html="True" returnValueType="Number" name="RBSituacaoCliente" required="True" _nameOfList="Todos" dataSource="A;Ativos;I;Inativos;T;Todos" caption="Situação dos clientes" defaultValue="A">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</RadioButton>
				<RadioButton id="27" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" html="True" returnValueType="Number" name="RBFormatoRelatorio" caption="Formato do relatório" dataSource="PDF;PDF;CSV;CSV" required="True">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</RadioButton>
				<Button id="7" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search">
					<Components/>
					<Events>
						<Event name="OnClick" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="9"/>
							</Actions>
						</Event>
						<Event name="OnClick" type="Client">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="16"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events>
				<Event name="OnValidate" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="26"/>
					</Actions>
				</Event>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="29"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<IncludePage id="3" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="PHPTemplates" name="RelServicosCliente_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Code" language="PHPTemplates" name="RelServicosCliente.php" forShow="True" url="RelServicosCliente.php" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="17" groupID="1"/>
		<Group id="18" groupID="2"/>
		<Group id="19" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Custom Code" actionCategory="General" id="20"/>
			</Actions>
		</Event>
	</Events>
</Page>
