<?php
//Include Common Files @1-2DAF2608
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "RelServicosCliente.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files
include(RelativePath . "/pdf/relatoriofpdf.class.php");
include(RelativePath . "/util.php");
//Include Page implementation @2-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordFormFiltro { //FormFiltro Class @4-6DE050D0

//Variables @4-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @4-214C900E
    function clsRecordFormFiltro($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record FormFiltro/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "FormFiltro";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->MES_REF_INI = new clsControl(ccsTextBox, "MES_REF_INI", "M�s inicial", ccsText, "", CCGetRequestParam("MES_REF_INI", $Method, NULL), $this);
            $this->MES_REF_INI->Required = true;
            $this->MES_REF_FIM = new clsControl(ccsTextBox, "MES_REF_FIM", "M�s final", ccsText, "", CCGetRequestParam("MES_REF_FIM", $Method, NULL), $this);
            $this->MES_REF_FIM->Required = true;
            $this->RBConsultar = new clsControl(ccsRadioButton, "RBConsultar", "Quais servi�os consultar", ccsText, "", CCGetRequestParam("RBConsultar", $Method, NULL), $this);
            $this->RBConsultar->DSType = dsListOfValues;
            $this->RBConsultar->Values = array(array("vencimento", "Servi�os que vencem no per�odo"), array("incidencia", "Servi�os que incidem no per�odo"));
            $this->RBConsultar->HTML = true;
            $this->RBConsultar->Required = true;
            $this->btBuscarServico = new clsButton("btBuscarServico", $Method, $this);
            $this->SUBSERVICO = new clsControl(ccsListBox, "SUBSERVICO", "SUBSERVICO", ccsText, "", CCGetRequestParam("SUBSERVICO", $Method, NULL), $this);
            $this->SUBSERVICO->DSType = dsSQL;
            $this->SUBSERVICO->DataSource = new clsDBFaturar();
            $this->SUBSERVICO->ds = & $this->SUBSERVICO->DataSource;
            list($this->SUBSERVICO->BoundColumn, $this->SUBSERVICO->TextColumn, $this->SUBSERVICO->DBFormat) = array("SUBSER", "DESCRICAO", "");
            $this->SUBSERVICO->DataSource->SQL = "SELECT\n" .
            "  SUBSER,\n" .
            "  SUBSER||' - '||TRIM(DESSUB) AS DESCRICAO\n" .
            "FROM\n" .
            "  SUBSER {SQL_OrderBy}";
            $this->SUBSERVICO->DataSource->Order = "  TRIM(DESSUB)";
            $this->RBSituacaoCliente = new clsControl(ccsRadioButton, "RBSituacaoCliente", "Situa��o dos clientes", ccsText, "", CCGetRequestParam("RBSituacaoCliente", $Method, NULL), $this);
            $this->RBSituacaoCliente->DSType = dsListOfValues;
            $this->RBSituacaoCliente->Values = array(array("A", "Ativos"), array("I", "Inativos"), array("T", "Todos"));
            $this->RBSituacaoCliente->HTML = true;
            $this->RBSituacaoCliente->Required = true;
            $this->RBFormatoRelatorio = new clsControl(ccsRadioButton, "RBFormatoRelatorio", "Formato do relat�rio", ccsText, "", CCGetRequestParam("RBFormatoRelatorio", $Method, NULL), $this);
            $this->RBFormatoRelatorio->DSType = dsListOfValues;
            $this->RBFormatoRelatorio->Values = array(array("PDF", "PDF"), array("CSV", "CSV"));
            $this->RBFormatoRelatorio->HTML = true;
            $this->RBFormatoRelatorio->Required = true;
            $this->Button_DoSearch = new clsButton("Button_DoSearch", $Method, $this);
            if(!$this->FormSubmitted) {
                if(!is_array($this->RBConsultar->Value) && !strlen($this->RBConsultar->Value) && $this->RBConsultar->Value !== false)
                    $this->RBConsultar->SetText(vencimento);
                if(!is_array($this->RBSituacaoCliente->Value) && !strlen($this->RBSituacaoCliente->Value) && $this->RBSituacaoCliente->Value !== false)
                    $this->RBSituacaoCliente->SetText(A);
            }
        }
    }
//End Class_Initialize Event

//Validate Method @4-AAF74EC3
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->MES_REF_INI->Validate() && $Validation);
        $Validation = ($this->MES_REF_FIM->Validate() && $Validation);
        $Validation = ($this->RBConsultar->Validate() && $Validation);
        $Validation = ($this->SUBSERVICO->Validate() && $Validation);
        $Validation = ($this->RBSituacaoCliente->Validate() && $Validation);
        $Validation = ($this->RBFormatoRelatorio->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->MES_REF_INI->Errors->Count() == 0);
        $Validation =  $Validation && ($this->MES_REF_FIM->Errors->Count() == 0);
        $Validation =  $Validation && ($this->RBConsultar->Errors->Count() == 0);
        $Validation =  $Validation && ($this->SUBSERVICO->Errors->Count() == 0);
        $Validation =  $Validation && ($this->RBSituacaoCliente->Errors->Count() == 0);
        $Validation =  $Validation && ($this->RBFormatoRelatorio->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @4-A61D95DD
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->MES_REF_INI->Errors->Count());
        $errors = ($errors || $this->MES_REF_FIM->Errors->Count());
        $errors = ($errors || $this->RBConsultar->Errors->Count());
        $errors = ($errors || $this->SUBSERVICO->Errors->Count());
        $errors = ($errors || $this->RBSituacaoCliente->Errors->Count());
        $errors = ($errors || $this->RBFormatoRelatorio->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @4-9E3ED6A5
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "btBuscarServico";
            if($this->btBuscarServico->Pressed) {
                $this->PressedButton = "btBuscarServico";
            } else if($this->Button_DoSearch->Pressed) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = $FileName . "?" . CCGetQueryString("QueryString", array("ccsForm"));
        if($this->Validate()) {
            if($this->PressedButton == "btBuscarServico") {
                $Redirect = $FileName . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("btBuscarServico", "btBuscarServico_x", "btBuscarServico_y", "Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")), CCGetQueryString("QueryString", array("MES_REF_INI", "MES_REF_FIM", "RBConsultar", "SUBSERVICO", "RBSituacaoCliente", "RBFormatoRelatorio", "ccsForm")));
                if(!CCGetEvent($this->btBuscarServico->CCSEvents, "OnClick", $this->btBuscarServico)) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "Button_DoSearch") {
                $Redirect = $FileName . "?" . CCMergeQueryStrings(CCGetQueryString("Form", array("btBuscarServico", "btBuscarServico_x", "btBuscarServico_y", "Button_DoSearch", "Button_DoSearch_x", "Button_DoSearch_y")), CCGetQueryString("QueryString", array("MES_REF_INI", "MES_REF_FIM", "RBConsultar", "SUBSERVICO", "RBSituacaoCliente", "RBFormatoRelatorio", "ccsForm")));
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick", $this->Button_DoSearch)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @4-09E6E1C3
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);

        $this->RBConsultar->Prepare();
        $this->SUBSERVICO->Prepare();
        $this->RBSituacaoCliente->Prepare();
        $this->RBFormatoRelatorio->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->MES_REF_INI->Errors->ToString());
            $Error = ComposeStrings($Error, $this->MES_REF_FIM->Errors->ToString());
            $Error = ComposeStrings($Error, $this->RBConsultar->Errors->ToString());
            $Error = ComposeStrings($Error, $this->SUBSERVICO->Errors->ToString());
            $Error = ComposeStrings($Error, $this->RBSituacaoCliente->Errors->ToString());
            $Error = ComposeStrings($Error, $this->RBFormatoRelatorio->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->MES_REF_INI->Show();
        $this->MES_REF_FIM->Show();
        $this->RBConsultar->Show();
        $this->btBuscarServico->Show();
        $this->SUBSERVICO->Show();
        $this->RBSituacaoCliente->Show();
        $this->RBFormatoRelatorio->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End FormFiltro Class @4-FCB6E20C

//Include Page implementation @3-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation

//Initialize Page @1-9CA92E59
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "RelServicosCliente.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-946ECC7A
CCSecurityRedirect("1;2;3", "");
//End Authenticate User

//Include events file @1-863FC222
include("./RelServicosCliente_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-B1B93B1D
$DBFaturar = new clsDBFaturar();
$MainPage->Connections["Faturar"] = & $DBFaturar;
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$FormFiltro = new clsRecordFormFiltro("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->FormFiltro = & $FormFiltro;
$MainPage->rodape = & $rodape;

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-DF262271
$cabec->Operations();
$FormFiltro->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-645F8F23
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    $DBFaturar->close();
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($FormFiltro);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-F729DBAD
$cabec->Show();
$FormFiltro->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-A28EE37D
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$DBFaturar->close();
$cabec->Class_Terminate();
unset($cabec);
unset($FormFiltro);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
