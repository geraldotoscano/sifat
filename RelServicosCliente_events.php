<?php
//BindEvents Method @1-60BAA5E3
function BindEvents()
{
    global $FormFiltro;
    global $CCSEvents;
    $FormFiltro->Button_DoSearch->CCSEvents["OnClick"] = "FormFiltro_Button_DoSearch_OnClick";
    $FormFiltro->CCSEvents["OnValidate"] = "FormFiltro_OnValidate";
    $FormFiltro->CCSEvents["BeforeShow"] = "FormFiltro_BeforeShow";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//FormFiltro_Button_DoSearch_OnClick @7-C1BC3466
function FormFiltro_Button_DoSearch_OnClick(& $sender)
{
    $FormFiltro_Button_DoSearch_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $FormFiltro; //Compatibility
//End FormFiltro_Button_DoSearch_OnClick

//Custom Code @9-2A29BDB7
// -------------------------
  	$bd = new clsDBfaturar();
  	$descricao_filtros = array();
  	//obtem os m�ses do per�odo especificado
  	$meses = mesesPeriodo($FormFiltro->MES_REF_INI->GetValue(), $FormFiltro->MES_REF_FIM->GetValue(), true);
  	$meses_ref = implode('\',\'', $meses);
  	//determina o primeiro e o �ltimo dia do per�odo
  	$inicio_periodo = '01/'.$meses[0];
  	$fim_periodo = DateTime::createFromFormat('d/m/Y', '01/'.$meses[count($meses)-1])->modify('+1 month -1 day')->format('d/m/Y');
  
  	$subservico = (int)$FormFiltro->SUBSERVICO->GetValue();
	$situacao_cliente = $FormFiltro->RBSituacaoCliente->GetValue();
  	//verifica os filtro aplicados
  	if($subservico) {
  		$descricao_filtros[] = "Subservi�o: $subservico - ".CCDLookUp('DESSUB','SUBSER',"SUBSER = $subservico", $bd);
  		$subservico = "AND S.SUBSER = $subservico";
  	} else {
  		$descricao_filtros[] = "Todos os Subservi�os";
  		$subservico = '';
  	}
  	if($situacao_cliente == 'A') {
  		$descricao_filtros[] = "Somente clientes ativos";
  		$situacao_cliente = "AND C.CODSIT = 'A'";
  	} elseif($situacao_cliente == 'I') {
  		$descricao_filtros[] = "Somente clientes inativos";
  		$situacao_cliente = "AND C.CODSIT = 'I'";
  	} else {
  		$situacao_cliente = '';
	}
	$descricao_filtros = array(implode(' / ', $descricao_filtros));
	
	if($FormFiltro->RBConsultar->GetValue() == 'vencimento') {
		$tipo_rel = 'que vencem no per�odo';
		//consulta servi�os que vencem no per�odo
		$sql_faturas = "
		SELECT
			C.CODCLI,
			TRIM(C.DESCLI) AS DESCLI,
			DECODE(C.CODSIT, 'A', 'ATIVO', 'INATIVO') AS CODSIT,
			MS.QTDMED,
			TO_CHAR(MS.DATINI, 'DD/MM/YYYY')||' - '||TO_CHAR(MS.DATFIM, 'DD/MM/YYYY') AS VALIDADE,
			CASE 
				WHEN MS.INIRES IS NULL THEN 'N�O' 
				ELSE TO_CHAR(MS.INIRES, 'DD/MM/YY')||' - '||TO_CHAR(MS.FIMRES, 'DD/MM/YY') 
			END AS SUSPENSAO,
			MS.IDSERCLI,
			S.SUBSER,
			S.SUBSER||' - '||S.DESSUB AS DESSUB,
			S.UNIDAD
		FROM
			MOVSER MS
			INNER JOIN SUBSER S ON (MS.SUBSER = S.SUBSER)
			INNER JOIN CADCLI C ON (MS.CODCLI = C.CODCLI)
		WHERE
			(
				/*N�o � servi�o por tempo indeterminado*/
				( MS.TIPSER = 'N' OR MS.TIPSER IS NULL ) 
				AND
				/*vence no per�odo*/
				( MS.DATFIM BETWEEN TO_DATE('$inicio_periodo') AND TO_DATE('$fim_periodo') )
			) 
			$subservico
			$situacao_cliente
		ORDER BY
			S.DESSUB, TRIM(C.DESCLI)
		";
	} else {
		$tipo_rel = 'que incidem no per�odo';
		//consulta servi�os ativos para cliente. O filtro de per�odo � semelhante ao utilizado no faturamento coletivo. 
		$sql_faturas = "
		SELECT
			C.CODCLI,
			TRIM(C.DESCLI) AS DESCLI,
			DECODE(C.CODSIT, 'A', 'ATIVO', 'INATIVO') AS CODSIT,
			MS.QTDMED,
			CASE
				WHEN NVL(MS.TIPSER,'N') = 'S' AND NVL(MS.SERENC, 'N') != 'S' THEN TO_CHAR(MS.DATINI, 'DD/MM/YYYY')||' em diante'
				ELSE TO_CHAR(MS.DATINI, 'DD/MM/YYYY')||' - '||TO_CHAR(MS.DATFIM, 'DD/MM/YYYY')
			END AS VALIDADE,
			CASE 
				WHEN MS.INIRES IS NULL THEN 'N�O' 
				ELSE TO_CHAR(MS.INIRES, 'DD/MM/YY')||' - '||TO_CHAR(MS.FIMRES, 'DD/MM/YY') 
			END AS SUSPENSAO,
			MS.IDSERCLI,
			S.SUBSER,
			S.SUBSER||' - '||S.DESSUB AS DESSUB,
			S.UNIDAD
		FROM
		  MOVSER MS
		  INNER JOIN SUBSER S ON (MS.SUBSER = S.SUBSER)
		  INNER JOIN CADCLI C ON (MS.CODCLI = C.CODCLI)
		WHERE
		  (
		    (
		      /*N�o � por tempo indeterminado*/
		      ( MS.TIPSER = 'N' OR MS.TIPSER IS NULL ) 
		      AND
		      ( 
		        /*incide no per�odo*/
		        ( TO_DATE('$inicio_periodo') BETWEEN MS.DATINI AND MS.DATFIM ) OR
		        ( MS.DATINI BETWEEN TO_DATE('$inicio_periodo') AND TO_DATE('$fim_periodo') ) OR
		        ( MS.DATFIM BETWEEN TO_DATE('$inicio_periodo') AND TO_DATE('$fim_periodo') ) 
		      )
		    )
		    OR
		    ( 
		      /*� por tempo indeterminado e n�o pediu para encerrar cobran�a por tempo indeterminado*/
		      ( MS.TIPSER = 'S' AND MS.SERENC = 'N' ) AND
		      /*Come�ou a incidir antes do primeiro dia do m�s ref*/
		      MS.DATINI < TO_DATE('$inicio_periodo')
		    )
		  ) 
		  AND
		  (
		    /*O per�odo de suspen��o do servi�o n�o incidia no per�odo*/
		    ( MS.INIRES IS NULL OR TO_DATE('$inicio_periodo') <= MS.INIRES ) OR
		    ( MS.FIMRES IS NULL OR TO_DATE('$inicio_periodo') >= MS.FIMRES )
		  )
		  $subservico
		  $situacao_cliente
		ORDER BY
		  S.DESSUB, TRIM(C.DESCLI)
		";
	}

	//propriedades do relat�rio
	$cfg = array( 
		'SOURCE' => $sql_faturas,
		'TITULO' => 'Relat�rio de Servi�os '.$tipo_rel,
		'PERIODO' => 'M�s/Ano de refer�ncia: '.(count($meses)==1 ? $meses[0] : $meses[0].' a '.$meses[count($meses)-1]),
		'FILTROS' => $descricao_filtros,
		'COLUNAS' => array(
			'CODCLI' => array('CLIENTE'),
			'DESCLI' => array('DESCRI��O', 'L', 80, 2),
			'CODSIT' => array('SITUA.'),
			'IDSERCLI' => array('C�D SERV.', 'R'),
			
			'QTDMED' => array('QTD MEDIDA', 'R', null, 1),
			'UNIDAD' => array('UND'),
			'VALIDADE' => array('PERIODO', 'L', '25', 2),
			'SUSPENSAO' => array('SUSPENS�O', 'L', '23', 2),
			'DESSUB' => array('SUBSERVI�O', 'L', '75', 2)
		),
		'AGRUPAMENTO' => array(
			'ID' => 'SUBSER',
			'VALUE' => 'DESSUB',
			'NAME' => 'Subservi�o'
		),
		'TOTAL' => false,
		'LINE-HEIGHT' => 4.5
	);

	$relatorio = new RelatorioFPDF($cfg);
	$relatorio->emitir($FormFiltro->RBFormatoRelatorio->GetValue());
	
// -------------------------
//End Custom Code

//Close FormFiltro_Button_DoSearch_OnClick @7-9E60E70E
    return $FormFiltro_Button_DoSearch_OnClick;
}
//End Close FormFiltro_Button_DoSearch_OnClick

//FormFiltro_OnValidate @4-875A8BB2
function FormFiltro_OnValidate(& $sender)
{
    $FormFiltro_OnValidate = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $FormFiltro; //Compatibility
//End FormFiltro_OnValidate

//Custom Code @26-2A29BDB7
// -------------------------
	try {
		$meses = mesesPeriodo($FormFiltro->MES_REF_INI->GetValue(), $FormFiltro->MES_REF_FIM->GetValue(), true);
	} catch(Exception $e) {
		$FormFiltro->Errors->addError($e->getMessage());
	}
// -------------------------
//End Custom Code

//Close FormFiltro_OnValidate @4-B13236C7
    return $FormFiltro_OnValidate;
}
//End Close FormFiltro_OnValidate

//FormFiltro_BeforeShow @4-161D3B04
function FormFiltro_BeforeShow(& $sender)
{
    $FormFiltro_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $FormFiltro; //Compatibility
//End FormFiltro_BeforeShow

//Custom Code @29-2A29BDB7
// -------------------------
    $FormFiltro->RBSituacaoCliente->SetValue('A');
	$FormFiltro->RBFormatoRelatorio->SetValue('PDF');
// -------------------------
//End Custom Code

//Close FormFiltro_BeforeShow @4-8EC9524E
    return $FormFiltro_BeforeShow;
}
//End Close FormFiltro_BeforeShow

//Page_BeforeShow @1-A27C9668
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $RelServicosCliente; //Compatibility
//End Page_BeforeShow

//Custom Code @20-2A29BDB7
// -------------------------
    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	/*
		Permiss�es requeridas:
		30 - Faturamento - Fatura - Visualiza��o Acesso total
		51 - Servi�os dos Clientes Visualizar
	*/
	$permissao_requerida=array(30, 51);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");
// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow

?>