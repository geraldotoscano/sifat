<?php
	//error_reporting (E_ALL);
	//define("RelativePath", ".");
	//define('FPDF_FONTPATH','/pdf/font/');
	require('pdf/fpdf.php');
	//include('pdf/fpdi.php');
	//require_once("pdf/fpdi_pdf_parser.php");
	//require_once("pdf/fpdf_tpl.php");
	//include(RelativePath . "/Common.php");
	//include(RelativePath . "/Template.php");
	//include(RelativePath . "/Sorter.php");
	//include(RelativePath . "/Navigator.php");
	//include_once(RelativePath . "/cabec.php");
class relSoArrEmisPDF extends fpdf {	
	var $titulo;
	var $mesRef;
	var $posCliente;
	var $posFatura;
	var $posEmissao;
	var $posValFat;
	var $posVenc;
	var $totalGeral;
	var $StringTam;
	var $relat;
	var $mudouDistrito;
	var $distrito;
	var $posMesAno;
	var $posINSS;
	var $posPGT;
	var $posAcres;
	var $posRec;
	var $posISSQN;
	var $posJuros;
	var $posValRec;
	//$pdf= new fpdi();
	function relatorio($titu,$mesAno,$rel)
	{
		$this->mesRef = $mesAno;
		$this->SetTitle('SUPERINTEND�NCIA DE LIMPEZA URBANA');
		$this->titulo = $titu;
		$this->AliasNbPages();
		$Tabela   = new clsDBfaturar();
		//              L   I   N   H   A       D   E       D   E   T   A   L   H   E
		//                       Relat�rio de Arrecada��o por Inscri��o
		$Tabela->query("SELECT 
							F.CODCLI,
							to_char(F.DATEMI,'dd/mm/yyyy') AS DATEMI,
							to_char(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
							to_char(F.DATPGT,'dd/mm/yyyy') AS DATPGT,
							F.CODFAT,
							F.VALFAT,
							F.ISSQN,
							F.RET_INSS,
							DECODE((F.DATPGT-F.DATVNC),ABS((F.DATPGT-F.DATVNC)),ROUND(F.VALMUL*(F.DATPGT-F.DATVNC),2),0) AS JUROS,
							F.VALPGT,
							C.DESCLI,
							C.CGCCPF,
							C.ESFERA,
							(
								SELECT
									SUM(VALFAT)
								FROM
									CADFAT F,
									CADCLI C
								WHERE 
									F.CODCLI=C.CODCLI      AND 
									MESREF='$this->mesRef' AND
									F.VALPGT IS NOT NULL   AND
									F.VALPGT <> 0
							) AS TOTGER,
							(
								SELECT
									SUM(ISSQN)
								FROM
									CADFAT F,
									CADCLI C,
									GRPATI T
								WHERE 
									F.CODCLI=C.CODCLI      AND 
									C.GRPATI=T.GRPATI      AND
									F.MESREF='$this->mesRef' AND
									SUBSTR(F.MESREF,4,4)||SUBSTR(F.MESREF,1,2) < '200812' AND
									F.VALPGT IS NOT NULL     AND
									F.VALPGT <> 0
							) AS TOTISSQN,
							(
								SELECT
									SUM(RET_INSS)
								FROM
									CADFAT F,
									CADCLI C,
									GRPATI T
								WHERE 
									F.CODCLI=C.CODCLI AND 
									C.GRPATI=T.GRPATI AND
									F.MESREF='$this->mesRef' AND
									F.VALPGT IS NOT NULL     AND
									F.VALPGT <> 0
							) AS TOTRET_INSS,
							(
								SELECT
									SUM(DECODE((F.DATPGT-F.DATVNC),ABS((F.DATPGT-F.DATVNC)),ROUND(F.VALMUL*(F.DATPGT-F.DATVNC),2),0))
								FROM
									CADFAT F,
									CADCLI C,
									GRPATI T
								WHERE 
									F.CODCLI=C.CODCLI        AND 
									C.GRPATI=T.GRPATI        AND
									F.MESREF='$this->mesRef' AND
									F.DATPGT > F.DATVNC      AND
									F.VALPGT IS NOT NULL     AND
									F.VALPGT <> 0
							) AS TOTJUROS,
							(
								SELECT
									SUM(VALPGT)
								FROM
									CADFAT F,
									CADCLI C,
									GRPATI T
								WHERE 
									F.CODCLI=C.CODCLI AND 
									C.GRPATI=T.GRPATI AND
									F.MESREF='$this->mesRef' AND
									F.VALPGT IS NOT NULL     AND
									F.VALPGT <> 0
							) AS TOTVALPGT
						FROM 
							CADFAT F,
							CADCLI C 
						WHERE
							F.CODCLI=C.CODCLI        AND 
							F.MESREF='$this->mesRef' AND
							F.VALPGT IS NOT NULL     AND
							F.VALPGT <> 0
						ORDER BY 
							MESREF,CODCLI"
						);
				//$articles = array();
		/*
		$Tabela->query("SELECT 
							F.CODCLI,
							to_char(F.DATEMI,'dd/mm/yyyy') AS DATEMI,
							to_char(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
							to_char(F.DATPGT,'dd/mm/yyyy') AS DATPGT,
							F.CODFAT,
							F.VALFAT,
							F.ISSQN,
							F.RET_INSS,
							DECODE((F.DATPGT-F.DATVNC),ABS((F.DATPGT-F.DATVNC)),ROUND(F.VALMUL*(F.DATPGT-F.DATVNC),2),0) AS JUROS,
							F.VALPGT,
							C.DESCLI,
							C.CGCCPF,
							C.ESFERA,
							(
								SELECT
									SUM(VALFAT)
								FROM
									CADFAT F,
									CADCLI C
								WHERE 
									F.CODCLI=C.CODCLI      AND 
									MESREF='$this->mesRef'
							) AS TOTGER,
							(
								SELECT
									SUM(ISSQN)
								FROM
									CADFAT F,
									CADCLI C,
									GRPATI T
								WHERE 
									F.CODCLI=C.CODCLI      AND 
									C.GRPATI=T.GRPATI      AND
									F.MESREF='$this->mesRef' AND
									SUBSTR(F.MESREF,4,4)||SUBSTR(F.MESREF,1,2) < '200812'
							) AS TOTISSQN,
							(
								SELECT
									SUM(RET_INSS)
								FROM
									CADFAT F,
									CADCLI C,
									GRPATI T
								WHERE 
									F.CODCLI=C.CODCLI AND 
									C.GRPATI=T.GRPATI AND
									F.MESREF='$this->mesRef'
							) AS TOTRET_INSS,
							(
								SELECT
									SUM(DECODE((F.DATPGT-F.DATVNC),ABS((F.DATPGT-F.DATVNC)),ROUND(F.VALMUL*(F.DATPGT-F.DATVNC),2),0))
								FROM
									CADFAT F,
									CADCLI C,
									GRPATI T
								WHERE 
									F.CODCLI=C.CODCLI        AND 
									C.GRPATI=T.GRPATI        AND
									F.MESREF='$this->mesRef' AND
									F.DATPGT > F.DATVNC
							) AS TOTJUROS,
							(
								SELECT
									SUM(VALPGT)
								FROM
									CADFAT F,
									CADCLI C,
									GRPATI T
								WHERE 
									F.CODCLI=C.CODCLI AND 
									C.GRPATI=T.GRPATI AND
									F.MESREF='$this->mesRef'
							) AS TOTVALPGT
						FROM 
							CADFAT F,
							CADCLI C 
						WHERE
							F.CODCLI=C.CODCLI        AND 
							F.MESREF='$this->mesRef'
						ORDER BY 
							MESREF,CODCLI"
						);
				*/
				$Linha = 50;
				$this->SetY($Linha);
				$this->addPage('L');
						$TOTISSQN    = 0;
						$TOTISSQN    = number_format($TOTISSQN, 2,',','.');
						
						$TOTRET_INSS = 0;
						$TOTRET_INSS = number_format($TOTRET_INSS, 2,',','.');
						
						$TOTJUROS    = 0;
						$TOTJUROS    = number_format($TOTJUROS, 2,',','.');
						
						$TOTVALPGT   = 0;
						$TOTVALPGT   = number_format($TOTVALPGT, 2,',','.');
				while ($Tabela->next_record())
				{
					if ($this->totalGeral==0)
					{
						$this->totalGeral = (float)((str_replace(",", ".", $Tabela->f("TOTGER"))));
						$this->totalGeral = number_format($this->totalGeral, 2,',','.');
						
						$TOTISSQN    = (str_replace(",", ".",$Tabela->f("TOTISSQN")));
						$TOTISSQN    = number_format($TOTISSQN, 2,',','.');
						
						$TOTRET_INSS = (str_replace(",", ".",$Tabela->f("TOTRET_INSS")));
						$TOTRET_INSS = number_format($TOTRET_INSS, 2,',','.');
						
						$TOTJUROS    = (str_replace(",", ".",$Tabela->f("TOTJUROS")));
						$TOTJUROS    = number_format($TOTJUROS, 2,',','.');
						
						$TOTVALPGT   = (str_replace(",", ".",$Tabela->f("TOTVALPGT")));
						$TOTVALPGT   = number_format($TOTVALPGT, 2,',','.');
					}
						//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
					if ($this->GetY() >= ($this->fw-12))
					{
						$Linha = 50;
						$this->addPage('L'); 
					}
					$this->SetY($Linha);
					$Cliente = $Tabela->f("DESCLI");
					$this->Text(3,$Linha,$Cliente);
		
					$Inscricao = $Tabela->f("CGCCPF");
					$this->Text($this->posCPF,$Linha,$Inscricao);
		
					$Emissao = $Tabela->f("DATEMI");
					$this->Text($this->posEmissao,$Linha,$Emissao);
		
					$Fatura = $Tabela->f("CODFAT");
					$this->Text($this->posFatura,$Linha,$Fatura);
				
					$ValFat = $Tabela->f("VALFAT");
					$ValFat = (float)((str_replace(",", ".", $ValFat)));
					$ValFat = number_format($ValFat, 2,',','.');
					//           (       f i n a l                   ) -       t  a  m  n  h  o    
					$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
					$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($ValFat)),$Linha,$ValFat);
					
					$Venci = $Tabela->f("DATVNC");
					$this->Text($this->posVenc,$Linha,$Venci);
					
					$PAGTO = $Tabela->f("DATPGT");
					$this->Text($this->posPGT,$Linha,$PAGTO);

					$ISSQN = $Tabela->f("ISSQN");
					$ISSQN = (float)((str_replace(",", ".", $ISSQN)));
					$ISSQN = number_format($ISSQN, 2,',','.');
					if ($Tabela->f("VALPGT") == 0)
					{
						$ISSQN.=" N";
					}
					else
					{
						if (round($Tabela->f("VALPGT"),2) == round(($Tabela->f("VALFAT") - $Tabela->f("INSS") - ($Tabela->f("INSS")=='N')?($Tabela->f("INSSQN")):(0)),2))
						{
							$ISSQN.=" R";
						}
						else
						{
							if (round($Tabela->f("VALPGT"),2) == round(($Tabela->f("VALFAT") - $Tabela->f("INSS")),2))
							{
								$ISSQN.=" N";
							}
							else
							{
								if (round($Tabela->f("VALPGT"),2) == round(($Tabela->f("VALFAT") - ($Tabela->f("INSS")=='N')?($Tabela->f("INSSQN")):(0)),2))
								{
									$ISSQN.=" R";
								}
								else
								{
									if (round($Tabela->f("VALPGT"),2) == round($Tabela->f("VALFAT"),2))
									{
										$ISSQN.=" N";
									}
									else
									{
										if (round($Tabela->f("VALPGT"),2) > round($Tabela->f("VALFAT"),2))
										{
											$ISSQN.=" +";
										}
										else
										{
											$ISSQN.=" -";
										}
									}
								}
							}
						}
					}
					//$this->Text($this->posISSQN,$Linha,$ISSQN);
					$this->StringTam = $this->GetStringWidth('  ISSQN');
					$this->Text((($this->posISSQN + $this->StringTam) - $this->GetStringWidth($ISSQN)),$Linha,$ISSQN);
					
					
					$INSS = $Tabela->f("RET_INSS");
					$INSS = (float)((str_replace(",", ".", $INSS)));
					$INSS = number_format($INSS, 2,',','.');
					//$this->Text($this->posINSS,$Linha,$INSS);
					$this->StringTam = $this->GetStringWidth('   INSS');
					$this->Text((($this->posINSS + $this->StringTam) - $this->GetStringWidth($INSS)),$Linha,$INSS);
					
					$JUROS = $Tabela->f("JUROS");
					$JUROS = (float)((str_replace(",", ".", $JUROS)));
					$JUROS = number_format($JUROS, 2,',','.');
					//$this->Text($this->posJuros,$Linha,$JUROS);
					$this->StringTam = $this->GetStringWidth('   JUROS');
					$this->Text((($this->posJuros + $this->StringTam) - $this->GetStringWidth($JUROS)),$Linha,$JUROS);
					
					$VALPGT = $Tabela->f("VALPGT");
					$VALPGT = (float)((str_replace(",", ".", $VALPGT)));
					$VALPGT = number_format($VALPGT, 2,',','.');
					//$this->Text($this->posValRec,$Linha,$VALPGT);
					$this->StringTam = $this->GetStringWidth('   RECEBIDO');
					$this->Text((($this->posValRec + $this->StringTam) - $this->GetStringWidth($VALPGT)),$Linha,$VALPGT);
					
					$Linha+=4;
				}
				$this->Text(3,$Linha,"T  o  t  a  l     G  e  r  a  l");
				//           (       f i n a l                   ) -       t  a  m  n  h  o    
				$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
				$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$Linha,$this->totalGeral);
				
				$this->StringTam = $this->GetStringWidth('  ISSQN');
				$this->Text((($this->posISSQN + $this->StringTam) - $this->GetStringWidth($TOTISSQN))-1,$Linha,$TOTISSQN);
				//$this->Text($this->posISSQN,$Linha,$TOTISSQN);
						
				$this->StringTam = $this->GetStringWidth('   INSS');
				$this->Text((($this->posINSS + $this->StringTam) - $this->GetStringWidth($TOTRET_INSS)),$Linha,$TOTRET_INSS);
				//$this->Text($this->posINSS,$Linha,$TOTRET_INSS);
						
						//$TOTJUROS    = (str_replace(",", ".",$Tabela->f("TOTJUROS")));
						//$TOTJUROS    = number_format($TOTJUROS, 2,',','.');
				$this->StringTam = $this->GetStringWidth('   JUROS');
				$this->Text((($this->posJuros + $this->StringTam) - $this->GetStringWidth($TOTJUROS)),$Linha,$TOTJUROS);
				//$this->Text($this->posJuros,$Linha,$TOTJUROS);
						
				$this->StringTam = $this->GetStringWidth('   RECEBIDO');
				$this->Text((($this->posValRec + $this->StringTam) - $this->GetStringWidth($TOTVALPGT)),$Linha,$TOTVALPGT);
				//$this->Text($this->posValRec,$Linha,$TOTVALPGT);
				
				
				//$this->SetMargins(5,5,5);
				$this->SetFont('Arial','U',10);
				$this->SetTextColor(0, 0, 0);
				$this->SetAutoPageBreak(1);
				$this->Output();
	}
	function Header()
	{

		$this->SetFont('Arial','B',20);
		// dimens�es da folha A4 - Largura = 210.6 mm e Comprimento 296.93 mm em Portrait. Em Landscape � s� inverter.
		$this->SetXY(0,0);
		//'SECRETARIA MUNICIPAL DE EDUCA��O DE BELO HORIZONTE'
		// Meio = (286/2) - (50/2) = 148,3 - 25 = 123,3
		$tanString = $this->GetStringWidth($this->title);
		$tamPonto = $this->fwPt;
		$tan = $this->fh;
		//$this->Text(($tamPonto/2) - ($tanString/2),6,$this->title);
		//$Unidade = CCGetSession("mDERCRI_UNI");
		$this->Text(($tan/2) - ($tanString/2),8,$this->title);

		//$this->Text(18,19,'DEPARTAMENTO DE ADMINISTRA��O FINANCEIRA');
		$this->SetFont('Arial','B',15);
		//$tanString = $this->GetStringWidth($Unidade);
		//$this->Text(($tan/2) - ($tanString/2),16,$Unidade);
		$this->SetFont('Arial','B',10);
		$tanString = $this->GetStringWidth($this->titulo);
		$this->Text(($tan/2) - ($tanString/2),24,$this->titulo);
		$this->Image('pdf/PBH_-_nova_-_Vertical_-_COR.jpg',5,10,33);
		//$mesRef = CCGetParam("mesRef");
		$this->Text(5,40,'M�s / Ano de Refer�ncia : '.$this->mesRef);
		$dataSist = "Data da Emiss�o : ".CCGetSession("DataSist");
		$tanString = $this->GetStringWidth($dataSist);
		$this->Text($tan-($tanString+5),40,$dataSist);
		$this->Line(0,41,296.93,41);
		//               T   I   T   U   L   O       D   A   S       C   O  L  U   N   A   S 
		
		// Relat�rio de Arrecada��o
		{
			$this->SetFont('Arial','B',06.5);

			$this->SetXY(1,45);

			$this->Text(1,$this->GetY(),'NOME DO CLIENTE');
			$tanString = $this->GetStringWidth('NOME DO CLIENTE');
			
			$this->Text($this->GetX()+$tanString+55,$this->GetY(),'CNPJ/CPF');
			$this->posCPF = ($this->GetX()+$tanString+55);
			$this->SetX($this->GetX()+$tanString+55);
			$tanString = $this->GetStringWidth('CNPJ/CPF');
			
			$this->Text($this->GetX()+$tanString+10,$this->GetY(),'EMISS�O');
			$this->posEmissao = ($this->GetX()+$tanString+10);
			$this->SetX($this->GetX()+$tanString+10); 
			$tanString = $this->GetStringWidth('EMISS�O');

			$this->Text($this->GetX()+$tanString+10,$this->GetY(),'FATURA');
			$this->posFatura = ($this->GetX()+$tanString+10);
			$this->SetX($this->GetX()+$tanString+10);
			$tanString = $this->GetStringWidth('FATURA');
			
			$this->Text($this->GetX()+$tanString+10,$this->GetY(),'VALOR DA FATURA');
			$this->posValFat = ($this->GetX()+$tanString+10);
			$this->SetX($this->GetX()+$tanString+10);
			$tanString = $this->GetStringWidth('VALOR DA FATURA');
			
			$this->Text($this->GetX()+$tanString+10,$this->GetY(),'VENCIMENTO');
			$this->posVenc = ($this->GetX()+$tanString+10);
			$this->SetX($this->GetX()+$tanString+10);
			$tanString = $this->GetStringWidth('VENCIMENTO');
			
			$this->Text($this->GetX()+$tanString+10,$this->GetY(),'PAGAMENTO');
			$this->posPGT = ($this->GetX()+$tanString+10);
			$this->SetX($this->GetX()+$tanString+10);
			$tanString = $this->GetStringWidth('PAGAMENTO');
			
			$this->Text($this->GetX()+$tanString+10,$this->GetY(),'  ISSQN');
			$this->posISSQN = ($this->GetX()+$tanString+10);
			$this->SetX($this->GetX()+$tanString+10);
			$tanString = $this->GetStringWidth('  ISSQN');
			
			$this->Text($this->GetX()+$tanString+10,$this->GetY(),'   INSS');
			$this->posINSS = ($this->GetX()+$tanString+10);
			$this->SetX($this->GetX()+$tanString+10);
			$tanString = $this->GetStringWidth('   INSS');
			
			$this->Text($this->GetX()+$tanString+10,$this->GetY(),'   JUROS');
			$this->posJuros = ($this->GetX()+$tanString+10);
			$this->SetX($this->GetX()+$tanString+10);
			$tanString = $this->GetStringWidth('   JUROS');
			
			$this->Text($this->GetX()+$tanString+10,$this->GetY(),'   RECEBIDO');
			$this->posValRec = ($this->GetX()+$tanString+10);
			$this->SetX($this->GetX()+$tanString+10);
			$tanString = $this->GetStringWidth('   RECEBIDO');
			
			$this->StringTam = $tanString;
			$this->Line(0,$this->GetY()+1,296.93,$this->GetY()+1);
		}
	}
	function footer()
	{
    //Position at 1.5 cm from bottom
    //$this->SetY(-7);
    //Arial italic 8
	//$this->SetFont('Arial','B',06);
	//$this->Text(3,$this->GetY(),"T  o  t  a  l     G  e  r  a  l");
	//           (       f i n a l                   ) -       t  a  m  n  h  o    
	//$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$this->GetY(),$this->totalGeral);
    //Position at 1.5 cm from bottom
    $this->SetY(-8);
    //Arial italic 8
    $this->SetFont('Arial','I',8);
	$this->Cell(0,10,'P�gina '.$this->PageNo().' / {nb}',0,0,'C');
    //Page number
	}
}
//unset($Tabela);
//unset($Grupo);
	/*$Imprime = new relatoriosPDF();
	$title = 'SUPERINTEND�NCIA DE LIMPESA URBANA';
	$Imprime->addPage('Portrait'); 
	$Imprime->SetFont('Arial','',10);
	$Imprime->SetTextColor(0, 0, 0);
	$Imprime->SetAutoPageBreak(0);
	$Imprime->Output();*/
?>