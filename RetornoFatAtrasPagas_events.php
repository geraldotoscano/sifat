<?php
//BindEvents Method @1-D6EE8602
function BindEvents()
{
    global $NRRetFatPag;
    global $CCSEvents;
    $NRRetFatPag->lblinformes->CCSEvents["BeforeShow"] = "NRRetFatPag_lblinformes_BeforeShow";
    $NRRetFatPag->lbltotfaturas->CCSEvents["BeforeShow"] = "NRRetFatPag_lbltotfaturas_BeforeShow";
    $NRRetFatPag->Button1->CCSEvents["OnClick"] = "NRRetFatPag_Button1_OnClick";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//NRRetFatPag_lblinformes_BeforeShow @25-E1F138C5
function NRRetFatPag_lblinformes_BeforeShow(& $sender)
{
    $NRRetFatPag_lblinformes_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRRetFatPag; //Compatibility
//End NRRetFatPag_lblinformes_BeforeShow

//Custom Code @26-2A29BDB7
// -------------------------
    // Write your own code here.
// -------------------------
//End Custom Code

//C-2A29BDB7
// -------------------------
    $db = new clsDBfaturar();

    $erro_validando=false;
	$db->query("select fac.codfat, fac.codfatatrs from fatatrs_cadfat fac,cadfat f, fatatrs fa where fa.codfatatrs= fac.codfatatrs and fac.codfat= f.codfat and f.valpgt>0 and ( fa.valpgt=0 or fa.valpgt is null) order by fac.codfatatrs");
	$faturas_pagas="";
	$ultcodfatatrs="";
	if($db->next_record())
	  {
       $erro_validando=true;
	   $faturas_pagas=$db->f("codfatatrs")."(".$db->f("codfat");
	   $ultcodfatatrs=$db->f("codfatatrs");
	  }

	while($db->next_record())
	  {
	   if($ultcodfatatrs==$db->f("codfatatrs"))
	     {
          $faturas_pagas=$faturas_pagas.", ".$db->f("codfat");
	     }
        else
		 {
 	      $faturas_pagas=$faturas_pagas.") ".$db->f("codfatatrs")."(".$db->f("codfat");
	      $ultcodfatatrs=$db->f("codfatatrs");
		 }
	  }

    if($faturas_pagas!=0)
	  {
	    $faturas_pagas=$faturas_pagas.")";
        $faturas_pagas="\n<br>As seguintes faturas foram pagas separadamente cobran�a(faturas,...): ".$faturas_pagas;
	  }

	$db->query("select fac.codfat from fatatrs_cadfat fac, cadfat_canc f, fatatrs fa where fa.codfatatrs= fac.codfatatrs and fac.codfat= f.codfat and  ( fa.valpgt=0 or fa.valpgt is null)");
	$faturas_canceladas="";
	if($db->next_record())
	  {
       $erro_validando=true;
	   $faturas_canceladas=$db->f("codfat");
	  }

	while($db->next_record())
	  {
       $faturas_canceladas=$faturas_canceladas.", ".$db->f("codfat");
	  }
    if($faturas_canceladas!="")
      $faturas_canceladas="\n<br>As seguintes faturas canceladas ".$faturas_canceladas;
    if($erro_validando)
	  {
       $NRRetFatPag->lblinformes->SetValue("N�o ser� poss�vel realizar o recebimento das faturas em atraso devido:\n<br>".$faturas_pagas.$faturas_canceladas);
	   $NRRetFatPag->Button1->Visible=false;

	  }



// -------------------------
//End Custom Code

//Close NRRetFatPag_lblinformes_BeforeShow @25-B367E2D8
    return $NRRetFatPag_lblinformes_BeforeShow;
}
//End Close NRRetFatPag_lblinformes_BeforeShow

//NRRetFatPag_lbltotfaturas_BeforeShow @23-C9FF0E68
function NRRetFatPag_lbltotfaturas_BeforeShow(& $sender)
{
    $NRRetFatPag_lbltotfaturas_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRRetFatPag; //Compatibility
//End NRRetFatPag_lbltotfaturas_BeforeShow

//Custom Code @24-2A29BDB7
// -------------------------
    // Write your own code here.

 $NRRetFatPag->lbltotfaturas->SetValue(CCGetSession('totfaturas'));
 CCSetSession('totfaturas','');
 
 
// -------------------------
//End Custom Code

//Close NRRetFatPag_lbltotfaturas_BeforeShow @23-B5C4379E
    return $NRRetFatPag_lbltotfaturas_BeforeShow;
}
//End Close NRRetFatPag_lbltotfaturas_BeforeShow

//DEL  // -------------------------
//DEL  	$mArqMov = fopen("c:\\sistfat\\Retorno.txt","w");
//DEL  	fwrite($mArqMov,"Nome do Arquivo \r\n");
//DEL  	$mArquivo1 =  basename($_FILES['file']['name']);//$NRRetFatPag->FileUpload1->GetFileName();
//DEL  	$linhas=file($mArquivo1);
//DEL  	fwrite($mArqMov,"el miguel $mArquivo1 \r\n");
//DEL      for($i=0;$i<count($linhas);$i++)
//DEL         {
//DEL  		fwrite($mArqMov, $linhas[$i]." \r\n");
//DEL         }
//DEL  	   fclose($mArqMov);
//DEL      /*
//DEL  	$mArquivo = "c:\\windows\\" . $mArquivo1;
//DEL  	$mArqMov = fopen("c:\\sistfat\\Retorno.txt","w");
//DEL  	fwrite($mArqMov,"Nome do Arquivo \r\n");
//DEL  	fwrite($mArqMov,"$mArquivo\r\n");
//DEL     	if (file_exists($mArquivo))
//DEL  	{
//DEL  		fwrite($mArqMov,"Arquivo existe\r\n");
//DEL  		fwrite($mArqMov,"$mArquivo\r\n");
//DEL  		$handle = fopen($mArquivo,"r");
//DEL  		$Tab_CADFAT = new clsDBfaturar();
//DEL  		$Tab_TABPBH = new clsDBfaturar();
//DEL  		$Tab_MOVFAT = new clsDBfaturar();
//DEL  		while (!feof($handle))
//DEL  		{
//DEL  			fwrite($mArqMov,"Lendo o Arquivo\r\n");
//DEL  			$mLinha = fgets($handle); //fread($handle, 400);
//DEL  			fwrite($mArqMov,"Linha Lida\r\n");
//DEL  			fwrite($mArqMov,"$mLinha\r\n");
//DEL  			fwrite($mArqMov,"Tamanho da Linha ".strlen($mLinha)."\r\n");
//DEL  
//DEL  
//DEL  			$l_codreg = substr($mLinha, 0, 1);
//DEL  			fwrite($mArqMov,"Tipo registro = $l_codreg\r\n");
//DEL  
//DEL  			$l_codoco = substr($mLinha, 108, 2);
//DEL  			fwrite($mArqMov,"Ocorrencia = $l_codoco\r\n");
//DEL  
//DEL  			$l_codfat = substr($mLinha, 116, 6);// 1=111 3=113
//DEL  			fwrite($mArqMov,"Fatura = $l_codfat\r\n");
//DEL  
//DEL  			$l_datpgt = substr($mLinha, 110, 2) . '/' . substr($mLinha, 112, 2) . '/' . '20' . substr ($mLinha, 114, 2);
//DEL  			fwrite($mArqMov,"Pagamento = $l_datpgt\r\n");
//DEL  
//DEL  			$l_valpgt = substr($mLinha, 253,13);
//DEL  			fwrite($mArqMov,"Valor 1 = $l_valpgt\r\n");
//DEL  			$l_valpgt = (float)$l_valpgt;
//DEL  			fwrite($mArqMov,"Valor 2 = $l_valpgt\r\n");
//DEL  			$l_valpgt /= 100;
//DEL  			fwrite($mArqMov,"Valor 3 = $l_valpgt\r\n");
//DEL  
//DEL  	
//DEL  			if (!(($l_codreg != '1') or ($l_codoco != '06' and $l_codoco != '16' and $l_codoco != '17' and $l_codoco != '03')))
//DEL  			{
//DEL  				fwrite($mArqMov,"Linha Fatura\r\n");
//DEL  				if ($l_valpgt > 0)
//DEL  				{
//DEL  					fwrite($mArqMov,"Fatura maior que zero\r\n");
//DEL  					$Tab_CADFAT->query("select valfat,mesref from cadfat where codfat = '$l_codfat'");
//DEL  					$Tab_CADFAT->next_record();
//DEL  					$l_mesref = $Tab_CADFAT->f("mesref");
//DEL  					$l_valfat = $Tab_CADFAT->f("valfat");
//DEL  					$l_valfat = str_replace("," , ".", $l_valfat);
//DEL  					$l_valfat = (float)$l_valfat;
//DEL  					
//DEL  					fwrite($mArqMov,"Mes de Referencia $l_mesref\r\n");
//DEL  					$Tab_TABPBH->query("select valpbh from tabpbh where mesref = '$l_mesref'");
//DEL  					$Tab_TABPBH->next_record();
//DEL  					$valpbh = $Tab_TABPBH->f("valpbh");
//DEL  					$valpbh = str_replace("," , ".", $valpbh);
//DEL  					fwrite($mArqMov,"Valor PBH $valpbh\r\n");
//DEL  	
//DEL  					$Tab_CADFAT->query("update cadfat set datpgt = to_date('$l_datpgt'),valpgt = $l_valpgt where codfat = '$l_codfat' ");
//DEL  					
//DEL  					$Tab_MOVFAT->query("update movfat set valpgt = round (qtdmed * equpbh * $valpbh, 2),datpgt = to_date('$l_datpgt') where codfat = '$l_codfat'");
//DEL  					if ($l_valfat != $l_valpgt)
//DEL  					{
//DEL  						fwrite($mArqMov,"Valor pago diferente do valor da fatura\r\n");
//DEL  						$Tab_MOVFAT->query("select codfat,grpser,subser from movfat where codfat = '$l_codfat'");
//DEL  						$Tab_MOVFAT->next_record();
//DEL  						$l_codfat = $Tab_MOVFAT->f("codfat"); 
//DEL  						$l_grpser = $Tab_MOVFAT->f("grpser");
//DEL  						$l_subser = $Tab_MOVFAT->f("subser");
//DEL  						$Tab_MOVFAT->query("update movfat set valpgt = valpgt + ($l_valpgt - $l_valfat) where codfat = '$l_codfat' and grpser = '$l_grpser' and subser = '$l_subser'");
//DEL  					}
//DEL  	
//DEL  				}
//DEL  				
//DEL  			}
//DEL  			else if ($l_codoco == '03')
//DEL  			{
//DEL  				// mensagem
//DEL  			}
//DEL  		}
//DEL  		$Tab_CADFAT->close();
//DEL  		$Tab_TABPBH->close();
//DEL  		$Tab_MOVFAT->close();
//DEL  		unset($Tab_CADFAT);
//DEL  		unset($Tab_TABPBH);
//DEL  		unset($Tab_MOVFAT);
//DEL  		fclose($mArqMov);
//DEL  		fclose($handle);
//DEL  	}*/
//DEL  // -------------------------

//NRRetFatPag_Button1_OnClick @7-2D869054
function NRRetFatPag_Button1_OnClick(& $sender)
{
    $NRRetFatPag_Button1_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRRetFatPag; //Compatibility
//End NRRetFatPag_Button1_OnClick

//Custom Code @8-2A29BDB7
// -------------------------
    //print_r($_FILES);
    $mArquivo1 =  $_FILES['uploadedfile']['tmp_name'];//$NRRetFatPag->FileUpload1->GetFileName();
    $linhas=file($mArquivo1);
	$totfaturas=0;
	$_SESSION['_MENSAGEM_PROX_TELA']='';
	$usuario_sistema=CCGetUserID();
	
	
	//print $mArquivo1."<br>";
    //print $mArquivo1."<br>";
    for($i=0;$i<count($linhas);$i++)
  	{
  		$Tab_FATATRS = new clsDBfaturar();
		$Tab_CADFAT  = new clsDBfaturar();
  		$Tab_TABPBH  = new clsDBfaturar();
  		$Tab_MOVFAT  = new clsDBfaturar();
  		{
  			$mLinha = $linhas[$i];
			//print $mLinha."<br>";
  			$l_codreg = substr($mLinha, 0, 1);
  			$l_codoco = substr($mLinha, 108, 2);
  			$l_codfat = substr($mLinha, 116, 10);// 1=111 3=113

  			$l_codfat = trim($l_codfat);
			$l_tipfat ='';
			if(strlen($l_codfat)==7)
			  {
			   
			   $l_tipfat = substr($l_codfat,6, 1);// Tipo da fatura
			   $l_codfat = substr($l_codfat,0, 6);// Tipo da fatura
			  }
             
			 
			 if($l_tipfat =='')
			  {
               $l_tipfat = '0';// Tipo da fatura
			  }
  			

  			$l_datpgt = substr($mLinha, 110, 2) . '/' . substr($mLinha, 112, 2) . '/' . '20' . substr ($mLinha, 114, 2);
  			$l_valpgt = substr($mLinha, 253,13);
  			$l_valpgt = (float)$l_valpgt;
  			$l_valpgt /= 100;

			if($l_tipfat=='1')
			  {
	  			if (!(($l_codreg != '1') or ($l_codoco != '06' and $l_codoco != '16' and $l_codoco != '17' and $l_codoco != '03')))
	  			{
	  				if ($l_valpgt > 0)
	  				{
	  					$Tab_FATATRS->query("select valfat+valjur-ret_inss as valfat,to_char(datvnc,'dd/mm/yyyy') as datvnc from fatatrs where codfatatrs = '$l_codfat' and ( valpgt=0 or valpgt is null ) ");

                        $atualizar=false;

	  					if($Tab_FATATRS->next_record())
						  {
		  					$l_valfat = $Tab_FATATRS->f("valfat");
		  					$l_valfat = str_replace("," , ".", $l_valfat);
		  					$l_valfat = (float)$l_valfat;
							$l_datvnc = $Tab_FATATRS->f("datvnc");
							$atualizar=true;
						  }

	  					if ($l_valfat == $l_valpgt && $atualizar==true)
	  					{

		  					$Tab_FATATRS->query("select codfat from fatatrs_cadfat where codfatatrs = '$l_codfat'");

							$Tab_CADFAT->query("update fatatrs set datpgt = sysdate,valpgt = $l_valfat,datamod=sysdate,idmod=$usuario_sistema where codfatatrs = '$l_codfat' ");

							$totfaturas=$totfaturas+1;

		  					while($Tab_FATATRS->next_record())
							  {

								$sf_codfat=$Tab_FATATRS->f("codfat");

			  					$Tab_CADFAT->query("select valfat+round(((VALFAT-RET_INSS)*(1/100/30))*((to_date('$l_datvnc','dd/mm/yyyy')-DATVNC)-1),2)- ret_inss AS valfat ,mesref from cadfat where codfat = '$sf_codfat'");
			  					$Tab_CADFAT->next_record();


			  					$sf_mesref = $Tab_CADFAT->f("mesref");
			  					$sf_valfat = $Tab_CADFAT->f("valfat");
			  					$sf_valfat = str_replace("," , ".", $sf_valfat);
			  					$sf_valfat = (float)$sf_valfat;

						    
                                

			  					$Tab_CADFAT->query("update cadfat set datpgt = to_date('$l_datvnc','dd/mm/yyyy'),valpgt = $sf_valfat,descfat=descfat||'\nPago em fatura coletiva codigo $l_codfat',datamod=sysdate,idmod=$usuario_sistema where codfat = '$sf_codfat' ");

			  					$Tab_TABPBH->query("select valpbh from tabpbh where mesref = '$sf_mesref'");
								$Tab_TABPBH->next_record();
								  
				  				$valpbh = $Tab_TABPBH->f("valpbh");
				  				$valpbh = str_replace("," , ".", $valpbh);

//				  				$Tab_MOVFAT->query("update movfat set valpgt = round (qtdmed * equpbh * $valpbh, 2),datpgt = to_date('$l_datvnc','dd/mm/yyyy') where codfat = '$sf_codfat'");

                                $Tab_MOVFAT->query("update movfat set valpgt = round((valser/(select sum(valser) from movfat where codfat='$sf_codfat'))*$sf_valfat,2),datpgt = to_date('$l_datvnc','dd/mm/yyyy') where codfat = '$sf_codfat'");


								$NRRetFatPag->lbltotfaturas->SetValue($NRRetFatPag->lbltotfaturas->GetValue()+1);
								
/*
		  						$Tab_MOVFAT->query("select codfat,grpser,subser from movfat where codfat = '$l_codfat'");
		  						$Tab_MOVFAT->next_record();
		  						$l_codfat = $Tab_MOVFAT->f("codfat"); 
		  						$l_grpser = $Tab_MOVFAT->f("grpser");
		  						$l_subser = $Tab_MOVFAT->f("subser");
		  						$Tab_MOVFAT->query("update movfat set valpgt = valpgt + ($l_valpgt - $l_valfat) where codfat = '$l_codfat' and grpser = '$l_grpser' and subser = '$l_subser'");
*/
							  }
                            
	  					}
                       else
					    {

						 if($atualizar==true)
						    $_SESSION['_MENSAGEM_PROX_TELA']=$_SESSION['_MENSAGEM_PROX_TELA'].'<br> Fatura "'.$l_codfat.$l_tipfat.'" com valor difer�nte. Esperado '.$l_valfat.' retornado '.$l_valpgt;


						}
                    

	  				}

	  			}
	  			else if ($l_codoco == '03')
	  			{
	  				// mensagem

	  			}
	  		}
		  }
  		$Tab_CADFAT->close();
  		$Tab_TABPBH->close();
  		$Tab_MOVFAT->close();
  		unset($Tab_CADFAT);
  		unset($Tab_TABPBH);
  		unset($Tab_MOVFAT);
    }


$_SESSION['_MENSAGEM_PROX_TELA']='Foram importadas '.$totfaturas.' Faturas'.$_SESSION['_MENSAGEM_PROX_TELA'];
CCSetSession('totfaturas',$totfaturas);

// -------------------------
//End Custom Code

//Close NRRetFatPag_Button1_OnClick @7-AF2E71A0
    return $NRRetFatPag_Button1_OnClick;
}
//End Close NRRetFatPag_Button1_OnClick

//Page_BeforeShow @1-5585EE88
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $RetornoFatAtrasPagas; //Compatibility
//End Page_BeforeShow

//Custom Code @22-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(56);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow

//DEL  // -------------------------
//DEL  	$mArqMov = fopen("c:\\sistfat\\Retorno.txt","w");
//DEL  	fwrite($mArqMov,"Nome do Arquivo \r\n");
//DEL  	$mArquivo1 =  basename($_FILES['file']['name']);//$NRRetFatPag->FileUpload1->GetFileName();
//DEL  	$linhas=file($mArquivo1);
//DEL  	fwrite($mArqMov,"el ambrosio $mArquivo1 \r\n");
//DEL      for($i=0;$i<count($linhas);$i++)
//DEL         {
//DEL  		fwrite($mArqMov, $linhas[$i]." \r\n");
//DEL         }
//DEL  	   fclose($mArqMov);
//DEL  // -------------------------


//DEL  // -------------------------
//DEL  	$mArqMov = fopen("c:\\sistfat\\Retorno.txt","w");
//DEL  	fwrite($mArqMov,"Nome do Arquivo \r\n");
//DEL  	$mArquivo1 =  basename($_FILES['file']['name']);//$NRRetFatPag->FileUpload1->GetFileName();
//DEL  	$linhas=file($mArquivo1);
//DEL  	fwrite($mArqMov,"el silva $mArquivo1 \r\n");
//DEL      for($i=0;$i<count($linhas);$i++)
//DEL         {
//DEL  		fwrite($mArqMov, $linhas[$i]." \r\n");
//DEL         }
//DEL  	   fclose($mArqMov);
//DEL  // -------------------------



?>
