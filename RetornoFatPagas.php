<?php
//Include Common Files @1-D6580970
define("RelativePath", ".");
define("PathToCurrentPage", "/");
define("FileName", "RetornoFatPagas.php");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
//End Include Common Files

//Include Page implementation @3-8EF0CAE1
include_once(RelativePath . "/cabec.php");
//End Include Page implementation

class clsRecordNRRetFatPag { //NRRetFatPag Class @4-264DBDD9

//Variables @4-9E315808

    // Public variables
    public $ComponentType = "Record";
    public $ComponentName;
    public $Parent;
    public $HTMLFormAction;
    public $PressedButton;
    public $Errors;
    public $ErrorBlock;
    public $FormSubmitted;
    public $FormEnctype;
    public $Visible;
    public $IsEmpty;

    public $CCSEvents = "";
    public $CCSEventResult;

    public $RelativePath = "";

    public $InsertAllowed = false;
    public $UpdateAllowed = false;
    public $DeleteAllowed = false;
    public $ReadAllowed   = false;
    public $EditMode      = false;
    public $ds;
    public $DataSource;
    public $ValidatingControls;
    public $Controls;
    public $Attributes;

    // Class variables
//End Variables

//Class_Initialize Event @4-A3531609
    function clsRecordNRRetFatPag($RelativePath, & $Parent)
    {

        global $FileName;
        global $CCSLocales;
        global $DefaultDateFormat;
        $this->Visible = true;
        $this->Parent = & $Parent;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record NRRetFatPag/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "NRRetFatPag";
            $this->Attributes = new clsAttributes($this->ComponentName . ":");
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->uploadedfile = new clsButton("uploadedfile", $Method, $this);
            $this->MAX_FILE_SIZE = new clsControl(ccsHidden, "MAX_FILE_SIZE", "MAX_FILE_SIZE", ccsText, "", CCGetRequestParam("MAX_FILE_SIZE", $Method, NULL), $this);
            $this->lbltotfaturas = new clsControl(ccsLabel, "lbltotfaturas", "lbltotfaturas", ccsInteger, "", CCGetRequestParam("lbltotfaturas", $Method, NULL), $this);
            $this->Button1 = new clsButton("Button1", $Method, $this);
            $this->Button_Cancel = new clsButton("Button_Cancel", $Method, $this);
            if(!is_array($this->lbltotfaturas->Value) && !strlen($this->lbltotfaturas->Value) && $this->lbltotfaturas->Value !== false)
                $this->lbltotfaturas->SetText(0);
        }
    }
//End Class_Initialize Event

//Validate Method @4-5E498C02
    function Validate()
    {
        global $CCSLocales;
        $Validation = true;
        $Where = "";
        $Validation = ($this->MAX_FILE_SIZE->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate", $this);
        $Validation =  $Validation && ($this->MAX_FILE_SIZE->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @4-2DDCA8ED
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->MAX_FILE_SIZE->Errors->Count());
        $errors = ($errors || $this->lbltotfaturas->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @4-02DE6937
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button1";
            if($this->uploadedfile->Pressed) {
                $this->PressedButton = "uploadedfile";
            } else if($this->Button1->Pressed) {
                $this->PressedButton = "Button1";
            } else if($this->Button_Cancel->Pressed) {
                $this->PressedButton = "Button_Cancel";
            }
        }
        $Redirect = $FileName . "?" . CCGetQueryString("QueryString", array("ccsForm"));
        if($this->PressedButton == "Button_Cancel") {
            if(!CCGetEvent($this->Button_Cancel->CCSEvents, "OnClick", $this->Button_Cancel)) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "uploadedfile") {
                if(!CCGetEvent($this->uploadedfile->CCSEvents, "OnClick", $this->uploadedfile)) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "Button1") {
                if(!CCGetEvent($this->Button1->CCSEvents, "OnClick", $this->Button1)) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @4-D0219701
    function Show()
    {
        global $CCSUseAmp;
        global $Tpl;
        global $FileName;
        global $CCSLocales;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect", $this);


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error = "";
            $Error = ComposeStrings($Error, $this->MAX_FILE_SIZE->Errors->ToString());
            $Error = ComposeStrings($Error, $this->lbltotfaturas->Errors->ToString());
            $Error = ComposeStrings($Error, $this->Errors->ToString());
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", !$CCSUseAmp ? $this->HTMLFormAction : str_replace("&", "&amp;", $this->HTMLFormAction));
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow", $this);
        $this->Attributes->Show();
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->uploadedfile->Show();
        $this->MAX_FILE_SIZE->Show();
        $this->lbltotfaturas->Show();
        $this->Button1->Show();
        $this->Button_Cancel->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End NRRetFatPag Class @4-FCB6E20C



//Include Page implementation @2-ED94D4BE
include_once(RelativePath . "/rodape.php");
//End Include Page implementation



//Initialize Page @1-23FACE6D
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "";
$ComponentName = "";
$Attributes = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = FileName;
$Redirect = "";
$TemplateFileName = "RetornoFatPagas.html";
$BlockToParse = "main";
$TemplateEncoding = "CP1252";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-7FACF37D
CCSecurityRedirect("1;3", "");
//End Authenticate User

//Include events file @1-93880675
include("./RetornoFatPagas_events.php");
//End Include events file

//Before Initialize @1-E870CEBC
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeInitialize", $MainPage);
//End Before Initialize

//Initialize Objects @1-818B6249
$Attributes = new clsAttributes("page:");
$MainPage->Attributes = & $Attributes;

// Controls
$cabec = new clscabec("", "cabec", $MainPage);
$cabec->Initialize();
$NRRetFatPag = new clsRecordNRRetFatPag("", $MainPage);
$rodape = new clsrodape("", "rodape", $MainPage);
$rodape->Initialize();
$MainPage->cabec = & $cabec;
$MainPage->NRRetFatPag = & $NRRetFatPag;
$MainPage->rodape = & $rodape;

BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize", $MainPage);

$Charset = $Charset ? $Charset : "windows-1252";
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-593C2978
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView", $MainPage);
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, "CP1252");
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow", $MainPage);
$Attributes->Show();
//End Initialize HTML Template

//Execute Components @1-0FD76A12
$cabec->Operations();
$NRRetFatPag->Operation();
$rodape->Operations();
//End Execute Components

//Go to destination page @1-5B4F0261
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
    if ($CCSFormFilter)
        $Redirect = CCAddParam($Redirect, "FormFilter", $CCSFormFilter);
    header("Location: " . $Redirect);
    $cabec->Class_Terminate();
    unset($cabec);
    unset($NRRetFatPag);
    $rodape->Class_Terminate();
    unset($rodape);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-332E4589
$cabec->Show();
$NRRetFatPag->Show();
$rodape->Show();
$Tpl->block_path = "";
$Tpl->Parse($BlockToParse, false);
$main_block = $Tpl->GetVar($BlockToParse);
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeOutput", $MainPage);
if ($CCSEventResult) echo $main_block;
//End Show Page

//Unload Page @1-02F3553D
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload", $MainPage);
$cabec->Class_Terminate();
unset($cabec);
unset($NRRetFatPag);
$rodape->Class_Terminate();
unset($rodape);
unset($Tpl);
//End Unload Page


?>
