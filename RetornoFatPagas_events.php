<?php
//BindEvents Method @1-6D9068DE
function BindEvents()
{
    global $NRRetFatPag;
    global $CCSEvents;
    $NRRetFatPag->lbltotfaturas->CCSEvents["BeforeShow"] = "NRRetFatPag_lbltotfaturas_BeforeShow";
    $NRRetFatPag->Button1->CCSEvents["OnClick"] = "NRRetFatPag_Button1_OnClick";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//NRRetFatPag_lbltotfaturas_BeforeShow @23-C9FF0E68
function NRRetFatPag_lbltotfaturas_BeforeShow(& $sender)
{
    $NRRetFatPag_lbltotfaturas_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRRetFatPag; //Compatibility
//End NRRetFatPag_lbltotfaturas_BeforeShow

//Custom Code @24-2A29BDB7
// -------------------------
    // Write your own code here.

 $NRRetFatPag->lbltotfaturas->SetValue(CCGetSession('totfaturas'));
 CCSetSession('totfaturas','');
 
 
// -------------------------
//End Custom Code

//Close NRRetFatPag_lbltotfaturas_BeforeShow @23-B5C4379E
    return $NRRetFatPag_lbltotfaturas_BeforeShow;
}
//End Close NRRetFatPag_lbltotfaturas_BeforeShow

//DEL  // -------------------------
//DEL  	$mArqMov = fopen("c:\\sistfat\\Retorno.txt","w");
//DEL  	fwrite($mArqMov,"Nome do Arquivo \r\n");
//DEL  	$mArquivo1 =  basename($_FILES['file']['name']);//$NRRetFatPag->FileUpload1->GetFileName();
//DEL  	$linhas=file($mArquivo1);
//DEL  	fwrite($mArqMov,"el miguel $mArquivo1 \r\n");
//DEL      for($i=0;$i<count($linhas);$i++)
//DEL         {
//DEL  		fwrite($mArqMov, $linhas[$i]." \r\n");
//DEL         }
//DEL  	   fclose($mArqMov);
//DEL      /*
//DEL  	$mArquivo = "c:\\windows\\" . $mArquivo1;
//DEL  	$mArqMov = fopen("c:\\sistfat\\Retorno.txt","w");
//DEL  	fwrite($mArqMov,"Nome do Arquivo \r\n");
//DEL  	fwrite($mArqMov,"$mArquivo\r\n");
//DEL     	if (file_exists($mArquivo))
//DEL  	{
//DEL  		fwrite($mArqMov,"Arquivo existe\r\n");
//DEL  		fwrite($mArqMov,"$mArquivo\r\n");
//DEL  		$handle = fopen($mArquivo,"r");
//DEL  		$Tab_CADFAT = new clsDBfaturar();
//DEL  		$Tab_TABPBH = new clsDBfaturar();
//DEL  		$Tab_MOVFAT = new clsDBfaturar();
//DEL  		while (!feof($handle))
//DEL  		{
//DEL  			fwrite($mArqMov,"Lendo o Arquivo\r\n");
//DEL  			$mLinha = fgets($handle); //fread($handle, 400);
//DEL  			fwrite($mArqMov,"Linha Lida\r\n");
//DEL  			fwrite($mArqMov,"$mLinha\r\n");
//DEL  			fwrite($mArqMov,"Tamanho da Linha ".strlen($mLinha)."\r\n");
//DEL  
//DEL  
//DEL  			$l_codreg = substr($mLinha, 0, 1);
//DEL  			fwrite($mArqMov,"Tipo registro = $l_codreg\r\n");
//DEL  
//DEL  			$l_codoco = substr($mLinha, 108, 2);
//DEL  			fwrite($mArqMov,"Ocorrencia = $l_codoco\r\n");
//DEL  
//DEL  			$l_codfat = substr($mLinha, 116, 6);// 1=111 3=113
//DEL  			fwrite($mArqMov,"Fatura = $l_codfat\r\n");
//DEL  
//DEL  			$l_datpgt = substr($mLinha, 110, 2) . '/' . substr($mLinha, 112, 2) . '/' . '20' . substr ($mLinha, 114, 2);
//DEL  			fwrite($mArqMov,"Pagamento = $l_datpgt\r\n");
//DEL  
//DEL  			$l_valpgt = substr($mLinha, 253,13);
//DEL  			fwrite($mArqMov,"Valor 1 = $l_valpgt\r\n");
//DEL  			$l_valpgt = (float)$l_valpgt;
//DEL  			fwrite($mArqMov,"Valor 2 = $l_valpgt\r\n");
//DEL  			$l_valpgt /= 100;
//DEL  			fwrite($mArqMov,"Valor 3 = $l_valpgt\r\n");
//DEL  
//DEL  	
//DEL  			if (!(($l_codreg != '1') or ($l_codoco != '06' and $l_codoco != '16' and $l_codoco != '17' and $l_codoco != '03')))
//DEL  			{
//DEL  				fwrite($mArqMov,"Linha Fatura\r\n");
//DEL  				if ($l_valpgt > 0)
//DEL  				{
//DEL  					fwrite($mArqMov,"Fatura maior que zero\r\n");
//DEL  					$Tab_CADFAT->query("select valfat,mesref from cadfat where codfat = '$l_codfat'");
//DEL  					$Tab_CADFAT->next_record();
//DEL  					$l_mesref = $Tab_CADFAT->f("mesref");
//DEL  					$l_valfat = $Tab_CADFAT->f("valfat");
//DEL  					$l_valfat = str_replace("," , ".", $l_valfat);
//DEL  					$l_valfat = (float)$l_valfat;
//DEL  					
//DEL  					fwrite($mArqMov,"Mes de Referencia $l_mesref\r\n");
//DEL  					$Tab_TABPBH->query("select valpbh from tabpbh where mesref = '$l_mesref'");
//DEL  					$Tab_TABPBH->next_record();
//DEL  					$valpbh = $Tab_TABPBH->f("valpbh");
//DEL  					$valpbh = str_replace("," , ".", $valpbh);
//DEL  					fwrite($mArqMov,"Valor PBH $valpbh\r\n");
//DEL  	
//DEL  					$Tab_CADFAT->query("update cadfat set datpgt = to_date('$l_datpgt'),valpgt = $l_valpgt where codfat = '$l_codfat' ");
//DEL  					
//DEL  					$Tab_MOVFAT->query("update movfat set valpgt = round (qtdmed * equpbh * $valpbh, 2),datpgt = to_date('$l_datpgt') where codfat = '$l_codfat'");
//DEL  					if ($l_valfat != $l_valpgt)
//DEL  					{
//DEL  						fwrite($mArqMov,"Valor pago diferente do valor da fatura\r\n");
//DEL  						$Tab_MOVFAT->query("select codfat,grpser,subser from movfat where codfat = '$l_codfat'");
//DEL  						$Tab_MOVFAT->next_record();
//DEL  						$l_codfat = $Tab_MOVFAT->f("codfat"); 
//DEL  						$l_grpser = $Tab_MOVFAT->f("grpser");
//DEL  						$l_subser = $Tab_MOVFAT->f("subser");
//DEL  						$Tab_MOVFAT->query("update movfat set valpgt = valpgt + ($l_valpgt - $l_valfat) where codfat = '$l_codfat' and grpser = '$l_grpser' and subser = '$l_subser'");
//DEL  					}
//DEL  	
//DEL  				}
//DEL  				
//DEL  			}
//DEL  			else if ($l_codoco == '03')
//DEL  			{
//DEL  				// mensagem
//DEL  			}
//DEL  		}
//DEL  		$Tab_CADFAT->close();
//DEL  		$Tab_TABPBH->close();
//DEL  		$Tab_MOVFAT->close();
//DEL  		unset($Tab_CADFAT);
//DEL  		unset($Tab_TABPBH);
//DEL  		unset($Tab_MOVFAT);
//DEL  		fclose($mArqMov);
//DEL  		fclose($handle);
//DEL  	}*/
//DEL  // -------------------------

//NRRetFatPag_Button1_OnClick @7-2D869054
function NRRetFatPag_Button1_OnClick(& $sender)
{
    $NRRetFatPag_Button1_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $NRRetFatPag; //Compatibility
//End NRRetFatPag_Button1_OnClick

//Custom Code @8-2A29BDB7
// -------------------------
    //print_r($_FILES);
    $mArquivo1 =  $_FILES['uploadedfile']['tmp_name'];//$NRRetFatPag->FileUpload1->GetFileName();
    $linhas=file($mArquivo1);
	$totfaturas=0;

    $arq_valido = true;
	//Verificando validade do arquivo:
	//Registro 0 (header de Arquivo), campo C�digo do Banco na Compensa��o, de 1 a 3 tem que ser 001 (n�mero do Banco do Brasil);
	//Registro 1 (header de lote), campo Tipo de Opera��o, de 9 a 9 tem que ser T (arquivo retorno);
	//Registro 2 (segmento T), campo Segmento do Registro Detalhe, de 14 a 14 tem que ser T (arquivo retorno);
	//Registro 3 (segmento U), campo Segmento do Registro Detalhe, de 14 a 14 tem que ser U (arquivo retorno);
	if (!((substr($linhas[0], 0, 3) == '001') and (substr($linhas[1], 8, 1) == 'T') and (substr($linhas[2], 13, 1) == 'T') and (substr($linhas[3], 13, 1) == 'U')
	       and (substr($linhas[4], 13, 1) == 'T') and (substr($linhas[5], 13, 1) == 'U')))
	{
        $arq_valido = false;
	}
	//print $mArquivo1."<br>";
    //print $mArquivo1."<br>";
	if ($arq_valido)
	{

		for($i=2;$i<count($linhas);$i+=2)
		{
			if (substr($linhas[$i], 13, 1) == 'T') 
			{
				$Tab_CADFAT = new clsDBfaturar();
				$Tab_TABPBH = new clsDBfaturar();
				$Tab_MOVFAT = new clsDBfaturar();
				{
					$mLinha_SeguimentoT = $linhas[$i];
					$mLinha_SeguimentoU = $linhas[$i+1];

					$l_codfat = substr($mLinha_SeguimentoT, 58, 15);

					$l_datpgt = substr($mLinha_SeguimentoU, 137, 2) . '/' . substr($mLinha_SeguimentoU, 139, 2) . '/' . substr ($mLinha_SeguimentoU, 141, 4);
					$l_valpgt = substr($mLinha_SeguimentoU, 77,15);
					$l_valpgt = (float)$l_valpgt;
					$l_valpgt /= 100;

							if ($l_valpgt > 0)
							{
								$Tab_CADFAT->query("select valfat,mesref from cadfat where codfat = $l_codfat");
								$Tab_CADFAT->next_record();
								$l_mesref = $Tab_CADFAT->f("mesref");
								$l_valfat = $Tab_CADFAT->f("valfat");
								$l_valfat = str_replace("," , ".", $l_valfat);
								$l_valfat = (float)$l_valfat;

								$Tab_TABPBH->query("select valpbh from tabpbh where mesref = '$l_mesref'");
								$Tab_TABPBH->next_record();
								$valpbh = $Tab_TABPBH->f("valpbh");
								$valpbh = str_replace("," , ".", $valpbh);

								$Tab_CADFAT->query("update cadfat set datpgt = to_date('$l_datpgt'),valpgt = $l_valpgt where codfat = $l_codfat ");
						
								$Tab_MOVFAT->query("update movfat set valpgt = round (qtdmed * equpbh * $valpbh, 2),datpgt = to_date('$l_datpgt') where codfat = $l_codfat");

								$NRRetFatPag->lbltotfaturas->SetValue($NRRetFatPag->lbltotfaturas->GetValue()+1);
								$totfaturas=$totfaturas+1;

								if ($l_valfat != $l_valpgt)
								{
									$Tab_MOVFAT->query("select codfat,grpser,subser from movfat where codfat = $l_codfat");
									$Tab_MOVFAT->next_record();
									$l_codfat = $Tab_MOVFAT->f("codfat"); 
									$l_grpser = $Tab_MOVFAT->f("grpser");
									$l_subser = $Tab_MOVFAT->f("subser");
									$Tab_MOVFAT->query("update movfat set valpgt = valpgt + ($l_valpgt - $l_valfat) where codfat = $l_codfat and grpser = '$l_grpser' and subser = '$l_subser'");
								}
							}
				}
				$Tab_CADFAT->close();
				$Tab_TABPBH->close();
				$Tab_MOVFAT->close();
				unset($Tab_CADFAT);
				unset($Tab_TABPBH);
				unset($Tab_MOVFAT);
			}
		}
	} 
CCSetSession('totfaturas',$totfaturas);

// -------------------------
//End Custom Code

//Close NRRetFatPag_Button1_OnClick @7-AF2E71A0
    return $NRRetFatPag_Button1_OnClick;
}
//End Close NRRetFatPag_Button1_OnClick

//Page_BeforeShow @1-AC7EEE51
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $RetornoFatPagas; //Compatibility
//End Page_BeforeShow

//Custom Code @22-2A29BDB7
// -------------------------

    include("controle_acesso.php");
    $perfil=CCGetSession("IDPerfil");
	$permissao_requerida=array(44);
    controleacesso($perfil,$permissao_requerida,"acessonegado.php");

// -------------------------
//End Custom Code

//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow

//DEL  // -------------------------
//DEL  	$mArqMov = fopen("c:\\sistfat\\Retorno.txt","w");
//DEL  	fwrite($mArqMov,"Nome do Arquivo \r\n");
//DEL  	$mArquivo1 =  basename($_FILES['file']['name']);//$NRRetFatPag->FileUpload1->GetFileName();
//DEL  	$linhas=file($mArquivo1);
//DEL  	fwrite($mArqMov,"el ambrosio $mArquivo1 \r\n");
//DEL      for($i=0;$i<count($linhas);$i++)
//DEL         {
//DEL  		fwrite($mArqMov, $linhas[$i]." \r\n");
//DEL         }
//DEL  	   fclose($mArqMov);
//DEL  // -------------------------


//DEL  // -------------------------
//DEL  	$mArqMov = fopen("c:\\sistfat\\Retorno.txt","w");
//DEL  	fwrite($mArqMov,"Nome do Arquivo \r\n");
//DEL  	$mArquivo1 =  basename($_FILES['file']['name']);//$NRRetFatPag->FileUpload1->GetFileName();
//DEL  	$linhas=file($mArquivo1);
//DEL  	fwrite($mArqMov,"el silva $mArquivo1 \r\n");
//DEL      for($i=0;$i<count($linhas);$i++)
//DEL         {
//DEL  		fwrite($mArqMov, $linhas[$i]." \r\n");
//DEL         }
//DEL  	   fclose($mArqMov);
//DEL  // -------------------------



?>
