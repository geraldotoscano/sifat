#Sorter Class @0-B3FEF984

package Sorter;
use Common;

sub new {
  my ( $class, $ComponentName, $SorterName, $FileName ) = @_;
  my $self  = {}; 
  $self->{TargetName}     = $ComponentName;
  $self->{SorterName}     = $SorterName;
  $self->{FileName}       = $FileName;
  $self->{Visible}        = 1;
  $self->{CCSEventResult} = undef;
  $self->{CCSEvents}      = {};
  $self->{OrderColumn}    = CCGetParam($self->{TargetName} . "Order", "");
  $self->{OrderDirection} = CCGetParam($self->{TargetName} . "Dir", "");
  $self->{IsOn}           = ( $self->{OrderColumn} eq $self->{SorterName} ? 1 : 0 );
  $self->{IsAsc}          = ( !length($self->{OrderDirection}) || ( $self->{OrderDirection} eq "ASC" ) ? 1 : 0 );
  bless $self, $class;
  return $self;
}

sub Show {
  my $self = shift;
  
  $self->{EventResult} = CCGetEvent($self->{CCSEvents}, "BeforeShow" );
  return if(!$self->{Visible});

  my $QueryString  = CCGetQueryString("QueryString", ( $self->{TargetName} . "Page", "ccsForm" ));
  my $SorterBlock  = "Sorter " . $self->{SorterName};
  my $AscOnPath    = $SorterBlock . "/Asc_On";
  my $AscOffPath   = $SorterBlock . "/Asc_Off";
  my $DescOnPath   = $SorterBlock . "/Desc_On";
  my $DescOffPath  = $SorterBlock . "/Desc_Off";
  $QueryString     = CCAddParam( $QueryString, $self->{TargetName} . "Order", $self->{SorterName} );
  my $AscOnExist   = $Tpl->blockexists($AscOnPath);
  my $AscOffExist  = $Tpl->blockexists($AscOffPath);
  my $DescOnExist  = $Tpl->blockexists($DescOnPath);
  my $DescOffExist = $Tpl->blockexists($DescOffPath);

  if ( $self->{IsOn} ) {
    if ( $self->{IsAsc} ) {
      $self->{OrderDirection} = "DESC";
      if ( $AscOnExist )  { $Tpl->parse($AscOnPath, "false") };
      if ( $AscOffExist ) { $Tpl->setvar($AscOffPath, "") };
      if ( $DescOnExist ) { $Tpl->setvar($DescOnPath, "") };
      if ( $DescOffExist ) {
        $Tpl->setvar("Desc_URL", $self->{FileName} . "?" . CCAddParam($QueryString, $self->{TargetName} . "Dir", "DESC"));
        $Tpl->parse($DescOffPath, "false");
      }
    } else {
      $self->{OrderDirection} = "ASC";
      if ( $AscOnExist ) { $Tpl->setvar($AscOnPath, "") };
      if ( $AscOffExist ) {
        $Tpl->setvar("Asc_URL", $self->{FileName} . "?" . CCAddParam($QueryString, $self->{TargetName} . "Dir", "ASC"));
        $Tpl->parse($AscOffPath, "false");
      }
      if ( $DescOnExist )  { $Tpl->parse($DescOnPath, "false") };
      if ( $DescOffExist ) { $Tpl->setvar($DescOffPath, "") };
    }
  } else {
    $self->{OrderDirection} = "ASC";
    if ( $AscOnExist ) { $Tpl->setvar($AscOnPath, "") };
    if ( $AscOffExist ) {
      $Tpl->setvar("Asc_URL", $self->{FileName} . "?" . CCAddParam($QueryString, $self->{TargetName} . "Dir", "ASC"));
      $Tpl->parse($AscOffPath, "false");
    }
    if ( $DescOnExist ) { $Tpl->setvar($DescOnPath, "") };
    if ( $DescOffExist ) {
      $Tpl->setvar("Desc_URL", $self->{FileName} . "?" . CCAddParam($QueryString, $self->{TargetName} . "Dir", "DESC"));
      $Tpl->parse($DescOffPath, "false");
    }
  }
  $QueryString = CCAddParam($QueryString, $self->{TargetName} . "Dir", $self->{OrderDirection} );
  $Tpl->setvar("Sort_URL", $self->{FileName} . "?" . $QueryString);
  $Tpl->parse($SorterBlock, "false");
}

1;

#End Sorter Class

