<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="True" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="MeuEstilo1" wizardThemeVersion="3.0" needGeneration="0" isService="False">
	<Components>
		<Grid id="13" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" name="USUARIOS1" connection="Faturar" dataSource="TABOPE" pageSizeLimit="100" PathID="cabecUSUARIOS1">
			<Components>
				<Label id="14" fieldSourceType="DBColumn" dataType="Text" html="False" name="NOME" editable="False">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="DLookup" actionCategory="Database" id="20" typeOfTarget="Control" expression="&quot;desope || ' -   Matrícula:   ' || codope&quot;" domain="&quot;tabope&quot;" criteria="&quot;codope='&quot;.CCGetUserLogin().&quot;'&quot;" connection="Faturar" eventType="Server"/>
								<Action actionName="Custom Code" actionCategory="General" id="21"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="25" fieldSourceType="DBColumn" dataType="Text" html="True" name="lblmensagem_prox_tela">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="15"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
			</Components>
			<Events/>
			<TableParameters>
			</TableParameters>
			<JoinTables>
				<JoinTable id="19" tableName="TABOPE" posLeft="10" posTop="10" posWidth="115" posHeight="180"/>
			</JoinTables>
			<JoinLinks/>
			<Fields/>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="PHPTemplates" name="cabec.php" forShow="True" url="cabec.php" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Events" language="PHPTemplates" name="cabec_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="22" groupID="1"/>
		<Group id="23" groupID="2"/>
		<Group id="24" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Events/>
	<Attributes/>
	<Features/>
</Page>
