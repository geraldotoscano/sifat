#!c:\perl\bin\perl.exe
package cabec::clsGridcabecUSUARIOS1; #USUARIOS1 class @13-CA9FF407

#Inheritance for the clsGridUSUARIOS1 class @13-1A6D3B03
use Common;
use Sorter;
use Navigator;
use vars qw(@ISA);
@ISA = qw/cabec::clsUSUARIOS1DataSource Sorter Navigator/;
#End Inheritance for the clsGridUSUARIOS1 class

#Class clsGridUSUARIOS1 constructor @13-92E39AD7
    sub new {
        my $class = shift;
        my $self = {};
        bless $self, $class;
        $self->{PageSize} = "";
        $self->{SorterName} = "";
        $self->{SorterDirection} = "";
        $self->{PageNumber} = "";
        $self->{RowNumber} = "";
        $self->{ForceIteration} = 0;
        $self->{HasRecord} = 0;
        $self->{CCSEvents} = {};
        $self->{CCSEventResult} = "" ;
        # Grid Controls
        $self->{StaticControls} = "";
        $self->{RowControls} = "";
        $self->{ComponentName} = "USUARIOS1";
        $self->{ComponentType} = "Grid";
        $self->{Visible} = 1;
        $self->{RelativePath} = shift;
        $self->{Parent} = shift;
        $self->{AltRowControls} = undef;
        $self->{Errors} = clsErrors->new();
        $self->{ErrorBlock} = "Grid USUARIOS1";
        $self->{Attributes}       = clsAttributes->new($self->{ComponentName} . ":");
        $self->{DataSource} = cabec::clscabecUSUARIOS1DataSource->new($self);
        $self->{ds} = $self->{DataSource};
        $self->{PageSize} = CCGetParam($self->{ComponentName} . "PageSize", "");
        if(!is_numeric($self->{PageSize}) || !length($self->{PageSize})) {
            $self->{PageSize} = 10;
        } else {
            $self->{PageSize} = 0 + $self->{PageSize};
        }
        $self->{PageSize} = 100 if ($self->{PageSize} > 100);
        if ($self->{PageSize} == 0) {
            $self->{Errors}->addError("<p>Form: Grid " . $self->{ComponentName} . "<br>Error: (CCS06) Invalid page size.</p>");
        }
        $self->{PageNumber} = 0 + CCGetParam($self->{ComponentName} . "Page", 1);
        $self->{PageNumber} = 1 if ($self->{PageNumber} <= 0);
        $self->{NOME} = clsControl->new($ccsLabel, "NOME", "NOME", $ccsText, "", CCGetRequestParam("NOME", $ccsGet), $self);
        return $self;
    }
#End Class clsGridUSUARIOS1 constructor

#Class clsGridUSUARIOS1 destructor @13-EDA19AD1
    sub finalize {
        my $self  = shift;
        return if($self->{finalized});
        $self->{finalized} = 1;
        undef $self->{NOME};
        $self->{DataSource}->finalize() if($self->{DataSource});
    }
#End Class clsGridUSUARIOS1 destructor

#Initialize Method @13-4ABD6491
    sub Initialize {
        my $self = shift;
        return "" if ( !$self->{Visible} );
        tie $self->{DataSource}->{PageSize}, "Tie::Scalar::Ref", \$self->{PageSize};
        tie $self->{DataSource}->{AbsolutePage}, "Tie::Scalar::Ref", \$self->{PageNumber};
        $self->{DataSource}->SetOrder($self->{SorterName}, $self->{SorterDirection});
    }
#End Initialize Method

#Show Method @13-3F1CEC22
sub Show {
    my $self = shift;
    return if ( !$self->{Visible} );

    $self->{RowNumber} = 0;

    $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeSelect", $self);


    $self->{DataSource}->Prepare();
    $self->{DataSource}->Open();
    $self->{HasRecord} = $self->{DataSource}->has_next_record();
    $self->{IsEmpty} = !$self->{HasRecord};

    $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeShow", $self);
    return if(!$self->{Visible});
    $self->{Attributes}->Show();

    my $GridBlock = "Grid " . $self->{ComponentName};
    my $ParentPath = $Tpl->{block_path};
    $Tpl->{block_path} = $ParentPath . "/" . $GridBlock;

    if (!$self->{IsEmpty}) {
        while ($self->{ForceIteration} || (($self->{RowNumber} < $self->{PageSize}) &&  ($self->{HasRecord} = $self->{DataSource}->has_next_record()))) {
            $self->{RowNumber}++;
            if ($self->{HasRecord}) {
                $self->{DataSource}->next_record();
                $self->{DataSource}->SetValues();
            }
            $Tpl->{block_path} = $ParentPath . "/" . $GridBlock . "/Row";
            $self->{Attributes}->SetValue("rowNumber", $self->{RowNumber});
            $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeShowRow", $self);
            $self->{Attributes}->Show();
            $Tpl->{block_path} = $ParentPath . "/" . $GridBlock;
            $Tpl->parse("Row", "true");
        }
    } 

    my $errors = $self->GetErrors();
    if( length($errors)) {
        $Tpl->replaceblock("", $errors);
        $Tpl->{block_path} = $ParentPath;
        $self->{DataSource}->{sth} = undef;
        return;
    }
    $self->{NOME}->Show();
    $Tpl->parse();
    $Tpl->{block_path} = $ParentPath;
}
#End Show Method

#GetErrors Method @13-ADFFE9D2
sub GetErrors {
    my $self = shift;
    my $errors = "";
    $errors .= $self->{Errors}->ToString();
    $errors .= $self->{DataSource}->{Errors}->ToString();
    return $errors;
}
#End GetErrors Method

package cabec::clscabecUSUARIOS1DataSource; #USUARIOS1DataSource Class @13-20AC5A4F

#Inheritance @13-87880A52
use Common;
use vars qw(@ISA);
@ISA = qw/clsDBFaturar/;
#End Inheritance

#Class USUARIOS1DataSource Constructor @13-860F9C6E
sub new {
    my $class = shift;
    my $self  = {};
    $self = clsDBFaturar->new();
    $self->{CCSEvents}      = {};
    $self->{ErrorBlock}     = undef;
    $self->{CCSEventResult} = undef;
    $self->{Parent}         = shift;
    $self->{CountSQL}         = undef;
    $self->{wp} = undef;
    $self->{ErrorBlock} = "Grid USUARIOS1";
    bless $self, $class;
    return $self;
}
#End Class USUARIOS1DataSource Constructor

#Class USUARIOS1DataSource Destructor @13-DFEDDF5B
sub finalize {
    my $self  = shift;
    $self->SUPER::finalize();
}
#End Class USUARIOS1DataSource Destructor

#SetOrder Method @13-02EDF845
sub SetOrder {
    my ($self, $SorterName, $SorterDirection) = @_;
    $self->{Order} = "";
}
#End SetOrder Method

#Prepare Method @13-08A6DA10
sub Prepare {
    my $self = shift;
    $self->{Where} = '';
}
#End Prepare Method

#Open Method @13-8A353BDC
sub Open {
    my $self = shift;
    $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeBuildSelect", $self->{Parent});
    $self->{CountSQL} = "SELECT COUNT(*)\n\n" .
    "FROM TABOPE";
    $self->{SQL} = "SELECT \"*\" \n\n" .
    "FROM TABOPE {SQL_Where} {SQL_OrderBy}";
    $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeExecuteSelect", $self->{Parent});
    if ($self->{CountSQL}) {
        $self->{RecordsCount} = CCGetDBValue(CCBuildSQL($self->{CountSQL}, $self->{Where}, ""), $self);
     } else { 
        $self->{RecordsCount} = "CCS not counted";
    }
    $self->query(CCBuildSQL($self->{SQL}, $self->{Where}, $self->{Order}));
    $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "AfterExecuteSelect", $self->{Parent});
    $self->MoveToPage($self->{AbsolutePage});
}
#End Open Method

#SetValues Method @13-2652141A
sub SetValues {
    my $self = shift;
}
#End SetValues Method

package clscabec; #cabec class @1-19B8C389

#use Common @1-8F9E1A3C
    use Common;
#End use Common

#Export @1-61652706
    BEGIN {
        use Exporter();
        @clscabec::ISA = qw( Exporter );
        @clscabec::EXPORT = qw( $cabec );
    }
    use vars qw( $cabec );
#End Export

#Class_Initialize Event @1-9A1F8F78
    sub new {
        my $class               = shift;
        my $self                = {};
        $self->{RelativePath}   = shift;
        $self->{ComponentName}  = shift;
        $self->{ComponentType}  = "IncludablePage";
        $self->{Parent}         = shift;
        $self->{CCSEventResult} = undef;
        $self->{CCSEvents}      = {};
        $self->{Visible}        = 1;
        $self->{Attributes}     = $self->{Parent}->{Attributes};
        bless $self, $class;
        return $self;
    }
#End Class_Initialize Event

#Class_Terminate Event @1-EAB9A646
    sub finalize {
        my $self  = shift;
        return if($self->{finalized});
        $self->{finalized} = 1;
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeUnload", $self);
        if($self->{USUARIOS1}) {
            $self->{USUARIOS1}->finalize();
            undef $self->{USUARIOS1};
        }
    }
#End Class_Terminate Event

#BindEvents Method @1-2E2FD23E
    sub BindEvents {
        my $self = shift;
        $self->{USUARIOS1}->{NOME}->{CCSEvents}->{"BeforeShow"} = "clscabec::cabec_USUARIOS1_NOME_BeforeShow";
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "AfterInitialize", $self);
    }
#End BindEvents Method

#Operations Method @1-658ACBC2
    sub Operations {
        my $self = shift;
        return "" if ( !$self->{Visible} );
    }
#End Operations Method

#Initialize Method @1-E9015AE7
    sub Initialize {
        my $self = shift;
        return "" if ( !$self->{Visible} );
        $self->{FileName}         = "cabec.php";
        $self->{Redirect}         = "";
        $self->{TemplateFileName} = "cabec.html";
        $self->{BlockToParse}     = "main";
        $self->{PathToCurrentPage} = "/";
        $self->{TemplateEncoding} = "Cp1252";
        # Create Components
        $self->{USUARIOS1} = cabec::clsGridcabecUSUARIOS1->new($self->{RelativePath}, $self);
        $self->{USUARIOS1}->Initialize();
        $self->BindEvents();
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "OnInitializeView", $self);
    }
#End Initialize Method

#Show Method @1-2C929FAF
    sub Show {
        my $self = shift;
        my $block_path = $Tpl->{block_path};
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeShow", $self);
        return if ( !$self->{Visible} );
        $Tpl->LoadTemplate("/" . $self->{TemplateFileName}, $self->{ComponentName}, $self->{TemplateEncoding}, "remove");
        $Tpl->{block_path} = $self->{ComponentName};
        $self->{USUARIOS1}->Show();
        $self->{Attributes}->Show();
        $Tpl->parse();
        my $html = $Tpl->getvar();
        $Tpl->{block_path} = $block_path;
        $Tpl->setvar($self->{ComponentName}, $html);
        return;
    }
#End Show Method

#Include Event File @1-6E0F696E
require $PathToRoot . "cabec_events.pl";
#End Include Event File

1; #End cabec Class @1-368FFC69



