<?php
// //Events @1-F81417CB

//cabec_USUARIOS1_NOME_BeforeShow @14-C50FBA7F
function cabec_USUARIOS1_NOME_BeforeShow(& $sender)
{
    $cabec_USUARIOS1_NOME_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $cabec; //Compatibility
//End cabec_USUARIOS1_NOME_BeforeShow

//DLookup @20-82759A76
    global $DBFaturar;
    $Page = CCGetParentPage($sender);
    $ccs_result = CCDLookUp("desope || ' -   Matr�cula:   ' || codope", "tabope", "codope='".CCGetUserLogin()."'", $Page->Connections["Faturar"]);
    $Component->SetValue($ccs_result);
//End DLookup

//Custom Code @21-2A29BDB7
// -------------------------
    // Write your own code here.
// -------------------------
//End Custom Code
	//                            string number_format ( float $number , int $decimals , string $dec_point , string $thousands_sep )
	// (float)(str_replace(",", ".", $l_equpbh))

	$mJuros = (float)(str_replace(",", ".",CCGetSession("mTAXA_JUROS")));
	$mINSS = (float)(str_replace(",", ".",CCGetSession("mINSS")));
	$mINSSMIM = (float)(str_replace(",", ".",CCGetSession("mINSS_MIM")));
	$mISSQN = (float)(str_replace(",", ".",CCGetSession("mISSQN")));
	$mUFIR = (float)(str_replace(",", ".",CCGetSession("mUFIR")));
	$mHoje = CCGetSession("DataSist");

	$mParametro = $Component->GetValue();
	$mParametro .= " - [Taxa de Juros:".number_format($mJuros, 2,',','.')."% ao m�s] - [Taxa do INSS:".number_format($mINSS, 2,',','.')."%/M�nimo Cobrado:".number_format($mINSSMIM, 2,',','.')."] - [Taxa do ISSQN:".number_format($mISSQN, 2,',','.')."%] - [UFIR:".number_format($mUFIR, 2,',','.')."] - [Hoje:$mHoje]";
	$Component->SetValue($mParametro);
	
//Close cabec_USUARIOS1_NOME_BeforeShow @14-E30BB5BB
    return $cabec_USUARIOS1_NOME_BeforeShow;
}
//End Close cabec_USUARIOS1_NOME_BeforeShow

//cabec_USUARIOS1_lblmensagem_prox_tela_BeforeShow @25-44F8E8E6
function cabec_USUARIOS1_lblmensagem_prox_tela_BeforeShow(& $sender)
{
    $cabec_USUARIOS1_lblmensagem_prox_tela_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $cabec; //Compatibility
//End cabec_USUARIOS1_lblmensagem_prox_tela_BeforeShow

//Custom Code @15-2A29BDB7
// -------------------------
	if(isset($_SESSION['_MENSAGEM_PROX_TELA']) && $_SESSION['_MENSAGEM_PROX_TELA']!='')
	{
		$sender->SetValue($_SESSION['_MENSAGEM_PROX_TELA']);
		$_SESSION['_MENSAGEM_PROX_TELA']='';
	}
// -------------------------
//End Custom Code

//Close cabec_USUARIOS1_lblmensagem_prox_tela_BeforeShow @25-34C5C967
    return $cabec_USUARIOS1_lblmensagem_prox_tela_BeforeShow;
}
//End Close cabec_USUARIOS1_lblmensagem_prox_tela_BeforeShow


?>
