<?php
		function controleacesso($perfil,$permissao_requerida,$pagina_acesso_negado)
		  {
        	$Tabela = new clsDBfaturar();
			if( is_array($permissao_requerida))
			  {
			   $autorizou=false;
			   for($i=0;$i<count($permissao_requerida);$i++)
			     {
					$permissao=CCDLookUp("codfuncao","tabperfil_funcao","idperfil=".$perfil." and codfuncao=".$permissao_requerida[$i],$Tabela);
					if($permissao==$permissao_requerida[$i])
					  {
					   $autorizou=true;
					   break;
					  }
				 }
				if(!$autorizou)
				  {
		           header('Location: acessonegado.php');
				   exit();
				  }

			  }
	         else
			  {
				$permissao=CCDLookUp("codfuncao","tabperfil_funcao","idperfil=".$perfil." and codfuncao=".$permissao_requerida,$Tabela);
        
				if($permissao!=$permissao_requerida)
				  {
		           header('Location: '.$pagina_acesso_negado);
				   exit();
				  }
			  }

		  }

		function controleacesso_extra($perfil,$permissao_requerida,$pagina_acesso_negado,$permissao_requerida_extra,$pagina_acesso_extra)
		{
		 $Tabela = new clsDBfaturar();
         $autorizou=false;
		 if( is_array($permissao_requerida))
		   {
		   
		    for($i=0;$i<count($permissao_requerida);$i++)
		      {
			 	$permissao=CCDLookUp("codfuncao","tabperfil_funcao","idperfil=".$perfil." and codfuncao=".$permissao_requerida[$i],$Tabela);
				if($permissao==$permissao_requerida[$i])
				  {
				   $autorizou=true;
				   break;
				  }
			  }

		   }
          else
		   {
			$permissao=CCDLookUp("codfuncao","tabperfil_funcao","idperfil=".$perfil." and codfuncao=".$permissao_requerida,$Tabela);
        
			if($permissao==$permissao_requerida)
			  {
			   $autorizou=false;
			  }
		   }

		 if(!$autorizou)
		  {

            $permissao_requerida=$permissao_requerida_extra;
			if( is_array($permissao_requerida))
			  {
			   for($i=0;$i<count($permissao_requerida);$i++)
			     {
					$permissao=CCDLookUp("codfuncao","tabperfil_funcao","idperfil=".$perfil." and codfuncao=".$permissao_requerida[$i],$Tabela);
					if($permissao==$permissao_requerida[$i])
					  {
					   $autorizou=true;
					   break;
					  }
				 }
				if(!$autorizou)
				  {
		           header('Location: '.$pagina_acesso_negado);
				   exit();
				  }

			  }

           header('Location: '.$pagina_acesso_extra);
		   exit();
		  }
		
		
		}



		function verificaacesso($perfil,$permissao_requerida)
		  {
        	$Tabela = new clsDBfaturar();
			if( is_array($permissao_requerida))
			  {
			   $autorizou=false;
			   for($i=0;$i<count($permissao_requerida);$i++)
			     {
					$permissao=CCDLookUp("codfuncao","tabperfil_funcao","idperfil=".$perfil." and codfuncao=".$permissao_requerida[$i],$Tabela);
					if($permissao==$permissao_requerida[$i])
					  {
					   $autorizou=true;
					   break;
					  }
				 }
				if(!$autorizou)
				  {
		           return(false);
				   
				  }

			  }
	         else
			  {
				$permissao=CCDLookUp("codfuncao","tabperfil_funcao","idperfil=".$perfil." and codfuncao=".$permissao_requerida,$Tabela);
        
				if($permissao!=$permissao_requerida)
				  {
		           return(false);
				  }
			  }

			return(true);
		  }


?>