<?php

//DB PDO Class @0-04361F78
/*
 * Database Management for PHP
 *
 * Copyright (c) 1998-2000 NetUSE AG
 *                    Boris Erdmann, Kristian Koehntopp
 * Derived from db_mssql.php
 *
 * db_pdo.php
 */ 

class DB_PDO {
  public $DBDatabase = "";
  public $DBUser     = "";
  public $DBPassword = "";
  public $Persistent = false;
  public $Uppercase  = true;
  public $Options    = array();
  public $Encoding   = "";

  public $Binds = array();

  public $Link_ID  = 0;
  public $Query_ID = 0;
  public $Record   = array();
  public $Row      = 0;
  
  public $Errno    = 0;
  public $Error    = "";

  public $Auto_Free = 1;     ## set this to 1 to automatically free results
  public $Debug     = 0;     ## Set to 1 for debugging messages.
  public $Connected = false;
  
  
  /* public: constructor */
  function DB_Sql($query = "") {
      $this->query($query);
  }

  function try_connect() 
  {
    $this->Connected = true;
    $this->Query_ID  = 0;
    try {
      if($this->Persistent)
        $this->Link_ID = new PDO($this->DBDatabase);
      else
        $this->Link_ID = new PDO($this->DBDatabase, $this->DBUser, $this->DBPassword,
          array(PDO::ATTR_PERSISTENT => true));
    } catch (PDOException $e) {
      $this->Connected = false;
    }
    return $this->Connected;
  }

  function connect() {
    if (!$this->Connected) {
      $this->Connected = true;
      $this->Query_ID  = 0;
      try {
        if($this->Persistent)
          $this->Link_ID = new PDO($this->DBDatabase, $this->DBUser, $this->DBPassword);
        else
          $this->Link_ID = new PDO($this->DBDatabase, $this->DBUser, $this->DBPassword);
        foreach($this->Options as $option) {
          $this->Link_ID->setAttribute($option[0], $option[1]);
        }
        if ($this->Encoding) {
          $this->Link_ID->query("SET NAMES " . $this->Encoding);
        }
      } catch (PDOException $e) {
        $this->Halt("Cannot connect to PDO Database: " . $e->getMessage());
        $this->Connected = false;
      }
    }
  }

  function free_result() {
    $this->Query_ID = null;
  }

  function bind($name, $value, $scale, $datatype, $direction)
  {
    $this->Binds[] = array(
      "name" => $name,
      "value" => $value,
      "scale" => $scale,
      "datatype" => $datatype,
      "direction" => $direction
    );
  }

  function unBindAll()
  {
    $this->Binds = array();
  }

  function execute($Procedure, $RS = 0) 
  {
    if ($Procedure == "")
      return "";
    if (!$this->Link_ID)
      $this->connect();

    if($this->Query_ID = $this->Link_ID->prepare($Procedure)) 
    {
      foreach ($this->Binds as $paramNum => $values) 
      {
        if (!$this->Query_ID->bindParam($paramNum + 1, $this->Binds[$paramNum]["value"], $values["direction"] | $values["datatype"], $values["scale"])) {
          $this->Errno = 1;
          $this->Error = "Error in Bind";
          $this->Errors->addError("Database error: " . $this->Error);
          return false;
        }
      }
      if($result = $this->Query_ID->execute()) 
      {
        for ($i = 0; $i < $RS; $i++) {
          $this->Query_ID = $this->Query_ID->nextRowset();
        }
        $bi = 0;
        foreach ($this->Binds as $paramNum => $values) {
          $this->Record[$values["name"]] = $this->Binds[$paramNum]["value"];
          $this->Record[$bi++] = $this->Record[$values["name"]];
        }
        return;
      }
    
 
    } 
    if(!$this->Query_ID)
    {
      $this->Errno = 1;
      $this->Error = "SP $Procedure executing failed";
      $this->Errors->addError("Database error: " . $this->Error);

    }

  }

  function query($Query_String) 
  {
    
    /* No empty queries, please, since PHP4 chokes on them. */
    if ($Query_String == "")
      /* The empty query string is passed on from the constructor,
       * when calling the class without a query, e.g. in situations
       * like these: '$db = new DB_Sql_Subclass;'
       */
      return 0;

    if (!$this->Link_ID)
      $this->connect();
    
    if ($this->Debug)
      printf("Debug: query = %s<br>\n", $Query_String);
    if ($this->Query_ID) {
      $this->free_result();
    }
    $this->Query_ID = $this->Link_ID->query($Query_String);
    if (!$this->Query_ID) {
      $errInfo = $this->Link_ID->errorInfo();
      $this->Errno = $errInfo[0];
      $this->Error = $errInfo[2];
      $this->Errors->addError("Database error: " . $this->Error);
    }
    $this->Row = 0;
    return $this->Query_ID;
  }
  
  function next_record() {
    if (!$this->Query_ID) 
      return 0;
    
    $this->Record = $this->Query_ID->fetch();
    $this->Row += 1;
    
    $stat = is_array($this->Record);
    if (!$stat && $this->Auto_Free) {
      $this->free_result();
    }
    return $stat;
  }
  
  function seek($pos) {
    $i = 0;
    while($i < $pos && $this->Query_ID->fetch()) { $i++; }
    $this->Row += $i;
    return true;
  }

  function affected_rows() {
    return false;
  }
    
  function num_rows() {
    return ($this->Query_ID instanceof PDOStatement) ? $this->Query_ID->rowCount() : "";
    //return is_resource($this->Query_ID) ? db2_num_rows($this->Query_ID) : "";
  }
  
  function num_fields() {
    return $this->Query_ID->rowCount();
  }

  function nf() {
    return $this->num_rows();
  }
  
  function np() {
    print $this->num_rows();
  }
  
  function f($Name) {
    if($this->Uppercase) $Name = strtoupper($Name);
    return $this->Record && array_key_exists($Name, $this->Record) ? $this->Record[$Name] : "";
  }
  
  function p($Field_Name) {
    print $this->f($Field_Name);
  }

  function close()
  {
    if ($this->Query_ID) {
      $this->free_result();
    }
    if ($this->Connected && !$this->Persistent) {
      $this->Link_ID = null;
      $this->Connected = false;
    }
  }  

  function halt($msg) {
    printf("</td></tr></table><b>Database error:</b> %s<br>\n", $msg);
    printf("<b>PDO Error</b><br>\n");
    die("Session halted.");
  }
}

//End DB PDO Class


?>
