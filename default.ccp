<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Blueprint" wizardThemeVersion="3.0" needGeneration="0" isService="False">
	<Components>
		<IncludePage id="10" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<IncludePage id="7" name="rodape" page="rodape.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="PHPTemplates" name="default_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Code" language="PHPTemplates" name="default.php" forShow="True" url="default.php" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="13" groupID="1"/>
		<Group id="14" groupID="2"/>
		<Group id="15" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Events>
		<Event name="AfterInitialize" type="Server">
			<Actions>
				<Action actionName="Logout" actionCategory="Security" id="9" pageRedirects="True" parameterName="Logout" returnPage="default.ccp"/>
			</Actions>
		</Event>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="DLookup" actionCategory="Database" id="12" typeOfTarget="Session" expression="&quot;to_char(sysdate,'dd/mm/yyyy')&quot;" domain="&quot;dual&quot;" connection="Faturar" target="DataSist"/>
			</Actions>
		</Event>
	</Events>
	<Attributes/>
	<Features/>
</Page>
