<?php
//BindEvents Method @1-8B556F44
function BindEvents()
{
    global $CCSEvents;
    $CCSEvents["AfterInitialize"] = "Page_AfterInitialize";
    $CCSEvents["BeforeShow"] = "Page_BeforeShow";
}
//End BindEvents Method

//Page_AfterInitialize @1-44ABE027
function Page_AfterInitialize(& $sender)
{
    $Page_AfterInitialize = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $default; //Compatibility
//End Page_AfterInitialize

//Logout @9-4DB6A804
    if(strlen(CCGetParam("Logout", ""))) 
    {
        CCLogoutUser();
        global $Redirect;
        $Redirect = "default.php";
    }
//End Logout

//Close Page_AfterInitialize @1-379D319D
    return $Page_AfterInitialize;
}
//End Close Page_AfterInitialize

//Page_BeforeShow @1-27BCD354
function Page_BeforeShow(& $sender)
{
    $Page_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $default; //Compatibility
//End Page_BeforeShow

//DEL  // -------------------------
//DEL  //CCSetSession(name, value)
//DEL  	CCSetSession("Usuario", "CCGetUserLogin()");
//DEL  // -------------------------

    global $DBFaturar;
    $Page = CCGetParentPage($sender);
	global $Redirect;
/*
//DLookup @12-CCFABAE9
    global $DBFaturar;
    $Page = CCGetParentPage($sender);
    $ccs_result = CCDLookUp("to_char(sysdate,'dd/mm/yyyy')", "dual", "", $Page->Connections["Faturar"]);
    CCSetSession("DataSist", $ccs_result);
//End DLookup
    $ccs_result = CCDLookUp("to_char(sysdate,'HH24:MI:SS')", "dual", "", $Page->Connections["Faturar"]);
    CCSetSession("HoraSist", $ccs_result);

    $ccs_result = CCDLookUp("to_char(sysdate+5,'dd/mm/yyyy')", "dual", "", $Page->Connections["Faturar"]);
    CCSetSession("DataFatu", $ccs_result);
*/
   	$Tab_Param = new clsDBfaturar();
	$Tab_Param->query("	SELECT
							TAXA_JUROS
						FROM
							PARAM
					   	WHERE 
					    	VIGENCIA_JUROS  = (SELECT MAX(VIGENCIA_JUROS) FROM PARAM)"
					 );
	$Tab_Param->next_record();
    CCSetSession("mTAXA_JUROS", $Tab_Param->f("TAXA_JUROS"));

   	$Tab_Dat_Hor_MesRef = new clsDBfaturar();
   	$Tab_Dat_Hor_MesRef->query("SELECT 
	                      to_number(to_char(sysdate,'yyyy')) as ano_Atual,
	                      to_char(sysdate,'dd/mm/yyyy')      as DataSist,
						  to_char(sysdate,'HH24:MI:SS')      as HoraSist,
						  to_char(sysdate+5,'dd/mm/yyyy')    as DataFatu,
						  to_char(sysdate-to_number(to_char(sysdate,'dd')),'dd/mm/yyyy') as DataMesAnt
					   FROM 
					      dual");
   	$Tab_Dat_Hor_MesRef->next_record();
	CCSetSession("DataSist", $Tab_Dat_Hor_MesRef->f("DataSist"));
	/*
		A vari�vel de se��o abaixo � somente para testes. N�o se esquecer de Reti-
		r�-la ou coment�-l� para o funcionamento em produ��o.
	*/
	//CCSetSession("DataSist","10/05/2011");
	CCSetSession("HoraSist", $Tab_Dat_Hor_MesRef->f("HoraSist"));
	CCSetSession("DataFatu", $Tab_Dat_Hor_MesRef->f("DataFatu"));
	CCSetSession("DataMesAnt", $Tab_Dat_Hor_MesRef->f("DataMesAnt"));
	/*
		A vari�vel de se��o abaixo � somente para testes. N�o se esquecer de Reti-
		r�-la ou coment�-l� para o funcionamento em produ��o.
	*/
	//CCSetSession("DataMesAnt", '01/04/2011');

   	$Tab_Param->query("SELECT 
	                      TAXA_INSS,
						  VIGENCIA_INSS,
						  INSS_MIM
					   FROM 
					      PARAM_ALIQUOTA_INSS 
					   WHERE 
					      VIGENCIA_INSS  = (SELECT MAX(VIGENCIA_INSS) FROM PARAM_ALIQUOTA_INSS)");
   	$Tab_Param->next_record();
   	CCSetSession("mINSS", $Tab_Param->f("TAXA_INSS"));
   	CCSetSession("mVIGENCIA_INSS", $Tab_Param->f("VIGENCIA_INSS"));
    CCSetSession("mINSS_MIM", $Tab_Param->f("INSS_MIM"));

   	$Tab_Param_ISSQN = new clsDBfaturar();
   	$Tab_Param_ISSQN->query("SELECT 
	                      		TAXA_ISSQN,
						  		VIGENCIA_ISSQN
					   		 FROM 
					      		PARAM_ALIQUOTA_ISSQN
					   		 WHERE 
					      		VIGENCIA_ISSQN  = (SELECT MAX(VIGENCIA_ISSQN) FROM PARAM_ALIQUOTA_ISSQN)");
   	$Tab_Param_ISSQN->next_record();
   	CCSetSession("mISSQN", $Tab_Param_ISSQN->f("TAXA_ISSQN"));
   	CCSetSession("mVIGENCIA_ISSQN", $Tab_Param_ISSQN->f("VIGENCIA_ISSQN"));

	$mCodOpe = CCGetUserLogin();
    $ccs_result = CCDLookUp("CODUNI", "TABOPE", "CODOPE='$mCodOpe'", $Page->Connections["Faturar"]);
    CCSetSession("mCOD_UNI", $ccs_result);

    $ccs_result = CCDLookUp("DESCRI", "TABUNI", "CODUNI=$ccs_result", $Page->Connections["Faturar"]);
    CCSetSession("mDERCRI_UNI", $ccs_result);

	$mMESREF = substr(CCGetSession("DataSist"),3,7);
   	$Tab_UFIR = new clsDBfaturar();
	$Tab_UFIR->query("	SELECT
							VALPBH
						FROM
							TABPBH
					   	WHERE 
					    	MESREF  = '$mMESREF'");

   	unset($Tab_Param_ISSQN);
   	unset($Tab_Param);
	if ($Tab_UFIR->next_record())
	{
		CCSetSession("mUFIR", $Tab_UFIR->f("VALPBH"));

		// Proceder aqui a rotina de expurgo.
		// Gra�as a Deus Por Jesus Cristo.
		//$Tab_Ano_Corrente = new clsDBfaturar();
        //$Tab_Ano_Corrente->query("select DISTINCT max(to_number(substr(mesref,4,4))) as Ano_Corrente from cadfat");
		//$Tab_Ano_Corrente->next_record();
		//if ($Tab_Dat_Hor_MesRef->f("ano_Atual") != $Tab_Ano_Corrente->f("Ano_Corrente"))
		//if ($Tab_Dat_Hor_MesRef->f("ano_Atual") != $Tab_Ano_Corrente->f("Ano_Corrente"))
		//{
		//   $Redirect = "Expurgo.php";
		//}
        //unset($Tab_Dat_Hor_MesRef);
	}
	else
	{
/*		// Proceder aqui a rotina de expurgo.
		// Gra�as a Deus Por Jesus Cristo.
		$Tab_Ano_Corrente = new clsDBfaturar();
        $Tab_Ano_Corrente->query("select DISTINCT max(to_number(substr(mesref,4,4))) as Ano_Corrente from cadfat");
		$Tab_Ano_Corrente->next_record();
		if ($Tab_Dat_Hor_MesRef-f("ano_Atual") != $Tab_Ano_Corrente->f("Ano_Corrente"))
		{
		   $Redirect = "Expurgo.php";
		}


        // Calculo do total de faturas a serem expurgadas
   		$Tab_CadFat_Exp = new clsDBfaturar();
   		$Tab_CadFat_Exp->query("SELECT 
	    	                  		count(*) as Tot_Fat_Exp
						   		 FROM 
						      		cadfat
					   			 WHERE 
					      			valfat = valpgt and
									to_number(to_char(sysdate,'yyyy')) - to_number(to_char(datemi,'yyyy')) > 5
								 ORDER By
								    to_number(substr(mesref,4,1)),
									to_number(substr(mesref,1,2))"
								);
   		$Tab_CadFat_Exp->next_record();
		$mTotFatExp = 449 * $Tab_CadFat_Exp->f("Tot_Fat_Exp");

   		$Tab_MovFat_Exp = new clsDBfaturar();
   		$Tab_MovFat_Exp->query("SELECT 
	    	                  		count(*) as Tot_Fat_Exp
						   		 FROM 
						      		cadfat c,
									movfat m
					   			 WHERE 
					      			c.valfat = c.valpgt and
									to_number(to_char(sysdate,'yyyy')) - to_number(to_char(c.datemi,'yyyy')) > 5 and
									c.codfat = m.codfat
								 ORDER By
								    to_number(substr(c.mesref,4,1)),
									to_number(substr(c.mesref,1,2))"
								);
   		$Tab_MovFat_Exp->next_record();
		$mTotFatExp += $Tab_CadFat_Exp->f("Tot_Fat_Exp")*106;
		if (file_exists ( "c:\" ))
		{
		   if (disk_free_space ( "c:\" ) > $mTotFatExp)
		   {
		   }
		}
		else if (file_exists ( "d:\" ))
		{
		}
		else
		{
		// Mensagem -> imposs�vel proceder a rotina de expurgo. N�o unidade de 
		//             disco dispon�vel
		}
		// Termino Expurgo*/
        unset($Tab_UFIR);
        unset($Tab_Dat_Hor_MesRef);
		$Redirect = "ManutTabPBH_UFIR.php";
	}
	//unset($Tab_CadFat_Exp);
	//unset($Tab_MovFat_Exp);
	    
//Close Page_BeforeShow @1-4BC230CD
    return $Page_BeforeShow;
}
//End Close Page_BeforeShow


?>
