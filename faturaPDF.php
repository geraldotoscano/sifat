<?php
    define("RelativePath", ".");
    define("PathToCurrentPage", "/");
    define("FileName", "faturaPDF.php");

	error_reporting (E_ALL);
	//define("RelativePath", "../..");
	//define('FPDF_FONTPATH','/pdf/font/');
	require('pdf/fpdf.php');
	//include('pdf/fpdi.php');
	//require_once("pdf/fpdi_pdf_parser.php");
	//require_once("pdf/fpdf_tpl.php");
    include(RelativePath . "/Common.php");
    include(RelativePath . "/Template.php");
    include(RelativePath . "/Sorter.php");
    include(RelativePath . "/Navigator.php");
	include(RelativePath . "/util.php");
	include(RelativePath . "/dompdf/dompdf_config.inc.php");
	//include(RelativePath . "/Template.php");
	//include(RelativePath . "/Sorter.php");
	//include(RelativePath . "/Navigator.php");
class faturaPDF {	
	//$pdf= new fpdi();
    private $Fatura=false;
	private $Grupo=false;
	private $Divisao=false;
	private $templatefatura="modelos/Fatura.html";
	private $var_modelo=array(
	'razaosoc'=>'%razaosoc%',
    'dadosemissor'=>'%dadosemissor%',
    'emissao'=>'%emissao%',
    'referencia'=>'%referencia%',
    'numerofat'=>'%numerofat%',
    'vencimento'=>'%vencimento%',
    'inscnomecli'=>'%inscnomecli%',
    'endcli'=>'%endcli%',
    'cnpjcli'=>'%cnpjcli%',
    'localcoletacli'=>'%localcoletacli%',
    'tabservicos'=>'%tabservicos%',
    'valbruto'=>'%valbruto%',
    'inss'=>'%inss%',
    'valpago'=>'%valpago%',
	'email'=>'%email%',
	'lograd'=>'%lograd%',
	'comple'=>'%comple%',
	'desbai'=>'%desbai%',
	'descid'=>'%descid%',
	'estado'=>'%estado%',
	'codpos'=>'%codpos%',
	'telefo'=>'%telefo%',
	'fax'=>'%fax%',
	'cnpj'=>'%cnpj%',
	'descfat'=>'%descfat%',
	'juros'=>'%juros%',
	'data'=>'%data%'
	);
    private $sql_fatura='SELECT CODFAT, CADFAT.CODCLI AS CADFAT_CODCLI, to_char(DATVNC,\'dd/mm/yyyy\') DATVNC,to_char(DATPGT,\'dd/mm/yyyy\') DATPGT,to_char(sysdate,\'dd/mm/yyyy\') HOJE,floor(sysdate-DATVNC)  ATRASO, VALPGT, DESCLI, to_char(DATEMI,\'dd/mm/yyyy\') DATEMI, VALPBH, EXPORT, CADFAT.MESREF AS CADFAT_MESREF, DATPGT,
VALCOB, VALJUR, VALMUL, ESFERA, ISSQN, RET_INSS, VALFAT, DESCFAT, CADFAT.GERBLTO AS GERBLTO,CADCLI.LOGCOB,CADCLI.BAICOB,CADCLI.CIDCOB,CADCLI.ESTCOB,CADCLI.POSCOB,CADCLI.TELCOB
,CADCLI.CGCCPF , CADFAT.DESCFAT
FROM CADFAT,
CADCLI,
TABPBH
WHERE ( (CADCLI.CODCLI = CADFAT.CODCLI) AND (TABPBH.MESREF = CADFAT.MESREF) ) AND ( CADFAT.CODFAT = \'{CODFAT}\' ) ';
    private $sql_servicos='SELECT SUBRED, TABPBH.VALPBH AS TABPBH_VALPBH, QTDMED, EQUPBH, VALSER, MOVFAT.MESREF AS MOVFAT_MESREF,UNIDAD 
FROM MOVFAT,
TABPBH,
SUBSER
WHERE ( (TABPBH.MESREF = MOVFAT.MESREF) AND (SUBSER.SUBSER = MOVFAT.SUBSER) ) AND ( MOVFAT.CODFAT = \'{CODFAT}\' ) ';
    private $sql_empresa='select * from cademp';
    
    function __construct() {
        $this->Fatura   = new clsDBfaturar();
	    $this->Empresa  = new clsDBfaturar();
	    $this->Servicos  = new clsDBfaturar();
		$this->Geral  = new clsDBfaturar();
    }

	function substitui($variavel,$valor,$texto)
	{
     return(str_replace($variavel,$valor,$texto));
	}



	function relatorio($codfat,$em_PDF=true)
	{
		//global $titulo;
		//global $mesRef;
		//global $posCliente;
		//global $posFatura;
		//global $posEmissao;
		//global $posValFat;
		//global $posVenc;
		$fatura=file_get_contents($this->templatefatura);
        $consulta=$this->substitui('{CODFAT}',$codfat,$this->sql_fatura);
		$consulta_empresa=$this->sql_empresa;

//		print "\n<br> fatura=\"".$consulta."\"";
        $fatura_db=$this->Fatura;
		$empresa_db=$this->Empresa;
		$servicos_db=$this->Servicos;

		$fatura_db->query($consulta);
		if($fatura_db->next_record())
		  {
		   
		   $empresa_db->query($consulta_empresa);
		   if($empresa_db->next_record())
		     {
			  $fatura=$this->substitui($this->var_modelo['razaosoc'],$empresa_db->f('RAZSOC'),$fatura);

			  $dados_emp=$empresa_db->f('LOGRAD').' '.$empresa_db->f('COMPLE').' - '.$empresa_db->f('DESBAI').'<br>'
			             .$empresa_db->f('DESCID').','.$empresa_db->f('ESTADO').' '.$empresa_db->f('CODPOS').'<br>'
						 .'Telefone:'.$empresa_db->f('TELEFO').' - Fax:'.$empresa_db->f('FAX').'<br>'
						 .$empresa_db->f('EMAIL').'<br>'
						 .'CNPJ:16.673.998/0001-25';


              $fatura=$this->substitui($this->var_modelo['data'],date('d/m/Y'),$fatura);


              $fatura=$this->substitui($this->var_modelo['lograd'],$empresa_db->f('LOGRAD'),$fatura);

			  $fatura=$this->substitui($this->var_modelo['comple'],$empresa_db->f('COMPLE'),$fatura);

			  $fatura=$this->substitui($this->var_modelo['desbai'],$empresa_db->f('DESBAI'),$fatura);

			  $fatura=$this->substitui($this->var_modelo['descid'],$empresa_db->f('DESCID'),$fatura);

			  $fatura=$this->substitui($this->var_modelo['estado'],$empresa_db->f('ESTADO'),$fatura);

			  $fatura=$this->substitui($this->var_modelo['codpos'],$empresa_db->f('CODPOS'),$fatura);

			  $fatura=$this->substitui($this->var_modelo['telefo'],$empresa_db->f('TELEFO'),$fatura);

			  $fatura=$this->substitui($this->var_modelo['telefo'],$empresa_db->f('TELEFO'),$fatura);

			  $fatura=$this->substitui($this->var_modelo['fax'],$empresa_db->f('FAX'),$fatura);

			  $fatura=$this->substitui($this->var_modelo['email'],$empresa_db->f('EMAIL'),$fatura);

			  
			  $fatura=$this->substitui($this->var_modelo['cnpj'],'16.673.998/0001-25',$fatura);


              $fatura=$this->substitui($this->var_modelo['email'],$empresa_db->f('EMAIL'),$fatura);


			  $fatura=$this->substitui($this->var_modelo['dadosemissor'],$dados_emp,$fatura);

			  
			  $fatura=$this->substitui($this->var_modelo['descfat'],$fatura_db->f('DESCFAT'),$fatura);

			  $fatura=$this->substitui($this->var_modelo['inscnomecli'],$fatura_db->f('CADFAT_CODCLI').' - '.$fatura_db->f('DESCLI'),$fatura);

			  $fatura=$this->substitui($this->var_modelo['endcli'],$fatura_db->f('LOGCOB').'<br>'.$fatura_db->f('POSCOB').' - '.$fatura_db->f('CIDCOB').' - '.$fatura_db->f('ESTCOB'),$fatura);

			  $fatura=$this->substitui($this->var_modelo['cnpjcli'],$fatura_db->f('CGCCPF'),$fatura);

			  //$fatura=$this->substitui($this->var_modelo['localcoletacli'],$fatura_db->f('CGCCPF'),$fatura);
			  $fatura=$this->substitui($this->var_modelo['emissao'],$fatura_db->f('DATEMI'),$fatura);
			  $fatura=$this->substitui($this->var_modelo['referencia'],$fatura_db->f('CADFAT_MESREF'),$fatura);
			  $fatura=$this->substitui($this->var_modelo['numerofat'],$fatura_db->f('CODFAT'),$fatura);
			  $fatura=$this->substitui($this->var_modelo['vencimento'],$fatura_db->f('DATVNC'),$fatura);
			  
              $fatura=$this->substitui($this->var_modelo['valbruto'],$this->em_real($this->substitui('.',',',$fatura_db->f('VALFAT'))),$fatura);
			  $fatura=$this->substitui($this->var_modelo['inss'],$this->em_real($this->substitui('.',',',$fatura_db->f('RET_INSS'))),$fatura);

              $valorpago=$fatura_db->f('VALPGT');

			  $mVr_Multa=0;

	          $vfat  = floatval( str_replace(',','.',$fatura_db->f('VALFAT')  ) );
	          $mInss = floatval(str_replace(',','.',$fatura_db->f('RET_INSS') ) );


			  if($valorpago==0 || $valorpago=='')
			    {
				 $mDias=$fatura_db->f('ATRASO');

				 if($mDias<0)
 		           $mDias=0;
			     $mTaxa_Juros = floatval(str_replace(',','.',CCGetSession("mTAXA_JUROS")));
  
		         $mVr_Multa = floatval(  round((($vfat-$mInss)*($mTaxa_Juros/100))/30*$mDias ,2  ));
		         $l_valpgt = $vfat + $mVr_Multa - $mInss;

			     $mvalpgt=round(($vfat + $mVr_Multa - $mInss),2);

                 $valorpago=$mvalpgt;
                 


				}
               else
			    {
				 $mVr_Multa = floatval( str_replace(',','.',$fatura_db->f('VALJUR')  ) );
                 
				}

              $fatura=$this->substitui($this->var_modelo['juros'],$this->em_real($this->substitui('.',',',$mVr_Multa)),$fatura);
			  $fatura=$this->substitui($this->var_modelo['valpago'],$this->em_real($this->substitui('.',',',$valorpago)),$fatura);

			  $consulta=$this->substitui('{CODFAT}',$codfat,$this->sql_servicos);
			  $servicos_db->query($consulta);
			  $servicos_cli='';
			  while($servicos_db->next_record())
			    {
				 $servicos_cli=$servicos_cli.'<tr><td>'.$servicos_db->f('SUBRED').'</td><td>'.$servicos_db->f('QTDMED').'</td><td>'.$servicos_db->f('UNIDAD').'</td><td>'.$this->em_real($servicos_db->f('EQUPBH')).'</td><td>'.$this->em_real($servicos_db->f('VALSER')).'</td></tr>';


				}
             $fatura=$this->substitui($this->var_modelo['tabservicos'],$servicos_cli,$fatura);



			 }
           

		  }
//        print "\n<br> fatura \n<br>".$fatura."\n<br>";

        if($em_PDF==true)
		  {
			$dompdf = new DOMPDF();
		    $dompdf->load_html($fatura);
		    $dompdf->set_paper(DOMPDF_DEFAULT_PAPER_SIZE,"portrait");
		    $dompdf->render();
		    return($dompdf->output());
		  }
         else
		  {
           return($fatura);
		  }
		
   }


 function em_real($valor)
   {
    $valores=explode(',',$valor);
    if( count($valores)==1)
	  {
       $valores=array($valores[0],'00');
	  }

    if( count($valores)!=2)
	  {
       $valores=array('0','00');
	  }

   if(strlen($valores[1])!=2)
     {
      $valores[1]=$valores[1].'00';
      $valores[1]=substr($valores[1],0,2);
	 }
	 
   if(strlen($valores[0])>3)
     {
	  $aux_numero='';
          $cont_num=0;
	  for($i=strlen($valores[0])-1;$i>=0;$i--)
	    {
		 if($cont_num==3)
                    {
                     $cont_num=0;
                     $aux_numero='.'.$aux_numero;
                    }
		 $aux_numero=substr($valores[0],$i,1).$aux_numero;
                 $cont_num++;
		}

	  $valores[0]=$aux_numero;
	 }

   return('R$ '.$valores[0].','.$valores[1]);

   }


}
?>