<?php
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// Fun��es para facilitar o processamento arquivos texto de largura fixa      //
//                                                                            //
// Data Inicio      - 27/04/2012                                              //
// Ultima Alteracao - 02/05/2012                                              //
// Analista Responsavel - Alysson dos Santos                                  //
////////////////////////////////////////////////////////////////////////////////



//Estrutura para conter um campo em uma string de largura fixa
class campostring
{
  public $nome;          //O nome campo
  public $nomeupper;     //O nome do campo em maiusculo
  public $posicao;       //A posicao do campo
  public $tamanho;       //O tamanho do campo
  public $preencher;     //O caracter utilizado para preencher os espa�o vazio
  public $preencheresq;  //Indica se o espa�o vazio deve ser preenchido a esquerda


}

//Esta classe quebra uma string em diversos campos
//Util para processar arquivos de texto que possuem largura fixa

class TFiltrostring{
      private $campos=array();
      private $ultimaposicao=0;
      private $linhadados='';
      //Cache nome e cache valor servem para otimizar o acesso em caso
      //De campos repetidos
      private $cachenome=array('','','');
      private $cachevalor=array('','','');
      private $cachecampostring=array('','','');
      private $ultimocache=0;
      private $maximocache=0;
      private $tamanholinha=0;
      //Indica se � necess�rio recontar o tamanho da linha
      private $recontartamlinha=true;
      //Indica o tamanho da linha
      private $tamanholinhacontado=0;

      //Indica qual campo � utilizado para inscricao no registro de erros
      private $campoinscricaoregistro='';
      //Indica qual campo � utilizado para indicar o erro no cabecalho
      private $indicerrocabecalho='';

      //Utilizado para preencher os espa�os vazios
      private $preenchervazio=' ';
      private $nomeinsensitive=true;
	  
	  
	

//Remove todos os itens da lista e libera a memoria

public function limpalista()
{
 $this->campos=array();

 $this->zeracontador();
 $this->tamanholinha=0;
}
	  
	  
//Construtores e destrutores
function __construct() {
  $this->nomeinsensitive=true;
  $this->campos=array();
  $this->zeracontador();
  $this->limpacache();
  $this->preenchervazio=' ';

}	  


function __destruct() {

  $this->limpalista();

}

	  
	  
//Retorna a quantidade de caracteres que existem na linha
public function pega_tamanholinha()
{
  return($this->tamanholinha-1);
}

//Retorna a quantidade de caracteres que existem na linha
public function tamanho()
{
  return($this->pega_tamanholinha());
}



public function limpacache()
{
  $this->maximocache=3;
  $this->ultimocache=$this->maximocache;
  //Se aumentarem o tamanho do cache utilizar um for
  $this->cachenome[1]='';
  $this->cachenome[2]='';
  $this->cachenome[3]='';
  
  
  $this->cachecampostring[1]=new campostring();
  $this->cachecampostring[1]->nome='';
  $this->cachecampostring[1]->nomeupper='';
  $this->cachecampostring[1]->posicao=0;
  $this->cachecampostring[1]->tamanho=0;
  $this->cachecampostring[1]->preencher='';

  $this->cachecampostring[2]=new campostring();
  $this->cachecampostring[2]->nome='';
  $this->cachecampostring[2]->nomeupper='';
  $this->cachecampostring[2]->posicao=0;
  $this->cachecampostring[2]->tamanho=0;
  $this->cachecampostring[2]->preencher='';

  $this->cachecampostring[3]=new campostring();
  $this->cachecampostring[3]->nome='';
  $this->cachecampostring[3]->nomeupper='';
  $this->cachecampostring[3]->posicao=0;
  $this->cachecampostring[3]->tamanho=0;
  $this->cachecampostring[3]->preencher='';


}

//Garante que a linha tem o tamanho necessario
public function verificalinha()
{
  if( strlen($this->linhadados)<$this->tamanholinha )
    {
      while( strlen($this->linhadados)<$this->tamanholinha )
        {
          $this->linhadados=$this->linhadados.$this->preenchervazio;
        }
    }

   if( strlen($this->linhadados)>$this->tamanholinha )
     {
       $this->linhadados=substr($this->linhadados,0,$this->tamanholinha);
     }
}
	  
	  
//Procedure faz a atualiza��o dos valores em cache
public function recarregacache()
{
$i=0;

  for($i=1;$i<count($this->cachenome);$i++)
    {
      if($this->cachenome[$i]=='') continue;
      $this->cachevalor[$i]=substr($this->linhadados,$this->cachecampostring[$i]->posicao,$this->cachecampostring[$i]->tamanho);
    }

}
    



//Adiciona os campos a serem separados
//Definindo o tamanho e a posicao
public function addfiltro($nome,$tamanho,$posicao,$preencher='',$preencheresq=false)
{

  $pcampo=new campostring();

  $pcampo->nome=$nome;
  $pcampo->nomeupper=strtoupper(nome);
  $pcampo->posicao=$posicao;
  $pcampo->tamanho=$tamanho;
  $pcampo->preencher=$preencher;
  $pcampo->preencheresq=$preencheresq;
  $this->recontartamlinha=true;
  if( (posicao+tamanho)>$this->ultimaposicao )
    {
      $this->ultimaposicao=$posicao+$tamanho;
      $this->tamanholinha=$this->ultimaposicao;
    }
  $this->campos[]=$pcampo;
  $this->setvalor($nome,'');
  
}


//Zera o contador de campos
//Utilizar antes da funcao add
public function zeracontador()
{
  $this->ultimaposicao=0;
}
	  
	  
//Adiciona os campos a serem separados
//Definindo apenas o tamanho a posicao e calculada automaticamente
//Neste caso os campos devem ser inseridos na ordem
public function add($nome,$tamanho,$preencher='',$preencheresq=false)
{
  $pcampo=new campostring();
  
  $pcampo->nome=$nome;
  $pcampo->nomeupper=strtoupper($nome);
  $pcampo->posicao=$this->ultimaposicao;
  $pcampo->tamanho=$tamanho;
  $pcampo->preencher=$preencher;
  $pcampo->preencheresq=$preencheresq;
  $this->ultimaposicao=$this->ultimaposicao+$tamanho;
  $this->recontartamlinha=true;
  if( $this->tamanholinha<$this->ultimaposicao )
    {
      $this->tamanholinha=$this->ultimaposicao;
    }
  $this->campos[]=$pcampo;
  $this->setvalor($nome,'');
  
}

//Define a string a ser filtrada
public function setstring($novalinha)
{

  $this->linhadados=$novalinha;
  $this->recarregacache();

}


//Pega o nome do campo pelo seu indice
public function nomecampo($indice)
{ 
  return($this->campos[$indice]->nome);
}

//Pega o indice do campo pelo nome
public function indicecampo($nome)
{
  $pcampo='';
  $i=0;

  $result=-1;
  
  
  if($this->nomeinsensitive)
    {
      $nome=strtoupper($nome);
      
	  for($i=0;$i<count($this->campos);$i++)
        {
          $pcampo=$this->campos[$i];
          if($pcampo->nomeupper==$nome)
            {
              $result=$i;
              break;
            }
        }
    }
   else
    {
      for($i=0;$i<count($this->campos);$i++)
        {
          $pcampo=$this->campos[$i];
          if($pcampo->nome==$nome)
            {
              $result=$i;
              break;
            }
        }
    }
 return($result);
}
	  
	  
//Pega o valor do campo pelo nome
public function valor($nome)
{

  if(is_int($nome))
    {
	 return($this->valorindice($nome));
	}
  $pcampo='';
  $i=0;


  if($this->nomeinsensitive)
    {
      $nome=strtoupper($nome);
    }
	
  for($i=1;$i<=$this->maximocache;$i++)
    {
      if( $this->cachenome[$i]==nome)
        {
		  return($this->cachevalor[$i]);
        }
    }
  	
  $pcampo=$this->campos[$this->indicecampo($nome)];
  
  
  $result=substr($this->linhadados,$pcampo->posicao,$pcampo->tamanho);
  $result=$this->verificaresultpcampo($result,$pcampo);

  $this->ultimaposicao=1+($this->ultimaposicao % $this->maximocache);
  $this->cachenome[$this->ultimaposicao]=$nome;

  $this->atribuircampostring($pcampo,$this->cachecampostring[$this->ultimaposicao]);
  $this->cachevalor[$this->ultimaposicao]=$result;
  return($result);
}

//Pega o valor do campo pelo nome
public function valorindice($indice)
{
  $pcampo='';
  $pcampo=$this->campos[$indice];
  return($result=$this->verificaresultpcampo(substr($this->linhadados,$pcampo->posicao,$pcampo->tamanho),$pcampo));
}
	  
function verificaresultpcampo($result,$pcampo)
{
  if($result===false)
    {
	 $preencher=$this->preenchervazio;
	 if($pcampo->preencher<>'')
	   $preencher=$pcampo->preencher;
	 $result=str_pad('',$pcampo->tamanho,$preencher);
	}
   else
    {   
     if(strlen($result)<$pcampo->tamanho)
	   {
		 $preencher=$this->preenchervazio;
		 if($pcampo->preencher<>'')
		   $preencher=$pcampo->preencher;
		 $pad_type=STR_PAD_RIGHT;
		 
		 if($pcampo->preencheresq)
		   $pad_type=STR_PAD_LEFT;
		 
		 $result=str_pad($result,$pcampo->tamanho,$preencher);
	   
	   }
	}
  return($result);
}

//Determina um valor para um campo
public function setvalor($nome,$valor)
{
  $pcampo='';
  $i=0;
  $poscache=0;
  $aux='';
  $preenchedor='';
  $achoucampo=false;

  //Processamento de cache

  
  if($this->nomeinsensitive)
    {
      $nome=strtoupper($nome);
    }
  for($i=1;$i<=$this->maximocache;$i++)
    {
      if( $this->cachenome[$i]==$nome)
        {
          $pcampo=$this->cachecampostring[$i];
          $achoucampo=true;
          $poscache=$i;
        }
    }
//Fim do processamento de cache
  if($achoucampo==false)
     $pcampo=$this->campos[$this->indicecampo($nome)];

  $aux=$valor;

  if(strlen($aux)<$pcampo->tamanho)
    {
      if($pcampo->preencher<>'')
        {
          $preenchedor=$pcampo->preencher;
        }
       else
        {
          $preenchedor=$this->preenchervazio;
        }
       if($pcampo->preencheresq)
         {
           while(strlen($aux)<$pcampo->tamanho)
             {
               $aux=$preenchedor.$aux;
             }
         }
        else
         {
           while( strlen($aux)<$pcampo->tamanho)
             {
               $aux=$aux.$preenchedor;
             }

         }
    }

  if(strlen($aux)>$pcampo->tamanho)
    {
      $aux=substr($aux,0,$pcampo->tamanho);
    }

  $this->verificalinha();

  $this->linhadados=substr($this->linhadados,0,$pcampo->posicao).$aux.substr($this->linhadados,$pcampo->posicao+$pcampo->tamanho);
  
  //Processamento de cache

  if($achoucampo==false)
    {
      $this->ultimocache=1+($this->ultimocache % $this->maximocache);
      $this->cachenome[$this->ultimocache]=$nome;

      $this->atribuircampostring($pcampo,$this->cachecampostring[$this->ultimocache]);
      $this->cachevalor[$this->ultimocache]=$aux;
    }
   else
    {
      $this->cachevalor[$poscache]=$aux;
    }

}

	  


//Retonra a posicao do campo na linha
public function posicaocampo($nome)
{
  $pcampo='';
  $i=0;
  $aux='';
  $preenchedor='';
  $achoucampo=false;
  $poscache=0;

  //Processamento de cache
  $achoucampo=false;
  if($this->nomeinsensitive)
    {
      $nome=strtoupper($nome);
    }
  for($i=1;$i<=$this->maximocache;$i++)
    {
      if($this->cachenome[$i]==$nome)
        {
          $pcampo=$this->cachecampostring[$i];
          $achoucampo=true;
          $poscache=$i;
        }
    }
//Fim do processamento de cache

  if($achoucampo=false)
    $pcampo=$this->campos[$this->indicecampo($nome)];

  $result=$pcampo->posicao;

//Processamento de cache
  if($achoucampo==false)
    {
      $this->ultimaposicao=1+($this->ultimaposicao % $this->maximocache);
      $this->cachenome[$this->ultimaposicao]=$nome;

      $this->atribuircampostring($pcampo,$this->cachecampostring[$this->ultimaposicao]);
      $this->cachevalor[$this->ultimaposicao]=substr($this->linhadados,$pcampo->posicao,$pcampo->tamanho);
    }
  return($result);
}



//Retorna o tamanho do campo na linha
public function tamanhocampo($nome)
{
  $pcampo='';
  $i=0;
  $aux='';
  $preenchedor='';
  $achoucampo=false;
  $poscache=0;

  //Processamento de cache
  $achoucampo=false;
  if($this->nomeinsensitive)
    {
      $nome=strtoupper($nome);
    }
  for($i=1;$i<=$this->maximocache;$i++)
    {
      if($this->cachenome[$i]==$nome)
        {
          $pcampo=$this->cachecampostring[$i];
          $achoucampo=true;
          $poscache=$i;
        }
    }
//Fim do processamento de cache

  if($achoucampo=false)
    $pcampo=$this->campos[$this->indicecampo($nome)];

  $result=$pcampo->tamanho;

//Processamento de cache
  if($achoucampo==false)
    {
      $this->ultimaposicao=1+($this->ultimaposicao % $this->maximocache);
      $this->cachenome[$this->ultimaposicao]=$nome;

      $this->atribuircampostring($pcampo,$this->cachecampostring[$this->ultimaposicao]);
      $this->cachevalor[$this->ultimaposicao]=substr($this->linhadados,$pcampo->posicao,$pcampo->tamanho);
    }
  return($result);
}

//Imprime a estrutuda de dados internos para conferencia
public function print_diagrama()
{

  print_r($this->campos);
}



public function atribuircampostring(&$origem,&$destino)
{

 $destino->nome=$origem->nome;
 $destino->nomeupper=$origem->nomeupper;
 $destino->posicao=$origem->posicao;
 $destino->tamanho=$origem->tamanho;
 $destino->preencher=$origem->preencher;
 $destino->preencheresq=$origem->preencheresq;

}

//Pega a String formatada
public function getstring()
{
 $i=0;
 $result='';
 for($i=0;$i<count($this->campos);$i++)
   {
    $result=$result.$this->valor($i);
   }
 return($result);  
}



}

?>
