function cdd_menu202499(){//////////////////////////Start Menu Data/////////////////////////////////

//**** NavStudio, (c) 2004, OpenCube Inc.,  -  www.opencube.com ****
//Note: This data file may be manually customized outside of the visual interface.

    //Unique Menu Id
	this.uid = 202499


/**********************************************************************************************

                               Icon Images

**********************************************************************************************/


    //Inline positioned icon images (flow with the item text)

	this.rel_icon_image0 = "squares_0.gif"
	this.rel_icon_rollover0 = "squares_0_hl.gif"
	this.rel_icon_image_wh0 = "13,8"

	this.rel_icon_image1 = "corner_arrow.gif"
	this.rel_icon_rollover1 = "corner_arrow.gif"
	this.rel_icon_image_wh1 = "13,10"

	this.rel_icon_image2 = "3d-cube.gif"
	this.rel_icon_rollover2 = "3d-cube.gif"
	this.rel_icon_image_wh2 = "13,13"

	this.rel_icon_image3 = "circle_0.gif"
	this.rel_icon_rollover3 = "circle_0.gif"
	this.rel_icon_image_wh3 = "16,14"

	this.rel_icon_image4 = "lock.gif"
	this.rel_icon_rollover4 = "lock.gif"
	this.rel_icon_image_wh4 = "7,8"



    //Absolute positioned icon images (x,y positioned)

	this.abs_icon_image0 = "arrows_6.gif"
	this.abs_icon_rollover0 = "arrows_6_hl.gif"
	this.abs_icon_image_wh0 = "10,8"
	this.abs_icon_image_xy0 = "-13,4"




/**********************************************************************************************

                              Global - Menu Container Settings

**********************************************************************************************/


	this.menu_background_color = "#dce0d6"
	this.menu_border_color = "#7d4535"
	this.menu_border_width = 1
	this.menu_padding = "4,5,4,5"
	this.menu_border_style = "solid"
	this.divider_caps = false
	this.divider_width = 1
	this.divider_height = 1
	this.divider_background_color = "#000000"
	this.divider_border_style = "none"
	this.divider_border_width = 0
	this.divider_border_color = "#000000"
	this.menu_is_horizontal = false
	this.menu_width = 100
	this.menu_xy = "-80,-2"
	this.menu_gradient = "progid:DXImageTransform.Microsoft.gradient(gradientType=0, startColorstr=#ffffff, endColorstr=#eee791)"
	this.menu_animation = "progid:DXImageTransform.Microsoft.RandomDissolve(duration=0.5)"
	this.menu_scroll_direction = 1
	this.menu_scroll_reverse_on_hide = true
	this.menu_scroll_delay = 0
	this.menu_scroll_step = 5




/**********************************************************************************************

                              Global - Menu Item Settings

**********************************************************************************************/


	this.icon_rel = 3
	this.menu_items_background_color_roll = "#e3ddac"
	this.menu_items_text_color = "#333333"
	this.menu_items_text_decoration = "none"
	this.menu_items_font_family = "Arial"
	this.menu_items_font_size = "11px"
	this.menu_items_font_style = "normal"
	this.menu_items_font_weight = "normal"
	this.menu_items_text_align = "left"
	this.menu_items_padding = "4,5,4,5"
	this.menu_items_border_style = "solid"
	this.menu_items_border_color = "#000000"
	this.menu_items_border_width = 0
	this.menu_items_width = 90
	this.menu_items_text_decoration_roll = "underline"
	this.menu_items_text_color_roll = "#db4448"




/**********************************************************************************************

                              Main Menu Settings

**********************************************************************************************/


        this.menu_items_background_color_roll_main = "#e3ddac"
        this.menu_items_text_color_roll_main = "#db4448"
        this.menu_items_width_main = 91
        this.menu_is_horizontal_main = true

        this.item0 = "Cadastrar"
        this.item1 = "Clientes"
        this.item2 = "Servi�os"
        this.item3 = "Faturamento"
        this.item4 = "Importa��o / Exporta��o"
        this.item5 = "Par�metros"
        this.item6 = "Sair"

        this.item_width2 = 89
        this.icon_rel4 = 3
        this.item_width4 = 173
        this.icon_rel6 = 3
        this.item_width6 = 126

        this.url1 = "CadCadCli.php"
        //this.url2 = "CadSer.php"
        this.url6 = "FechaSessao.php"




/**********************************************************************************************

                              Sub Menu Settings

**********************************************************************************************/


    //Sub Menu 0

        this.menu_xy0 = "-97,5"
        this.menu_width0 = 167

        this.item0_0 = "Divis�es"


            //Sub Menu 0_0

                this.menu_xy0_0 = "11,-26"
                this.menu_width0_0 = 150

                this.item0_0_0 = "Visualiza��o"
                this.item0_0_1 = "Rela��o de Clientes"

                this.url0_0_0 = "CadTabUni.php"
                this.url0_0_1 = "ManutTabUniRelCli.php"



        this.item0_1 = "Distritos"


            //Sub Menu 0_1

                this.menu_xy0_1 = "11,-20"
                this.menu_width0_1 = 145

                this.item0_1_0 = "Visualiza��o"
                this.item0_1_1 = "Rela��o de Clientes"

                this.url0_1_0 = "ManutTabDis.php"
                this.url0_1_1 = "ManutTabDisRelCli.php"



        this.item0_2 = "Atividades"


            //Sub Menu 0_2

                this.menu_xy0_2 = "11,-19"
                this.menu_width0_2 = 138

                this.item0_2_0 = "Visualiza��o"
                this.item0_2_1 = "Rela��o de Clientes"

                this.url0_2_0 = "CadGRPAti.php"
                this.url0_2_1 = "ManutGRPAtiRel.php"



        this.item0_3 = "Sub Atividades"


            //Sub Menu 0_3

                this.menu_xy0_3 = "11,-22"
                this.menu_width0_3 = 138

                this.item0_3_0 = "Visualiza��o"
                this.item0_3_1 = "Rela��o de Clientes"

                this.url0_3_0 = "CadSubAti.php"
                this.url0_3_1 = "ManutSubAtiRelCli.php"



        this.item0_4 = "Servi�os"


            //Sub Menu 0_4

                this.menu_xy0_4 = "10,-20"
                this.menu_width0_4 = 167

                this.item0_4_0 = "Visualiza��o"
                this.item0_4_1 = "Rela��o de Clientes"

                this.url0_4_0 = "CadGrpSer.php"
                this.url0_4_1 = "ManutGrpSerRelCli.php"



        this.item0_5 = "Subservi�os"


            //Sub Menu 0_5

                this.menu_xy0_5 = "11,-18"
                this.menu_width0_5 = 167

                this.item0_5_0 = "Visualiza��o"
                this.item0_5_1 = "Rela��o de Clientes"

                this.url0_5_0 = "CadSubServico.php"
                this.url0_5_1 = "ManutSubServicoRelCli.php"



        this.item0_6 = "UFIR's"


            //Sub Menu 0_6

                this.menu_xy0_6 = "10,-19"
                this.menu_width0_6 = 167

                this.item0_6_0 = "Visualiza��o"

                this.url0_6_0 = "CadTabPBH.php"



        this.item0_7 = "Tabela de Pre�os"


            //Sub Menu 0_7

                this.menu_xy0_7 = "10,-16"
                this.menu_width0_7 = 167

                this.item0_7_0 = "Visualiza��o"
                this.item0_7_1 = "Inclus�o Mensal"

                this.url0_7_0 = "CadTabPreco.php"
                this.url0_7_1 = "ManurEquSerIncMens.php"



        this.item0_8 = "Frequ�ncia de Coletas"


            //Sub Menu 0_8

                this.menu_xy0_8 = "11,-17"
                this.menu_width0_8 = 167

                this.item0_8_0 = "Visualiza��o"

                this.url0_8_0 = "CadTabCol.php"



        this.item0_9 = "Empresa"
        this.item0_10 = "Funcoes de Acesso"
        this.item0_11 = "Perfis de Operadores"
        this.item0_12 = "Alterar minha senha"
        this.item0_13 = "Operadores do Sistema"
        this.item0_14 = "Banco"

        this.url0_9 = "CadCadEmp.php"
        this.url0_10 = "CadTabFuncao.php"
        this.status0_10 = "CadTabFuncao.php"
        this.url0_11 = "CadPerfil.php"
        this.url0_12 = "ModificarSenha.php"
        this.url0_13 = "CadTabOpe.php"
        this.url0_14 = "CadTabItau.php"



    //Sub Menu 1

        this.menu_width1 = 117




    //Sub Menu 2

        this.menu_width2 = 92

        this.item2_0 = "Cadastrar"
        this.item2_1 = "Relat�rio"

        this.url2_0 = "CadSer.php"
        this.url2_1 = "RelServicosCliente.php"



    //Sub Menu 3

        this.menu_xy3 = "-89,6"
        this.menu_width3 = 87

        this.item3_0 = "Coletivo"


            //Sub Menu 3_0

                this.menu_xy3_0 = "7,-20"
                this.menu_width3_0 = 253

                this.item3_0_0 = "Faturamento coletivo mensal"
                this.item3_0_1 = "Faturamento coletivo de faturas atrasadas"

                this.url3_0_0 = "FatCol.php"
                this.url3_0_1 = "GerFatAtraso.php"
                this.status3_0_1 = "GerFatAtraso.php"



        this.item3_1 = "Faturas"


            //Sub Menu 3_1

                this.menu_xy3_1 = "7,-23"
                this.menu_width3_1 = 210

                this.item3_1_0 = "Visualiza��o"
                this.item3_1_1 = "Pagamento"
                this.item3_1_2 = "Cancelar Pagamento(Estorno)"
                this.item3_1_3 = "Excluir (Cancelar)"
                this.item3_1_4 = "Excluir (Cancelar) V�rias"
                this.item3_1_5 = "Recuperar Fatura"
                this.item3_1_6 = "Faturas que n�o Geram Boleto"
                this.item3_1_7 = "Faturas de recobran�a de atrasos"

                this.url3_1_0 = "PagtoFatu.php?opcao=%271%27"
                this.url3_1_1 = "PagtoFatu.php?opcao=%272%27"
                this.url3_1_2 = "PagtoFatu.php?opcao=%273%27"
                this.url3_1_3 = "PagtoFatu.php?opcao=%274%27"
                this.url3_1_4 = "PagtoFatu_Dados_Canc_Lote.php"
                this.url3_1_5 = "PagtoFatu.php?opcao=%275%27"
                this.url3_1_6 = "FafSemBol.php"
                this.url3_1_7 = "CadFatAtrs.php"
                this.status3_1_7 = "CadFatAtrs.php"



        this.item3_2 = "Extrato"
        this.item3_3 = "Relat�rios"


            //Sub Menu 3_3

                this.menu_xy3_3 = "7,-75"
                this.menu_width3_3 = 202

                this.item3_3_0 = "Por Emiss�o"


                    //Sub Menu 3_3_0

                        this.menu_xy3_3_0 = "-103,-31"
                        this.menu_width3_3_0 = 202

                        this.item3_3_0_0 = "Por Inscri��o"
                        this.item3_3_0_1 = "Por Divis�o"
                        this.item3_3_0_2 = "Por Distrito"
                        this.item3_3_0_3 = "Por Atividade"
                        this.item3_3_0_4 = "Por Sub Atividade"
                        this.item3_3_0_5 = "Por Servi�o"
                        this.item3_3_0_6 = "Por Subservi�o"

                        this.url3_3_0_0 = "RelEmisInsc.php?opcao=411"
                        this.url3_3_0_1 = "RelEmisDivi.php?opcao=412"
                        this.url3_3_0_2 = "RelEmisDistr.php?opcao=413"
                        this.url3_3_0_3 = "RelEmisAtiv.php?opcao=414"
                        this.url3_3_0_4 = "RelEmisSubA.php?opcao=415"
                        this.url3_3_0_5 = "RelEmisServ.php"
                        this.url3_3_0_6 = "RelEmisSubS.php"



                this.item3_3_1 = "Por Receita"


                    //Sub Menu 3_3_1

                        this.menu_xy3_3_1 = "-105,-25"
                        this.menu_width3_3_1 = 202

                        this.item3_3_1_0 = "Por Inscri��o"
                        this.item3_3_1_1 = "Por Divis�o"
                        this.item3_3_1_2 = "Por Distrito"
                        this.item3_3_1_3 = "Por Atividade"
                        this.item3_3_1_4 = "Por Sub Atividade"
                        this.item3_3_1_5 = "Por Servi�o"
                        this.item3_3_1_6 = "Por Subservi�o"

                        this.url3_3_1_0 = "RelReceInsc.php?opcao=421"
                        this.url3_3_1_1 = "RelReceDivi.php?opcao=422"
                        this.url3_3_1_2 = "RelReceDistr.php?opcao=423"
                        this.url3_3_1_3 = "RelReceAtiv.php?opcao=424"
                        this.url3_3_1_4 = "RelReceSubA.php?opcao=425"
                        this.url3_3_1_5 = "RelReceServ.php?opcao=426"
                        this.url3_3_1_6 = "RelReceSubS.php?opcao=427"



                this.item3_3_2 = "Por Emiss�o/Arrecada��o"


                    //Sub Menu 3_3_2

                        this.menu_xy3_3_2 = "-17,-26"
                        this.menu_width3_3_2 = 202

                        this.item3_3_2_0 = "Por Inscri��o"
                        this.item3_3_2_1 = "Por Divis�o"
                        this.item3_3_2_2 = "Por Distrito"
                        this.item3_3_2_3 = "Por Atividade"
                        this.item3_3_2_4 = "Por Sub Atividade"
                        this.item3_3_2_5 = "Por Servi�o"
                        this.item3_3_2_6 = "Por Subservi�o"
                        this.item3_3_2_7 = "Total de arrecada��o por m�s"

                        this.url3_3_2_0 = "RelArrecInsc.php"
                        this.url3_3_2_1 = "RelArrecDivi.php"
                        this.url3_3_2_2 = "RelArrDist.php"
                        this.url3_3_2_3 = "RelArrecAtiv.php"
                        this.url3_3_2_4 = "RelArrecSubAti.php"
                        this.url3_3_2_5 = "RelArrecServ.php"
                        this.url3_3_2_6 = "RelArrecSubSer.php"
                        this.url3_3_2_7 = "RelArrecMesAno.php"



                this.item3_3_3 = "Por Arrecada��o"


                    //Sub Menu 3_3_3

                        this.menu_xy3_3_3 = "-79,-26"
                        this.menu_width3_3_3 = 202

                        this.item3_3_3_0 = "Por Inscri��o"
                        this.item3_3_3_1 = "Por Divis�o"
                        this.item3_3_3_2 = "Por Distrito"
                        this.item3_3_3_3 = "Por Atividade"
                        this.item3_3_3_4 = "Por Sub Atividade"
                        this.item3_3_3_5 = "Por Servi�o"
                        this.item3_3_3_6 = "Por Subservi�o"

                        this.url3_3_3_0 = "RelSoArrecInsc.php"
                        this.url3_3_3_1 = "RelSoArrecDivi.php"
                        this.url3_3_3_2 = "RelSoArrDist.php"
                        this.url3_3_3_3 = "RelSoArrecAtiv.php"
                        this.url3_3_3_4 = "RelSoArrecSubAti.php"
                        this.url3_3_3_5 = "RelSoArrecServ.php"
                        this.url3_3_3_6 = "RelSoArrecSubSer.php"



                this.item3_3_4 = "Por D�bito"


                    //Sub Menu 3_3_4

                        this.menu_xy3_3_4 = "-110,-26"
                        this.menu_width3_3_4 = 202

                        this.item3_3_4_0 = "Por Inscri��o"
                        this.item3_3_4_1 = "Por Divis�o"
                        this.item3_3_4_2 = "Por Distrito"
                        this.item3_3_4_3 = "Por Atividade"
                        this.item3_3_4_4 = "Por Sub Atividade"
                        this.item3_3_4_5 = "Por Servi�o"
                        this.item3_3_4_6 = "Por Subservi�o"
                        this.item3_3_4_7 = "Por Cliente"


                            //Sub Menu 3_3_4_7

                                this.menu_xy3_3_4_7 = "-573,-94"
                                this.menu_width3_3_4_7 = 359

                                this.item3_3_4_7_0 = "Totaliza��o de D�bitos de Todos os Clientes (Analitico)"
                                this.item3_3_4_7_1 = "Totaliza��o de D�bitos de Todos os Clientes (Sint�tico)"
                                this.item3_3_4_7_2 = "Totaliza��o de D�bitos Para um Determinado Cliente"
                                this.item3_3_4_7_3 = "Totaliza��o de D�bitos Para um Determinado Cliente no Per�odo"
				this.item3_3_4_7_4 = "Totaliza��o de D�bitos por Cliente por Atividade (Anal�tico)"
                                this.item3_3_4_7_5 = "Totaliza��o de D�bitos de todos os Clientes no Per�odo (Sint�tico)"
                                this.item3_3_4_7_6 = "Totaliza��o de D�bitos de todos os Clientes no Per�odo (Sint�tico) com Endere�o e Divis�o"

                                this.url3_3_4_7_0 = "RelDebiGera.php"
                                this.url3_3_4_7_1 = "RelDebiGeraCliente.php"
                                this.url3_3_4_7_2 = "RelDebiParcCliente.php"
                                this.url3_3_4_7_3 = "RelDebiParcClientePeri.php"
				this.url3_3_4_7_4 = "RelDebiGeraClienteAtiv.php"
                                this.url3_3_4_7_5 = "RelDebiGeraClientePeri.php"
                                this.url3_3_4_7_6 = "RelDebiGeraClientePeriEndDiv.php"
                                this.status3_3_4_7_5 = "RelDebiGeraClientePeriEndDiv.php"




                        this.url3_3_4_0 = "RelDebiEmis.php"
                        this.url3_3_4_1 = "RelDebiDivi.php"
                        this.url3_3_4_2 = "RelDebiDist.php"
                        this.url3_3_4_3 = "RelDebiAtiv.php"
                        this.url3_3_4_4 = "RelDebiSubAti.php"
                        this.url3_3_4_5 = "RelDebiServ.php"
                        this.url3_3_4_6 = "RelDebiSubSer.php"



                this.item3_3_5 = "Por Cliente"


                    //Sub Menu 3_3_5

                        this.menu_xy3_3_5 = "-107,-26"
                        this.menu_width3_3_5 = 202

                        this.item3_3_5_0 = "Por Situa��o"
                        this.item3_3_5_1 = "Por Divis�o"
                        this.item3_3_5_2 = "Por Distrito"
                        this.item3_3_5_3 = "Por Atividade"
                        this.item3_3_5_4 = "Por Sub Atividade"
                        this.item3_3_5_5 = "Por Servi�o"
                        this.item3_3_5_6 = "Por Subservi�o"

                        this.url3_3_5_0 = "RelClienteSitu.php"
                        this.url3_3_5_1 = "RelClienteDivi.php"
                        this.url3_3_5_2 = "RelClienteDist.php"
                        this.url3_3_5_3 = "RelClienteAtiv.php"
                        this.url3_3_5_4 = "RelClienteSubAtiv.php"
                        this.url3_3_5_5 = "RelClienteServ.php"
                        this.url3_3_5_6 = "RelClienteSubServ.php"



                this.item3_3_6 = "Faturas Canceladas"

                this.url3_3_6 = "RelFatCancClientePeri.php"




        this.url3_0 = "http://"
        this.url3_2 = "Extrato.php"



    //Sub Menu 4

        this.menu_xy4 = "-80,1"
        this.menu_width4 = 218

        this.item4_0 = "Retorno de Faturas(Faturas Pagas)"
        this.item4_1 = "Remessa (Faturas a Pagar)"
        //this.item4_2 = "Retorno de Faturas em atraso"
        //this.item4_3 = "Remessa de Faturas em atraso"

        this.url4_0 = "RetornoFatPagas.php"
        this.url4_1 = "GeracaoBB.php"
        this.url4_2 = "RetornoFatAtrasPagas.php"
        this.status4_2 = "RetornoFatAtrasPagas.php"
        this.url4_3 = "GeracaoBBFatatras.php"
        this.status4_3 = "GeracaoBBFatatras.php"



    //Sub Menu 5

        this.menu_width5 = 167

        this.item5_0 = "Al�quota/Vig�ncia - JUROS"
        this.item5_1 = "Al�quota/Vig�ncia - INSS"
        this.item5_2 = "Al�quota/Vig�ncia - ISSQN"

        this.url5_0 = "CadParam.php"
        this.url5_1 = "CadParamAliq.php"
        this.url5_2 = "CadParamAliqISSQN.php"



    //Sub Menu 6




}///////////////////////// END Menu Data /////////////////////////////////////////



//Document Level Settings

cdd__activate_onclick = false
cdd__showhide_delay = 100
cdd__url_target = "_self"
cdd__url_features = "resizable=1, scrollbars=1, titlebar=1, menubar=1, toolbar=1, location=1, status=1, directories=1, channelmode=0, fullscreen=0"
cdd__display_urls_in_status_bar = true
cdd__default_cursor = "hand"


//NavStudio Code (Warning: Do Not Alter!)

if (window.showHelp){b_type = "ie"; if (!window.attachEvent) b_type += "mac";}if (document.createElementNS) b_type = "dom";if (navigator.userAgent.indexOf("afari")>-1) b_type = "safari";if (window.opera) b_type = "opera"; qmap1 = "\<\script language=\"JavaScript\" vqptag='loader_sub' src=\""; qmap2 = ".js\">\<\/script\>";;function iesf(){};;function vqp_error(val){/*alert(val) */}
if (b_type){document.write(qmap1+cdd__codebase+"pbrowser_"+b_type+qmap2);document.close();}

