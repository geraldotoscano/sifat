<?php

require('pdf/fpdf.php');

/**
 * Especializa da classe FPDF para emiss�o de relat�rios no modelo do SIFAT SLU
 * 
 * O objetivo dessa classe � simplificar o processo de emiss�o de relat�rios no SIFAT:
 * <ul>
 *   <li>N�o � necess�rio criar uma nova classe para cada relat�rio</li>
 *   <li>simplifica todo o processo de altera��o de relat�rio mudando apenas par�metros</li>
 *   <li>A ajuste do layout do PDF � autom�tico e parametriz�vel</li>
 *   <li>Simplifica a consulta SQL, porque a totaliza��o geral e de grupos � autom�tica</li>
 *   <li>Automatiza o tratamento de campos com formata��o especial. Ex.: Dinheiro.</li>
 * </ul>
 *   
 * Detalhes de como utilizar dispon�veis no arquivo da classe
 */
/*
 * Para criar um relat�rio, � necess�rio fornecer um array associativo com os seguintes par�metros:
 * 
 * PAR�METROS OBRIGAT�RIOS:
 *  'SOURCE': a fonte de dados do relat�rio, podendo ser uma consulta sql ou um inst�ncia da classe clsDBFaturar
 *  'COLUNAS': array associativo com as colunas da consulta devem ser apresentadas no relat�rio. Cada coluna deve ter o seguinte prot�tipo:
 *      'NOME_DA_COLUNA' => array(<DESCRICAO DA COLUNA NO CABE�ALHO>, <ALINHAMENTO>, <LARGURA DA COLUNA>, <FORMATO>, <FUN��O DE FILTRO>)
 *      Ex.:    'VALFAT' => array('Valor da fatura', 'L', 80),
 *              'MESREF' => array('M�s de refer�ncia', 'R', null, 1, 'converte_nome_mes')
 *      Valores esperados para cada informa��o da coluna:
 *          (String) NOME_DA_COLUNA: obrigat�rio e deve ser uma coluna v�lida retornada no sql
 *          (String) DESCRICAO DA COLUNA NO CABE�ALHO: obrigat�ria e pode ser qualquer nome
 *          (String[1]) ALINHAMENTO: opcional e pode ser 'L', 'C' ou 'R', corresponde a LEFT, RIGHT E CENTER, por padr�o � 'L'
 *          (int) LARGURA DA COLUNA: opcional e pode ser qualquer largura em mm sem sigla, por padr�o � a largura do texto do cabe�alho. Sempre que poss�vel, mantenha o valor padr�o.
 *          (int) FORMATO: opcional, � tipo do dado da coluna. Padr�o 0
 *              S�o aceitos os seguintes formatos:
 *                  0: sem formata��o;
 *                  1: formatado como dinheiro sem sigla
 *                  2: campo de texto truncado na largura da coluna
 *          (string) FUN��O DE FILTRO:  fun��o � qual todos os valores da coluna ser�o submetidos antes de exibir na tela.
 *                                      Recomenda-se usar somente quando for imposs�vel determinar direto na sql o valor filtrado.
 *                                      Para essa fun��o ser� fornecido o valor a ser filtrado e a tupla onde ele foi encontrado:
 *                                      Ex.:
                                       //exemplo: filtro que s� exibe valores maiores que zero
                                        function maior_que_zero($valor) {
                                            if(!is_numeric($valor))
                                                $valor = strtr($valor,',.','.,');
                                            return ($valor>0)?$valor:0;
                                        }
                                        //exemplo: filtro que exibe o valor da c�lula somente se o valor da coluna VALFAT da mesma linha for maior que 50
                                        function maior_que_cinquenta($valor, $linha) {
                                            if(!is_numeric($valor))
                                                $valor = strtr($valor,',.','.,');
                                            return ($linha['VALFAT']>50)?$valor:0;
                                        }
 * PAR�METROS OPCIONAIS
 *  'ORGAO': (String) <Descri��o do org�o que aparece no topo do relat�rio>. Padr�o 'SUPERINTEND�NCIA DE LIMPEZA URBANA'
 *  'TITULO': (String) <Nome do relat�rio>. Padr�o 'Relat�rio'. � muito importante especificar um nome pr�prio para cada relat�rio
 *  'PERIODO': (String) <Descri��o do per�odo ao qual o relat�rio se refere>. Padr�o ''. Ex.: '02/2016 a 03/2016', '01/01/2016 a 31/01/2016'
 *      | (Array) <contendo uma ou duas posi��es: 0 => m�s inicial 'mm/aaaa', 1 => (opcional) m�s final 'mm/yyyy' >
 *  'FILTROS': (Array) <Array associativo que descreve para o usu�rio quai filtro foram aplicados ao relat�rio>. Padr�o array vazio. Cada posi��o do array ser� apresentada em uma linha no cabe�alho.
 *  'TIPO': (String) <Tipo do arquivo do relat�rio>. Padr�o 'PDF'. Por enquanto n�o aceita outro tipo, mas o objetivo � implementar 'CSV'
 *  'AGRUPAMENTO': <Descreve se deve ser feito o agrupamento das informa��es do relat�rio>. Padr�o false. Pode ser um Array ou false.
 *                  (Array) <associativo, descreve os par�metros do agrupamento>
 *                          As propriedade s�o as seguintes: 
 *                          'ID': (String) <campo da consulta que identifica o agrupamento, deve ser o primeiro campo utilizado na cl�usula ORDER>. Obrigat�rio. Ex.: 'GRPATI' que � o c�digo da atividade
 *                          'VALUE': (String) <campo da consulta com a descri��o do agrupamento>. Obrigat�rio. Ex.: 'DESATI' � a descri��o da atividade
 *                          'NAME': (String) <nome do agrupamento como ir� aparecer no relat�rio>. Obrigat�rio. Ex.: 'Atividade' � a descri��o compat�vel com os exemplo acima.
 *                          'QUEBRA-PAGINA': (boolean) <se deve quebrar a p�gina a cada nova sess�o>. Opcional. Padr�o true
 *                          'DESCRICAO-TOTAL': (String) <prot�tipo da descri��o que deve aparecer nos totais de cada agrupamento>. Opcional. Padr�o 'Totais {NAME} {VALUE}'.
 *                              Palavras-chave aceitas no prot�tipo e valores correspondentes:
 *                                  '{NAME}': Valor da propriedade 'NAME' do agrupamento
 *                                  '{VALUE}': Valor do campo especificado em VALUE para o agrupamento atual.
 *                                  '{QUANTIDADE}': Quantidade de registro encontrados para o agrupamento
 *                                  Ex.: Um agrupamento de NAME = 'Atividade' e VALUE = 'DESATI', e que o valor de 'DESATI' para o agrupamento atual seja 'COM�RCIO'.
 *                                      Resultado: 'Totais Atividade COM�RCIO'
 *                  (boolean) false. Se n�o quiser utilizar agrupamento nenhum
 *  'TOTAL': <Descreve para quais colunas devem ser exibidos totais. Se usa AGRUPAMENTO, apresenta totais parciais do agrupamento tamb�m>. Opcional. Padr�o 'auto'
 *                  (String) 'auto' <Totaliza��o autom�tica>
 *                          Exibe automaticamente todas as colunas com valores financeiros (A coluna tem o valor 1 na quarta posi��o, que corresponde ao formato da coluna)>
 *                          Exibe a descri��o do total na primeira coluna que n�o tiver valor financeiro
 *                  (Array) <array associativo, descreve os par�metros das linhas com os totais>
 *                          As propriedade s�o as seguintes:
 *                          'DESCRICAO': (String) <nome da coluna onde ser� mostrada a descri��o do total>. Obrigat�rio. Ex.: DESCLI, apresentaria a descri��o dos totais abaixo da coluna 'DESCLI'.
 *                                      A descri��o do total geral ser� sempre 'TOTAL GERAL', e a descri��o do total do agrupamento depende do par�metro 'DESCRICAO-TOTAL' do agrupamento
 *                          'COLUNAS': (Array) <array num�rico com as colunas que devem ser totalizadas>. Obrigat�rio. Ex.: array('RET_INSS', 'JUROS', 'VALPGT')
 *                  (boolean) false. Significa que n�o ir� exibir totais
 *  //os par�metros, abaixo, s�o todos aplicados em mil�metros, mas devem ser expressas somente com o n�meros sem a sigla
 *  'LEFT-MARGIN': (int) Margem esquerda. Ser� usada para a margem direita tamb�m. Padr�o 5
 *  'TOP-MARGIN': (int) Margem superior da p�gina. Padr�o 5
 *  'BOTTOM-MARGIN': (int) Margem inferior da p�gina. Padr�o 7. O padr�o considera a numera��o aparece dentro da margem inferior.
 *  'FONT-SIZE': (float) Tamanho da fonte das linhas do relat�rio e do cabe�alho das colunas. N�o se aplica aos item do topo. Padr�o 6.5
 *  'LINE-HEIGHT': (float) Altura das linhas do relat�rio. Padr�o 5
 *  'CELL-SPACE': (float) Dist�ncia entre as c�lulas do relat�rio. Padr�o o valor exato para que as c�lulas ocupem toda p�gina
 *  'ORIENTATION': (String[1]) Orienta��o das p�ginas do relat�rio. Pode ser 'A' (Ajuste autom�tico), 'L' ("landscape", para paisagem) ou 'P' ("portrait", para retrato). Padr�o 'A'
 */
class RelatorioFPDF extends FPDF {
    //Propriedade comuns em relat�rios (nomes em ingl�s)
    var $defaultFontSize;
    var $lines;
    var $cellSpace;
    var $cellHeight;
    var $orientation;
    var $source;
    var $cols;
    var $filename;
    
    //Propriedades espec�ficas da SLU (nomes em portugu�s)
    var $titulo;
    var $periodo;
    var $meses_periodo;
    var $filtros;//apenas a descri��o dos filtros utilizados que ser� impressa no relat�rio. N�o restringe os dados da busca.
    var $agg;//false, se n�o aplica agrupamento
    var $agg_somas;//acumula o valor somado para cada coluna no �ltimo agrupamento
    var $agg_qtd;
    var $total;//false, se n�o aplica totais ou, um array contendo os nomes das colunas
    var $total_descricao;//nome da coluna onde � apresentada a descri��o dos totais
    var $total_somas;//acumula o valor somado para cada coluna
    var $mudouAgg = false;
    
    function RelatorioFPDF($cfg) {
        parent::FPDF();
        
       //COLUNAS � obrigat�rio e deve conter as colunas vis�veis no relat�rio
       if(!isset($cfg['COLUNAS']))
           throw new Exception('Especifique as colunas que devem constar no relat�rio');
       else
           $this->cols = $cfg['COLUNAS'];
        
       //valores padr�o para as propriedades n�o obrigat�rias do relat�rio
        $cfg = array_merge(
            array(
                'ORGAO' => 'SUPERINTEND�NCIA DE LIMPEZA URBANA',
                'TITULO' => 'Relat�rio',
                'PERIODO' => '',
                'FILTROS' => array(),
                'TIPO' => 'PDF',
                'AGRUPAMENTO' => false,
                'TOTAL' => 'auto',
                'LEFT-MARGIN' => 5,
                'TOP-MARGIN' => 5,
                'BOTTOM-MARGIN' => 7,
                'FONT-SIZE' => 6.5,
                'LINE-HEIGHT' => 5,
                'ORIENTATION' => 'A'
            ),
            $cfg
        );
        
        if(is_array($cfg['PERIODO'])) {
            $qtd_meses = count($cfg['PERIODO']);
            if($qtd_meses == 1) {
                $this->meses_periodo = self::mesesPeriodo($cfg['PERIODO'][0]);
            } else if($qtd_meses == 2) {
                $this->meses_periodo = self::mesesPeriodo($cfg['PERIODO'][0], $cfg['PERIODO'][1]);
            } else {
                throw new Exception('O par�metro \'PERIODO\' deve conter apenas o m�s inicial e o final (opcional)');
            }
            $this->periodo = 'M�s/Ano de refer�ncia: '.((count($this->meses_periodo)>1)?$cfg['PERIODO'][0].' a '.$cfg['PERIODO'][1]:$cfg['PERIODO'][0]);
        } else {
            $this->meses_periodo = array();
            $this->periodo = $cfg['PERIODO'];
        }
        
        //SOURCE � obrigat�rio e deve ser uma string com o SQL ou o objeto de dados padr�o do codecharge com a query j� executada
        if(!isset($cfg['SOURCE']))
            throw new Exception('Especifique a fonte de dados no par�metro \'SOURCE\'');
        elseif(is_string($cfg['SOURCE'])) {
            if(count($this->meses_periodo)>0)
                $cfg['SOURCE'] = str_replace('{PERIODO}', ' IN (\''.implode('\',\'', $this->meses_periodo).'\')', $cfg['SOURCE']);
            
            $this->source = new clsDBfaturar();
            $this->source->query($cfg['SOURCE']);
        }
        elseif($cfg['SOURCE'] instanceof clsDBFaturar) {
            $this->source = $cfg['SOURCE'];
        }
        else
            throw new Exception('A propriedade SOURCE deve ser uma instru��o SQL ou uma inst�ncia da classe clsDBFaturar');
        $this->source->Auto_Free = 0;
        
        //se determinou algum agrupamento
        if($cfg['AGRUPAMENTO'])
            $this->agg = array_merge(
                array(
                    'LAST' => false,
                    'QUEBRA-PAGINA' => true
                ),
                $cfg['AGRUPAMENTO']
            );
        else
            $this->agg = false;
        
        $this->defaultFontSize = $cfg['FONT-SIZE'];
        $this->cellHeight = $cfg['LINE-HEIGHT'];
        $this->filtros = $cfg['FILTROS'];
        
        $this->SetFillColor(225, 225, 225);
        $this->SetFont('Arial', '', $this->defaultFontSize);
        $this->SetAutoPageBreak(true, $cfg['BOTTOM-MARGIN']);

        $total_col_width = 0;
        //normaliza as informa��es das colunas para agilizar o uso no restante da classe
        foreach($this->cols as $k => $v) {
            //seta os valores padr�o para largura das colunas e orienta��o do texto
            if(!isset($v[1]))
                $this->cols[$k][1] = 'L';//alinha � esquerda
            if(!isset($v[2]))
                $this->cols[$k][2] = $this->GetStringWidth($v[0]);//largura igual � largura do cabe�alho da coluna
            if(!isset($v[3]))
                $this->cols[$k][3] = 0;//sem formata��o

            $total_col_width += $this->cols[$k][2];
        }
        
        //n�o foi especificada uma orienta��o espec�fica
        if($cfg['ORIENTATION']=='A') {
            //se todas as colunas couberem espa�adas em pelo menos 3pts (ou o espa�o especificado), imprime relat�rio como retrato para economizar papel
            if($total_col_width + (2 * $this->lMargin) +(count($this->cols)-1)*(isset($cfg['CELL-SPACE']) ? (int)$cfg['CELL-SPACE'] : 3) < 210)
                $this->orientation = 'P';
            else
                $this->orientation = 'L';
        } else 
            $this->orientation = $cfg['ORIENTATION'];
        
        $this->SetMargins($cfg['LEFT-MARGIN'], $cfg['TOP-MARGIN']);
        
        $somas = array();
        $colunas_total = array();
        //se a totaliza��o � autom�tica
        if($cfg['TOTAL'] == 'auto') {
            $col_names = array_keys($this->cols);
            $alocou_descricao = false;
            foreach($col_names as $col_name) {
                //por padr�o, apresenta totais para todas as colunas com valores financeiros
                if($this->cols[$col_name][3]==1) {
                    $colunas_total[] = $col_name;
                    $somas[$col_name] = 0.0;
                }
                //reserva o primeiro espa�o n�o financeiro para a descri��o dos totais
                elseif(!$alocou_descricao) {
                    $this->total_descricao = $col_name;
                    $alocou_descricao = true;
                }
            }
        }
        //se especificou totais customizados    '
        elseif($cfg['TOTAL'] !== false) {
            $this->total = $cfg['TOTAL']['COLUNAS'];
            $this->total_descricao = $cfg['TOTAL']['DESCRICAO'];
            
            foreach($this->total as $col_name) {
                $colunas_total[] = $col_name;
                $somas[$col_name] = 0.0;
            }
        }
        
        //se n�o havia nenhuma coluna que pudesse ser totalizada
        //if(empty($somas)) {
        if($cfg['TOTAL'] === false) {
            $this->total = false;//n�o aplica totais
        } else {
            $this->total = $colunas_total;
            $this->agg_somas = $somas;
            $this->total_somas = $somas;
            if($this->total_descricao==null)
                $this->total_descricao = $cfg['TOTAL']['DESCRICAO'];
            //determina o valor padr�o para descri��o de totais de agrupamento
            if($this->agg && !isset($this->agg['DESCRICAO-TOTAL']))
                $this->agg['DESCRICAO-TOTAL'] = 'Totais {NAME} {VALUE}';
        }

        //se n�o especificou um espa�amento entre as c�lulas, distribui de forma que ocupem toda p�gina
        if(isset($cfg['CELL-SPACE']))
            $this->cellSpace = $cfg['CELL-SPACE'];
        else {
            $page_w = ($this->orientation == 'P') ? $this->w : $this->h;
            $this->cellSpace = ($page_w - $total_col_width - 2 * $this->lMargin) / (count($this->cols)-1);
        }
    
        //informa��es do cabe�alho        
        $this->SetTitle($cfg['ORGAO']);
        $this->SetAuthor($cfg['ORGAO']);
        $this->titulo = $cfg['TITULO'];
        $this->filename = $this->_fileName($this->titulo.'-'.date('Y-m-d H-i-s'));
        $this->AliasNbPages();
    }
    
    function emitir($tipo = 'PDF')
    {
        //gera o relat�rio de acordo com o tipo
        switch($tipo) {
            case 'PDF':
                $this->_emitirPDF();
                break;
            case 'CSV':
                $this->_emitirCSV();
                break;
            default:
                throw new Exception("Tipo desconhecido de relat�rio: {$cfg['TIPO']}");
        }
    }
    
    function _emitirPDF() {
        $src = $this->source;
        $i=1;
        $col_names = array_keys($this->cols);
        //adiciona a primeira p�gina para os relat�rio configurados para n�o fazer quebras em novos agrupamentos
        if(!$this->agg || !$this->agg['QUEBRA-PAGINA']) {
            $this->mudouAgg = true;
            $this->AddPage($this->orientation);
        }
        
        $j=0;
        //para cada linha do resultado
        while ($src->next_record())
        {
            //est� aplicando algum agrupamento
            if($agg = $this->agg) {
                //mudou agrupamento (no primeiro registro sempre considera que mudou)
                if($this->mudouAgg = ($src->f($agg['ID']) != $agg['LAST'][$agg['ID']])) {
                    $line_y = $this->GetY();
                    //tinha salvo um agrupamento anterior est� configurado para apresentar totais
                    if($agg['LAST'] && $this->total!==false) {
                        $this->agg_qtd = $i-1;
                        //exibe os totais do agrupamento anterior
                        $this->_printSubTotals();
                        //linha divis�ria
                        $this->Line(0, $line_y, $this->w, $line_y);
                        $line_y += $this->cellHeight*2;//o pr�ximo agrupamento come�a uma linha abaixo dos totais
                    }
                    //guarda a tupla atual
                    $this->agg['LAST'] = $src->Record;
                    $i=1;
                    
                    if($agg['QUEBRA-PAGINA']) {
                        //quebra a p�gina para come�ar o novo agrupamento
                        $this->AddPage($this->orientation);
                    } else {
                        //novo agrupamento come�a logo abaixo do anterior
                        $this->SetY($line_y);
                        $this->_mudaAgrupamento();
                    }
                    //retira o negrito da fonte para imprimir as linhas
                    $this->SetFont('Arial', '', $this->defaultFontSize);
                }
            }
            //n�o est� aplicando agrupamento mas est� exibindo totais
            elseif($this->total) {
                $num = 0.0;
                foreach($this->total as $col_name) {
                    $num = $src->f($col_name);
                    if(!is_numeric($num))
                        $num = strtr($num, ',.', '.,');
                        
                    $this->total_somas[$col_name] += (float)$num;
                }
            }
             
            //insere um fundo nas linhas impares para facilitar a leitura
            if(($i++)%2) {
                //impede que o ret�ngulo ultrapasse os limites da p�gina
                $this->_previneOrfa($this->cellHeight);
                $this->Rect(0, $this->GetY(), $this->w, $this->cellHeight, 'F');
            }
                 
            //exibe os valores da linha
            foreach($col_names as $col_name)
                $this->textCell($col_name, $src->f($col_name));
                 
            $this->Ln($this->cellHeight);
            $j++;
        }
        
        $num_registros = $j;//infelizmente o m�todo num_rows() n�o tem um comportamento confi�vel para SELECT no PDO
        if($this->page==0)
            $this->AddPage($this->orientation);
        
        //exibe os totais
        if($this->total!==false) {
            if ($this->agg && $num_registros!==0)
            {
                $this->agg_qtd = $i-1;
                //exibe os totais do �ltimo agrupamento
                $this->_printSubTotals();
                 
                $line_y = $this->GetY();
                $this->Line(0, $line_y, $this->w, $line_y);
                $this->Ln($this->cellHeight);
            }
            
            //destaca a linha de totais gerais em amarelo
            $this->SetFillColor(255, 255, 215);
            //impede que o ret�ngulo ultrapasse os limites da p�gina
            $this->_previneOrfa($this->cellHeight);
            $this->Rect(0, $this->GetY(), $this->w, $this->cellHeight, 'F');
            //exibe totais gerais
            $this->_printTotals();
        }
        
        $this->Ln($this->cellHeight);
        
        $this->textCell($col_names[0], "TOTAL REGISTROS ENCONTRADOS  =>  {$num_registros}");
        
        $this->Output($this->filename.'.pdf', 'I');
    }
    
    function _emitirCSV() {
        $src = $this->source;
        if($this->agg) {
            //quando exporta em CSV, coloca o agrupamento na a primeira coluna
            $cols = array($this->agg['VALUE'] => array(strtoupper($this->agg['NAME']), 'L'));
            $cols = array_merge($cols, $this->cols);
        } else {
            $cols = $this->cols;
        }
        $col_names = array_keys($cols);
        
        //cabe�alho de aqrquivo CSV para download
        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename={$this->filename}.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        
        //cria o arquivo com permiss�o de escrita
        $arquivo = fopen('php://output', 'w');
        $linha = array();
        //imprime, t�tulos das colunas
        foreach($cols as $col) {
            $linha[] = $col[0];
        }
        fputcsv($arquivo, $linha, ';');
        //para cada linha do resultado
        while ($src->next_record())
        {
            $linha = array();
            foreach($col_names as $col_name) {
                $text = $src->f($col_name);
                //trata campos de dinheiro
                if($cols[$col_name][3]==1 && $text!=='') {
                    if(!is_numeric($text))
                        $text = strtr($text,',.','.,');
                
                    $text = number_format((float)$text, 2, ',', '.');
                }
                //adicionar valor � linha
                $linha[] = $text;
            }
            
            fputcsv($arquivo, $linha, ';');
        }
        
        flush();
        fclose($arquivo);
    }
    
    function Header()
    {
        //descri��o do �rg�o emissor do relat�rio
        $this->SetFont('Arial','B',14);
        $this->Cell(0, 8, $this->title, 0, 1, 'C');
        //imagem com o bras�o da PBH
        $this->SetFont('Arial', 'B', 12);
        $this->Image('pdf/logo-pbh-horizontal.jpg', $this->lMargin+1, $this->tMargin, 40);
        
        //t�tulo do relat�rio
        $this->SetFont('Arial', 'B', 11);
        $this->Cell(0, 5, $this->titulo, 0, 1, 'C');
        $this->SetFont('Arial', '', 9);
        if(count($this->filtros)>0) {
            //linhas que descrevem os filtros aplicados ao relat�rio
            foreach($this->filtros as $filtro)
                $this->Cell(0, 5, $filtro, 0, 1, 'C');
        }
        elseif(!empty($this->periodo)) {
            //se n�o aplicou filtro ao relat�rio mas utilizou per�odo, insere um espa�o de meia linha para distanciar o logo do per�odo
            $this->Cell(0, 2.5, '', 0, 1);
        }
        
        $this->Cell($this->w/2, $this->cellHeight, $this->periodo);
        $this->Cell(0,$this->cellHeight, 'Data da Emiss�o : '.CCGetSession('DataSist'), 0, 1, 'R');
        
        $this->Line(0, $this->GetY(), $this->w, $this->GetY());
        
        if(!$this->agg)
            $this->_cabecalho();
        else {
            if($this->agg['QUEBRA-PAGINA'])
                $this->_mudaAgrupamento();
            elseif(!$this->mudouAgg)
                $this->_cabecalho();
        }
    }
    
    function _mudaAgrupamento()
    {
  
        //se mudou agrupamento
        if ($this->agg && $this->mudouAgg)
        {
            $this->SetFont('Arial', 'B', 10);
            //impede que o cabe�alho fique quebrado entre duas p�ginas
            $this->_previneOrfa($this->cellHeight*3);
            
            $this->Cell(0, $this->cellHeight*1.5, "{$this->agg['NAME']}: {$this->agg['LAST'][$this->agg['VALUE']]}", 0, 1);
            $this->Line(0, $this->GetY(), $this->w, $this->GetY());
        }
        else 
        {
            //impede que o cabe�alho fique quebrado entre duas p�ginas
            $this->_previneOrfa($this->cellHeight*2.5);
        }
        
        $this->_cabecalho();
    }
    
    function _cabecalho() {
        $this->SetFont('Arial', 'B', $this->defaultFontSize);
        //imprime, t�tulos das colunas
        foreach($this->cols as $k => $v) {
            $col = $this->cols[$k];
        
            $this->Cell($col[2], $this->cellHeight, $v[0], 0, 0, $col[1]);
            $this->cell($this->cellSpace);
        }
        
        //insere uma linha abaixo do cabe�alho
        $line_y = $this->GetY()+$this->cellHeight;
        $this->Line(0, $line_y, $this->w, $line_y);
        $this->Ln($this->cellHeight);
    }
    
    /**
     * Se uma determinada altura supera os limites da p�gina atual, faz uma quebra de p�gina
     */
    function _previneOrfa($altura) {
        if($this->GetY()+$altura > $this->h-$this->bMargin)
            $this->AddPage($this->orientation);
    }
    
    function footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetY(-8);
        //Arial italic 8
        $this->SetFont('Arial','I',8);
        $this->Cell(0,10,'P�gina '.$this->PageNo().' / {nb}',0,0,'C');
    }
    
    function textCell($col_name, $text='')
    {
        if(!isset($this->cols[$col_name]))
            throw new Exception("Undefined column '$col_name'");
        else
            $col = $this->cols[$col_name];
        
        //se o valor � filtrado por uma fun��o antes de exibir (utilizar somente quando n�o for vi�vel retornar o valor filtrado na consulta SQL)
        if(isset($col[4])) {
            //efetua o chamado da fun��o passando dois par�metros: o valor da c�lula avaliada e a tupla completa
            $text = call_user_func($col[4], $text, $this->source->Record);
        }
        
        switch($col[3]) {
            //dinheiro sem sigla de moeda
            case 1:
                if($text!=='') {
                    if(!is_numeric($text))
                        $text = strtr($text,',.','.,');
                        //se foi solicitada totaliza��o autom�tica para esse campo
                        if(isset($this->agg_somas[$col_name]))
                            $this->agg_somas[$col_name] += $text;
                
                        $text = number_format((float)$text, 2, ',', '.');
                }
                break;
            //texto truncado na largura da c�lula 
            case 2:
                if($col[2]<1) break;
                $text_w = $this->GetStringWidth($text);
                while($text_w > $col[2]) {
                    if($text_w > 2*$col[2])
                        $text = substr($text,0,ceil(strlen($text)/2));
                    $text = substr($text,0,-1);
                    $text_w = $this->GetStringWidth($text);
                }
                break;
        }

        $this->Cell(
            $col[2],//cell width
            $this->cellHeight,//cell heigth
            $text,//text
            0,//no border
            0,//next cell begin right after in the same line
            $col[1]//text align [L, R, C]
        );
        $this->Cell($this->cellSpace);
    }
    
    /**
     * Exibe uma linha com a totaliza��o dos valores de um agrupamento
     */
    function _printSubTotals() {
        $this->SetFont('Arial', 'B', $this->defaultFontSize);
        //para cada coluna do relat�rio
        foreach($this->cols as $col => $v) {
            //verifica se foi solicitado exibir totais para a coluna
            if(in_array($col, $this->total)) {
                $soma = $this->agg_somas[$col];
                $this->textCell($col, $soma);
                $this->agg_somas[$col] = 0.0;//reseta acumulador da soma parcial
                $this->total_somas[$col] += $soma;//acumula soma total
            }
            //se a coluna � a descri��o do total
            elseif($col == $this->total_descricao) {
                $total_col = str_replace(
                    array('{NAME}','{VALUE}','{QUANTIDADE}'),
                    array(
                        $this->agg['NAME'],//nome dado ao agrupamento
                        $this->agg['LAST'][$this->agg['VALUE']],//valor da coluna VALUE para o agrupamento atual
                        $this->agg_qtd//quantidade de registros encontrados no agrupamento atual
                    ),
                    $this->agg['DESCRICAO-TOTAL']
                );
                
                $this->textCell($col, $total_col);
            }
            //se n�o foi solicitado exibir totais para a coluna, deixa o espa�o vazio
            else
                $this->textCell($col);
        }
    }
    
    /**
     * Exibe uma linha com a totaliza��o geral
     */
    function _printTotals() {
        $this->SetFont('Arial', 'B', $this->defaultFontSize);
        //para cada coluna do relat�rio
        foreach($this->cols as $col => $v) {
            //verifica se foi solicitado exibir totais para a coluna
            if(in_array($col, $this->total)) {
                $this->textCell($col, $this->total_somas[$col]);
            }
            //se a coluna � a descri��o do total
            elseif($col == $this->total_descricao) {
                $this->textCell($col, 'TOTAL GERAL');
            }
            //se n�o foi solicitado exibir totais para a coluna, deixa o espa�o vazio
            else
                $this->textCell($col);
        }
    }
    
    /**
     * Trata uma string e transforma em um nome de arquivo ascii
     * 
     * @param String $str string que ser� utilizada como nome de arquivo
     */
    function _fileName($str) {
        $forbidden = Array(",",".","'","\"","&","|","!","#","$","�","*","(",")","`","�","<",">",";","=","+","�","{","}","[","]","^","~","?","%");
        $special = Array(' ','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�',
            '�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�');
        $ascii = Array('-','a','e','o','c','a','e','o','c','a','e','o','a','e','o','n','a','d','o','n','a','o','o','a','o','y','a','i','o','y','a','i','o','a',
            'i','a','i','u','a','i','u','a','i','u','a','i','u','i','u','','e','u','c','e','o','u','p','e','o','u','b','e','o','b','','','','','','');
        $str = str_replace($special, $ascii, trim($str));
        $str = str_replace($forbidden, '', $str);
        return strtolower($str);
    }
    
    /**
     * Calcula os meses presentes em um per�odo para ser utilizado em consultas envolvendo o campo MESREF, j� que ele � uma string (mm/aaaa) ao inv�s de um datetime
     * 
     * @param String $mesIni m�s inicial padr�o mm/aaaa
     * @param String $mesFim (optional) m�s final padr�o mm/aaaa
     * @param boolean $permiteDataFutura se aceitar m�s futuro como v�lido
     * 
     * @return Array contendo todos os meses encontrado para o per�odo
     */
    static function mesesPeriodo($mesIni, $mesFim = false, $permiteDataFutura = false) {
        $ini = self::validaMesRef($mesIni, $permiteDataFutura);
        if(!$ini) throw new Exception("M�s inicial inv�lido ou muito antigo: $mesIni"); 
        if($mesFim === false)
            return array($mesIni);
        $fim = self::validaMesRef($mesFim, $permiteDataFutura);
        if(!$fim) throw new Exception('M�s final inv�lido'.($permiteDataFutura ? '' : ' ou maior que o m�s atual').": $mesFim");
        $qtd_meses = ($fim[1]*12+$fim[0]) - ($ini[1]*12+$ini[0]) + 1;
        if($qtd_meses<1) throw new Exception("O m�s final n�o pode ser anterior ao inicial");
        
        $meses = array();
        $ano = $ini[1]; $mes = $ini[0];
        for($i=0; $i<$qtd_meses; $i++) {
            if($mes==13) {
                $mes=1;
                $ano++;
            }
            $meses[] = str_pad($mes, 2, '0', STR_PAD_LEFT)."/$ano";
            $mes++;
        }
        return $meses;
    }
    
    /**
     * Verifica se um m�s no padr�o do sifat � v�lido
     * 
     * @param String $mes m�s no padr�o mm/aaaa
     * @param boolean $permiteDataFutura se aceitar m�s futuro como v�lido
     * 
     * @return Array [(int)m�s, (int)ano], ou (boolean) false, se m�s inv�lido  
     */
    static function validaMesRef($mes, $permiteDataFutura = false) {
        $mes = explode('/', $mes);
        if(count($mes)==2) {
            $mes[0] = (int)$mes[0];
            $mes[1] = (int)$mes[1];
            if($mes[0]>0 && $mes[1]>1980 && $mes[0] < 13 && $mes[1] < 3000) {
                if($permiteDataFutura)
                    return $mes;
                else {
                    $mesAtual = explode('/', date('m/Y'));
                    if (($mes[1]*12+$mes[0]) <= ($mesAtual[1]*12+(int)$mesAtual[0]))
                        return $mes;
                }
            }
        }
        return false;
    }
}

?>
