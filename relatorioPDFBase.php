<?php
##ifndef RelativePath
#    define("RelativePath", ".");
#endif

##ifndef PathToCurrentPage
#    define("PathToCurrentPage", "/");
#endif


	error_reporting (E_ALL & ~E_DEPRECATED);

	require_once('pdf/fpdf.php');

    include_once(RelativePath . "/Common.php");
    include_once(RelativePath . "/Template.php");
    include_once(RelativePath . "/Sorter.php");
    include_once(RelativePath . "/Navigator.php");
	include_once(RelativePath . "/util.php");
	include_once(RelativePath . "/dompdf/dompdf_config.inc.php");

	class relatorioPDFBase {
	//$pdf= new fpdi();
    protected $relatorio=false;
	protected $detalhe=false;
	protected $modelo="";
	protected $modelo_corpo=array();
	protected $modelo_linha="";
	protected $modelo_cabecalho='';
	protected $modelo_final_impressao='';
	protected $modelo_rodape;
	protected $arq_modelo="";
	protected $linhas_pagina=20;
	
	
	protected $var_modelo=array();
	
	protected $sql_relatorio='';
	protected $sql_fim_relatorio='';
	
    protected $em_real=array();
    
    function __construct() {
        $this->relatorio   = new clsDBfaturar();
	    $this->detalhe  = new clsDBfaturar();
    }

	function substitui($variavel,$valor,$texto)
	{
     return(str_replace($variavel,$valor,$texto));
	}

    function pega_campos($consulta)
	{
	 return(array_keys($consulta->Record));
	
	}

    function substitui_parametros_sql($parametros,$consulta)
	{
	 $campos=array_keys($parametros);
	 for($i=0;$i<count($campos);$i++)
	   {
	    $consulta=$this->substitui('{'.$campos[$i].'}',$parametros[$campos[$i]],$consulta);
	   }
	 return($consulta);
	}

    function substitui_parametros_modelos($parametros,$modelo)
	{
	 $campos=array_keys($parametros);
	 for($i=0;$i<count($campos);$i++)
	   {
	    $aux_valor=$parametros[$campos[$i]];
	    if( in_array($campos[$i],$this->em_real) )
		  {
		   $aux_valor=$this->em_real($aux_valor);
		  }
		  
	    $modelo=$this->substitui('%'.$campos[$i].'%',$aux_valor,$modelo);
	   }
	 return($modelo);
	}
	
	
	function adicionar_parametros_consulta(&$parametros,$consulta)
	{
	 $campos=$this->pega_campos($consulta);
	 for($i=0;$i<count($campos);$i++)
	   {
		$parametros[$campos[$i]]=$consulta->f($campos[$i]);
	   }
	 
	
	}
	
	function set_sql_relatorio($sql)
	{
	 $this->sql_relatorio=$sql;
	}

	function set_sql_fim_relatorio($sql)
	{
	 $this->sql_fim_relatorio=$sql;
	}
	
	function set_modelo($arquivo)
	{

 	 $this->arq_modelo=$arquivo;
	 $this->modelo=file_get_contents($this->arq_modelo);

	 
	 $pos_body=strpos($this->modelo,'<body>');
	 $pos_body_f=strpos($this->modelo,'</body>');
	 $corpo_pre="";
	 
	 
	 if($pos_body!=0 && $pos_body_f!=0 && $pos_body<$pos_body_f)
	   {

 	    $this->modelo_cabecalho=substr($this->modelo,0,$pos_body+6);
		
	    $corpo_pre=substr($this->modelo,$pos_body+6,$pos_body_f-($pos_body+6));

	    $this->modelo_rodape=substr($this->modelo,$pos_body_f,strlen($this->modelo));

		$pos_linha=strpos($corpo_pre,'<!--linha1');
		$pos_linha_f=strpos($corpo_pre,'linha1-->');
		
		if($pos_linha!=0 && $pos_linha_f!=0 && $pos_linha<$pos_linha_f)
		  {

		   $this->modelo_corpo=array();
		   $this->modelo_corpo[]=substr($corpo_pre,0,$pos_linha);
		   $this->modelo_corpo[]=substr($corpo_pre,$pos_linha+10,$pos_linha_f-($pos_linha+10));
		   $this->modelo_corpo[]=substr($corpo_pre,$pos_linha_f+10,strlen($corpo_pre));
		   $final_corpo_pre=$this->modelo_corpo[2];
		   $pos_final=strpos($final_corpo_pre,'<!--final1');
		   $pos_final_f=strpos($final_corpo_pre,'final1-->');
		   if($pos_final!=0 && $pos_final_f!=0 && $pos_final<$pos_final_f)
		     {
			  $this->modelo_corpo[2]=substr($final_corpo_pre,0,$pos_final);
			  $this->modelo_final_impressao=substr($final_corpo_pre,$pos_final+10,$pos_final_f-($pos_final+10));
			  //$this->modelo_corpo[]=substr($pos_final_f,$pos_final+10,$pos_final_f-($pos_final+10));
			  $this->modelo_corpo[]=substr($final_corpo_pre,$pos_final_f+10,strlen($final_corpo_pre));
			 
			 }
		  }
		 else
		  {
		   $this->modelo_corpo=array($corpo_pre);
		  }
		
	   }
	 
	 
	 

	}


    function parametro_em_real($parametros)
	  {
	   if(is_array($parametros))
	     {
		  $this->em_real=$parametros;
		 }
		else
		 {
		  $this->em_real=array($parametros);
		 }
	  
	  }
   function set_linhas($valor)
     {
	 
	  $this->linhas_pagina=$valor;
	 }
	
	function relatorio($parametros,$em_PDF=true)
	{
		//global $titulo;
		//global $mesRef;
		//global $posCliente;
		//global $posFatura;
		//global $posEmissao;
		//global $posValFat;
		//global $posVenc;

        $consulta_principal=$this->substitui_parametros_sql($parametros,$this->sql_relatorio);
		


//		print "\n<br> fatura=\"".$consulta."\"";
        $relatorio_db=$this->relatorio;
		$detalhe_db=$this->detalhe;

		$relatorio_db->query($consulta_principal);
		$resultado=false;
		$relatorio="";
		$cabecalho=$this->substitui_parametros_modelos($parametros,$this->modelo_cabecalho);
		$linhas_proc=0;
		$corpo="";
		$fechou=true;
		$parametros['NUMERO_LINHA']=0;
		$numero_pagina=0;
		
		while($relatorio_db->next_record())
		  {
		   $resultado=true;
		   $parametros['NUMERO_PAGINA']=$numero_pagina+1;
		   
		   $this->adicionar_parametros_consulta($parametros,$relatorio_db);
		   if(count($this->modelo_corpo)==1)
		     {
			  $corpo=$corpo.$this->substitui_parametros_modelos($parametros,$this->modelo_corpo[0]);
			  $numero_pagina++;
			 }
			else
			 {
			  
			  if($linhas_proc==0)
			    {
				 
				 if(count($this->modelo_corpo)>2 && $corpo!='')
				   {
				    $parametros['NUMERO_PAGINA']=$numero_pagina;
				    for($i=3;$i<count($this->modelo_corpo);$i++)
					  {
					   $corpo=$corpo.$this->substitui_parametros_modelos($parametros,$this->modelo_corpo[$i]);
					  }
					$parametros['NUMERO_PAGINA']=$numero_pagina+1;  
				   }
				 $fechou=false;
				 $corpo=$corpo.$this->substitui_parametros_modelos($parametros,$this->modelo_corpo[0]);
				}
			  $parametros['NUMERO_LINHA']=$linhas_proc+1;
			  $corpo=$corpo.$this->substitui_parametros_modelos($parametros,$this->modelo_corpo[1]);
			  if(($linhas_proc+1)==$this->linhas_pagina)
			    {
				 $fechou=true;
				 $corpo=$corpo.$this->substitui_parametros_modelos($parametros,$this->modelo_corpo[2]);
				 $linhas_proc=-1;
				 $numero_pagina++;
				}
				
				
			  $linhas_proc++;
			 }
		   
		   
		  }
		if($fechou==false)  
		  {
		   $fechou=true;
		   $corpo=$corpo.$this->substitui_parametros_modelos($parametros,$this->modelo_corpo[2]);
		   $numero_pagina++;
		   
		  }
		
		if($this->sql_fim_relatorio!='')
		  {
		    $consulta_fim_relatorio=$this->substitui_parametros_sql($parametros,$this->sql_fim_relatorio);
			$detalhe_db->query($consulta_fim_relatorio);
			if($detalhe_db->next_record())
			  $this->adicionar_parametros_consulta($parametros,$detalhe_db);
		  }
		  
		$corpo=$corpo.$this->substitui_parametros_modelos($parametros,$this->modelo_final_impressao);
		if(count($this->modelo_corpo)>2)
		  {
		   for($i=3;$i<count($this->modelo_corpo);$i++)
			 {
			  $corpo=$corpo.$this->substitui_parametros_modelos($parametros,$this->modelo_corpo[$i]);
			 }
		  }
		  
		
		$rodape=$this->substitui_parametros_modelos($parametros,$this->modelo_rodape);
		
		$relatorio=$cabecalho.$corpo.$rodape;

        
        if($em_PDF==true)
		  {
			$dompdf = new DOMPDF();
		    $dompdf->load_html($relatorio);
		    $dompdf->set_paper(DOMPDF_DEFAULT_PAPER_SIZE,"portrait");
		    $dompdf->render();
		    return($dompdf->output());
		  }
         else
		  {
           return($relatorio);
		  }
		
   }


 	function em_real($valor)
	{
		if(strrpos($valor,',') === strlen($valor)-3) {
			$valor = (float)((  str_replace('.','',substr($valor, 0, -3))  ).'.'.substr($valor, -2));
			//$valor = (float)str_replace(array(',','.'),array('.',''),$valor);
		} else
			$valor = (float)$valor;

		return 'R$ '.number_format($valor, 2, ',', '.');
   }


}
?>