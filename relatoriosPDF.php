<?php
	error_reporting (E_ALL);
	//define("RelativePath", "../..");
	//define('FPDF_FONTPATH','/pdf/font/');
	require('pdf/fpdf.php');
	//include('pdf/fpdi.php');
	//require_once("pdf/fpdi_pdf_parser.php");
	//require_once("pdf/fpdf_tpl.php");
	//include(RelativePath . "/Common.php");
	//include(RelativePath . "/Template.php");
	//include(RelativePath . "/Sorter.php");
	//include(RelativePath . "/Navigator.php");
class relatoriosPDF extends fpdf {	
	var $titulo;
	var $mesRef;
	var $posCliente;
	var $posFatura;
	var $posEmissao;
	var $posValFat;
	var $posVenc;
	var $totalGeral;
	var $StringTam;
	var $relat;
	var $mudouDistrito;
	var $distrito;
	var $posMesAno;
	var $posINSS;
	var $posPGT;
	var $posAcres;
	var $posRec;
	//$pdf= new fpdi();
	function relatorio($titu,$mesAno,$rel,$CodDis='')
	{
		//global $titulo;
		//global $mesRef;
		//global $posCliente;
		//global $posFatura;
		//global $posEmissao;
		//global $posValFat;
		//global $posVenc;
		$this->totalGeral = 0;
		$this->mesRef = $mesAno;
		$this->titulo = $titu;
		$this->relat = $rel;
		$this->SetTitle('SUPERINTEND�NCIA DE LIMPEZA URBANA');
		$this->AliasNbPages();
		
		$Tabela   = new clsDBfaturar();
		$Grupo    = new clsDBfaturar();
		$Divisao  = new clsDBfaturar();
		//                            L   I   N   H   A       D   E       D   E   T   A   L   H   E
		switch ($rel) 
		{
			case 411; //                       Relat�rio de Emissao por Inscri��o
				$Tabela->query("SELECT 
									F.CODCLI,
									to_char(F.DATEMI,'dd/mm/yyyy') AS DATEMI,
									to_char(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
									F.CODFAT,
									F.VALFAT,
									C.DESCLI,
									(
										SELECT
											SUM(VALFAT)
										FROM
											CADFAT F,
											CADCLI C
										WHERE 
											F.CODCLI=C.CODCLI AND 
											MESREF='$this->mesRef'
									) AS TOTGER
									FROM 
										CADFAT F,
										CADCLI C 
									WHERE 
										F.CODCLI=C.CODCLI AND 
										MESREF='$this->mesRef'
									ORDER BY 
										MESREF,CODCLI"
							  );
				//$articles = array();
				$Linha = 50;
				$this->SetY($Linha);
				$this->addPage();
				while ($Tabela->next_record())
				{
					if ($this->totalGeral==0)
					{
						$this->totalGeral = (float)((str_replace(",", ".", $Tabela->f("TOTGER"))));
						$this->totalGeral = number_format($this->totalGeral, 2,',','.');
					}
						//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
					if ($this->GetY() >= ($this->fh-12))
					{
						$Linha = 50;
						$this->addPage(); 
					}
					$this->SetY($Linha);
					$Inscricao = $Tabela->f("CODCLI");
					$this->Text(3,$Linha,$Inscricao);
		
					$Cliente = $Tabela->f("DESCLI");
					$this->Text($this->posCliente,$Linha,$Cliente);
		
					$Fatura = $Tabela->f("CODFAT");
					$this->Text($this->posFatura,$Linha,$Fatura);
				
					$Emissao = $Tabela->f("DATEMI");
					$this->Text($this->posEmissao,$Linha,$Emissao);
		
					$ValFat = $Tabela->f("VALFAT");
					$ValFat = (float)((str_replace(",", ".", $ValFat)));
					$ValFat = number_format($ValFat, 2,',','.');
					$this->StringTam;
					//           (       f i n a l                   ) -       t  a  m  n  h  o    
					$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($ValFat)),$Linha,$ValFat);
					
					$Venci = $Tabela->f("DATVNC");
					$this->Text($this->posVenc,$Linha,$Venci);
					
					$Linha+=4;
				}
				$this->Text(3,$Linha,"T  o  t  a  l     G  e  r  a  l");
				//           (       f i n a l                   ) -       t  a  m  n  h  o    
				$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$Linha,$this->totalGeral);
				
				//$this->SetMargins(5,5,5);
				$this->SetFont('Arial','U',10);
				$this->SetTextColor(0, 0, 0);
				$this->SetAutoPageBreak(1);
				$this->Output();
			break;
			case 412; //                       Relat�rio de Emissao por Divis�o
				$mSelect =	"	SELECT
									SUM(VALFAT) AS TotalDistr,
									C.CODUNI
								FROM
									CADFAT F,
									CADCLI C,
									TABUNI T
								WHERE 
									F.CODCLI=C.CODCLI AND 
									C.CODUNI=T.CODUNI AND
							";
				if ($CodDis!="Todos")
				{
					$mSelect .= "	C.CODUNI='$CodDis' AND ";
				}
				$mSelect .= "		F.MESREF='$this->mesRef'
								GROUP BY 
									C.CODUNI
								ORDER BY
									C.CODUNI
							";
							 
				$Grupo->query($mSelect);
									
				$Linha = 46;
				$this->SetY($Linha);
				
				$this->totalGeral = 0;
				
				$Grupo->next_record();
				$this->addPage();
				$this->SetFont('Arial','B',10);
				do 
				{
					$codUni = $Grupo->f("CODUNI");
					$Divisao->query("SELECT DESCRI FROM TABUNI WHERE CODUNI = '$codUni'");
					$Divisao->next_record();
					$this->distrito = $Divisao->f("DESCRI");
					
					//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
					if ($this->GetY() >= ($this->fh-14))
					{
						$Linha = 46;
						$this->addPage(); 
					}
					
					$this->Text(05,$Linha,"Divis�o : $this->distrito");
					
					$Linha+=7;
					$this->SetY($Linha);
					$totalDistrito = (float)((str_replace(",", ".", $Grupo->f("TotalDistr"))));
					$this->totalGeral += $totalDistrito;
					$totalDistrito = number_format($totalDistrito, 2,',','.');
					
					$this->Text(05,$Linha,"T  o  t  a  l     n  a     D  i  v  i  s  �  o  ");
					
					//           (       f i n a l                   ) -       t  a  m  n  h  o    
					$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($totalDistrito)),$Linha,$totalDistrito);
					
					$this->Line(0,$Linha+1,210.6,$Linha+1);
					$Linha += 7;
				}while ($Grupo->next_record());

				//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
				if ($this->GetY() >= ($this->fh-14))
				{
					$Linha = 46;
					$this->addPage(); 
				}
					
				$this->totalGeral = number_format($this->totalGeral, 2,',','.');
				
				$this->Text(5,$Linha,"T  o  t  a  l     G  e  r  a  l");
				//           (       f i n a l                   ) -       t  a  m  n  h  o    
				$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$Linha,$this->totalGeral);
				
				$this->SetFont('Arial','U',10);
				$this->SetTextColor(0, 0, 0);
				$this->SetAutoPageBreak(1);
				$this->Output();
			break;
			case 413; //                       Relat�rio de Emissao por Distrito
				$mSelect =		"SELECT 
									F.CODCLI,
									to_char(F.DATEMI,'dd/mm/yyyy') AS DATEMI,
									to_char(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
									F.CODFAT,
									F.VALFAT,
									C.DESCLI,
									T.DESDIS,
									(
										SELECT
											SUM(VALFAT)
										FROM
											CADFAT F,
											CADCLI C,
											TABDIS T
										WHERE 
											F.CODCLI=C.CODCLI AND 
											C.CODDIS=T.CODDIS AND
										";
											
				if ($CodDis!="Todos")
				{
					$mSelect .= "C.CODDIS='$CodDis'                                                      AND ";
				}
				$mSelect .= 	"			MESREF='$this->mesRef'
									) AS TOTGER
								FROM 
									CADFAT F,
									CADCLI C,
									TABDIS T
								WHERE 
									F.CODCLI=C.CODCLI                                            AND 
									C.CODDIS=T.CODDIS                                            AND
								";
								
									
				if ($CodDis!="Todos")
				{
					$mSelect .= "C.CODDIS='$CodDis'                                                      AND ";
				}
				$mSelect .= "	MESREF='$this->mesRef'
								ORDER BY 
									C.CODDIS,
									C.DESCLI
								";
				$Tabela->query($mSelect);
				//$this->addPage();
				//$this->Text(05,50,$mSelect);
				$mSelect =	"	SELECT
									SUM(VALFAT) AS TotalDistr
								FROM
									CADFAT F,
									CADCLI C,
									TABDIS T
								WHERE 
									F.CODCLI=C.CODCLI AND 
									C.CODDIS=T.CODDIS AND
							";
				if ($CodDis!="Todos")
				{
					$mSelect .= "	C.CODDIS='$CodDis' AND ";
				}
				$mSelect .= "		MESREF='$this->mesRef'
								GROUP BY 
									C.CODDIS
								ORDER BY
									C.CODDIS
							";
							 
				//$this->addPage();
				//$this->Text(05,50,$mSelect);
				$Grupo->query($mSelect);
									
				//$articles = array();
				$Linha = 54;
				$this->SetY($Linha);
				$this->mudouDistrito = true;
				$Tabela->next_record();
				$Grupo->next_record();
				$this->distrito = $Tabela->f("DESDIS");
				$this->addPage();
				//$this->SetFont('Arial','B',10);
				//$this->Text(05,50,"Distrito : $distrito");
				//$this->Line(0,51,210.6,51);
				$this->SetFont('Arial','B',06);
				do 
				{
					$this->mudouDistrito = false;
					if ($this->totalGeral==0)
					{
						$this->totalGeral = (float)((str_replace(",", ".", $Tabela->f("TOTGER"))));
						$this->totalGeral = number_format($this->totalGeral, 2,',','.');
					}
						//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
					if ($this->GetY() >= ($this->fh-14))
					{
						$Linha = 50;
						$this->addPage(); 
					}
					else
					{
						if ($Tabela->f("DESDIS") != $this->distrito)
						{
							$totalDistrito = (float)((str_replace(",", ".", $Grupo->f("TotalDistr"))));
							$totalDistrito = number_format($totalDistrito, 2,',','.');
							$this->Text(3,$Linha,"T  o  t  a  l     n  o     D  i  s  t  r  i  t  o  ");
							//           (       f i n a l                   ) -       t  a  m  n  h  o    
							$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($totalDistrito)),$Linha,$totalDistrito);
							$Grupo->next_record();
							$Linha = 54;
							$this->mudouDistrito = true;
							$this->distrito = $Tabela->f("DESDIS");
							$this->addPage(); 
							//$this->SetFont('Arial','B',10);
							//$this->Text(05,50,"Distrito : $distrito");
							//$this->Line(0,51,210.6,51);
							$this->SetFont('Arial','B',06);
						}
					}
					$this->SetY($Linha);
					$Inscricao = $Tabela->f("CODCLI");
					$this->Text(3,$Linha,$Inscricao);
		
					$Cliente = $Tabela->f("DESCLI");
					$this->Text($this->posCliente,$Linha,$Cliente);
		
					$Fatura = $Tabela->f("CODFAT");
					$this->Text($this->posFatura,$Linha,$Fatura);
				
					$Emissao = $Tabela->f("DATEMI");
					$this->Text($this->posEmissao,$Linha,$Emissao);
		
					$ValFat = $Tabela->f("VALFAT");
					$ValFat = (float)((str_replace(",", ".", $ValFat)));
					$ValFat = number_format($ValFat, 2,',','.');
					$this->StringTam;
					//           (       f i n a l                   ) -       t  a  m  n  h  o    
					$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($ValFat)),$Linha,$ValFat);
					
					$Venci = $Tabela->f("DATVNC");
					$this->Text($this->posVenc,$Linha,$Venci);
					
					$Linha+=4;
				}while ($Tabela->next_record());

						if (!$this->mudouDistrito)
						{
							$totalDistrito = (float)((str_replace(",", ".", $Grupo->f("TotalDistr"))));
							$totalDistrito = number_format($totalDistrito, 2,',','.');
							$this->Text(3,$Linha,"T  o  t  a  l     n  o     D  i  s  t  r  i  t  o  ");
							//           (       f i n a l                   ) -       t  a  m  n  h  o    
							$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($totalDistrito)),$Linha,$totalDistrito);
							$Linha+=4;
						}

				
				$this->Text(3,$Linha,"T  o  t  a  l     G  e  r  a  l");
				//           (       f i n a l                   ) -       t  a  m  n  h  o    
				$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$Linha,$this->totalGeral);
				
				//$this->SetMargins(5,5,5);
				$this->SetFont('Arial','U',10);
				$this->SetTextColor(0, 0, 0);
				$this->SetAutoPageBreak(1);
				$this->Output();
			break;
			case 414; //                       Relat�rio de Emissao por Atividade
				$mSelect =		"SELECT 
									F.CODCLI,
									to_char(F.DATEMI,'dd/mm/yyyy') AS DATEMI,
									to_char(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
									F.CODFAT,
									F.VALFAT,
									C.DESCLI,
									G.DESATI,
									(
										SELECT
											SUM(VALFAT)
										FROM
											CADFAT F,
											CADCLI C,
											GRPATI G
										WHERE 
											F.CODCLI=C.CODCLI AND 
											C.GRPATI=G.GRPATI AND
										";
											
				if ($CodDis!="Todos")
				{
					$mSelect .= "C.GRPATI='$CodDis'                                                      AND ";
				}
				$mSelect .= 	"			F.MESREF='$this->mesRef'
									) AS TOTGER
								FROM 
									CADFAT F,
									CADCLI C,
									GRPATI G
								WHERE 
									F.CODCLI=C.CODCLI                                            AND 
									C.GRPATI=G.GRPATI                                            AND
								";
								
									
				if ($CodDis!="Todos")
				{
					$mSelect .= "C.GRPATI='$CodDis'                                                      AND ";
				}
				$mSelect .= "	F.MESREF='$this->mesRef'
								ORDER BY 
									C.GRPATI,
									C.DESCLI
								";
				$Tabela->query($mSelect);
				//$this->addPage();
				//$this->Text(05,50,$mSelect);
				$mSelect =	"	SELECT
									SUM(VALFAT) AS TotalDistr
								FROM
									CADFAT F,
									CADCLI C,
									GRPATI G
								WHERE 
									F.CODCLI=C.CODCLI AND 
									C.GRPATI=G.GRPATI AND
							";
				if ($CodDis!="Todos")
				{
					$mSelect .= "	C.GRPATI='$CodDis' AND ";
				}
				$mSelect .= "		F.MESREF='$this->mesRef'
								GROUP BY 
									C.GRPATI
								ORDER BY
									C.GRPATI
							";
							 
				//$this->addPage();
				//$this->Text(05,50,$mSelect);
				$Grupo->query($mSelect);
									
				//$articles = array();
				$Linha = 54;
				$this->SetY($Linha);
				$this->mudouDistrito = true;
				$Tabela->next_record();
				$Grupo->next_record();
				$this->distrito = $Tabela->f("DESATI");
				$this->addPage();
				//$this->SetFont('Arial','B',10);
				//$this->Text(05,50,"Distrito : $distrito");
				//$this->Line(0,51,210.6,51);
				$this->SetFont('Arial','B',06);
				do 
				{
					$this->mudouDistrito = false;
					if ($this->totalGeral==0)
					{
						$this->totalGeral = (float)((str_replace(",", ".", $Tabela->f("TOTGER"))));
						$this->totalGeral = number_format($this->totalGeral, 2,',','.');
					}
						//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
					if ($this->GetY() >= ($this->fh-14))
					{
						$Linha = 50;
						$this->addPage(); 
					}
					else
					{
						if ($Tabela->f("DESATI") != $this->distrito)
						{
							$totalDistrito = (float)((str_replace(",", ".", $Grupo->f("TotalDistr"))));
							$totalDistrito = number_format($totalDistrito, 2,',','.');
							$this->Text(3,$Linha,"T  o  t  a  l     n  a     A  t  i  v  i  d  a  d  e  ");
							//           (       f i n a l                   ) -       t  a  m  n  h  o    
							$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($totalDistrito)),$Linha,$totalDistrito);
							$Grupo->next_record();
							$Linha = 54;
							$this->mudouDistrito = true;
							$this->distrito = $Tabela->f("DESATI");
							$this->addPage(); 
							//$this->SetFont('Arial','B',10);
							//$this->Text(05,50,"Distrito : $distrito");
							//$this->Line(0,51,210.6,51);
							$this->SetFont('Arial','B',06);
						}
					}
					$this->SetY($Linha);
					$Inscricao = $Tabela->f("CODCLI");
					$this->Text(3,$Linha,$Inscricao);
		
					$Cliente = $Tabela->f("DESCLI");
					$this->Text($this->posCliente,$Linha,$Cliente);
		
					$Fatura = $Tabela->f("CODFAT");
					$this->Text($this->posFatura,$Linha,$Fatura);
				
					$Emissao = $Tabela->f("DATEMI");
					$this->Text($this->posEmissao,$Linha,$Emissao);
		
					$ValFat = $Tabela->f("VALFAT");
					$ValFat = (float)((str_replace(",", ".", $ValFat)));
					$ValFat = number_format($ValFat, 2,',','.');
					$this->StringTam;
					//           (       f i n a l                   ) -       t  a  m  n  h  o    
					$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($ValFat)),$Linha,$ValFat);
					
					$Venci = $Tabela->f("DATVNC");
					$this->Text($this->posVenc,$Linha,$Venci);
					
					$Linha+=4;
				}while ($Tabela->next_record());

						if (!$this->mudouDistrito)
						{
							$totalDistrito = (float)((str_replace(",", ".", $Grupo->f("TotalDistr"))));
							$totalDistrito = number_format($totalDistrito, 2,',','.');
							$this->Text(3,$Linha,"T  o  t  a  l     n  a     A  t  i  v  i  d  a  d  e  ");
							//           (       f i n a l                   ) -       t  a  m  n  h  o    
							$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($totalDistrito)),$Linha,$totalDistrito);
							$Linha+=4;
						}

				
				$this->Text(3,$Linha,"T  o  t  a  l     G  e  r  a  l");
				//           (       f i n a l                   ) -       t  a  m  n  h  o    
				$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$Linha,$this->totalGeral);
				
				//$this->SetMargins(5,5,5);
				$this->SetFont('Arial','U',10);
				$this->SetTextColor(0, 0, 0);
				$this->SetAutoPageBreak(1);
				$this->Output();
			break;
			case 415; //                       Relat�rio de Emissao por Sub Atividade
				$mSelect =		"SELECT 
									F.CODCLI,
									to_char(F.DATEMI,'dd/mm/yyyy') AS DATEMI,
									to_char(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
									F.CODFAT,
									F.VALFAT,
									C.DESCLI,
									S.DESSUB,
									(
										SELECT
											SUM(VALFAT)
										FROM
											CADFAT F,
											CADCLI C,
											SUBATI S
										WHERE 
											F.CODCLI=C.CODCLI AND 
											C.SUBATI=S.SUBATI AND
										";
											
				if ($CodDis!="Todos")
				{
					$mSelect .= "C.SUBATI='$CodDis' AND ";
				}
				$mSelect .= 	"			F.MESREF='$this->mesRef'
									) AS TOTGER
								FROM 
									CADFAT F,
									CADCLI C,
									SUBATI S
								WHERE 
									F.CODCLI=C.CODCLI AND 
									C.SUBATI=S.SUBATI AND
								";
								
									
				if ($CodDis!="Todos")
				{
					$mSelect .= "C.SUBATI='$CodDis'                                                      AND ";
				}
				$mSelect .= "	F.MESREF='$this->mesRef'
								ORDER BY 
									C.SUBATI,
									C.DESCLI
								";
				$Tabela->query($mSelect);
				//$this->addPage();
				//$this->Text(05,50,$mSelect);
				$mSelect =	"	SELECT
									SUM(VALFAT) AS TotalDistr
								FROM
									CADFAT F,
									CADCLI C,
									SUBATI S
								WHERE 
									F.CODCLI=C.CODCLI AND 
									C.SUBATI=S.SUBATI AND
							";
				if ($CodDis!="Todos")
				{
					$mSelect .= "	C.SUBATI='$CodDis' AND ";
				}
				$mSelect .= "		F.MESREF='$this->mesRef'
								GROUP BY 
									C.SUBATI
								ORDER BY
									C.SUBATI
							";
							 
				//$this->addPage();
				//$this->Text(05,50,$mSelect);
				$Grupo->query($mSelect);
									
				//$articles = array();
				$Linha = 54;
				$this->SetY($Linha);
				$this->mudouDistrito = true;
				$Tabela->next_record();
				$Grupo->next_record();
				$this->distrito = $Tabela->f("DESSUB");
				$this->addPage();
				//$this->SetFont('Arial','B',10);
				//$this->Text(05,50,"Distrito : $distrito");
				//$this->Line(0,51,210.6,51);
				$this->SetFont('Arial','B',06);
				do 
				{
					$this->mudouDistrito = false;
					if ($this->totalGeral==0)
					{
						$this->totalGeral = (float)((str_replace(",", ".", $Tabela->f("TOTGER"))));
						$this->totalGeral = number_format($this->totalGeral, 2,',','.');
					}
						//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
					if ($this->GetY() >= ($this->fh-14))
					{
						$Linha = 50;
						$this->addPage(); 
					}
					else
					{
						if ($Tabela->f("DESSUB") != $this->distrito)
						{
							$totalDistrito = (float)((str_replace(",", ".", $Grupo->f("TotalDistr"))));
							$totalDistrito = number_format($totalDistrito, 2,',','.');
							$this->Text(3,$Linha,"T  o  t  a  l     n  a     S  u  b   -   A  t  i  v  i  d  a  d  e  ");
							//           (       f i n a l                   ) -       t  a  m  n  h  o    
							$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($totalDistrito)),$Linha,$totalDistrito);
							$Grupo->next_record();
							$Linha = 54;
							$this->mudouDistrito = true;
							$this->distrito = $Tabela->f("DESSUB");
							$this->addPage(); 
							//$this->SetFont('Arial','B',10);
							//$this->Text(05,50,"Distrito : $distrito");
							//$this->Line(0,51,210.6,51);
							$this->SetFont('Arial','B',06);
						}
					}
					$this->SetY($Linha);
					$Inscricao = $Tabela->f("CODCLI");
					$this->Text(3,$Linha,$Inscricao);
		
					$Cliente = $Tabela->f("DESCLI");
					$this->Text($this->posCliente,$Linha,$Cliente);
		
					$Fatura = $Tabela->f("CODFAT");
					$this->Text($this->posFatura,$Linha,$Fatura);
				
					$Emissao = $Tabela->f("DATEMI");
					$this->Text($this->posEmissao,$Linha,$Emissao);
		
					$ValFat = $Tabela->f("VALFAT");
					$ValFat = (float)((str_replace(",", ".", $ValFat)));
					$ValFat = number_format($ValFat, 2,',','.');
					$this->StringTam;
					//           (       f i n a l                   ) -       t  a  m  n  h  o    
					$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($ValFat)),$Linha,$ValFat);
					
					$Venci = $Tabela->f("DATVNC");
					$this->Text($this->posVenc,$Linha,$Venci);
					
					$Linha+=4;
				}while ($Tabela->next_record());

						if (!$this->mudouDistrito)
						{
							$totalDistrito = (float)((str_replace(",", ".", $Grupo->f("TotalDistr"))));
							$totalDistrito = number_format($totalDistrito, 2,',','.');
							$this->Text(3,$Linha,"T  o  t  a  l     n  a     S  u  b     -     A  t  i  v  i  d  a  d  e");
							//           (       f i n a l                   ) -       t  a  m  n  h  o    
							$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($totalDistrito)),$Linha,$totalDistrito);
							$Linha+=4;
						}

				
				$this->Text(3,$Linha,"T  o  t  a  l     G  e  r  a  l");
				//           (       f i n a l                   ) -       t  a  m  n  h  o    
				$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$Linha,$this->totalGeral);
				
				//$this->SetMargins(5,5,5);
				$this->SetFont('Arial','U',10);
				$this->SetTextColor(0, 0, 0);
				$this->SetAutoPageBreak(1);
				$this->Output();
			break;
		}
		
		if (($rel > 420) && ($rel < 426)) //                       Relat�rio de Receita: 
		{
			//$mArqMov = fopen('c:\sistfat\relat.txt','w');
			$select = "SELECT 
								F.CODCLI,
								to_char(F.DATPGT,'dd/mm/yyyy') AS DATPGT,
								to_char(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
								F.CODFAT,
								F.VALFAT,
								F.MESREF,
								F.RET_INSS,
								(F.VALPGT - F.VALFAT) AS ACRESCIMO,
								F.VALPGT,
								C.DESCLI,";
						if ($rel==422)	// por Divis�o
						{
							$select .="
								C.CODUNI,
								T.DESCRI,";
						}
						elseif ($rel==423)	// por Distrito
						{
							$select .="
								C.CODDIS,
								T.DESDIS,";
						}
						elseif ($rel==424)	// por Atividade
						{
							$select .="
								C.GRPATI,
								T.DESATI,";
						}
						elseif ($rel==425)	// por Sub Atividade
						{
							$select .="
								C.SUBATI,
								T.DESSUB,";
						}
						$select .="
								(
									SELECT
										SUM(VALFAT)
									FROM
										CADFAT F,
										CADCLI C";
						if ($rel==422)	// por Divis�o
						{
							$select .=",
										TABUNI T";
						}
						elseif ($rel==423)	// por Distrito
						{
							$select .=",
										TABDIS T";
						}
						elseif ($rel==424)	// por Atividade
						{
							$select .=",
										GRPATI T";
						}
						elseif ($rel==425)	// por SUB ATIVIDADE
						{
							$select .=",
										SUBATI T";
						}
						$select .="
									WHERE 
										F.VALPGT <> 0     AND
										F.CODCLI=C.CODCLI AND ";
						if ($rel==422)	// por Divis�o
						{
							$select .="
										C.CODUNI=T.CODUNI AND";
							if ($CodDis!="Todos")
							{
								$select .= "	T.CODUNI='$CodDis' AND ";
							}
						}
						elseif ($rel==423)	// por Distrito
						{
							$select .="
										C.CODDIS=T.CODDIS AND";
							if ($CodDis!="Todos")
							{
								$select .= "	T.CODDIS='$CodDis' AND ";
							}
						}
						elseif ($rel==424)	// por ATIVIDADE
						{
							$select .="
										C.GRPATI=T.GRPATI AND";
							if ($CodDis!="Todos")
							{
								$select .= "	T.GRPATI='$CodDis' AND ";
							}
						}
						elseif ($rel==425)	// por SUB ATIVIDADE
						{
							$select .="
										C.SUBATI=T.SUBATI AND";
							if ($CodDis!="Todos")
							{
								$select .= "	T.SUBATI='$CodDis' AND ";
							}
						}
						$select .="
										TO_CHAR(F.DATPGT,'MM/YYYY')='$this->mesRef'
								) AS TOTGER,
								(
									SELECT
										SUM((F.VALPGT - F.VALFAT))
									FROM
										CADFAT F,
										CADCLI C";
						if ($rel==422)	// por Divis�o
						{
							$select .=",
										TABUNI T";
						}
						elseif ($rel==423)	// por Distrito
						{
							$select .=",
										TABDIS T";
						}
						elseif ($rel==424)	// por ATIVIDADE
						{
							$select .=",
										GRPATI T";
						}
						elseif ($rel==425)	// por SUB ATIVIDADE
						{
							$select .=",
										SUBATI T";
						}
						$select .="
									WHERE 
										F.VALPGT <> 0     AND
										F.CODCLI=C.CODCLI AND ";
						if ($rel==422)	// por Divis�o
						{
							$select .="
										C.CODUNI=T.CODUNI AND";
							if ($CodDis!="Todos")
							{
								$select .= "	T.CODUNI='$CodDis' AND ";
							}
						}
						elseif ($rel==423)	// por Distrito
						{
							$select .="
										C.CODDIS=T.CODDIS AND";
							if ($CodDis!="Todos")
							{
								$select .= "	T.CODDIS='$CodDis' AND ";
							}
						}
						elseif ($rel==424)	// por ATIVIDADE
						{
							$select .="
										C.GRPATI=T.GRPATI AND";
							if ($CodDis!="Todos")
							{
								$select .= "	T.GRPATI='$CodDis' AND ";
							}
						}
						elseif ($rel==425)	// por SUB ATIVIDADE
						{
							$select .="
										C.SUBATI=T.SUBATI AND";
							if ($CodDis!="Todos")
							{
								$select .= "	T.SUBATI='$CodDis' AND ";
							}
						}
						$select .="
										TO_CHAR(F.DATPGT,'MM/YYYY')='$this->mesRef'
								) AS TOTGERACR,
								(
									SELECT
										SUM(F.VALPGT)
									FROM
										CADFAT F,
										CADCLI C";
						if ($rel==422)	// por Divis�o
						{
							$select .=",
										TABUNI T";
						}
						elseif ($rel==423)	// por Distrito
						{
							$select .=",
										TABDIS T";
						}
						elseif ($rel==424)	// por ATIVIDADE
						{
							$select .=",
										GRPATI T";
						}
						elseif ($rel==425)	// por SUB ATIVIDADE
						{
							$select .=",
										SUBATI T";
						}
						$select .="
									WHERE 
										F.VALPGT <> 0     AND
										F.CODCLI=C.CODCLI AND ";
						if ($rel==422)	// por Divis�o
						{
							$select .="
										C.CODUNI=T.CODUNI AND";
							if ($CodDis!="Todos")
							{
								$select .= "	T.CODUNI='$CodDis' AND ";
							}
						}
						elseif ($rel==423)	// por Distrito
						{
							$select .="
										C.CODDIS=T.CODDIS AND";
							if ($CodDis!="Todos")
							{
								$select .= "	T.CODDIS='$CodDis' AND ";
							}
						}
						elseif ($rel==424)	// por ATIVIDADE
						{
							$select .="
										C.GRPATI=T.GRPATI AND";
							if ($CodDis!="Todos")
							{
								$select .= "	T.GRPATI='$CodDis' AND ";
							}
						}
						elseif ($rel==425)	// por SUB ATIVIDADE
						{
							$select .="
										C.SUBATI=T.SUBATI AND";
							if ($CodDis!="Todos")
							{
								$select .= "	T.SUBATI='$CodDis' AND ";
							}
						}
						$select .="
										TO_CHAR(F.DATPGT,'MM/YYYY')='$this->mesRef'
								) AS TOTGERPGT
							FROM 
								CADFAT F,
								CADCLI C";
						if ($rel==422)	// por Divis�o
						{
							$select .=",
										TABUNI T";
						}
						elseif ($rel==423)	// por Distrito
						{
							$select .=",
										TABDIS T";
						}
						elseif ($rel==424)	// por ATIVIDADE
						{
							$select .=",
										GRPATI T";
						}
						elseif ($rel==425)	// por SUB ATIVIDADE
						{
							$select .=",
										SUBATI T";
						}
						$select .="
							WHERE 
								F.VALPGT <> 0     AND
								F.CODCLI=C.CODCLI AND ";
						if ($rel==422)	// por Divis�o
						{
							$select .="
										C.CODUNI=T.CODUNI AND";
							if ($CodDis!="Todos")
							{
								$select .= "	T.CODUNI='$CodDis' AND ";
							}
						}
						elseif ($rel==423)	// por Distrito
						{
							$select .="
										C.CODDIS=T.CODDIS AND";
							if ($CodDis!="Todos")
							{
								$select .= "	T.CODDIS='$CodDis' AND ";
							}
						}
						elseif ($rel==424)	// por ATIVIDADE
						{
							$select .="
										C.GRPATI=T.GRPATI AND";
							if ($CodDis!="Todos")
							{
								$select .= "	T.GRPATI='$CodDis' AND ";
							}
						}
						elseif ($rel==425)	// por SUB ATIVIDADE
						{
							$select .="
										C.SUBATI=T.SUBATI AND";
							if ($CodDis!="Todos")
							{
								$select .= "	T.SUBATI='$CodDis' AND ";
							}
						}
						$select .="
								TO_CHAR(F.DATPGT,'MM/YYYY')='$this->mesRef'
							ORDER BY ";
						if ($rel==422)	// por Divis�o
						{
							$select .="
								CODUNI,
								DESCLI";
						}
						elseif ($rel==423)	// por Distrito
						{
							$select .="
								CODDIS,
								DESCLI";
						}
						elseif ($rel==424)	// por ATIVIDADE
						{
							$select .="
								GRPATI,
								DESCLI";
						}
						elseif ($rel==425)	// por SUB ATIVIDADE
						{
							$select .="
								SUBATI,
								DESCLI";
						}
						else               // POR INSCRI��O
						{
							$select .="
								MESREF,CODCLI";
						}
			//$articles = array();
			$Tabela->query($select);
			//fwrite($mArqMov,$select.'\r\n');
			if ($rel == 422) // por Divis�o
			{
				$mSelect =	"	SELECT
									SUM(F.VALFAT) AS TotalDistr,
									SUM((F.VALPGT - F.VALFAT)) as totReceita,
									SUM(F.VALPGT) as totRecebido
								FROM
									CADFAT F,
									CADCLI C,
									TABUNI T
								WHERE 
									F.VALPGT <> 0     AND
									F.CODCLI=C.CODCLI AND 
									C.CODUNI=T.CODUNI AND
							";
				if ($CodDis!="Todos")
				{
					$mSelect .= "	C.CODUNI='$CodDis' AND ";
				}
				$mSelect .= "		TO_CHAR(F.DATPGT,'MM/YYYY')='$this->mesRef'
								GROUP BY 
									C.CODUNI
								ORDER BY
									C.CODUNI
							";
				//fwrite($mArqMov,$mSelect.'\r\n');
				$Grupo->query($mSelect);
			}
			elseif ($rel == 423) // por Distrito
			{
				$mSelect =	"	SELECT
									SUM(F.VALFAT) AS TotalDistr,
									SUM((F.VALPGT - F.VALFAT)) as totReceita,
									SUM(F.VALPGT) as totRecebido
								FROM
									CADFAT F,
									CADCLI C,
									TABDIS T
								WHERE 
									F.VALPGT <> 0     AND
									F.CODCLI=C.CODCLI AND 
									C.CODDIS=T.CODDIS AND
							";
				if ($CodDis!="Todos")
				{
					$mSelect .= "	C.CODDIS='$CodDis' AND ";
				}
				$mSelect .= "		TO_CHAR(F.DATPGT,'MM/YYYY')='$this->mesRef'
								GROUP BY 
									C.CODDIS
								ORDER BY
									C.CODDIS
							";
				//fwrite($mArqMov,$mSelect.'\r\n');
				$Grupo->query($mSelect);
			}
			elseif ($rel == 424) // por ATIVIDADE
			{
				$mSelect =	"	SELECT
									SUM(F.VALFAT) AS TotalDistr,
									SUM((F.VALPGT - F.VALFAT)) as totReceita,
									SUM(F.VALPGT) as totRecebido
								FROM
									CADFAT F,
									CADCLI C,
									GRPATI T
								WHERE 
									F.VALPGT <> 0     AND
									F.CODCLI=C.CODCLI AND 
									C.GRPATI=T.GRPATI AND
							";
				if ($CodDis!="Todos")
				{
					$mSelect .= "	C.GRPATI='$CodDis' AND ";
				}
				$mSelect .= "		TO_CHAR(F.DATPGT,'MM/YYYY')='$this->mesRef'
								GROUP BY 
									C.GRPATI
								ORDER BY
									C.GRPATI
							";
				//fwrite($mArqMov,$mSelect.'\r\n');
				$Grupo->query($mSelect);
			}
			elseif ($rel == 425) // por SUB ATIVIDADE
			{
				$mSelect =	"	SELECT
									SUM(F.VALFAT) AS TotalDistr,
									SUM((F.VALPGT - F.VALFAT)) as totReceita,
									SUM(F.VALPGT) as totRecebido
								FROM
									CADFAT F,
									CADCLI C,
									SUBATI T
								WHERE 
									F.VALPGT <> 0     AND
									F.CODCLI=C.CODCLI AND 
									C.SUBATI=T.SUBATI AND
							";
				if ($CodDis!="Todos")
				{
					$mSelect .= "	C.SUBATI='$CodDis' AND ";
				}
				$mSelect .= "		TO_CHAR(F.DATPGT,'MM/YYYY')='$this->mesRef'
								GROUP BY 
									C.SUBATI
								ORDER BY
									C.SUBATI
							";
				//fwrite($mArqMov,$mSelect.'\r\n');
				$Grupo->query($mSelect);
			}

			//fclose($mArqMov);
			$Tabela->next_record();
			$Grupo->next_record();
			if ($rel>421) // Op��es com quebra de grupos
			{
				/*
				   Inicio da impress�o do relat�rio que pode ser por inscri��o sem quebras(grupos de dados tais como funcion�rios
				   por se��o) ou os demais com suas repectivas quebras(Divis�o,Distrito,Atividade,Sub Atividade,Servi�o e Sub Ser-
				   vi�o.Neste ponto inicia-se a impress�o para o caso onde h� quebras e a referida quebra � mostrada no cabe�alho
				   do relat�rio. � tamb�m apresentado al�m de uma totaliza��o geral, uma totaliza��o por quebra de grupo.
				*/
				if ($rel==422)// por Divis�o
				{
					$this->distrito = $Tabela->f("DESCRI");// Nome geral da quebra que pode ser un dos acima citados
					/*
					C�digo pr� fixado do campo da tabela pelo qual � definido como sendo o grupo para quebra.S� varia quando h� 
					mudan�a	no grupo.
					*/
					$codigoVar = $Tabela->f("CODUNI");
					/*
		�			C�digo vari�vel do campo da tabela pelo qual � definido como sendo o grupo para quebra.Varia a todo o momento.
					*/
					$codigo = $Tabela->f("CODUNI");
				}
				elseif ($rel==423)// por Distrito
				{
					$this->distrito = $Tabela->f("DESDIS");// Nome geral da quebra que pode ser un dos acima citados
					/*
					C�digo pr� fixado do campo da tabela pelo qual � definido como sendo o grupo para quebra.S� varia quando h� 
					mudan�a	no grupo.
					*/
					$codigoVar = $Tabela->f("CODDIS");
					/*
		�			C�digo vari�vel do campo da tabela pelo qual � definido como sendo o grupo para quebra.Varia a todo o momento.
					*/
					$codigo = $Tabela->f("CODDIS");
				}
				elseif ($rel==424)// por ATIVIDADE
				{
					$this->distrito = $Tabela->f("DESATI");// Nome geral da quebra que pode ser un dos acima citados
					/*
					C�digo pr� fixado do campo da tabela pelo qual � definido como sendo o grupo para quebra.S� varia quando h� 
					mudan�a	no grupo.
					*/
					$codigoVar = $Tabela->f("GRPATI");
					/*
		�			C�digo vari�vel do campo da tabela pelo qual � definido como sendo o grupo para quebra.Varia a todo o momento.
					*/
					$codigo = $Tabela->f("GRPATI");
				}
				elseif ($rel==425)// por SUB ATIVIDADE
				{
					$this->distrito = $Tabela->f("DESSUB");// Nome geral da quebra que pode ser un dos acima citados
					/*
					C�digo pr� fixado do campo da tabela pelo qual � definido como sendo o grupo para quebra.S� varia quando h� 
					mudan�a	no grupo.
					*/
					$codigoVar = $Tabela->f("SUBATI");
					/*
		�			C�digo vari�vel do campo da tabela pelo qual � definido como sendo o grupo para quebra.Varia a todo o momento.
					*/
					$codigo = $Tabela->f("SUBATI");
				}

				//Controle para imprimir a quebra, somente uma vez.(Dados podem preencher mais de uma p�gina)
				$this->mudouDistrito = true; 
				$Linha = 54;
				$this->SetY($Linha);
				$this->addPage();// Adiciona uma p�gina.Chama automaticamente Header() e Footer().
				//$this->SetFont('Arial','B',05);
			}
			else
			{
			    /*
				   In�cio da impress�o do relatorio de receitas por inscri��o.Este n�o apresenta quebras por grupo. Apenas apresen-
				   uma totaliza��o geral.
				*/
				$Linha = 50;
				$this->SetY($Linha);
				$this->addPage();
			}
			do 
			{
				if ($rel>421) // O abaixo tem haver apenas com as quebras por grupo.
				{
					//Controle para imprimir a quebra, somente uma vez.(Dados podem preencher mais de uma p�gina)
					$this->mudouDistrito = false;
				}
				/*
					Garante que os totais sejam pegos somente uma vez na medida em que os registros v�o avan�ando.
				*/
				if ($this->totalGeral==0)
				{
					$this->totalGeral = (float)((str_replace(",", ".", $Tabela->f("TOTGER"))));
					$this->totalGeral = number_format($this->totalGeral, 2,',','.');
					/*
						O tratamento dado a totalGeral acima � igualmente feito nos campos abaixo no final desta rotina.
					*/
					$totAcresc = $Tabela->f("TOTGERACR");
					$totValPGT = $Tabela->f("TOTGERPGT");
				}
				//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
				if ($this->GetY() >= ($this->fh-12))
				{
					$Linha = 50;
					$this->addPage(); 
				}
				else
				{
					if ($rel==422) // POR DIVIS�O
					{
						$codigo = $Tabela->f("CODUNI");
						$tituloGrupo = "T  o  t  a  i  s    n  a    D  i  v  i  s  �  o";
						$this->distrito = $Tabela->f("DESCRI");
					}
					elseif ($rel==423) // POR DISTRITO
					{
						$codigo = $Tabela->f("CODDIS");
						$tituloGrupo = "T  o  t  a  i  s    n  o    D  i  s  t  r  i  t  o";
						$this->distrito = $Tabela->f("DESDIS");
					}
					elseif ($rel==424) // POR ATIVIDADE
					{
						$codigo = $Tabela->f("GRPATI");
						$tituloGrupo = "T  o  t  a  i  s    n  a    A  t  i  v  i  d  a  d  e";
						$this->distrito = $Tabela->f("DESATI");
					}
					elseif ($rel==425) // POR SUB ATIVIDADE
					{
						$codigo = $Tabela->f("SUBATI");
						$tituloGrupo = "T  o  t  a  i  s    n  a    S  u  b     A  t  i  v  i  d  a  d  e";
						$this->distrito = $Tabela->f("DESSUB");
					}
					// Quebra a p�gina no grupo
					if (($rel>421) && ($codigo != $codigoVar))
					{
						
						$totalDistrito = (float)((str_replace(",", ".", $Grupo->f("TotalDistr"))));
						$totalDistrito = number_format($totalDistrito, 2,',','.');
						$this->Text(3,$Linha,$tituloGrupo);
						//           (       f i n a l                   ) -       t  a  m  n  h  o    
						$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
						$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($totalDistrito)),$Linha,$totalDistrito);

						$acresc = $Grupo->f("totReceita");
						$acresc = (float)((str_replace(",", ".", $acresc)));
						$acresc = number_format($acresc, 2,',','.');
						
						$this->StringTam = $this->GetStringWidth(' ACR�SCIMO ');
						$this->Text((($this->posAcres + $this->StringTam) - $this->GetStringWidth($acresc)),$Linha,$acresc);
						
						$valPGT = $Grupo->f("totRecebido");
						$valPGT = (float)((str_replace(",", ".", $valPGT)));
						$valPGT = number_format($valPGT, 2,',','.');
						
						$this->StringTam = $this->GetStringWidth('   RECEBIDO   ');
						$this->Text((($this->posRec + $this->StringTam) - $this->GetStringWidth($valPGT))-1,$Linha,$valPGT);
						
						$Grupo->next_record();
						
						$Linha = 54;
						$this->mudouDistrito = true;
						//$this->distrito = $codigo;
						$codigoVar = $codigo;
						$this->addPage(); 
						$this->SetFont('Arial','B',05);
					}
				}
				$this->SetY($Linha);
				$Inscricao = $Tabela->f("CODCLI");
				$this->Text(3,$Linha,$Inscricao);
				$Cliente = $Tabela->f("DESCLI");
				$this->Text($this->posCliente,$Linha,$Cliente);
				$Fatura = $Tabela->f("CODFAT");
				$this->Text($this->posFatura,$Linha,$Fatura);
			
				$mesRef = $Tabela->f("MESREF");
				$this->Text($this->posMesAno+1,$Linha,$mesRef);
				$Venci = $Tabela->f("DATVNC");
				$this->Text($this->posVenc+1,$Linha,$Venci);
				$ValFat = $Tabela->f("VALFAT");
				$ValFat = (float)((str_replace(",", ".", $ValFat)));
				$ValFat = number_format($ValFat, 2,',','.');
				
				$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
				
				//           (       f i n a l                   ) -       t  a  m  n  h  o    
				$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($ValFat)),$Linha,$ValFat);
				
				$retINSS = $Tabela->f("RET_INSS");
				$retINSS = (float)((str_replace(",", ".", $retINSS)));
				$retINSS = number_format($retINSS, 2,',','.');
				$this->Text($this->posINSS,$Linha,$retINSS);
				
				$datPGT = $Tabela->f("DATPGT");
				$this->Text($this->posPGT+1,$Linha,$datPGT);
				
				$acresc = $Tabela->f("ACRESCIMO");
				$acresc = (float)((str_replace(",", ".", $acresc)));
				$acresc = number_format($acresc, 2,',','.');
				$this->StringTam = $this->GetStringWidth(' ACR�SCIMO ');
				$this->Text((($this->posAcres + $this->StringTam) - $this->GetStringWidth($acresc)),$Linha,$acresc);
	
				$valPGT = $Tabela->f("VALPGT");
				$valPGT = (float)((str_replace(",", ".", $valPGT)));
				$valPGT = number_format($valPGT, 2,',','.');
				$this->StringTam = $this->GetStringWidth('   RECEBIDO   ');
				$this->Text((($this->posRec + $this->StringTam) - $this->GetStringWidth($valPGT))-1,$Linha,$valPGT);
				
				$Linha+=4;
			}while ($Tabela->next_record());
			//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
			if ($this->GetY() >= ($this->fh-12))
			{
				$Linha = 50;
				$this->addPage(); 
			}
					if ((!$this->mudouDistrito) && ($rel>421))
					{
						if ($rel==422) // por divisao
						{
							$codigo = $Tabela->f("CODUNI");
							$tituloGrupo = "T  o  t  a  i  s    n  a    D  i  v  i  s  �  o";
							$this->distrito = $Tabela->f("DESCRI");
						}
						elseif ($rel==423) // por distrito
						{
							$codigo = $Tabela->f("CODDIS");
							$tituloGrupo = "T  o  t  a  i  s    n  o    D  i  s  t  r  i  t  o";
							$this->distrito = $Tabela->f("DESDIS");
						}
						elseif ($rel==424) // por ATIVIDADDE
						{
							$codigo = $Tabela->f("GRPATI");
							$tituloGrupo = "T  o  t  a  i  s    n  a    A  t  i  v  i  d  a  d  e";
							$this->distrito = $Tabela->f("DESATI");
						}
						elseif ($rel==425) // por SUBATIVIDADE
						{
							$codigo = $Tabela->f("SUBATI");
							$tituloGrupo = "T  o  t  a  i  s    n  a    S  u  b     A  t  i  v  i  d  a  d  e";
							$this->distrito = $Tabela->f("DESSUB");
						}
						//$Grupo->next_record();
						$totalDistrito = (float)((str_replace(",", ".", $Grupo->f("TotalDistr"))));
						$totalDistrito = number_format($totalDistrito, 2,',','.');
						$this->Text(3,$Linha,$tituloGrupo);
						//           (       f i n a l                   ) -       t  a  m  n  h  o    
						$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
						$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($totalDistrito)),$Linha,$totalDistrito);

						$acresc = $Grupo->f("totReceita");
						$acresc = (float)((str_replace(",", ".", $acresc)));
						$acresc = number_format($acresc, 2,',','.');
						
						$this->StringTam = $this->GetStringWidth(' ACR�SCIMO ');
						$this->Text((($this->posAcres + $this->StringTam) - $this->GetStringWidth($acresc)),$Linha,$acresc);
						
						$valPGT = $Grupo->f("totRecebido");
						$valPGT = (float)((str_replace(",", ".", $valPGT)));
						$valPGT = number_format($valPGT, 2,',','.');
						
						$this->StringTam = $this->GetStringWidth('   RECEBIDO   ');
						$this->Text((($this->posRec + $this->StringTam) - $this->GetStringWidth($valPGT))-1,$Linha,$valPGT);

						$Linha += 4;
						$this->SetFont('Arial','B',05);
					}

			$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
			$this->Text(3,$Linha,"T  o  t  a  l  i  z  a  �  �  e  s  ");
			//           (       f i n a l                   ) -       t  a  m  n  h  o    
			$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$Linha,$this->totalGeral);
			
			$totAcresc = (float)((str_replace(",", ".", $totAcresc)));
			$totAcresc = number_format($totAcresc, 2,',','.');
			$this->StringTam = $this->GetStringWidth(' ACR�SCIMO ');
			$this->Text((($this->posAcres + $this->StringTam) - $this->GetStringWidth($totAcresc)),$Linha,$totAcresc);
	
			$totValPGT = (float)((str_replace(",", ".", $totValPGT)));
			$totValPGT = number_format($totValPGT, 2,',','.');
			$this->StringTam = $this->GetStringWidth('   RECEBIDO   ');
			$this->Text((($this->posRec + $this->StringTam) - $this->GetStringWidth($totValPGT))-1,$Linha,$totValPGT);

			//$this->SetMargins(5,5,5);
			$this->SetFont('Arial','U',10);
			$this->SetTextColor(0, 0, 0);
			$this->SetAutoPageBreak(1);
			$this->Output();
		}
		if (($rel > 425) && ($rel < 428)) //   Relat�rio de Receita: Servi�os e Sub-Servi�os 
		{
			//$mArqMov = fopen('c:\sistfat\relat.txt','w');
			$select = "SELECT 
								F.CODCLI,
								to_char(F.DATPGT,'dd/mm/yyyy') AS DATPGT,
								to_char(F.DATVNC,'dd/mm/yyyy') AS DATVNC,
								F.CODFAT,
								F.VALFAT,
								F.MESREF,
								F.RET_INSS,
								(F.VALPGT - F.VALFAT) AS ACRESCIMO,
								F.VALPGT,
								C.DESCLI,";
						if ($rel==426)	// por servi�os
						{
							$select .="
								M.GRPSER,
								G.DESSER,";
						}
						elseif ($rel==427)	// por SUB SERVI�OS
						{
							$select .="
								M.SUBSER,
								S.SUBRED,";
						}
						$select .="
								(
									SELECT
										SUM(VALFAT)
									FROM
										CADFAT F,
										CADCLI C,
										MOVFAT M";
						if ($rel==426)	// por Divis�o
						{
							$select .=",
										GRPSER G";
						}
						elseif ($rel==427)	// por Distrito
						{
							$select .=",
										SUBSER S";
						}
						$select .="
									WHERE
										M.VALPGT <> 0     AND
										M.CODFAT=F.CODFAT AND
										F.CODCLI=C.CODCLI AND ";
						if ($rel==426)	// por Divis�o
						{
							$select .="
										M.GRPSER=G.GRPSER AND";
							if ($CodDis!="Todos")
							{
								$select .= "	M.GRPSER='$CodDis' AND ";
							}
						}
						elseif ($rel==427)	// por Distrito
						{
							$select .="
										M.SUBSER=S.SUBSER AND";
							if ($CodDis!="Todos")
							{
								$select .= "	M.SUBSER='$CodDis' AND ";
							}
						}
						$select .="
										TO_CHAR(M.DATPGT,'MM/YYYY')='$this->mesRef'
								) AS TOTGER,
								(
									SELECT
										SUM((F.VALPGT - F.VALFAT))
									FROM
										CADFAT F,
										CADCLI C,
										MOVFAT M
									";
						if ($rel==426)	// por Divis�o
						{
							$select .=",
										GRPSER G";
						}
						elseif ($rel==427)	// por Distrito
						{
							$select .=",
										SUBSER S";
						}
						$select .="
									WHERE 
										M.VALPGT <> 0     AND
									    M.CODFAT=F.CODFAT AND
										F.CODCLI=C.CODCLI AND ";
						if ($rel==426)	// por Divis�o
						{
							$select .="
										M.GRPSER=G.GRPSER AND";
							if ($CodDis!="Todos")
							{
								$select .= "	M.GRPSER='$CodDis' AND ";
							}
						}
						elseif ($rel==427)	// por Distrito
						{
							$select .="
										M.SUBSER=S.SUBSER AND";
							if ($CodDis!="Todos")
							{
								$select .= "	M.SUBSER='$CodDis' AND ";
							}
						}
						$select .="
										TO_CHAR(M.DATPGT,'MM/YYYY')='$this->mesRef'
								) AS TOTGERACR,
								(
									SELECT
										SUM(F.VALPGT)
									FROM
										CADFAT F,
										CADCLI C,
										MOVFAT M";
						if ($rel==426)	// por Divis�o
						{
							$select .=",
										GRPSER G";
						}
						elseif ($rel==427)	// por Distrito
						{
							$select .=",
										SUBSER S";
						}
						$select .="
									WHERE 
										M.VALPGT <> 0     AND
									    M.CODFAT=F.CODFAT AND
										F.CODCLI=C.CODCLI AND ";
						if ($rel==426)	// por Divis�o
						{
							$select .="
										M.GRPSER=G.GRPSER AND";
							if ($CodDis!="Todos")
							{
								$select .= "	M.GRPSER='$CodDis' AND ";
							}
						}
						elseif ($rel==427)	// por Distrito
						{
							$select .="
										M.SUBSER=S.SUBSER AND";
							if ($CodDis!="Todos")
							{
								$select .= "	M.SUBSER='$CodDis' AND ";
							}
						}
						$select .="
										TO_CHAR(M.DATPGT,'MM/YYYY')='$this->mesRef'
								) AS TOTGERPGT
							FROM 
								CADFAT F,
								CADCLI C,
								MOVFAT M";
						if ($rel==426)	// por Divis�o
						{
							$select .=",
										GRPSER G";
						}
						elseif ($rel==427)	// por Distrito
						{
							$select .=",
										SUBSER S";
						}
						$select .="
							WHERE 
								M.VALPGT <> 0     AND
							    M.CODFAT=F.CODFAT AND
								F.CODCLI=C.CODCLI AND ";
						if ($rel==426)	// por Divis�o
						{
							$select .="
										M.GRPSER=G.GRPSER AND";
							if ($CodDis!="Todos")
							{
								$select .= "	M.GRPSER='$CodDis' AND ";
							}
						}
						elseif ($rel==427)	// por Distrito
						{
							$select .="
										M.SUBSER=S.SUBSER AND";
							if ($CodDis!="Todos")
							{
								$select .= "	M.SUBSER='$CodDis' AND ";
							}
						}
						$select .="
								TO_CHAR(M.DATPGT,'MM/YYYY')='$this->mesRef'
							ORDER BY ";
						if ($rel==426)	// por Divis�o
						{
							$select .="
								GRPSER,
								TO_CHAR(M.DATPGT,'MM/YYYY')";
						}
						elseif ($rel==427)	// por Distrito
						{
							$select .="
								SUBSER,
								TO_CHAR(M.DATPGT,'MM/YYYY')";
						}
			//$articles = array();
			$Tabela->query($select);
			//fwrite($mArqMov,$select.'\r\n');
			if ($rel == 426) // por Divis�o
			{
				$mSelect =	"	SELECT
									SUM(F.VALFAT) AS TotalDistr,
									SUM((F.VALPGT - F.VALFAT)) as totReceita,
									SUM(F.VALPGT) as totRecebido
								FROM
									CADFAT F,
									CADCLI C,
									MOVFAT M,
									GRPSER G
								WHERE 
									M.VALPGT <> 0     AND
								    G.GRPSER=M.GRPSER AND
								    M.CODFAT=F.CODFAT AND
									F.CODCLI=C.CODCLI AND
							";
				if ($CodDis!="Todos")
				{
					$mSelect .= "	M.GRPSER='$CodDis' AND ";
				}
				$mSelect .= "		TO_CHAR(M.DATPGT,'MM/YYYY')='$this->mesRef'
								GROUP BY 
									M.GRPSER
								ORDER BY
									M.GRPSER
							";
				//fwrite($mArqMov,$mSelect.'\r\n');
				$Grupo->query($mSelect);
			}
			elseif ($rel == 427) // por Distrito
			{
				$mSelect =	"	SELECT
									SUM(F.VALFAT) AS TotalDistr,
									SUM((F.VALPGT - F.VALFAT)) as totReceita,
									SUM(F.VALPGT) as totRecebido
								FROM
									CADFAT F,
									CADCLI C,
									MOVFAT M,
									SUBSER S
								WHERE 
									M.VALPGT <> 0     AND
								    S.SUBSER=M.SUBSER AND
								    M.CODFAT=F.CODFAT AND
									F.CODCLI=C.CODCLI AND
							";
				if ($CodDis!="Todos")
				{
					$mSelect .= "	M.SUBSER='$CodDis' AND ";
				}
				$mSelect .= "		TO_CHAR(M.DATPGT,'MM/YYYY')='$this->mesRef'
								GROUP BY 
									M.SUBSER
								ORDER BY
									M.SUBSER
							";
				//fwrite($mArqMov,$mSelect.'\r\n');
				$Grupo->query($mSelect);
			}

			//fclose($mArqMov);
			$Tabela->next_record();
			$Grupo->next_record();
			if ($rel>421) // Op��es com quebra de grupos
			{
				/*
				   Inicio da impress�o do relat�rio que pode ser por inscri��o sem quebras(grupos de dados tais como funcion�rios
				   por se��o) ou os demais com suas repectivas quebras(Divis�o,Distrito,Atividade,Sub Atividade,Servi�o e Sub Ser-
				   vi�o.Neste ponto inicia-se a impress�o para o caso onde h� quebras e a referida quebra � mostrada no cabe�alho
				   do relat�rio. � tamb�m apresentado al�m de uma totaliza��o geral, uma totaliza��o por quebra de grupo.
				*/
				if ($rel==426)// por Divis�o
				{
					$this->distrito = $Tabela->f("DESSER");// Nome geral da quebra que pode ser un dos acima citados
					/*
					C�digo pr� fixado do campo da tabela pelo qual � definido como sendo o grupo para quebra.S� varia quando h� 
					mudan�a	no grupo.
					*/
					$codigoVar = $Tabela->f("GRPSER");
					/*
		�			C�digo vari�vel do campo da tabela pelo qual � definido como sendo o grupo para quebra.Varia a todo o momento.
					*/
					$codigo = $Tabela->f("GRPSER");
				}
				elseif ($rel==427)// por Distrito
				{
					$this->distrito = $Tabela->f("SUBRED");// Nome geral da quebra que pode ser un dos acima citados
					/*
					C�digo pr� fixado do campo da tabela pelo qual � definido como sendo o grupo para quebra.S� varia quando h� 
					mudan�a	no grupo.
					*/
					$codigoVar = $Tabela->f("SUBSER");
					/*
		�			C�digo vari�vel do campo da tabela pelo qual � definido como sendo o grupo para quebra.Varia a todo o momento.
					*/
					$codigo = $Tabela->f("CODDIS");
				}

				//Controle para imprimir a quebra, somente uma vez.(Dados podem preencher mais de uma p�gina)
				$this->mudouDistrito = true; 
				$Linha = 54;
				$this->SetY($Linha);
				$this->addPage();// Adiciona uma p�gina.Chama automaticamente Header() e Footer().
				//$this->SetFont('Arial','B',05);
			}
			else
			{
			    /*
				   In�cio da impress�o do relatorio de receitas por inscri��o.Este n�o apresenta quebras por grupo. Apenas apresen-
				   uma totaliza��o geral.
				*/
				$Linha = 54;
				$this->SetY($Linha);
				$this->addPage();
			}
			do 
			{
				//if ($rel>421) // O abaixo tem haver apenas com as quebras por grupo.
				//{
					//Controle para imprimir a quebra, somente uma vez.(Dados podem preencher mais de uma p�gina)
				//	$this->mudouDistrito = false;
				//}
				/*
					Garante que os totais sejam pegos somente uma vez na medida em que os registros v�o avan�ando.
				*/
				if ($this->totalGeral==0)
				{
					$this->totalGeral = (float)((str_replace(",", ".", $Tabela->f("TOTGER"))));
					$this->totalGeral = number_format($this->totalGeral, 2,',','.');
					/*
						O tratamento dado a totalGeral acima � igualmente feito nos campos abaixo no final desta rotina.
					*/
					$totAcresc = $Tabela->f("TOTGERACR");
					$totValPGT = $Tabela->f("TOTGERPGT");
				}
				//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
				if ($this->GetY() >= ($this->fh-12))
				{
					$Linha = 54;
					$this->addPage(); 
				}
				else
				{
					if ($rel==426) // POR DIVIS�O
					{
						$codigo = $Tabela->f("GRPSER");
						$tituloGrupo = "T  o  t  a  i  s    n  o   S  e  r  v  i  �  o";
						$this->distrito = $Tabela->f("DESSER");
					}
					elseif ($rel==427) // POR DISTRITO
					{
						$codigo = $Tabela->f("SUBSER");
						$tituloGrupo = "T  o  t  a  i  s    n  o    S  u  b     S  e  r  v  i  �  o";
						$this->distrito = $Tabela->f("SUBRED");
					}
					// Quebra a p�gina no grupo
					if (($rel>421) && ($codigo != $codigoVar))
					{
						
						$totalDistrito = (float)((str_replace(",", ".", $Grupo->f("TotalDistr"))));
						$totalDistrito = number_format($totalDistrito, 2,',','.');
						$this->Text(3,$Linha,$tituloGrupo);
						//           (       f i n a l                   ) -       t  a  m  n  h  o    
						$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
						$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($totalDistrito)),$Linha,$totalDistrito);

						$acresc = $Grupo->f("totReceita");
						$acresc = (float)((str_replace(",", ".", $acresc)));
						$acresc = number_format($acresc, 2,',','.');
						
						$this->StringTam = $this->GetStringWidth(' ACR�SCIMO ');
						$this->Text((($this->posAcres + $this->StringTam) - $this->GetStringWidth($acresc)),$Linha,$acresc);
						
						$valPGT = $Grupo->f("totRecebido");
						$valPGT = (float)((str_replace(",", ".", $valPGT)));
						$valPGT = number_format($valPGT, 2,',','.');
						
						$this->StringTam = $this->GetStringWidth('   RECEBIDO   ');
						$this->Text((($this->posRec + $this->StringTam) - $this->GetStringWidth($valPGT))-1,$Linha,$valPGT);
						
						$Grupo->next_record();
						
						$Linha = 54;
						$this->mudouDistrito = true;
						//$this->distrito = $codigo;
						$codigoVar = $codigo;
						$this->addPage(); 
						$this->SetFont('Arial','B',05);
						$this->mudouDistrito = false;
					}
				}
				$this->SetY($Linha);
				$Inscricao = $Tabela->f("CODCLI");
				$this->Text(3,$Linha,$Inscricao);
				$Cliente = $Tabela->f("DESCLI");
				$this->Text($this->posCliente,$Linha,$Cliente);
				$Fatura = $Tabela->f("CODFAT");
				$this->Text($this->posFatura,$Linha,$Fatura);
			
				$mesRef = $Tabela->f("MESREF");
				$this->Text($this->posMesAno+1,$Linha,$mesRef);
				$Venci = $Tabela->f("DATVNC");
				$this->Text($this->posVenc+1,$Linha,$Venci);
				$ValFat = $Tabela->f("VALFAT");
				$ValFat = (float)((str_replace(",", ".", $ValFat)));
				$ValFat = number_format($ValFat, 2,',','.');
				
				$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
				
				//           (       f i n a l                   ) -       t  a  m  n  h  o    
				$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($ValFat)),$Linha,$ValFat);
				
				$retINSS = $Tabela->f("RET_INSS");
				$retINSS = (float)((str_replace(",", ".", $retINSS)));
				$retINSS = number_format($retINSS, 2,',','.');
				$this->Text($this->posINSS,$Linha,$retINSS);
				
				$datPGT = $Tabela->f("DATPGT");
				$this->Text($this->posPGT+1,$Linha,$datPGT);
				
				$acresc = $Tabela->f("ACRESCIMO");
				$acresc = (float)((str_replace(",", ".", $acresc)));
				$acresc = number_format($acresc, 2,',','.');
				$this->StringTam = $this->GetStringWidth(' ACR�SCIMO ');
				$this->Text((($this->posAcres + $this->StringTam) - $this->GetStringWidth($acresc)),$Linha,$acresc);
	
				$valPGT = $Tabela->f("VALPGT");
				$valPGT = (float)((str_replace(",", ".", $valPGT)));
				$valPGT = number_format($valPGT, 2,',','.');
				$this->StringTam = $this->GetStringWidth('   RECEBIDO   ');
				$this->Text((($this->posRec + $this->StringTam) - $this->GetStringWidth($valPGT))-1,$Linha,$valPGT);
				
				$Linha+=4;
			}while ($Tabela->next_record());
			//                                 Q   U   E   B   R   A       D   E       P   �   G   I   N   A 
			if ($this->GetY() >= ($this->fh-12))
			{
				$Linha = 50;
				$this->addPage(); 
			}
			if ((!$this->mudouDistrito) && ($rel>421))
			{
				if ($rel==426) // por divisao
				{
					$codigo = $Tabela->f("GRPSER");
					$tituloGrupo = "T  o  t  a  i  s    n  o    S  e  r  v  i  �  o";
					$this->distrito = $Tabela->f("DESSER");
				}
				elseif ($rel==427) // por distrito
				{
					$codigo = $Tabela->f("SUBSER");
					$tituloGrupo = "T  o  t  a  i  s    n  o    S  u  b     S  e  r  v  i �  o";
					$this->distrito = $Tabela->f("SUBRED");
				}
				//$Grupo->next_record();
				$totalDistrito = (float)((str_replace(",", ".", $Grupo->f("TotalDistr"))));
				$totalDistrito = number_format($totalDistrito, 2,',','.');
				$this->Text(3,$Linha,$tituloGrupo);
				//           (       f i n a l                   ) -       t  a  m  n  h  o    
				$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
				$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($totalDistrito)),$Linha,$totalDistrito);
				$acresc = $Grupo->f("totReceita");
				$acresc = (float)((str_replace(",", ".", $acresc)));
				$acresc = number_format($acresc, 2,',','.');
				
				$this->StringTam = $this->GetStringWidth(' ACR�SCIMO ');
				$this->Text((($this->posAcres + $this->StringTam) - $this->GetStringWidth($acresc)),$Linha,$acresc);
				
				$valPGT = $Grupo->f("totRecebido");
				$valPGT = (float)((str_replace(",", ".", $valPGT)));
				$valPGT = number_format($valPGT, 2,',','.');
				
				$this->StringTam = $this->GetStringWidth('   RECEBIDO   ');
				$this->Text((($this->posRec + $this->StringTam) - $this->GetStringWidth($valPGT))-1,$Linha,$valPGT);

				$Linha += 4;
				$this->SetFont('Arial','B',05);
			}

			$this->StringTam = $this->GetStringWidth('VALOR DA FATURA');
			$this->Text(3,$Linha,"T  o  t  a  l  i  z  a  �  �  e  s  ");
			//           (       f i n a l                   ) -       t  a  m  n  h  o    
			$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$Linha,$this->totalGeral);
			
			$totAcresc = (float)((str_replace(",", ".", $totAcresc)));
			$totAcresc = number_format($totAcresc, 2,',','.');
			$this->StringTam = $this->GetStringWidth(' ACR�SCIMO ');
			$this->Text((($this->posAcres + $this->StringTam) - $this->GetStringWidth($totAcresc)),$Linha,$totAcresc);
	
			$totValPGT = (float)((str_replace(",", ".", $totValPGT)));
			$totValPGT = number_format($totValPGT, 2,',','.');
			$this->StringTam = $this->GetStringWidth('   RECEBIDO   ');
			$this->Text((($this->posRec + $this->StringTam) - $this->GetStringWidth($totValPGT))-1,$Linha,$totValPGT);

			//$this->SetMargins(5,5,5);
			$this->SetFont('Arial','U',10);
			$this->SetTextColor(0, 0, 0);
			$this->SetAutoPageBreak(1);
			$this->Output();
		}
	}
	function Header()
	{

		//global $title;
		//global $titulo;
		//global $mesRef;
		//global $posCliente;
		//global $posFatura;
		//global $posEmissao;
		//global $posValFat;
		//global $posVenc;
		//global $fwPt;
		//global $fw;
		$this->SetFont('Arial','B',20);
		// dimens�es da folha A4 - Largura = 210.6 mm e Comprimento 296.93 mm em Portrait. Em Landscape � s� inverter.
		$this->SetXY(0,0);
		//'SECRETARIA MUNICIPAL DE EDUCA��O DE BELO HORIZONTE'
		//$this->Cell(210,296,'',1,1,'C');
		//$this->Cell(210.6,296.93,'',1,0,'C',0,'');
		//Cell($w,$h=0,$txt='',$border=0,$ln=0,$align='',$fill=0,$link='')
		//$this->Cell($this->fw,$this->fh,'',1,0,'C');
		// Meio = (286/2) - (50/2) = 148,3 - 25 = 123,3
		$tanString = $this->GetStringWidth($this->title);
		$tamPonto = $this->fwPt;
		$tan = $this->fw;
		//$this->Text(($tamPonto/2) - ($tanString/2),6,$this->title);
		//$Unidade = CCGetSession("mDERCRI_UNI");
		$this->Text(($tan/2) - ($tanString/2),8,$this->title);

		//$this->Text(18,19,'DEPARTAMENTO DE ADMINISTRA��O FINANCEIRA');
			$this->SetFont('Arial','B',15);
		//$tanString = $this->GetStringWidth($Unidade);
		//$this->Text(($tan/2) - ($tanString/2),16,$Unidade);
			$this->SetFont('Arial','B',10);
		$tanString = $this->GetStringWidth($this->titulo);
		$this->Text(($tan/2) - ($tanString/2),24,$this->titulo);
		$this->Image('pdf/PBH_-_nova_-_Vertical_-_COR.jpg',5,10,33);
		//$mesRef = CCGetParam("mesRef");
		$this->Text(5,40,'M�s / Ano de Refer�ncia : '.$this->mesRef);
		$dataSist = "Data da Emiss�o : ".CCGetSession("DataSist");
		$tanString = $this->GetStringWidth($dataSist);
		$this->Text($tan-($tanString+5),40,$dataSist);
		$this->Line(0,41,210.6,41);
		//               T   I   T   U   L   O       D   A   S       C   O  L  U   N   A   S 
		
		if ($this->relat > 410 && $this->relat < 418) // Relatorio de Emissao
		{
			switch ($this->relat) 
			{
				case 411; //                       Relat�rio de Emissao por Inscri��o
					$this->SetY(45);
				break;
				case 412; //                       Relat�rio de Emissao por Divis�o
					/*
					if ($this->mudouDistrito)
					{
						$this->SetFont('Arial','B',10);
						$this->Text(05,45,"Divis�o : $this->distrito");
						$this->Line(0,47,210.6,47);
						$this->SetY(50);
					}
					else
					{
						$this->SetY(45);
					}
					*/
				break;
				case 413; //                       Relat�rio de Emissao por Distrito
					if ($this->mudouDistrito)
					{
						$this->SetFont('Arial','B',10);
						$this->Text(05,45,"Distrito : $this->distrito");
						$this->Line(0,47,210.6,47);
						$this->SetY(50);
					}
					else
					{
						$this->SetY(45);
					}
				break;
				case 414; //                       Relat�rio de Emissao por Atividade
					if ($this->mudouDistrito)
					{
						$this->SetFont('Arial','B',10);
						$this->Text(05,45,"Atividade : $this->distrito");
						$this->Line(0,47,210.6,47);
						$this->SetY(50);
					}
					else
					{
						$this->SetY(45);
					}
				break;
				case 415; //                       Relat�rio de Emissao por Sub-Atividade
					if ($this->mudouDistrito)
					{
						$this->SetFont('Arial','B',10);
						$this->Text(05,45,"Sub - Atividade : $this->distrito");
						$this->Line(0,47,210.6,47);
						$this->SetY(50);
					}
					else
					{
						$this->SetY(45);
					}
				break;
			}
			$this->SetFont('Arial','B',06);

			if (($this->relat != 412) || ($this->relat != 416) || ($this->relat != 417)){$this->Text(1,$this->GetY(),'INSCRI��O');}
			$tanString = $this->GetStringWidth('INSCRI��O');
			
			$this->SetX(1);
			if (($this->relat != 412) || ($this->relat != 416) || ($this->relat != 417)){($this->Text($this->GetX()+$tanString+5,$this->GetY(),'NOME DO CLIENTE'));}
			$this->posCliente = ($this->GetX()+$tanString+5);
			$this->SetX($this->GetX()+$tanString+5);
			$tanString = $this->GetStringWidth('NOME DO CLIENTE');
			
			if (($this->relat != 412) || ($this->relat != 416) || ($this->relat != 417)){($this->Text($this->GetX()+$tanString+85,$this->GetY(),'FATURA'));}
			$this->posFatura = ($this->GetX()+$tanString+85);
			$this->SetX($this->GetX()+$tanString+85);
			$tanString = $this->GetStringWidth('FATURA');
			
			if (($this->relat != 412) || ($this->relat != 416) || ($this->relat != 417)){($this->Text($this->GetX()+$tanString+10,$this->GetY(),'EMISS�O'));}
			$this->posEmissao = ($this->GetX()+$tanString+10);
			$this->SetX($this->GetX()+$tanString+10); 
			$tanString = $this->GetStringWidth('EMISS�O');
			
			if (($this->relat != 412) || ($this->relat != 416) || ($this->relat != 417)){($this->Text($this->GetX()+$tanString+10,$this->GetY(),'VALOR DA FATURA'));}
			$this->posValFat = ($this->GetX()+$tanString+10);
			$this->SetX($this->GetX()+$tanString+10);
			$tanString = $this->GetStringWidth('VALOR DA FATURA');
			$this->StringTam = $tanString;
			$this->posVenc = ($this->GetX()+$tanString+10);
			
			if (($this->relat != 412) || ($this->relat != 416) || ($this->relat != 417)){($this->Text($this->GetX()+$tanString+10,$this->GetY(),'VENCIMENTO'));}
			$this->Line(0,$this->GetY()+1,210.6,$this->GetY()+1);
		}
		elseif ($this->relat > 420 && $this->relat < 428) // Relat�rio de Receita
		{
			switch ($this->relat) 
			{
				case 421; //                       Relat�rio de Emissao por Inscri��o
					$this->SetY(45);
				break;
				case 422; //                       Relat�rio de Emissao por Divis�o
					if ($this->mudouDistrito)
					{
						$this->SetFont('Arial','B',10);
						$this->Text(05,45,"Divis�o : $this->distrito");
						$this->Line(0,47,210.6,47);
						$this->SetY(50);
					}
					else
					{
						$this->SetY(45);
					}
				break;
				case 423; //                       Relat�rio de Emissao por Distrito
					if ($this->mudouDistrito)
					{
						$this->SetFont('Arial','B',10);
						$this->Text(05,45,"Distrito : $this->distrito");
						$this->Line(0,47,210.6,47);
						$this->SetY(50);
					}
					else
					{
						$this->SetY(45);
					}
				break;
				case 424; //                       Relat�rio de Emissao por Atividade
					if ($this->mudouDistrito)
					{
						$this->SetFont('Arial','B',10);
						$this->Text(05,45,"Atividade : $this->distrito");
						$this->Line(0,47,210.6,47);
						$this->SetY(50);
					}
					else
					{
						$this->SetY(45);
					}
				break;
				case 425; //                       Relat�rio de Emissao por Sub-Atividade
					if ($this->mudouDistrito)
					{
						$this->SetFont('Arial','B',10);
						$this->Text(05,45,"Sub - Atividade : $this->distrito");
						$this->Line(0,47,210.6,47);
						$this->SetY(50);
					}
					else
					{
						$this->SetY(45);
					}
				break;
				case 426; //                       Relat�rio de Emissao por Servi�os
					if ($this->mudouDistrito)
					{
						$this->SetFont('Arial','B',10);
						$this->Text(05,45,"Servi�os : $this->distrito");
						$this->Line(0,47,210.6,47);
						$this->SetY(50);
					}
					else
					{
						$this->SetY(45);
					}
				break;
				case 427; //                       Relat�rio de Emissao por Sub - Servi�os
					if ($this->mudouDistrito)
					{
						$this->SetFont('Arial','B',10);
						$this->Text(05,45,"Sub - Servi�os : $this->distrito");
						$this->Line(0,47,210.6,47);
						$this->SetY(50);
					}
					else
					{
						$this->SetY(45);
					}
				break;
			}
			//$this->SetY(45);
			$this->SetFont('Arial','B',05);

			$this->Text(1,$this->GetY(),'INSCRI��O');
			$tanString = $this->GetStringWidth('INSCRI��O');
			
			$this->SetX(1);
			($this->Text($this->GetX()+$tanString+5,$this->GetY(),'NOME DO CLIENTE'));
			$this->posCliente = ($this->GetX()+$tanString+5);
			$this->SetX($this->GetX()+$tanString+5);
			$tanString = $this->GetStringWidth('NOME DO CLIENTE');
			
			($this->Text($this->GetX()+$tanString+55,$this->GetY(),'FATURA'));
			$this->posFatura = ($this->GetX()+$tanString+55);
			$this->SetX($this->GetX()+$tanString+55);
			$tanString = $this->GetStringWidth('FATURA');

			($this->Text($this->GetX()+$tanString+5,$this->GetY(),'MES/ANO'));
			$this->posMesAno = ($this->GetX()+$tanString+5);
			$this->SetX($this->GetX()+$tanString+5);
			$tanString = $this->GetStringWidth('MES/ANO');
			
			($this->Text($this->GetX()+$tanString+5,$this->GetY(),'VENCIMENTO'));
			$this->posVenc = ($this->GetX()+$tanString+5);
			$this->SetX($this->GetX()+$tanString+5);
			$tanString = $this->GetStringWidth('VENCIMENTO');
			
			($this->Text($this->GetX()+$tanString+5,$this->GetY(),'VALOR DA FATURA'));
			$this->posValFat = ($this->GetX()+$tanString+5);
			$this->SetX($this->GetX()+$tanString+5);
			$tanString = $this->GetStringWidth('VALOR DA FATURA');
			
			($this->Text($this->GetX()+$tanString+5,$this->GetY(),'   INSS   '));
			$this->posINSS = ($this->GetX()+$tanString+5);
			$this->SetX($this->GetX()+$tanString+5);
			$tanString = $this->GetStringWidth('   INSS   ');
			
			($this->Text($this->GetX()+$tanString+5,$this->GetY(),' PAGAMENTO '));
			$this->posPGT = ($this->GetX()+$tanString+5);
			$this->SetX($this->GetX()+$tanString+5);
			$tanString = $this->GetStringWidth(' PAGAMENTO ');
			
			($this->Text($this->GetX()+$tanString+5,$this->GetY(),' ACR�SCIMO '));
			$this->posAcres = ($this->GetX()+$tanString+5);
			$this->SetX($this->GetX()+$tanString+5);
			$tanString = $this->GetStringWidth(' ACR�SCIMO ');
			
			($this->Text($this->GetX()+$tanString+5,$this->GetY(),'   RECEBIDO   '));
			$this->posRec = ($this->GetX()+$tanString+5);
			$this->SetX($this->GetX()+$tanString+5);
			$tanString = $this->GetStringWidth('   RECEBIDO   ');
			
			$this->Line(0,$this->GetY()+1,210.6,$this->GetY()+1);
		}
		/*
                        @ 7, 0 say 'INSCRI'         
                        @ 7, 7 say 'NOME DO CLIENTE' 
                        @ 7, 53 say 'FATURA' 
                        @ 7, 60 say 'EMISSAO' 
                        @ 7, 71 say 'VR. DA FATURA' 
                        @ 7, 87 say ' VENCTO' 
		*/
		//$this->Text(28,37,'SE��O COMERCIAL');
		//$this->Text(26,43,'RELAT�RIO POR EMISS�O ');
		///$this->Text(26,49,'RELA��O GERAL DE CLIENTES FATURADOS');
		//$this->Line(0,59,$this->LineWidth,59);
		//$this->Text(26,59,$this->LineWidth);
		//$DBFatudbf = new clsDBFatudbf();
	
	
	
		//$this->SetXY(12,29);
		//$this->Cell(20,05.5,'EMPRESA:',1,1,'L');//135
		
		//$this->closeParsers();
	}
	
	function footer()
	{
    //Position at 1.5 cm from bottom
    //$this->SetY(-7);
    //Arial italic 8
	//$this->SetFont('Arial','B',06);
	//$this->Text(3,$this->GetY(),"T  o  t  a  l     G  e  r  a  l");
	//           (       f i n a l                   ) -       t  a  m  n  h  o    
	//$this->Text((($this->posValFat + $this->StringTam) - $this->GetStringWidth($this->totalGeral)),$this->GetY(),$this->totalGeral);
    //Position at 1.5 cm from bottom
    $this->SetY(-8);
    //Arial italic 8
    $this->SetFont('Arial','I',8);
	$this->Cell(0,10,'P�gina '.$this->PageNo().' / {nb}',0,0,'C');
    //Page number
	}
}
//unset($Tabela);
//unset($Grupo);
	/*$Imprime = new relatoriosPDF();
	$title = 'SUPERINTEND�NCIA DE LIMPESA URBANA';
	$Imprime->addPage('Portrait'); 
	$Imprime->SetFont('Arial','',10);
	$Imprime->SetTextColor(0, 0, 0);
	$Imprime->SetAutoPageBreak(0);
	$Imprime->Output();*/
?>