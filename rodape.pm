#!c:\perl\bin\perl.exe
package clsrodape; #rodape class @1-36AD7CE0

#use Common @1-8F9E1A3C
    use Common;
#End use Common

#Export @1-4F37A03E
    BEGIN {
        use Exporter();
        @clsrodape::ISA = qw( Exporter );
        @clsrodape::EXPORT = qw( $rodape );
    }
    use vars qw( $rodape );
#End Export

#Class_Initialize Event @1-9A1F8F78
    sub new {
        my $class               = shift;
        my $self                = {};
        $self->{RelativePath}   = shift;
        $self->{ComponentName}  = shift;
        $self->{ComponentType}  = "IncludablePage";
        $self->{Parent}         = shift;
        $self->{CCSEventResult} = undef;
        $self->{CCSEvents}      = {};
        $self->{Visible}        = 1;
        $self->{Attributes}     = $self->{Parent}->{Attributes};
        bless $self, $class;
        return $self;
    }
#End Class_Initialize Event

#Class_Terminate Event @1-F3928C13
    sub finalize {
        my $self  = shift;
        return if($self->{finalized});
        $self->{finalized} = 1;
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeUnload", $self);
    }
#End Class_Terminate Event

#BindEvents Method @1-36B295C1
    sub BindEvents {
        my $self = shift;
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "AfterInitialize", $self);
    }
#End BindEvents Method

#Operations Method @1-658ACBC2
    sub Operations {
        my $self = shift;
        return "" if ( !$self->{Visible} );
    }
#End Operations Method

#Initialize Method @1-9C277265
    sub Initialize {
        my $self = shift;
        return "" if ( !$self->{Visible} );
        $self->{FileName}         = "rodape.php";
        $self->{Redirect}         = "";
        $self->{TemplateFileName} = "rodape.html";
        $self->{BlockToParse}     = "main";
        $self->{PathToCurrentPage} = "/";
        $self->{TemplateEncoding} = "Cp1252";
        $self->BindEvents();
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "OnInitializeView", $self);
    }
#End Initialize Method

#Show Method @1-DC295443
    sub Show {
        my $self = shift;
        my $block_path = $Tpl->{block_path};
        $self->{CCSEventResult} = CCGetEvent($self->{CCSEvents}, "BeforeShow", $self);
        return if ( !$self->{Visible} );
        $Tpl->LoadTemplate("/" . $self->{TemplateFileName}, $self->{ComponentName}, $self->{TemplateEncoding}, "remove");
        $Tpl->{block_path} = $self->{ComponentName};
        $self->{Attributes}->Show();
        $Tpl->parse();
        my $html = $Tpl->getvar();
        $Tpl->{block_path} = $block_path;
        $Tpl->setvar($self->{ComponentName}, $html);
        return;
    }
#End Show Method

1; #End rodape Class @1-368FFC69



