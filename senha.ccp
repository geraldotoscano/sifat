<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" wizardTheme="Aqua" wizardThemeType="File" needGeneration="0" cachingEnabled="False" cachingDuration="1 minutes">
	<Components>
		<Record id="2" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" debugMode="False" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="Login" wizardCaption="Login" wizardTheme="Aqua" wizardThemeType="File" wizardOrientation="Vertical" wizardFormMethod="post" removeParameters="login;password">
			<Components>
				<TextBox id="8" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="txbDat_Ref" wizardTheme="Aqua" wizardThemeType="File">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="11"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="12" name="DatePicker_txbDat_Ref1" wizardTheme="Aqua" wizardThemeType="File" control="txbDat_Ref" wizardDatePickerType="Image" wizardPicture="Themes/DatePicker/DatePicker1.gif" style="Themes/Aqua/Style.css">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
				<TextBox id="5" fieldSourceType="DBColumn" dataType="Text" html="False" editable="True" hasErrorCollection="True" name="login" fieldSource="CODOPE" required="True" wizardTheme="Aqua" wizardThemeType="File" wizardCaption="Login" wizardSize="20" wizardMaxLength="100" wizardIsPassword="False" visible="Yes">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="6" fieldSourceType="DBColumn" dataType="Text" html="False" editable="True" hasErrorCollection="True" name="password" fieldSource="PASSWO" required="True" wizardTheme="Aqua" wizardThemeType="File" wizardCaption="Password" wizardSize="20" wizardMaxLength="100" wizardIsPassword="True" visible="Yes">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<Button id="3" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoLogin" wizardTheme="Aqua" wizardThemeType="File" wizardCaption="Login">
					<Components/>
					<Events>
						<Event name="OnClick" type="Server">
							<Actions>
								<Action actionName="Login" actionCategory="Security" id="4" redirectToPreviousPage="True" loginParameter="login" passwordParameter="password"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="PHPTemplates" name="senha_events.php" comment="//" codePage="iso-8859-1" forShow="False"/>
		<CodeFile id="Code" language="PHPTemplates" name="senha.php" comment="//" codePage="iso-8859-1" forShow="True" url="senha.php"/>
	</CodeFiles>
	<SecurityGroups/>
	<Events>
		<Event name="OnLoad" type="Client">
			<Actions>
				<Action actionName="Set Focus" actionCategory="General" id="7" form="Login" name="login"/>
			</Actions>
		</Event>
	</Events>
	<CachingParameters/>
	<Attributes/>
	<Features/>
</Page>
