<?php
//BindEvents Method @1-AAF2EA2A
function BindEvents()
{
    global $Login;
    $Login->txbDat_Ref->CCSEvents["BeforeShow"] = "Login_txbDat_Ref_BeforeShow";
    $Login->Button_DoLogin->CCSEvents["OnClick"] = "Login_Button_DoLogin_OnClick";
}
//End BindEvents Method

//Login_txbDat_Ref_BeforeShow @8-EAB80E68
function Login_txbDat_Ref_BeforeShow(& $sender)
{
    $Login_txbDat_Ref_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Login; //Compatibility
//End Login_txbDat_Ref_BeforeShow

//Custom Code @11-2A29BDB7
// -------------------------
   	$Tab_Dat_Hor_MesRef = new clsDBfaturar();
   	$Tab_Dat_Hor_MesRef->query("SELECT 
	                      to_char(sysdate,'dd/mm/yyyy')   as DataSist
					   FROM 
					      dual");
   	$Tab_Dat_Hor_MesRef->next_record();
	$Login->txbDat_Ref->SetValue($Tab_Dat_Hor_MesRef->f("DataSist"));
	
// -------------------------
//End Custom Code

//Close Login_txbDat_Ref_BeforeShow @8-1E3576BD
    return $Login_txbDat_Ref_BeforeShow;
}
//End Close Login_txbDat_Ref_BeforeShow

//Login_Button_DoLogin_OnClick @3-1454CF55
function Login_Button_DoLogin_OnClick(& $sender)
{
    $Login_Button_DoLogin_OnClick = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Login; //Compatibility
//End Login_Button_DoLogin_OnClick

    //CCSetSession("mSessao", $mCod_Divs);

//Login @4-319E0F84
    global $Login;
	/*
	ok -> Pr�prio do CodeCharge
	$xconst = $Login->password->Value;
	$xcripto = '';
	for (x = 1;x<=strlen(trim (ltrim (xconst))))
   		xcripto = xcripto + chr (asc (substr (xconst, x, 1)) - 50)
	next
	//return xcripto

	*/
    CCSetSession("mSessao", $ccs_result);
	$Login->password->Value = CCEncryptString($Login->password->Value, "5");

    if(!CCLoginUser($Login->login->Value, $Login->password->Value))
    {
        $Login->Errors->addError("Login or Password is incorrect.");
        $Login->password->SetValue("");
        $Login_Button_DoLogin_OnClick = false;
		CCSetSession("DataSist", "");
		CCSetSession("IDPerfil", "");
		CCSetSession("IDUsuario", "");

    }
    else
    {

        global $Redirect;
        $Redirect = CCGetParam("ret_link", $Redirect);
		$Redirect = "index.php";
        $Login_Button_DoLogin_OnClick = true;
		CCSetSession("DataSist", $Login->txbDat_Ref->GetValue());
		
        $Tabela = new clsDBfaturar();

		CCSetSession("IDPerfil", CCDLookUp("IDPERFIL","TABOPE","CODOPE='".$Login->login->GetValue()."'",$Tabela));
		CCSetSession("IDUsuario", CCDLookUp("ID","TABOPE","CODOPE='".$Login->login->GetValue()."'",$Tabela));

    }
	
//End Login

//Close Login_Button_DoLogin_OnClick @3-0EB5DCFE
    return $Login_Button_DoLogin_OnClick;
}
//End Close Login_Button_DoLogin_OnClick


?>
