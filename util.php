<?php
/*
Util.php

Este arquivo contem fun��es comuns uteis a varias partes do sistema sifat.

Alysson dos Santos 22/05/2013

Ultima altera��o 22/05/2013

*/


//Retorna o numero de dias entre 2 datas
function verificaData($inicio,$fim)
{
 //Inicio e fim sao vetores com [1] => ano [2] => mes [3] => dia

 $dt_inicio=mktime(0,0,0,intval($inicio[2]),intval($inicio[3]),intval($inicio[1]));
 $dt_fim=mktime(0,0,0,intval($fim[2]),intval($fim[3]),intval($fim[1]));
 
 return(ceil(($dt_fim-$dt_inicio) / 86400));
}
//20/03/12
//dd/mm/yy
//Retorna o numero de dias entre 2 datas a partir de uma string que veio do banco de dados
function verificaDataBD($inicio,$fim)
{
 //Inicio e fim sao vetores com [1] => ano [2] => mes [3] => dia
 
  $arr_aux=explode('/',$inicio);
  $arr_inicio=array(0,$arr_aux[2],$arr_aux[1],$arr_aux[0]);
  $arr_aux=explode('/',$fim);
  $arr_fim=array(0,$arr_aux[2],$arr_aux[1],$arr_aux[0]);
 
   return( verificaData($arr_inicio,$arr_fim)  );
}



//Converte uma string data no formarto dd/mm/yyyy em um vetor utilizado pelo codecharge
function data_todbdata($data_bd)
{
 $databd_aux=explode('/',$data_bd);
 $saida=array(
 0=>mktime(0,0,0,intval($databd_aux[1]),intval($inicio[0]),intval($inicio[2])),
 1=>intval($databd_aux[2]),
 2=>intval($databd_aux[1]),
 3=>intval($databd_aux[0]),
 4=>0,
 5=>0,
 6=>0,
 7=>0,
 8=>0,
 9=>0,
 10=>0,
 12=>0,
 13=>intval($databd_aux[2])
 );

 return( $saida );

}


//Converte uma data em um vetor utilizado pelo codecharge no formarto dd/mm/yyyy 
function data_tostrdata($data_bd)
{
 
 $saida=$data_bd[3].'/'.$data_bd[2].'/'.$data_bd[1];

 return( $saida );

}
//Cria ou adiciona campos para criar um sql
function addsqlcampos($campo,$tipo,$listacampos=array())
{
 $listacampos[]=array($campo,$tipo,'');
 return($listacampos);
}

//Altera o valor de um campo
function valorsqlcampos($campo,$valor,&$listacampos)
{
 
 $achou=false;
 for($i=0;$i<count($listacampos);$i++)
   {
    
    if( strtoupper(trim($listacampos[$i][0]))==strtoupper(trim($campo)) )
	  {
       $listacampos[$i][2]=$valor;
	   $achou=true;
	   break;
	  }
   }

if($achou==false)
  {
   trigger_error("Campo '".$campo."' n�o encontrado",E_USER_ERROR);
  }
   

}


function campossqlcampos(&$listacampos)
{
 $saida="(".$listacampos[0][0];
 for($i=1;$i<count($listacampos);$i++)
   {
    $saida=$saida.",".$listacampos[$i][0];
   }
 $saida=$saida.")";
 return($saida);

}


function valoressqlcampos(&$listacampos)
{
 $saida="(".valorsqlcampo($listacampos[0]);
 for($i=1;$i<count($listacampos);$i++)
   {
    $saida=$saida.",".valorsqlcampo($listacampos[$i]);
   }
 $saida=$saida.")";
 return($saida);

}

function valorsqlcampo(&$valor)
{
 $saida='';
 switch(strtoupper($valor[1]))
  {
   case 'S':
     $saida="'".$valor[2]."'";
     break;
   case 'D':
     $saida="to_date('".$valor[2]."','dd/mm/yyyy')";
     break;
   case 'N':
     $saida=$valor[2];
     break;
   case 'E':
     $saida=$valor[2];
     break;
   
  }

return($saida);

}

function mascaraTelefone($campo) {
	$valor = ltrim(preg_replace("/\D/",'',$campo->getValue()), '0');
	
	//aplica a m�scara (00)00000000 somente aos telefones n�o vazio e que n�o precisam ser truncados
	if($valor && strlen($valor)<12) {
		$campo->setValue('('.substr($valor,0,2).')'.substr($valor,2));
	}
}
function validaTelefone($campo) {
	$valor = $campo->GetValue();
	if(strlen($valor) && !preg_match("/^\(\d{2}\)9?\d{8}$/", $valor))
		$campo->Errors->addError("{$campo->Caption} inv�lido. Utilize o padr�o (XX)XXXXXXXXX, ou (XX)9XXXXXXXX se for celular.");
}

/**
 * Calcula os meses presentes em um per�odo para ser utilizado em consultas envolvendo o campo MESREF, j� que o mesmo � uma string (mm/aaaa) ao inv�s de um datetime
 * 
 * @param String $mesIni m�s inicial padr�o mm/aaaa
 * @param String $mesFim m�s final padr�o mm/aaaa
 * 
 * @return Array contendo todos os meses encontrado para o per�odo
 */
function mesesPeriodo($mesIni, $mesFim, $permiteDataFutura = false) {
    $ini = validaMesRef($mesIni, $permiteDataFutura);
    if(!$ini) throw new Exception("M�s inicial inv�lido ou muito antigo: $mesIni");    
    $fim = validaMesRef($mesFim, $permiteDataFutura);
    if(!$fim) throw new Exception('M�s final inv�lido'.($permiteDataFutura ? '' : ' ou maior que o m�s atual').": $mesFim");
    $qtd_meses = ($fim[1]*12+$fim[0]) - ($ini[1]*12+$ini[0]) + 1;
    if($qtd_meses<1) throw new Exception("O m�s final n�o pode ser anterior ao inicial");
    
    $meses = array();
    $ano = $ini[1]; $mes = $ini[0];
    for($i=0; $i<$qtd_meses; $i++) {
        if($mes==13) {
            $mes=1;
            $ano++;
        }
        $meses[] = str_pad($mes, 2, '0', STR_PAD_LEFT)."/$ano";
        $mes++;
    }
    return $meses;
}

/**
 * Verifica se um m�s no padr�o do sifat � v�lido
 * 
 * @param String $mes m�s no padr�o mm/aaaa
 * @param boolean $permiteDataFutura se aceitar m�s futuro como v�lido
 * 
 * @return Array [(int)m�s, (int)ano], ou (boolean) false, se m�s inv�lido  
 */
function validaMesRef($mes, $permiteDataFutura = false) {
    $mes = explode('/', $mes);
    if(count($mes)==2) {
        $mes[0] = (int)$mes[0];
        $mes[1] = (int)$mes[1];
        if($mes[0]>0 && $mes[1]>1980 && $mes[0] < 13 && $mes[1] < 3000) {
            if($permiteDataFutura)
                return $mes;
            else {
                $mesAtual = explode('/', date('m/Y'));
                if (($mes[1]*12+$mes[0]) <= ($mesAtual[1]*12+(int)$mesAtual[0]))
                    return $mes;
            }
        }
    }
    return false;
}

?>