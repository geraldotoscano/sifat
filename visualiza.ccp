<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Compact" wizardThemeVersion="3.0" needGeneration="0" isService="False">
	<Components>
		<IncludePage id="30" name="cabec" page="cabec.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="3" sourceType="Table" urlType="Relative" secured="True" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="CadcliSearch" wizardCaption="Search Cadcli " wizardOrientation="Vertical" wizardFormMethod="post" returnPage="visualiza.ccp" PathID="CadcliSearch" debugMode="False">
			<Components>
				<ListBox id="5" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="s_CGCCPF" wizardCaption="CGCCPF" wizardSize="14" wizardMaxLength="14" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="Faturar" dataSource="Cadcli" boundColumn="CGCCPF" textColumn="DESCLI" PathID="CadcliSearchs_CGCCPF" editable="True" hasErrorCollection="True">
					<Components/>
					<Events>
						<Event name="BeforeBuildSelect" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="25"/>
							</Actions>
						</Event>
					</Events>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<Button id="4" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="Search" PathID="CadcliSearchButton_DoSearch">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups>
				<Group id="28" groupID="1" read="True" insert="True" update="True" delete="True"/>
				<Group id="29" groupID="2" read="True" insert="True" update="True" delete="True"/>
			</SecurityGroups>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="2" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="Faturar" dataSource="Cadcli" name="Cadcli" pageSizeLimit="100" wizardCaption="List of Cadcli " wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="Nenhum cliente foi encontrado" PathID="Cadcli">
			<Components>
				<Label id="9" fieldSourceType="DBColumn" dataType="Text" html="False" name="Cadcli_TotalRecords" wizardUseTemplateBlock="False" editable="False">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Retrieve number of records" actionCategory="Database" id="10"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Label>
				<Sorter id="12" visible="True" name="Sorter_DESCLI" column="DESCLI" wizardCaption="DESCLI" wizardSortingType="SimpleDir" wizardControl="DESCLI" wizardAddNbsp="False" PathID="CadcliSorter_DESCLI">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="13" visible="True" name="Sorter_CGCCPF" column="CGCCPF" wizardCaption="CGCCPF" wizardSortingType="SimpleDir" wizardControl="CGCCPF" wizardAddNbsp="False" PathID="CadcliSorter_CGCCPF">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Label id="15" fieldSourceType="DBColumn" dataType="Text" html="False" name="DESCLI" fieldSource="DESCLI" wizardCaption="DESCLI" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" editable="False">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Link id="17" fieldSourceType="DBColumn" dataType="Text" html="False" name="CGCCPF" fieldSource="CGCCPF" wizardCaption="CGCCPF" wizardSize="14" wizardMaxLength="14" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="All" hrefSource="Pag_Rel_Faturas_Cliente.ccp" PathID="CadcliCGCCPF" editable="False">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Custom Code" actionCategory="General" id="34"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="22" sourceType="DataField" name="CODCLI" source="CODCLI"/>
						<LinkParameter id="23" sourceType="DataField" name="DESCLI" source="DESCLI"/>
						<LinkParameter id="24" sourceType="DataField" name="CGCCPF" source="CGCCPF"/>
					</LinkParameters>
				</Link>
				<Hidden id="21" fieldSourceType="DBColumn" dataType="Text" name="Hidden1" fieldSource="CODCLI" PathID="CadcliHidden1" html="False" editable="True" hasErrorCollection="True">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Hidden>
				<Navigator id="18" size="10" type="Simple" name="Navigator" wizardPagingType="Simple" wizardFirst="True" wizardFirstText="First" wizardPrev="True" wizardPrevText="Prev" wizardNext="True" wizardNextText="Next" wizardLast="True" wizardLastText="Last" wizardImages="Images" wizardPageNumbers="Simple" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="of" wizardImagesScheme="Compact" pageSizes="1;5;10;25;50" PathID="CadcliNavigator">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="11" conditionType="Parameter" useIsNull="False" field="CGCCPF" parameterSource="s_CGCCPF" dataType="Text" logicOperator="And" searchConditionType="Contains" parameterType="URL" orderNumber="1"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="19" tableName="Cadcli" posLeft="10" posTop="10" posWidth="115" posHeight="180"/>
			</JoinTables>
			<JoinLinks/>
			<Fields>
				<Field id="20" fieldName="*"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="PHPTemplates" name="visualiza_events.php" forShow="False" comment="//" codePage="iso-8859-1"/>
		<CodeFile id="Code" language="PHPTemplates" name="visualiza.php" forShow="True" url="visualiza.php" comment="//" codePage="iso-8859-1"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="31" groupID="1"/>
		<Group id="32" groupID="2"/>
		<Group id="33" groupID="3"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
