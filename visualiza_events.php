<?php
//BindEvents Method @1-AAC5D6AA
function BindEvents()
{
    global $CadcliSearch;
    global $Cadcli;
    $CadcliSearch->s_CGCCPF->ds->CCSEvents["BeforeBuildSelect"] = "CadcliSearch_s_CGCCPF_ds_BeforeBuildSelect";
    $Cadcli->Cadcli_TotalRecords->CCSEvents["BeforeShow"] = "Cadcli_Cadcli_TotalRecords_BeforeShow";
    $Cadcli->CGCCPF->CCSEvents["BeforeShow"] = "Cadcli_CGCCPF_BeforeShow";
}
//End BindEvents Method

//CadcliSearch_s_CGCCPF_ds_BeforeBuildSelect @5-CBE01E26
function CadcliSearch_s_CGCCPF_ds_BeforeBuildSelect(& $sender)
{
    $CadcliSearch_s_CGCCPF_ds_BeforeBuildSelect = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $CadcliSearch; //Compatibility
//End CadcliSearch_s_CGCCPF_ds_BeforeBuildSelect

//Custom Code @25-2A29BDB7
// -------------------------
  If ($CadcliSearch->s_CGCCPF->DataSource->Order == "") { 
     $CadcliSearch->s_CGCCPF->DataSource->Order = "descli";
  }
// -------------------------
//End Custom Code

//Close CadcliSearch_s_CGCCPF_ds_BeforeBuildSelect @5-A1E937CA
    return $CadcliSearch_s_CGCCPF_ds_BeforeBuildSelect;
}
//End Close CadcliSearch_s_CGCCPF_ds_BeforeBuildSelect

//Cadcli_Cadcli_TotalRecords_BeforeShow @9-68084115
function Cadcli_Cadcli_TotalRecords_BeforeShow(& $sender)
{
    $Cadcli_Cadcli_TotalRecords_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Cadcli; //Compatibility
//End Cadcli_Cadcli_TotalRecords_BeforeShow

//Retrieve number of records @10-ABE656B4
    $Component->SetValue($Container->DataSource->RecordsCount);
//End Retrieve number of records

//Close Cadcli_Cadcli_TotalRecords_BeforeShow @9-BEC6BBE4
    return $Cadcli_Cadcli_TotalRecords_BeforeShow;
}
//End Close Cadcli_Cadcli_TotalRecords_BeforeShow

//Cadcli_CGCCPF_BeforeShow @17-D5453A90
function Cadcli_CGCCPF_BeforeShow(& $sender)
{
    $Cadcli_CGCCPF_BeforeShow = true;
    $Component = & $sender;
    $Container = & CCGetParentContainer($sender);
    global $Cadcli; //Compatibility
//End Cadcli_CGCCPF_BeforeShow

//Custom Code @34-2A29BDB7
// -------------------------
	$mCgcCpf = $Cadcli->CGCCPF->GetValue();
	if (strlen(trim($mCgcCpf))==14)
	{
       $mCgcCpf = substr($mCgcCpf,0,2).".".substr($mCgcCpf,2,3).".".substr($mCgcCpf,5,3).".".substr($mCgcCpf,8,3).".".substr($mCgcCpf,11,2);
    }
	else
	{
       $mCgcCpf = substr($mCgcCpf,2,3).".".substr($mCgcCpf,5,3).".".substr($mCgcCpf,8,3).".".substr($mCgcCpf,11,2);
	}
    $Cadcli->CGCCPF->SetValue($mCgcCpf);
// -------------------------
//End Custom Code

//Close Cadcli_CGCCPF_BeforeShow @17-F30B4452
    return $Cadcli_CGCCPF_BeforeShow;
}
//End Close Cadcli_CGCCPF_BeforeShow


?>
